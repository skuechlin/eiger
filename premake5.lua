-- premake5.lua


newoption {
    trigger     = "builddir",
    description = "makefiles and binaries will be created here"
}

newoption {
    trigger     = "embreelibdir",
    description = "path to directory containing libembree3.so"
}

newoption {
    trigger     = "eigenincludedir",
    description = "path to directory containing the Eigen library headers"
}

newoption {
    trigger     = "embreesrcdir",
    description = "path to directory containing the embree source files"
}

if not _OPTIONS["builddir"] then
  premake.showhelp()
  premake.error("missing mandatory argument --builddir")
end

if not _OPTIONS["embreelibdir"] then
    premake.showhelp()
    premake.error("missing mandatory argument --embreelibdir")
end

if not _OPTIONS["eigenincludedir"] then
    premake.showhelp()
    premake.error("missing mandatory argument --eigenincludedir")
end

if not _OPTIONS["embreesrcdir"] then
    premake.showhelp()
    premake.error("missing mandatory argument --embreesrcdir")
end

local builddir_ = (_OPTIONS["builddir"])
local embreelibdir_ = (_OPTIONS["embreelibdir"])
local eigenincldir_ = (_OPTIONS["eigenincludedir"])
local embreesrcdir_ = (_OPTIONS["embreesrcdir"])


workspace "EIGER"
   configurations { "Release", "Debug" }
   location (builddir_) 

project "EIGER"
   kind "ConsoleApp"

   language "C++"

   makesettings [[ 
   VERSION_ID = $(shell git describe --always --dirty="-modified" --tags --abbrev=40)
   ]] 

   targetdir (path.join(builddir_,"bin/%{cfg.buildcfg}"))

   objdir (path.join(builddir_,"obj"))

   defines { "EIGEN_DONT_PARALLELIZE", "EIGEN_NO_DEBUG", "VERSION_ID=$(VERSION_ID)" }

   files { "src/**.cpp", "thirdparty/**.cpp" }

   links {"embree3"}

   libdirs {embreelibdir_}

   includedirs {"src","thirdparty",path.join(embreesrcdir_,"include"),eigenincldir_}

   buildoptions {"-std=c++17 -march=native -Wall -Wextra -pedantic -fopenmp -flto"}
   linkoptions {"-fopenmp -flto=24 -Wl,-rpath=" .. embreelibdir_ .. " -Wl,-rpath-link=" .. embreelibdir_}

   filter "configurations:Debug"
      defines { "DEBUG" }
      buildoptions {"-fno-omit-frame-pointer -g3 -Og"}

   filter "configurations:Release"
      defines { "NDEBUG" }
      buildoptions {"-Ofast"}
