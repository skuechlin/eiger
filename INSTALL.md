# Prerequisites

## Compiler - GCC 8.3.0 or newer

GCC requires the latest binutils, wich need to be installed into *the same directory as GCC*.  
Here, we assume this directory to be `gcc-install-prefix` (for example, `$HOME/gcc/gcc-8.3.0`).  

1.  Download, compile & install [GNU binutils 2.32](https://www.gnu.org/software/binutils/), including libiberty, which will be necessary for building ucx later on:  
    
        wget https://mirror.kumi.systems/gnu/binutils/binutils-2.32.tar.gz
        tar -xzf binutils-2.32.tar.gz
        cd binutils-2.32
        ./configure --prefix=gcc-install-prefix \
            --enable-gold \
            --enable-ld=default \
            --enable-plugins \
            --enable-shared \
            --disable-werror \
            --enable-64-bit-bfd \
            --enable-install-libiberty
        make
        make install       
  
2.  Download, compile & install [GCC 8.3.0](https://gcc.gnu.org/).  
    Automatic prerequisite download (gmp,mpfr,isl,...) is available via the script in the gcc contrib directory.  
    The prerequisites will be built and installed along with gcc automatically.          
    
        wget ftp://ftp.gnu.org/gnu/gcc/gcc-8.3.0/gcc-8.3.0.tar.gz
        tar -xzf ./gcc-8.3.0.tar.gz
        cd gcc-8.3.0
        ./contrib/download_prerequisites
        cd ..
        mkdir build_gcc && cd build_gcc
        ../gcc-8.3.0/configure --prefix=gcc-install-prefix \
            --enable-lto \
            --with-languages=all \
            --disable-nls \
            --disable-multilib \
            --enable-threads
        make
        make install       
        
3.  Make sure that all following steps are executed with the newly intstalled version of gcc by modifying the local environment appropriately.  
    A convenient way to do this is via the [Environment Modules](https://sourceforge.net/projects/modules/) package.

## Message passing library - OpenMPI 4.0.1 or newer, with UCX 1.5.1 or newer

1.  Download, compile & intall [UCX 1.5.1](http://www.openucx.org/) with multithreading support:  

        wget https://github.com/openucx/ucx/releases/download/v1.5.1/ucx-1.5.1.tar.gz
        tar -xzf ./ucx-1.5.1.tar.gz
        cd ucx-1.5.1
        ./contrib/configure_release --prefix=ucx-install-prefix --enable-mt
        make
        make install          
  
2.  Download, compile & install [OpenMPI 4.0.1](https://www.open-mpi.org/).  
    Important: configure openmpi to use the ucx version just installed!  
    The rest of the configuration command will depend on the local cluster environment.  
    For the Euler cluster using the platform LSF scheduler and a lustre file system, it reads as follows:          
        
        wget https://download.open-mpi.org/release/open-mpi/v4.0/openmpi-4.0.1.tar.gz
        tar -xzf ./openmpi-4.0.1.tar.gz
        cd openmpi-4.0.1.tar.gz
        ./configure --prefix=openmpi-install-prefix \ 
            --with-ucx=ucx-intall-prefix \
            --with-lsf=/cluster/apps/lsf/10.1/ \
            --with-lsf-libdir=/cluster/apps/lsf/10.1/linux2.6-glibc2.3-x86_64/lib/ \
            --with-io-romio-flags='--with-file-system=lustre+panfs+nfs+ufs --with-elan=no'
        make
        make install
        
## Linear algebra and vector library - Eigen 3.3.7 or newer

[Eigen](eigen.tuxfamily.org) is a header only library. Simply download and extract, then copy the directory `Eigen` to a local include path, e.g.  

    wget http://bitbucket.org/eigen/eigen/get/3.3.7.tar.gz 
    mkdir eigen-3.3.7 && tar -xzf ./3.3.7.tar.gz -C eigen-3.3.7 --strip-components 1
    cp -r eigen-3.3.7/Eigen/ $HOME/include/
    
## Ray tracing library - Intel Embree 3.5.2

We use the [Embree](https://www.embree.org/) source (as part of custom gridcell - geometry intersection detection routines).  
Additionaly, the pre-compiled embree binaries are used at link- and runtime.

1.  Embree source files are easiest obtained via `git`.  
    The path to the embree source files, in the example below `git/embree`, will be referred to as `embree_src_path` in the following. 

        cd git
        git clone https://github.com/embree/embree.git
        cd embree
        git checkout v3.5.2   
        
2.  Create empty `config.h`.  
    To stop some embree files from complaining about a missing include, execute the following:
           
        touch embree_src_path/kernels/config.h
  
3.  Embree binaries are available directly from the website. Simply downlowd and extract, then copy the contents of the `lib` directory to a local linker include path,
    which in the following will be assumed to be `embree_lib_path`:  
    
        wget https://github.com/embree/embree/releases/download/v3.5.2/embree-3.5.2.x86_64.linux.tar.gz
        tar -xzf ./embree-3.5.2.x86_64.linux.tar.gz
        cp ./embree-3.5.2.x86_64.linux/lib/* embree_lib_path      
  
4.  Patching Embree binary rpath: the embree library itself has libtbb as a dependency, which will not be found at link time without patching the rpath.  
    I suggest using the [patchelf](https://nixos.org/patchelf.html) tool for this purpose.         

        wget https://nixos.org/releases/patchelf/patchelf-0.10/patchelf-0.10.tar.gz
        tar -xzf ./patchelf-0.10.tar.gz
        cd patchelf-0.10
        ./configure --prefix=patchelf-install-dir
        make 
        make install
        patchelf-install-dir/bin/patchelf --set-rpath '$ORIGIN' embree_lib_path/libembree3.so     

## Configuration utility - Premake 5.0

Configuration of SPOC is most convenientltly done using the [Premake](https://premake.github.io/) utility.
Simply download and extract Premake binary:

    wget https://github.com/premake/premake-core/releases/download/v5.0.0-alpha14/premake-5.0.0-alpha14-linux.tar.gz
    tar -xzf premake-5.0.0-alpha14-linux.tar.gz
        
# Configure & Build

## Configuration via Premake

A lua script [premake5.lua](./premake5.lua) is provided for configuration via [Premake](https://premake.github.io/).
To configure, run premake from a directory where the binaries should be produced:

    mkdir build && cd build    
    premake5 \
        --file=eiger_src_path/premake5.lua \
        --verbose \
        --builddir=$(pwd) \
        --eigenincludedir=eigen_path \
        --embreelibdir=embree_lib_path \
        --embreesrcdir=embree_src_path \
        gmake

Custom compilation options (e.g. for a specific architecture) may be set by modifying [premake5.lua](./premake5.lua) appropriatly.

## Build

To build the Eiger binary, simply run `make` from the same directory where premake was executed ([see the previous step)](#markdown-header-configuration-via-premake).  
Note: it is important to specify the openmpi compiler wrapper explicitly!


    make CXX=mpic++ config=release -j 24
    

The argument `-j n` specifies a parallel build using `n` threads.

Voila. See [README.md](./README.md) for instructions on running simulations.
