/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * Constants.h
 *
 *  Created on: Apr 22, 2014
 *      Author: Stephan Kuechlin
 */

#ifndef CONSTANTS_H_
#define CONSTANTS_H_


namespace Constants {


  //
  constexpr double sqrt2	=  1.4142135623730950488016887242097;
  constexpr double invsqrt2	=  0.70710678118654752440084436210485;

  // the number pi
  constexpr double pi 		    =  3.1415926535897932384626433832795;
  constexpr double pisq         =  9.8696044010893586188344909998762;
  constexpr double piq		    = 31.006276680299820175476315067101;
  constexpr double sqrtpi 	    =  1.7724538509055160272981674833411;
  constexpr double sqrttwopi    =  2.5066282746310005024157652848110;
  constexpr double invsqrtpi    =  0.56418958354775628694807945156077;

  // Boltzmann constant
  constexpr double kB = 1.380649e-23; // [J K-1]

  // minimum cell volume below which cell will be blanked
  //    value chosen is < volume of Nitrogen molecule
  constexpr double minCellVolume = 1.e-29;   // [m3]

  constexpr double mindbl = std::numeric_limits<double>::min(); // >0 !!!
  constexpr double maxdbl = std::numeric_limits<double>::max();

}

#endif /* CONSTANTS_H_ */
