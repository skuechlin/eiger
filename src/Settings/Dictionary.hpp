/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * Dictionary.hpp
 *
 *  Created on: Nov 25, 2014
 *      Author: kustepha
 */

#ifndef SETTINGS_DICTIONARY_HPP_
#define SETTINGS_DICTIONARY_HPP_

namespace Settings
{

  template<typename T>
  inline
  bool
  Dictionary::isType() const
  {
    return isNumber();
  }


  template<>
  inline
  bool
  Dictionary::isType<bool>() const
  {
    return isBool();
  }

  template<>
  inline
  bool
  Dictionary::isType<std::string>() const
  {
    return isString();
  }

  template<>
  inline
  double
  Dictionary::get() const
  {
    return d_->GetDouble();
  }

  template<>
  inline
  bool
  Dictionary::get() const
  {
    return d_->GetBool();
  }

  template<>
  inline
  uint8_t
  Dictionary::get() const
  {
    return d_->GetUint();
  }

  template<>
  inline
  uint64_t
  Dictionary::get() const
  {
    return d_->GetUint64();
  }

  template<>
  inline
  int64_t
  Dictionary::get() const
  {
    return d_->GetInt64();
  }

  template<>
  inline
  std::string
  Dictionary::get() const
  {
    return d_->GetString();
  }

  template<typename T>
  inline
  std::vector<T>
  Dictionary::getAsVector() const
  {
    std::vector<T> ret;
    if (d_->IsArray())
      {
        ret.reserve(d_->Size());
        for (uint64_t i = 0; i < d_->Size(); ++i)
          ret.push_back( get<T>(i) );
      }
    else
      {
        MYASSERT( isType<T>(), "requested wrong type from dictionary");
        ret.push_back( get<T>() );
      }
    return ret;
  }



  template<typename T>
  inline
  T
  Dictionary::get(const std::string& key) const
  {
    MYASSERT(hasMember(key),
             std::string("couldn't find key \"") +
             key +
             std::string("\" in dict:\n") +
             this->toString());
    Dictionary d = get<Dictionary>(key);
    return d.get<T>();
  }

  template<typename T>
  inline
  T
  Dictionary::get(const IndexType& key) const
  {
    Dictionary d = get<Dictionary>(key);
    MYASSERT(d.isType<T>(),"requested wrong type from dictionary");
    return d.get<T>();
  }

  template<>
  inline
  Dictionary
  Dictionary::get<Dictionary>(const std::string& key) const
  {
    MYASSERT(hasMember(key),
             std::string("couldn't find key \"") +
             key +
             std::string("\" in dict:\n") +
             this->toString());
    rapidjson::Value* raw = d_.get();
    return std::shared_ptr<rapidjson::Value>(d_, &((*raw)[key.c_str()]) );
  }

  template<>
  inline
  Dictionary
  Dictionary::get<Dictionary>(const IndexType& key) const
  {
    rapidjson::Value* raw = d_.get();
    return std::shared_ptr<rapidjson::Value>(d_, &((*raw)[key]) );
  }



  template<typename T>
  inline
  void
  Dictionary::get(
      T target[],
      const IndexType& first,
      const IndexType& n
  ) const
  {

    MYASSERT(
        first+n <= this->size(),
        std::string("requested ") + std::to_string(n) + std::string(" elements, starting at ")
    + std::to_string(first)
    + std::string(", but array only has ") + std::to_string(this->size())
    );
    T* t = target;

    for (SizeType i = first; i < first+n; ++i)
      {
        *(t++) = this->get<T>(i);
      }

  }

  template<typename T>
  inline
  void
  Dictionary::get(
      T target[],
      const std::string& key,
      const IndexType& first,
      const IndexType& n
  ) const
  {
    Dictionary dict = this->get<Dictionary>(key);
    dict.get(target,first,n);
  }


  template<typename T>
  inline
  T
  Dictionary::get(
      const std::string& key,
      const T& defaultVal)
  const
  {

    if ( this->isEmpty() || !(this->hasMember(key)) ) return defaultVal;
    return this->get<T>(key);

  }


  template<typename T>
  inline
  Dictionary
  Dictionary::getDictByValue(
      const std::string& key,
      const T& value )
  const
  {

    if (this->isArray())
      {

        for (DictIt it = this->begin(); it != this->end(); ++it)
          {
            Dictionary d( std::shared_ptr<rapidjson::Value>(d_, (rapidjson::Value*)it ) );
            if (d.get<T>(key) == value)
              return d;
          }
      }
    else
      {
        if (this->get<T>(key) == value)
          return *this;
      }


    MYASSERT(false,"dictionary with key \"" + key + "\" == " + std::string(value) + " not found!");

    return Dictionary();

  }


}

#endif /* SETTINGS_DICTIONARY_HPP_ */
