/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * Settings.cpp
 *
 *  Created on: Jul 30, 2014
 *      Author: kustepha
 */

#include "Settings.h"

#include "Primitives/MyString.h"


void
Settings::Settings::writeToOutput()
{
  std::string of = MyString::catDirs(opts_.oDir_,std::string("settings.txt"));
  dictSrc_.write(of);
}


Settings::Settings::Settings (int argc, char* argv[], bool isRoot)
: opts_(argc,argv,isRoot)
, dictSrc_(opts_.sFileName_)
, dict_(dictSrc_)
{}

Settings::Settings::~Settings () = default;
