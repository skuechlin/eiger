/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * FileDict.h
 *
 *  Created on: Feb 10, 2016
 *      Author: kustepha
 */

#ifndef SRC_SETTINGS_FILEDICT_H_
#define SRC_SETTINGS_FILEDICT_H_

#include <string>

#include "Eigen/Dense"

#include "Settings/Dictionary.h"

namespace Settings {

	enum FILEFORMAT_TYPE: uint
	{
		STL,
		NASTRAN,
		TECPLOT,
		N_FILEFORMAT_TYPES,
		INVALID
	};

	class FileDict {
	public:

		bool isEmpty = true;

		Settings::Dictionary dict;

		std::string file_format_name;

		FILEFORMAT_TYPE file_format_type = FILEFORMAT_TYPE::INVALID;


		std::string file_name;
		std::string full_file_name;

		double tolerance = -1.;

		Eigen::Transform<double,4,Eigen::Affine> transform;

		double units_per_meter = -1.;
		double yaw_ = 0.;
		double pitch_ = 0.;
		double roll_ = 0.;

		Eigen::Vector4d translation_vector = Eigen::Vector4d::Zero();

		bool remove_outside_ = true;
		bool flip_normals_ = false;

	public:

		FileDict() = default;

		FileDict(const Settings::Dictionary& d, const std::string& inputDirectory);

		EIGEN_MAKE_ALIGNED_OPERATOR_NEW

	};

} /* namespace Settings */

#endif /* SRC_SETTINGS_FILEDICT_H_ */
