/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * Options.h
 *
 *  Created on: Dec 9, 2014
 *      Author: kustepha
 */

#ifndef SETTINGS_OPTIONS_H_
#define SETTINGS_OPTIONS_H_

#include <string>

class Options
{
public:

  std::string sFileName_;   // name of settings file
  std::string iDir_ = "./"; // input directory
  std::string oDir_ = "./"; // output directory

  uint64_t npc_ = 0;        // number of particles to preallocate per core

  Options (int argc, char* argv[], bool isRoot = true);
  ~Options (){}
};

#endif /* SETTINGS_OPTIONS_H_ */
