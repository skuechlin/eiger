/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * Options.cpp
 *
 *  Created on: Dec 9, 2014
 *      Author: kustepha
 */

#include <vector>
#include <iostream>
#include <mpi.h>

// ignore certain warnings in optionparser header
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wmaybe-uninitialized"
#include <optionparser.h>
#pragma GCC diagnostic pop

#include "Options.h"

#include "Primitives/MyError.h"
#include "Primitives/MyString.h"

#include "licenses/licenses.h"

namespace
{
  enum  optionIndex
  {
    UNKNOWN,
    HELP,
    LICENSES,
    SFILE,
    IDIR,
    ODIR,
    NPC
  };
  static option::ArgStatus Required(const option::Option& option, bool msg)
  {

    static_cast<void>(msg);

    MYASSERT(option.arg != 0 && !(std::string(option.arg).empty()),
	     std::string("Option \"") + std::string(option.name) + std::string("\" requires an argument\n"));

    return option::ARG_OK;
  }
  constexpr option::Descriptor usage[] =
      {
//         const unsigned   index
//             Index of this option's linked list in the array filled in by the parser.
//         const int   type
//             Used to distinguish between options with the same index. See index for details.
//         const char *const   shortopt
//             Each char in this string will be accepted as a short option character.
//         const char *const   longopt
//             The long option name (without the leading -- ).
//         const CheckArg  check_arg
//             For each option that matches shortopt or longopt this function will be called to check a potential argument to the option.
//         const char *    help
//             The usage text associated with the options in this Descriptor.
//

          {
              UNKNOWN, 0, "" ,  "",                 option::Arg::None,
              "USAGE: EIGER -s \"settings-file\" [options]\n\n"  "Options:"
          },
          {
              HELP,    0, "h",  "help",             option::Arg::None,
              "  --help \t-h  \tPrint usage and exit."
          },
          {
              LICENSES,0, "l",  "show-licenses",    option::Arg::None,
              "  --show-licenses \t-l  \tPrint third-party software licenses and exit."
          },
          {
              SFILE,   0, "s",  "settings-file",    Required,
              "  --settings-file \t-s  \tThe file containing JSON formatted settings for the program run."
          },
          {
              IDIR,    0, "i",  "input-directory",  Required,
              "  --input-directory \t-i  \tThe directory relative to which program will read input data."
          },
          {
              ODIR,    0, "o",  "output-directory", Required,
              "  --output-directory \t-o  \tThe directory relative to which program will write output data."
          },
          {
              NPC,     0, "n",  "prealloc-per-core",Required,
              "  --prealloc-per-core \t-n \tThe number of particles per core for which the program should pre-allocate memory."
          },
          {
              UNKNOWN, 0, "" ,  "",                 option::Arg::None,
              "\nExamples:\n"
              "  EIGER --settings-file \"/path-to-settings-file/settings-file\"\n"
              "  EIGER -s \"settings-file\" -i \"/data-directory\" -o \"/output-directory\"\n"
          },
          {
              0,0,0,0,0,0
          }
      };

}

Options::Options (int argc, char* argv[], bool isRoot)
{

  argc-=(argc>0); argv+=(argc>0); // skip program name argv[0] if present
  option::Stats  stats(usage, argc, argv);
  std::vector<option::Option> options(stats.options_max), buffer(stats.buffer_max);
  option::Parser parse(usage, argc, argv, options.data(), buffer.data());

  MYASSERT(!parse.error(),"Fatal error parsing program command line options!");


  if ( isRoot && (options[HELP] || argc == 0 || options[UNKNOWN]) ) {
    option::printUsage(std::cout, usage);
    MPI_Finalize();
    exit(0);
  }

  if ( isRoot && options[LICENSES] ) {
    ThirdpartyLicenses::print_thirdparty_licenses();
    MPI_Finalize();
    exit(0);
  }


  MYASSERT(options[SFILE] || parse.nonOptionsCount() == 1,
	   "Program invoked without specifying settings file.");


  if (isRoot)
    {
      for (option::Option* opt = options[UNKNOWN]; opt; opt = opt->next())
	std::cout << "Unknown option: " << opt->name << "\n";
    }

  MYASSERT(!options[UNKNOWN],"Program invoked with unknown options.");

  if (isRoot)
    {
      for (int i = (options[SFILE] ? 0 : 1); i < parse.nonOptionsCount(); ++i)
	std::cout << "Non-option #" << i << ": " << parse.nonOption(i) << "\n";
    }

  MYASSERT(
      parse.nonOptionsCount() == 0
      || (parse.nonOptionsCount() == 1 && !options[SFILE]),
      "Program invoked with unusable non-options.");


  // now get options
  if (options[SFILE])
    sFileName_ = std::string(options[SFILE].arg);
  else
    sFileName_ = std::string(parse.nonOption(0));

  if (options[IDIR])
    iDir_ = MyString::makeLastCharDirSep(std::string(options[IDIR].arg));

  if (options[ODIR])
    oDir_ = MyString::makeLastCharDirSep(std::string(options[ODIR].arg));

  if (options[NPC])
    npc_ = strtoul(options[NPC].arg, 0, 0);


}

