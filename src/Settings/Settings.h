/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * Settings.h
 *
 *  Created on: Jul 30, 2014
 *      Author: kustepha
 */

#ifndef SETTINGS_H_
#define SETTINGS_H_

#include <string>
#include <memory>

#include "Options.h"
#include "Dictionary.h"

class Simulation;
class SimulationCore;
class SimulationBox;

namespace Settings
{


class Settings
{

public:

	Options opts_;

	DictionarySource dictSrc_;

	Dictionary dict_;


	void
	addGasModel (
			SimulationCore* core,
			const Dictionary& dict);

public:

	const Options* opts() const { return &opts_; }

	const Dictionary* dict() const { return &dict_; }

	void writeToOutput();


public:

	Settings (int argc, char* argv[], bool isRoot = true);
	~Settings ();
};


}


#endif /* SETTINGS_H_ */
