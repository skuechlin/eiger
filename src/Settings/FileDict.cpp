/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * FileDict.cpp
 *
 *  Created on: Feb 10, 2016
 *      Author: kustepha
 */

#include <map>

#include <iostream>

#include <Settings/FileDict.h>

#include "Primitives/MyString.h"

namespace {


	static constexpr char FILEFORMAT_NAME[Settings::FILEFORMAT_TYPE::N_FILEFORMAT_TYPES][24] = {
			"stl",
			"nastran",
			"tecplot",
	};

	std::map<std::string,Settings::FILEFORMAT_TYPE>
	makeFileFormatNameTypeMap()
	{
		std::map<std::string,Settings::FILEFORMAT_TYPE> theMap;
		for (uint i = 0; i < Settings::FILEFORMAT_TYPE::N_FILEFORMAT_TYPES; ++i)
			theMap.insert({FILEFORMAT_NAME[i],(Settings::FILEFORMAT_TYPE)i});
		return theMap;
	}

} // namespace


Settings::FileDict::FileDict(const Settings::Dictionary& d, const std::string& inputDirectory)
{
	isEmpty = false;

	dict = d;

	MYASSERT(dict.hasMember("format"),"file dictionary missing entry \"format\"!");
	MYASSERT(dict.hasMember("name"),"file dictionary missing entry \"name\"!");

	file_format_name = dict.get<std::string>("format");

	static std::map<std::string,FILEFORMAT_TYPE> fileFormatNameTypeMap
	= makeFileFormatNameTypeMap();


	MYASSERT(fileFormatNameTypeMap.find(file_format_name) != fileFormatNameTypeMap.end(),
			std::string("unsupported file format \"") + file_format_name + std::string("\""));

	file_format_type = fileFormatNameTypeMap[file_format_name];

	file_name = dict.get<std::string>("name");

	full_file_name = MyString::catDirs( inputDirectory, file_name );

	tolerance = dict.get<double>("tol",-1.0);

	units_per_meter = dict.get<double>("per meter",1.);

	yaw_ = dict.get<double>("yaw",0.) * M_PI / 180.;
	pitch_ = dict.get<double>("pitch",0.) * M_PI / 180.;
	roll_ = dict.get<double>("roll",0.)* M_PI / 180.;

	transform = Eigen::Transform<double,4,Eigen::Affine>::Identity();

	transform.prescale( 1. / units_per_meter );

	Eigen::Matrix4d m4 = Eigen::Matrix4d::Identity();

	m4.topLeftCorner(3,3) = (Eigen::AngleAxisd(yaw_,Eigen::Vector3d::UnitZ())
    *Eigen::AngleAxisd(pitch_,Eigen::Vector3d::UnitY())
	*Eigen::AngleAxisd(roll_,Eigen::Vector3d::UnitX())).toRotationMatrix();

	transform.prerotate(m4);

//	std::cout << transform.rotation() << std::endl;

	remove_outside_ = dict.get<bool>("remove outside",true);
	flip_normals_ = dict.get<bool>("flip normals",false);

	if (dict.hasMember("translate"))
	{
		dict.get(translation_vector.data(),"translate",0,3);
		transform.pretranslate( translation_vector );
	}

} // ctor



