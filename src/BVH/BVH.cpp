/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * BVH.cpp
 *
 *  Created on: Dec 14, 2015
 *      Author: kustepha
 */


#include <omp.h>
#include <cfenv>

#include <BVH/BVH.h>

#include "Primitives/Progress.h"

// ignore certain warnings in embree headers
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wsign-compare"
#pragma GCC diagnostic ignored "-Wunused-but-set-variable"
#pragma GCC diagnostic ignored "-Wmisleading-indentation"
#pragma GCC diagnostic ignored "-Wstrict-aliasing"
#pragma GCC diagnostic ignored "-Wparentheses"
#pragma GCC diagnostic ignored "-Wunused-variable"
#pragma GCC diagnostic ignored "-Wunused-parameter"
#pragma GCC diagnostic ignored "-Wreorder"
#pragma GCC diagnostic ignored "-Wnarrowing"
#pragma GCC diagnostic ignored "-Wpedantic"
#pragma GCC diagnostic ignored "-Wempty-body"

// use internal task manager
#ifndef TASKING_INTERNAL
#ifndef TASKING_TBB
#define TASKING_INTERNAL
#endif
#endif


//#include "../kernels/xeon/bvh/bvh.h"
//#include "../kernels/xeon/bvh/bvh_traverser1.h"
//#include "../kernels/xeon/geometry/object.h"
//#include "../kernels/common/stack_item.h"
//#include "../kernels/common/accelset.h"
//#include "../common/sys/intrinsics.h"

#include "../kernels/bvh/bvh.h"
#include "../kernels/bvh/bvh_traverser1.h"
#include "../kernels/geometry/object.h"
#include "../kernels/common/stack_item.h"
#include "../kernels/common/accelset.h"
#include "../common/math/bbox.h"
//#include "../common/sys/intrinsics.h"


#pragma GCC diagnostic pop

#include "Geometry/Shape.h"

namespace BVH {


  namespace
  {


    // return intersection mask of aabb with N node children
    template<int N>
    bool
    NODE_AABB_intersect(
        const typename embree::BVHN<N>::NodeRef& cur,
        const embree::BBox<embree::Vec3<embree::vfloat<N>>>& box,
        size_t& mask )
    {
      using namespace embree;

      if (unlikely(cur.isLeaf())) return false; // no intersection performed

      const typename BVHN<N>::AlignedNode* __restrict__ node = cur.alignedNode();

      vboolf<N> vmask(embree::True);

      vmask &= min(box.upper.x,node->upper_x) >= max(box.lower.x,node->lower_x);
      vmask &= min(box.upper.y,node->upper_y) >= max(box.lower.y,node->lower_y);
      vmask &= min(box.upper.z,node->upper_z) >= max(box.lower.z,node->lower_z);

      mask = movemask(vmask);

      return true; // intersection was performed

    }


    template<int N>
    embree::Vec3fa
    relax_posInf(const embree::Vec3fa f)
    {
      using namespace embree;
      using vf = embree::Vec3fa;
      return (vf(1.0f) + sign(f)*vf(2.0f*float(embree::ulp))) * f;
    }

    template<int N>
    embree::Vec3fa
    relax_negInf(const embree::Vec3fa f)
    {
      using namespace embree;
      using vf = embree::Vec3fa;
      return (vf(1.0f) - sign(f)*vf(2.0f*float(embree::ulp))) * f;
    }

    __forceinline
    bool test_Object_AABB_intersect(
        const embree::Scene* const scene,
        const embree::Object& prim,
        const Eigen::AlignedBox4d& aabb )
    {
      // see ObjectIntersector1 in object_intersector.h

      const embree::UserGeometry* geom = scene->get<embree::UserGeometry>(prim.geomID());

      if (geom->isDisabled()) return false; // do not test this geometry

      const Shapes::Shape* sh = (const Shapes::Shape*)(geom->getUserData());

      return sh->element(prim.primID())->test_AABB(aabb);
    }


    template<int N>
    bool
    test_BVHN_AABB_intersect(
        const embree::BVHN<N>* const __restrict__ bvh,
        const Eigen::AlignedBox4d& __restrict__ aabb)
    {

      using namespace embree;
      using BVH = BVHN<N>;
      using NodeRef = typename BVH::NodeRef;
      using vf = vfloat<N>;

      constexpr size_t stackSize = 1+(N-1)*BVH::maxDepth+3; // +3 due to 16-wide store


      // scene pointer
      const embree::Scene* const scene = bvh->scene;

      // see bvh_intersector1.cpp

      /*! stack state */
      NodeRef stack[stackSize];  //!< stack of nodes that still need to get traversed
      NodeRef* stackPtr = stack+1;        //!< current stack pointer
      NodeRef* stackEnd = stack+stackSize;
      stack[0] = bvh->root;

      /*! load the aabb into SIMD registers */
      __m256d lowerd = *(__m256d*)&aabb.min();
      __m256d upperd = *(__m256d*)&aabb.max();
      const Vec3fa lower = relax_negInf<N>( _mm256_cvtpd_ps( lowerd )  );
      const Vec3fa upper = relax_posInf<N>( _mm256_cvtpd_ps( upperd )  );

      /* broadcast to BVH node size */
      const BBox<Vec3<vf>> box( lower, upper );

      /*! initialize the node traverser */
      embree::isa::BVHNNodeTraverser1Hit<N,N,BVHNodeFlags::BVH_FLAG_ALIGNED_NODE> nodeTraverser;


      /* pop loop */
      while (true) pop:
          {
          /*! pop next node */
          if (unlikely(stackPtr == stack)) break;
          stackPtr--;
          NodeRef cur = (NodeRef) *stackPtr;

          /* downtraversal loop */
          while (true)
            {
              /* intersect node */
              size_t mask; vf dist;
              //              bool nodeIntersected = NODE_AABB_intersect<N>(cur,min_x,min_y,min_z,max_x,max_y,max_z,mask);
              bool nodeIntersected = NODE_AABB_intersect<N>(cur,box,mask);
              if ( unlikely(!nodeIntersected) ) { break; } // leaf node

              /*! if no child is hit, pop next node */
              if (unlikely(mask == 0))
                goto pop;

              /* select next child and push other children */
              nodeTraverser.traverseAnyHit(cur,mask,dist,stackPtr,stackEnd);
            }

          /*! this is a leaf node */
          //         assert(cur != BVH::emptyNode);

          // get array of objects in leaf
          size_t num; Object* prim = (Object*) cur.leaf(num);

          // process objects
          // see ArrayIntersector1 in intersector_iterators.h
          for (size_t i=0; i<num; ++i)
            if( test_Object_AABB_intersect(scene, prim[i], aabb ) ) return true;
          } // pop loop

      return false;
    }




    /* prints the bvh */
    template<int N>
    void print_BVHN(
        embree::Scene* scene,
        typename embree::BVHN<N>::NodeRef node,
        size_t depth,
        size_t shape_type_count[])
    {
      if (!(node.isLeaf()))
        {
          typename embree::BVHN<N>::AlignedNode* n = node.alignedNode();

          std::cout << "Node {" << std::endl;
          for (size_t k=0; k<depth; k++) std::cout << "  ";
          std::cout << "  type = " << node.type() << std::endl;
          for (size_t i=0; i<N; i++)
            {
              for (size_t k=0; k<depth; k++) std::cout << "  ";
              std::cout << "  bounds" << i << " = " << n->bounds(i) << std::endl;
            }

          for (size_t i=0; i<N; i++)
            {
              if (n->child(i) == embree::BVHN<N>::emptyNode)
                continue;

              for (size_t k=0; k<depth; k++) std::cout << "  ";
              std::cout << "  child" << i << " = ";
              print_BVHN<N>(scene,n->child(i),depth+1,shape_type_count);
            }
          for (size_t k=0; k<depth; k++) std::cout << "  ";
          std::cout << "}" << std::endl;
        }
      else
        {
          size_t num;
          const embree::Object* ob = (const embree::Object*)(node.leaf(num));
          const embree::Geometry* geom = scene->get(ob->geomID());
          const Shapes::Shape* sh = static_cast<const Shapes::Shape*>(geom->getUserData());

          ++shape_type_count[sh->type()];

          std::cout << "Leaf {" << std::endl;
          std::cout << "  Object { "
              << " geomID = " 		<< ob->geomID()
              << ", geom->geomID = " 	<< geom->geomID
              << ", shape = " 		<< sh->name();



          std::cout << " }" << std::endl;
          //	      for (size_t i=0; i<num; i++) {
          //	        for (size_t j=0; j<sh[i].size(); j++) {
          //	          for (size_t k=0; k<depth; k++) std::cout << "  ";
          //	          std::cout << "  Shape { type = " //<< sh[i].type() <<
          //	            <<", geomID = " << sh[i].geomID(j) << ", primID = " << sh[i].primID(j) << " }" << std::endl;
          //	        }
          //	      }
          for (size_t k=0; k<depth; k++) std::cout << "  ";
          std::cout << "}" << std::endl;
        }
    }


    bool test_AABB_forwarder( const embree::AccelData* accel, const Eigen::AlignedBox4d& aabb )
    {
      switch (accel->type) {
        case embree::Accel::Type::TY_UNKNOWN:
          MYASSERT(false,"got embree::Accel::Type::TY_UNKNOWN as bvh type in BVH::test_AABB(const Eigen::AlignedBox4d& aabb)");
          break;
        case embree::Accel::Type::TY_ACCELN:
          MYASSERT(false,"got embree::Accel::Type::TY_ACCELN as bvh type in BVH::test_AABB(const Eigen::AlignedBox4d& aabb)");
          break;
        case embree::Accel::Type::TY_ACCEL_INSTANCE:
          MYASSERT(false,"got embree::Accel::Type::TY_ACCEL_INSTANCE as bvh type in BVH::test_AABB(const Eigen::AlignedBox4d& aabb)");
          break;
        case embree::Accel::Type::TY_BVH4:
          return test_BVHN_AABB_intersect<4>( (embree::BVH4*)accel, aabb );
          break;
        case embree::Accel::Type::TY_BVH8:
          return test_BVHN_AABB_intersect<8>( (embree::BVH8*)accel, aabb );
          break;
      }
      return false;
    }

  } // namespace


  bool BVH::geom_enabled_tf(const uint64_t geomID) const {
    return getGeom(geomID)->isEnabled(); }

  bool BVH::modified_tf() const {  return getScene()->isModified(); }


  bool
  BVH::test_AABB(const Eigen::AlignedBox4d& aabb)
  const
  {

    using namespace embree;

    /* if the scene contains only one geometry, acceleration structure can be obtained this way */
    const AccelData* accel = ((Accel*)getScene())->intersectors.ptr;
    if (accel->type != AccelData::TY_ACCELN)
      {
        return test_AABB_forwarder( accel, aabb );
      }
    else //if (accel->type == AccelData::TY_ACCELN)
      {
        /* if there are also other geometry types, one has to iterate over the toplevel AccelN structure */
        AccelN* accelN = (AccelN*)(accel);
        for (size_t i=0; i<accelN->accels.size(); i++) {
            if(
                test_AABB_forwarder( accelN->accels[i]->intersectors.ptr, aabb )
            ) return true;
        }
      }
    return false;
  }

  void
  BVH::print()
  const
  {
    size_t shape_type_count[Shapes::SHAPE_TYPE::N_SHAPE_TYPES] = {0};

    const auto& accels = getScene()->accels;

    // see acceln.cpp
    for (size_t i=0; i<accels.size(); i++)
      {

        const embree::AccelData* accel = accels[i]->intersectors.ptr;

        switch (accel->type) {
          case embree::Accel::Type::TY_UNKNOWN:
            MYASSERT(false,"got embree::Accel::Type::TY_UNKNOWN as bvh type in BVH::print()");
            break;
          case embree::Accel::Type::TY_ACCELN:
            MYASSERT(false,"got embree::Accel::Type::TY_ACCELN as bvh type in BVH::print()");
            break;
          case embree::Accel::Type::TY_ACCEL_INSTANCE:
            MYASSERT(false,"got embree::Accel::Type::TY_ACCEL_INSTANCE as bvh type in BVH::print()");
            break;
          case embree::Accel::Type::TY_BVH4:
            {
              const embree::BVH4* bvh4 = (embree::BVH4*)accel;
              print_BVHN<4>(bvh4->scene,bvh4->root,0,shape_type_count);
              std::cout << std::endl;
            }
            break;
          case embree::Accel::Type::TY_BVH8:
            {
              const embree::BVH8* bvh8 = (embree::BVH8*)accel;
              print_BVHN<8>(bvh8->scene,bvh8->root,0,shape_type_count);
              std::cout << std::endl;
            }
            break;
          default:
            MYASSERT(false,"enum out of range in BVH::print()");
            break;
        }

      }

    for (uint i = 0; i < Shapes::SHAPE_TYPE::N_SHAPE_TYPES; ++i)
      std::cout << Shapes::SHAPE_NAME[i] << ": " << shape_type_count[i] << std::endl;

    std::cout << std::endl;

  }


  bool progressMonitorFun(void*, const double n)
  {

    printProgress(n);
    return true; // dont't cancel
  }



  // build the bvh
  void
  BVH::commit()
  {

    // disable any floating point exceptions
    const int feflags = fedisableexcept(FE_ALL_EXCEPT);

    //    progressMonitorFun(nullptr,1.0); // ensure printing 100%
    //    progressMonitorFun(nullptr,1.1); // finalize
    //
    //    std::cout << std::endl;
    //
    //
    //    // deregister progress callback
    //    rtcSetSceneProgressMonitorFunction(scene_, nullptr, nullptr);


    rtcCommitScene(scene_);

    // reset floating point exception status
    feenableexcept(feflags);
  }



  BVH::BVH()
  : device_( // initialize the ray tracing core
      rtcNewDevice("ignore_config_files=1") )
  , scene_( // create ray tracing scene
      rtcNewScene(device_) )
  {
    rtcSetSceneBuildQuality(scene_,RTC_BUILD_QUALITY_HIGH);
    rtcSetSceneFlags(scene_,RTC_SCENE_FLAG_ROBUST);
  }

  BVH::~BVH()
  {

    // delete the ray tracing scene
    rtcReleaseScene(scene_);

    // delete the ray tracing device
    rtcReleaseDevice(device_);

  }


} /* namespace BVH */



