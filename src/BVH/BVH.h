/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * BVH.h
 *
 *  Created on: Dec 14, 2015
 *      Author: kustepha
 */

#ifndef SRC_BVH_BVH_H_
#define SRC_BVH_BVH_H_

#ifndef TASKING_TBB_INTERNAL
#define TASKING_TBB_INTERNAL
#endif

#include <embree3/rtcore.h>
#include <Eigen/Dense>

#include "Primitives/MyError.h"

#include "Geometry/Ray.h"
#include "Geometry/Shape.h"

namespace Boundary
{
	class BoundaryManager;
}

namespace embree
{
  class Scene;
  class Geometry;
}

namespace BVH {

	class BVH {

		friend class Boundary::BoundaryManager;

	private:

		// ray tracing device
		RTCDevice device_;

		// ray tracing scene for ray intersection queries
		RTCScene scene_;


	private:

		  embree::Scene* getScene() {
		    return (embree::Scene*)scene_; }
		  const embree::Scene* getScene() const {
		    return (const embree::Scene*)scene_; }

		embree::Geometry* getGeom(size_t geomID) {
		  return (embree::Geometry*)rtcGetGeometry(scene_,geomID); }
		const embree::Geometry* getGeom(size_t geomID) const {
		  return (const embree::Geometry*)rtcGetGeometry(scene_,geomID); }




	public:



		Eigen::AlignedBox4d worldBB() const
		{
			RTCBounds bounds;
			rtcGetSceneBounds(scene_,&bounds);
			return { Eigen::Vector4d( bounds.lower_x, bounds.lower_y, bounds.lower_z, 0.0),
				Eigen::Vector4d( bounds.upper_x, bounds.upper_y, bounds.upper_z, 0.0) };

		}

		void addShape(Shapes::Shape* const s)
		{
			MYASSERT(s->rtcCompatible(),
					std::string("tried to add non-rtc-compatible shape \"")
			+ s->name() + std::string("\" to bvh") );

			uint64_t geomID = Shapes::addToRTCScene(s,scene_,device_);
			MYASSERT(geomID != RTC_INVALID_GEOMETRY_ID,std::string("failed to add shape ") +
			         s->name() + std::string(" with ") + std::to_string(s->nElements()) +
			         std::string(" elements to rtc scene!"));

			s->setGeomID(geomID);
		}

		void commitShapeChange(const Shapes::Shape* const s) {
		  MYASSERT(s->geomID() != RTC_INVALID_GEOMETRY_ID,
		           "tried to commit change to shape without valid geomID in bvh ");
		  Shapes::commitToRTCScene(s,scene_);

		}


		void intersect(RayTracing::IntersectContext* context, RTCRayHit* rayhit) const {
			rtcIntersect1(scene_,(RTCIntersectContext*)context,rayhit); }

		bool test_AABB(const Eigen::AlignedBox4d& aabb) const;

		void disable_geom(const uint64_t geomID) {
		  rtcDisableGeometry(rtcGetGeometry(scene_,geomID)); }

		void enable_geom(const uint64_t geomID) {
		  rtcEnableGeometry(rtcGetGeometry(scene_,geomID)); }

		bool geom_enabled_tf(const uint64_t geomID) const;

		bool modified_tf() const;

		void print() const;

		// build the bvh
		void commit();


	public:

		BVH();

		~BVH();


	};

} /* namespace BVH */

#endif /* SRC_BVH_BVH_H_ */
