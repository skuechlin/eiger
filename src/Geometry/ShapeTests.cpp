/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * ShapeTests.cpp
 *
 *  Created on: Oct 23, 2015
 *      Author: kustepha
 */

#include "Primitives/MyError.h"

#include "ShapeTests.h"

#include <iostream>

// return in matrix V the intersection points (if any)
// of plane given by N and dist with box bb sorted s.t.
// they form a convex polygon
// return number of points found, or uint64_t(-1) if plane is co-planar to box face
uint64_t
Shapes::intersect_AABB_plane(
    const Eigen::AlignedBox4d& bb,
    const Eigen::Vector4d& N,
    const double dist,
    std::list<Eigen::Vector4d,Eigen::aligned_allocator<Eigen::Vector4d>>& V)
{

  uint64_t npts = 0; // number of intersections found

  // first check plane intersection
  if ( !(Shapes::test_AABB_plane(bb,N,dist) ) )
    return npts; // no intersection

  // treat case of plane co-planar to one of the box faces
  for (uint8_t i = 0; i < 3; ++i)
    if (fabs(N.coeffRef(0)) == 1. && ( dist == bb.min().coeffRef(0) || dist == bb.max().coeffRef(i)))
      return -1;

  // bb penetrates plane, find intersection points (max 6) by checking the 12 edges

  Eigen::Vector4d A;
  Eigen::Vector4d B;


  double t; // intersection parameter (<0 for no intersection)


  for (uint64_t i = 0; i < 12; ++i)
    {
      AABB_edge(bb,i,A,B);

      if ( ( t = Shapes::intersect_segment_plane(A, B, N, dist) ) > 0.0 )
        V.emplace_back( Eigen::Vector4d(A + t*(B-A)) );

      MYASSERT(V.size()<7,"error in plane--bounding-box intersection calculation: more than 6 plane-edge intersection detected");


    }

  npts = V.size();

  MYASSERT(npts>0,"error in plane--bounding-box intersection calculation: plane intersection detected, but no plane-edge intersection found");

  if (npts == 2)
    {

      std::cout << bb.min().transpose(); std::cout << "\n";
      std::cout << bb.max().transpose(); std::cout << "\n";
      std::cout << N.transpose(); std::cout << "\n";
      std::cout << dist << std::endl;
      for (auto& v : V)
        {
          std::cout << v.transpose(); std::cout << "\n";
        }

      MYASSERT(npts!=2,"error in plane--bounding-box intersection calculation: exactly 2 plane-edge intersection found, which is not possible");

    }
  if (npts == 1)
    {
      return npts;
    }


  // sort intersections counterclockwise w.r.t first point using plane normal
  A = *(V.begin());
  V.sort(
      [&A,&N](const Eigen::Vector4d& lhs, const Eigen::Vector4d& rhs)
      {
    //	the expression comp(a,b), where comp is an object of this type and a and b
    //	are key values, shall return true if a is considered to go before b in the
    //	strict weak ordering the function defines.
    return N.dot( (lhs-A).cross3(rhs-A) ) > 0;
      });


  return npts;


}



// Test if AABB given by center Cnt and extents Ext intersects
// quadrilateral given by vertices A,B,C,D, normal N and distance dist
bool
Shapes::test_AABB_quadrilateral(
    const Eigen::Vector4d& Cnt,
    const Eigen::Vector4d& e,
    const Eigen::Vector4d& A,
    const Eigen::Vector4d& B,
    const Eigen::Vector4d& C,
    const Eigen::Vector4d& D,
    const Eigen::Vector4d& N,
    const double dist)
{

  // based on separating axis test

  // following the triangle test in:
  // Christer Ericson: Real-Time Collision Detection, (c) Elsevier 2005
  // pp. 171

  double p0, p1, p2, p3, r;


  // Translate quadrilateral as conceptually moving AABB to origin
  const Eigen::Vector4d v0 = A - Cnt;
  const Eigen::Vector4d v1 = B - Cnt;
  const Eigen::Vector4d v2 = C - Cnt;
  const Eigen::Vector4d v3 = D - Cnt;

  // Compute edge vectors for quadrilateral
  const Eigen::Vector4d f0 = v1 - v0;
  const Eigen::Vector4d f1 = v2 - v1;
  const Eigen::Vector4d f2 = v3 - v2;
  const Eigen::Vector4d f3 = v0 - v3;



  // Test axes a00..a23 (category 3)

  // generated from Maple code

  // test axis a00, p0==p1

  p1 = f0[1] * v1[2] - f0[2] * v1[1];
  p2 = f0[1] * v2[2] - f0[2] * v2[1];
  p3 = f0[1] * v3[2] - f0[2] * v3[1];
  r = fabs(f0[2]) * e[1] + fabs(f0[1]) * e[2];

  if ( std::max( -std::max({p1,p2,p3}), std::min({p1,p2,p3}) ) > r)
    return 0; // Axis is a separating axis


  // test axis a01, p1==p2

  p0 = f1[1] * v0[2] - f1[2] * v0[1];
  p2 = f1[1] * v2[2] - f1[2] * v2[1];
  p3 = f1[1] * v3[2] - f1[2] * v3[1];
  r = fabs(f1[2]) * e[1] + fabs(f1[1]) * e[2];

  if ( std::max( -std::max({p2,p3,p0}), std::min({p2,p3,p0}) ) > r)
    return 0; // Axis is a separating axis


  // test axis a02, p2==p3

  p0 = f2[1] * v0[2] - f2[2] * v0[1];
  p1 = f2[1] * v1[2] - f2[2] * v1[1];
  p3 = f2[1] * v3[2] - f2[2] * v3[1];
  r = fabs(f2[2]) * e[1] + fabs(f2[1]) * e[2];

  if ( std::max( -std::max({p3,p0,p1}), std::min({p3,p0,p1}) ) > r)
    return 0; // Axis is a separating axis


  // test axis a03, p3==p0

  p0 = f3[1] * v0[2] - f3[2] * v0[1];
  p1 = f3[1] * v1[2] - f3[2] * v1[1];
  p2 = f3[1] * v2[2] - f3[2] * v2[1];
  r = fabs(f3[2]) * e[1] + fabs(f3[1]) * e[2];

  if ( std::max( -std::max({p0,p1,p2}), std::min({p0,p1,p2}) ) > r)
    return 0; // Axis is a separating axis


  // test axis a10, p0==p1

  p1 = -f0[0] * v1[2] + f0[2] * v1[0];
  p2 = -f0[0] * v2[2] + f0[2] * v2[0];
  p3 = -f0[0] * v3[2] + f0[2] * v3[0];
  r = fabs(f0[2]) * e[0] + fabs(f0[0]) * e[2];

  if ( std::max( -std::max({p1,p2,p3}), std::min({p1,p2,p3}) ) > r)
    return 0; // Axis is a separating axis


  // test axis a11, p1==p2

  p0 = -f1[0] * v0[2] + f1[2] * v0[0];
  p2 = -f1[0] * v2[2] + f1[2] * v2[0];
  p3 = -f1[0] * v3[2] + f1[2] * v3[0];
  r = fabs(f1[2]) * e[0] + fabs(f1[0]) * e[2];

  if ( std::max( -std::max({p2,p3,p0}), std::min({p2,p3,p0}) ) > r)
    return 0; // Axis is a separating axis


  // test axis a12, p2==p3

  p0 = -f2[0] * v0[2] + f2[2] * v0[0];
  p1 = -f2[0] * v1[2] + f2[2] * v1[0];
  p3 = -f2[0] * v3[2] + f2[2] * v3[0];
  r = fabs(f2[2]) * e[0] + fabs(f2[0]) * e[2];

  if ( std::max( -std::max({p3,p0,p1}), std::min({p3,p0,p1}) ) > r)
    return 0; // Axis is a separating axis


  // test axis a13, p3==p0

  p0 = -f3[0] * v0[2] + f3[2] * v0[0];
  p1 = -f3[0] * v1[2] + f3[2] * v1[0];
  p2 = -f3[0] * v2[2] + f3[2] * v2[0];
  r = fabs(f3[2]) * e[0] + fabs(f3[0]) * e[2];

  if ( std::max( -std::max({p0,p1,p2}), std::min({p0,p1,p2}) ) > r)
    return 0; // Axis is a separating axis


  // test axis a20, p0==p1

  p1 = f0[0] * v1[1] - f0[1] * v1[0];
  p2 = f0[0] * v2[1] - f0[1] * v2[0];
  p3 = f0[0] * v3[1] - f0[1] * v3[0];
  r = fabs(f0[1]) * e[0] + fabs(f0[0]) * e[1];

  if ( std::max( -std::max({p1,p2,p3}), std::min({p1,p2,p3}) ) > r)
    return 0; // Axis is a separating axis


  // test axis a21, p1==p2

  p0 = f1[0] * v0[1] - f1[1] * v0[0];
  p2 = f1[0] * v2[1] - f1[1] * v2[0];
  p3 = f1[0] * v3[1] - f1[1] * v3[0];
  r = fabs(f1[1]) * e[0] + fabs(f1[0]) * e[1];

  if ( std::max( -std::max({p2,p3,p0}), std::min({p2,p3,p0}) ) > r)
    return 0; // Axis is a separating axis


  // test axis a22, p2==p3

  p0 = f2[0] * v0[1] - f2[1] * v0[0];
  p1 = f2[0] * v1[1] - f2[1] * v1[0];
  p3 = f2[0] * v3[1] - f2[1] * v3[0];
  r = fabs(f2[1]) * e[0] + fabs(f2[0]) * e[1];

  if ( std::max( -std::max({p3,p0,p1}), std::min({p3,p0,p1}) ) > r)
    return 0; // Axis is a separating axis


  // test axis a23, p3==p0

  p0 = f3[0] * v0[1] - f3[1] * v0[0];
  p1 = f3[0] * v1[1] - f3[1] * v1[0];
  p2 = f3[0] * v2[1] - f3[1] * v2[0];
  r = fabs(f3[1]) * e[0] + fabs(f3[0]) * e[1];

  if ( std::max( -std::max({p0,p1,p2}), std::min({p0,p1,p2}) ) > r)
    return 0; // Axis is a separating axis





  // Test the three axes corresponding to the face normals of AABB bb (category 1)

  // Exit if...
  // .. [-e0,e0] and [min(v0.x,v1.x,v2.x,v3.x), max(v0.x,v1.x,v2.x,v3.x)] do not overlap
  if (std::max({v0.x(),v1.x(),v2.x(),v3.x()}) < -e.x() || std::min({v0.x(),v1.x(),v2.x(),v3.x()}) > e.x())
    return 0;
  // .. [-e1,e1] and [min(v0.y,v1.y,v2.y,v3.y), max(v0.y,v1.y,v2.y,v3.y)] do not overlap
  if (std::max({v0.y(),v1.y(),v2.y(),v3.y()}) < -e.y() || std::min({v0.y(),v1.y(),v2.y(),v3.y()}) > e.y())
    return 0;
  // .. [-e2,e2] and [min(v0.z,v1.z,v2.z,v3.z), max(v0.z,v1.z,v2.z,v3.z)] do not overlap
  if (std::max({v0.z(),v1.z(),v2.z(),v3.z()}) < -e.z() || std::min({v0.z(),v1.z(),v2.z(),v3.z()}) > e.z())
    return 0;


  // Test separating axis corresponding to quadrilateral face normal (category 2)
  return Shapes::test_AABB_plane(Cnt,e,N,dist);


}


namespace
{

  // Copyright 2000 softSurfer, 2012 Dan Sunday
  // This code may be freely used and modified for any purpose
  // providing that this copyright notice is included with it.
  // SoftSurfer makes no warranty for this code, and cannot be held
  // liable for any real or imagined damage resulting from its use.
  // Users of this code must verify correctness for their application.


  // a Point is defined by its coordinates {int x, y;}
  //===================================================================

  typedef Eigen::Vector2d Point;


  // isLeft(): tests if a point is Left|On|Right of an infinite line.
  //    Input:  three points P0, P1, and P2
  //    Return: >0 for P2 left of the line through P0 and P1
  //            =0 for P2  on the line
  //            <0 for P2  right of the line
  //    See: Algorithm 1 "Area of Triangles and Polygons"
  inline double
  isLeft( Point P0, Point P1, Point P2 )
  {
    return ( (P1.x() - P0.x()) * (P2.y() - P0.y())
        - (P2.x() -  P0.x()) * (P1.y() - P0.y()) );
  }
  //===================================================================


  // cn_PnPoly(): crossing number test for a point in a polygon
  //      Input:   P = a point,
  //               V[] = vertex points of a polygon V[n+1] with V[n]=V[0]
  //      Return:  0 = outside, 1 = inside
  // This code is patterned after [Franklin, 2000]
//  int
//  cn_PnPoly( const Point P, const Point* const V, int n )
//  {
//    int    cn = 0;    // the  crossing number counter
//
//    // loop through all edges of the polygon
//    for (int i=0; i<n; i++) {    // edge from V[i]  to V[i+1]
//        if (((V[i].y() <= P.y()) && (V[i+1].y() > P.y()))     // an upward crossing
//            || ((V[i].y() > P.y()) && (V[i+1].y() <=  P.y()))) { // a downward crossing
//            // compute  the actual edge-ray intersect x-coordinate
//            double vt = (P.y()  - V[i].y()) / (V[i+1].y() - V[i].y());
//            if (P.x() <  V[i].x() + vt * (V[i+1].x() - V[i].x())) // P.x < intersect
//              ++cn;   // a valid crossing of y=P.y right of P.x
//        }
//    }
//    return (cn&1);    // 0 if even (out), and 1 if  odd (in)
//
//  }
  //===================================================================


  // wn_PnPoly(): winding number test for a point in a polygon
  //      Input:   P = a point,
  //               V[] = vertex points of a polygon V[n+1] with V[n]=V[0]
  //      Return:  wn = the winding number (==0 only when P is outside)
  int
  wn_PnPoly( const Point P, const Point* const V, int n )
  {
    int    wn = 0;    // the  winding number counter

    // loop through all edges of the polygon
    for (int i=0; i<n; i++) {   // edge from V[i] to  V[i+1]
        if (V[i].y() <= P.y()) {          // start y <= P.y
            if (V[i+1].y()  > P.y())      // an upward crossing
              if (isLeft( V[i], V[i+1], P) > 0)  // P left of  edge
                ++wn;            // have  a valid up intersect
        }
        else {                        // start y > P.y (no test needed)
            if (V[i+1].y()  <= P.y())     // a downward crossing
              if (isLeft( V[i], V[i+1], P) < 0)  // P right of  edge
                --wn;            // have  a valid down intersect
        }
    }
    return wn;
  }
  //===================================================================

} // anon namespace


// test if point p is contained in polygon defined by points V[]
//      Input:   P = a point,
//               V[] = vertex points of a polygon V[n+1] with V[n]=V[0]
bool Shapes::test_point_in_polygon2D(const Eigen::Vector2d& P, const Eigen::Vector2d* const V, const uint64_t n)
{
  return wn_PnPoly(P,V,n);
}

