/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * Cylinder.h
 *
 *  Created on: Jun 22, 2015
 *      Author: kustepha
 */

#ifndef GEOMETRY_CYLINDER_H_
#define GEOMETRY_CYLINDER_H_

#include "Constants/Constants.h"

#include "Shape.h"

namespace Settings
{
  class Dictionary;
}

namespace Shapes
{

  class Cylinder : public Shape
  {


  private:

    const Eigen::Vector4d X_; // base point
    const Eigen::Vector4d D_; // direction
    const double r_; // radius
    const double l_; // length
    const double sign_; // -1 when flow is inside, +1 when flow is outside

    uint64_t flags() const override { return RTC_COMPATIBLE | FINITE | PLANAR | SHAPE_TYPE::CYLINDER; }

  private:

    // intersection routine
    template<HIT_ACTION action> void intersect_impl(RayTracing::Ray* const r) const;

  public:

    // INTERFACE

    // return true if shape intersects axis aligned bounding box bb
    bool test_AABB(const Eigen::AlignedBox4d& bb) const override;

    // RTC COMPATIBLE

    void intersect(RayTracing::Ray *const r) const override {
      intersect_impl<HIT_ACTION::INTERSECT>(r); }

    void occluded(RayTracing::Ray *const r) const override {
      intersect_impl<HIT_ACTION::OCCLUDE>(r); }

    // FINITE

    // return the area of the shape
    double area() const override { return 2.*Constants::pi*r_*l_; }

    // return the bounding box of the shape
    Eigen::AlignedBox4d boundingBox() const override;


    // SAMPLEABLE

    // return a random point on the shape
    Eigen::Vector4d sample(MyRandom::Rng& rndGen) const override;

    Eigen::Vector4d unitNormal(const Eigen::Vector4d& R) const override;


    // PLANAR

    double flux(const Eigen::Vector4d& V) const override {
      return sign_*Eigen::Vector4d(V - V.dot(D_)*D_).norm(); }

  public:
    Cylinder (
        const Eigen::Vector4d& X,
        const Eigen::Vector4d& D,
        const double r,
        const double l,
        const double s,
        const uint32_t prim_id = 0);

    Cylinder (
        const Settings::Dictionary& dict,
        const uint32_t prim_id = 0);

    EIGEN_MAKE_ALIGNED_OPERATOR_NEW
  };

} /* namespace Shapes */

#endif /* GEOMETRY_CYLINDER_H_ */
