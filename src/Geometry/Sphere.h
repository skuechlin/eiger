/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * Sphere.h
 *
 *  Created on: Oct 28, 2015
 *      Author: kustepha
 */

#ifndef SRC_GEOMETRY_SPHERE_H_
#define SRC_GEOMETRY_SPHERE_H_

#include <Geometry/Shape.h>
#include <Eigen/Dense>

namespace Shapes {

  class Sphere: public Shape {

    friend class SphereCollection;

    uint64_t flags() const override {
      return RTC_COMPATIBLE | FINITE | SAMPLEABLE | SHAPE_TYPE::SPHERE; }


    const Eigen::Vector4d X_; // center

    const double r_; // radius
    const double rsq_; // radius squared
    const double sign_; // -1 when flow is inside, +1 when flow is outside
    const double pad_ = 0.;

  public:

    // implicit tecplot writer interface

    // number of variables
    static uint nVars();

    // value of variable i
    double  getVar(const uint i)  const;

    // name of variable i
    static std::string getVarName(const uint i);

  private:

    template<HIT_ACTION action> void intersect_impl(RayTracing::Ray *const r) const;

  public:

    // return true if shape intersects axis aligned bounding box bb
    bool test_AABB(const Eigen::AlignedBox4d& bb) const override;

    void intersect(RayTracing::Ray *const r) const override {
      intersect_impl<HIT_ACTION::INTERSECT>(r); }

    void occluded(RayTracing::Ray *const r) const override {
      intersect_impl<HIT_ACTION::OCCLUDE>(r); }

    // return a random point on the sphere
    Eigen::Vector4d sample(MyRandom::Rng& rndGen) const override;

    Eigen::Vector4d unitNormal(const Eigen::Vector4d& R) const override {
      return sign_*(R - X_).normalized(); }

    double area() const override { return 4.*M_PI*r_*r_; }

    Eigen::AlignedBox4d  boundingBox() const override {
      const double rext = r_ + BBEPS;

      return Eigen::AlignedBox4d{
        X_ - Eigen::Vector4d(rext,rext,rext,0.0),
            X_ + Eigen::Vector4d(rext,rext,rext,0.0) };
    }

  public:

    EIGEN_MAKE_ALIGNED_OPERATOR_NEW

    Sphere(
        const Eigen::Vector4d& X,
        const double r,
        const double s,
        const uint32_t prim_id = 0 );

    Sphere(
        const Settings::Dictionary& dict,
        const uint32_t prim_id = 0);

    virtual ~Sphere() = default;
  };

} /* namespace Shapes */

#endif /* SRC_GEOMETRY_SPHERE_H_ */
