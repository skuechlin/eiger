/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * Triangle.h
 *
 *  Created on: Apr 10, 2014
 *      Author: Stephan Kuechlin
 */

#ifndef TRIANGLE_H_
#define TRIANGLE_H_


#include "Shape.h"
#include "ShapeTests.h"
#include "Ray.h"
#include "Primitives/MyError.h"

#include <iostream>

namespace Shapes
{

  class Parallelogram;

  class Triangle: public Shape
  {
    friend class Parallelogram;

    uint64_t flags() const override {
      return RTC_COMPATIBLE | FINITE | PLANAR | SAMPLEABLE | SHAPE_TYPE::TRIANGLE; }

  private:

    // vertices
    Eigen::Vector4d A_;
    Eigen::Vector4d B_;
    Eigen::Vector4d C_;

    // (unit) normal vector
    Eigen::Vector4d N_ = Eigen::Vector4d::Zero();

    // aux. planes
    Eigen::Vector4d N1_;
    Eigen::Vector4d N2_;

    // area
    double area_;

    // offset
    double d_;
    double d1_;
    double d2_;

  private:
    // update vertex change
    void update();


  public:

    // vertices
    Eigen::Vector4d A() const { return A_; }
    Eigen::Vector4d B() const { return B_; }
    Eigen::Vector4d C() const { return C_; }
    // normal vector
    Eigen::Vector4d N() const { return N_; }
    // edges
    Eigen::Vector4d AB() const { return B() - A(); }
    Eigen::Vector4d AC() const { return C() - A(); }
    Eigen::Vector4d BC() const { return C() - B(); }

    // number of vertices
    uint64_t nVerts() const override { return 3; }

  private:

    template<HIT_ACTION action>
    void  intersect_impl(RayTracing::Ray *const r)
    const
    {
      // Jiri Havel and Adam Herout
      // Yet Faster Ray-Triangle Intersection (Using SSE4)
      // IEEE Transactions on Visualization and Computer Graphics 2010


      double t( d_ - r->org().dot(N_) );    // compute distance to shape in direction of normal
      double det( r->dir().dot(N_) );       // velocity in direction of normal

      // check intersection candidate
      // if it lies on line segment (line-plane intersection)
      if ( !test_approx_segment_plane(t,det,r->tfar) ) return;

      // check if it lies inside the triangle
      Eigen::Vector4d P(det*r->org() + t*r->dir());

      double u(P.dot(N1_) + det*d1_);
      //    if ( std::signbit(u) != std::signbit(det - u) ) return;
      if ( !gt(u,det,-ABSEPS) ) return;

      double v(P.dot(N2_) + det*d2_);
      //    if ( std::signbit(v) != std::signbit(det - u - v) ) return;
      if ( !gt(v,det,-ABSEPS) || !lt(v+u,det,1.0+2.*ABSEPS) ) return;

      // we have intersect
      switch (action) {
        case HIT_ACTION::OCCLUDE:
          r->setOccluded();
          break;
        case HIT_ACTION::INTERSECT:
          r->setHit(N_,t,det,geomID(),primID());
          break;
      }

      return;

    }

  public:

    // return true if shape intersects axis aligned bounding box bb
    bool test_AABB(const Eigen::AlignedBox4d& bb) const override;

    void intersect(RayTracing::Ray *const r) const override {
      intersect_impl<HIT_ACTION::INTERSECT>(r); }

    void occluded(RayTracing::Ray *const r) const override {
      intersect_impl<HIT_ACTION::OCCLUDE>(r); }

    Eigen::Vector4d sample(MyRandom::Rng& rndGen) const override;

    Eigen::Vector4d unitNormal(const Eigen::Vector4d&) const override { return N(); }

    double flux(const Eigen::Vector4d& V) const override { return N().dot(V); }

    Eigen::Vector4d centroid() const override {
      return A() + (1./3.)*AB() + (1./3.)*AC(); }

    double area() const override { return area_; }

    Eigen::AlignedBox4d boundingBox() const override;

  public:

    // CONSTRUCTOR
    Triangle(
        const Eigen::Matrix<double,4,3>& vrts,
        const uint32_t prim_id = 0,
        const uint32_t geom_id = -1);

    Triangle(
        const Eigen::Vector4d& A,
        const Eigen::Vector4d& B,
        const Eigen::Vector4d& C,
        const uint32_t prim_id = 0,
        const uint32_t geom_id = -1);

    Triangle(
        const Settings::Dictionary& dict,
        const uint32_t prim_id = 0,
        const uint32_t geom_id = -1);

    virtual ~Triangle() {}

    EIGEN_MAKE_ALIGNED_OPERATOR_NEW
  };


}  // namespace Shapes




#endif /* TRIANGLE_H_ */
