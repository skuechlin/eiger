/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * FEShapeData.cpp
 *
 *  Created on: Jun 11, 2018
 *      Author: kustepha
 */

#include <iomanip>
#include <algorithm>

#include "FEShapeData.h"

#include "FieldData/FieldData.h"
#include "Parallel/MyMPI.h"
#include "Primitives/MySearch.h"



namespace Shapes
{


  void FEShapeData::allgather(const MyMPI::MPIMgr& mpiMgr) {

    //    int MPI_Allgatherv(const void *sendbuf, int sendcount,
    //        MPI_Datatype sendtype, void *recvbuf, const int recvcounts[],
    //        const int displs[], MPI_Datatype recvtype, MPI_Comm comm)


    MPI_Datatype tri_mpi_t;
    MPI_Datatype quad_mpi_t;
    MPI_Type_contiguous(sizeof(Triangle)/sizeof(double),MPI_DOUBLE,&tri_mpi_t);
    MPI_Type_contiguous(sizeof(Quadrilateral)/sizeof(double),MPI_DOUBLE,&quad_mpi_t);
    MPI_Type_commit(&tri_mpi_t);
    MPI_Type_commit(&quad_mpi_t);

    // exchange send counts
    std::vector<int32_t> recvcounts(mpiMgr.nRanks(),0);
    std::vector<int32_t> displs(mpiMgr.nRanks(),0);
    int32_t sendcounts;

    // triangles
    sendcounts = tris_.size(); //,quads_.size(),verts_.size()};
    MPI_Allgather(&sendcounts,1,MPI_INT32_T,recvcounts.data(),1,MPI_INT32_T,mpiMgr.comm());

    // reduce new sizes and receive displacements
    uint64_t tot_num = 0;
    for (uint64_t i = 0; i < mpiMgr.nRanks(); ++i)
      {
        displs[i] = tot_num;
        tot_num += recvcounts[i];
      }

    // exchange triangles
    MyMemory::aligned_array<Triangle> t_recv_buffer(tot_num);
    // avoid nullptr as buffer by reserving 1
    t_recv_buffer.reserve(1);
    tris_.reserve(1);

    MPI_Allgatherv(
        tris_.data(),
        sendcounts,
        tri_mpi_t,
        t_recv_buffer.data(),
        recvcounts.data(),
        displs.data(),
        tri_mpi_t,
        mpiMgr.comm());

    MPI_Type_free(&tri_mpi_t);

    tris_.swap(t_recv_buffer);
    t_recv_buffer.free();


    // quads
    sendcounts = quads_.size();
    MPI_Allgather(&sendcounts,1,MPI_INT32_T,recvcounts.data(),1,MPI_INT32_T,mpiMgr.comm());

    // reduce new sizes and receive displacements
    tot_num = 0;
    for (uint64_t i = 0; i < mpiMgr.nRanks(); ++i)
      {
        displs[i] = tot_num;
        tot_num += recvcounts[i];
      }

    // exchange quads
    MyMemory::aligned_array<Quadrilateral> q_recv_buffer(tot_num);
    // avoid nullptr as buffer by reserving 1
    q_recv_buffer.reserve(1);
    quads_.reserve(1);

    MPI_Allgatherv(
        quads_.data(),
        sendcounts,
        quad_mpi_t,
        q_recv_buffer.data(),
        recvcounts.data(),
        displs.data(),
        quad_mpi_t,
        mpiMgr.comm());

    MPI_Type_free(&quad_mpi_t);

    quads_.swap(q_recv_buffer);
    q_recv_buffer.free();

  }


  FEShapeData::~FEShapeData() = default;

  // must be called after last shape is added
  void
  FEShapeData::finalize()
  {
    //MYASSERT(nElements_ > 0, "tried to finalize empty FEShapeData");

    buildConnectivity();
    buildShapePtrVec();
  }



  void
  FEShapeData::buildShapePtrVec()
  {
    shapes_.resize(nElements());

    // for each element, save pointer in shape_ptrs_ vector
    // and assign primitive id

    // assign new primitive ids
    nextElId = 0;

    // triangles
    for (auto& t : tris_)
      {
        t.setPrimID(nextElId);
        shapes_[nextElId] = &t;
        ++nextElId;
      }

    // quads
    for (auto& q : quads_)
      {
        q.setPrimID(nextElId);
        shapes_[nextElId] = &q;
        ++nextElId;
      }

    MYASSERT(nextElId == nElements(),"not all elements inserted");

  }


  namespace {

    bool vertComp (const Eigen::Vector4d& a, const Eigen::Vector4d& b)
    {

      if (a[2] < b[2]) return true;
      if (a[2] > b[2]) return false;

      if (a[1] < b[1]) return true;
      if (a[1] > b[1]) return false;

      if (a[0] < b[0]) return true;
      return false;
    }

    bool vertPred (const Eigen::Vector4d& a, const Eigen::Vector4d& b)
    {
      return (a[0] == b[0]) && (a[1] == b[1]) && (a[2] == b[2]);
    }
  }


  void
  FEShapeData::buildConnectivity()
  {

//    nextVertId = 0;

    // build vertex list and connectivity
    verts_.reserve(tris_.size()*3 + quads_.size()*4);
    verts_.resize(0);
    connectivity_.reserve(tris_.size()*4 + quads_.size()*4);
    connectivity_.resize(0);

    for (const auto& t : tris_)
      {
        verts_.emplace_back(t.A());
        verts_.emplace_back(t.B());
        verts_.emplace_back(t.C());
        //        connectivity_.emplace_back(nextVertId++);
        //        connectivity_.emplace_back(nextVertId++);
        //        connectivity_.emplace_back(nextVertId); // duplicate last vertex
        //        connectivity_.emplace_back(nextVertId++);
      }

    for (const auto& q : quads_)
      {
        verts_.emplace_back(q.A());
        verts_.emplace_back(q.B());
        verts_.emplace_back(q.C());
        verts_.emplace_back(q.D());
        //        connectivity_.emplace_back(nextVertId++);
        //        connectivity_.emplace_back(nextVertId++);
        //        connectivity_.emplace_back(nextVertId++);
        //        connectivity_.emplace_back(nextVertId++);
      }

    //    // create permutation vector
    //    std::vector<uint64_t> perm(nVerts());
    //    for (uint64_t i = 0; i < nVerts(); ++i)
    //      perm[i] = i;
    //
    //    // sort permutation vector by vertices
    //    std::sort(
    //        perm.begin(),
    //        perm.end(),
    //        [this](const uint64_t a, const uint64_t b)->bool{
    //      return vertComp(*vert(a),*vert(b));
    //    });
    //
    //    // invert permutation to give ranks, eliminate duplicates
    //    std::vector<uint64_t> rank(perm.size());
    //    uint64_t i = 0;
    //    uint64_t j = 0;
    //    while(true)
    //      {
    //        rank[perm[i++]] = j;
    //        if (i>=perm.size()) break;
    //        if ( !vertPred( *vert( perm[i] ), *vert( perm[i-1] ) ) ) ++j;
    //      }
    //
    //
    //    // permute connectivity
    //    for (uint64_t i = 0; i < connectivity_.size(); ++i)
    //      connectivity_[i] = rank[ connectivity_[i] ];

    // now actually sort the vertices
    std::sort(verts_.begin(),verts_.end(),&vertComp);
    // ... and make them unique
    auto end_unique = std::unique(verts_.begin(),verts_.end(),&vertPred);
    verts_.conservative_resize(std::distance(verts_.begin(),end_unique));


    // build connectivity by searching vertices in unique range

    for (const auto& t : tris_)
      {
        const uint32_t vindA = MySearch::binary_search(
            verts_.begin(),
            verts_.end(),
            t.A(),
            &vertComp);
        const uint32_t vindB = MySearch::binary_search(
            verts_.begin(),
            verts_.end(),
            t.B(),
            &vertComp);
        const uint32_t vindC = MySearch::binary_search(
            verts_.begin(),
            verts_.end(),
            t.C(),
            &vertComp);
        connectivity_.emplace_back( vindA );
        connectivity_.emplace_back( vindB );
        connectivity_.emplace_back( vindC );
        connectivity_.emplace_back( vindC ); // duplicate last vert for tris

      }

    for (const auto& q : quads_)
      {
        const uint32_t vindA = MySearch::binary_search(
            verts_.begin(),
            verts_.end(),
            q.A(),
            &vertComp);
        const uint32_t vindB = MySearch::binary_search(
            verts_.begin(),
            verts_.end(),
            q.B(),
            &vertComp);
        const uint32_t vindC = MySearch::binary_search(
            verts_.begin(),
            verts_.end(),
            q.C(),
            &vertComp);
        const uint32_t vindD = MySearch::binary_search(
            verts_.begin(),
            verts_.end(),
            q.D(),
            &vertComp);
        connectivity_.emplace_back( vindA );
        connectivity_.emplace_back( vindB );
        connectivity_.emplace_back( vindC );
        connectivity_.emplace_back( vindD );
      }

  }


  void
  FEShapeData::addTri(
      const Eigen::Vector4d& A,
      const Eigen::Vector4d& B,
      const Eigen::Vector4d& C)
  {

    if (world_bb_.contains(A) || world_bb_.contains(B) || world_bb_.contains(C))
      { // any node in world
        tris_.emplace_back(A,B,C,nextElId++);
      }

  }

  void
  FEShapeData::addQuad(
      const Eigen::Vector4d& A,
      const Eigen::Vector4d& B,
      const Eigen::Vector4d& C,
      const Eigen::Vector4d& D)
  {
    if (world_bb_.contains(A) || world_bb_.contains(B) || world_bb_.contains(C) || world_bb_.contains(D))
      { // any node in world
        quads_.emplace_back(A,B,C,D,nextElId++);
      }

  }

  FEShapeData::FEShapeData(
      std::unique_ptr<FieldData> fdat,
      const Eigen::AlignedBox4d& world_bb,
      const bool flip_normals)
  : FEShapeData(world_bb)
  {


    const uint64_t nVpE = fdat->nVertsPerElement();
    MYASSERT( nVpE == 3 || nVpE == 4,
              "field data passed to FEShapeData ctor has number of verts per element not equal 3 or 4");

    auto& conn = fdat->getConnectivity();
    const uint64_t nE = conn.size() / nVpE;

    const double* X = &(fdat->getVarData("X")[0]);
    const double* Y = &(fdat->getVarData("Y")[0]);
    const double* Z = &(fdat->getVarData("Z")[0]);

    int32_t* cptr = &conn[0];

    if (nVpE == 3)
      {

        reserveTris(nE);

        for (uint64_t i = 0; i < nE; ++i)
          {
            const Eigen::Vector4d A = { X[*cptr], Y[*cptr], Z[*cptr], 0.0 };
            ++cptr;
            const Eigen::Vector4d B = { X[*cptr], Y[*cptr], Z[*cptr], 0.0 };
            ++cptr;
            const Eigen::Vector4d C = { X[*cptr], Y[*cptr], Z[*cptr], 0.0 };
            ++cptr;

            if (flip_normals)
              addTri(C,B,A);
            else
              addTri(A,B,C);

          }
      }
    else
      {

        for (uint64_t i = 0; i < nE; ++i)
          {
            const Eigen::Vector4d A = { X[*cptr], Y[*cptr], Z[*cptr], 0.0 };
            ++cptr;
            const Eigen::Vector4d B = { X[*cptr], Y[*cptr], Z[*cptr], 0.0 };
            ++cptr;
            const Eigen::Vector4d C = { X[*cptr], Y[*cptr], Z[*cptr], 0.0 };
            ++cptr;
            const Eigen::Vector4d D = { X[*cptr], Y[*cptr], Z[*cptr], 0.0 };
            ++cptr;

            if (*(cptr - 2) == *(cptr - 1))
              {
                if (flip_normals)
                  addTri(C,B,A);
                else
                  addTri(A,B,C);
              }
            else
              {
                if (flip_normals)
                  addQuad(D,C,B,A);
                else
                  addQuad(A,B,C,D);
              }

          }

      }

    finalize();

  }


} // namespace Shapes
