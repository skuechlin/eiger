/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * FEShapeData.h
 *
 *  Created on: Jun 11, 2018
 *      Author: kustepha
 */

#ifndef SRC_GEOMETRY_FESHAPEDATA_H_
#define SRC_GEOMETRY_FESHAPEDATA_H_

#include <memory>

#include "Primitives/MyMemory.h"

#include <Eigen/Dense>

#include "Triangle.h"
#include "Quadrilateral.h"

namespace MyMPI { class MPIMgr; }

class FieldData;

namespace Shapes
{
  class FEShapeData
  {

  public:
    enum { vrtsPerElem = 4 };

    typedef Eigen::Vector4d         VertT;
    typedef Shapes::Shape*          ShapePtrT;

    // list of all "Triangle" elements
    MyMemory::aligned_array<Triangle> tris_;

    // list of all "Quadrilateral" elements
    MyMemory::aligned_array<Quadrilateral> quads_;

    // vector of pointers to individual shapes
    MyMemory::aligned_array<Shape*> shapes_;

    // all shape vertices
    MyMemory::aligned_array<VertT> verts_;

    // maps shape vertices to the unique vertices stored in "verts_"
    MyMemory::aligned_array<int32_t>  connectivity_;

//    int32_t  nextVertId = 0;
    uint32_t nextElId = 0;

    Eigen::AlignedBox4d world_bb_ = {
        Eigen::Vector4d::Constant(-std::numeric_limits<double>::max()),
        Eigen::Vector4d::Constant(std::numeric_limits<double>::max()) };


    // build connectivity
    void buildConnectivity();

    // build shape ptr vec
    void buildShapePtrVec();


  public:

    void reserveTris(const uint64_t n) {
      tris_.conservative_reserve(n);
    }

    void reserveQuads(const uint64_t n) {
      quads_.conservative_reserve(n);
    }

    void allgather(const MyMPI::MPIMgr& mpiMgr);

    void merge(FEShapeData* const other) {
      if (!other) return;

      if (other->tris_.size() > tris_.size())
          tris_.swap(other->tris_);
      if (other->quads_.size() > quads_.size())
        quads_.swap(other->quads_);

      tris_.concatenate(other->tris_);
      quads_.concatenate(other->quads_);
    }

    // must be called after last shape is added
    void finalize();

    static auto& required_variables()  {
      static std::vector<std::string> vnames{"X","Y","Z"};
      return vnames;  }

    uint64_t nElements() const { return tris_.size() + quads_.size(); }

    uint64_t nEdges() const { return tris_.size()*3 + quads_.size()*4; }

    uint64_t nVerts() const { return verts_.size(); }

    const VertT* vert(const uint64_t i) const {
      return &(verts_[i]);  }

    int32_t
    connectivity(const int32_t shape_idx, const int32_t vrt_idx) const {
      return connectivity_[uint64_t(shape_idx)*(FEShapeData::vrtsPerElem) + vrt_idx];
    }


    void
    addTri(
        const Eigen::Vector4d& A,
        const Eigen::Vector4d& B,
        const Eigen::Vector4d& C);

    void
    addQuad(
        const Eigen::Vector4d& A,
        const Eigen::Vector4d& B,
        const Eigen::Vector4d& C,
        const Eigen::Vector4d& D);

    FEShapeData(){};

    FEShapeData(const Eigen::AlignedBox4d& world_bb)
    : world_bb_(world_bb){};

    FEShapeData(std::unique_ptr<FieldData> fdat,
                const Eigen::AlignedBox4d& world_bb,
                const bool flip_normals = false);

    ~FEShapeData();

    EIGEN_MAKE_ALIGNED_OPERATOR_NEW


  };
}


#endif /* SRC_GEOMETRY_FESHAPEDATA_H_ */
