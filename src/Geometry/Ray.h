/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * Intersect.h
 *
 *  Created on: Apr 28, 2014
 *      Author: Stephan Kuechlin
 */

#ifndef INTERSECT_H_
#define INTERSECT_H_

#include <limits>

#include <embree3/rtcore_ray.h>

#include "Primitives/MyIntrin.h"

namespace RayTracing
{


  struct alignas(32) Ray {

    enum {
      FRONT_FLAG = 1<<0,
      BACK_FLAG = 1<<1,
      OCCLUDED_FLAG = 1<<2
    };

    // trajectory
    v4df o;
    v4df d;
    double tfar;

    // hit
    double thit;

    uint32_t geomID; // geometry ID
    uint32_t primID; // primitive ID

    uint32_t flags;
    uint32_t pad;

    v4df N;

    // accessors
    Eigen::Vector4d org() const { return to_eigen(o); }
    Eigen::Vector4d dir() const { return to_eigen(d); }

    bool hit_tf() const { return geomID != RTC_INVALID_GEOMETRY_ID; }
    bool front_hit_tf() const { return flags == FRONT_FLAG; }
    bool back_hit_tf() const { return flags == BACK_FLAG; }
    bool occluded_tf() const { return flags == OCCLUDED_FLAG; }

    // events

    // occlusion
    void setOccluded() { flags = OCCLUDED_FLAG; };

    // hit
    void setHit(const Eigen::Vector4d& _N,
                double t,
                uint32_t _geomID, uint32_t _primID,
                uint32_t front_tf)
    {
      tfar = fmax(t,0.0);
      thit = fmin(thit,tfar);
      geomID = _geomID;
      primID = _primID;
      flags = (front_tf ? FRONT_FLAG : BACK_FLAG);
      N = from_eigen(_N);
    }

    // hit
    void setHit(const Eigen::Vector4d& N,
                double t, double det,
                uint32_t _geomID, uint32_t _primID) {
      setHit(N,t/det,_geomID,_primID,std::signbit(det)); }


  };

  // augments rtc intersect context with double precision ray data
  struct IntersectContext {
    enum RTCIntersectContextFlags flags;     // intersection flags
    RTCFilterFunctionN filter;               // filter function to execute
    unsigned int instID[RTC_MAX_INSTANCE_LEVEL_COUNT]; // will be set to geomID of instance when instance is entered

    Ray ray;
    uint32_t filter_geomID = -1;
    uint32_t filter_primID = -1;
  };

  inline __attribute__((always_inline)) void setFilter(
      IntersectContext* context) {

    context->filter_geomID = context->ray.geomID;
    context->filter_primID = context->ray.primID;
  }

  inline __attribute__((always_inline)) void initRay(
      Ray* ray, const v4df& o, const v4df& d, const double dt) {
    ray->o = o;
    ray->d = d;
    ray->tfar = dt;

    ray->thit = dt;
    ray->geomID = RTC_INVALID_GEOMETRY_ID;
    ray->primID = RTC_INVALID_GEOMETRY_ID;
    ray->flags = 0;
    ray->pad = 0;

    ray->N = v4df{0.,0.,0.,0.};
  }

  inline __attribute__((always_inline)) void initRay(
      Ray* ray, const Eigen::Vector4d& o, const Eigen::Vector4d& d, const double dt) {
    initRay(ray, from_eigen(o), from_eigen(d), dt); }

  inline __attribute__((always_inline)) void initRayTrace(
      IntersectContext* context,
      RTCRayHit* rtcrh,
      const v4df& o, const v4df& d, const double dt)
  {
    // context
    rtcInitIntersectContext((RTCIntersectContext*)context);

    initRay(&(context->ray),o,d,dt);

    // ray hit
    v4sf* rof = reinterpret_cast<v4sf*>( &(rtcrh->ray.org_x) );
    *rof = _mm256_cvtpd_ps(o);

    v4sf* rdf = reinterpret_cast<v4sf*>( &(rtcrh->ray.dir_x) );
    *rdf = _mm256_cvtpd_ps(d);

    rtcrh->ray.tfar = static_cast<float>(dt*1.01);
    rtcrh->ray.mask = -1;
    rtcrh->ray.id = 0;
    rtcrh->ray.flags = 0;

    rtcrh->hit.geomID = RTC_INVALID_GEOMETRY_ID;
    rtcrh->hit.instID[0] = RTC_INVALID_GEOMETRY_ID;


  }


  inline __attribute__((always_inline)) void initRayTrace(
      IntersectContext* context,
      RTCRayHit* rtcrh,
      const Eigen::Vector4d& o, const Eigen::Vector4d& d, const double dt )  {
    initRayTrace(context, rtcrh, from_eigen(o), from_eigen(d), dt  );  }

  inline __attribute__((always_inline)) void setOccluded(RTCRay* r) {
    r->tfar = -std::numeric_limits<float>::infinity(); }

  inline __attribute__((always_inline)) void setHit(RTCRayHit* rtcrh, const Ray* r) {
    const v4sf Nf = _mm256_cvtpd_ps(r->N);
    rtcrh->ray.tfar = static_cast<float>(r->tfar);
    rtcrh->hit.Ng_x = Nf[0];
    rtcrh->hit.Ng_y = Nf[1];
    rtcrh->hit.Ng_z = Nf[2];
    rtcrh->hit.geomID = r->geomID;
    rtcrh->hit.primID = r->primID;
  }

}

#endif /* INTERSECT_H_ */
