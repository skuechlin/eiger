/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * FECollection.h
 *
 *  Created on: Apr 22, 2014
 *      Author: Stephan Kuechlin
 */

#ifndef FECOLLECTION_H_
#define FECOLLECTION_H_

#include <memory>

#include "Shape.h"

#include "FEShapeData.h"

namespace MyMPI { class MPIMgr; }



namespace Shapes
{
  class FECollection: public Shape
  {


  private:

    std::unique_ptr<FEShapeData> shapeDat_ = nullptr;


    Eigen::AlignedBox4d bb_;

    double area_ = 0.;

    uint64_t flags() const override {
      return RTC_COMPATIBLE | FINITE | WRITABLE | SHAPE_TYPE::FE_COLLECTION; }

  private:

    // return the total area of all shapes
    double
    compute_area()
    const;

    // return the bounding box of the entire shape data
    Eigen::AlignedBox4d
    compute_boundingBox()
    const;


  public:

    void setGeomID(const uint32_t id) override {
      Shape::setGeomID(id);
      for (uint64_t i = 0; i < nElements(); ++i)
        element(i)->setGeomID(id);
    }

    // number of vertices
    uint64_t nVerts() const override {
      if (shapeDat_) return shapeDat_->nVerts();
      else return 0; }

    // return pointer to shape element idx,
    // or shape itself if it only has one (itself)
    Shape* element(const uint64_t idx) override { return shapeDat_->shapes_[idx]; }

    // return const pointer to shape element idx,
    // or shape itself if it only has one (itself)
    const Shape* element(const uint64_t idx) const override { return shapeDat_->shapes_[idx]; }

    void writeToTecplot(
        const std::string& boundaryName,
        const std::string& dir,
        const double solTime,
        const uint64_t tn,
        const MyMPI::MPIMgr& mpiMgr ) const override;

    Tecplot::ZoneType tecplotZoneType() const override;

    // return the area of the shape
    double area() const override { return area_; }

    // return the bounding box of the shape
    Eigen::AlignedBox4d boundingBox() const override { return bb_;  }

    void clear();

    void update(std::unique_ptr<FEShapeData> shapeDat);

    FECollection(std::unique_ptr<FEShapeData> shapeDat);

    FECollection();

    ~FECollection();

    EIGEN_MAKE_ALIGNED_OPERATOR_NEW
  };


}  // namespace Shapes



#endif /* FECOLLECTION_H_ */
