/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * ShapeFactory.cpp
 *
 *  Created on: Jun 24, 2015
 *      Author: kustepha
 */

#include <map>
#include <memory>

#include "ShapeFactory.h"

#include "Primitives/MyString.h"
#include "Primitives/MyError.h"

#include "Settings/Dictionary.h"
#include "Settings/FileDict.h"

#include "SimulationBox/SimulationBox.h"

#include "IO/StlReader.h"
#include "IO/NastranReader.h"
#include "IO/Tecplot/Tecplot.h"

#include "FieldData/FieldData.h"

#include "Parallelogram.h"
#include "FECollection.h"
#include "Cylinder.h"
#include "Disk.h"
#include "Sphere.h"
#include "SphereCollection.h"



namespace
{


  std::map<std::string,SHAPE_TYPE>
  makeShapeNameTypeMap()
  {
    std::map<std::string,SHAPE_TYPE> theMap;
    for (uint i = 0; i < SHAPE_TYPE::N_SHAPE_TYPES; ++i)
      theMap.insert( {std::string(SHAPE_NAME[i]),(SHAPE_TYPE)i} );
    return theMap;
  }

} // namespace


namespace Shapes
{

  std::unique_ptr<Shape>
  makeShape(
      const SimulationBox& box,
      const Settings::Dictionary& dict,
      const std::string& inputDirectory )
      {


    const std::map<std::string,SHAPE_TYPE> shapeNameTypeMap = makeShapeNameTypeMap();

    std::unique_ptr<Shape> p;


    std::string shape_name = dict.get<std::string>("type");
    MyString::toLower(shape_name);

    MYASSERT(shapeNameTypeMap.find(shape_name) != shapeNameTypeMap.end(),
             std::string("unsupported shape type \"") + shape_name + std::string("\""));

    // for shapes specified in a file

    Settings::FileDict file_spec;

    if (dict.hasMember("file"))
      file_spec = Settings::FileDict(dict.get<Settings::Dictionary>("file"),inputDirectory);

    auto styit = shapeNameTypeMap.find(shape_name);
    MYASSERT(styit != shapeNameTypeMap.end(),
             std::string("unsupported shape type \"") + shape_name + std::string("\""));

    switch (styit->second)
    {
      case SHAPE_TYPE::TRIANGLE:
        {
          p = std::unique_ptr<Shape>( new class Triangle(dict) );
        }
        break;

      case SHAPE_TYPE::PARALLELOGRAM:
        {
          if (dict.hasMember("box_face"))
            p = std::unique_ptr<Shape>( box.face(dict.get<std::string>("box_face")) );
          else if (dict.hasMember("box face"))
            p = std::unique_ptr<Shape>( box.face(dict.get<std::string>("box face")) );
          else
            p = std::unique_ptr<Shape>( new class Parallelogram(dict) );
        }
        break;

      case SHAPE_TYPE::FE_COLLECTION:
        {


          if (file_spec.isEmpty)
            p = std::unique_ptr<Shape>( new class FECollection() );
          else
            {

              //        MYASSERT(!(file_spec.isEmpty),"fe_collection shape dictionary missing sub-dictionary \"file\"!");

              Eigen::AlignedBox4d bb;
              if (file_spec.remove_outside_)
                bb = Shapes::relaxBox( Eigen::AlignedBox4d(box.min(), box.max()) );
              else
                bb = Eigen::AlignedBox4d(
                    Eigen::Vector4d::Constant(-std::numeric_limits<double>::max()),
                    Eigen::Vector4d::Constant(std::numeric_limits<double>::max()) );

              switch (file_spec.file_format_type) {
                case Settings::FILEFORMAT_TYPE::STL:
                  {
                    p = std::unique_ptr<Shape>
                    (
                        new class FECollection
                        (
                            StlReader::read
                            (
                                file_spec.full_file_name,
                                file_spec.tolerance,
                                file_spec.transform,
                                file_spec.flip_normals_,
                                bb
                            )
                        )
                    );
                  }
                  break;
                case Settings::FILEFORMAT_TYPE::NASTRAN:
                  {
                    p = std::unique_ptr<Shape>
                    (
                        new class FECollection
                        (
                            NastranReader::read
                            (
                                file_spec.full_file_name,
                                file_spec.tolerance,
                                file_spec.transform,
                                file_spec.flip_normals_,
                                bb
                            )
                        )
                    );
                  }
                  break;
                case Settings::FILEFORMAT_TYPE::TECPLOT:
                  {
                    p = std::unique_ptr<Shape>
                    (
                        new class FECollection
                        (
                            std::unique_ptr<FEShapeData>(
                                new class FEShapeData(
                                    Tecplot::readTecplot1DData
                                    (
                                        file_spec.full_file_name,
                                        FEShapeData::required_variables(),
                                        {},
                                        true,
                                        MPI_COMM_WORLD
                                    ),
                                    bb,
                                    file_spec.flip_normals_
                                )
                            )
                        )
                    );

                  }
                  break;
                default:
                  MYASSERT(false,
                           std::string("file format for fe_collection shape must be either \"stl\", \"nastran\", or \"tecplot\", got \"")
                  + file_spec.file_format_name + std::string("\" instead!"));
                  break;
              } // switch (file_spec.file_format_type)

            }

        }
        break; // case fe_collection


      case SHAPE_TYPE::DISK:
        p = std::unique_ptr<Shape>( new class Disk(dict) );
        break;

      case SHAPE_TYPE::CYLINDER:
        p = std::unique_ptr<Shape>( new class Cylinder(dict) );
        break;

      case SHAPE_TYPE::SPHERE:
        p = std::unique_ptr<Shape>( new class Sphere(dict) );
        break;

      case SHAPE_TYPE::SPHERE_COLLECTION:
        {

          MYASSERT(!(file_spec.isEmpty),"sphere_collection shape dictionary missing sub-dictionary \"file\"!");



          switch (file_spec.file_format_type) {
            case Settings::FILEFORMAT_TYPE::TECPLOT:
              {
                std::unique_ptr<FieldData> FieldData = Tecplot::readTecplot1DData(
                    file_spec.full_file_name,
                    SphereCollection::requiredVars());


                //				std::cout << "field data read in sphere collection factory: " << FieldData->nData() << std::endl;


                p = std::unique_ptr<Shape>
                (
                    new class SphereCollection(
                        FieldData.get(),
                        file_spec.tolerance,
                        file_spec.transform )
                );

              }
              break;
            default:
              MYASSERT(false,
                       std::string("file format for sphere_collection shape must be \"tecplot\", got \"")
              + file_spec.file_format_name + std::string("\" instead!"));
              break;
          } // switch file_format_name



        }
        break; // case sphere_collection
      default:
        MYASSERT(false,std::string("construction of ") + shape_name + std::string(" not implemented"));
        break;

    } // switch shape_type


    return p;
      } // makeShape

} /* namespace Shapes */
