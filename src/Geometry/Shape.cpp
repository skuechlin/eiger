/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * Shape.cpp
 *
 *  Created on: Oct 21, 2014
 *      Author: kustepha
 */

#include <embree3/rtcore.h>

#include "Ray.h"
#include "Shape.h"
#include "Primitives/MyError.h"

#include "Triangle.h"

#include "IO/Output.h"


using namespace Shapes;


namespace
{

  void boundsFunc(const struct RTCBoundsFunctionArguments* args)
  {
    const Shape* shapes = (const Shape*) args->geometryUserPtr;
    RTCBounds* bounds_o = args->bounds_o;
    Shapes::AABB2RTCBounds(shapes->element(args->primID)->boundingBox(),bounds_o);
  }


  void intersectFunc(const RTCIntersectFunctionNArguments* args)
  {

    RayTracing::IntersectContext* context = (RayTracing::IntersectContext*)args->context;
    RayTracing::Ray* r = &(context->ray);

    const Shape* shapes = (const Shape*) args->geometryUserPtr;
    const Shape* shape = shapes->element(args->primID);

    if (__builtin_expect(
        context->filter_geomID == shape->geomID() &&
        context->filter_primID == args->primID
        ,false))
      { return; }

    shape->intersect(r);

    if (__builtin_expect(r->hit_tf(),false))
      {
        RayTracing::setHit((RTCRayHit*)args->rayhit,r);
      }
    //    else if(context->verb)
    //      {
    //        Shapes::Triangle* tri = (Shapes::Triangle*)shape;
    //#pragma omp critical (COUT)
    //        {
    //          std::cout <<
    //              context->rayhit->ray.o << " -> " <<
    //              (context->rayhit->ray.o + context->rayhit->ray.t*context->rayhit->ray.d) << " " <<
    //              " missed " << shape->name() << " " <<
    //              shape->geomID() << " " << shape->primID() << " (" << args->primID << "): " <<
    //              tri->A().transpose() << " " << tri->B().transpose() << " " << tri->C().transpose() << " " << tri->N().transpose() <<
    //              std::endl;
    //        }
    //      }


  }

  void occludedFunc(const RTCOccludedFunctionNArguments* args)
  {

    RayTracing::IntersectContext* context = (RayTracing::IntersectContext*)args->context;
    RayTracing::Ray* r = &(context->ray);

    const Shape* shapes = (const Shape*) args->geometryUserPtr;
    const Shape* shape = shapes->element(args->primID);

    if (__builtin_expect(
        context->filter_geomID == shape->geomID() &&
        context->filter_primID == args->primID
        ,false))
      { return; }

    shape->occluded(r);

    if (__builtin_expect(r->occluded_tf(),false))
      RayTracing::setOccluded((RTCRay*)(args->ray));

  }


} // namespace


void Shapes::commitToRTCScene(const Shape* s, RTCScene scene)
{
  MYASSERT(s->geomID() != RTC_INVALID_GEOMETRY_ID,"called commitPrimitiveChangeToRTCScene before addToRTCScene");

  RTCGeometry geometry = rtcGetGeometry(scene, s->geomID());

  rtcSetGeometryUserPrimitiveCount(geometry, s->nElements());
  rtcCommitGeometry(geometry);

}

// add geometry to rtc scene
uint32_t Shapes::addToRTCScene(const Shape* s, RTCScene scene, RTCDevice device)
{

  RTCGeometry geometry = rtcNewGeometry(device, RTC_GEOMETRY_TYPE_USER);
  const uint32_t geomID = rtcAttachGeometry(scene,geometry);

  rtcSetGeometryUserData(geometry, const_cast<Shape*>(s));
  rtcSetGeometryBoundsFunction(geometry, boundsFunc, nullptr);
  rtcSetGeometryIntersectFunction(geometry, intersectFunc);
  rtcSetGeometryOccludedFunction(geometry, occludedFunc);
  rtcSetGeometryBuildQuality(geometry,RTC_BUILD_QUALITY_HIGH);

  rtcSetGeometryUserPrimitiveCount(geometry, s->nElements());
  rtcCommitGeometry(geometry);

  rtcReleaseGeometry(geometry);

  return geomID;
}


// NOTE: we could make the functions pure virtual, but then we would have to give them
// a body in each derived class.

// return true if shape intersects axis aligned bounding box bb
bool
Shape::test_AABB(const Eigen::AlignedBox4d& bb)
const
{
  static_cast<void>(bb);
  MYASSERT(false,std::string(name()) + " does not implement \"intersect(const Eigen::AlignedBox4d& bb)\"");
  return false;
}

void
Shape::intersect(RayTracing::Ray *const)
const
{
  MYASSERT(false,std::string(name()) + " does not implement \"intersect(RTCRay& r)\"");
}

void
Shape::occluded(RayTracing::Ray *const)
const
{
  MYASSERT(false,std::string(name()) + " does not implement \"occluded(RTCRay& r)\"");
}


Eigen::Vector4d
Shape::centroid()
const
{
  MYASSERT(false,std::string(name()) + " does not implement \"centroid()\"");
  return Eigen::Vector4d::Zero();
}


// return the area of the shape
double
Shape::area()
const
{
  MYASSERT(false,std::string(name()) + " does not implement \"area\"");
  return 0.;
}
// return the bounding box of the shape
Eigen::AlignedBox4d
Shape::boundingBox()
const
{
  MYASSERT(false,std::string(name()) + " does not implement \"boundingBox\"");
  return Eigen::AlignedBox4d();
}

// return scalar magnitude of vector V normal to shape surface
double
Shape::flux(const Eigen::Vector4d&)
const
{
  MYASSERT(false,std::string(name()) + " does not implement \"flux\"");
  return 0.0;
}

// return a random point on the shape
Eigen::Vector4d
Shape::sample(
    MyRandom::Rng&)
const
{
  MYASSERT(false,std::string(name()) + " does not implement \"sample\"");
  return Eigen::Vector4d();
}

// return the unit normal vector of the shape at a given point
Eigen::Vector4d
Shape::unitNormal(const Eigen::Vector4d&)
const
{
  MYASSERT(false,std::string(name()) + " does not implement \"unitNormal\"");
  return Eigen::Vector4d();
}

// number of vertices in shape
uint64_t
Shape::nVerts()
const
{
  MYASSERT(false,std::string(name()) + " does not implement \"nVerts\"");
  return -1;
}


Tecplot::ZoneType
Shape::tecplotZoneType()
const
{
  MYASSERT(false,std::string(name()) + " does not implement \"tecplotZoneType\"");
  return Tecplot::ZoneType::INVALID;
}

void
Shape::writeToTecplot(
    const std::string& ,
    const std::string& ,
    const double ,
    const uint64_t ,
    const MyMPI::MPIMgr&  ) const
{
  MYASSERT(false,std::string(name()) + " does not implement \"writeToTecplot\"");
}




