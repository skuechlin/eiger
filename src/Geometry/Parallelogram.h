/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * Parallelogram.h
 *
 *  Created on: May 5, 2014
 *      Author: Stephan Kuechlin
 */

#ifndef PARALLELOGRAM_H_
#define PARALLELOGRAM_H_


#include "Triangle.h"

#include "ShapeTests.h"

namespace Settings
{
  class Dictionary;
}

namespace Shapes
{

  class Parallelogram: public Shape
  {
  private:

    // represented by Triangle
    // vertex A mirrored on triangle line BC
    Triangle rep_;

  private:

    // intersection routine
    template<HIT_ACTION action> void intersect_impl(RayTracing::Ray *const rhit) const;

    uint64_t flags() const override {
      return RTC_COMPATIBLE | FINITE | PLANAR | SAMPLEABLE | SHAPE_TYPE::PARALLELOGRAM; }

  public:

    EIGEN_MAKE_ALIGNED_OPERATOR_NEW

    // vertices
    Eigen::Vector4d A() const { return rep_.A(); }
    Eigen::Vector4d B() const { return rep_.B(); }
    Eigen::Vector4d C() const { return rep_.B() + rep_.AC(); } // A mirrored on rep_.BC()
    Eigen::Vector4d D() const { return rep_.C(); }

    // number of vertices
    uint64_t nVerts() const { return 4; }

    // return true if shape intersects axis aligned bounding box bb
    bool test_AABB(const Eigen::AlignedBox4d& bb) const override {
      return Shapes::test_AABB_quadrilateral(bb,A(),B(),C(),D(),rep_.N(),rep_.d_);  }

    // RTC COMPATIBLE

    void intersect(RayTracing::Ray *const r) const override {
      intersect_impl<HIT_ACTION::INTERSECT>(r); }

    void occluded(RayTracing::Ray *const r) const override {
      intersect_impl<HIT_ACTION::OCCLUDE>(r); }

    // implements sampleable

    Eigen::Vector4d sample(MyRandom::Rng& rndGen) const override;

    Eigen::Vector4d unitNormal(const Eigen::Vector4d&) const override { return rep_.N(); }

    // is planar

    double flux(const Eigen::Vector4d& V) const override { return rep_.N().dot(V); }

    // implements finite

    Eigen::Vector4d centroid() const override {
      return A() + .5*( rep_.AB() + rep_.AC() ); }

    double area() const override { return 2.0*rep_.area_; }

    Eigen::AlignedBox4d boundingBox() const override { return rep_.boundingBox(); }

    // NOT WRITABLE

    Parallelogram(
        const Settings::Dictionary& dict,
        const uint32_t prim_id = 0,
        const uint32_t geom_id = -1);

    Parallelogram(
        const Eigen::Vector4d& A,
        const Eigen::Vector4d& B,
        const Eigen::Vector4d& C,
        const uint32_t prim_id = 0,
        const uint32_t geom_id = -1);

    ~Parallelogram() {}
  };

}  // namespace Shapes




#endif /* PARALLELOGRAM_H_ */
