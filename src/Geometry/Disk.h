/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * Disk.h
 *
 *  Created on: Jun 23, 2015
 *      Author: kustepha
 */

#ifndef GEOMETRY_DISK_H_
#define GEOMETRY_DISK_H_

#include "Shape.h"

#include "Constants/Constants.h"

#include <Eigen/Dense>


namespace MyRandom
{
  class Rng;
}

namespace Settings
{
  class Dictionary;
}

namespace Shapes
{

  class Disk : public Shape
  {

  public:

    enum class SAMPLE_WEIGHTING : char {
      RADIUS,
      AREA
    };


  private:

    const Eigen::Vector4d X_; // base point
    const Eigen::Vector4d N_; // normal
    const double r_; // radius
    const double pad_[3] = {0};

    uint64_t flags() const override {
      return RTC_COMPATIBLE | FINITE | PLANAR | SAMPLEABLE | SHAPE_TYPE::DISK; }


  private:
    static constexpr SAMPLE_WEIGHTING weighting_ = SAMPLE_WEIGHTING::AREA;

  private:

    // intersection routine
    template<HIT_ACTION action> void intersect_impl(RayTracing::Ray *const r) const;


    // uniform area sampling
    // return a random point on the shape
    Eigen::Vector4d sample_unif_area(MyRandom::Rng& rndGen) const;

    // uniform radius sampling
    // return a random point on the shape
    Eigen::Vector4d sample_unif_radius(MyRandom::Rng& rndGen) const;

  public:

    // INTERFACE

    // return true if shape intersects axis aligned bounding box bb
    bool test_AABB(const Eigen::AlignedBox4d& bb) const override;

  private:

    // return true if shape intersects axis aligned bounding box bb
    bool test_AABB_impl(const Eigen::AlignedBox4d& bb) const;

  public:


    // RTC COMPATIBLE

    void intersect(RayTracing::Ray *const r) const override {
      intersect_impl<HIT_ACTION::INTERSECT>(r); }

    void occluded(RayTracing::Ray *const r) const override {
      intersect_impl<HIT_ACTION::OCCLUDE>(r); }

    // FINITE

    Eigen::Vector4d centroid() const override { return X_; }

    // return the area of the shape
    double area() const override { return Constants::pi*r_*r_; }

    // return the bounding box of the shape
    Eigen::AlignedBox4d boundingBox() const override;


    // SAMPLEABLE

    // return a random point on the shape
    Eigen::Vector4d sample(MyRandom::Rng& rndGen) const override
    {
      switch (weighting_) {
        case SAMPLE_WEIGHTING::AREA:
          return sample_unif_area(rndGen);
          break;
        case SAMPLE_WEIGHTING::RADIUS:
          return sample_unif_radius(rndGen);
          break;
      }
      __builtin_unreachable();

      // never reached (Inch'Allah...
      return Eigen::Vector4d::Zero();
    }

    Eigen::Vector4d unitNormal(const Eigen::Vector4d&) const override {
      return N_; }

    double flux(const Eigen::Vector4d& V) const override { return N_.dot(V); }


  public:
    Disk (
        const Eigen::Vector4d& X, 	// base point
        const Eigen::Vector4d& N, 	// normal
        const double r,	  		// radius
        const uint32_t prim_id = 0 );

    Disk (
        const Settings::Dictionary& dict,
        const uint32_t prim_id = 0 );

    virtual ~Disk(){};

    EIGEN_MAKE_ALIGNED_OPERATOR_NEW

  };

} /* namespace Shapes */

#endif /* GEOMETRY_DISK_H_ */
