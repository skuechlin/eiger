/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * FECollection.cpp
 *
 *  Created on: Apr 22, 2014
 *      Author: Stephan Kuechlin
 */

#include <iomanip>

#include "FECollection.h"

#include "Parallel/MyMPI.h"

#include "IO/Output.h"
#include "IO/Tecplot/Tecplot.h"

#include "Settings/Dictionary.h"

#include "Primitives/MyError.h"
#include "Primitives/MyString.h"
#include "Primitives/MyMemory.h"

#include "FieldData/FieldData.h"

//#include "SFC/MortonCodes.h"

namespace Shapes {


  // return the total surface area of the shapes
  double
  FECollection::compute_area()
  const
  {
    double a = 0.;
    for (uint64_t i = 0; i < nElements(); ++i)
      a += element(i)->area();
    return a;
  }


  // return the bounding box of the entire shape data
  Eigen::AlignedBox4d
  FECollection::compute_boundingBox()
  const
  {
    Eigen::AlignedBox4d box;

    for (uint64_t i = 0; i < nElements(); ++i)
      box.extend( element(i)->boundingBox() );

    return box;
  }


  Tecplot::ZoneType
  FECollection::tecplotZoneType()
  const
  {
    return Tecplot::ZoneType::FEQUADRILATERAL;
  }



  void FECollection::writeToTecplot(
      const std::string& boundaryName,
      const std::string& dir,
      const double solTime,
      const uint64_t tn,
      const MyMPI::MPIMgr& mpiMgr ) const {

    // print message
    std::string fname = boundaryName;

    if (mpiMgr.isRoot())
      {
        if (tn != uint64_t(-1))
          std::cout << "writing ts #" << tn << " boundary grid \"" << fname << "\"" << std::endl;
        else
          std::cout << "writing boundary grid \"" << fname << "\"" << std::endl;
      }

    // prepend directory
    fname = dir + std::string("boundary_grid_") + fname;

    std::string fNameTime("");

    if (solTime >= 0.)
      {

        // append solution time
        std::stringstream fnamestr;

        fnamestr.str(std::string(""));
        fnamestr << "_at_ts_";
        fnamestr << std::setw(10);
        fnamestr << std::setfill('0');
        fnamestr << tn;
        fnamestr << "_time_";
        fnamestr << std::fixed;
        fnamestr << std::setw(20);
        fnamestr << std::setprecision(10);
        fnamestr << std::setfill('0');
        fnamestr << solTime;

        fNameTime = fnamestr.str();

      }

    fname += fNameTime;

    // write

    //        void write1DData2Tecplot(
    //            const std::function<double(const int32_t iDat, const int32_t iVar)>& dataFun,
    //            const std::function<int32_t(const int32_t iElem, const int32_t iVrt)>& connFun,
    //            const std::function<int32_t(std::vector<int32_t>& buffer, const int32_t iElem)>& fnConnFun,
    //            const ZoneType zoneType,
    //            const VarLoc varLoc,
    //            const DataSharing dataSharing,
    //            const FileType fileType,
    //            const int32_t nVars,
    //            const int32_t nPoints,
    //            const int32_t nElements,
    //            const std::string& fileName,
    //            const std::string& dataSetName,
    //            const std::string& zoneName,
    //            const std::string& varNames,
    //            const int32_t data_id,
    //            const double solTime,
    //            const bool writeConnectivity,
    //            const bool writeFaceNeighborConnectivity,
    //            const MyMPI::MPIMgr& mpiMgr );

    constexpr int32_t n_vars = 4;

    std::string varNames = "X Y Z ID";

    const auto sdat = shapeDat_.get();

    auto dataFun = [sdat](const int32_t iDat, const int32_t iVar) -> double {
      if (iVar < 3) return sdat->vert(iDat)->coeffRef(iVar);
      return sdat->shapes_[iDat]->primID();
    };

    auto connFun = [sdat](const int32_t iElem, const int32_t iVrt) -> int32_t {
      return sdat->connectivity(iElem,iVrt);
    };

    std::vector<Tecplot::VarLoc> varLoc = {
        Tecplot::VarLoc::NodeCentered,
        Tecplot::VarLoc::NodeCentered,
        Tecplot::VarLoc::NodeCentered,
        Tecplot::VarLoc::CellCentered
    };



    // write
    Tecplot::write1DData2Tecplot(
        dataFun,
        connFun,
        Tecplot::NO_FACENEIGHBORS,
        Tecplot::ZoneType::FEQUADRILATERAL,
        varLoc,
        Tecplot::DataSharing::SHARED,
        Tecplot::FileType::GridFile,
        n_vars,
        static_cast<int32_t>( nVerts() ),
        static_cast<int32_t>( nElements() ),
        fname,
        boundaryName,
        boundaryName,
        varNames,
        geomID(),
        solTime,
        true,
        false,
        mpiMgr);

    //        std::cout << mpiMgr.rank() << " reached " << __FILE__ << " ln: " << std::to_string(__LINE__) << std::endl;


  }

  void FECollection::clear() {
    setNElements(0);
    shapeDat_ = nullptr;
    bb_ = {};
    area_ = 0.;
  }

  void FECollection::update(std::unique_ptr<FEShapeData> shapeDat)
  {
    setNElements( shapeDat->nElements() );
    shapeDat_ = std::move(shapeDat);
    bb_ = compute_boundingBox();
    area_ = compute_area();
  }


  FECollection::FECollection(std::unique_ptr<FEShapeData> shapeDat)
  : Shape(0,0)
  {
    update(std::move(shapeDat));
  }

  FECollection::FECollection()
  : Shape(0,0)
  {}

  FECollection::~FECollection() = default;


} // namespace Shapes
