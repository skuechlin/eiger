/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * Sphere.cpp
 *
 *  Created on: Oct 28, 2015
 *      Author: kustepha
 */

#include <Geometry/Sphere.h>

#include <iomanip>


#include "Ray.h"
#include "ShapeTests.h"

#include "Primitives/MyRandom.h"
#include "Primitives/MyError.h"
#include "Primitives/NormalDistribution.h"

#include "Constants/Constants.h"

#include "Settings/Dictionary.h"

using namespace Shapes;



namespace Shapes {


  // implicit tecplot writer interface

  // number of variables
  uint Sphere::nVars() { return 4; }

  // value of variable i
  double
  Sphere::getVar(const uint i)
  const
  {
    switch (i) {
      case  0 : return X_.coeffRef(0); break;
      case  1 : return X_.coeffRef(1); break;
      case  2 : return X_.coeffRef(2); break;
      case  3 : return r_; break;
      default: return 0./0.;
    }
  }

  // name of variable i
  std::string
  Sphere::getVarName(const uint i)
  {
    switch (i) {
      case 0 : return std::string("X"); 	break;
      case 1 : return std::string("Y");	break;
      case 2 : return std::string("Z");	break;
      case 3 : return std::string("R"); 	break;
      default: return std::string();
    }
  }

  // return true if shape intersects axis aligned bounding box bb
  bool
  Sphere::test_AABB(const Eigen::AlignedBox4d& bb)
  const
  {
    // sphere and AABB intersect if the (squared) distance
    // between AABB and sphere center is less than the (squared) sphere radius
    if (sqDist_point_AABB(bb,X_) <= rsq_)
      {
        // box might be entirely contained in sphere
        // check all vertices. if one is outside (or touches), we intersect
        for (uint64_t i = 0; i < 8; ++i)
          if ( (AABB_corner(bb,i)-X_).squaredNorm() >= rsq_ ) return 1;
      }
    return 0;

  }


  template<HIT_ACTION action>
  void
  Sphere::intersect_impl(RayTracing::Ray *const ray)
  const
  {

    // from:
    // Christer Ericson: Real-Time Collision Detection, (c) Elsevier 2005
    // p. 178

    // intersection time given by quadratic equation
    // a*t*t + 2*b*t + c = 0
    // t = (-b +- sqrt(b*b-a*c))/a

    Eigen::Vector4d M = ray->org() - X_;
    double a = ray->dir().dot(ray->dir());
    double b = M.dot(ray->dir());
    double c = M.dot(M) - rsq_;

    // exit if ray origin outside of sphere (c>0) and direction pointing away from sphere
    // (b>0)
    if ( b > 0.0 && c > -ABSEPS )
      return;

    double discr = b*b-a*c;

    // a negative discriminant corresponds to ray missing sphere
    if ( discr < 0.0 )
      return;


    // we have intersect
    switch (action) {
      case HIT_ACTION::OCCLUDE:
        ray->setOccluded();
        break;
      case HIT_ACTION::INTERSECT:
        // candidate
        // smaller solution is sphere entry, larger is exit.
        // if ray origin is inside sphere by more than tolerance ( c < -ABSEPS ), choose exit value
        double t = (c > -ABSEPS) ? -b - sqrt(discr) : -b + sqrt(discr);
        t /= a;
        ray->setHit(
               unitNormal( (Eigen::Vector4d)(ray->org() + t*ray->dir()) ),
               t,
               geomID(),primID(),
               bool( ( c > -ABSEPS ) ^ ( sign_ < 0 ) ) // flip if flow inside
        );
        break;
    }

    return;

  }


  // return a random point on the shape
  Eigen::Vector4d
  Sphere::sample( MyRandom::Rng& rng )
  const
  {
    const MyRandom::NormalDistribution sn;
    return X_ + r_* ( Eigen::Vector4d(
        sn(rng),
        sn(rng),
        sn(rng),
        0.0).normalized() );
  }


  namespace
  {

    Eigen::Vector4d
    getX( const Settings::Dictionary& dict)
    {
      MYASSERT(dict.hasMember("center"),"Sphere dictionary missing entry \"center\"");

      Settings::Dictionary cDict = dict.get<Settings::Dictionary>("center");

      MYASSERT(cDict.isArray(),"Sphere dictionary entry \"center\" must be array");

      MYASSERT(cDict.size() == 3,"Sphere dictionary entry \"center\" must be array of length 3");

      // get the three elements

      Eigen::Vector4d X;
      X.setZero();

      cDict.get(X.data(),0,3);

      return X;
    }

    double
    getR( const Settings::Dictionary& dict)
    {
      MYASSERT(dict.hasMember("radius"),"Sphere dictionary missing entry \"radius\"");
      return dict.get<double>("radius");
    }

    double
    getS ( const Settings::Dictionary& dict )
    {
      return ( dict.get<bool>("internal",false) ) ? -1. : 1.;
    }

  }


  Sphere::Sphere(
      const Eigen::Vector4d& X,
      const double r,
      const double s,
      const uint32_t prim_id)
  : Shape(prim_id)
  ,  X_(X)
  ,  r_(r)
  ,  rsq_(r_*r_)
  ,  sign_(s)
  {

    //		std::cout << std::scientific << std::setprecision(6) << std::setw(13) << X_.transpose() << " " << r_;
    //		std::cout << std::endl<<std::endl;

  }

  Sphere::Sphere (
      const Settings::Dictionary& dict,
      const uint32_t prim_id )
  : Shape(prim_id)
  , X_(getX(dict))
  , r_(getR(dict))
  , rsq_(r_*r_)
  , sign_(getS(dict))
  {}





} /* namespace Shapes */
