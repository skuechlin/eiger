/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * Quadrilateral.h
 *
 *  Created on: Nov 6, 2014
 *      Author: kustepha
 */

#ifndef GEOMETRY_QUADRILATERAL_H_
#define GEOMETRY_QUADRILATERAL_H_

#include "Ray.h"
#include "Shape.h"

#include "ShapeTests.h"

#include <Eigen/Dense>

namespace Shapes {

  class Quadrilateral: public Shape {

  private:

    //   /B
    //  / |
    // C  A -->N
    // | /
    // D/

    // (unit) normal vector
    Eigen::Vector4d N_ = Eigen::Vector4d::Zero();

    // vertices
    Eigen::Vector4d A_;
    Eigen::Vector4d B_;
    Eigen::Vector4d C_;
    Eigen::Vector4d D_;

    // aux. planes
    Eigen::Vector4d N1_;
    Eigen::Vector4d N2_;
    Eigen::Vector4d N3_;

    // area
    double area_;

    // ratio of sub-triangle areas
    double rat_;
    double pad_[2] = {0.};

    // offset
    double d_;
    double d1_;
    double d2_;
    double d3_;

  private:

    uint64_t flags() const override {
      return RTC_COMPATIBLE | FINITE | PLANAR | SAMPLEABLE | SHAPE_TYPE::QUADRILATERAL; }

    // update vertex change
    void update();

    // intersection routine
    template<HIT_ACTION action> void intersect_impl(RayTracing::Ray *const r) const;

  public:

    // vertices
    const Eigen::Vector4d& A() const { return A_; }
    const Eigen::Vector4d& B() const { return B_; }
    const Eigen::Vector4d& C() const { return C_; }
    const Eigen::Vector4d& D() const { return D_; }


    // number of vertices
    uint64_t nVerts() const override { return 4; }

    // return true if shape intersects axis aligned bounding box bb
    bool test_AABB(const Eigen::AlignedBox4d& bb) const override {
      return Shapes::test_AABB_quadrilateral(bb,A(),B(),C(),D(),N_,d_); }

    void intersect(RayTracing::Ray *const r) const override {
      intersect_impl<HIT_ACTION::INTERSECT>(r); }

    void occluded(RayTracing::Ray *const r) const override {
      intersect_impl<HIT_ACTION::OCCLUDE>(r); }

    Eigen::Vector4d sample(MyRandom::Rng& rndGen) const override;

    Eigen::Vector4d unitNormal(const Eigen::Vector4d&) const override { return N_; }

    double flux(const Eigen::Vector4d& V) const override { return N_.dot(V); }

    Eigen::Vector4d centroid() const override { return .25*( A() + B() + C() + D() ); }

    double area() const override { return area_; }

    Eigen::AlignedBox4d boundingBox() const override;

  public:

    // CONSTRUCTOR
    Quadrilateral(
        const Eigen::Matrix<double,4,4>& vrts,
        const uint32_t prim_id = 0);

    Quadrilateral(
        const Eigen::Vector4d& A,
        const Eigen::Vector4d& B,
        const Eigen::Vector4d& C,
        const Eigen::Vector4d& D,
        const uint32_t prim_id = 0);

    virtual ~Quadrilateral() = default;

    EIGEN_MAKE_ALIGNED_OPERATOR_NEW


  };

} /* namespace Shapes */

#endif /* GEOMETRY_QUADRILATERAL_H_ */
