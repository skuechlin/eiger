/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * Quadrilateral.cpp
 *
 *  Created on: Nov 6, 2014
 *      Author: kustepha
 */

#include "Ray.h"
#include "Quadrilateral.h"

#include "ShapeTests.h"

#include "Primitives/MyRandom.h"

namespace Shapes {

  Eigen::AlignedBox4d
  Quadrilateral::boundingBox()
  const
  {
    Eigen::AlignedBox4d bb(A_);
    bb.extend(B_);
    bb.extend(C_);
    bb.extend(D_);
    bb.extend( (Eigen::Vector4d)( bb.min() - BBEPS*Eigen::Vector4d(1.,1.,1.,0.) ) );
    bb.extend( (Eigen::Vector4d)( bb.max() + BBEPS*Eigen::Vector4d(1.,1.,1.,0.) ) );
    return bb;
  }


  template<HIT_ACTION action>
  void
  Quadrilateral::intersect_impl(RayTracing::Ray *const ray)
  const
  {
    // triangle tests based on
    // Jiri Havel and Adam Herout
    // Yet Faster Ray-Triangle Intersection (Using SSE4)
    // IEEE Transactions on Visualization and Computer Graphics 2010


    double t(d_ - ray->org().dot(N_)); // compute candidate intersection
    double det(ray->dir().dot(N_));

    // check intersection candidate
    // if it lies on line segment (line-plane intersection)
    if ( !test_approx_segment_plane(t,det,ray->tfar) ) return;

    // check if it lies inside the triangle
    Eigen::Vector4d P(det*ray->org() + t*ray->dir());

    double u(P.dot(N1_) + det*d1_);
    double v;
    //	if ( std::signbit(u) != std::signbit(det - u) )
    if ( !gt(u,det,-ABSEPS) )
      {
        // check other triangle
        u *= -rat_;
        //		if ( std::signbit(u) != std::signbit(det - u) ) return;

        v = P.dot(N3_) + det*d3_;

      }
    else
      v = P.dot(N2_) + det*d2_;

    //	if ( std::signbit(v) != std::signbit(det - u - v) ) return;
    if ( !gt(v,det,-ABSEPS) || !lt(v+u,det,1.0+2*ABSEPS) ) return;

    // we have intersect
    switch (action) {
      case HIT_ACTION::OCCLUDE:
        ray->setOccluded();
        break;
      case HIT_ACTION::INTERSECT:
        ray->setHit(N_,t,det,geomID(),primID());
        break;
    }

    return;
  }




  Eigen::Vector4d
  Quadrilateral::sample(
      MyRandom::Rng& rndGen)
  const
  {
    double u(rndGen.uniform());
    double v(rndGen.uniform());
    double w(rndGen.uniform());

    if ((u+v) > 1.0) { u = 1.0 - u; v = 1.0 - v; }

    if (w < rat_/(rat_+1.))
      return A_ + u*(B_-A_) + v*(C_-A_);
    else
      return A_ + u*(D_-A_) + v*(C_-A_);

  }



  void
  Quadrilateral::update()
  {

    A_(3) = 0.0;
    B_(3) = 0.0;
    C_(3) = 0.0;
    D_(3) = 0.0;

    N_.setZero();
    N1_.setZero();
    N2_.setZero();


    Eigen::Vector4d AB = B_ - A_;
    Eigen::Vector4d AC = C_ - A_;
    Eigen::Vector4d AD = D_ - A_;


    N_ = AB.cross3(AC);
    double nnrm1 = N_.norm();
    double nnrm2 = AD.cross3(AC).norm();


    // area ratio
    rat_ = nnrm1 / nnrm2;

    // area
    area_ = .5*(nnrm1+nnrm2);


    N_.normalize();

    d_ = A_.dot(N_);

    N1_ = AC.cross3(N_) / nnrm1;
    N2_ = N_.cross3(AB) / nnrm1;
    N3_ = AD.cross3(N_) / nnrm2;

    d1_ = -N1_.dot(A_);
    d2_ = -N2_.dot(A_);
    d3_ = -N3_.dot(A_);


  }

  // CONSTRUCTOR
  Quadrilateral::Quadrilateral(
      const Eigen::Vector4d& A,
      const Eigen::Vector4d& B,
      const Eigen::Vector4d& C,
      const Eigen::Vector4d& D,
      const uint32_t prim_id)
  : Shape(prim_id)
  , A_(A), B_(B), C_(C), D_(D)
  {
    update();
  }

  Quadrilateral::Quadrilateral(
      const Eigen::Matrix<double,4,4>& vrts,
      const uint32_t prim_id)
  : Quadrilateral(
      vrts.col(0),
      vrts.col(1),
      vrts.col(2),
      vrts.col(3),
      prim_id)
  {}

} /* namespace Shapes */
