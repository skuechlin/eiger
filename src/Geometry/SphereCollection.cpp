/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * SphereCollection.cpp
 *
 *  Created on: Nov 13, 2015
 *      Author: kustepha
 */

#include "SphereCollection.h"


#include "FieldData/FieldData.h"

#include "Parallel/MyMPI.h"

#include "IO/Output.h"
#include "IO/Tecplot/Tecplot.h"

#include "Settings/Dictionary.h"

#include "Primitives/MyString.h"
#include "Primitives/MyArithm.h"
#include "Primitives/MyIntrin.h"

#include "Boundary/BoundaryProbe.h"

namespace Shapes {



  // return the total surface area of the shape data
  double
  SphereCollection::area()
  const
  {
    double a = 0.;
    for (uint64_t i = 0; i < nElements(); ++i)
      a += element(i)->area();
    return a;
  }

  void SphereCollection::writeToTecplot(
      const std::string& boundaryName,
      const std::string& dir,
      const double solTime,
      const uint64_t /*tn*/,
      const MyMPI::MPIMgr& mpiMgr ) const {

    // print message
    std::string fname = boundaryName;

    if (mpiMgr.isRoot())
      std::cout << "writing boundary shape \"" << fname << "\"" << std::endl;

    // prepend directory
    fname = dir + std::string("boundary_") + fname;

    // write

    std::string varNames = getVarName(0);
    for (uint64_t i = 1; i < nVars(); ++i)
      varNames += (" " + getVarName(i));

    std::vector<Tecplot::VarLoc> varLoc(nVars(),Tecplot::VarLoc::NodeCentered);


    //    void write1DData2Tecplot(
    //        const std::function<double(const int32_t iDat, const int32_t iVar)>& dataFun,
    //        const std::function<int32_t(const int32_t iElem, const int32_t iVrt)>& connFun,
    //        const std::function<int32_t(std::vector<int32_t>& buffer, const int32_t iElem)>& fnConnFun,
    //        const ZoneType zoneType,
    //        const VarLoc varLoc,
    //        const DataSharing dataSharing,
    //        const FileType fileType,
    //        const int32_t nVars,
    //        const int32_t nPoints,
    //        const int32_t nElements,
    //        const std::string& fileName,
    //        const std::string& dataSetName,
    //        const std::string& zoneName,
    //        const std::string& varNames,
    //        const int32_t data_id,
    //        const double solTime,
    //        const bool writeConnectivity,
    //        const bool writeFaceNeighborConnectivity,
    //        const MyMPI::MPIMgr& mpiMgr );

    // write
    Tecplot::write1DData2Tecplot(
        [this](const int32_t iDat, const int32_t iVar)->double{ return getVar(iDat,iVar);},
        Tecplot::NO_CONNECTIVITY,
        Tecplot::NO_FACENEIGHBORS,
        tecplotZoneType(),
        varLoc,
        Tecplot::DataSharing::SHARED,
        Tecplot::FileType::GridFile,
        nVars(),
        nVerts(),
        nElements(),
        fname,
        boundaryName,
        boundaryName,
        varNames,
        geomID(),
        solTime,
        false,
        false,
        mpiMgr);

  }




  SphereCollection::SphereCollection(const SphereDataT& sphereData)
  : Shape(0,sphereData.cols())
  {

    spheres_.reserve( nElements() );

    for (uint64_t i = 0; i < nElements(); ++i)
      addSphere(sphereData.col(i));

    //		std::cout << "created " << nElements() << " spheres" << std::endl;

  }

  SphereCollection::SphereCollection(
      const FieldData* fData,
      const double tol,
      const Eigen::Transform<double,4,Eigen::Affine>& tr)
  : Shape(0,fData->getNData("R"))
  {


    //	std::cout << "constructing SphereCollection from FieldData with " << nElements() << " data" << std::endl;


    std::string msg = "Error constructing SphereCollection: ";
    std::string msg2 = "available vars are: ";

    for (uint i = 0; i < fData->varNames().size(); ++i)
      {
        msg2 += ("\"" + fData->varNames()[i] + "\" ");
      }


    for (uint i = 0; i < requiredVars().size(); ++i)
      {
        std::string var = requiredVars()[i];
        MYASSERT(fData->hasVar(var), msg + "missing variable \"" + var + "\". " + msg2 );
      }


    spheres_.reserve( nElements() );

    const double* X = &(fData->getVarData("X")[0]);
    const double* Y = &(fData->getVarData("Y")[0]);
    const double* Z = &(fData->getVarData("Z")[0]);
    const double* R = &(fData->getVarData("R")[0]);

    //    std::cout << msg2 << std::endl;


    double invtol = 1./tol;

    for (uint64_t i = 0; i < nElements(); ++i,++X,++Y,++Z,++R)
      {
        //      std::cout << i << " " << *X << " " << *Y << " " << *Z << " " << *R << " " << std::endl;


        const double Xval = *X;
        const double Yval = *Y;
        const double Zval = *Z;
        const double Rval = *R;

        MYASSERT(std::isfinite(Xval),
                 std::string("sphere collection received illegal X position value from field data: ")
        + std::to_string(Xval));
        MYASSERT(std::isfinite(Yval),
                 std::string("sphere collection received illegal Y position value from field data: ")
        + std::to_string(Yval));
        MYASSERT(std::isfinite(Zval),
                 std::string("sphere collection received illegal Z position value from field data: ")
        + std::to_string(Zval));
        MYASSERT(std::isfinite(Rval) && Rval > 0.,
                 std::string("sphere collection received illegal radius value from field data: ")
        + std::to_string(Rval));

        Eigen::Vector4d V(Xval,Yval,Zval,Rval);

        if (tol > 0.)
          V = MyIntrin::round(V,tol,invtol);

        V = tr*V;

        addSphere( V );
      }

    //
    //    for (uint64_t i = 0; i < nElements(); ++i)
    //      std::cout << i << ": " << spheres_[i].primID() << std::endl;

  }









} /* namespace Shapes */
