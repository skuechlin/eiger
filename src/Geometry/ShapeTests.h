/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * ShapeTests.h
 *
 *  Created on: Oct 23, 2015
 *      Author: kustepha
 */

#ifndef SRC_GEOMETRY_SHAPETESTS_H_
#define SRC_GEOMETRY_SHAPETESTS_H_

#include <list>

#include <Eigen/Dense>

#include "Shape.h"


namespace Shapes
{



  // get box corner based on number
  inline
  Eigen::Vector4d
  AABB_corner(const Eigen::AlignedBox4d& bb, const int corner)
  {

    // box corner numbering:
    //
    //   6----7
    //  /|   /|
    // 2----3 |
    // | 4--|-5
    // |/   |/
    // 0----1
    //

    //	    BottomLeftFloor=0,
    //	    BottomRightFloor=1,
    //	    TopLeftFloor=2,
    //		TopRightFloor=3,
    //	    BottomLeftCeil=4,
    //		BottomRightCeil=5,
    //	    TopLeftCeil=6,
    //		TopRightCeil=7

    // set bit at position dim in corner number indicates
    // that dim coordinate of corner is max of box
    //
    //  # bits
    //  0 000  minx miny minz
    //  1 001
    //  2 010  minx maxy minz
    //  3 011
    //  4 100  maxz miny minx etc.
    //  5 101
    //  6 110
    //  7 111


    Eigen::Vector4d res(0.,0.,0.,0.);
    int mask = 1;
    for(int d=0; d<3; ++d)
      {
        if( mask & corner ) res[d] = bb.max()[d];
        else                res[d] = bb.min()[d];
        mask <<= 1; // select next bit
      }
    return res;
  }

  // get box edge based on number
  inline
  void
  AABB_edge(const Eigen::AlignedBox4d& bb, const int edge, Eigen::Vector4d& A, Eigen::Vector4d& B)
  {

    // box edge numbering:
    //
    //               10
    //       6--------------7
    //      /|             /|
    //   11/ |           9/ |
    //    /  |           /  |
    //   /  6|   8      /   |5
    //  2--------------3    |
    //  |    |         |    |
    //  |    |      2  |    |
    //  |    4---------|----5
    // 7|   /          |4  /
    //  | 3/           |  /1
    //  | /            | /
    //  |/             |/
    //  0--------------1
    //          0


    // box corner numbering:
    //
    //   6----7
    //  /|   /|
    // 2----3 |
    // | 4--|-5
    // |/   |/
    // 0----1
    //

    // edge     0   1   2   3   4     5     6    7    8    9   10   11
    //         01  23  45  67  89  1011, 1213 1415 1617 1819 2021 2223
    // corner: 01, 15, 54, 40, 13,  5 7,  4 6, 0 2, 2 3, 3 7, 7 6, 6 2

    constexpr int corner[] =
    {0,1, 1,5, 5,4, 4,0, 1,3, 5,7, 4,6, 0,2, 2,3, 3,7, 7,6, 6,2};

    A = AABB_corner(bb,corner[(edge*2)%24]);
    B = AABB_corner(bb,corner[((edge*2)+1)%24]);

  }

  // get box face based on number
  inline
  void
  AABB_face(
      const Eigen::AlignedBox4d& bb,
      const int edge,
      Eigen::Vector4d& A,
      Eigen::Vector4d& B,
      Eigen::Vector4d& C,
      Eigen::Vector4d& D)
  {

    // box face numbering:
    //
    //
    //       6--------------7
    //      /|             /|
    //     / |    5       / |
    //    /  |           /  |
    //   /   |          /   |
    //  2--------------3    |
    //  |    |     3   | 2  |
    //  | 4  |         |    |
    //  |    4---------|----5
    //  |   /          |   /
    //  |  /    1      |  /
    //  | /            | /
    //  |/             |/
    //  0--------------1
    //          0


    // box corner numbering:
    //
    //   6----7
    //  /|   /|
    // 2----3 |
    // | 4--|-5
    // |/   |/
    // 0----1
    //

    // face          0        1        2         3         4         5
    //         0 1 2 3  4 5 6 7  8 91011, 12131415  16171819  20212223
    // corner: 0 4 5 1, 0 1 3 2, 1 5 7 3,  5 4 6 7,  0 2 6 4,  2 3 7 6

    constexpr int corner[] =
    {0,4,5,1, 0,1,3,2, 1,5,7,3, 5,4,6,7 ,0,2,6,4, 2,3,7,6};

    A = AABB_corner(bb,corner[(edge*4)%24]);
    B = AABB_corner(bb,corner[((edge*4)+1)%24]);
    C = AABB_corner(bb,corner[((edge*4)+2)%24]);
    D = AABB_corner(bb,corner[((edge*4)+3)%24]);

  }


  // common interval tests

  __attribute__((__always_inline__))
  inline bool
  lt(const double s, const double v, const double ub)
  {
    return std::signbit(v) == std::signbit(v*ub - s);
  }

  __attribute__((__always_inline__))
  inline bool
  le(const double s, const double v, const double ub)
  {
    if (v > 0.)
      return v*ub-s >= 0.;
    else
      return v*ub-s <= 0.;
  }


  __attribute__((__always_inline__))
  inline bool
  gt(const double s, const double v, const double lb)
  {
    return std::signbit(v) == std::signbit(s - v*lb);
  }

  __attribute__((__always_inline__))
  inline bool
  ge(const double s, const double v, const double lb)
  {
    if (v > 0.)
      return s-v*lb >= 0.;
    else
      return s-v*lb <= 0.;
  }


  __attribute__((__always_inline__))
  inline bool
  test_approx_segment_plane(const double dist, const double vel, const double t_max)
  {

    // negative velocity: allow negative hit distance of ABSEPS
    // positive velocity: minimum hit distance of ABSEPS
    // max hit distance increased by ABSEPS for both negative and positive velocity
    return gt( dist - ABSEPS,                    vel, 0.0   )
        && lt( dist - std::copysign(ABSEPS,vel), vel, t_max );

  }

  // test line segment for intersection with plane
  inline bool
  test_segment_plane(
      const Eigen::Vector4d& A,
      const Eigen::Vector4d& B,
      const Eigen::Vector4d& N,
      const double d)
  {

    double dist = d - N.dot(A);
    double vel = N.dot(B-A);
    return ( ge(dist,vel,0.0) && le(dist,vel,1.0) );

  }


  // find intersection point of line segment and plane
  inline double
  intersect_segment_plane(
      const Eigen::Vector4d& A,
      const Eigen::Vector4d& B,
      const Eigen::Vector4d& N,
      const double d)
  {

    double dist = d - N.dot(A);
    double vel = N.dot(B-A);
    if ( ge(dist,vel,0.0) && le(dist,vel,1.0) )
      return dist / vel;
    else
      return -1.0;

  }

  // returns squared distance between point C and segment AB
  inline double
  sqDist_point_segment(
      const Eigen::Vector4d& A,
      const Eigen::Vector4d& B,
      const Eigen::Vector4d& C)
  {

    // from:
    // Christer Ericson: Real-Time Collision Detection, (c) Elsevier 2005
    // p. 130


    Eigen::Vector4d AB( B-A );
    Eigen::Vector4d AC( C-A );
    Eigen::Vector4d BC( C-B );

    double e = AC.dot(AB);

    if (e <= 0.0) // C projects outside AB, A closest
      return AC.dot(AC);

    double f = AB.dot(AB);

    if (e >= f) // C projects outside AB, B closest
      return BC.dot(BC);

    // C project onto AB
    return AC.dot(AC) - e*e/f;

  }


  // returns squared distance between point C and line with point A, unit direction D
  inline double
  sqDist_point_line(
      const Eigen::Vector4d& A,
      const Eigen::Vector4d& D,
      const Eigen::Vector4d& C)
  {

    // from:
    // Christer Ericson: Real-Time Collision Detection, (c) Elsevier 2005
    // p. 130


    Eigen::Vector4d AC( C-A );

    double e = AC.dot(D);

    // C project onto AB
    return AC.dot(AC) - e*e;

  }


  // returns squared distance between point A and AABB bb
  inline double
  sqDist_point_AABB(
      const Eigen::AlignedBox4d& bb,
      const Eigen::Vector4d& A)
  {

    // from:
    // Christer Ericson: Real-Time Collision Detection, (c) Elsevier 2005
    // p. 131


    double sqDist = 0.;
    for (uint64_t i = 0; i < 3; ++i )
      {
        // for each axis count any excess distance outside box extents
        double v = A[i];
        if ( v < bb.min()[i] ) sqDist += (bb.min()[i] - v)*(bb.min()[i] - v);
        if ( v > bb.max()[i] ) sqDist += (v - bb.max()[i])*(v - bb.max()[i]);

      }
    return sqDist;
  }

  // project point A to plane given by unit normal N and distance d
  inline Eigen::Vector4d
  project_point_to_plane(
      const Eigen::Vector4d& A,
      const Eigen::Vector4d& N,
      const double d)
  {
    return A - N.dot( Eigen::Vector4d(A-d*N) )*N;
  }

  // project point A to plane given by point X and unit normal N
  inline Eigen::Vector4d
  project_point_to_plane(
      const Eigen::Vector4d& A,
      const Eigen::Vector4d& X,
      const Eigen::Vector4d& N)
  {
    return A - N.dot(A-X)*N;
  }


  // Test if AABB given by center Cnt and extents Ext intersects
  // segment given by points A and B
  inline bool
  test_AABB_segment(
      const Eigen::Vector4d& Cnt,
      const Eigen::Vector4d& Ext,
      const Eigen::Vector4d& A,
      const Eigen::Vector4d& B)
  {

    // based on separating axis test

    // from:
    // Christer Ericson: Real-Time Collision Detection, (c) Elsevier 2005
    // p. 183

    Eigen::Vector4d M = .5*(A + B); // segment midpoint
    Eigen::Vector4d D = B-M; // segment halflength vector

    M -= Cnt; // translate box and segment to origin;

    // try world coordinate axes as separating axes

    double adx = fabs(D.x());
    if ( fabs(M.x()) > Ext.x() + adx )
      return 0;

    double ady = fabs(D.y());
    if ( fabs(M.y()) > Ext.y() + ady )
      return 0;

    double adz = fabs(D.z());
    if ( fabs(M.z()) > Ext.z() + adz )
      return 0;

    // add in an epsilon term to counteract arithmetic errors when segment is
    // near parallel to a coordinate axis
    adx += ABSEPS;
    ady += ABSEPS;
    adz += ABSEPS;

    // try cross products of segment direction vector with coordinate axes

    if ( fabs(M.y()*D.z() - M.z()*D.y()) > Ext.y()*adz + Ext.z()*ady )
      return 0;

    if ( fabs(M.z()*D.x() - M.x()*D.z()) > Ext.x()*adz + Ext.z()*adx )
      return 0;

    if ( fabs(M.x()*D.y() - M.y()*D.x()) > Ext.x()*ady + Ext.y()*adx )
      return 0;

    // no separating axis found; segment must be overlapping AABB
    return 1;

  }

  // Test if AABB bb intersects
  // segment given by points A and B
  inline bool
  test_AABB_segment(
      const Eigen::AlignedBox4d& bb,
      const Eigen::Vector4d& A,
      const Eigen::Vector4d& B)
  {
    // Compute box center and extents

    // box center
    Eigen::Vector4d c = bb.center();
    // box extents
    Eigen::Vector4d e = bb.max() - c;

    return test_AABB_segment(c,e,A,B);
  }


  // Test if AABB given by center Cnt and extents Ext intersects
  // plane given by normal N and distance dist
  inline bool
  test_AABB_plane(
      const Eigen::Vector4d& Cnt,
      const Eigen::Vector4d& Ext,
      const Eigen::Vector4d& N,
      const double dist)
  {
    // based on separating axis test

    // from:
    // Christer Ericson: Real-Time Collision Detection, (c) Elsevier 2005
    // p. 164

    // Compute the projection interval radius of AABB onto L(t) = c +t*n
    const double r = Ext.dot(N.cwiseAbs());
    // Compute distance of box center from plane
    const double s = N.dot(Cnt) - dist;
    // Intersection occurs when distance s falls within [-r,r] interval
    return fabs(s) <= r;

  }

  // Test if AABB bb intersects
  // plane given by normal N and distance dist
  inline bool
  test_AABB_plane(
      const Eigen::AlignedBox4d& bb,
      const Eigen::Vector4d& N,
      const double dist)
  {
    // Compute box center and extents

    // box center
    Eigen::Vector4d c = bb.center();
    // box extents
    Eigen::Vector4d e = bb.max() - c;

    return test_AABB_plane(c,e,N,dist);
  }

  enum LOCATION_VS_PLANE : int {
    BELOW,
    INTERSECT,
    ABOVE
  };

  inline LOCATION_VS_PLANE
  classify_AABB_plane(
      const Eigen::AlignedBox4d& bb,
      const Eigen::Vector4d& N,
      const double dist)
  {
    if (bb.min().dot(N) - dist > 0.)
      return LOCATION_VS_PLANE::ABOVE;
    else if (bb.max().dot(N) - dist < 0.)
      return LOCATION_VS_PLANE::BELOW;
    else
      return LOCATION_VS_PLANE::INTERSECT;
  }

  // return in matrix V the intersection points (if any)
  // of plane given by N and dist with box bb sorted s.t.
  // they form a convex polygon
  // return number of points found, or uint64_t(-1) if plane is co-planar to box face
  uint64_t
  intersect_AABB_plane(
      const Eigen::AlignedBox4d& bb,
      const Eigen::Vector4d& N,
      const double dist,
      std::list<Eigen::Vector4d,Eigen::aligned_allocator<Eigen::Vector4d>>& V);


  // Test if AABB given by center Cnt and extents Ext intersects
  // quadrilateral given by vertices A,B,C,D, normal N and distance dist
  bool
  test_AABB_quadrilateral(
      const Eigen::Vector4d& Cnt,
      const Eigen::Vector4d& Ext,
      const Eigen::Vector4d& A,
      const Eigen::Vector4d& B,
      const Eigen::Vector4d& C,
      const Eigen::Vector4d& D,
      const Eigen::Vector4d& N,
      const double dist);

  // Test if AABB  intersects
  // quadrilateral given by vertices A,B,C,D, normal N and distance dist
  inline  bool
  test_AABB_quadrilateral(
      const Eigen::AlignedBox4d& bb,
      const Eigen::Vector4d& A,
      const Eigen::Vector4d& B,
      const Eigen::Vector4d& C,
      const Eigen::Vector4d& D,
      const Eigen::Vector4d& N,
      const double dist)
  {
    // Compute box center and extents

    // box center
    Eigen::Vector4d Cnt = bb.center();
    // box extents
    Eigen::Vector4d Ext = bb.max() - Cnt;

    return test_AABB_quadrilateral(Cnt,Ext,A,B,C,D,N,dist);

  }

  // test if point p is contained in polygon defined by points V[]
  //      Input:   P = a point,
  //               V[] = vertex points of a polygon V[n+1] with V[n]=V[0]
  bool test_point_in_polygon2D(const Eigen::Vector2d& P, const Eigen::Vector2d* const V, const uint64_t n);


}  // namespace Shapes



#endif /* SRC_GEOMETRY_SHAPETESTS_H_ */
