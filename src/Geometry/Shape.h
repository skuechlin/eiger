/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * Shape.h
 *
 *  Created on: Apr 10, 2014
 *      Author: Stephan Kuechlin
 */

#ifndef SHAPE_H_
#define SHAPE_H_


#include <Eigen/Dense>

#include <limits>
#include <memory>
//#include <map>

#include <embree3/rtcore.h>

#include "IO/Tecplot/TecplotConsts.h"


namespace MyRandom { class Rng; }
namespace Boundary { class BoundaryProbe; }
namespace Settings { class Dictionary; }
namespace MyMPI    { class MPIMgr; }

namespace RayTracing
{
  struct Ray;
  struct IntersectContext;
}

namespace Shapes
{

  // epsilon for shape bounding box (minimum bounding box half width)
  static constexpr double BBEPS = std::numeric_limits<float>::epsilon(); // [m]

  // epsilon for absolute tolerance in intersection calculations (wall thickness)
  static constexpr double ABSEPS = 1.e-12; //[m]

  inline Eigen::AlignedBox4d relaxBox(const Eigen::AlignedBox4d& in)
  {
    const Eigen::Vector4d BBEPSVEC(BBEPS,BBEPS,BBEPS,0.0);
    return { in.min() - BBEPSVEC, in.max() + BBEPSVEC };
  }


  enum class HIT_ACTION : uint8_t {
    INTERSECT,
    OCCLUDE
  };

  enum SHAPE_TYPE : uint64_t {
    TRIANGLE,
    PARALLELOGRAM,
    QUADRILATERAL,
    FE_COLLECTION,
    DISK,
    CYLINDER,
    SPHERE,
    SPHERE_COLLECTION,
    CELLFACE_COLLECTION,
    N_SHAPE_TYPES,
    MAX_N_SHAPE_TYPES=32
  };

  static constexpr char SHAPE_NAME[SHAPE_TYPE::N_SHAPE_TYPES][32] = {
      "triangle",
      "parallelogram",
      "quadrilateral",
      "fe_collection",
      "disk",
      "cylinder",
      "sphere",
      "sphere_collection",
      "cellface_collection"
  };

  inline void AABB2RTCBounds(const Eigen::AlignedBox4d& aabb, RTCBounds* bounds_o)
  {
    double v;

    v = aabb.min().coeffRef(0);
    v = v*(1.0-std::copysign(BBEPS,v));
    bounds_o->lower_x = v;

    v = aabb.min().coeffRef(1);
    v = v*(1.0-std::copysign(BBEPS,v));
    bounds_o->lower_y = v;

    v = aabb.min().coeffRef(2);
    v = v*(1.0-std::copysign(BBEPS,v));
    bounds_o->lower_z = v;

    v = aabb.max().coeffRef(0);
    v = v*(1.0+std::copysign(BBEPS,v));
    bounds_o->upper_x = v;

    v = aabb.max().coeffRef(1);
    v = v*(1.0+std::copysign(BBEPS,v));
    bounds_o->upper_y = v;

    v = aabb.max().coeffRef(2);
    v = v*(1.0+std::copysign(BBEPS,v));
    bounds_o->upper_z = v;
  }


  class Shape
  {
      // a Shape is any piece of Geometry (either a geometric primitive, eg.
      // a Triangle or a collection of primitives)

    protected:
      static constexpr uint64_t SHAPE_TYPE_MASK     = MAX_N_SHAPE_TYPES-1;
      static constexpr uint64_t RTC_COMPATIBLE      = MAX_N_SHAPE_TYPES<<0;
      static constexpr uint64_t FINITE              = MAX_N_SHAPE_TYPES<<1;
      static constexpr uint64_t SAMPLEABLE          = MAX_N_SHAPE_TYPES<<2;
      static constexpr uint64_t PLANAR              = MAX_N_SHAPE_TYPES<<3;
      static constexpr uint64_t WRITABLE            = MAX_N_SHAPE_TYPES<<4;


    private:

      uint32_t geomID_ = -1;	// id of geometry this shape belongs to

      uint32_t primID_;	// id of this shape primitive

      uint64_t nElements_;	// number of atomic shapes this shape is composed of

      virtual uint64_t flags() const = 0;


    protected:


      // CONSTRUCTOR
      Shape(
          const uint32_t primID = 0,
          const uint64_t nElements = 1,
          const uint32_t geomID = -1)
    : geomID_(geomID)
    , primID_(primID)
    , nElements_(nElements)
    {}


    public:

      // return pointer to shape element idx,
      // or shape itself if it only has one (itself)
      virtual Shape* element(const uint64_t) { return this; }

      // return const pointer to shape element idx,
      // or shape itself if it only has one (itself)
      virtual const Shape* element(const uint64_t) const { return this; }

      void setPrimID(const uint32_t primID) { primID_ = primID; }
      virtual void setGeomID(const uint32_t geomID) { geomID_ = geomID; }

      void setNElements(const uint64_t nElements) { nElements_ = nElements; }

      // return geometry ID
      uint32_t geomID() const { return geomID_; }

      // returns primitive ID
      virtual uint32_t primID() const { return primID_; }

      uint64_t nElements() const { return nElements_; }

      uint64_t size() const { return nElements_; }

      // returns object type enum
      SHAPE_TYPE type() const { return static_cast<SHAPE_TYPE>( flags() & SHAPE_TYPE_MASK ); }

      // returns name of object type
      const char* name() const { return SHAPE_NAME[type()]; }

    public:

      // INTERFACE

      // RTC COMPATIBLE
      bool rtcCompatible() const { return flags() & RTC_COMPATIBLE; }

      virtual void intersect(RayTracing::Ray *const r) const;

      virtual void occluded(RayTracing::Ray *const r) const;

      // return the bounding box of the shape
      virtual Eigen::AlignedBox4d boundingBox() const;

      // return true if shape intersects axis aligned bounding box bb
      virtual bool test_AABB(const Eigen::AlignedBox4d& bb) const;


      // FINITE
      bool finite() const { return flags() & FINITE; }

      virtual Eigen::Vector4d centroid() const;

      // return the area of the shape
      virtual double  area() const;

      // SAMPLEABLE
      bool sampleable() const { return flags() & SAMPLEABLE; }

      // return a random point on the shape
      virtual Eigen::Vector4d sample(MyRandom::Rng& rndGen) const;

      // return the unit normal vector of the shape at a given point
      virtual Eigen::Vector4d unitNormal(const Eigen::Vector4d& R) const;

      // PLANAR
      bool planar() const  { return flags() & PLANAR; };

      // return scalar magnitude of vector V normal to shape surface
      virtual double  flux(const Eigen::Vector4d& V) const;

      // WRITABLE (has output method)
      bool writable() const { return flags() & WRITABLE; }

      // number of vertices in shape
      virtual uint64_t nVerts() const;

      virtual Tecplot::ZoneType tecplotZoneType() const;

      virtual void writeToTecplot(
          const std::string& boundaryName,
          const std::string& dir,
          const double solTime,
          const uint64_t tn,
          const MyMPI::MPIMgr& mpiMgr ) const;

      // DESTRUCTOR
      virtual ~Shape() {}
  };


  // add geometry to ray-tracing scene
  uint32_t addToRTCScene(const Shape* s, RTCScene scene, RTCDevice device);

  // must be called after changes to primitives
  void commitToRTCScene(const Shape* s, RTCScene scene);



}  // namespace Shapes



#endif /* SHAPE_H_ */
