/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * ShapeFactory.h
 *
 *  Created on: Jun 24, 2015
 *      Author: kustepha
 */

#ifndef GEOMETRY_SHAPEFACTORY_H_
#define GEOMETRY_SHAPEFACTORY_H_

#include <memory>
#include "Shape.h"

class SimulationBox;

namespace Settings
{
  class Dictionary;
}

namespace Shapes
{


  std::unique_ptr<Shape>
  makeShape(
      const SimulationBox& box,
      const Settings::Dictionary& dict,
      const std::string& inputDirectory );

} /* namespace Shapes */

#endif /* GEOMETRY_SHAPEFACTORY_H_ */
