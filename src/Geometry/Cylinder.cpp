/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * Cylinder.cpp
 *
 *  Created on: Jun 22, 2015
 *      Author: kustepha
 */

#include "Primitives/MyError.h"
#include "Primitives/MyRandom.h"
#include "Primitives/MyVecArithm.h"


#include "ShapeTests.h"

#include "Cylinder.h"

#include "Ray.h"

#include "Settings/Dictionary.h"

namespace Shapes
{

  // return true if shape intersects axis aligned bounding box bb
  bool
  Cylinder::test_AABB(const Eigen::AlignedBox4d& bb)
  const
  {


    // classify AABB vs cylinder end-planes
    double d = X_.dot(D_);

    LOCATION_VS_PLANE bbvslower = classify_AABB_plane(bb,D_,d);
    LOCATION_VS_PLANE bbvsupper = classify_AABB_plane(bb,D_,d+l_);

    if (  bbvslower == LOCATION_VS_PLANE::BELOW || bbvsupper == LOCATION_VS_PLANE::ABOVE )
      return 0; // the AABB lies outside the end-planes


    // test all edges for intersect
    Eigen::Vector4d A;
    Eigen::Vector4d B;
    Eigen::Vector4d AB;


    RayTracing::Ray edge;

    for (uint64_t i = 0; i < 12; ++i)
      {
        AABB_edge(bb,i,A,B);
        AB = B-A;
        RayTracing::initRay(&edge,A,AB,1.0);
        this->occluded(&edge);
        if (edge.occluded_tf())
          return true;// edge intersects cylinder
      }

    // we are still here, only chance for intersect is now pure face intersection

    // following
    // Murilo G. coutinho
    // Dynamic Simulations of Multibody Systems
    // (c) 2001 Springer Verlag New York Inc.
    // pp. 90

    Eigen::Vector4d Na;
    Eigen::Vector4d Nb;
    Eigen::Vector4d Ni;

    // for each axis,
    for (uint64_t i = 0; i < 3; ++i)
      {

        // if cylinder is parallel to this axis, check if axis intersects AABB. if not, no intersection
        if ( fabs(double(D_[i])) == 1.0 )
          return test_AABB_segment(bb,X_,Eigen::Vector4d(X_+l_*D_));

        Ni = Eigen::Vector4d::Unit(i);


        // cylinder not parallel to axis

        // construct plane containing cylinder axis normal to faces normal to axis

        // aux plane normal vector
        Na = D_.cross3(Ni);

        // project cylinder onto this plane:
        // vector from cylinder axis to edges in aux plane
        Nb = std::copysign(r_,double(D_[i]))*(Na.cross3(D_)).normalized();

        // check upper and lower edges for box intersection

        double min = bb.min()[i];
        double max = bb.max()[i];

        // upper
        A = X_+Nb;
        B = X_+l_*D_+Nb;
        if ( test_segment_plane(A,B,Ni,min) ) // front
          return true;
        if ( test_segment_plane(A,B,Ni,max) ) // back
          return true;

        // lower
        A = X_-Nb;
        B = X_+l_*D_-Nb;
        if ( test_segment_plane(A,B,Ni,min) ) // front
          return true;
        if ( test_segment_plane(A,B,Ni,max) ) // back
          return true;


      } // for each axis

    return false; // no intersection


  }



  template<HIT_ACTION action>
  void
  Cylinder::intersect_impl(RayTracing::Ray* const ray)
  const
  {


    // Christer Ericson - Real-Time Collision Detection
    // 5.3.7 (pp 194)

    //    volatile bool should_hit = ((ray.dorg + ray.dtfar*ray.ddir).segment<2>(1).norm() > .05);

    Eigen::Vector4d m = ray->org() - X_;
    Eigen::Vector4d n = ray->tfar*ray->dir();
    Eigen::Vector4d d = l_*D_;

    double md = m.dot(d);
    double nd = n.dot(d);
    double dd = d.dot(d);

    // Test if segment fully outside either endcap of cyl

    if (md < 0. && (md + nd) < 0.)
      {
        //	if (should_hit)
        //	  {
        //	    std::cout << "// segment outside X_ side of cyl" << std::endl;
        //	    std::cout << md << " " << nd << " " << md+nd << std::endl;
        //	  }
        return; // segment outside X_ side of cyl
      }

    if (md > dd && (md + nd) > dd)
      {
        //	if (should_hit)
        //	  {
        //	    std::cout << "// segment outside X_+l_*d_ side of cyl" << std::endl;
        //	    std::cout << md << " " << nd << " " << md+nd << std::endl;
        //	  }
        return; // segment outside X_+l_*d_ side of cyl
      }

    double nn = n.squaredNorm();
    double mn = m.dot(n);

    double a = dd*nn - nd*nd;

    if ( a == 0. )
      {
        //	if (should_hit)
        //	  {
        //	    std::cout << "// segment parallel to cylinder axis" << std::endl;
        //	    std::cout << fabs(a) << " " << Shapes::ABSEPS << std::endl;
        //	  }
        return; // segment parallel to cylinder axis
      }


    double k = m.squaredNorm() - r_*r_;
    double c = dd*k - md*md; // c < 0: ray origin in cylinder, c > 0: ray origin outside of cylinder

    //    if ( (c > 0. && sign_ < 0.) || ( c <= 0. && sign_ >= 0. ) )
    //      {
    ////	if (should_hit)
    ////	  {
    ////	    std::cout << "//intersect from back" << std::endl;
    ////	    std::cout << c << " " << sign_ << std::endl;
    ////
    ////	  }
    //	return; //intersect from back
    //      }

    double b = dd*mn - nd*md;
    double discr = b*b - a*c;

    if (discr < 0.)
      {
        //	if (should_hit)
        //	  {
        //	    std::cout << "// no real roots; no intersection" << std::endl;
        //	    std::cout << discr << " " << b << " " << a<< " "<<c<< " "<< dd << " " << mn <<" " << nd<<" " <<md<<std::endl;
        //	  }

        return; // no real roots; no intersection
      }

    // candidate
    // smaller solution is cylinder entry, larger is exit.
    // if ray origin is inside cylinder ( c < 0 ), choose exit value
    double t = (c > 0.) ? -b - sqrt(discr) : -b + sqrt(discr);

    if ( Shapes::lt(t,a,-Shapes::ABSEPS) || Shapes::gt(t,a,1.0+Shapes::ABSEPS) )
      {
        //	if (should_hit)
        //	  {
        //	    std::cout << "// intersection outside segment" << std::endl;
        //	    std::cout << t << " " << a << " " << t/a << " " << -Shapes::ABSEPS << " " << 1.0+Shapes::ABSEPS << std::endl;
        //	  }
        return; // intersection outside segment
      }

    t /= a;


    if ((md + t*nd) < -Shapes::ABSEPS || (md + t*nd) > dd+Shapes::ABSEPS)
      {
        //	if (should_hit)
        //	  {
        //	    std::cout << "// intersection occurs outside cylinder" << std::endl;
        //	    std::cout << md << " " << dd << " " << t << " " << nd << " " << md+t*nd << " " <<  -Shapes::ABSEPS << " " << dd+Shapes::ABSEPS << std::endl;
        //	  }
        return; // intersection occurs outside cylinder
      }

    // segment intersects cylinder between the end caps

    switch (action) {
      case HIT_ACTION::OCCLUDE:
        ray->setOccluded();
        break;
      case HIT_ACTION::INTERSECT:
        t *= ray->tfar;
        ray->setHit(
               unitNormal((Eigen::Vector4d)(ray->org() + t*ray->dir())),
               t,
               geomID(),primID(),
               uint32_t((sign_*c >= 0.)) );
        break;
    }


    return;
  }





  // return a random point on the shape
  Eigen::Vector4d
  Cylinder::sample( MyRandom::Rng& rndGen )
  const
  {

    double x = l_*rndGen.uniform();
    double theta = 2.*Constants::pi*rndGen.uniform();

    return X_ + MyVecArithm::aimRot(
        {x,r_*sin(theta),r_*cos(theta),0.0},
        D_ );

  }

  Eigen::Vector4d
  Cylinder::unitNormal(const Eigen::Vector4d& R) const {
    return sign_*((Eigen::Vector4d)( (R - X_) - (R - X_).dot(D_)*D_)).normalized(); }


  Eigen::AlignedBox4d
  Cylinder::boundingBox()
  const
  {

    Eigen::Vector4d R = {
        ( D_.y()*D_.y() + D_.z()*D_.z() ),
        ( D_.x()*D_.x() + D_.z()*D_.z() ),
        ( D_.x()*D_.x() + D_.y()*D_.y() ),
        0.0 };

    R = r_*R.cwiseSqrt();
    R += Eigen::Vector4d(Shapes::BBEPS,Shapes::BBEPS,Shapes::BBEPS,0.0);


    Eigen::AlignedBox4d box(X_-R, X_+R);

    box.extend( (Eigen::Vector4d)(X_ + l_*D_ -R ) );
    box.extend( (Eigen::Vector4d)(X_ + l_*D_ +R ) );

    return box;

  }



  Cylinder::Cylinder (
      const Eigen::Vector4d& X,
      const Eigen::Vector4d& D,
      const double r,
      const double l,
      const double s,
      const uint32_t prim_id )
  : Shape(prim_id)
  , X_(X)
  , D_(D.normalized())
  , r_(r)
  , l_(l)
  , sign_(s)
  {}




  namespace
  {

    Eigen::Vector4d
    getX( const Settings::Dictionary& dict)
    {
      MYASSERT(dict.hasMember("base"),"Cylinder dictionary missing entry \"base\"");

      Settings::Dictionary bDict = dict.get<Settings::Dictionary>("base");

      MYASSERT(bDict.isArray(),"Cylinder dictionary entry \"base\" must be array");

      MYASSERT(bDict.size() == 3,"Cylinder dictionary entry \"base\" must be array of length 3");

      // get the three elements

      Eigen::Vector4d X;
      X.setZero();

      bDict.get(X.data(),0,3);

      return X;
    }

    Eigen::Vector4d
    getD( const Settings::Dictionary& dict)
    {
      MYASSERT(dict.hasMember("direction"),"Cylinder dictionary missing entry \"direction\"");

      Settings::Dictionary dDict = dict.get<Settings::Dictionary>("direction");

      MYASSERT(dDict.isArray(),"Cylinder dictionary entry \"direction\" must be array");

      MYASSERT(dDict.size() == 3,"Cylinder dictionary entry \"direction\" must be array of length 3");

      // get the three elements

      Eigen::Vector4d D;
      D.setZero();

      dDict.get(D.data(),0,3);

      return D.normalized();
    }

    double
    getR( const Settings::Dictionary& dict)
    {
      MYASSERT(dict.hasMember("radius"),"Cylinder dictionary missing entry \"radius\"");
      return dict.get<double>("radius");
    }

    double
    getL( const Settings::Dictionary& dict )
    {
      MYASSERT(dict.hasMember("length"),"Cylinder dictionary missing entry \"length\"");
      return dict.get<double>("length");
    }

    double
    getS ( const Settings::Dictionary& dict )
    {
      MYASSERT(dict.hasMember("internal"),"Cylinder dictionary missing entry \"internal\"");
      return ( dict.get<bool>("internal") ) ? -1. : 1.;
    }
  }


  Cylinder::Cylinder (
      const Settings::Dictionary& dict,
      const uint32_t prim_id )
  : Shape(prim_id)
  , X_(getX(dict))
  , D_(getD(dict))
  , r_(getR(dict))
  , l_(getL(dict))
  , sign_(getS(dict))
  {

    //	  std::cout << X_.transpose() << std::endl;
    //	  std::cout << D_.transpose() << std::endl;
    //	  std::cout << r_ << std::endl;
    //	  std::cout << l_ << std::endl;
    //	  std::cout << sign_ << std::endl;
    //
    //		          Eigen::AlignedBox4d bb = boundingBox();
    //		          std::cout << bb.min().transpose(); std::cout << std::endl;
    //		          std::cout << bb.max().transpose(); std::cout << std::endl;

  }

} /* namespace Shapes */
