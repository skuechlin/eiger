/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * SphereCollection.h
 *
 *  Created on: Nov 13, 2015
 *      Author: kustepha
 */

#ifndef SRC_GEOMETRY_SPHERECOLLECTION_H_
#define SRC_GEOMETRY_SPHERECOLLECTION_H_

#include <mpi.h>
#include <vector>

//#include <iomanip>

#include "Shape.h"
#include "Sphere.h"
#include "Ray.h"

#include "IO/Tecplot/TecplotConsts.h"

namespace MyMPI
{
  class MPIMgr;
}

class FieldData;

namespace Shapes {

  class SphereCollection: public Shape {

      uint64_t flags() const override {
        return RTC_COMPATIBLE | FINITE | WRITABLE | SHAPE_TYPE::SPHERE_COLLECTION; }

    public:

      typedef Sphere DataT;
      typedef std::vector<DataT,Eigen::aligned_allocator<DataT>> DataVecT;

      typedef DataVecT::iterator		DataTItT;
      typedef DataVecT::const_iterator	cDataTItT;

      typedef Eigen::Matrix<double,4,-1> SphereDataT;

    private:

      DataVecT spheres_;

      Eigen::AlignedBox4d bb_;

    public:

      void addSphere( const Eigen::Vector4d& X, const double r) {
        spheres_.emplace_back(X,r,1.0,static_cast<uint32_t>(spheres_.size())); // only external flow spheres
        bb_.extend(spheres_.back().boundingBox());

        //			std::cout << std::scientific << std::setprecision(6) << std::setw(13) << X.transpose() << " " << r;
        //			std::cout << std::endl<<std::endl;
      }

      void addSphere( const Eigen::Vector4d& Xr) {
        addSphere(Eigen::Vector4d(Xr[0],Xr[1],Xr[2],0.0),Xr.coeffRef(3)); }


    public:

      // implicit tecplot writer interface

      // number of variables
      static uint64_t nVars() { return Sphere::nVars(); }

      // value of variable iVar in Element iEl
      double getVar(const uint64_t iEl, const uint64_t iVar) const {
        return spheres_[iEl].getVar(iVar); }

      // name of variable i
      static std::string getVarName(const uint64_t i) {
        return Sphere::getVarName(i); }

    public:

      // array of spheres
      const Shapes::Sphere* spheres() const { return spheres_.data(); }

    public:

      // return pointer to shape element idx
      Shape* element(const uint64_t idx) override { return &(spheres_[idx]); }

      // return const pointer to shape element idx,
      // or shape itself if it only has one (itself)
      const Shape* element(const uint64_t idx) const override { return &(spheres_[idx]); }

      void setGeomID(const uint32_t id) override {
        Shape::setGeomID(id);
        for (uint64_t i = 0; i < nElements(); ++i)
          element(i)->setGeomID(id);
      }

      // number of vertices in shape
      uint64_t nVerts() const override { return nElements(); }

      Tecplot::ZoneType tecplotZoneType() const override { return Tecplot::ZoneType::ORDERED; }

      void writeToTecplot(
          const std::string& boundaryName,
          const std::string& dir,
          const double solTime,
          const uint64_t tn,
          const MyMPI::MPIMgr& mpiMgr ) const override;

    public:

      // return the total surface area of the shape data
      double area() const override;

      // return the bounding box of the shape
      Eigen::AlignedBox4d boundingBox() const override { return bb_; }

    public:

      // vector of variables names necessary for construction
      static std::vector<std::string> requiredVars() {
        return {"X","Y","Z","R"}; }

      SphereCollection(const SphereDataT& sphereData);

      SphereCollection(
          const FieldData* fData,
          const double tol = 1.0e-16,
          const Eigen::Transform<double,4,Eigen::Affine>& tr = Eigen::Transform<double,4,Eigen::Affine>::Identity());

      ~SphereCollection() = default;

      EIGEN_MAKE_ALIGNED_OPERATOR_NEW

  };

} /* namespace Shapes */


#endif /* SRC_GEOMETRY_SPHERECOLLECTION_H_ */
