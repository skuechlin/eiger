/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * Disk.cpp
 *
 *  Created on: Jun 23, 2015
 *      Author: kustepha
 */

#include <set>

#include "Disk.h"

#include "Ray.h"

#include "Settings/Dictionary.h"

#include "ShapeTests.h"

#include "Primitives/MyRandom.h"
#include "Primitives/MyVecArithm.h"
#include "Primitives/MyError.h"


namespace Shapes
{

  // return true if shape intersects axis aligned bounding box bb
  bool
  Disk::test_AABB(const Eigen::AlignedBox4d& bb)
  const
  {
    if (this->boundingBox().intersects(bb))
      {
        const bool test = test_AABB_impl(bb);

//        std::cout << "testing " << bb.min().transpose() << " | " << bb.max().transpose() << "\n"
//            << "vs\n" << this->boundingBox().min().transpose() << " | " << this->boundingBox().max().transpose() << "\n"
//            << X_.transpose() << "\n" << N_.transpose() << "\n" << r_ << "\n result: " << std::to_string(test) << std::endl;

        return test;
      }
    else
      return false;
  }

  // return true if shape intersects axis aligned bounding box bb
  bool
  Disk::test_AABB_impl(const Eigen::AlignedBox4d& bb)
  const
  {

    const double d = X_.dot(N_); // plane offset

//    std::cout << "check -1" << std::endl;

    // first check plane intersection
    if ( !(Shapes::test_AABB_plane(bb,N_,d) ) )
      return 0; // no intersection

//    std::cout << "check 0" << std::endl;

    Eigen::Vector4d A = bb.center();
    Eigen::Vector4d B = A - X_;


    // try early exit by checking extreme case: box center in disk plane with corner
    // oriented towards disk center
    if ( Eigen::Vector4d(B - B.dot(N_)*N_).squaredNorm() // distance in disk plane squared
        > std::pow(r_+ .5*bb.diagonal().norm(),2) )
      return 0;

//    std::cout << "check 1" << std::endl;


    // bb penetrates plane, find intersection points (max 6)
    std::list<Eigen::Vector4d,Eigen::aligned_allocator<Eigen::Vector4d>> pnts;

    // number of intersections found
    const uint64_t npts = intersect_AABB_plane(bb,N_,d,pnts);

    if (npts == uint64_t(-1))
      return 1;

//    std::cout << "check 2" << std::endl;

    if (npts == 1)
      {
        // plane touches corner
        return (X_ - *(pnts.begin()) ).squaredNorm() < r_*r_;
      }

//    std::cout << "check 3" << std::endl;

    // pnts now form a polygon in the same plane as the disk
    // proceed with quasi-2D line-circle tests
    // for each edge, compute closest point on edge to disk center.
    // if <= r, have intersect, and disk intersects the AABB

    const double rsq = r_*r_;
    double denom;
    double t;

    auto pntit = pnts.begin();


    for (uint64_t i = 0; i < npts; ++i)
      {


//        std::cout << "point " << i << " of " << npts << ": " << pntit->transpose() << std::endl;

        auto nextptit = std::next(pntit,1);

        if (nextptit == pnts.end())
          nextptit = pnts.begin(); // last point is beginning of edge with first pnts[ (i+1) % npts ] - pnts[i];

        A = X_ - *pntit;
        B = *nextptit - *pntit;

        t = A.dot(B);

        // ||t|| = ||X-A||*||B-A||*cos(theta) // in code: B is actually B-A, A is X-A

        if ( t <= 0 )
          {
            // pnts[i] is closest
            if ( A.squaredNorm() <= rsq )
              return 1;
          }

//        std::cout << "point " << i <<  " check 4" << std::endl;

        // check projection of X-A onto B-A: ||t*|| = ||X-A||*cos(theta)
        // if it exceeds ||B-A||
        // the following is equivalent
        denom = (B).squaredNorm();
        if ( t >=  denom )
          {
            // pnts[i+1] is closest
            if ( (X_-*nextptit).squaredNorm() <= rsq )
              return 1;
          }

//        std::cout << "point " << i <<  " check 5" << std::endl;

        // closest point is on segment
        // re-use A:
        A = *pntit + (t/denom)*B;

        if ( (X_-A).squaredNorm() <= rsq )
          return 1;

//        std::cout << "point " << i <<  " check 6" << std::endl;

        ++pntit;

      }

    // at this point, if disk center is contained in polygon, then entire disk is contained in box
    // transform all points to 2d

    const Eigen::Vector2d X2D = MyVecArithm::aimRot(X_,N_).segment<2>(1);
    std::vector<Eigen::Vector2d,Eigen::aligned_allocator<Eigen::Vector2d>> pnts2D(pnts.size()+1);
    uint64_t i = 0;
    for (auto&& p : pnts)
      pnts2D[i++] = MyVecArithm::aimRot(p,N_).segment<2>(1);
    pnts2D[i] = pnts2D[0];

    return test_point_in_polygon2D(X2D,pnts2D.data(),pnts.size());

  }



  //	// add geometry to rtc scene
  //	uint
  //	Disk::addToRTCScene(RTCScene scene)
  //	{
  //
  //		uint geomID = rtcNewUserGeometry(scene,1);
  //
  //		rtcSetUserData(scene,geomID,this);
  //
  //		rtcSetBoundsFunction(scene,geomID,(RTCBoundsFunc)&diskBoundsFunc);
  //		rtcSetIntersectFunction(scene,geomID,(RTCIntersectFunc)&diskIntersectFunc);
  //		rtcSetOccludedFunction (scene,geomID,(RTCOccludedFunc )&diskOccludedFunc);
  //
  //		// set geometry ID
  //		setGeomID(geomID);
  //
  //		return geomID;
  //
  //	}


  template<HIT_ACTION action>
  void
  Disk::intersect_impl(RayTracing::Ray *const ray)
  const
  {


    double t( X_.dot(N_) - ray->org().dot(N_) ); 	// compute distance to shape in direction of normal
    double det( ray->dir().dot(N_) );		// velocity in direction of normal

    // check intersection candidate

    // if it lies on line segment (line-plane intersection)
    if ( !test_approx_segment_plane(t,det,ray->tfar) )
      return;

    // if it lies inside circle
    Eigen::Vector4d dx = ray->org() - X_;
    if ( det*det*(dx.squaredNorm() - r_*r_) + 2.*t*det*dx.dot(ray->dir()) + t*t*ray->dir().squaredNorm() > ABSEPS )
      return;


    // we have intersect
    switch (action) {
      case HIT_ACTION::OCCLUDE:
        ray->setOccluded();
        break;
      case HIT_ACTION::INTERSECT:
        ray->setHit(N_,t,det,geomID(),primID());
        break;
    }

    return;
  }


  // uniform area sampling
  // return a random point on the shape
  Eigen::Vector4d
  Disk::sample_unif_area(
      MyRandom::Rng& rndGen)
  const
  {

    const double r = r_*sqrt( rndGen.uniform() );
    const double theta = 2.*Constants::pi*rndGen.uniform();

    Eigen::Vector4d R = {0.,r*sin(theta),r*cos(theta),0.0};

    R = MyVecArithm::aimRot(R,N_);

    return R + X_;

  }

  // uniform radius sampling
  // return a random point on the shape
  Eigen::Vector4d
  Disk::sample_unif_radius(
      MyRandom::Rng& rndGen)
  const
  {

    const double r = r_*rndGen.uniform();
    const double theta = 2.*Constants::pi*rndGen.uniform();

    Eigen::Vector4d R = {0.,r*sin(theta),r*cos(theta),0.0};

    R = MyVecArithm::aimRot(R,N_);

    return R + X_;

  }




  Eigen::AlignedBox4d
  Disk::boundingBox()
  const
  {

    Eigen::Vector4d R = {
        ( N_.y()*N_.y() + N_.z()*N_.z() )*r_*r_,
        ( N_.x()*N_.x() + N_.z()*N_.z() )*r_*r_,
        ( N_.x()*N_.x() + N_.y()*N_.y() )*r_*r_,
        0.0
    };

    R = R.cwiseSqrt();
    R += Eigen::Vector4d(Shapes::BBEPS,Shapes::BBEPS,Shapes::BBEPS,0.0);

    return Eigen::AlignedBox4d(
        X_ - R,
        X_ + R );
  }


  Disk::Disk (
      const Eigen::Vector4d& X, 	// base point
      const Eigen::Vector4d& N, 	// normal
      const double r,	  	 	// radius
      const uint32_t prim_id )
  : Shape(prim_id)
  , X_(X)
  , N_(N.normalized())
  , r_(r)
  {}


  namespace
  {

    Eigen::Vector4d
    getX( const Settings::Dictionary& dict)
    {
      MYASSERT(dict.hasMember("center"),"Disk dictionary missing entry \"center\"");

      Settings::Dictionary cDict = dict.get<Settings::Dictionary>("center");

      MYASSERT(cDict.isArray(),"Disk dictionary entry \"center\" must be array");

      MYASSERT(cDict.size() == 3,"Disk dictionary entry \"center\" must be array of length 3");

      // get the three elements

      Eigen::Vector4d X;
      X.setZero();

      cDict.get(X.data(),0,3);

      return X;
    }

    Eigen::Vector4d
    getN( const Settings::Dictionary& dict)
    {
      MYASSERT(dict.hasMember("normal"),"Disk dictionary missing entry \"normal\"");

      Settings::Dictionary nDict = dict.get<Settings::Dictionary>("normal");

      MYASSERT(nDict.isArray(),"Disk dictionary entry \"normal\" must be array");

      MYASSERT(nDict.size() == 3,"Disk dictionary entry \"normal\" must be array of length 3");

      // get the three elements

      Eigen::Vector4d N;
      N.setZero();

      nDict.get(N.data(),0,3);

      return N.normalized();
    }

    double
    getR( const Settings::Dictionary& dict)
    {
      MYASSERT(dict.hasMember("radius"),"Disk dictionary missing entry \"radius\"");
      return dict.get<double>("radius");
    }
  }


  Disk::Disk (
      const Settings::Dictionary& dict,
      const uint32_t prim_id )
  : Shape(prim_id)
  , X_(getX(dict))
  , N_(getN(dict))
  , r_(getR(dict))
  {
//    	  std::cout << "X: " << X_.transpose() << std::endl;
//    	  std::cout << "N: " << N_.transpose() << std::endl;
//    	  std::cout << "r: " << r_ << std::endl;
  }


} /* namespace Shapes */
