/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * Parallelogram.cpp
 *
 *  Created on: May 5, 2014
 *      Author: Stephan Kuechlin
 */


#include "Ray.h"
#include "Parallelogram.h"

#include "ShapeTests.h"

#include "Settings/Dictionary.h"

#include "Primitives/MyRandom.h"

#include "Primitives/MyError.h"


//
//namespace {
//	void paraBoundsFunc(const Shapes::Parallelogram* para, size_t item, RTCBounds* bounds_o)
//	{
//
//		static_cast<void>(item);
//		Shapes::AABB2RTCBounds(para->boundingBox(),bounds_o);
//	}
//
//	void paraIntersectFunc(const Shapes::Parallelogram* para, RTCRay& ray, size_t item)
//	{
//		static_cast<void>(item);
//		para->intersect(ray);
//	}
//
//
//	void paraOccludedFunc(const Shapes::Parallelogram* para, RTCRay& ray, size_t item)
//	{
//		static_cast<void>(item);
//		para->occluded(ray);
//	}
//
//
//}


namespace Shapes {

  //	// add geometry to rtc scene
  //	uint
  //	Parallelogram::addToRTCScene(RTCScene scene)
  //	{
  //
  //		uint geomID = rtcNewUserGeometry(scene,1);
  //
  //		rtcSetUserData(scene,geomID,this);
  //
  //		rtcSetBoundsFunction(scene,geomID,(RTCBoundsFunc)&paraBoundsFunc);
  //		rtcSetIntersectFunction(scene,geomID,(RTCIntersectFunc)&paraIntersectFunc);
  //		rtcSetOccludedFunction (scene,geomID,(RTCOccludedFunc )&paraOccludedFunc);
  //
  //		// set geometry ID
  //		setGeomID(geomID);
  //
  //		return geomID;
  //
  //	}


  template<HIT_ACTION action>
  void
  Parallelogram::intersect_impl(RayTracing::Ray *const ray)
  const
  {

    // Jiri Havel and Adam Herout
    // Yet Faster Ray-Triangle Intersection (Using SSE4)
    // IEEE Transactions on Visualization and Computer Graphics 2010


    double t(rep_.d_ - ray->org().dot(rep_.N_)); // compute candidate intersection
    double det(ray->dir().dot(rep_.N_));

    // check intersection candidate
    // if it lies on line segment (line-plane intersection)
    if ( !test_approx_segment_plane(t,det,ray->tfar) ) return;

    // check if it lies inside the parallelogram
    Eigen::Vector4d P(det*ray->org() + t*ray->dir());

    double u(P.dot(rep_.N1_) + det*rep_.d1_);
    //		if ( std::signbit(u) != std::signbit(det - u) ) return;
    if ( !gt(u,det,-ABSEPS) || !lt(u,det,1.0+ABSEPS) ) return;

    double v(P.dot(rep_.N2_) + det*rep_.d2_);
    //		if ( std::signbit(v) != std::signbit(det - v) ) return;
    if ( !gt(v,det,-ABSEPS) || !lt(v,det,1.0+ABSEPS) ) return;

    // we have intersect
    switch (action) {
      case HIT_ACTION::OCCLUDE:
        ray->setOccluded();
        break;
      case HIT_ACTION::INTERSECT:
        ray->setHit(rep_.N_,t,det,geomID(),primID());
        break;
    }

    return;
  }



  Eigen::Vector4d
  Parallelogram::sample(
      MyRandom::Rng& rndGen )
  const
  {

    double u(rndGen.uniform());
    double v(rndGen.uniform());

    return rep_.A() + u*(rep_.AB()) + v*(rep_.AC());

    //	return (1.-u-v)*rep_.A_ + u*rep_.B_ + v*rep_.C_;
    // not stable numerically (will yield off-plane value)

  }


} // namespace Shapes

namespace
{
  Eigen::Matrix<double,4,3>
  getVrtsMat(const Settings::Dictionary& dict)
  {

    MYASSERT(dict.hasMember("vertices"),"Parallelogram dictionary missing entry \"vertices\"");

    Settings::Dictionary vrtsDict = dict.get<Settings::Dictionary>("vertices");

    MYASSERT(vrtsDict.isArray(),"Parallelogram dictionary entry \"vertices\" must be array");

    MYASSERT(vrtsDict.size() == 3,"Parallelogram dictionary entry \"vertices\" must be array of length 3");

    // get the three vertices


    Eigen::Matrix<double,4,3> vrts;
    vrts.setZero();

    for (Settings::Dictionary::IndexType i = 0; i < 3; ++i)
      {
        Settings::Dictionary vrtDict = vrtsDict.get<Settings::Dictionary>(i);

        MYASSERT(vrtDict.size() == 3,
                 std::string("Parallelogram \"vertices\" dictionary entry ")
        + std::to_string(i)
        + std::string(" must be array of length 3")
        );

        vrtDict.get(vrts.data()+4*i,0,3);
      }

    return vrts;


  }
}

Shapes::Parallelogram::Parallelogram(
    const Settings::Dictionary& dict,
    const uint32_t prim_id,
    const uint32_t geom_id)
: rep_(getVrtsMat(dict),prim_id,geom_id)
{}

Shapes::Parallelogram::Parallelogram(
    const Eigen::Vector4d& _A,
    const Eigen::Vector4d& _B,
    const Eigen::Vector4d& _C,
    const uint32_t _prim_id,
    const uint32_t _geom_id)
: rep_(_A,_B,_C,_prim_id,_geom_id)
{
  //  std::cout << "constructed parallelogram with\n" <<
  //      "   A: " << this->A().transpose() << "\n" <<
  //      "   B: " << this->B().transpose() << "\n" <<
  //      "   C: " << this->C().transpose() << "\n" <<
  //      "   D: " << this->D().transpose() << "\n" <<
  //      "   N: " << unitNormal(Eigen::Vector4d::Zero()).transpose() << "\n" <<
  //      std::endl;
}


