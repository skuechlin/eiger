/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * FieldData.h
 *
 *  Created on: Jan 8, 2015
 *      Author: kustepha
 */

#ifndef FIELDDATA_FIELDDATA_H_
#define FIELDDATA_FIELDDATA_H_

#include <string>
#include <vector>
#include <valarray>
#include <unordered_map>

#include "Primitives/MyError.h"


class FieldData
{

  const double solTime_;

  const uint64_t nVars_;
  const std::vector<uint64_t> nData_;

  const std::vector<std::string> varNames_;
  std::unordered_map<std::string,uint64_t> varNameIndMap_;

  std::vector<std::valarray<double>> data_;

  uint64_t nVerts_per_element_ = -1;
  std::vector<int32_t> connectivity_;

public:

  double
  solTime()
  const
  {
    return solTime_;
  }

  uint64_t
  nVars()
  const
  {
    return nVars_;
  }

  uint64_t
  getNData(const std::string& varName)
  const
  {
    auto vi = varNameIndMap_.find(varName);
    MYASSERT(vi != varNameIndMap_.end(),"variable \"" + varName + "\" not in data!");
    return nData(vi->second);
  }

  uint64_t
  nData(uint64_t ind = 0)
  const
  {
    return nData_[ind];
  }

  const std::vector<std::string>&
  varNames()
  const
  {
    return varNames_;
  }

  const std::string&
  getVarName(const uint64_t i)
  {
    return varNames_[i];
  }

  bool
  hasVar(const std::string& varName)
  const
  {
    return varNameIndMap_.find(varName)!= varNameIndMap_.end();
  }

  void
  setConnectivity(const uint64_t nElements, const uint64_t nVertsPerElement)
  {
    nVerts_per_element_ = nVertsPerElement;
    connectivity_.resize(nElements*nVertsPerElement);
  }


  uint64_t
  nVertsPerElement() const
  {
    return nVerts_per_element_;
  }

  bool hasConnectivity() const { return nVerts_per_element_ != uint64_t(-1); }

  const std::vector<int32_t>&
  getConnectivity()
  const
  {
    return connectivity_;
  }

  std::vector<int32_t>&
  getConnectivity()
  {
    return connectivity_;
  }

  const double *
  getVarData(const std::string& varName)
  const
  {
    auto vi = varNameIndMap_.find(varName);
    MYASSERT(vi != varNameIndMap_.end(),"variable \"" + varName + "\" not in data!");
    return &(data_[vi->second][0]);
  }

  const double *
  getVarData(const uint64_t vind)
  const
  {
    MYASSERT(vind < nVars_,"variable index \"" + std::to_string(vind) + "\" out of range!");
    return &(data_[vind][0]);
  }

  double *
  getVarData(const uint64_t vind)
  {
    MYASSERT(vind < nVars_,"variable index \"" + std::to_string(vind) + "\" out of range!");
    return &(data_[vind][0]);
  }

  FieldData(
        const double solTime,
        const std::vector<uint64_t>& nData,
        const std::vector<std::string>& varNames)
  : solTime_(solTime)
  , nVars_(varNames.size())
  , nData_(nData)
  , varNames_(varNames)
  {

    MYASSERT(nData.size() == nVars_ ,
        "vector specifying number of data points must have length equal to number of variables");

    // allocate
    data_.resize(nVars_);
    for (uint64_t i = 0; i < nVars_; ++i)
      data_[i].resize(nData[i]);

    // build var name map
    for (uint64_t i = 0; i < nVars_; ++i)
      varNameIndMap_.insert({varNames_[i],i});

  }


};


#endif /* FIELDDATA_FIELDDATA_H_ */
