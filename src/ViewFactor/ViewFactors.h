/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * ViewFactors.h
 *
 *  Created on: Feb 17, 2016
 *      Author: hulfeldl
 */

#ifndef SRC_VIEWFACTOR_VIEWFACTORS_H_
#define SRC_VIEWFACTOR_VIEWFACTORS_H_

#include <vector>
#include <map>
#include <iostream>
#include <string>

#include "IO/Tecplot/TecplotConsts.h"

namespace MyMPI{
  class MPIMgr;
}

namespace Shapes{
  class Shape;
}

namespace Boundary{
  class BoundaryManager;
}

namespace MyRandom{
  class Rng;
}

namespace ViewFactor {


  class ViewFactors {


      typedef std::map<uint64_t,double> InnerT;
      typedef std::vector<InnerT> OuterT;
      typedef std::vector<double> FlatT;

      uint64_t geomID_ = -1;

      uint64_t outer_offset_ = 0;

      OuterT view_factors_;

      FlatT I_;
      FlatT J_;
      FlatT F_;

      uint64_t size_ = 0;

    private:

      double&
      getViewFactor(const uint64_t i, const uint64_t j)
      {
        return view_factors_[i - outer_offset_][j];
      }

      void flatten()
      {

        size_ = 0;

        for (auto outer = view_factors_.begin(); outer != view_factors_.end(); ++outer)
            size_ += outer->size();

        I_.resize(size_);
        J_.resize(size_);
        F_.resize(size_);

        uint64_t idx = 0;
        uint64_t i = outer_offset_;

        for (auto outer = view_factors_.begin(); outer != view_factors_.end(); ++outer, ++i)
          for (auto inner = outer->begin(); inner != outer->end(); ++inner)
            {
              I_[idx] = i;
              J_[idx] = inner->first;
              F_[idx] = inner->second;
              ++idx;
            }

        view_factors_.clear();
        view_factors_ = {};

      }


    public:
      // implicit tecplot ordered write interface

      // number of variables
      static
      uint64_t
      nVars();

      // value of variable iVar of element iEl
      double
      getVar(const uint64_t iEl, const uint64_t iVar)
      const
      {
        switch (iVar) {
          case 0:
            return I_[iEl];
            break;
          case 1:
            return J_[iEl];
            break;
          case 2:
            return F_[iEl];
            break;
          default:
            return 0./0.;
            break;
        }
      }

      // name of variable i
      static
      std::string
      getVarName(const uint64_t iVar);

      // id
      uint64_t id() const { return geomID_; }

    public:
      // number of data
      uint64_t size() const { return size_; }

    public:

      void
      printViewFactors()
      const
      {
        for (uint64_t i = 0; i < size(); ++i)
          std::cout << I_[i] << " " << J_[i] << ": " << F_[i] << std::endl;
      }

      void
      writeViewFactors(
          const std::string& oDir,
          const std::string& boundaryName,
          const MyMPI::MPIMgr& mpiMgr)
      const;

      void
      estimateViewFactors(
          const uint64_t nPoints,
          const uint64_t nRaysPerPoint,
          const Shapes::Shape* sh,
          const Boundary::BoundaryManager* bndMgr,
          MyRandom::Rng& rndGen,
          const MyMPI::MPIMgr& mpiMgr );




      ViewFactors() = default;

  };



} /* namespace ViewFactor */

#endif /* SRC_VIEWFACTOR_VIEWFACTORS_H_ */
