/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * ViewFactors.cpp
 *
 *  Created on: Feb 17, 2016
 *      Author: hulfeldl
 */



#include <string>
#include <sstream>


#include "ViewFactor/ViewFactors.h"

#include "Primitives/MyError.h"

#include "Parallel/MyMPI.h"

#include "IO/Tecplot/Tecplot.h"

namespace ViewFactor {


  // implicit tecplot writer interface

  // number of variables
  uint64_t
  ViewFactors::nVars()
  {
    return 3;
  }

//  // value of variable iVar of element iEl
//  double
//  ViewFactors::getVar(uint64_t iEl, const uint64_t iVar)
//  const
//  {
//
//    typedef ViewFactors::OuterT::const_iterator OuterItT;
//    typedef ViewFactors::InnerT::const_iterator InnerItT;
//
//    // cache iterator position
//    static OuterItT outer( view_factors_.begin() );
//    static uint64_t outer_sz_sum( 0 );
//    static uint64_t last_iEl( 0 );
//
//    if (iEl < last_iEl)
//      {// reset
//        outer = view_factors_.begin();
//        outer_sz_sum = 0;
//        last_iEl = iEl;
//      }
//    else
//      {// continue from previous position
//        last_iEl = iEl;
//        iEl -= outer_sz_sum;
//      }
//
//    uint64_t outer_sz;
//
////    while ( iEl >= (outer_sz = outer->second.size()) )
//    while ( iEl >= (outer_sz = outer->size()) )
//      {
//        iEl -= outer_sz;
//        outer_sz_sum += outer_sz;
//        ++outer;
//      }
//
//    if (iVar == 0)
//      return std::distance( view_factors_.begin(), outer) + outer_offset_;
//
////    if (iVar == 0)
////      return outer->first;
//
////    InnerItT inner( outer->second.begin() );
//    InnerItT inner = std::next( outer->begin(), iEl );
//
//
//    switch (iVar) {
//      case  1 : return inner->first; break;
//      case  2 : return inner->second; break;
//      default: return 0./0.;
//    }
//  }

  // name of variable i
  std::string
  ViewFactors::getVarName(const uint64_t iVar)
  {
    switch (iVar) {
      case 0 : return std::string("I");     break;
      case 1 : return std::string("J"); break;
      case 2 : return std::string("F"); break;
      default: return std::string();
    }
  }

  void
  ViewFactors::writeViewFactors(
      const std::string& oDir,
      const std::string& boundaryName,
      const MyMPI::MPIMgr& mpiMgr)
  const
  {

    // make sure all mpi procs are involved and are writing information about the same geometry
    std::vector<uint64_t> geomids(mpiMgr.nRanks(),0);
    MPI_Gather(&geomID_,1,MPI_UINT64_T,geomids.data(),1,MPI_UINT64_T,mpiMgr.root(),mpiMgr.comm());

    if (mpiMgr.isRoot())
      {
        MYASSERT( std::all_of( geomids.begin(), geomids.end(), [&geomids](uint64_t i)->bool{return i == *(geomids.begin());} ),
                  "trying to write view factors of different geometries to same file!");
      }

    // file name
    std::string fname = boundaryName;


    // prepend directory
    fname = oDir + std::string("boundary_") + fname;

    // append solution time
    std::stringstream fnamestr;


    fnamestr.str(std::string(""));
    fnamestr << fname << "_viewfactors";

    fname = fnamestr.str();

    std::string varNames = getVarName(0);
    for (uint64_t i = 1; i < nVars(); ++i)
      varNames += (" " + getVarName(i));


    const uint64_t sz = size();




    //DEBUG:
//        std::cout << mpiMgr.rank() << " will write " << std::to_string(sz) << " viewfactors, average of "
//            << std::to_string(
//            static_cast<double>(sz)/static_cast<double>(view_factors_.size()) )
//             << " elements visible per element" << std::endl;

//    void write1DData2Tecplot(
//        const std::function<double(const int32_t iDat, const int32_t iVar)>& dataFun,
//        const std::function<int32_t(const int32_t iElem, const int32_t iVrt)>& connFun,
//        const std::function<int32_t(std::vector<int32_t>& buffer, const int32_t iElem)>& fnConnFun,
//        const ZoneType zoneType,
//        const VarLoc varLoc,
//        const DataSharing dataSharing,
//        const FileType fileType,
//        const int32_t nVars,
//        const int32_t nPoints,
//        const int32_t nElements,
//        const std::string& fileName,
//        const std::string& dataSetName,
//        const std::string& zoneName,
//        const std::string& varNames,
//        const int32_t data_id,
//        const double solTime,
//        const bool writeConnectivity,
//        const bool writeFaceNeighborConnectivity,
//        const MyMPI::MPIMgr& mpiMgr );

    // write
    Tecplot::write1DData2Tecplot(
        [this](const int32_t iDat, const int32_t iVar)->double{return getVar(iDat,iVar);},
        Tecplot::NO_CONNECTIVITY,
        Tecplot::NO_FACENEIGHBORS,
        Tecplot::ZoneType::ORDERED,
        std::vector<Tecplot::VarLoc>(nVars(),Tecplot::VarLoc::NodeCentered),
        Tecplot::DataSharing::DISTRIBUTED,
        Tecplot::FileType::FullFile,
        nVars(),
        sz,
        sz,
        fname,
        boundaryName,
        boundaryName,
        varNames,
        id(),
        0.0, // solTime
        false,
        false,
        mpiMgr);

  }



} /* namespace ViewFactor */
