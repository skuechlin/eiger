/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * ComputeViewFactors.cpp
 *
 *  Created on: Mar 3, 2016
 *      Author: kustepha
 */


#include <omp.h>

#include <Eigen/Dense>

#include <ViewFactor/ViewFactors.h>

#include "Parallel/MyMPI.h"

#include "Boundary/BoundaryManager/BoundaryManager.h"

#include "Geometry/Shape.h"

#include "Primitives/MyRandom.h"
#include "Primitives/MyArithm.h"
#include "Primitives/MyVecArithm.h"
#include "Primitives/Partitioning.h"

#include "Primitives/NormalDistribution.h"

#include "Primitives/Progress.h"

#include "Particles/ParticleCollection.h"


namespace
{

  Eigen::Vector4d
  UniformSampleHemisphere(MyRandom::Rng& rng)
  {
    const MyRandom::NormalDistribution sn;

    Eigen::Vector4d ret(fabs(sn(rng)),sn(rng),sn(rng),0.0);

    //    if (ret[0] < 0.)
    //      ret *= -1.;

    return ret.normalized();
  }

  Eigen::Vector4d
  sampleDirection(const Eigen::Vector4d& N,MyRandom::Rng& rndGen)
  {
    return MyVecArithm::aimRot(
        UniformSampleHemisphere(rndGen) ,
        N);
  }
}


namespace ViewFactor {


  void
  ViewFactors::estimateViewFactors(
      const uint64_t nPoints,
      const uint64_t nRaysPerPoint,
      const Shapes::Shape* sh,
      const Boundary::BoundaryManager* bndMgr,
      MyRandom::Rng& rndGenParent,
      const MyMPI::MPIMgr& mpiMgr )
  {

    //    std::cout << mpiMgr.id() << " entered computeViewFactors " << std::endl;


    // set geometry id for this view factors object
    geomID_ = sh->geomID();

    // reset the view factors
    view_factors_.clear();




    if ( mpiMgr.rank() >= sh->nElements() )
      {
        // this proc won't take part in computation
        return;
      }

    // partition shape elements over mpi procs
    uint64_t begin = MyPartitioning::partitionStart(mpiMgr.rank(),sh->nElements(),mpiMgr.nRanks());
    uint64_t end = begin + MyPartitioning::partitionLength(mpiMgr.rank(),sh->nElements(),mpiMgr.nRanks());

    //        std::cout << mpiMgr.rank() << " will process shape elements "
    //        	<< begin << " up to (exkl) " << end << " of total " << sh->nElements() << std::endl;


    view_factors_.resize( end - begin ); // allocate

    outer_offset_ = begin;


    if (mpiMgr.isRoot()) printProgress(-1.0);

    // begin parallel region
#pragma omp parallel
    {

      // each thread gets own particle collection object
      Particles::ParticleCollection prts;
      prts.resize(nPoints,Particles::ParticleCollection::ResizeMode::ALL); // dont keep any data

      // each thread gets own child random number generator
      MyRandom::Rng rndGen(rndGenParent);



      // for all boundary shapes in this MPI proc's range
#pragma omp for schedule(runtime)
      for ( uint64_t i = begin; i < end; ++i)
        {

          // get shape element pointer
          const Shapes::Shape* el = sh->element(i);


          MYASSERT(el->sampleable(),"viewfactor computation requires shape element to be sampleable!");


          // count number of points inside
          uint64_t npts = nPoints;

          // for each sampling point on shape
          v4df r;
          const uint64_t max_tries = 100; // max number of trials to find point on shape inside domain
          for ( Particles::Particle* p = prts.first(); p != prts.last(); ++p)
            {
              // sample point location
              uint64_t tries = 0;
              do{ r = from_eigen(el->sample(rndGen)); } while(bndMgr->isOutside(r) && tries++ < max_tries);

              p->R = r;
              p->dt = 1.0;

              if (!(tries < max_tries))
                {
                  Particles::mark_remove(p);
                  --npts;
                }
            }

//          // mark points (particles sampled outside flow volume
//          bndMgr->markOutside(prts,0,nPoints);

          // count points inside

//          for ( uint64_t p = 0; p < nPoints; ++p)
//            npts -= !!(prts.isMarkedRemove(p));

          // viewfactor increment per hit
          const double inc = 1./static_cast<double>(nRaysPerPoint*npts);

          const double dtinf = std::numeric_limits<double>::infinity();

          // for each sampling point on shape
          for ( Particles::Particle* p = prts.first(); p != prts.last(); ++p)
            {
              // skip particles outside flow volume
              if ( __builtin_expect(Particles::is_marked_remove_tf(p),false) ) continue;


              Eigen::Vector4d d; // direction
              const Eigen::Vector4d o = to_eigen(p->R); // origin

              // normal vector to shape at point location
              const Eigen::Vector4d N = el->unitNormal(o);

              // for each ray direction
              for ( uint64_t r = 0; r < nRaysPerPoint; ++r)
                {

                  // sample random direction
                  d = sampleDirection(N,rndGen);


                  // find intersect
                  uint64_t geomID;
                  uint64_t shapeID;
                  if ( bndMgr->findBoundaryIntersection(
                      o.data(),d.data(),&dtinf,geomID,shapeID)
                      && geomID == sh->geomID() )
                    getViewFactor(i,shapeID) += inc;

                }

            }

          if (mpiMgr.isRoot() && (rndGen.uniform() < .25) )
            printProgress( static_cast<double>(i - begin) / static_cast<double>(end-begin) );



        } // parallel for all shape elements


    } // end parallel region


    if (mpiMgr.isRoot())
    {
        printProgress(1.0);
        printProgress(2.0);
    }

    //    for (uint64_t r = 0; r < mpiMgr.nRanks(); ++r)
    //      {
    //        if (r == mpiMgr.rank())
    //          {
    //            std::cout << r << std::endl;
    //            printViewFactors();
    //            std::cout << std::endl;
    //          }
    //        MyMPI::barrier();
    //      }

    flatten();

  } // compute view factors



}
