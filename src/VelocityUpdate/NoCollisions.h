/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * NoCollisions.h
 *
 *  Created on: May 28, 2014
 *      Author: Stephan Kuechlin
 */

#ifndef NOCOLLISIONS_H_
#define NOCOLLISIONS_H_

#include "VelocityUpdateStrategy.h"

namespace VelocityUpdate
{

  class NoCollisions: public Strategy {

  public:

    CollisionOperatorStatistics updateVelocities(
        Particles::Particle*,
        Particles::Particle*,
        CellData&,
        const double,
        const Gas::GasModels::GasModel* const,
        MyRandom::Rng&
    ) const override
    { return {};   }

  };

}

#endif /* NOCOLLISIONS_H_ */
