/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * BGK.h
 *
 *  Created on: May 9, 2019
 *      Author: kustepha
 */

#ifndef SRC_VELOCITYUPDATE_BGK_H_
#define SRC_VELOCITYUPDATE_BGK_H_

#include <VelocityUpdate/VelocityUpdateStrategy.h>

#include "Gas/GasModel.h"
#include "Gas/PDF/Maxwellian.h"

#include "Grid/CellData.h"

#include "Primitives/MyIntrin.h"
#include "Primitives/MyRandom.h"
#include "Primitives/NormalDistribution.h"

namespace VelocityUpdate
{

  class BGK : public Strategy
  {
  public:

    CollisionOperatorStatistics updateVelocities(
        Particles::Particle* pfirst,
        Particles::Particle* plast,
        CellData& c,
        const double dt, // time step in s
        const Gas::GasModels::GasModel* const gas,
        MyRandom::Rng& rng
    ) const override {


      const uint64_t num_particles = std::distance(pfirst,plast);
      const double num_particlesd = static_cast<double>(num_particles);

      std::uniform_int_distribution<uint64_t> u(0,num_particles-1); // inclusive range 0 ... num_particles-1

      const double T = fmax(c.T(),5.0);

      const double coll_freq = c.p()/gas->mu(T);

      const double num_colld = num_particlesd*( -expm1(-coll_freq*dt) ) + c.coll_carry_;
      const double num_colld_floor = floor(num_colld); // truncate
      // set carry to remainder
      c.coll_carry_ = num_colld - num_colld_floor;

      const uint64_t num_coll = static_cast<uint64_t>(num_colld_floor);

      // target distribution object - Maxwellian for standard BGK
      const Gas::Maxwellian td(gas, to_eigen(c.v()), T, c.Trot(), c.Tvib());


      for (uint64_t i = 0; i < num_coll; ++i) {

          // choose particle randomly
          Particles::Particle* p = pfirst + u(rng);

          Particles::set_velocity( p, from_eigen(td.sampleVelocity(rng)) );

      }


      CollisionOperatorStatistics cos;
      return cos;

    }

  };

} /* namespace VelocityUpdate */

#endif /* SRC_VELOCITYUPDATE_BGK_H_ */
