/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * DSMCLarsenBorgnakke.h
 *
 *  Created on: May 12, 2015
 *      Author: kustepha
 */

#ifndef VELOCITYUPDATE_DSMC_H_
#define VELOCITYUPDATE_DSMC_H_

#include <VelocityUpdate/VelocityUpdateStrategy.h>

namespace VelocityUpdate
{

  class DSMC : public Strategy
  {

  protected:
    uint64_t select_and_collide(
        Particles::Particle* pfirst,
        Particles::Particle* plast,
        const uint64_t npairs,
        const Gas::GasModels::GasModel* const gasModel,
        MyRandom::Rng& rndGen,
        double& sigmaCr_max,
        const double p_redist ) const;

  public:

    CollisionOperatorStatistics updateVelocities(
        Particles::Particle* pfirst,
        Particles::Particle* plast,
        CellData& c,
        const double dt,
        const Gas::GasModels::GasModel* const gasModel,
        MyRandom::Rng& rndGen ) const override;

  };

} /* namespace VelocityUpdate */

#endif /* VELOCITYUPDATE_DSMC_H_ */
