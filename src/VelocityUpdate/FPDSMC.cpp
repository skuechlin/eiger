/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * FPDSMC.cpp
 *
 *  Created on: May 26, 2015
 *      Author: kustepha
 */

#include <Grid/CellData.h>
#include <VelocityUpdate/FPDSMC.h>
#include "Gas/GasModel.h"

namespace VelocityUpdate
{


  CollisionOperatorStatistics
  FPDSMC::updateVelocities(
      Particles::Particle* pfirst,
      Particles::Particle* plast,
      CellData& c,
      const double dt,
      const GasModel* const gasModel,
      MyRandom::Rng& rndGen ) const
  {

    if (pfirst==plast)
      return {};

    const uint64_t N = std::distance(pfirst,plast);

    // estimate number of collisions per comp. particles
    const double ncoll = 0.5*static_cast<double>(N-1)*(c.n()/c.Np()) * (gasModel->sigmaCrMax()) * dt;

//
//    double T = c.T();
//
//    double ncoll = 0.5
//        * gasModel.sigmaCr( .5*gasModel.m(), pow( gasModel.crAv(T), 2. ) )
//        * c.n()
//        * dt;

    if (ncoll > invKnCrit_)
      {
        return fp_->updateVelocities(pfirst,plast,c,dt,gasModel,rndGen);
      }
    else
      return dsmc_->updateVelocities(pfirst,plast,c,dt,gasModel,rndGen);


  }


  FPDSMC::FPDSMC (
      std::unique_ptr<Strategy> dsmc,
      std::unique_ptr<Strategy> fp,
      double invKnCrit)
  : dsmc_(std::move(dsmc))
  , fp_(std::move(fp))
  , invKnCrit_(invKnCrit)
  {}

} /* namespace VelocityUpdate */
