/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * VelocityUpdateStrategyFactory.cpp
 *
 *  Created on: Sep 17, 2014
 *      Author: kustepha
 */


#include <map>
#include <string>

#include "Primitives/MyError.h"
#include "Primitives/MyString.h"
#include "Settings/Dictionary.h"

#include "FokkerPlanck/FokkerPlanck.h"
#include "FPDSMC.h"
#include "DSMC.h"
#include "DSMCMF.h"
#include "NoCollisions.h"
#include "VelocityUpdateStrategy.h"
#include "FokkerPlanckStandalone.h"
#include "BGK.h"

namespace VelocityUpdate
{

  enum Algorithm
  {
    NONE,
    FP,
    DSMC,
    DSMCMF,
    FPDSMC,
    FP_STANDALONE,
    BGK
  };

  static std::map<std::string,Algorithm> AlgorithmNames =
  {
      {"collisionless",NONE},
      {"fokker-planck",FP},
      {"fp",FP},
      {"dsmc",DSMC},
      {"dsmc-mf",DSMCMF},
      {"hybrid",FPDSMC},
      {"fp-dsmc",FPDSMC},
      {"fp-standalone",FP_STANDALONE},
      {"bgk",BGK}
  };


  std::unique_ptr<Strategy>
  makeStrategy(const Settings::Dictionary& dict)
  {


    std::unique_ptr<Strategy> p;

    MYASSERT(dict.hasMember("type"),std::string("velocity update strategy dictionary missing entry \"type\""));

    std::string type = dict.get<std::string>("type");
    MyString::toLower(type);

    auto algit = AlgorithmNames.find(type);

    MYASSERT(algit != AlgorithmNames.end(),
             std::string("unknown velocity update strategy type \"") + type + std::string("\"") );

    switch (algit->second)
    {
      case NONE:
        p = std::unique_ptr<Strategy>(new class NoCollisions);
        break;
      case FP:
        p = std::unique_ptr<Strategy>(new class FokkerPlanck(dict));
        break;
      case DSMC:
        p = std::unique_ptr<Strategy>(new class DSMC);
        break;
      case DSMCMF:
        p = std::unique_ptr<Strategy>(new class DSMCMF);
        break;
      case FPDSMC:
        p = std::unique_ptr<Strategy>(new class FPDSMC(
            makeStrategy(dict.get<Settings::Dictionary>("DSMC")),
            makeStrategy(dict.get<Settings::Dictionary>("FOKKER-PLANCK")),
            1./( dict.get<double>("Kn_crit",1.0) ) ) );
        break;
      case FP_STANDALONE:
        p = std::make_unique<class FokkerPlanck_Standalone>();
        break;
      case BGK:
        p = std::make_unique<class BGK>();
        break;
      default:
        MYASSERT(false,"unknown velocity update strategy enum");
        break;
    }

    return p;

  }



}
