/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * CubicLegacySystem.h
 *
 *  Created on: Dec 5, 2018
 *      Author: kustepha
 */

#ifndef SRC_VELOCITYUPDATE_FOKKERPLANCK_CUBICLEGACYSYSTEM_H_
#define SRC_VELOCITYUPDATE_FOKKERPLANCK_CUBICLEGACYSYSTEM_H_

#include <Eigen/Dense>

#include "Grid/CellData.h"
#include "Gas/GasModel.h"

namespace VelocityUpdate {

  namespace CubicLegacyCoefficients {

    inline void set_lhs(const CellData& c, Eigen::Matrix<double,9,9>& A) {

      A( 0, 0 ) = 2.0*c.u0<0,0>();
      A( 1, 0 ) = c.u0<0,1>();
      A( 2, 0 ) = c.u0<0,2>();
      A( 3, 0 ) = 0;
      A( 4, 0 ) = 0;
      A( 5, 0 ) = 0;
      A( 6, 0 ) = c.u2<0>() + 2.0*c.u0<0,0,0>();
      A( 7, 0 ) = 2.0*c.u0<0,0,1>();
      A( 8, 0 ) = 2.0*c.u0<0,0,2>();

      A( 0, 1 ) = 2.0*c.u0<0,1>();
      A( 1, 1 ) = c.u0<0,0>() + c.u0<1,1>();
      A( 2, 1 ) = c.u0<1,2>();
      A( 3, 1 ) = 2.0*c.u0<0,1>();
      A( 4, 1 ) = c.u0<0,2>();
      A( 5, 1 ) = 0;
      A( 6, 1 ) = c.u2<1>() + 4.0*c.u0<0,0,1>();
      A( 7, 1 ) = c.u2<0>() + 4.0*c.u0<0,1,1>();
      A( 8, 1 ) = 4.0*c.u0<0,1,2>();

      A( 0, 2 ) = 2.0*c.u0<0,2>();
      A( 1, 2 ) = c.u0<1,2>();
      A( 2, 2 ) = c.u0<0,0>() + c.u0<2,2>();
      A( 3, 2 ) = 0;
      A( 4, 2 ) = c.u0<0,1>();
      A( 5, 2 ) = 2.0*c.u0<0,2>();
      A( 6, 2 ) = c.u2<2>() + 4.0*c.u0<0,0,2>();
      A( 7, 2 ) = 4.0*c.u0<0,1,2>();
      A( 8, 2 ) = c.u2<0>() + 4.0*c.u0<0,2,2>();

      A( 0, 3 ) = 0;
      A( 1, 3 ) = c.u0<0,1>();
      A( 2, 3 ) = 0;
      A( 3, 3 ) = 2.0*c.u0<1,1>();
      A( 4, 3 ) = c.u0<1,2>();
      A( 5, 3 ) = 0;
      A( 6, 3 ) = 2.0*c.u0<0,1,1>();
      A( 7, 3 ) = c.u2<1>() + 2.0*c.u0<1,1,1>();
      A( 8, 3 ) = 2.0*c.u0<1,1,2>();

      A( 0, 4 ) = 0;
      A( 1, 4 ) = c.u0<0,2>();
      A( 2, 4 ) = c.u0<0,1>();
      A( 3, 4 ) = 2.0*c.u0<1,2>();
      A( 4, 4 ) = c.u0<1,1>() + c.u0<2,2>();
      A( 5, 4 ) = 2.0*c.u0<1,2>();
      A( 6, 4 ) = 4.0*c.u0<0,1,2>();
      A( 7, 4 ) = c.u2<2>() + 4.0*c.u0<1,1,2>();
      A( 8, 4 ) = c.u2<1>() + 4.0*c.u0<1,2,2>();

      A( 0, 5 ) = 0;
      A( 1, 5 ) = 0;
      A( 2, 5 ) = c.u0<0,2>();
      A( 3, 5 ) = 0;
      A( 4, 5 ) = c.u0<1,2>();
      A( 5, 5 ) = 2.0*c.u0<2,2>();
      A( 6, 5 ) = 2.0*c.u0<0,2,2>();
      A( 7, 5 ) = 2.0*c.u0<1,2,2>();
      A( 8, 5 ) = c.u2<2>() + 2.0*c.u0<2,2,2>();

      A( 0, 6 ) = 2.0*c.u2<0>();
      A( 1, 6 ) = c.u2<1>();
      A( 2, 6 ) = c.u2<2>();
      A( 3, 6 ) = 0;
      A( 4, 6 ) = 0;
      A( 5, 6 ) = 0;
      A( 6, 6 ) = -c.u2()*c.u2() + 2.0*(c.u2<0,0>() - c.u0<0,0>()*c.u2()) + c.u4();
      A( 7, 6 ) = 2.0*(c.u2<0,1>() - c.u0<0,1>()*c.u2());
      A( 8, 6 ) = 2.0*(c.u2<0,2>() - c.u0<0,2>()*c.u2());

      A( 0, 7 ) = 0;
      A( 1, 7 ) = c.u2<0>();
      A( 2, 7 ) = 0;
      A( 3, 7 ) = 2.0*c.u2<1>();
      A( 4, 7 ) = c.u2<2>();
      A( 5, 7 ) = 0;
      A( 6, 7 ) = 2.0*(c.u2<0,1>() - c.u0<0,1>()*c.u2());
      A( 7, 7 ) = -c.u2()*c.u2() + 2.0*(c.u2<1,1>() - c.u0<1,1>()*c.u2()) + c.u4();
      A( 8, 7 ) = 2.0*(c.u2<1,2>() - c.u0<1,2>()*c.u2());

      A( 0, 8 ) = 0;
      A( 1, 8 ) = 0;
      A( 2, 8 ) = c.u2<0>();
      A( 3, 8 ) = 0;
      A( 4, 8 ) = c.u2<1>();
      A( 5, 8 ) = 2.0*c.u2<2>();
      A( 6, 8 ) = 2.0*(c.u2<0,2>() - c.u0<0,2>()*c.u2());
      A( 7, 8 ) = 2.0*(c.u2<1,2>() - c.u0<1,2>()*c.u2());
      A( 8, 8 ) = -c.u2()*c.u2() + 2.0*(c.u2<2,2>() - c.u0<2,2>()*c.u2()) + c.u4();

    }

    inline void set_rhs(
        const double d2,
        const double lambda,
        const CellData& c,
        const GasModel* const gas,
        const GasModel::Approx approx,
        Eigen::Matrix<double,9,1>& B) {


      B( 0 ) = gas->P0<0,0>(approx,c) - 2.0*lambda*c.u2<0,0>() - d2;
      B( 1 ) = gas->P0<0,1>(approx,c) - 2.0*lambda*c.u2<0,1>();
      B( 2 ) = gas->P0<0,2>(approx,c) - 2.0*lambda*c.u2<0,2>();
      B( 3 ) = gas->P0<1,1>(approx,c) - 2.0*lambda*c.u2<1,1>() - d2;
      B( 4 ) = gas->P0<1,2>(approx,c) - 2.0*lambda*c.u2<1,2>();
      B( 5 ) = gas->P0<2,2>(approx,c) - 2.0*lambda*c.u2<2,2>() - d2;
      B( 6 ) = gas->P2<0>(approx,c) - lambda*(3.0*c.u4<0>() - 2.0*(c.u2<0>()*c.u0<0,0>() + c.u2<1>()*c.u0<0,1>() + c.u2<2>()*c.u0<0,2>()) - c.u2<0>()*c.u2());
      B( 7 ) = gas->P2<1>(approx,c) - lambda*(3.0*c.u4<1>() - 2.0*(c.u2<0>()*c.u0<0,1>() + c.u2<1>()*c.u0<1,1>() + c.u2<2>()*c.u0<1,2>()) - c.u2<1>()*c.u2());
      B( 8 ) = gas->P2<2>(approx,c) - lambda*(3.0*c.u4<2>() - 2.0*(c.u2<0>()*c.u0<0,2>() + c.u2<1>()*c.u0<1,2>() + c.u2<2>()*c.u0<2,2>()) - c.u2<2>()*c.u2());
    }

  }


  void
  FokkerPlanck::cubic_legacy_coefficients(
      Eigen::Matrix<double,16,1>& X,
      const CellData& c,
      const GasModel* const gas,
      const GasModel::Approx approx ) {

    // stress tensor relaxation rate
    const double nur = c.p()/c.mu();

    // FP frequency
    const double nu = .5*nur;

    // internal inverse relaxation rates
    const double nu_rot = nur*(gas->zRotInv(c.T()))*(4./M_PI);
    const double nu_vib = nur*(gas->zVibInv(c.T()))*(4./M_PI);

    // diffusion coefficients

    const double d2_tr = c.theta()*nur;

    const double d2 = d2_tr + (
        (2./3.)*nu_rot*(c.u_rot() - gas->esEqRot(c.T())) +
        (2./3.)*nu_vib*(c.u_vib() - gas->esEqVib(c.T())) );
    const double d2_rot = nu_rot*(gas->esEqRot(c.T()));
    const double d2_vib = nu_vib*(gas->esEqVib(c.T()));

    // coefficient for cubic term
    //    lambda = -1./(alpha*rho^3)*abs(det(sigma_ij))
    //    alpha  = tau*(3*theta)^4
    // -> lambda = - nu * rho^-3 * (3*theta)^-4 * abs(det(sigma_ij))
    // det of symm matrix:
    // | aa  *  * |
    // | bb dd  * | = aa(dd*ff-ee*ee)+bb*(cc*ee-bb*ff)+cc*(bb*ee-dd*cc)
    // | cc ee ff |

    const double aa = c.sigma<0,0>();
    const double bb = c.sigma<0,1>();
    const double cc = c.sigma<0,2>();
    const double dd = c.sigma<1,1>();
    const double ee = c.sigma<1,2>();
    const double ff = c.sigma<2,2>();

    const double lambda = - nu * pow(c.rho(),-3.0) * pow(3.*c.theta(),-4.0) * fabs(
        aa*(dd*ff-ee*ee) + bb*(cc*ee-bb*ff) + cc*(bb*ee-dd*cc) );

    // linear system
    Eigen::Matrix<double,9,9> AA;
    Eigen::Matrix<double,9,1> B;

    // set left hand side
    CubicLegacyCoefficients::set_lhs(c, AA);

    // set right hand side
    CubicLegacyCoefficients::set_rhs(d2_tr, lambda, c, gas, approx, B);

    // solve for coefficients
    X.head<9>() = AA.partialPivLu().solve(B);
    X[ 9] = lambda;

    X[10] = nu;
    X[11] = nu_rot;
    X[12] = nu_vib;

    X[13] = d2;
    X[14] = d2_rot;
    X[15] = d2_vib;

  }


}


#endif /* SRC_VELOCITYUPDATE_FOKKERPLANCK_CUBICLEGACYSYSTEM_H_ */
