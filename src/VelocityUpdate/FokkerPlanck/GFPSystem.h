/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * GFPSystem.h
 *
 *  Created on: Jan 3, 2019
 *      Author: kustepha
 */

#ifndef SRC_VELOCITYUPDATE_FOKKERPLANCK_GFPSYSTEM_H_
#define SRC_VELOCITYUPDATE_FOKKERPLANCK_GFPSYSTEM_H_

#include <iostream>

#include <Eigen/Dense>

#include "Grid/CellData.h"
#include "Gas/GasModel.h"


namespace VelocityUpdate {

  namespace GFPCoefficients {

    template<int N>
    inline void set_sigma(const CellData& c, Eigen::Matrix<double,N-4,N-4>& Sigma);

    template<int N>
    inline void set_p(
        const CellData& c,
        const GasModel* const gas,
        const GasModel::Approx approx,
        Eigen::Ref<Eigen::Matrix<double,N-4,1>> P);

    template<int N>
    inline void set_lh(
        const CellData& c,
        Eigen::Ref<Eigen::Matrix<double,N-4,1>> LH);

    template<int N>
    inline void set_ghnl(
        const v4df& V,
        const CellData& c,
        Eigen::Ref<Eigen::Matrix<double,4,N-10>> GH);

    template<int N>
    inline void set_ghl(
        const v4df& V,
        Eigen::Ref<Eigen::Matrix<double,4,6>> GH) {

      GH(  0,  0 ) = V[0];
      GH(  1,  0 ) = 0;
      GH(  2,  0 ) = 0;
      GH(  3,  0 ) = 0;

      GH(  0,  1 ) = 0;
      GH(  1,  1 ) = V[1];
      GH(  2,  1 ) = 0;
      GH(  3,  1 ) = 0;

      GH(  0,  2 ) = 0;
      GH(  1,  2 ) = 0;
      GH(  2,  2 ) = V[2];
      GH(  3,  2 ) = 0;

      GH(  0,  3 ) = V[1];
      GH(  1,  3 ) = V[0];
      GH(  2,  3 ) = 0;
      GH(  3,  3 ) = 0;

      GH(  0,  4 ) = V[2];
      GH(  1,  4 ) = 0;
      GH(  2,  4 ) = V[0];
      GH(  3,  4 ) = 0;

      GH(  0,  5 ) = 0;
      GH(  1,  5 ) = V[2];
      GH(  2,  5 ) = V[1];
      GH(  3,  5 ) = 0;
    }

  }
}

#include "GFP13System.h"
#include "GFP14System.h"
#include "GFP20System.h"
#include "GFP21System.h"
#include "GFP26System.h"


namespace VelocityUpdate {


  template<int N>
  void FokkerPlanck::generalized_coefficients(
      Eigen::Matrix<double,N-4+6,1>& X,
      const CellData& c,
      const GasModel* const gas,
      const GasModel::Approx approx) {

    constexpr int sz = N-4;

    // linear system
    Eigen::Matrix<double,sz,sz> Sigma;
    Eigen::Matrix<double,sz,2> PLH;
    Eigen::Matrix<double,sz,1> LH;

    // set covariance matrix Sigma
    GFPCoefficients::set_sigma<N>(c, Sigma);

    // set (specific) production terms P
    GFPCoefficients::set_p<N>(c, gas, approx, PLH.col(0));

    const double T = fmax(c.T(),5.0);

    // add contributions by internal modes to diagonal of ((1/2)*) stress tensor production
    // equal contribution---(1./3.) of total---for each direction

    // stress tensor inverse relaxation rate
    const double nur = c.p()/gas->mu(T);

    // internal inverse relaxation rates
    const double nu_rot = nur*(gas->zRotInv(T))*(4./M_PI);
    const double nu_vib = nur*(gas->zVibInv(T))*(4./M_PI);

    PLH.col(0).template head<3>() += Eigen::Vector3d::Constant( (1./3.)*(
        nu_rot*( c.u_rot() - gas->esEqRot(T) ) +
        nu_vib*( c.u_vib() - gas->esEqVib(T) ) ));

    // set average Laplacian of moments
    GFPCoefficients::set_lh<N>(c, PLH.col(1));
    LH = PLH.col(1).eval();

    // decompose Sigma (inplace)
    const Eigen::LDLT< Eigen::Ref<decltype(Sigma)>, Eigen::Lower > SigmaLDLT( Sigma );

    // solve
    SigmaLDLT.solveInPlace(PLH);

    // FP frequency
    const double nu_num = 2.*LH.dot(PLH.col(0));
    const double nu_den = c.theta() * LH.dot(PLH.col(1)) - 3.;
    const double nu     = fabs( nu_num / nu_den );

    // diffusion coefficients
    const double d2_tr  = c.theta()*nu;
    const double d2_rot = nu_rot*(gas->esEqRot(c.T()));
    const double d2_vib = nu_vib*(gas->esEqVib(c.T()));

    // coefficients are a function of diffusion
    X.head(sz) = PLH.col(0) - .5*d2_tr*PLH.col(1);

    X.tail(6) << nu,nu_rot,nu_vib,d2_tr,d2_rot,d2_vib;

  }


} // namespace VelocityUpdate


#endif /* SRC_VELOCITYUPDATE_FOKKERPLANCK_GFPSYSTEM_H_ */
