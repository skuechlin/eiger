/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * CubicSystem.h
 *
 *  Created on: Dec 5, 2018
 *      Author: kustepha
 */

#ifndef SRC_VELOCITYUPDATE_FOKKERPLANCK_CUBICSYSTEM_H_
#define SRC_VELOCITYUPDATE_FOKKERPLANCK_CUBICSYSTEM_H_

#include <Eigen/Dense>

#include "Grid/CellData.h"
#include "Gas/GasModel.h"

#include "FokkerPlanck.h"

namespace VelocityUpdate {

  namespace CubicCoefficients {

    inline constexpr double Power(const double x, const int p) {
      return __builtin_powi(x,p); }


    template<class Moments>
    inline void set_lhs(const Moments& c, Eigen::Matrix<double,9,9>& A) {

      A( 0, 0 ) = 2.0*c.template u0<0, 0>();
      A( 1, 0 ) = c.template u0<0, 1>();
      A( 2, 0 ) = c.template u0<0, 2>();
      A( 3, 0 ) = 0;
      A( 4, 0 ) = 0;
      A( 5, 0 ) = 0;
      A( 6, 0 ) = 2.0*c.template u0<0, 0, 0>() + c.template u2<0>();
      A( 7, 0 ) = 2.0*c.template u0<0, 0, 1>();
      A( 8, 0 ) = 2.0*c.template u0<0, 0, 2>();

      A( 0, 1 ) = 2.0*c.template u0<0, 1>();
      A( 1, 1 ) = c.template u0<0, 0>() + c.template u0<1, 1>();
      A( 2, 1 ) = c.template u0<1, 2>();
      A( 3, 1 ) = 2.0*c.template u0<0, 1>();
      A( 4, 1 ) = c.template u0<0, 2>();
      A( 5, 1 ) = 0;
      A( 6, 1 ) = 4.0*c.template u0<0, 0, 1>() + c.template u2<1>();
      A( 7, 1 ) = 4.0*c.template u0<0, 1, 1>() + c.template u2<0>();
      A( 8, 1 ) = 4.0*c.template u0<0, 1, 2>();

      A( 0, 2 ) = 2.0*c.template u0<0, 2>();
      A( 1, 2 ) = c.template u0<1, 2>();
      A( 2, 2 ) = c.template u0<0, 0>() + c.template u0<2, 2>();
      A( 3, 2 ) = 0;
      A( 4, 2 ) = c.template u0<0, 1>();
      A( 5, 2 ) = 2.0*c.template u0<0, 2>();
      A( 6, 2 ) = 4.0*c.template u0<0, 0, 2>() + c.template u2<2>();
      A( 7, 2 ) = 4.0*c.template u0<0, 1, 2>();
      A( 8, 2 ) = 4.0*c.template u0<0, 2, 2>() + c.template u2<0>();

      A( 0, 3 ) = 0;
      A( 1, 3 ) = c.template u0<0, 1>();
      A( 2, 3 ) = 0;
      A( 3, 3 ) = 2.0*c.template u0<1, 1>();
      A( 4, 3 ) = c.template u0<1, 2>();
      A( 5, 3 ) = 0;
      A( 6, 3 ) = 2.0*c.template u0<0, 1, 1>();
      A( 7, 3 ) = 2.0*c.template u0<1, 1, 1>() + c.template u2<1>();
      A( 8, 3 ) = 2.0*c.template u0<1, 1, 2>();

      A( 0, 4 ) = 0;
      A( 1, 4 ) = c.template u0<0, 2>();
      A( 2, 4 ) = c.template u0<0, 1>();
      A( 3, 4 ) = 2.0*c.template u0<1, 2>();
      A( 4, 4 ) = c.template u0<1, 1>() + c.template u0<2, 2>();
      A( 5, 4 ) = 2.0*c.template u0<1, 2>();
      A( 6, 4 ) = 4.0*c.template u0<0, 1, 2>();
      A( 7, 4 ) = 4.0*c.template u0<1, 1, 2>() + c.template u2<2>();
      A( 8, 4 ) = 4.0*c.template u0<1, 2, 2>() + c.template u2<1>();

      A( 0, 5 ) = 0;
      A( 1, 5 ) = 0;
      A( 2, 5 ) = c.template u0<0, 2>();
      A( 3, 5 ) = 0;
      A( 4, 5 ) = c.template u0<1, 2>();
      A( 5, 5 ) = 2.0*c.template u0<2, 2>();
      A( 6, 5 ) = 2.0*c.template u0<0, 2, 2>();
      A( 7, 5 ) = 2.0*c.template u0<1, 2, 2>();
      A( 8, 5 ) = 2.0*c.template u0<2, 2, 2>() + c.template u2<2>();

      A( 0, 6 ) = 4.0*c.template u0<0, 0, 0>() + 2.0*c.template u2<0>();
      A( 1, 6 ) = 4.0*c.template u0<0, 0, 1>() + c.template u2<1>();
      A( 2, 6 ) = 4.0*c.template u0<0, 0, 2>() + c.template u2<2>();
      A( 3, 6 ) = 4.0*c.template u0<0, 1, 1>();
      A( 4, 6 ) = 4.0*c.template u0<0, 1, 2>();
      A( 5, 6 ) = 4.0*c.template u0<0, 2, 2>();
      A( 6, 6 ) = -4.0*Power(c.template u0<0, 0>(),2) - 4.0*Power(c.template u0<0, 1>(),2) - 4.0*Power(c.template u0<0, 2>(),2) - 4.0*c.template u0<0, 0>()*c.template u2() - Power(c.template u2(),2) + 8.0*c.template u2<0, 0>() + c.template u4();
      A( 7, 6 ) = -4.0*c.template u0<0, 0>()*c.template u0<0, 1>() - 4.0*c.template u0<0, 1>()*c.template u0<1, 1>() - 4.0*c.template u0<0, 2>()*c.template u0<1, 2>() - 4.0*c.template u0<0, 1>()*c.template u2() + 8.0*c.template u2<0, 1>();
      A( 8, 6 ) = -4.0*c.template u0<0, 0>()*c.template u0<0, 2>() - 4.0*c.template u0<0, 1>()*c.template u0<1, 2>() - 4.0*c.template u0<0, 2>()*c.template u0<2, 2>() - 4.0*c.template u0<0, 2>()*c.template u2() + 8.0*c.template u2<0, 2>();

      A( 0, 7 ) = 4.0*c.template u0<0, 0, 1>();
      A( 1, 7 ) = 4.0*c.template u0<0, 1, 1>() + c.template u2<0>();
      A( 2, 7 ) = 4.0*c.template u0<0, 1, 2>();
      A( 3, 7 ) = 4.0*c.template u0<1, 1, 1>() + 2.0*c.template u2<1>();
      A( 4, 7 ) = 4.0*c.template u0<1, 1, 2>() + c.template u2<2>();
      A( 5, 7 ) = 4.0*c.template u0<1, 2, 2>();
      A( 6, 7 ) = -4.0*c.template u0<0, 0>()*c.template u0<0, 1>() - 4.0*c.template u0<0, 1>()*c.template u0<1, 1>() - 4.0*c.template u0<0, 2>()*c.template u0<1, 2>() - 4.0*c.template u0<0, 1>()*c.template u2() + 8.0*c.template u2<0, 1>();
      A( 7, 7 ) = -4.0*Power(c.template u0<0, 1>(),2) - 4.0*Power(c.template u0<1, 1>(),2) - 4.0*Power(c.template u0<1, 2>(),2) - 4.0*c.template u0<1, 1>()*c.template u2() - Power(c.template u2(),2) + 8.0*c.template u2<1, 1>() + c.template u4();
      A( 8, 7 ) = -4.0*c.template u0<0, 1>()*c.template u0<0, 2>() - 4.0*c.template u0<1, 1>()*c.template u0<1, 2>() - 4.0*c.template u0<1, 2>()*c.template u0<2, 2>() - 4.0*c.template u0<1, 2>()*c.template u2() + 8.0*c.template u2<1, 2>();

      A( 0, 8 ) = 4.0*c.template u0<0, 0, 2>();
      A( 1, 8 ) = 4.0*c.template u0<0, 1, 2>();
      A( 2, 8 ) = 4.0*c.template u0<0, 2, 2>() + c.template u2<0>();
      A( 3, 8 ) = 4.0*c.template u0<1, 1, 2>();
      A( 4, 8 ) = 4.0*c.template u0<1, 2, 2>() + c.template u2<1>();
      A( 5, 8 ) = 4.0*c.template u0<2, 2, 2>() + 2.0*c.template u2<2>();
      A( 6, 8 ) = -4.0*c.template u0<0, 0>()*c.template u0<0, 2>() - 4.0*c.template u0<0, 1>()*c.template u0<1, 2>() - 4.0*c.template u0<0, 2>()*c.template u0<2, 2>() - 4.0*c.template u0<0, 2>()*c.template u2() + 8.0*c.template u2<0, 2>();
      A( 7, 8 ) = -4.0*c.template u0<0, 1>()*c.template u0<0, 2>() - 4.0*c.template u0<1, 1>()*c.template u0<1, 2>() - 4.0*c.template u0<1, 2>()*c.template u0<2, 2>() - 4.0*c.template u0<1, 2>()*c.template u2() + 8.0*c.template u2<1, 2>();
      A( 8, 8 ) = -4.0*Power(c.template u0<0, 2>(),2) - 4.0*Power(c.template u0<1, 2>(),2) - 4.0*Power(c.template u0<2, 2>(),2) - 4.0*c.template u0<2, 2>()*c.template u2() - Power(c.template u2(),2) + 8.0*c.template u2<2, 2>() + c.template u4();

    }

    template<class Moments>
    inline void set_rhs(
        const double d2,
        const Moments& c,
        const GasModel* const gas,
        const GasModel::Approx approx,
        Eigen::Matrix<double,9,1>& B) {

      B( 0 ) = gas->P0<0,0>(approx,c)- d2;
      B( 1 ) = gas->P0<0,1>(approx,c);
      B( 2 ) = gas->P0<0,2>(approx,c);
      B( 3 ) = gas->P0<1,1>(approx,c) - d2;
      B( 4 ) = gas->P0<1,2>(approx,c);
      B( 5 ) = gas->P0<2,2>(approx,c) - d2;
      B( 6 ) = gas->P2<0>(approx,c);
      B( 7 ) = gas->P2<1>(approx,c);
      B( 8 ) = gas->P2<2>(approx,c);
    }

  }

  void
  FokkerPlanck::cubic_coefficients(
      Eigen::Matrix<double,16,1>& X,
      const CellData& c,
      const GasModel* const gas,
      const GasModel::Approx approx) {

    // temperature in cell
    const double T = fmin(1.e6,fmax(c.T(),5.));

    // stress tensor relaxation rate
    const double nur = c.p()/gas->mu(T);

    // FP frequency
    const double nu = .5*nur;

    // internal inverse relaxation rates
    const double nu_rot = nur*(gas->zRotInv(T))*(4./M_PI);
    const double nu_vib = nur*(gas->zVibInv(T))*(4./M_PI);

    // diffusion coefficients

    const double d2_tr = c.theta()*nur; // == (2/3)*(2*nu*es_tr) == (2/3)*(nur*(3/2)*theta)

    const double d2 = d2_tr + (
        (2./3.)*nu_rot*(c.u_rot() - gas->esEqRot(T)) +
        (2./3.)*nu_vib*(c.u_vib() - gas->esEqVib(T)) );
    const double d2_rot = nu_rot*(gas->esEqRot(T));
    const double d2_vib = nu_vib*(gas->esEqVib(T));

    MYWARN(d2>0.,"negative diffusion");

    // linear system
    Eigen::Matrix<double,9,9> AA;
    Eigen::Matrix<double,9,1> B;

    // set left hand side
    CubicCoefficients::set_lhs(c, AA);

    // set right hand side
    CubicCoefficients::set_rhs(d2_tr, c, gas, approx, B);

    // solve for coefficients
    X.head<9>() = AA.partialPivLu().solve(B);
    X[ 9] = 0.;

    X[10] = nu;
    X[11] = nu_rot;
    X[12] = nu_vib;

    X[13] = fabs(d2);
    X[14] = d2_rot;
    X[15] = d2_vib;

  }


}


#endif /* SRC_VELOCITYUPDATE_FOKKERPLANCK_CUBICSYSTEM_H_ */
