/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * Models.h
 *
 *  Created on: Jan 9, 2019
 *      Author: kustepha
 */

#ifndef SRC_VELOCITYUPDATE_FOKKERPLANCK_MODELS_H_
#define SRC_VELOCITYUPDATE_FOKKERPLANCK_MODELS_H_


#include "CubicSystem.h"
#include "CubicLegacySystem.h"
#include "GFPSystem.h"

#include "FokkerPlanck.h"


namespace VelocityUpdate {

  template<FokkerPlanck::ModelTy model> struct FokkerPlanck::Model {

    static_assert(model == ModelTy::CUBIC ||
                  model == ModelTy::CUBIC_LEGACY );

    const Eigen::Matrix<double,16,1> X;
    const CellData& c;

    // FP frequency
    const double nu     = X[10];
    const double nu_rot = X[11];
    const double nu_vib = X[12];

    // diffusion coefficients
    const double d2_tr  = X[13];
    const double d2_rot = X[14];
    const double d2_vib = X[15];

    // set coefficients, decompose into linear, non-linear part
    const v4df a2_i0 = v4df{X[0] + nu, X[1],      X[2],      0.};
    const v4df a2_i1 = v4df{X[1]     , X[3] + nu, X[4],      0.};
    const v4df a2_i2 = v4df{X[2],      X[4],      X[5] + nu, 0.};
    const v4df a3_i  = v4df{X[6], X[7], X[8], 0.};
    const v4df a4    = MyIntrin::set3(X[9]);

    // ---------------------------------------------------------------------//
    // means
    const v4df u0_i  = c.v();
    const v4df u2    = MyIntrin::set3( c.u2() );
    const v4df u0_0j = v4df{c.u0<0,0>(), c.u0<0,1>(), c.u0<0,2>(), 0.};
    const v4df u0_1j = v4df{c.u0<1,0>(), c.u0<1,1>(), c.u0<1,2>(), 0.};
    const v4df u0_2j = v4df{c.u0<2,0>(), c.u0<2,1>(), c.u0<2,2>(), 0.};
    const v4df u2_i  = c.u2i();

  public:

    void coordinates(Eigen::Matrix4d& V, Eigen::Vector4d& lambda) const {
      Eigen::Matrix3d A;
      A(0,0) = X[0];
      A(1,0) = X[1];
      A(2,0) = X[2];
      A(1,1) = X[3];
      A(2,1) = X[4];
      A(2,2) = X[5];

      FokkerPlanck::coordinates(V,lambda,A);

    }

    v4df nonlinear_drift(const v4df Vi) const {

      v4df V0,V1,V2;
      MyIntrin::bcast3(Vi,V0,V1,V2);

      return a2_i0*V0 +a2_i1*V1 + a2_i2*V2 + nonlinear_drift2(Vi);
    }

    v4df nonlinear_drift2(const v4df Vi) const {

      v4df V0,V1,V2,VkVk;
      MyIntrin::bcast3(Vi,V0,V1,V2);
      VkVk = MyIntrin::abssq3(Vi);

      switch (model) {
        case ModelTy::CUBIC: {
          return /*a2_i0*V0 +a2_i1*V1 + a2_i2*V2 +*/ // linear
              a3_i*( VkVk      - u2  ) +       // quadratic 1
              2.*(                             // quadratic 2
                  a3_i[0]*(V0*Vi - u0_0j) +
                  a3_i[1]*(V1*Vi - u0_1j) +
                  a3_i[2]*(V2*Vi - u0_2j)
              );
        } break;
        case ModelTy::CUBIC_LEGACY: {
          return /*a2_i0*V0 + a2_i1*V1 + a2_i2*V2 +*/ // linear
              a3_i*( VkVk      - u2   )  + // quadratic
              a4  *( Vi*VkVk   - u2_i );   // cubic
        } break;
      } // switch model

      static_assert(model == ModelTy::CUBIC ||
                    model == ModelTy::CUBIC_LEGACY );

      __builtin_unreachable();
      return zero<v4df>();

    }




  private:

    static Eigen::Matrix<double,16,1> getX(
        const CellData&c, const GasModel* const gas, const GasModel::Approx approx) {
      Eigen::Matrix<double,16,1> X;
      switch (model) {
        case ModelTy::CUBIC:         { cubic_coefficients(X,c,gas,approx);         } break;
        case ModelTy::CUBIC_LEGACY:  { cubic_legacy_coefficients(X,c,gas,approx);  } break;
      }
      static_assert(model == ModelTy::CUBIC ||
                    model == ModelTy::CUBIC_LEGACY );
      return X;
    }

  public:
    Model(const CellData& _c, const GasModel* const gas, const GasModel::Approx approx)
  : X(getX(_c,gas,approx))
  , c(_c)
  {}

  };

  template<int N> struct FokkerPlanck::GeneralizedModel {

    static constexpr int sz = N-4;
    static constexpr int szx = sz+6;

    const Eigen::Matrix<double,szx,1> X;
    const CellData& c;

    // FP frequency
    const double nu     = X[sz+0];
    const double nu_rot = X[sz+1];
    const double nu_vib = X[sz+2];

    // diffusion coefficients
    const double d2_tr  = X[sz+3];
    const double d2_rot = X[sz+4];
    const double d2_vib = X[sz+5];

  public:

    void coordinates(Eigen::Matrix4d& V, Eigen::Vector4d& lambda) const {
      Eigen::Matrix3d A = Eigen::Matrix3d::Zero();
      A(0,0) = X[0]-nu;
      A(1,0) = X[3];
      A(2,0) = X[4];
      A(1,1) = X[1]-nu;
      A(2,1) = X[5];
      A(2,2) = X[2]-nu;

      FokkerPlanck::coordinates(V,lambda,A);

    }

    __attribute__((__hot__))
    inline v4df nonlinear_drift(const v4df& V) const {

      Eigen::Matrix<double,4,sz> GH; // gradient of H

      GFPCoefficients::set_ghl<N>(V,GH.template leftCols<6>());
      GFPCoefficients::set_ghnl<N>(V,c,GH.template rightCols<sz-6>());

      return from_eigen( (GH * (X.template head<sz>())).eval() );

    }

    __attribute__((__hot__))
    inline v4df nonlinear_drift2(const v4df& V) const {

      Eigen::Matrix<double,4,sz-6> GH; // gradient of H

      GH.setZero();

      GFPCoefficients::set_ghnl<N>(V,c,GH);

      return from_eigen( (GH * (X.template segment<sz-6>(6))).eval() );

    }

  private:
    static Eigen::Matrix<double,szx,1> getX(
        const CellData&c, const GasModel* const gas, const GasModel::Approx approx) {
      Eigen::Matrix<double,szx,1> X;
      generalized_coefficients<N>(X,c,gas,approx);
      // add nu to first 3 components
      const double nu = X[sz];
      X[0] += nu; X[1] += nu; X[2] += nu;
      return X;
    }

  protected:
    GeneralizedModel(const CellData& _c, const GasModel* const gas, const GasModel::Approx approx)
  : X(getX(_c,gas,approx))
  , c(_c)
  {}

  public:
    virtual ~GeneralizedModel() = default;

  };

  template<> struct FokkerPlanck::Model<FokkerPlanck::ModelTy::GENERALIZED13> :
  FokkerPlanck::GeneralizedModel<13> {
    Model(const CellData& c,  const GasModel* const gas,
          const GasModel::Approx approx) : GeneralizedModel(c,gas,approx){}
  };

  template<> struct FokkerPlanck::Model<FokkerPlanck::ModelTy::GENERALIZED14> :
  FokkerPlanck::GeneralizedModel<14> {
    Model(const CellData& c,  const GasModel* const gas,
          const GasModel::Approx approx) : GeneralizedModel(c,gas,approx){}
  };

  template<> struct FokkerPlanck::Model<FokkerPlanck::ModelTy::GENERALIZED20> :
  FokkerPlanck::GeneralizedModel<20> {
    Model(const CellData& c,  const GasModel* const gas,
          const GasModel::Approx approx) : GeneralizedModel(c,gas,approx){}
  };

  template<> struct FokkerPlanck::Model<FokkerPlanck::ModelTy::GENERALIZED21> :
  FokkerPlanck::GeneralizedModel<21> {
    Model(const CellData& c,  const GasModel* const gas,
          const GasModel::Approx approx) : GeneralizedModel(c,gas,approx){}
  };

  template<> struct FokkerPlanck::Model<FokkerPlanck::ModelTy::GENERALIZED26> :
  FokkerPlanck::GeneralizedModel<26> {
    Model(const CellData& c,  const GasModel* const gas,
          const GasModel::Approx approx) : GeneralizedModel(c,gas,approx){}
  };



} // namespace Velocity Update


#endif /* SRC_VELOCITYUPDATE_FOKKERPLANCK_MODELS_H_ */
