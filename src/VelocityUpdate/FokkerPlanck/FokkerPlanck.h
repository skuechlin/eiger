/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * FokkerPlanck.h
 *
 *  Created on: Dec 5, 2018
 *      Author: kustepha
 */

#ifndef SRC_VELOCITYUPDATE_FOKKERPLANCK_H_
#define SRC_VELOCITYUPDATE_FOKKERPLANCK_H_

#include <Eigen/Dense>
#include <Eigen/Eigenvalues>

#include "Gas/GasModel.h"

#include "Grid/CellData.h"

#include "Primitives/MyIntrin.h"
#include "Primitives/MyRandom.h"
#include "Primitives/NormalDistribution.h"

#include "VelocityUpdate/VelocityUpdateStrategy.h"

namespace {

  struct AssertFiniteVisitor {
    std::string loc;
    AssertFiniteVisitor& operator()(const char* f, int l) {
      loc = std::string(f) + std::to_string(l);
      return *this;
    }
    void operator()(const double& v, uint64_t i, uint64_t j) const {

      if (__builtin_expect(!(std::isfinite(v)),false)) {
          std::stringstream ss;
          ss << loc << ": non-finite at " << i << "," << j;

          throw std::runtime_error(ss.str());

      }

    }
    void init(const double& v, uint64_t i, uint64_t j) const {
      this->operator ()(v,i,j);
    }
  };

}

namespace Settings {
  class Dictionary;
}

namespace VelocityUpdate
{

  class FokkerPlanck : public Strategy
  {


  public:

    enum class VelocityIntegration : uint64_t {
      STANDARD,
      TRANSFORMED,
      INVALID
    };

    enum class PositionIntegration : uint64_t {
      EULER,
      JOINT,
      CONDITIONAL,
      INVALID
    };

    enum class ModelTy : uint64_t {
      CUBIC,
      CUBIC_LEGACY,
      GENERALIZED13,
      GENERALIZED14,
      GENERALIZED20,
      GENERALIZED21,
      GENERALIZED26,
      INVALID
    };



  private:

    template<ModelTy> struct Model;
    template<int N> struct GeneralizedModel;

    friend class FokkerPlanck_Standalone;

    static void
    cubic_coefficients(
        Eigen::Matrix<double,16,1>& X,
        const CellData& c,
        const GasModel* const gas,
        const GasModel::Approx approx);

    static void
    cubic_legacy_coefficients(
        Eigen::Matrix<double,16,1>& X,
        const CellData& c,
        const GasModel* const gas,
        const GasModel::Approx approx);

    static void
    entropic_coefficients(
        Eigen::Matrix<double,16,1>& X,
        const CellData& c,
        const GasModel* const gas,
        const GasModel::Approx approx);

    template<int N>
    static void
    generalized_coefficients(
        Eigen::Matrix<double,N-4+6,1>& X,
        const CellData& c,
        const GasModel* const gas,
        const GasModel::Approx approx);

  private:

    static void coordinates(Eigen::Matrix4d& V, Eigen::Vector4d& lambda, const Eigen::Matrix3d& A) {

      Eigen::SelfAdjointEigenSolver<Eigen::Matrix3d> es(A);

      V.setZero();
      V.topLeftCorner<3,3>() = es.eigenvectors();
      lambda.setZero();
      lambda.head<3>() = es.eigenvalues();
    }


  public:

    template<ModelTy model, VelocityIntegration velocity_integration, PositionIntegration position_integration>
    static void updateVelocities_impl(
        Particles::Particle* pfirst,
        Particles::Particle* plast,
        CellData& c,
        const double dt,
        const GasModel* const gas,
        MyRandom::Rng& rng,
        const GasModel::Approx production_approximation
    )  {

      static_assert(model != ModelTy::INVALID);
      static_assert(velocity_integration != VelocityIntegration::INVALID);
      static_assert(position_integration != PositionIntegration::INVALID);


      const uint64_t num_prts = std::distance(pfirst,plast);

      if (num_prts < 1) return;
      if (c.Nptot() < 5.) return;


      MYWARN(c.p()>0.,"non-positive pressure");
      if (c.p()<= 0.) return;

      // ---------------------------------------------------------------------//
      // model coefficients
      const Model<model> cf(c,gas,production_approximation);


      // ---------------------------------------------------------------------//
      // transformed integration factors
      bool transformed_velocity_integration = false;

      // compute tranform
      Eigen::Vector4d lambda;
      Eigen::Matrix4d V,VT;
      if (velocity_integration == VelocityIntegration::TRANSFORMED && num_prts > 2) {

          cf.coordinates(V,lambda); lambda *= -1.; lambda(3) = 1.;

          if ((lambda*dt).minCoeff()>-100.) {
              VT = V.transpose();
              transformed_velocity_integration = true;
          }

          // else default to un-transformed
      }

      if (!transformed_velocity_integration) {
          lambda(0) = cf.nu; lambda(1) = cf.nu; lambda(2) = cf.nu; lambda(3) = 1.;
      }

      const v4df nu = from_eigen(lambda);
      const v4df nudt = nu*dt;

      // ---------------------------------------------------------------------//
      // standard integration factors

      // velocity, position correlation factors
      v4df aa = -cf.d2_tr*MyIntrin::expm14(-2.*nudt) / (2.*nu);
      aa[3] = 0.;

      v4df bb =  cf.d2_tr*(2.*nudt + 4.*MyIntrin::expm14(-nudt) - MyIntrin::expm14(-2.*nudt)) /
          (2.*nu*nu*nu);
      bb[3] = 1.;

      // ensure positive
      v4df bb_pos = (bb>0.) ? bb : one<v4df>();

      v4df cc =  cf.d2_tr*MyIntrin::pow4(MyIntrin::expm14(-nudt),2.) / (2.*nu*nu);
      cc[3] = 0.;

      // avoide div-by-0
      cc = (bb<=0.) ? zero<v4df>() : cc;

      if (position_integration==PositionIntegration::JOINT)
        aa -= cc*cc / bb_pos;

      // diffusion integration factors
      const v4df Dfac1 =  MyIntrin::sqrt4(aa);
      const v4df Dfac2 =  cc/MyIntrin::sqrt4(bb_pos);
      const v4df Dfac3 =  (bb > 0.) ? MyIntrin::sqrt4(bb_pos)/dt : zero<v4df>();

      // for conditional position integration
      const v4df dd = MyIntrin::sqrt4(aa)*MyIntrin::tanh4(.5*nudt) / nu;
      const v4df ee = cf.d2_tr*(nudt - 2.*MyIntrin::tanh4(.5*nudt)) / (nu*nu*nu);
      const v4df Dfac4 = dd/dt;
      const v4df Dfac5 = MyIntrin::sqrt4((ee > 0.) ? ee : zero<v4df>())/dt;

      // linear drift integration factor

      // velocity
      const v4df Mfac = MyIntrin::exp4(-nudt);
      // position
      const v4df XMfac = -MyIntrin::expm14(-nudt)/(nudt);

      // non-linear drift integration factor

      // velocity
      const v4df Nfac = -MyIntrin::exp4(-nudt)*MyIntrin::expm14(-nudt)/nu;
      // position
      const v4df XNfac = MyIntrin::exp4(-nudt)*(nudt + MyIntrin::expm14(-nudt))/( nu*nudt );




      // ---------------------------------------------------------------------//
      // compute random numbers

      MyRandom::NormalDistribution sn;

      static MyMemory::aligned_array<v4df,4096> rands;
#pragma omp threadprivate(rands)


      {

        if(position_integration == PositionIntegration::EULER) rands.resize(  num_prts);
        else                                                   rands.resize(2*num_prts);

        for (uint64_t i = 0; i < rands.size(); ++i)
          rands[i] = v4df{sn(rng),sn(rng),sn(rng),0.};//rng.normal3();//v4df{dist(gen),dist(gen),dist(gen),0.};//rng.normal3();

        //  correct sample mean and variance
        if (num_prts>2)
          {

            for(uint64_t j = 0; j < rands.size(); j+=num_prts) {

                v4df mu1 = zero<v4df>();
                v4df mu2 = zero<v4df>();
                double count = 0.;

                for (uint64_t i = j; i < (j+num_prts); ++i) {
                    count += 1.;
                    v4df delta = rands[i] - mu1;
                    mu1 += delta/count;
                    v4df delta2 = rands[i] - mu1;
                    mu2 += delta*delta2;
                }
                mu2 /= count;

                const v4df f = 1./MyIntrin::sqrt4(mu2 + inactive4df<3>());

                for (uint64_t i = j; i < (j+num_prts); ++i)
                  rands[i] = f*(rands[i]-mu1);

            }

          }

      }


      // ---------------------------------------------------------------------//
      // velocity update

      // a-priori ensemble energy
      double e_pre  = 0.;
      // a-posteriori ensemble energy
      double e_post = 0.;

      // random number use counter
      uint64_t rcnt = 0;

      // update velocities
      for (auto p = pfirst; p != plast; ++p, ++rcnt) {

          v4df Vi     = p->C - c.v();
          e_pre += MyIntrin::sabssq3(Vi);

          // non-linear drift
          v4df N;

          if (transformed_velocity_integration) {
              N  = from_eigen( ( VT*to_eigen( cf.nonlinear_drift2(Vi) ) ).eval() );
              Vi = from_eigen( ( VT*to_eigen( Vi                      ) ).eval() );
          } else {
              N = cf.nonlinear_drift(Vi);
          }

          p->C =
              Mfac*Vi + // linear drift
              Nfac*N  + // non-linear drift
              Dfac1*rands[rcnt]; // diffusion

          if (position_integration==PositionIntegration::JOINT) {
              p->C += Dfac2*rands[rcnt + num_prts];
              p->D =  XMfac*Vi + XNfac*N + Dfac3*rands[rcnt + num_prts];
          } else if (position_integration==PositionIntegration::CONDITIONAL) {
              p->D =  XMfac*Vi + XNfac*N + Dfac4*rands[rcnt] + Dfac5*rands[rcnt + num_prts];
          } else {
              p->D = p->C;
          }

          if (transformed_velocity_integration) {
              p->C = from_eigen( (V*to_eigen(p->C)).eval() );
              p->D = from_eigen( (V*to_eigen(p->D)).eval() );
          }

          e_post += MyIntrin::sabssq3(p->C);

      } // for all particles


      // ---------------------------------------------------------------------//
      // internal modes

      if (gas->diatomic())
        {

          // compute and correct rands
          {
            for (uint64_t i = 0; i < num_prts; ++i)
              rands[i] = v4df{sn(rng),sn(rng),sn(rng),sn(rng)};

            //  correct sample mean and variance
            if (num_prts > 2)
              {
                v4df mu1 = zero<v4df>();
                v4df mu2 = zero<v4df>();
                double count = 0.;
                for (uint64_t i = 0; i < num_prts; ++i)
                  {
                    count += 1.;
                    v4df delta  = rands[i] - mu1;
                    mu1 += delta/count;
                    v4df delta2 = rands[i] - mu1;
                    mu2 += delta*delta2;
                  }
                mu2 /= count;

                const v4df f = 1./MyIntrin::sqrt4(mu2);

                for (uint64_t i = 0; i < num_prts; ++i)
                  rands[i] = f*(rands[i]-mu1);
              }
          }

          // reset random variable use counte
          rcnt = 0;

          // rel. freq. and non-dimensional times
          const double nurot   = cf.nu_rot;
          const double nuvib   = cf.nu_vib;
          const double nurotdt = nurot * dt;
          const double nuvibdt = nuvib * dt;

          // average internal state
          const v2df rot_av = c.im_.omega();
          const v2df vib_av = c.im_.xi();

          // drift integration factor
          const v2df rot_fac = _mm_set1_pd(exp(-.5*nurotdt));
          const v2df vib_fac = _mm_set1_pd(exp(-.5*nuvibdt));

          // diffusion integration factor
          const v2df rot_d2fac = _mm_set1_pd(sqrt(-cf.d2_rot*expm1(-nurotdt)/nurot));
          const v2df vib_d2fac = _mm_set1_pd(sqrt(-cf.d2_vib*expm1(-nuvibdt)/nuvib));

          for (Particles::Particle* p = pfirst; p != plast; ++p, ++rcnt)
            {

              p->Omega  -= rot_av;
              p->Xi     -= vib_av;

              e_pre += MyIntrin::sabssq<v2df>(p->Omega) + MyIntrin::sabssq<v2df>(p->Xi);

              p->Omega  *= rot_fac;
              p->Xi     *= vib_fac;

              p->Omega  += rot_d2fac*_mm256_extractf128_pd(rands[rcnt],0);
              p->Xi     += vib_d2fac*_mm256_extractf128_pd(rands[rcnt],1);

              e_pre  -= MyIntrin::sabssq<v2df>(p->Omega) + MyIntrin::sabssq<v2df>(p->Xi);

              p->Omega  += rot_av;
              p->Xi     += vib_av;

              // add difference of pre and post state update energy to tally
              // this means we assume any difference in energy to be distributed to
              // translational modes
            }

          // make sure we didn't take more energy from translational modes than available
          e_pre = fmax(e_pre,0.0);

        } // if diatomic

      // energy correction factor
      const double e_fac = sqrt(e_pre/e_post);

      // correct energy and re-set mean
      for (auto p = pfirst; p != plast; ++p)
        Particles::set_velocity(p, c.v() + e_fac*(p->C), c.v() + e_fac*(p->D) );

    }

  public:

    CollisionOperatorStatistics updateVelocities(
        Particles::Particle* pfirst,
        Particles::Particle* plast,
        CellData& c,
        const double dt,
        const GasModel* const gas,
        MyRandom::Rng& rng
    ) const override;



  private:

    const VelocityIntegration velocity_integration;
    const PositionIntegration position_integration;
    const ModelTy model;
    const GasModel::Approx production_approximation;


  public:
    FokkerPlanck (const Settings::Dictionary& dict);
  };


} /* namespace VelocityUpdate */

#endif /* SRC_VELOCITYUPDATE_FOKKERPLANCK_H_ */
