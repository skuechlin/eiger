/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * GFP14System.h
 *
 *  Created on: Jan 7, 2019
 *      Author: kustepha
 */

#ifndef SRC_VELOCITYUPDATE_FOKKERPLANCK_GFP14SYSTEM_H_
#define SRC_VELOCITYUPDATE_FOKKERPLANCK_GFP14SYSTEM_H_

namespace VelocityUpdate {

  namespace GFPCoefficients {

    template<>
    inline void set_sigma<14>(
        const CellData& c,
        Eigen::Matrix<double,10,10>& Sigma) {

      Sigma( 0, 0 ) = c.u0<0,0>();
      Sigma( 1, 0 ) = 0;
      Sigma( 2, 0 ) = 0;
      Sigma( 3, 0 ) = c.u0<0,1>();
      Sigma( 4, 0 ) = c.u0<0,2>();
      Sigma( 5, 0 ) = 0;
      Sigma( 6, 0 ) = c.u2<0>() + 2.0*c.u0<0,0,0>();
      Sigma( 7, 0 ) = 2.0*c.u0<0,0,1>();
      Sigma( 8, 0 ) = 2.0*c.u0<0,0,2>();
      Sigma( 9, 0 ) = 4.0*c.u2<0,0>();

      Sigma( 1, 1 ) = c.u0<1,1>();
      Sigma( 2, 1 ) = 0;
      Sigma( 3, 1 ) = c.u0<0,1>();
      Sigma( 4, 1 ) = 0;
      Sigma( 5, 1 ) = c.u0<1,2>();
      Sigma( 6, 1 ) = 2.0*c.u0<0,1,1>();
      Sigma( 7, 1 ) = c.u2<1>() + 2.0*c.u0<1,1,1>();
      Sigma( 8, 1 ) = 2.0*c.u0<1,1,2>();
      Sigma( 9, 1 ) = 4.0*c.u2<1,1>();

      Sigma( 2, 2 ) = c.u0<2,2>();
      Sigma( 3, 2 ) = 0;
      Sigma( 4, 2 ) = c.u0<0,2>();
      Sigma( 5, 2 ) = c.u0<1,2>();
      Sigma( 6, 2 ) = 2.0*c.u0<0,2,2>();
      Sigma( 7, 2 ) = 2.0*c.u0<1,2,2>();
      Sigma( 8, 2 ) = c.u2<2>() + 2.0*c.u0<2,2,2>();
      Sigma( 9, 2 ) = 4.0*c.u2<2,2>();

      Sigma( 3, 3 ) = c.u0<0,0>() + c.u0<1,1>();
      Sigma( 4, 3 ) = c.u0<1,2>();
      Sigma( 5, 3 ) = c.u0<0,2>();
      Sigma( 6, 3 ) = c.u2<1>() + 4.0*c.u0<0,0,1>();
      Sigma( 7, 3 ) = c.u2<0>() + 4.0*c.u0<0,1,1>();
      Sigma( 8, 3 ) = 4.0*c.u0<0,1,2>();
      Sigma( 9, 3 ) = 8.0*c.u2<0,1>();

      Sigma( 4, 4 ) = c.u0<0,0>() + c.u0<2,2>();
      Sigma( 5, 4 ) = c.u0<0,1>();
      Sigma( 6, 4 ) = c.u2<2>() + 4.0*c.u0<0,0,2>();
      Sigma( 7, 4 ) = 4.0*c.u0<0,1,2>();
      Sigma( 8, 4 ) = c.u2<0>() + 4.0*c.u0<0,2,2>();
      Sigma( 9, 4 ) = 8.0*c.u2<0,2>();

      Sigma( 5, 5 ) = c.u0<1,1>() + c.u0<2,2>();
      Sigma( 6, 5 ) = 4.0*c.u0<0,1,2>();
      Sigma( 7, 5 ) = c.u2<2>() + 4.0*c.u0<1,1,2>();
      Sigma( 8, 5 ) = c.u2<1>() + 4.0*c.u0<1,2,2>();
      Sigma( 9, 5 ) = 8.0*c.u2<1,2>();

      Sigma( 6, 6 ) = -(c.u2()*c.u2()) + c.u4() - 4.0*c.u2()*c.u0<0,0>() - 4.0*(c.u0<0,0>()*c.u0<0,0>()) - 4.0*(c.u0<0,1>()*c.u0<0,1>()) - 4.0*(c.u0<0,2>()*c.u0<0,2>()) + 8.0*c.u2<0,0>();
      Sigma( 7, 6 ) = -4.0*(c.u2()*c.u0<0,1>() + c.u0<0,0>()*c.u0<0,1>() + c.u0<0,1>()*c.u0<1,1>() + c.u0<0,2>()*c.u0<1,2>() - 2.0*c.u2<0,1>());
      Sigma( 8, 6 ) = -4.0*(c.u2()*c.u0<0,2>() + c.u0<0,0>()*c.u0<0,2>() + c.u0<0,1>()*c.u0<1,2>() + c.u0<0,2>()*c.u0<2,2>() - 2.0*c.u2<0,2>());
      Sigma( 9, 6 ) = -4.0*(c.u2()*c.u2<0>() - 3.0*c.u4<0>() + 2.0*c.u2<0>()*c.u0<0,0>() + 2.0*c.u2<1>()*c.u0<0,1>() + 2.0*c.u2<2>()*c.u0<0,2>());

      Sigma( 7, 7 ) = -(c.u2()*c.u2()) + c.u4() - 4.0*(c.u0<0,1>()*c.u0<0,1>()) - 4.0*c.u2()*c.u0<1,1>() - 4.0*(c.u0<1,1>()*c.u0<1,1>()) - 4.0*(c.u0<1,2>()*c.u0<1,2>()) + 8.0*c.u2<1,1>();
      Sigma( 8, 7 ) = -4.0*(c.u0<0,1>()*c.u0<0,2>() + c.u2()*c.u0<1,2>() + c.u0<1,1>()*c.u0<1,2>() + c.u0<1,2>()*c.u0<2,2>() - 2.0*c.u2<1,2>());
      Sigma( 9, 7 ) = -4.0*(c.u2()*c.u2<1>() - 3.0*c.u4<1>() + 2.0*c.u2<0>()*c.u0<0,1>() + 2.0*c.u2<1>()*c.u0<1,1>() + 2.0*c.u2<2>()*c.u0<1,2>());

      Sigma( 8, 8 ) = -(c.u2()*c.u2()) + c.u4() - 4.0*(c.u0<0,2>()*c.u0<0,2>()) - 4.0*(c.u0<1,2>()*c.u0<1,2>()) - 4.0*c.u2()*c.u0<2,2>() - 4.0*(c.u0<2,2>()*c.u0<2,2>()) + 8.0*c.u2<2,2>();
      Sigma( 9, 8 ) = -4.0*(c.u2()*c.u2<2>() - 3.0*c.u4<2>() + 2.0*c.u2<0>()*c.u0<0,2>() + 2.0*c.u2<1>()*c.u0<1,2>() + 2.0*c.u2<2>()*c.u0<2,2>());

      Sigma( 9, 9 ) = 16.0*(c.u6() - (c.u2<0>()*c.u2<0>()) - (c.u2<1>()*c.u2<1>()) - (c.u2<2>()*c.u2<2>()));


    }

    template<>
    inline void set_p<14>(
        const CellData& c,
        const GasModel* const gas,
        const GasModel::Approx approx,
        Eigen::Ref<Eigen::Matrix<double,10,1>> P) {

      P( 0 ) = gas->P0<0,0>(approx,c)*0.5;
      P( 1 ) = gas->P0<1,1>(approx,c)*0.5;
      P( 2 ) = gas->P0<2,2>(approx,c)*0.5;
      P( 3 ) = gas->P0<0,1>(approx,c);
      P( 4 ) = gas->P0<0,2>(approx,c);
      P( 5 ) = gas->P0<1,2>(approx,c);
      P( 6 ) = gas->P2<0>(approx,c);
      P( 7 ) = gas->P2<1>(approx,c);
      P( 8 ) = gas->P2<2>(approx,c);
      P( 9 ) = gas->P4(approx,c);

    }

    template<>
    inline void set_lh<14>(
        const CellData& c,
        Eigen::Ref<Eigen::Matrix<double,10,1>> LH) {


      LH( 0 ) = 1.;
      LH( 1 ) = 1.;
      LH( 2 ) = 1.;
      LH( 3 ) = 0.;
      LH( 4 ) = 0.;
      LH( 5 ) = 0.;
      LH( 6 ) = 0.;
      LH( 7 ) = 0.;
      LH( 8 ) = 0.;
      LH( 9 ) = 20.0*c.u2();

    }

    template<>
    inline void set_ghnl<14>(
        const v4df& V, const CellData& c,
        Eigen::Ref<Eigen::Matrix<double,4,4>> GH) {


      const double VkVk = V[0]*V[0] + V[1]*V[1] + V[2]*V[2];

      GH( 0, 0 ) = VkVk - c.u2() + 2.0*(V[0]*V[0] - c.u0<0,0>());
      GH( 1, 0 ) = 2.0*(V[0]*V[1] - c.u0<0,1>());
      GH( 2, 0 ) = 2.0*(V[0]*V[2] - c.u0<0,2>());
      GH( 3, 0 ) = 0;

      GH( 0, 1 ) = 2.0*(V[0]*V[1] - c.u0<0,1>());
      GH( 1, 1 ) = VkVk - c.u2() + 2.0*(V[1]*V[1] - c.u0<1,1>());
      GH( 2, 1 ) = 2.0*(V[1]*V[2] - c.u0<1,2>());
      GH( 3, 1 ) = 0;

      GH( 0, 2 ) = 2.0*(V[0]*V[2] - c.u0<0,2>());
      GH( 1, 2 ) = 2.0*(V[1]*V[2] - c.u0<1,2>());
      GH( 2, 2 ) = VkVk - c.u2() + 2.0*(V[2]*V[2] - c.u0<2,2>());
      GH( 3, 2 ) = 0;

      GH( 0, 3 ) = 4.0*(-c.u2<0>() + VkVk*V[0]);
      GH( 1, 3 ) = 4.0*(-c.u2<1>() + VkVk*V[1]);
      GH( 2, 3 ) = 4.0*(-c.u2<2>() + VkVk*V[2]);
      GH( 3, 3 ) = 0;

    }

  } // namespace GFPCoefficients

} // namespace VelocityUpdate



#endif /* SRC_VELOCITYUPDATE_FOKKERPLANCK_GFP14SYSTEM_H_ */
