/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * FokkerPlanck.cpp
 *
 *  Created on: Dec 5, 2018
 *      Author: kustepha
 */

#include "Parallel/MyMPI.h"

#include "Settings/Dictionary.h"
#include "Primitives/MyString.h"
#include "Primitives/MyError.h"

#include "FokkerPlanck.h"

#include "Models.h"

namespace {

  template<
  VelocityUpdate::FokkerPlanck::VelocityIntegration velocity_integration,
  VelocityUpdate::FokkerPlanck::PositionIntegration position_integration
  >
  void dispatch_by_model_updateVelocities(
      const VelocityUpdate::FokkerPlanck::ModelTy model,
      Particles::Particle* pfirst,
      Particles::Particle* plast,
      CellData& c,
      const double dt,
      const GasModel* const gas,
      MyRandom::Rng& rng,
      const GasModel::Approx production_approximation
  ) {

    using namespace VelocityUpdate;

    switch (model) {
      case FokkerPlanck::ModelTy::CUBIC:
        FokkerPlanck::updateVelocities_impl<FokkerPlanck::ModelTy::CUBIC,velocity_integration,position_integration>(
            pfirst,plast,c,dt,gas,rng, production_approximation );
        break;
      case FokkerPlanck::ModelTy::CUBIC_LEGACY:
        FokkerPlanck::updateVelocities_impl<FokkerPlanck::ModelTy::CUBIC_LEGACY,velocity_integration,position_integration>(
            pfirst,plast,c,dt,gas,rng, production_approximation );
        break;
      case FokkerPlanck::ModelTy::GENERALIZED13:
        FokkerPlanck::updateVelocities_impl<FokkerPlanck::ModelTy::GENERALIZED13,velocity_integration,position_integration>(
            pfirst,plast,c,dt,gas,rng, production_approximation );
        break;
      case FokkerPlanck::ModelTy::GENERALIZED14:
        FokkerPlanck::updateVelocities_impl<FokkerPlanck::ModelTy::GENERALIZED14,velocity_integration,position_integration>(
            pfirst,plast,c,dt,gas,rng, production_approximation );
        break;
      case FokkerPlanck::ModelTy::GENERALIZED20:
        FokkerPlanck::updateVelocities_impl<FokkerPlanck::ModelTy::GENERALIZED20,velocity_integration,position_integration>(
            pfirst,plast,c,dt,gas,rng, production_approximation );
        break;
      case FokkerPlanck::ModelTy::GENERALIZED21:
        FokkerPlanck::updateVelocities_impl<FokkerPlanck::ModelTy::GENERALIZED21,velocity_integration,position_integration>(
            pfirst,plast,c,dt,gas,rng, production_approximation );
        break;
      case FokkerPlanck::ModelTy::GENERALIZED26:
        FokkerPlanck::updateVelocities_impl<FokkerPlanck::ModelTy::GENERALIZED26,velocity_integration,position_integration>(
            pfirst,plast,c,dt,gas,rng, production_approximation );
        break;
      default:
        MYASSERT(false,"invalid/unknown model configured");
        break;
    }

  }


  template<
  VelocityUpdate::FokkerPlanck::VelocityIntegration velocity_integration
  >
  void dispatch_by_pI_and_model_updateVelocities(
      const VelocityUpdate::FokkerPlanck::ModelTy model,
      VelocityUpdate::FokkerPlanck::PositionIntegration position_integration,
      Particles::Particle* pfirst,
      Particles::Particle* plast,
      CellData& c,
      const double dt,
      const GasModel* const gas,
      MyRandom::Rng& rng,
      const GasModel::Approx production_approximation
  ) {

    using namespace VelocityUpdate;

    switch (position_integration) {
      case FokkerPlanck::PositionIntegration::EULER:
        dispatch_by_model_updateVelocities<velocity_integration,FokkerPlanck::PositionIntegration::EULER>(
            model, pfirst, plast, c, dt, gas, rng, production_approximation);
        break;
      case FokkerPlanck::PositionIntegration::JOINT:
        dispatch_by_model_updateVelocities<velocity_integration,FokkerPlanck::PositionIntegration::JOINT>(
            model, pfirst, plast, c, dt, gas, rng, production_approximation);
        break;
      case FokkerPlanck::PositionIntegration::CONDITIONAL:
        dispatch_by_model_updateVelocities<velocity_integration,FokkerPlanck::PositionIntegration::CONDITIONAL>(
            model, pfirst, plast, c, dt, gas, rng, production_approximation);
        break;
      default:
        MYASSERT(false,"invalid/unknown position integration configured");
        break;
    }
  }

  void dispatch_by_vI_pI_and_model_updateVelocities(
      const VelocityUpdate::FokkerPlanck::ModelTy model,
      VelocityUpdate::FokkerPlanck::PositionIntegration position_integration,
      VelocityUpdate::FokkerPlanck::VelocityIntegration velocity_integration,
      Particles::Particle* pfirst,
      Particles::Particle* plast,
      CellData& c,
      const double dt,
      const GasModel* const gas,
      MyRandom::Rng& rng,
      const GasModel::Approx production_approximation
  ) {

    using namespace VelocityUpdate;

    switch (velocity_integration) {
      case FokkerPlanck::VelocityIntegration::STANDARD:
        dispatch_by_pI_and_model_updateVelocities<FokkerPlanck::VelocityIntegration::STANDARD>(
            model, position_integration, pfirst, plast, c, dt, gas, rng, production_approximation);
        break;
      case FokkerPlanck::VelocityIntegration::TRANSFORMED:
        dispatch_by_pI_and_model_updateVelocities<FokkerPlanck::VelocityIntegration::TRANSFORMED>(
            model, position_integration, pfirst, plast, c, dt, gas, rng, production_approximation);
        break;
      default:
        MYASSERT(false,"invalid/unknown velocity integration configured");
        break;
    }
  }


}

VelocityUpdate::CollisionOperatorStatistics
VelocityUpdate::FokkerPlanck::updateVelocities(
    Particles::Particle* pfirst,
    Particles::Particle* plast,
    CellData& c,
    const double dt,
    const GasModel* const gas,
    MyRandom::Rng& rng
) const {

  // dispatch
  dispatch_by_vI_pI_and_model_updateVelocities(
      model,position_integration,velocity_integration,
      pfirst,  plast, c, dt, gas, rng,
      production_approximation);

  CollisionOperatorStatistics cos;
  cos.fp_dsmc_fraction = 1.;
  return cos;
}

VelocityUpdate::FokkerPlanck::VelocityIntegration
velocityIntegrationChoice(std::string choice) {
  using namespace VelocityUpdate;

  if (choice.empty()) return FokkerPlanck::VelocityIntegration::STANDARD;

  MyString::toLower(choice);

  if(choice.compare("standard") == 0)
    return FokkerPlanck::VelocityIntegration::STANDARD;

  if(choice.compare("transformed") == 0)
    return FokkerPlanck::VelocityIntegration::TRANSFORMED;

  MYASSERT(false,"invalid velocity integration \"" + choice + "\"");

  return FokkerPlanck::VelocityIntegration::INVALID;

}

VelocityUpdate::FokkerPlanck::PositionIntegration
positionIntegrationChoice(std::string choice) {
  using namespace VelocityUpdate;

  if (choice.empty()) return FokkerPlanck::PositionIntegration::EULER;

  MyString::toLower(choice);

  if(choice.compare("euler") == 0)
    return FokkerPlanck::PositionIntegration::EULER;

  if(choice.compare("joint") == 0)
    return FokkerPlanck::PositionIntegration::JOINT;

  if(choice.compare("conditional") == 0)
    return FokkerPlanck::PositionIntegration::CONDITIONAL;

  MYASSERT(false,"invalid position integration \"" + choice + "\"");

  return FokkerPlanck::PositionIntegration::INVALID;

}

VelocityUpdate::FokkerPlanck::ModelTy
modelChoice(std::string choice) {
  using namespace VelocityUpdate;

  if (choice.empty()) return FokkerPlanck::ModelTy::CUBIC;

  MyString::toLower(choice);

  if(choice.compare("cubic") == 0)
    return FokkerPlanck::ModelTy::CUBIC;

  if(choice.compare("cubic legacy") == 0)
    return FokkerPlanck::ModelTy::CUBIC_LEGACY;

  if(choice.compare("gfp13") == 0)
    return FokkerPlanck::ModelTy::GENERALIZED13;

  if(choice.compare("gfp14") == 0)
    return FokkerPlanck::ModelTy::GENERALIZED14;

  if(choice.compare("gfp20") == 0)
    return FokkerPlanck::ModelTy::GENERALIZED20;

  if(choice.compare("gfp21") == 0)
    return FokkerPlanck::ModelTy::GENERALIZED21;

  if(choice.compare("gfp26") == 0)
    return FokkerPlanck::ModelTy::GENERALIZED26;

  return FokkerPlanck::ModelTy::INVALID;

}

GasModel::Approx
approxChoice(std::string choice) {
  if (choice.empty()) return GasModel::Approx::GRAD13;

  MyString::toLower(choice);

  if (choice.compare("grad 13") == 0)
    return GasModel::Approx::GRAD13;

  if (choice.compare("grad 26") == 0)
    return GasModel::Approx::GRAD26;

  return GasModel::Approx::INVALID;
}

namespace VelocityUpdate
{

  FokkerPlanck::FokkerPlanck(const Settings::Dictionary& dict)
  : velocity_integration(
      velocityIntegrationChoice( dict.get<std::string>("velocity integration","")) )
  , position_integration(
      positionIntegrationChoice( dict.get<std::string>("position integration","")) )
  , model( modelChoice( dict.get<std::string>("model","") ) )
  , production_approximation( approxChoice( dict.get<std::string>("productions","") ))
  {

    if (MyMPI::isRoot(MPI_COMM_WORLD,0)) {

        std::cout << "using ";
        switch (model) {
          case ModelTy::CUBIC:         std::cout << "cubic";                  break;
          case ModelTy::CUBIC_LEGACY:  std::cout << "cubic legacy";           break;
          case ModelTy::GENERALIZED13: std::cout << "generalized 13 moment";  break;
          case ModelTy::GENERALIZED14: std::cout << "generalized 14 moment";  break;
          case ModelTy::GENERALIZED20: std::cout << "generalized 20 moment";  break;
          case ModelTy::GENERALIZED21: std::cout << "generalized 21 moment";  break;
          case ModelTy::GENERALIZED26: std::cout << "generalized 26 moment";  break;
          case ModelTy::INVALID: MYASSERT(false,"invalid model choice"); break;
        }
        std::cout << " fokker-planck model\n";


        switch (production_approximation) {
          case GasModel::Approx::GRAD13: std::cout << "- Grad 13 moment"; break;
          case GasModel::Approx::GRAD26: std::cout << "- Grad 26 moment"; break;
          case GasModel::Approx::INVALID: MYASSERT(
              false,"invalid choice for production approximation"); break;
        }
        std::cout << " approximation for productions\n";

        switch (velocity_integration) {
          case VelocityIntegration::STANDARD:       std::cout << "- standard";       break;
          case VelocityIntegration::TRANSFORMED:    std::cout << "- transformed";    break;
          default: MYASSERT(
              false,"invalid choice for velocity integration"); break;
        }
        std::cout << " velocity integration\n";

        switch (position_integration) {
          case PositionIntegration::EULER:       std::cout << "- forward Euler"; break;
          case PositionIntegration::JOINT:       std::cout << "- joint";         break;
          case PositionIntegration::CONDITIONAL: std::cout << "- conditional";   break;
          default: MYASSERT(
              false,"invalid choice for position integration"); break;
        }
        std::cout << " position integration\n" << std::endl;

    }


  }

} /* namespace VelocityUpdate */
