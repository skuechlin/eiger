/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * FokkerPlanckStandalone.h
 *
 *  Created on: Nov 26, 2018
 *      Author: kustepha
 */

#ifndef SRC_VELOCITYUPDATE_FOKKERPLANCKSTANDALONE_H_
#define SRC_VELOCITYUPDATE_FOKKERPLANCKSTANDALONE_H_

#include "VelocityUpdateStrategy.h"

#include "Grid/CellData.h"

namespace VelocityUpdate
{

  class FokkerPlanck_Standalone : public Strategy
  {

  private:
    void updateVelocities_impl(
        Particles::Particle* pfirst,
        Particles::Particle* plast,
        CellData& c,
        const double dt,
        const GasModel* const gasModel,
        MyRandom::Rng& rndGen) const;

  public:

    CollisionOperatorStatistics updateVelocities(
        Particles::Particle* pfirst,
        Particles::Particle* plast,
        CellData& c,
        const double dt,
        const GasModel* const gasModel,
        MyRandom::Rng& rndGen
    ) const override {

      updateVelocities_impl(pfirst,plast,c,dt,gasModel,rndGen);


      CollisionOperatorStatistics cos;
      cos.fp_dsmc_fraction = 1.;
      return cos;

    }

  public:
    FokkerPlanck_Standalone () = default;
  };

} /* namespace VelocityUpdate */

#endif /* SRC_VELOCITYUPDATE_FOKKERPLANCKSTANDALONE_H_ */
