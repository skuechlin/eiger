/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * FPDSMC.h
 *
 *  Created on: May 26, 2015
 *      Author: kustepha
 */

#ifndef VELOCITYUPDATE_HYBRID_H_
#define VELOCITYUPDATE_HYBRID_H_

#include <memory>

#include "VelocityUpdateStrategy.h"

namespace VelocityUpdate
{

  class FPDSMC : public Strategy
  {

    std::unique_ptr<Strategy> dsmc_ = 0;
    std::unique_ptr<Strategy> fp_ = 0;

    double invKnCrit_;

  public:

    CollisionOperatorStatistics updateVelocities(
        Particles::Particle* pfirst,
        Particles::Particle* plast,
        CellData& c,
        const double dt,
        const GasModel* const gasModel,
        MyRandom::Rng& rndGen
    ) const override;



    FPDSMC (
        std::unique_ptr<Strategy> dsmc,
        std::unique_ptr<Strategy> fp,
        double invKnCrit = 1.0);
  };

} /* namespace VelocityUpdate */

#endif /* VELOCITYUPDATE_HYBRID_H_ */
