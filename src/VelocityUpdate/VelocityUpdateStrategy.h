/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * VelocityUpdateStrategy.h
 *
 *  Created on: Apr 16, 2014
 *      Author: Stephan Kuechlin
 */

#ifndef VELOCITYUPDATESTRATEGY_H_
#define VELOCITYUPDATESTRATEGY_H_

#include <stdint.h>
#include <memory>

#include "Gas/GasForwardDecls.h"

struct CellData;

namespace Settings  { class Dictionary; }

namespace Particles { class Particle; }

namespace MyRandom  { class Rng; }

namespace VelocityUpdate
{

  struct CollisionOperatorStatistics {
    double num_collisions = 0.;   // [#] number of computed DSMC collisions
    double fp_dsmc_fraction = 0.; // [-] relative number of time steps the cell was updated using the Fokker-Planck scheme
  };

  class Strategy {

  public:

    virtual CollisionOperatorStatistics updateVelocities(
        Particles::Particle* pfirst,
        Particles::Particle* plast,
        CellData& c,
        const double dt,
        const Gas::GasModels::GasModel* const gasModel,
        MyRandom::Rng& rndGen
    ) const = 0;

  protected:

    Strategy(){};

  public:
    virtual ~Strategy() {
    }
  };

  std::unique_ptr<Strategy>
  makeStrategy(const Settings::Dictionary& dict);

} // namespace

#endif /* VELOCITYUPDATESTRATEGY_H_ */
