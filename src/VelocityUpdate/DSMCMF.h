/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * DSMCMF.h
 *
 *  Created on: May 28, 2015
 *      Author: kustepha
 */

#ifndef VELOCITYUPDATE_DSMCMF_H_
#define VELOCITYUPDATE_DSMCMF_H_

#include <VelocityUpdate/DSMC.h>

namespace VelocityUpdate
{

  class DSMCMF : public DSMC
  {
  public:

    CollisionOperatorStatistics updateVelocities(
        Particles::Particle* pfirst,
        Particles::Particle* plast,
    	CellData& c,
    	const double dt,
    	const Gas::GasModels::GasModel* const gasModel,
    	MyRandom::Rng& rndGen ) const override;
  };

} /* namespace VelocityUpdate */

#endif /* VELOCITYUPDATE_DSMCMF_H_ */
