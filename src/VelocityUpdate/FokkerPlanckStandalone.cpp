/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * FokkerPlanckStandalone.cpp
 *
 *  Created on: Nov 26, 2018
 *      Author: kustepha
 */

#include <iterator>
#include <vector>

#include <Eigen/Dense>

#include "Particles/Particle.h"

#include "Primitives/MyIntrin.h"

#include "Primitives/MyRandom.h"
#include "Primitives/NormalDistribution.h"


#include "Gas/GasModel.h"

#include "FokkerPlanck/FokkerPlanck.h"

#include "FokkerPlanckStandalone.h"

struct CenteredMoments {

  double m = 0.; // [kg] mass

  v4df u0i = zero<v4df>();  // [m  s-1] mean velocity

  double u0ij[6]    = {0.}; // [m2 s-2] second order central moments
  double u0ijk[10]  = {0.}; // [m3 s-3] third  order central moments
  double u0ijkl[15] = {0.}; // [m4 s-4] fourth order central moments

  // layout
  //     l k j i
  //  0: 0 0 0 0 **
  //  1: 0 0 0 1
  //  2: 0 0 0 2
  //  3: 0 0 1 1 *
  //  4: 0 0 1 2
  //  5: 0 0 2 2 *
  //
  //  6: 0 1 1 1 *
  //  7: 0 1 1 2 *
  //  8: 0 1 2 2 *
  //  9: 0 2 2 2 *
  //
  // 10: 1 1 1 1 **
  // 11: 1 1 1 2 *
  // 12: 1 1 2 2 **
  // 13: 1 2 2 2 *
  // 14: 2 2 2 2 **


  // [m2 s-2] second order moments
  template<uint8_t i, uint8_t j>
  double u0() const {
    static_assert(i<3,"index i out of bounds");
    static_assert(j<3,"index j out of bounds");
    if (j<i) return u0<j,i>();

    switch (i) {
      case 0: {
        switch (j) {
          case 0: return u0ij[0]; break; //00
          case 1: return u0ij[1]; break; //01
          case 2: return u0ij[2]; break; //02
        }
      } break;
      case 1: {
        switch (j) {
          case 1: return u0ij[3]; break; //11
          case 2: return u0ij[4]; break; //12
        }
      } break;
      case 2: return u0ij[5]; break; // 22
    } // switch i
  }

  // [m3 s-3] third order moments
  template<uint8_t i, uint8_t j, uint8_t k>
  double u0() const {
    static_assert(i<3,"index i out of bounds");
    static_assert(j<3,"index j out of bounds");
    static_assert(k<3,"index k out of bounds");
    if (j<i) return u0<j,i,k>();
    if (k<j) return u0<i,k,j>();

    switch (i) {
      case 0: {
        switch (j) {
          case 0: {
            switch (k) {
              case 0: return u0ijk[0]; break; // 000
              case 1: return u0ijk[1]; break; // 001
              case 2: return u0ijk[2]; break; // 002
            } // switch k
          } break; // case j=0
          case 1: {
            switch (k) {
              case 1: return u0ijk[3]; break; // 011
              case 2: return u0ijk[4]; break; // 012
            } // switch k
          } break; // case j=1
          case 2: return u0ijk[5]; break; // 022
        } // switch j
      } break; // case i=0
      case 1: {
        switch (j) {
          case 1: {
            switch (k) {
              case 1: return u0ijk[6]; break; // 111
              case 2: return u0ijk[7]; break; // 112
            } // switch k
          } break; // case j=1
          case 2: return u0ijk[8]; break; // 122
        } // switch j
      } break; // case i=1
      case 2: return u0ijk[9]; break; // 222
    } // switch i
  }

  // [m4 s-4] fourth order moments
  template<uint8_t i, uint8_t j, uint8_t k, uint8_t l>
  double u0() const {
    static_assert(i<3,"index i out of bounds");
    static_assert(j<3,"index j out of bounds");
    static_assert(k<3,"index k out of bounds");
    static_assert(l<3,"index l out of bounds");
    if (j<i) return u0<j,i,k,l>();
    if (k<j) return u0<i,k,j,l>();
    if (l<k) return u0<i,j,l,k>();

    switch (i) {
      case 0: {
        switch (j) {
          case 0: {
            switch (k) {
              case 0: {
                switch (l) {
                  case 0: return u0ijkl[0]; break; // 0000
                  case 1: return u0ijkl[1]; break; // 0001
                  case 2: return u0ijkl[2]; break; // 0002
                } // switch l
              } break; // case k=0
              case 1: {
                switch (l) {
                  case 1: return u0ijkl[3]; break; // 0011
                  case 2: return u0ijkl[4]; break; // 0012
                } // switch l
              } break; // case k=1
              case 2: return u0ijkl[5]; break; // 0022
            } // switch k
          } break; // case j=0
          case 1: {
            switch (k) {
              case 1: {
                switch (l) {
                  case 1: return u0ijkl[6]; break; // 0111
                  case 2: return u0ijkl[7]; break; // 0112
                } // switch l
              } break; // case k=1
              case 2: return u0ijkl[8]; break; // 0122
            } // switch k
          } break; // case j=1
          case 2: return u0ijkl[9]; break; // 0222
        } // switch j
      } break; // case i=0
      case 1: {
        switch (j) {
          case 1: {
            switch (k) {
              case 1: {
                switch (l) {
                  case 1: return u0ijkl[10]; break; // 1111
                  case 2: return u0ijkl[11]; break; // 1112
                } // switch l
              } break; // case k=1
              case 2: return u0ijkl[12]; break; // 1122
            } // switch k
          } break; // case j=1
          case 2: return u0ijkl[13]; break; // 1222
        } // switch j
      } break; // case i=1
      case 2: return u0ijkl[14]; break; // 2222
    } // switch i
  }

  // [m2 s-2] 1x contracted second order moments
  double u2() const {
    return u0<0,0>() + u0<1,1>() + u0<2,2>();
  }

  // [m3 s-3] 1x contracted third order moments
  template<uint8_t i>
  double u2() const {
    static_assert(i<3,"index i out of bounds");
    return u0<i,0,0>() + u0<i,1,1>() + u0<i,2,2>();
  }

  // [m4 s-4] 1x contracted fourth order moments
  template<uint8_t i, uint8_t j>
  double u2() const {
    static_assert(i<3,"index i out of bounds");
    static_assert(j<3,"index j out of bounds");
    return u0<i,j,0,0>() + u0<i,j,1,1>() + u0<i,j,2,2>();
  }

  // [m4 s-4] 2x contracted fourth order moments
  double u4() const {
    return u2<0,0>() + u2<1,1>() + u2<2,2>();
  }


  CenteredMoments(Particles::Particle* pfirst, Particles::Particle* plast) {

    // sample mass and mean
    for (auto p = pfirst; p != plast; ++p)
      {
        m += (p->m*p->w);
        u0i += (p->m)*(p->w)*p->C;
      }

    u0i /= m;

    // higher order
    for (auto p = pfirst; p != plast; ++p)
      {
        const v4df   V = p->C - u0i;
        const double w = (p->m)*(p->w);

        u0ij[0]     += w*V[0]*V[0];
        u0ijk[0]    += w*V[0]*V[0]*V[0];
        u0ijkl[0]   += w*V[0]*V[0]*V[0]*V[0];

        u0ij[1]     += w*V[1]*V[0];
        u0ijk[1]    += w*V[1]*V[0]*V[0];
        u0ijkl[1]   += w*V[1]*V[0]*V[0]*V[0];

        u0ij[2]     += w*V[2]*V[0];
        u0ijk[2]    += w*V[2]*V[0]*V[0];
        u0ijkl[2]   += w*V[2]*V[0]*V[0]*V[0];

        u0ij[3]     += w*V[1]*V[1];
        u0ijk[3]    += w*V[1]*V[1]*V[0];
        u0ijkl[3]   += w*V[1]*V[1]*V[0]*V[0];

        u0ij[4]     += w*V[2]*V[1];
        u0ijk[4]    += w*V[2]*V[1]*V[0];
        u0ijkl[4]   += w*V[2]*V[1]*V[0]*V[0];

        u0ij[5]     += w*V[2]*V[2];
        u0ijk[5]    += w*V[2]*V[2]*V[0];
        u0ijkl[5]   += w*V[2]*V[2]*V[0]*V[0];

        u0ijk[6]    += w*V[1]*V[1]*V[1];
        u0ijkl[6]   += w*V[1]*V[1]*V[1]*V[0];

        u0ijk[7]    += w*V[2]*V[1]*V[1];
        u0ijkl[7]   += w*V[2]*V[1]*V[1]*V[0];

        u0ijk[8]    += w*V[2]*V[2]*V[1];
        u0ijkl[8]   += w*V[2]*V[2]*V[1]*V[0];

        u0ijk[9]    += w*V[2]*V[2]*V[2];
        u0ijkl[9]   += w*V[2]*V[2]*V[2]*V[0];

        u0ijkl[10]  += w*V[1]*V[1]*V[1]*V[1];

        u0ijkl[11]  += w*V[2]*V[1]*V[1]*V[1];
        u0ijkl[12]  += w*V[2]*V[2]*V[1]*V[1];
        u0ijkl[13]  += w*V[2]*V[2]*V[2]*V[1];

        u0ijkl[14]  += w*V[2]*V[2]*V[2]*V[2];

      } // for all particles

    for (uint8_t i = 0; i <  6; ++i)
      u0ij[i] /= m;

    for (uint8_t i = 0; i < 10; ++i)
      u0ijk[i] /= m;

    for (uint8_t i = 0; i < 15; ++i)
      u0ijkl[i] /= m;

  }

};

inline constexpr double Power(const double x, const int p) {
  return __builtin_powi(x,p);
}

template<class Moments>
inline void set_lhs(const Moments& c, Eigen::Matrix<double,9,9>& A) {

  A( 0, 0 ) = 2.0*c.template u0<0, 0>();
  A( 1, 0 ) = c.template u0<0, 1>();
  A( 2, 0 ) = c.template u0<0, 2>();
  A( 3, 0 ) = 0;
  A( 4, 0 ) = 0;
  A( 5, 0 ) = 0;
  A( 6, 0 ) = 2.0*c.template u0<0, 0, 0>() + c.template u2<0>();
  A( 7, 0 ) = 2.0*c.template u0<0, 0, 1>();
  A( 8, 0 ) = 2.0*c.template u0<0, 0, 2>();
  A( 0, 1 ) = 2.0*c.template u0<0, 1>();
  A( 1, 1 ) = c.template u0<0, 0>() + c.template u0<1, 1>();
  A( 2, 1 ) = c.template u0<1, 2>();
  A( 3, 1 ) = 2.0*c.template u0<0, 1>();
  A( 4, 1 ) = c.template u0<0, 2>();
  A( 5, 1 ) = 0;
  A( 6, 1 ) = 4.0*c.template u0<0, 0, 1>() + c.template u2<1>();
  A( 7, 1 ) = 4.0*c.template u0<0, 1, 1>() + c.template u2<0>();
  A( 8, 1 ) = 4.0*c.template u0<0, 1, 2>();
  A( 0, 2 ) = 2.0*c.template u0<0, 2>();
  A( 1, 2 ) = c.template u0<1, 2>();
  A( 2, 2 ) = c.template u0<0, 0>() + c.template u0<2, 2>();
  A( 3, 2 ) = 0;
  A( 4, 2 ) = c.template u0<0, 1>();
  A( 5, 2 ) = 2.0*c.template u0<0, 2>();
  A( 6, 2 ) = 4.0*c.template u0<0, 0, 2>() + c.template u2<2>();
  A( 7, 2 ) = 4.0*c.template u0<0, 1, 2>();
  A( 8, 2 ) = 4.0*c.template u0<0, 2, 2>() + c.template u2<0>();
  A( 0, 3 ) = 0;
  A( 1, 3 ) = c.template u0<0, 1>();
  A( 2, 3 ) = 0;
  A( 3, 3 ) = 2.0*c.template u0<1, 1>();
  A( 4, 3 ) = c.template u0<1, 2>();
  A( 5, 3 ) = 0;
  A( 6, 3 ) = 2.0*c.template u0<0, 1, 1>();
  A( 7, 3 ) = 2.0*c.template u0<1, 1, 1>() + c.template u2<1>();
  A( 8, 3 ) = 2.0*c.template u0<1, 1, 2>();
  A( 0, 4 ) = 0;
  A( 1, 4 ) = c.template u0<0, 2>();
  A( 2, 4 ) = c.template u0<0, 1>();
  A( 3, 4 ) = 2.0*c.template u0<1, 2>();
  A( 4, 4 ) = c.template u0<1, 1>() + c.template u0<2, 2>();
  A( 5, 4 ) = 2.0*c.template u0<1, 2>();
  A( 6, 4 ) = 4.0*c.template u0<0, 1, 2>();
  A( 7, 4 ) = 4.0*c.template u0<1, 1, 2>() + c.template u2<2>();
  A( 8, 4 ) = 4.0*c.template u0<1, 2, 2>() + c.template u2<1>();
  A( 0, 5 ) = 0;
  A( 1, 5 ) = 0;
  A( 2, 5 ) = c.template u0<0, 2>();
  A( 3, 5 ) = 0;
  A( 4, 5 ) = c.template u0<1, 2>();
  A( 5, 5 ) = 2.0*c.template u0<2, 2>();
  A( 6, 5 ) = 2.0*c.template u0<0, 2, 2>();
  A( 7, 5 ) = 2.0*c.template u0<1, 2, 2>();
  A( 8, 5 ) = 2.0*c.template u0<2, 2, 2>() + c.template u2<2>();
  A( 0, 6 ) = 4.0*c.template u0<0, 0, 0>() + 2.0*c.template u2<0>();
  A( 1, 6 ) = 4.0*c.template u0<0, 0, 1>() + c.template u2<1>();
  A( 2, 6 ) = 4.0*c.template u0<0, 0, 2>() + c.template u2<2>();
  A( 3, 6 ) = 4.0*c.template u0<0, 1, 1>();
  A( 4, 6 ) = 4.0*c.template u0<0, 1, 2>();
  A( 5, 6 ) = 4.0*c.template u0<0, 2, 2>();
  A( 6, 6 ) = -4.0*Power(c.template u0<0, 0>(),2) - 4.0*Power(c.template u0<0, 1>(),2) - 4.0*Power(c.template u0<0, 2>(),2) - 4.0*c.template u0<0, 0>()*c.template u2() - Power(c.template u2(),2) + 8.0*c.template u2<0, 0>() + c.template u4();
  A( 7, 6 ) = -4.0*c.template u0<0, 0>()*c.template u0<0, 1>() - 4.0*c.template u0<0, 1>()*c.template u0<1, 1>() - 4.0*c.template u0<0, 2>()*c.template u0<1, 2>() - 4.0*c.template u0<0, 1>()*c.template u2() + 8.0*c.template u2<0, 1>();
  A( 8, 6 ) = -4.0*c.template u0<0, 0>()*c.template u0<0, 2>() - 4.0*c.template u0<0, 1>()*c.template u0<1, 2>() - 4.0*c.template u0<0, 2>()*c.template u0<2, 2>() - 4.0*c.template u0<0, 2>()*c.template u2() + 8.0*c.template u2<0, 2>();
  A( 0, 7 ) = 4.0*c.template u0<0, 0, 1>();
  A( 1, 7 ) = 4.0*c.template u0<0, 1, 1>() + c.template u2<0>();
  A( 2, 7 ) = 4.0*c.template u0<0, 1, 2>();
  A( 3, 7 ) = 4.0*c.template u0<1, 1, 1>() + 2.0*c.template u2<1>();
  A( 4, 7 ) = 4.0*c.template u0<1, 1, 2>() + c.template u2<2>();
  A( 5, 7 ) = 4.0*c.template u0<1, 2, 2>();
  A( 6, 7 ) = -4.0*c.template u0<0, 0>()*c.template u0<0, 1>() - 4.0*c.template u0<0, 1>()*c.template u0<1, 1>() - 4.0*c.template u0<0, 2>()*c.template u0<1, 2>() - 4.0*c.template u0<0, 1>()*c.template u2() + 8.0*c.template u2<0, 1>();
  A( 7, 7 ) = -4.0*Power(c.template u0<0, 1>(),2) - 4.0*Power(c.template u0<1, 1>(),2) - 4.0*Power(c.template u0<1, 2>(),2) - 4.0*c.template u0<1, 1>()*c.template u2() - Power(c.template u2(),2) + 8.0*c.template u2<1, 1>() + c.template u4();
  A( 8, 7 ) = -4.0*c.template u0<0, 1>()*c.template u0<0, 2>() - 4.0*c.template u0<1, 1>()*c.template u0<1, 2>() - 4.0*c.template u0<1, 2>()*c.template u0<2, 2>() - 4.0*c.template u0<1, 2>()*c.template u2() + 8.0*c.template u2<1, 2>();
  A( 0, 8 ) = 4.0*c.template u0<0, 0, 2>();
  A( 1, 8 ) = 4.0*c.template u0<0, 1, 2>();
  A( 2, 8 ) = 4.0*c.template u0<0, 2, 2>() + c.template u2<0>();
  A( 3, 8 ) = 4.0*c.template u0<1, 1, 2>();
  A( 4, 8 ) = 4.0*c.template u0<1, 2, 2>() + c.template u2<1>();
  A( 5, 8 ) = 4.0*c.template u0<2, 2, 2>() + 2.0*c.template u2<2>();
  A( 6, 8 ) = -4.0*c.template u0<0, 0>()*c.template u0<0, 2>() - 4.0*c.template u0<0, 1>()*c.template u0<1, 2>() - 4.0*c.template u0<0, 2>()*c.template u0<2, 2>() - 4.0*c.template u0<0, 2>()*c.template u2() + 8.0*c.template u2<0, 2>();
  A( 7, 8 ) = -4.0*c.template u0<0, 1>()*c.template u0<0, 2>() - 4.0*c.template u0<1, 1>()*c.template u0<1, 2>() - 4.0*c.template u0<1, 2>()*c.template u0<2, 2>() - 4.0*c.template u0<1, 2>()*c.template u2() + 8.0*c.template u2<1, 2>();
  A( 8, 8 ) = -4.0*Power(c.template u0<0, 2>(),2) - 4.0*Power(c.template u0<1, 2>(),2) - 4.0*Power(c.template u0<2, 2>(),2) - 4.0*c.template u0<2, 2>()*c.template u2() - Power(c.template u2(),2) + 8.0*c.template u2<2, 2>() + c.template u4();

}

template<class Moments>
inline void set_rhs(
    const double nur,
    const double d2,
    const Moments& c,
    Eigen::Matrix<double,9,1>& B) {
  B( 0 ) = -nur*(c.template u0<0,0>() - c.template u2()/3.) - d2;
  B( 1 ) = -nur*c.template u0<0,1>();
  B( 2 ) = -nur*c.template u0<0,2>();
  B( 3 ) = -nur*(c.template u0<1,1>() - c.template u2()/3.) - d2;
  B( 4 ) = -nur*c.template u0<1,2>();
  B( 5 ) = -nur*(c.template u0<2,2>() - c.template u2()/3.) - d2;
  B( 6 ) = -nur*(2./3.)*c.template u2<0>();
  B( 7 ) = -nur*(2./3.)*c.template u2<1>();
  B( 8 ) = -nur*(2./3.)*c.template u2<2>();
}

void
VelocityUpdate::FokkerPlanck_Standalone::updateVelocities_impl (
    Particles::Particle* pfirst, Particles::Particle* plast,
    CellData& c,
    const double dt,
    const GasModel* const gm,
    MyRandom::Rng& rng) const
{

  // constants
//  static constexpr double k = 1.380649e-23;
//  static constexpr double m = 66.3e-27;
//  static constexpr double Nc = 200.;
//  static constexpr double N0c = 500.;
//  static constexpr double vol = 1./Nc;
//  static constexpr double n0 = 1.4e20;
//  static constexpr double N0 = N0c*Nc;
//  static constexpr double w = m*n0/N0;
//
//  static constexpr double dt = 2.5e-6;

  const uint64_t N = std::distance(pfirst,plast);

  // rng
  MyRandom::NormalDistribution sn;

//#define SAMPLE
#define USE_CUBICSYSTEMH

#ifdef USE_CUBICSYSTEMH

  // stress tensor relaxation rate
  const double nur = c.p()/c.mu();

  // FP frequency
  const double nu = .5*nur;
  const double d2 = 2.*c.theta()*nu;

  Eigen::Matrix<double,16,1> X;

  FokkerPlanck::cubic_coefficients(X,c,gm,GasModel::Approx::GRAD13);

#else

#ifdef SAMPLE

  // sample particles
  const CenteredMoments cm(pfirst,plast);

  // observables

  double vol = c.volume();

  // density
  const double rho = cm.m / vol;
  // temperature in energy units [m2 s-2]
  const double theta = cm.u2() / 3.;
#else
  const double rho = c.rho();
  const double theta = c.theta();
#endif

  // temperature [K]
  const double T = theta/gm.R();
  // pressure [Pa] = [kg m2 s-2]
  const double p = rho*theta;
  // viscosiy [Pa s]
  const double mu = gm.mu(T);


  // stress tensor relaxation rate
  const double nur = p/mu;

  // FP frequency
  const double nu = .5*nur;

  // diffusion coefficient
  const double d2 = 2.*theta*nu;

  // linear system
  Eigen::Matrix<double,9,9> AA;
  Eigen::Matrix<double,9,1> B;
  Eigen::Matrix<double,9,1> X;

#ifdef SAMPLE
  // set left hand side
  set_lhs(cm, AA);

  // set right hand side
  set_rhs(nur, d2, cm, B);
#else
  // set left hand side
  set_lhs(c, AA);

  // set right hand side
  set_rhs(nur, d2, c, B);
#endif

  // solve for drift coefficients
  X = AA.partialPivLu().solve(B);

#endif

  // diffusion integration factor
  v4df Dfac = MyIntrin::set3(sqrt(-d2*expm1(-2.*nu*dt)/(2.*nu)));

  // linear drift integration factor
  const v4df Mfac = MyIntrin::set3(exp(-nu*dt));

  // non-linear drift integration factor
  const v4df Nfac = MyIntrin::set3(
      dt
      //-exp(-nu*dt)*expm1(-nu*dt)/nu
  );

  // set coefficients, decompose into linear, non-linear part
  const v4df a2_i0 = v4df{X[0] + nu, X[1],      X[2],      0.};
  const v4df a2_i1 = v4df{X[1]     , X[3] + nu, X[4],      0.};
  const v4df a2_i2 = v4df{X[2],      X[4],      X[5] + nu, 0.};
  const v4df a3_i  = v4df{X[6], X[7], X[8], 0.};

  // means
#ifdef SAMPLE
  const v4df u0_i  = cm.u0i;
  const v4df u2    = MyIntrin::set3( cm.u2() );
  const v4df u0_0j = v4df{cm.u0<0,0>(), cm.u0<0,1>(), cm.u0<0,2>(), 0.};
  const v4df u0_1j = v4df{cm.u0<1,0>(), cm.u0<1,1>(), cm.u0<1,2>(), 0.};
  const v4df u0_2j = v4df{cm.u0<2,0>(), cm.u0<2,1>(), cm.u0<2,2>(), 0.};
#undef SAMPLE
#else
  const v4df u0_i  = c.v();
  const v4df u2    = MyIntrin::set3( c.u2() );
  const v4df u0_0j = v4df{c.u0<0,0>(), c.u0<0,1>(), c.u0<0,2>(), 0.};
  const v4df u0_1j = v4df{c.u0<1,0>(), c.u0<1,1>(), c.u0<1,2>(), 0.};
  const v4df u0_2j = v4df{c.u0<2,0>(), c.u0<2,1>(), c.u0<2,2>(), 0.};
#endif



  // compute random numbers
  MyMemory::aligned_array<v4df,4096> rands(N);

  for (uint64_t i = 0; i < N; ++i)
    rands[i] = v4df{sn(rng),sn(rng),sn(rng),0.};//rng.normal3();//v4df{dist(gen),dist(gen),dist(gen),0.};//rng.normal3();

  //  correct sample mean and variance
  v4df mu1 = zero<v4df>();
  v4df mu2 = zero<v4df>();
  if (N>2)
    {
      double count = 0.;
      for (uint64_t i = 0; i < N; ++i)
        {
          count += 1.;
          v4df delta = rands[i] - mu1;
          mu1 += delta/count;
          v4df delta2 = rands[i] - mu1;
          mu2 += delta*delta2;
        }
      mu2 /= count;
      mu2 = MyIntrin::sqrt4(mu2) + inactive4df<3>();
      Dfac /= mu2;
    }

  // a-priori ensemble energy
  double e_pre = 0.;
  // a-posteriori ensemble energy
  double e_post = 0.;


  // random number use counter
  uint64_t rcnt = 0;

  // update velocities
  for (auto p = pfirst; p != plast; ++p) {

      const v4df Vi     = p->C - u0_i;
      const v4df V0     = MyIntrin::set3((double)(Vi[0]));
      const v4df V1     = MyIntrin::set3((double)(Vi[1]));
      const v4df V2     = MyIntrin::set3((double)(Vi[2]));
      const v4df VkVk   = V0*V0 + V1*V1 + V2*V2;

      e_pre += VkVk[0];

      p->C =
          Mfac*Vi + // linear drift
          Nfac*( // non-linear drift
              a2_i0*V0 + a2_i1*V1 + a2_i2*V2 + // quadratic
              a3_i*( VkVk      - u2  ) + // cubic 1
              2.*( // cubic 2
                  a3_i[0]*(V0*Vi - u0_0j) +
                  a3_i[1]*(V1*Vi - u0_1j) +
                  a3_i[2]*(V2*Vi - u0_2j)
              )
          ) +
          Dfac*(rands[rcnt++] - mu1); // diffusion

      e_post += MyIntrin::sdot3(p->C,p->C);
  }


  // energy correction factor
  const double e_fac = sqrt(e_pre/e_post);

  // correct energy and re-set mean
  for (auto p = pfirst; p != plast; ++p)
    Particles::set_velocity(p,u0_i + e_fac*(p->C));


}
