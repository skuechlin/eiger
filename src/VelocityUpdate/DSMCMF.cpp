/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * DSMCMF.cpp
 *
 *  Created on: May 28, 2015
 *      Author: kustepha
 */

#include <Grid/CellData.h>
#include <math.h>
#include <iomanip>

#include "Gas/GasModel.h"
#include "Primitives/MyRandom.h"
#include "Particles/ParticleCollection.h"

#include "DSMCMF.h"

namespace VelocityUpdate
{

  CollisionOperatorStatistics DSMCMF::updateVelocities(
      Particles::Particle* pfirst,
      Particles::Particle* plast,
      CellData& c,
      const double dt,
      const Gas::GasModels::GasModel* const gm,
      MyRandom::Rng& rndGen
  ) const
  {

    // this should only be executed once (pray it is thread safe)
    static double sigmaCr_max( gm->sigmaCrMax() );

    const uint64_t N = std::distance(pfirst,plast);

    if (N < 2)
      return {};

    const double Nd = static_cast<double>(N);

    //  majorant frequency
//    const double nu_majorant =  0.5 * static_cast<double>(N-1) * c.n() * sigmaCr_max;
    const double nu_majorant = 0.5*(Nd-1.)*Nd*(c.n()/c.Np()) * sigmaCr_max;

    // time over which to perform collisions, in units of inverse majorant frequency
    const double coll_t = (c.coll_carry_ + dt) *nu_majorant;

    // time counter
    double t = 0.;

    // number of collision pairs to sample
    uint64_t npairs = 0;

    // sample collision time

    while (true)
      {
        const double ddt = -log( rndGen.uniform() );
        t += ddt;
        if ( t > coll_t){ t -= ddt; break; }
        ++npairs;
      }

    if (npairs < 1)
      return {};


    // set the collision carry counter to the remaining collision sampling time
    c.coll_carry_ = fmax(coll_t - t,0.) / nu_majorant;

    // probability of (rotationally) in-elastic collision
    // (redistribution involving rotational degrees)
    const double p_rredist = gm->pRotRedist(c.T());

    CollisionOperatorStatistics cs;

    cs.num_collisions = select_and_collide(
        pfirst,
        plast,
        npairs,
        gm,
        rndGen,
        sigmaCr_max,
        p_rredist );

    return cs;

  }

} /* namespace VelocityUpdate */
