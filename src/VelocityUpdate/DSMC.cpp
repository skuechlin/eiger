/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * DSMC.cpp
 *
 *  Created on: May 12, 2015
 *      Author: kustepha
 */



#include <Grid/CellData.h>
#include "DSMC.h"

#include <cmath>
#include <array>
#include <list>

#include "Gas/GasModel.h"

#include "Particles/ParticleCollection.h"

#include "Primitives/MyRandom.h"



namespace VelocityUpdate
{


  double redist_rot(
      Particles::Particle* p1,
      Particles::Particle* p2,
      const double mr,
      const double crsq,
      const Gas::GasModels::GasModel* const gm,
      MyRandom::Rng& rndGen   )
  {
    // perform rotationally in-elastic collision

    // ! --- energies multiplied by 2 --- !

    // internal energies
    const v2df o1sq = (p1->Omega)*(p1->Omega);
    const v2df o2sq = (p2->Omega)*(p2->Omega);
    const double ei1_pre = p1->m*(o1sq[0]+o1sq[1]);
    const double ei2_pre = p2->m*(o2sq[0]+o2sq[1]);
    double ei1 = ei1_pre;
    double ei2 = ei2_pre;

    // translational energy
    const double et_pre = mr*crsq;
    double et = et_pre;

    // redistribute, using appropriate number of classical internal modes
    gm->redist<sizeof(p1->Omega)/sizeof(double)>(&et, &ei1, &ei2, rndGen);

    // scale rotational degrees to match post coll. rotational energy
    p1->Omega  *= sqrt( ei1/ei1_pre );
    p2->Omega  *= sqrt( ei2/ei2_pre );

    // return relevant fraction for relative velocity
    return sqrt( et/et_pre );
  }


  uint64_t DSMC::select_and_collide(
      Particles::Particle* pfirst,
      Particles::Particle* plast,
      const uint64_t npairs,
      const Gas::GasModels::GasModel* const gm,
      MyRandom::Rng& rng,
      double& sigmaCr_max,
      const double p_rredist ) const
  {

    const uint64_t N = std::distance(pfirst,plast);

    std::uniform_int_distribution<uint64_t> u(0,N-1);

    uint64_t n_coll = 0;

    for (uint64_t i = 0; i < npairs; ++i) {
        // for all candidates

        // choose first particle
        Particles::Particle* p1 = pfirst + u(rng);

        // choose second particle != first particle
        Particles::Particle* p2;
        do{p2 = pfirst + u(rng);} while(__builtin_expect(p1==p2,false));

        // compute relative velocity and its magnitude
        v4df cr = p1->C - p2->C;
        const double cr2 = MyIntrin::sabssq3(cr);

//        MYASSERT(MyIntrin::isfinite(cr),"non-finite relative velocity");

        // compute reduced mass
        const double mr = ((p1->m) * (p2->m)) / (p1->m + p2->m);

        // compute product of cross-section and relative velocity magnitude
        const double sigmaCr = gm->sigmaTCr(mr,cr2);

        // potentially update max of this product
        if ( __builtin_expect(sigmaCr > sigmaCr_max,false) )
          {

#pragma omp critical (COUT)
            {
              std::cout << "sigmaCr_max " << sigmaCr_max;

              sigmaCr_max = 1.1*sigmaCr;

              std::cout << " updated to " << sigmaCr_max << std::endl;
            }

          }

        // accept/reject collision
        if( sigmaCr > (sigmaCr_max * rng.uniform() ) ) {
            // collide

            // accept/reject rotationally in-elastic collision
            if (p_rredist > rng.uniform()) {
                // redistribute rotational energy
                cr *= redist_rot(p1,p2,mr,cr2,gm,rng);
//                MYASSERT(MyIntrin::isfinite(cr),"non-finite relative velocity");
            }

            const double mr1 = mr/p2->m;
            const double mr2 = mr/p1->m;

            // compute mean (center of mass) velocity
            const v4df cm  = mr1*(p1->C) + mr2*(p2->C);

            // compute new, post-collision relative velocity
            cr = gm->collide(cr,rng);

            // update velocities
            Particles::set_velocity(p1,  cm + mr2*cr );
            Particles::set_velocity(p2,  cm - mr1*cr );

            ++n_coll;
        } // if collision
    } // for all collision candidates


    return n_coll;


  }



  CollisionOperatorStatistics DSMC::updateVelocities(
      Particles::Particle* pfirst,
      Particles::Particle* plast,
      CellData& c,
      const double dt,
      const Gas::GasModels::GasModel* const gm,
      MyRandom::Rng& rndGen
  ) const
  {

    // this should only be executed once (pray it is thread safe)
    static double sigmaCr_max( gm->sigmaCrMax() );

    const uint64_t N = std::distance(pfirst,plast);

    if (N < 2)
      return {};

    const double Nd = static_cast<double>(N);



    // number of potential pairs
//    const double npairsd = 0.5*Nd*c.n() * sigmaCr_max * dt + rndGen.uniform();

    const double npairsd = 0.5*(Nd-1.)*Nd*(c.n()/c.Np()) * sigmaCr_max * dt + c.coll_carry_;
    //    const double npairsd = 0.5 * sigmaCr_max * c.n() * static_cast<double>(N-1) * dt + c.coll_carry_;
    const double npairsd_floor = floor(npairsd); // truncate
    // set carry to remainder
    c.coll_carry_ = npairsd - npairsd_floor;

    // the actual number of collision pairs to sample
    const uint64_t npairs = static_cast<uint64_t>( npairsd ); // static_cast truncates


    if (npairs < 1)
      return {};


    // probability of (rotationally) in-elastic collision
    // (redistribution involving rotational degrees)
    const double p_rredist = gm->pRotRedist(c.T());

    CollisionOperatorStatistics cs;

    cs.num_collisions = select_and_collide(
        pfirst,
        plast,
        npairs,
        gm,
        rndGen,
        sigmaCr_max,
        p_rredist );

    return cs;

  }

} /* namespace VelocityUpdate */
