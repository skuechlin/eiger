/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * Inflow.h
 *
 *  Created on: Dec 22, 2016
 *      Author: kustepha
 */

#ifndef INFLOW_H_
#define INFLOW_H_

#include <vector>
#include <memory>
#include <functional>
#include <mpi.h>

namespace Grids{ class Grid;}
namespace Shapes{ class Shape; }

namespace Gas
{
  class MDF;
  namespace GasModels{ class GasModel; }
}

namespace Settings{ class Dictionary;}

namespace Particles
{
  class Particle;
  class ParticleCollection;
}

namespace MyRandom{ class Rng;}

namespace Boundary{ class BoundaryManager; }


class Inflow
{
    // Inflow owns shapes and associated conditions


    uint64_t n_elements_;

    std::vector< const Shapes::Shape* > shape_;
    std::vector< std::shared_ptr<Gas::MDF> > mdf_;

    std::vector< const Shapes::Shape* > shape_element_;
    std::vector< uint8_t    > element_level_;
//    std::vector< double     > element_weight_factor_;
    std::vector< uint64_t   > element_centroid_rank_;
    std::vector< double     > element_average_influx_;
    std::vector< uint64_t   > element_influx_realization_;

    double maxMostProbableSpeed_;

  private:

    uint64_t computeInfluxRealizations(
        const double dt_ref,
        const uint64_t rank,
        MyRandom::Rng& rndGen);

    void sampleParticle(
        Particles::Particle* const prt,
        const double el_dt,
        const double el_wf,
        const uint64_t el,
        MyRandom::Rng& rndGen) const;

  public:

    void updateElementRanks(const Grids::Grid *const nodeGrid);

  public:

    double maxMostProbableSpeed() const { return maxMostProbableSpeed_; };

  public:

    template<typename WFFunT>
    void sample(
        Particles::ParticleCollection& prts,
        const double dt_ref,
        WFFunT&& weightFacFun,
        const uint64_t rank,
        MyRandom::Rng& rndGen);

  public:

    Inflow(
        const bool local_ts,
        const double FN,
        const Boundary::BoundaryManager* bndMgr,
        const Gas::GasModels::GasModel* gasModel,
        const std::string& inputDirectory,
        const MPI_Comm& comm);

    ~Inflow();
};

#include "Inflow.hpp"


#endif /* INFLOW_H_ */
