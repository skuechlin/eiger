/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * InflowConstructor.cpp
 *
 *  Created on: Jan 3, 2017
 *      Author: kustepha
 */


#include <Gas/PDF/MDF.h>
#include <mpi.h>
#include <string>
#include <unordered_set>

#include "Inflow.h"

#include "Boundary/Boundary.h"
#include "Boundary/BoundaryManager/BoundaryManager.h"

#include "SimulationBox/SimulationBox.h"

#include "Settings/Dictionary.h"

#include "Primitives/MyError.h"
#include "Primitives/MyString.h"

#include "Gas/GasModel.h"



Inflow::Inflow(
    const bool local_ts,
    const double FN,
    const Boundary::BoundaryManager* bndMgr,
    const Gas::GasModels::GasModel* gasModel,
    const std::string& inputDirectory,
    const MPI_Comm& comm)
{

  const std::unordered_set<std::string> inletCondNames =
      {
          "inlet",
          "stream",
          "inflow"
      };


  std::string cndType;

  n_elements_ = 0;


  // add all inflows based on boundaries with appropriate conditions
  for (uint i = 0; i < bndMgr->nBoundaries(); ++i)
    {
      auto b = bndMgr->getBoundary(i);

      for (uint back = 0; back <= 1; ++back)
        {

          const Settings::Dictionary* cndDict = nullptr;

          if (back) {
              if (b->hasBackStrat()) cndDict = b->backCndDict();
              else continue;
          }
          else {
              if (b->hasFrontStrat()) cndDict = b->frontCndDict();
              else continue;
          }

          if (cndDict->isEmpty())
            continue;


          cndType = cndDict->get<std::string>("type");
          MyString::toLower(cndType);

          if ( inletCondNames.find(cndType) != inletCondNames.end() )
            { // boundary has inflow condition

              if (local_ts)
                {
                  MYASSERT(b->level() != uint8_t(-1),
                           "inflow boundary \"" + b->name() + "\" missing explicit level specification");

                  MYASSERT(b->level_permanent_tfd() == 1,
                           "inflow boundary \"" + b->name() + "\" must have explicit permanent level");
                }

              shape_.push_back( b->shape() );

              const uint64_t nElements = shape_.back()->nElements();

              n_elements_ += nElements;

              element_level_.reserve(element_level_.size()+nElements);
              for (uint64_t i = 0; i < nElements; ++i )
                element_level_.push_back(b->level() != uint8_t(-1) ? b->level() : 0);

              Gas::makeNMDF( mdf_, nElements, gasModel, FN, *cndDict, inputDirectory, comm );


            }
        } // for front and back

    } // for all boundaries

  // finalize

  // allocate storage
  shape_element_.reserve( n_elements_ );
  element_centroid_rank_.resize( n_elements_, 0 );
  element_average_influx_.resize( n_elements_ );
  element_influx_realization_.resize( n_elements_ );

  const uint64_t nShapes = shape_.size();

  for (uint64_t i = 0; i < nShapes; ++i)
    {
      const Shapes::Shape *const curr_shape = shape_[i];

      const uint64_t curr_n_elements = curr_shape->nElements();

      for (uint64_t j = 0; j < curr_n_elements; ++j)
        {
          shape_element_.push_back( curr_shape->element(j)  );
        }

    }

  //  for (auto se : shape_element_)
  //    std::cout << se->centroid().transpose() << std::endl;

  // compute average influx per element and max most probable speed
  maxMostProbableSpeed_ = 0.;

  for (uint64_t i = 0; i < n_elements_; ++i)
    {
      const Shapes::Shape *const curr_shape_elem = shape_element_[i];
      element_average_influx_[i] = curr_shape_elem->area() * mdf_[i]->meanFlux( curr_shape_elem->unitNormal( curr_shape_elem->centroid() ) );
      maxMostProbableSpeed_ = std::max( maxMostProbableSpeed_, mdf_[i]->mostProbableSpeed() );
    }

} // CTOR
