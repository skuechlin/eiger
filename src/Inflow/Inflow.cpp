/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*

 * Inflow.cpp
 *
 *  Created on: Jan 3, 2017
 *      Author: kustepha
 */


#include <Gas/PDF/MDF.h>
#include <omp.h>
#include <random>

#include "Inflow.h"

#include "Grid/Grid.h"

#include "Geometry/Shape.h"

#include "Primitives/MyError.h"
#include "Primitives/MyIntrin.h"
#include "Primitives/Partitioning.h"

#include "Particles/ParticleCollection.h"




void Inflow::updateElementRanks(const Grids::Grid *const nodeGrid)
{
  if (nodeGrid->nCells() < 2)
    return;

  auto topo = nodeGrid->topology();

#pragma omp for schedule(static)
  for (uint64_t i = 0; i < n_elements_; ++i)
    {
      Eigen::Vector4d c = shape_element_[i]->centroid();
      c += Shapes::ABSEPS*shape_element_[i]->unitNormal(c);
      element_centroid_rank_[i] = nodeGrid->locateKey( topo->pos2Key( from_eigen(c) ) );
      MYASSERT(element_centroid_rank_[i] < nodeGrid->nCells(),
               "invalid element centroid rank");
    }

}


uint64_t Inflow::computeInfluxRealizations(
    const double dt_ref,
    const uint64_t rank,
    MyRandom::Rng& rndGen)
{

  MyRandom::Rng rng(rndGen); // thread local rng


  typedef std::poisson_distribution<uint64_t> DistT;
  DistT d; // thread local distribution

  static uint64_t total; // shared total

#pragma omp single
  total = 0;

#pragma omp for schedule(static) reduction(+:total)
  for (uint64_t i = 0; i < n_elements_; ++i)
    {
      if (element_centroid_rank_[i] == rank)
        {
          d.param( DistT::param_type(
              element_average_influx_[i]*dt_ref /* (* element_weight_factor_[i]/element_weight_factor_[i] == 1.) */
              ) ); // set mean
          const uint64_t flux = d(rng); // sample
          element_influx_realization_[i] = flux; // save
          total += flux; // tally
        }
      else
        element_influx_realization_[i] = 0;
    }

  return total;

}

void Inflow::sampleParticle(
    Particles::Particle* const prt,
    const double el_dt,
    const double el_wf,
    const uint64_t el,
    MyRandom::Rng& rndGen) const
{

  Eigen::Vector4d R = shape_element_[el]->sample(rndGen);
  Eigen::Vector4d N = shape_element_[el]->unitNormal(R);
  R += Shapes::ABSEPS*N;

  Eigen::Vector4d C = mdf_[el]->sampleHalfspaceVelocity(N,rndGen);

  prt->R = from_eigen(R);
  Particles::set_velocity(prt,from_eigen(C));
  prt->Omega =  from_eigen(mdf_[el]->sampleRotation(rndGen));
  prt->Xi = from_eigen(mdf_[el]->sampleVibration(rndGen));
  prt->dt = el_dt*rndGen.uniform();
  prt->w = el_wf*mdf_[el]->w();
  prt->m = mdf_[el]->m();

  //  MYASSERT(prt.isFinite(),"sampled invalid particle");
}



Inflow::~Inflow() = default;
