/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * Inflow.hpp
 *
 *  Created on: Jun 14, 2018
 *      Author: kustepha
 */

#ifndef SRC_INFLOW_INFLOW_HPP_
#define SRC_INFLOW_INFLOW_HPP_

#include "Particles/ParticleCollection.h"
#include "Primitives/MyError.h"

template<typename WFFunT>
void Inflow::sample(
    Particles::ParticleCollection& prts,
    const double dt_ref,
    WFFunT&& weightFacFun,
    const uint64_t rank,
    MyRandom::Rng& rndGen)
{

//
//#pragma omp for schedule(static)
//  for( uint64_t i = 0; i < n_elements_; ++i)
//    element_weight_factor_[i] = weightFacFun(element_level_[i]);

  const uint64_t total_num_to_sample = computeInfluxRealizations(dt_ref,rank,rndGen);

  typedef Particles::ParticleCollection::ResizeMode Mode;
  const auto& flag = Particles::ParticleCollection::resizeflag;

  // allocate particle storage
#pragma omp single
  prts.resize(prts.size() + total_num_to_sample, flag(Mode::ALL) | flag(Mode::KEEP_CURRENT) );


  // partition sampling
  const uint64_t threadnum  = omp_get_thread_num();
  const uint64_t nthreads   = omp_get_num_threads();
  const uint64_t begin      = MyPartitioning::partitionStart(  threadnum, total_num_to_sample, nthreads );
  const uint64_t n          = MyPartitioning::partitionLength( threadnum, total_num_to_sample, nthreads );
  const uint64_t offset     = prts.size() - total_num_to_sample;
  uint64_t       prt        = offset + begin;
  const uint64_t end        = prt + n;


  if (n > 0)
    {
      // find starting element
      uint64_t sum = 0;
      uint64_t element = 0;

      while ( element < n_elements_ && (sum += element_influx_realization_[element]) < begin)
        ++element;

      // sampling

      uint64_t n_to_sample = sum-begin; // first (potentially partial) element
      while (prt < end)
        {
          const double el_wf = weightFacFun(element_level_[element]);
          const double el_dt = dt_ref*el_wf;

          for (uint64_t j = 0; j < n_to_sample && prt < end; ++j, ++prt)
            sampleParticle(prts.getParticle(prt),el_dt,el_wf,element,rndGen);

          ++element;

          if (element < n_elements_)
            n_to_sample = element_influx_realization_[element];
          else
            MYASSERT(prt >= end,"sampling incomplete");
        }

    }

#pragma omp barrier // wait till all particles are sampled



}


#endif /* SRC_INFLOW_INFLOW_HPP_ */
