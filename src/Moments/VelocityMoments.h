/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * VelocityMoments.h
 *
 *  Created on: Aug 9, 2017
 *      Author: kustepha
 */

#ifndef VELOCITYMOMENTS_H_
#define VELOCITYMOMENTS_H_

#include "Primitives/MyIntrin.h"
#include "Primitives/MyArithm.h"
#include "Primitives/Box.h"
#include "Particles/ParticleCollection.h"


namespace Moments
{

  struct alignas(32) VelocityMoments {

    // S: weighted power sums
    // C: weighted centered power sums
    // F: weighted convective moments
    // rho: weighted centered moments
    // u: centered velocity moments (weighted centered moments divided by density)

  private:

    v4df S0_i           = zero<v4df>(); // 1st order                // use 0--2
    double S0_ji[8]     = {0.};         // 2nd order                // use 0--5
    double S0_kji[12]   = {0.};         // 3rd order                // use 0--9
    double S0_lkji[16]  = {0.};         // 4th order                // use 0--14
    double S1_lkji[16]  = {0.};         // 1x contracted 6th order  // use 0--14
    double S0_mlkji[23] = {0.};         // 5th order                // use 0--20
    double S0           = 1.e-60;       // 0th order (sum of weights)

    // gradients

    v4df dS0_dx_m    = zero<v4df>(); // gradient of sum of weights

    v4df dS0_0_dx_m  = zero<v4df>(); // gradient of m*v[0]
    v4df dS0_1_dx_m  = zero<v4df>(); // gradient of m*v[1]
    v4df dS0_2_dx_m  = zero<v4df>(); // gradient of m*v[2]

    v4df dS0_00_dx_m = zero<v4df>(); // gradient of m*v[0]*v[0]
    v4df dS0_01_dx_m = zero<v4df>(); // gradient of m*v[0]*v[1]
    v4df dS0_02_dx_m = zero<v4df>(); // gradient of m*v[0]*v[2]
    v4df dS0_11_dx_m = zero<v4df>(); // gradient of m*v[1]*v[1]
    v4df dS0_12_dx_m = zero<v4df>(); // gradient of m*v[1]*v[2]
    v4df dS0_22_dx_m = zero<v4df>(); // gradient of m*v[2]*v[2]


    // derived:
    v4df v_  = zero<v4df>(); // mean velocity, == S_i/S0;
    double vsq = 0.; // squared norm of mean velocity
    double pad[3] = {0.};

  private:

    // sampling

    template<uint8_t nd>
    __attribute__((hot))
    inline void add_sample(
        const Particles::Particle* p,
        const v4df& cntr,
        const v4df& hrecp) {

      const double C0   = p->C[0];
      const double C1   = p->C[1];
      const double C2   = p->C[2];
      const double w    = (p->w)*(p->m);
      const double e    = w*(C0*C0 + C1*C1 + C2*C2);

      S0_i += w*(p->C);

      S0_ji[0] += w*C0*C0;
      S0_ji[1] += w*C0*C1;
      S0_ji[2] += w*C0*C2;
      S0_ji[3] += w*C1*C1;
      S0_ji[4] += w*C1*C2;
      S0_ji[5] += w*C2*C2;

      S0_kji[0] += w*C0*C0*C0;
      S0_kji[1] += w*C0*C0*C1;
      S0_kji[2] += w*C0*C0*C2;
      S0_kji[3] += w*C0*C1*C1;
      S0_kji[4] += w*C0*C1*C2;
      S0_kji[5] += w*C0*C2*C2;
      S0_kji[6] += w*C1*C1*C1;
      S0_kji[7] += w*C1*C1*C2;
      S0_kji[8] += w*C1*C2*C2;
      S0_kji[9] += w*C2*C2*C2;

      S0_lkji[0]  += w*C0*C0*C0*C0;
      S0_lkji[1]  += w*C0*C0*C0*C1;
      S0_lkji[2]  += w*C0*C0*C0*C2;
      S0_lkji[3]  += w*C0*C0*C1*C1;
      S0_lkji[4]  += w*C0*C0*C1*C2;
      S0_lkji[5]  += w*C0*C0*C2*C2;
      S0_lkji[6]  += w*C0*C1*C1*C1;
      S0_lkji[7]  += w*C0*C1*C1*C2;
      S0_lkji[8]  += w*C0*C1*C2*C2;
      S0_lkji[9]  += w*C0*C2*C2*C2;
      S0_lkji[10] += w*C1*C1*C1*C1;
      S0_lkji[11] += w*C1*C1*C1*C2;
      S0_lkji[12] += w*C1*C1*C2*C2;
      S0_lkji[13] += w*C1*C2*C2*C2;
      S0_lkji[14] += w*C2*C2*C2*C2;

      S1_lkji[0]  += e*C0*C0*C0*C0;
      S1_lkji[1]  += e*C0*C0*C0*C1;
      S1_lkji[2]  += e*C0*C0*C0*C2;
      S1_lkji[3]  += e*C0*C0*C1*C1;
      S1_lkji[4]  += e*C0*C0*C1*C2;
      S1_lkji[5]  += e*C0*C0*C2*C2;
      S1_lkji[6]  += e*C0*C1*C1*C1;
      S1_lkji[7]  += e*C0*C1*C1*C2;
      S1_lkji[8]  += e*C0*C1*C2*C2;
      S1_lkji[9]  += e*C0*C2*C2*C2;
      S1_lkji[10] += e*C1*C1*C1*C1;
      S1_lkji[11] += e*C1*C1*C1*C2;
      S1_lkji[12] += e*C1*C1*C2*C2;
      S1_lkji[13] += e*C1*C2*C2*C2;
      S1_lkji[14] += e*C2*C2*C2*C2;

      S0_mlkji[0]  += w*C0*C0*C0*C0*C0;
      S0_mlkji[1]  += w*C0*C0*C0*C0*C1;
      S0_mlkji[2]  += w*C0*C0*C0*C0*C2;

      S0_mlkji[3]  += w*C0*C0*C0*C1*C1;
      S0_mlkji[4]  += w*C0*C0*C0*C1*C2;
      S0_mlkji[5]  += w*C0*C0*C0*C2*C2;

      S0_mlkji[6]  += w*C0*C0*C1*C1*C1;
      S0_mlkji[7]  += w*C0*C0*C1*C1*C2;
      S0_mlkji[8]  += w*C0*C0*C1*C2*C2;
      S0_mlkji[9]  += w*C0*C0*C2*C2*C2;

      S0_mlkji[10] += w*C0*C1*C1*C1*C1;
      S0_mlkji[11] += w*C0*C1*C1*C1*C2;
      S0_mlkji[12] += w*C0*C1*C1*C2*C2;
      S0_mlkji[13] += w*C0*C1*C2*C2*C2;
      S0_mlkji[14] += w*C0*C2*C2*C2*C2;

      S0_mlkji[15] += w*C1*C1*C1*C1*C1;
      S0_mlkji[16] += w*C1*C1*C1*C1*C2;
      S0_mlkji[17] += w*C1*C1*C1*C2*C2;
      S0_mlkji[18] += w*C1*C1*C2*C2*C2;
      S0_mlkji[19] += w*C1*C2*C2*C2*C2;
      S0_mlkji[20] += w*C2*C2*C2*C2*C2;

      S0  += w;

      // sample gradients
      const v4df kdx    = K<nd>(p->R,cntr,hrecp);

      dS0_dx_m += w*kdx;

      dS0_0_dx_m += w*C0*kdx;
      dS0_1_dx_m += w*C1*kdx;
      dS0_2_dx_m += w*C2*kdx;

      dS0_00_dx_m += w*C0*C0*kdx;
      dS0_01_dx_m += w*C0*C1*kdx;
      dS0_02_dx_m += w*C0*C2*kdx;
      dS0_11_dx_m += w*C1*C1*kdx;
      dS0_12_dx_m += w*C1*C2*kdx;
      dS0_22_dx_m += w*C2*C2*kdx;

    }


  public:

    template<uint8_t s>
    double S() const {
      static_assert(s<4,"invalid trace number (valid: 0,1,2,3)");
      if constexpr(s==0) return S0;
      if constexpr(s==1) return S<0,0,0>() + S<0,1,1>() + S<0,2,2>();
      if constexpr(s==2) return S<1,0,0>() + S<1,1,1>() + S<1,2,2>();
      if constexpr(s==3) return S<2,0,0>() + S<2,1,1>() + S<2,2,2>();
    }

    template<uint8_t s, uint8_t i>
    double S() const {
      static_assert(s<3,"invalid trace number (valid: 0,1,2)");
      static_assert(i<3,"index i out of bounds");
      if constexpr(s==0) return S0_i[i];
      if constexpr(s==1) return S<0,0,0,i>() + S<0,1,1,i>() + S<0,2,2,i>();
      if constexpr(s==2) return S<1,0,0,i>() + S<1,1,1,i>() + S<1,2,2,i>();
    }

    template<uint8_t s, uint8_t j, uint8_t i>
    double S() const {
      static_assert(s<3,"invalid trace number (valid: 0,1,2)");
      static_assert(i<3,"index i out of bounds");
      static_assert(j<3,"index i out of bounds");
      if constexpr(i<j) return S<s,i,j>();
      constexpr uint8_t m = i+j+(j>0)*1;
      if constexpr(s==0) return S0_ji[m];
      if constexpr(s==1) return S<0,0,0,j,i>() + S<0,1,1,j,i>() + S<0,2,2,j,i>();
      if constexpr(s==2) return S<1,0,0,j,i>() + S<1,1,1,j,i>() + S<1,2,2,j,i>();
    }

    template<uint8_t s, uint8_t k, uint8_t j, uint8_t i>
    double S() const {
      static_assert(s<2,"invalid trace number (valid: 0,1)");
      static_assert(i<3,"index i out of bounds");
      static_assert(j<3,"index j out of bounds");
      static_assert(k<3,"index k out of bounds");
      if constexpr(i<j) return S<s,k,i,j>();
      if constexpr(j<k) return S<s,j,k,i>();
      constexpr uint8_t m = i+j+k+(j>0)*1+(k>0)*2;
      if constexpr(s==0) return S0_kji[m];
      if constexpr(s==1) return S<0,0,0,k,j,i>() + S<0,1,1,k,j,i>() + S<0,2,2,k,j,i>();
    }

    template<uint8_t s, uint8_t l, uint8_t k, uint8_t j, uint8_t i>
    double S() const {
      static_assert(s<2,"invalid trace number (valid: 0,1)");
      static_assert(i<3,"index i out of bounds");
      static_assert(j<3,"index j out of bounds");
      static_assert(k<3,"index k out of bounds");
      static_assert(l<3,"index l out of bounds");
      if constexpr(i<j) return S<s,l,k,i,j>();
      if constexpr(j<k) return S<s,l,j,k,i>();
      if constexpr(k<l) return S<s,k,l,j,i>();
      constexpr uint8_t m = i+j+k+l+(j>0)*1+(k>0)*2+(l>0)*3;
      if constexpr(s==0) return S0_lkji[m];
      if constexpr(s==1) return S1_lkji[m];
    }

    template<uint8_t s, uint8_t m, uint8_t l, uint8_t k, uint8_t j, uint8_t i>
    double S() const {
      static_assert(s<1,"invalid trace number (valid: 0)");
      static_assert(i<3,"index i out of bounds");
      static_assert(j<3,"index j out of bounds");
      static_assert(k<3,"index k out of bounds");
      static_assert(l<3,"index l out of bounds");
      static_assert(m<3,"index m out of bounds");
      if constexpr(i<j) return S<s,m,l,k,i,j>();
      if constexpr(j<k) return S<s,m,l,j,k,i>();
      if constexpr(k<l) return S<s,m,k,l,j,i>();
      if constexpr(l<m) return S<s,l,m,k,j,i>();
      constexpr uint8_t n = i+j+k+l+m+(j>0)*1+(k>0)*2+(l>0)*3+(m>0)*4;
      return S0_mlkji[n];
    }

    template<uint8_t s, uint8_t m>
    double dSdx() const {
      static_assert(s<2,"invalid trace number (valid: 0,1)");
      static_assert(m<3,"index m out of bounds");
      if constexpr(s==0) return dS0_dx_m[m];
      if constexpr(s==1) return dSdx<0,0,0,m>() + dSdx<0,1,1,m>() + dSdx<0,2,2,m>();
    }

    template<uint8_t s, uint8_t i, uint8_t m>
    double dSdx() const {
      static_assert(s<1,"invalid trace number (valid: 0)");
      static_assert(i<3,"index i out of bounds");
      static_assert(m<3,"index m out of bounds");
      if constexpr(i==0) return dS0_0_dx_m[m];
      if constexpr(i==1) return dS0_1_dx_m[m];
      if constexpr(i==2) return dS0_2_dx_m[m];
    }

    template<uint8_t s, uint8_t j, uint8_t i, uint8_t m>
    double dSdx() const {
      static_assert(s<1,"invalid trace number (valid: 0)");
      static_assert(j<3,"index j out of bounds");
      static_assert(i<3,"index i out of bounds");
      static_assert(m<3,"index m out of bounds");

      if constexpr(i<j) return dSdx<s,i,j,m>();

      if constexpr (j==0) {
          if constexpr (i==0) return dS0_00_dx_m[m];
          if constexpr (i==1) return dS0_01_dx_m[m];
          if constexpr (i==2) return dS0_02_dx_m[m];
      }
      if constexpr (j==1) {
          if constexpr (i==1) return dS0_11_dx_m[m];
          if constexpr (i==2) return dS0_12_dx_m[m];
      }
      if constexpr (j==2) {
          return dS0_22_dx_m[m];
      }

    }

  public:

    // centered power sums

    template<uint8_t s>
    double C() const {

      static_assert(s<4,"invalid trace number (valid: 0,1,2,3)");

      if constexpr(s==0) return S<0>();
      if constexpr(s==1) return S<1>() - vsq*S<0>();
      if constexpr(s==2) return S<2>() - 4.0*S<1,0>()*v<0>() - 4.0*S<1,1>()*v<1>() -
          4.0*S<1,2>()*v<2>() + 2.0*vsq*S<1>() - 3.0*vsq*vsq*S<0>() +
          4.0*S<0,0,0>()*v<0>()*v<0>() + 4.0*S<0,1,1>()*v<1>()*v<1>() +
          4.0*S<0,2,2>()*v<2>()*v<2>() + 8.0*S<0,0,1>()*v<0>()*v<1>() +
          8.0*S<0,0,2>()*v<0>()*v<2>() + 8.0*S<0,1,2>()*v<1>()*v<2>();
      if constexpr(s==3) return S<3>() -
          6.0*S<2,0>()*v<0>() - 6.0*S<2,1>()*v<1>() -
          6.0*S<2,2>()*v<2>() + 3.0*vsq*S<2>() -
          12.0*vsq*S<1,0>()*v<0>() - 12.0*vsq*S<1,1>()*v<1>() -
          12.0*vsq*S<1,2>()*v<2>() + 3.0*vsq*vsq*S<1>() +
          12.0*S<1,0,0>()*v<0>()*v<0>() + 12.0*S<1,1,1>()*v<1>()*v<1>() +
          12.0*S<1,2,2>()*v<2>()*v<2>() + 24.0*S<1,0,1>()*v<0>()*v<1>() +
          24.0*S<1,0,2>()*v<0>()*v<2>() + 24.0*S<1,1,2>()*v<1>()*v<2>() -
          48.0*S<0,0,1,2>()*v<0>()*v<1>()*v<2>() - 24.0*S<0,0,0,1>()*v<0>()*v<0>()*v<1>() -
          24.0*S<0,0,0,2>()*v<0>()*v<0>()*v<2>() - 24.0*S<0,0,1,1>()*v<0>()*v<1>()*v<1>() -
          24.0*S<0,0,2,2>()*v<0>()*v<2>()*v<2>() - 24.0*S<0,1,1,2>()*v<1>()*v<1>()*v<2>() -
          24.0*S<0,1,2,2>()*v<1>()*v<2>()*v<2>() - 8.0*S<0,0,0,0>()*v<0>()*v<0>()*v<0>() -
          8.0*S<0,1,1,1>()*v<1>()*v<1>()*v<1>() - 8.0*S<0,2,2,2>()*v<2>()*v<2>()*v<2>() -
          5.0*vsq*vsq*vsq*S<0>() + 12.0*vsq*S<0,0,0>()*v<0>()*v<0>() +
          12.0*vsq*S<0,1,1>()*v<1>()*v<1>() + 12.0*vsq*S<0,2,2>()*v<2>()*v<2>() +
          24.0*vsq*S<0,0,1>()*v<0>()*v<1>() + 24.0*vsq*S<0,0,2>()*v<0>()*v<2>() +
          24.0*vsq*S<0,1,2>()*v<1>()*v<2>();
    }

    template<uint8_t s, uint8_t i>
    double C() const {
      static_assert(s<3,"invalid trace number (valid: 0,1,2)");
      static_assert(i<3,"index i out oS bounds");
      if constexpr(s==0) return 0.0;
      if constexpr(s==1) return S<1,i>() - 2.0*S<0,0,i>()*v<0>() - 2.0*S<0,1,i>()*v<1>() -
          2.0*S<0,2,i>()*v<2>() - S<1>()*v<i>() + 2.0*vsq*S<0>()*v<i>();
      if constexpr(s==2) return S<2,i>() -
          4.0*S<1,0,i>()*v<0>() - 4.0*S<1,1,i>()*v<1>() - 4.0*S<1,2,i>()*v<2>() -
          S<2>()*v<i>() + 2.0*vsq*S<1,i>() -
          4.0*vsq*S<0,0,i>()*v<0>() - 4.0*vsq*S<0,1,i>()*v<1>() - 4.0*vsq*S<0,2,i>()*v<2>() -
          2.0*vsq*S<1>()*v<i>() +
          4.0*S<1,0>()*v<0>()*v<i>() + 4.0*S<1,1>()*v<1>()*v<i>() + 4.0*S<1,2>()*v<2>()*v<i>() +
          4.0*S<0,0,0,i>()*v<0>()*v<0>() + 4.0*S<0,1,1,i>()*v<1>()*v<1>() +
          4.0*S<0,2,2,i>()*v<2>()*v<2>() + 8.0*S<0,0,1,i>()*v<0>()*v<1>() +
          8.0*S<0,0,2,i>()*v<0>()*v<2>() + 8.0*S<0,1,2,i>()*v<1>()*v<2>() -
          8.0*S<0,0,1>()*v<0>()*v<1>()*v<i>() - 8.0*S<0,0,2>()*v<0>()*v<2>()*v<i>() -
          8.0*S<0,1,2>()*v<1>()*v<2>()*v<i>() - 4.0*S<0,0,0>()*v<0>()*v<0>()*v<i>() -
          4.0*S<0,1,1>()*v<1>()*v<1>()*v<i>() - 4.0*S<0,2,2>()*v<2>()*v<2>()*v<i>() +
          4.0*vsq*vsq*S<0>()*v<i>();

    }


    template<uint8_t s, uint8_t i, uint8_t j>
    double C() const {
      static_assert(s<3,"invalid trace number (valid: 0,1,2)");
      static_assert(i<3,"index i out oS bounds");
      static_assert(j<3,"index j out oS bounds");
      if constexpr(s==0) return S<0,i,j>() - S<0>()*v<i>()*v<j>();
      if constexpr(s==1) return S<1,i,j>() + vsq*S<0,i,j>() - 2.0*S<0,0,i,j>()*v<0>() -
          2.0*S<0,1,i,j>()*v<1>() - 2.0*S<0,2,i,j>()*v<2>() - S<1,i>()*v<j>() -
          S<1,j>()*v<i>() + S<1>()*v<i>()*v<j>() + 2.0*S<0,0,i>()*v<0>()*v<j>() +
          2.0*S<0,0,j>()*v<0>()*v<i>() + 2.0*S<0,1,i>()*v<1>()*v<j>() +
          2.0*S<0,1,j>()*v<1>()*v<i>() + 2.0*S<0,2,i>()*v<2>()*v<j>() +
          2.0*S<0,2,j>()*v<2>()*v<i>() - 3.0*vsq*S<0>()*v<i>()*v<j>();
      if constexpr(s==2) return S<2,i,j>() -
          4.0*S<1,0,i,j>()*v<0>() - 4.0*S<1,1,i,j>()*v<1>() -
          4.0*S<1,2,i,j>()*v<2>() - S<2,i>()*v<j>() -
          S<2,j>()*v<i>() + 2.0*vsq*S<1,i,j>() +
          vsq*vsq*S<0,i,j>() + S<2>()*v<i>()*v<j>() -
          4.0*vsq*S<0,0,i,j>()*v<0>() - 4.0*vsq*S<0,1,i,j>()*v<1>() -
          4.0*vsq*S<0,2,i,j>()*v<2>() - 2.0*vsq*S<1,i>()*v<j>() -
          2.0*vsq*S<1,j>()*v<i>() + 4.0*S<1,0,i>()*v<0>()*v<j>() +
          4.0*S<1,0,j>()*v<0>()*v<i>() + 4.0*S<1,1,i>()*v<1>()*v<j>() +
          4.0*S<1,1,j>()*v<1>()*v<i>() + 4.0*S<1,2,i>()*v<2>()*v<j>() +
          4.0*S<1,2,j>()*v<2>()*v<i>() + 4.0*S<0,0,0,i,j>()*v<0>()*v<0>() +
          4.0*S<0,1,1,i,j>()*v<1>()*v<1>() + 4.0*S<0,2,2,i,j>()*v<2>()*v<2>() +
          8.0*S<0,0,1,i,j>()*v<0>()*v<1>() + 8.0*S<0,0,2,i,j>()*v<0>()*v<2>() +
          8.0*S<0,1,2,i,j>()*v<1>()*v<2>() - 8.0*S<0,0,1,i>()*v<0>()*v<1>()*v<j>() -
          8.0*S<0,0,1,j>()*v<0>()*v<1>()*v<i>() - 8.0*S<0,0,2,i>()*v<0>()*v<2>()*v<j>() -
          8.0*S<0,0,2,j>()*v<0>()*v<2>()*v<i>() - 8.0*S<0,1,2,i>()*v<1>()*v<2>()*v<j>() -
          8.0*S<0,1,2,j>()*v<1>()*v<2>()*v<i>() - 4.0*S<1,0>()*v<0>()*v<i>()*v<j>() -
          4.0*S<1,1>()*v<1>()*v<i>()*v<j>() - 4.0*S<1,2>()*v<2>()*v<i>()*v<j>() -
          4.0*S<0,0,0,i>()*v<0>()*v<0>()*v<j>() - 4.0*S<0,0,0,j>()*v<0>()*v<0>()*v<i>() -
          4.0*S<0,1,1,i>()*v<1>()*v<1>()*v<j>() - 4.0*S<0,1,1,j>()*v<1>()*v<1>()*v<i>() -
          4.0*S<0,2,2,i>()*v<2>()*v<2>()*v<j>() - 4.0*S<0,2,2,j>()*v<2>()*v<2>()*v<i>() +
          2.0*vsq*S<1>()*v<i>()*v<j>() + 4.0*vsq*S<0,0,i>()*v<0>()*v<j>() +
          4.0*vsq*S<0,0,j>()*v<0>()*v<i>() + 4.0*vsq*S<0,1,i>()*v<1>()*v<j>() +
          4.0*vsq*S<0,1,j>()*v<1>()*v<i>() + 4.0*vsq*S<0,2,i>()*v<2>()*v<j>() +
          4.0*vsq*S<0,2,j>()*v<2>()*v<i>() - 5.0*vsq*vsq*S<0>()*v<i>()*v<j>() +
          4.0*S<0,0,0>()*v<0>()*v<0>()*v<i>()*v<j>() + 4.0*S<0,1,1>()*v<1>()*v<1>()*v<i>()*v<j>() +
          4.0*S<0,2,2>()*v<2>()*v<2>()*v<i>()*v<j>() + 8.0*S<0,0,1>()*v<0>()*v<1>()*v<i>()*v<j>() +
          8.0*S<0,0,2>()*v<0>()*v<2>()*v<i>()*v<j>() + 8.0*S<0,1,2>()*v<1>()*v<2>()*v<i>()*v<j>();
    }

    template<uint8_t s, uint8_t i, uint8_t j, uint8_t k>
    double C() const {
      static_assert(s<2,"invalid trace number (valid: 0,1");
      static_assert(i<3,"index i out of bounds");
      static_assert(j<3,"index j out of bounds");
      static_assert(k<3,"index k out of bounds");
      if constexpr(s==0) return S<0,i,j,k>() - S<0,i,j>()*v<k>() - S<0,i,k>()*v<j>() -
          S<0,j,k>()*v<i>() + 2.0*S<0>()*v<i>()*v<j>()*v<k>();
      if constexpr(s==1) return S<1,i,j,k>() +
          vsq*S<0,i,j,k>() - 2.0*S<0,0,i,j,k>()*v<0>() -
          2.0*S<0,1,i,j,k>()*v<1>() - 2.0*S<0,2,i,j,k>()*v<2>() -
          S<1,i,j>()*v<k>() - S<1,i,k>()*v<j>() -
          S<1,j,k>()*v<i>() + S<1,i>()*v<j>()*v<k>() +
          S<1,j>()*v<i>()*v<k>() + S<1,k>()*v<i>()*v<j>() -
          vsq*S<0,i,j>()*v<k>() - vsq*S<0,i,k>()*v<j>() -
          vsq*S<0,j,k>()*v<i>() + 2.0*S<0,0,i,j>()*v<0>()*v<k>() +
          2.0*S<0,0,i,k>()*v<0>()*v<j>() + 2.0*S<0,0,j,k>()*v<0>()*v<i>() +
          2.0*S<0,1,i,j>()*v<1>()*v<k>() + 2.0*S<0,1,i,k>()*v<1>()*v<j>() +
          2.0*S<0,1,j,k>()*v<1>()*v<i>() + 2.0*S<0,2,i,j>()*v<2>()*v<k>() +
          2.0*S<0,2,i,k>()*v<2>()*v<j>() + 2.0*S<0,2,j,k>()*v<2>()*v<i>() -
          2.0*S<0,0,i>()*v<0>()*v<j>()*v<k>() - 2.0*S<0,0,j>()*v<0>()*v<i>()*v<k>() -
          2.0*S<0,0,k>()*v<0>()*v<i>()*v<j>() - 2.0*S<0,1,i>()*v<1>()*v<j>()*v<k>() -
          2.0*S<0,1,j>()*v<1>()*v<i>()*v<k>() - 2.0*S<0,1,k>()*v<1>()*v<i>()*v<j>() -
          2.0*S<0,2,i>()*v<2>()*v<j>()*v<k>() - 2.0*S<0,2,j>()*v<2>()*v<i>()*v<k>() -
          2.0*S<0,2,k>()*v<2>()*v<i>()*v<j>() - S<1>()*v<i>()*v<j>()*v<k>() +
          4.0*vsq*S<0>()*v<i>()*v<j>()*v<k>();
    }

    template<uint8_t s, uint8_t i, uint8_t j, uint8_t k, uint8_t l>
    double C() const {
      static_assert(s<2,"invalid trace number (valid: 0,1");
      static_assert(i<3,"index i out of bounds");
      static_assert(j<3,"index j out of bounds");
      static_assert(k<3,"index k out of bounds");
      static_assert(l<3,"index l out of bounds");
      if constexpr(s==0) return S<0,i,j,k,l>() - S<0,i,j,k>()*v<l>() - S<0,i,j,l>()*v<k>() -
          S<0,i,k,l>()*v<j>() - S<0,j,k,l>()*v<i>() + S<0,i,j>()*v<k>()*v<l>() +
          S<0,i,k>()*v<j>()*v<l>() + S<0,i,l>()*v<j>()*v<k>() + S<0,j,k>()*v<i>()*v<l>() +
          S<0,j,l>()*v<i>()*v<k>() + S<0,k,l>()*v<i>()*v<j>() -
          3.0*S<0>()*v<i>()*v<j>()*v<k>()*v<l>();
      if constexpr(s==1) return S<1,i,j,k,l>() +
          vsq*S<0,i,j,k,l>() - 2.0*S<0,0,i,j,k,l>()*v<0>() -
          2.0*S<0,1,i,j,k,l>()*v<1>() - 2.0*S<0,2,i,j,k,l>()*v<2>() -
          S<1,i,j,k>()*v<l>() - S<1,i,j,l>()*v<k>() -
          S<1,i,k,l>()*v<j>() - S<1,j,k,l>()*v<i>() +
          S<1,i,j>()*v<k>()*v<l>() + S<1,i,k>()*v<j>()*v<l>() +
          S<1,i,l>()*v<j>()*v<k>() + S<1,j,k>()*v<i>()*v<l>() +
          S<1,j,l>()*v<i>()*v<k>() + S<1,k,l>()*v<i>()*v<j>() -
          vsq*S<0,i,j,k>()*v<l>() - vsq*S<0,i,j,l>()*v<k>() -
          vsq*S<0,i,k,l>()*v<j>() - vsq*S<0,j,k,l>()*v<i>() +
          2.0*S<0,0,i,j,k>()*v<0>()*v<l>() + 2.0*S<0,0,i,j,l>()*v<0>()*v<k>() +
          2.0*S<0,0,i,k,l>()*v<0>()*v<j>() + 2.0*S<0,0,j,k,l>()*v<0>()*v<i>() +
          2.0*S<0,1,i,j,k>()*v<1>()*v<l>() + 2.0*S<0,1,i,j,l>()*v<1>()*v<k>() +
          2.0*S<0,1,i,k,l>()*v<1>()*v<j>() + 2.0*S<0,1,j,k,l>()*v<1>()*v<i>() +
          2.0*S<0,2,i,j,k>()*v<2>()*v<l>() + 2.0*S<0,2,i,j,l>()*v<2>()*v<k>() +
          2.0*S<0,2,i,k,l>()*v<2>()*v<j>() + 2.0*S<0,2,j,k,l>()*v<2>()*v<i>() +
          vsq*S<0,i,j>()*v<k>()*v<l>() + vsq*S<0,i,k>()*v<j>()*v<l>() +
          vsq*S<0,i,l>()*v<j>()*v<k>() + vsq*S<0,j,k>()*v<i>()*v<l>() +
          vsq*S<0,j,l>()*v<i>()*v<k>() + vsq*S<0,k,l>()*v<i>()*v<j>() -
          2.0*S<0,0,i,j>()*v<0>()*v<k>()*v<l>() - 2.0*S<0,0,i,k>()*v<0>()*v<j>()*v<l>() -
          2.0*S<0,0,i,l>()*v<0>()*v<j>()*v<k>() - 2.0*S<0,0,j,k>()*v<0>()*v<i>()*v<l>() -
          2.0*S<0,0,j,l>()*v<0>()*v<i>()*v<k>() - 2.0*S<0,0,k,l>()*v<0>()*v<i>()*v<j>() -
          2.0*S<0,1,i,j>()*v<1>()*v<k>()*v<l>() - 2.0*S<0,1,i,k>()*v<1>()*v<j>()*v<l>() -
          2.0*S<0,1,i,l>()*v<1>()*v<j>()*v<k>() - 2.0*S<0,1,j,k>()*v<1>()*v<i>()*v<l>() -
          2.0*S<0,1,j,l>()*v<1>()*v<i>()*v<k>() - 2.0*S<0,1,k,l>()*v<1>()*v<i>()*v<j>() -
          2.0*S<0,2,i,j>()*v<2>()*v<k>()*v<l>() - 2.0*S<0,2,i,k>()*v<2>()*v<j>()*v<l>() -
          2.0*S<0,2,i,l>()*v<2>()*v<j>()*v<k>() - 2.0*S<0,2,j,k>()*v<2>()*v<i>()*v<l>() -
          2.0*S<0,2,j,l>()*v<2>()*v<i>()*v<k>() - 2.0*S<0,2,k,l>()*v<2>()*v<i>()*v<j>() -
          S<1,i>()*v<j>()*v<k>()*v<l>() - S<1,j>()*v<i>()*v<k>()*v<l>() -
          S<1,k>()*v<i>()*v<j>()*v<l>() - S<1,l>()*v<i>()*v<j>()*v<k>() +
          S<1>()*v<i>()*v<j>()*v<k>()*v<l>() + 2.0*S<0,0,i>()*v<0>()*v<j>()*v<k>()*v<l>() +
          2.0*S<0,0,j>()*v<0>()*v<i>()*v<k>()*v<l>() + 2.0*S<0,0,k>()*v<0>()*v<i>()*v<j>()*v<l>() +
          2.0*S<0,0,l>()*v<0>()*v<i>()*v<j>()*v<k>() + 2.0*S<0,1,i>()*v<1>()*v<j>()*v<k>()*v<l>() +
          2.0*S<0,1,j>()*v<1>()*v<i>()*v<k>()*v<l>() + 2.0*S<0,1,k>()*v<1>()*v<i>()*v<j>()*v<l>() +
          2.0*S<0,1,l>()*v<1>()*v<i>()*v<j>()*v<k>() + 2.0*S<0,2,i>()*v<2>()*v<j>()*v<k>()*v<l>() +
          2.0*S<0,2,j>()*v<2>()*v<i>()*v<k>()*v<l>() + 2.0*S<0,2,k>()*v<2>()*v<i>()*v<j>()*v<l>() +
          2.0*S<0,2,l>()*v<2>()*v<i>()*v<j>()*v<k>() - 5.0*vsq*S<0>()*v<i>()*v<j>()*v<k>()*v<l>();
    }

    template<uint8_t s, uint8_t i, uint8_t j, uint8_t k, uint8_t l, uint8_t m>
    double C() const {
      static_assert(s<1,"invalid trace number (valid: 0");
      static_assert(i<3,"index i out of bounds");
      static_assert(j<3,"index j out of bounds");
      static_assert(k<3,"index k out of bounds");
      static_assert(l<3,"index l out of bounds");
      static_assert(m<3,"index m out of bounds");
      return S<0,i,j,k,l,m>() -
          S<0,i,j,k,l>()*v<m>() -
          S<0,i,j,k,m>()*v<l>() -
          S<0,i,j,l,m>()*v<k>() -
          S<0,i,k,l,m>()*v<j>() -
          S<0,j,k,l,m>()*v<i>() +
          S<0,i,j,k>()*v<l>()*v<m>() +
          S<0,i,j,l>()*v<k>()*v<m>() +
          S<0,i,j,m>()*v<k>()*v<l>() +
          S<0,i,k,l>()*v<j>()*v<m>() +
          S<0,i,k,m>()*v<j>()*v<l>() +
          S<0,i,l,m>()*v<j>()*v<k>() +
          S<0,j,k,l>()*v<i>()*v<m>() +
          S<0,j,k,m>()*v<i>()*v<l>() +
          S<0,j,l,m>()*v<i>()*v<k>() +
          S<0,k,l,m>()*v<i>()*v<j>() -
          S<0,i,j>()*v<k>()*v<l>()*v<m>() -
          S<0,i,k>()*v<j>()*v<l>()*v<m>() -
          S<0,i,l>()*v<j>()*v<k>()*v<m>() -
          S<0,i,m>()*v<j>()*v<k>()*v<l>() -
          S<0,j,k>()*v<i>()*v<l>()*v<m>() -
          S<0,j,l>()*v<i>()*v<k>()*v<m>() -
          S<0,j,m>()*v<i>()*v<k>()*v<l>() -
          S<0,k,l>()*v<i>()*v<j>()*v<m>() -
          S<0,k,m>()*v<i>()*v<j>()*v<l>() -
          S<0,l,m>()*v<i>()*v<j>()*v<k>() +
          4.0*S<0>()*v<i>()*v<j>()*v<k>()*v<l>()*v<m>();
    }

    template<uint8_t s, uint8_t m>
    double dCdx() const {
      static_assert(s<2,"invalid trace number (valid: 0,1)");
      static_assert(m<3,"index m out oS bounds");

      if constexpr(s==0) return dSdx<0,m>();
      if constexpr(s==1) return dSdx<1,m>() - 2.0*S<0>()*(dvdx<0,m>()*v<0>() + dvdx<1,m>()*v<1>() + \
          dvdx<2,m>()*v<2>()) - vsq*dSdx<0,m>();

    }

    template<uint8_t s, uint8_t i, uint8_t m>
    double dCdx() const {
      static_assert(s<2,"invalid trace number (valid: 0,1)");
      static_assert(i<3,"index i out oS bounds");
      static_assert(m<3,"index m out oS bounds");

      if constexpr(s==0) return 0.0;
      if constexpr(s==1) return dSdx<1,i,m>() - 2.0*(dSdx<0,i,0,m>()*v<0>() + dSdx<0,i,1,m>()*v<1>() \
            + dSdx<0,i,2,m>()*v<2>() + dvdx<0,m>()*S<0,0,i>() + \
            dvdx<1,m>()*S<0,1,i>() + dvdx<2,m>()*S<0,2,i>()) + \
            dvdx<i,m>()*(-S<1>() + 2.0*vsq*S<0>()) + (-dSdx<1,m>() + \
                2.0*vsq*dSdx<0,m>() + 4.0*S<0>()*(dvdx<0,m>()*v<0>() + \
                    dvdx<1,m>()*v<1>() + dvdx<2,m>()*v<2>()))*v<i>();
    }

    template<uint8_t s, uint8_t i, uint8_t j, uint8_t m>
    double dCdx() const {
      static_assert(s==0,"invalid trace number (valid: 0)");
      static_assert(i<3,"index i out oS bounds");
      static_assert(j<3,"index j out oS bounds");
      static_assert(m<3,"index m out oS bounds");
      return dSdx<0,i,j,m>() - (dSdx<0,m>()*v<i>() + dvdx<i,m>()*S<0>())*v<j>() - \
          dvdx<j,m>()*S<0>()*v<i>();
    }

  public:

    // centered velocity moments [ (m/s)**(twos+len(A)) ]
    template<uint8_t twos, uint8_t... A>
    double u() const {
      static_assert(twos==0||twos==2||twos==4||twos==6,"invalid (2x) trace number (valid: 0,2,4,6)");
      static_assert(sizeof...(A) < 6, "invalid number of subscripts");
      return C<twos/2,A...>()/C<0>();
    }

    // gradient of centered velocity moments [ (m/s)**(twos) / m ]
    template<uint8_t twos, uint8_t m>
    double dudx() const {
      static_assert(twos==0||twos==2||twos==4,"invalid (2x) trace number (valid: 0,2,4)");
      return (C<0>()*dCdx<twos/2,m>() - dCdx<0,m>()*C<twos/2>() )/(C<0>()*C<0>());
    }

    // gradient of centered velocity moments [ (m/s)**(twos+len(A)) / m ]
    template<uint8_t twos, uint8_t... A, uint8_t m>
    double dudx() const {
      static_assert(twos==0||twos==2,"invalid (2x) trace number (valid: 0,2)");
      static_assert(sizeof...(A) < 3, "invalid number of subscripts");
      return (C<0>()*dCdx<twos/2,A...,m>() - dCdx<0,m>()*C<twos/2,A...>() )/(C<0>()*C<0>());
    }


  public:

    // [m s-1] norm of mean velocity
    double vabs() const { return sqrt(vsq); }

    // [m s-1] mean velocity
    v4df v() const { return v_; };

    // [m s-1] mean velocity
    template<uint8_t i>
    double v() const {
      static_assert(i<3,"index i out of bounds");
      return v_[i];
    }

    // [s-1] gradient of mean velocity
    template<uint8_t i, uint8_t m>
    double dvdx() const
    {
      static_assert(i<3,"index i out of bounds");
      static_assert(m<3,"index m out of bounds");
      return (dSdx<0,i,m>() - v<i>()*dSdx<0,m>())/S<0>();
    }

    // [J kg-1] == [m2 s-2] total specific translational energy
    double e() const { return 0.5 * S<1>()/S<0>(); }


  public:

    void update_derived() {
      v_ = S0_i / S0;
      vsq = MyIntrin::sabssq3(v_);
    }

    void reset() {
      *this = VelocityMoments();
    }


    template<class FunT>
    void for_all_S(FunT&& fun) {

      for(uint8_t i = 0; i < 3; ++i) {
          fun(S0_i[i]); }
      for(uint8_t i = 0; i < 6; ++i) {
          fun(S0_ji[i]); }
      for(uint8_t i = 0; i < 10; ++i) {
          fun(S0_kji[i]); }
      for(uint8_t i = 0; i < 15; ++i) {
          fun(S0_lkji[i]);
          fun(S1_lkji[i]); }
      for(uint8_t i = 0; i < 21; ++i) {
          fun(S0_mlkji[i]); }
      fun(S0);

    }

    template<class FunT>
    void for_all_dS(FunT&& fun) {
      fun(dS0_dx_m);

      fun(dS0_0_dx_m);
      fun(dS0_1_dx_m);
      fun(dS0_2_dx_m);

      fun(dS0_00_dx_m);
      fun(dS0_01_dx_m);
      fun(dS0_02_dx_m);
      fun(dS0_11_dx_m);
      fun(dS0_12_dx_m);
      fun(dS0_22_dx_m);
    }

    template<class FunT>
    void for_all_S(FunT&& fun, const VelocityMoments& rhs)
    {
      for(uint8_t i = 0; i < 3; ++i) {
          fun(S0_i[i], rhs.S0_i[i]); }
      for(uint8_t i = 0; i < 6; ++i) {
          fun(S0_ji[i], rhs.S0_ji[i]); }
      for(uint8_t i = 0; i < 10; ++i) {
          fun(S0_kji[i], rhs.S0_kji[i]); }
      for(uint8_t i = 0; i < 15; ++i) {
          fun(S0_lkji[i], rhs.S0_lkji[i]);
          fun(S1_lkji[i], rhs.S1_lkji[i]); }
      for(uint8_t i = 0; i < 21; ++i) {
          fun(S0_mlkji[i], rhs.S0_mlkji[i]); }
      fun(S0, rhs.S0);
    }

    template<class FunT>
    void for_all_dS(FunT&& fun, const VelocityMoments& rhs)
    {

      fun(dS0_dx_m, rhs.dS0_dx_m);

      fun(dS0_0_dx_m, rhs.dS0_0_dx_m);
      fun(dS0_1_dx_m, rhs.dS0_1_dx_m);
      fun(dS0_2_dx_m, rhs.dS0_2_dx_m);

      fun(dS0_00_dx_m, rhs.dS0_00_dx_m);
      fun(dS0_01_dx_m, rhs.dS0_01_dx_m);
      fun(dS0_02_dx_m, rhs.dS0_02_dx_m);
      fun(dS0_11_dx_m, rhs.dS0_11_dx_m);
      fun(dS0_12_dx_m, rhs.dS0_12_dx_m);
      fun(dS0_22_dx_m, rhs.dS0_22_dx_m);

    }

  public:

    void assert_finite() { for_all_S( [](const double& v)->void{
      MYASSERT(std::isfinite(v),"VelocityMoments contains non finite data"); } ); }

  public:


    VelocityMoments& operator+=(const VelocityMoments& rhs) {
      for_all_S( [](double& a, const double& b)->void{a+=b;}, rhs );
      for_all_dS( [](v4df& a, const v4df& b)->void{a+=b;}, rhs );
      return *this; }

    friend VelocityMoments operator+(VelocityMoments lhs, const VelocityMoments& rhs) {
      lhs += rhs;
      return lhs; }

    VelocityMoments& operator-=(const VelocityMoments& rhs) {
      for_all_S( [](double& a, const double& b)->void{a-=b;}, rhs );
      for_all_dS( [](v4df& a, const v4df& b)->void{a-=b;}, rhs );
      return *this; }

    friend VelocityMoments operator-(VelocityMoments lhs, const VelocityMoments& rhs) {
      lhs -= rhs;
      return lhs; }

    VelocityMoments& operator*=(const double s) {
      for_all_S( [s](double& a)->void{a*=s;} );
      for_all_dS( [s](v4df& a)->void{a*=s;} );
      return *this; }

    friend VelocityMoments operator*(VelocityMoments vm, const double s) {
      vm *= s;
      return vm; }


    void rescale_gradients(const v4df& fac) {
      //      // gradients scale as delta^(nd+1)
      for_all_dS( [fac](v4df& a)->void{a*=fac;} );
    }


    static void* operator new( std::size_t count_bytes ) {
      return aligned_alloc(alignof(VelocityMoments),count_bytes); }

    static void operator delete( void* ptr) { free(ptr); }



  private:

    // gradient kernel vector evaluated at position x in cell with key "key" and level "level"
    // attention: result is kernel vector times cell volume
    template<uint8_t nd>
    static constexpr v4df K(const v4df& x, const v4df& c, const v4df& hrecp) {

      static_assert(nd==1 || nd==2 || nd==3);

      const v4df dx = hrecp*(c-x);

      // includes factor 2^d to account for division by cell volume

      /*
      // optI, alpha = 1
      switch (nd) {
        case 1:
          return -3.*hrecp*dx;
          break;
        case 2:
          return -(15./76.)*hrecp*dx*(28.*dx*dx - 21.*MyIntrin::dot2(dx,dx) + 18.);
          break;
        case 3:
          return -(15./82.)*hrecp*dx*(35.*dx*dx - 21.*MyIntrin::dot3(dx,dx) + 22.);
          break;
      }
      __builtin_unreachable();
      return zero<v4df>();
      */

      // minV, alpha = 1
      return -3.*hrecp*dx;
    }


    // sampling strategies

    template<uint8_t nd>
    void sample_naive(
        const Particles::Particle* first,
        const Particles::Particle* last,
        const v4df& cntr,
        const v4df& hrecp) {
      for (auto p = first; p != last; ++p) {
          Particles::prefetch(p+1);
          add_sample<nd>(p,cntr,hrecp);
      }
    }


    template<uint8_t nd>
    void sample_pairwise(
        const Particles::Particle* first,
        const Particles::Particle* last,
        const v4df& cntr,
        const v4df& hrecp) {
      uint64_t n = std::distance(first,last);
      if( n < 25 ) // base case
        sample_naive<nd>(first,last,cntr,hrecp);
      else {
          // divide
          VelocityMoments v1,v2;
          v1.sample_pairwise<nd>(first,first+n/2,cntr,hrecp);
          v2.sample_pairwise<nd>(first+n/2,last,cntr,hrecp);
          // conquer
          v1 += v2;
          (*this) += v1;
      }
    }


  public:

    enum SampleType : uint8_t {
      NAIVE,
      PAIR,
      NSAMPLETYPES
    };

    template<uint8_t nd, SampleType type>
    void sample(
        const Particles::Particle* first,
        const Particles::Particle* last,
        const Box::box4_t cell_box)
    {

      static_assert(type < NSAMPLETYPES);

      const v4df cntr   = cell_box.center();
      v4df hwidth = .5*cell_box.width();
      hwidth = (hwidth!=zero<v4df>()) ? hwidth : one<v4df>();
      const v4df hrecp  = 1./hwidth;

      //#define check_contain
#ifdef check_contain
      for(auto p = first; p!=last; ++p)
        {
          if (__builtin_expect(!cell_box.contains<nd>(p->R),false))
            {
#pragma omp critical  (COUT)
              std::cout << "particle not in cell: " +
                  to_string(cell_box.lower) + " / " + to_string(p->R) + " / " +
                  to_string(cell_box.upper) << std::endl;
            }
        }
#undef check_contain
#endif


      switch (type) {
        case NAIVE: sample_naive<nd>(first,last,cntr,hrecp);       break;
        case PAIR:  sample_pairwise<nd>(first,last,cntr,hrecp);    break;
      }

      update_derived();

      //      assert_finite();

    }


  }; // struct VelocityMoments

} // namespace Moments


#endif /* VELOCITYMOMENTS_H_ */
