/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * AveragingStrategies.h
 *
 *  Created on: Aug 12, 2014
 *      Author: kustepha
 */

#ifndef AVERAGINGSTRATEGIES_H_
#define AVERAGINGSTRATEGIES_H_

#include <memory>
#include <cmath>
#include <iostream>

namespace Settings
{
  class Dictionary;
}

namespace Moments
{

  namespace Averaging
  {

    struct AveragingStrategy {
      virtual double operator()(const uint64_t tn) const = 0;

      AveragingStrategy(){};
      virtual ~AveragingStrategy(){};
    };

    struct ExpWeighted: public AveragingStrategy {
      // Jenny et al.
      // A solution algorithm for the fluid dynamic equations based on a stochastic model for molecular motion
      // Journal of Computational Physics, 2010
      // suggested window size ~1000

      const double alpha_;
      double operator()(const uint64_t tn) const override {
        if (tn==0) return 0.;
        return alpha_;
      }
      explicit ExpWeighted(const double windowSize)
      : alpha_( (fmax(windowSize,1.) - 1.0)/fmax(windowSize,1.) )
      {
        /*
        std::cout << "exponential averaging: window size " << windowSize  <<
            ", -> alpha: " << alpha_ << std::endl;
         */
      }
      explicit ExpWeighted(const Settings::Dictionary& dict);
    };

    struct Arithmetic: public AveragingStrategy {
      const uint64_t skip_;
      double operator()(const uint64_t tn) const override {
        if ( tn%(skip_+1) == 0 )
          {
            const double a = static_cast<double>( tn/(skip_+1) + 1 );
            return (a - 1.)/a;
          }
        else
          return 1.;
      }
      explicit Arithmetic(const uint64_t skip = 0): skip_(skip) {
        /*
        if (skip_>0)
          std::cout << "arithmetic averaging: skipping " << skip_ << "\n"<< std::endl;
         */
      }
      explicit Arithmetic(const Settings::Dictionary& dict);
    };

    struct Switched: public AveragingStrategy {
      const uint64_t tn_switch;
      const std::unique_ptr<const AveragingStrategy> first;
      const std::unique_ptr<const AveragingStrategy> second;

      double operator()(const uint64_t tn) const override {
        if (tn < tn_switch) return first->operator()(tn);
        else                return second->operator()(tn - tn_switch);
      }
      explicit Switched(const uint64_t _tn_switch,
                        std::unique_ptr<const AveragingStrategy> _first,
                        std::unique_ptr<const AveragingStrategy> _second)
      : tn_switch(_tn_switch)
      , first(std::move(_first))
      , second(std::move(_second))
      {}
      explicit Switched(const Settings::Dictionary& dict);
    };

    struct NoAverage: public AveragingStrategy {
      double operator()(const uint64_t) const override {
        return 0.0;
      }
    };

    std::unique_ptr<AveragingStrategy>
    makeAveragingStrategy(const Settings::Dictionary& dict);


    struct Averager
    {

      const double alpha_;

      // number of time steps in average
      // window size of exp weighted
      double Wtot() const { return (alpha_ == 1.) ? 0. : 1./(1.-alpha_); }

      // avnew = alpha*avold + (1-alpha)*samp == alpha*(avold-samp) + samp;
      template<typename T>
      void operator()(T& av, const T& samp)
      const
      {
        if (alpha_ == 1.)
          return;
        if (alpha_ == 0.)
          {av = samp; return;}

        av -= samp;
        av *= alpha_;
        av += samp;
      }

      explicit Averager(const double alpha)
      : alpha_(alpha)
      {};
    };

  } // namespace Averaging


} /* namespace Moments */

#endif /* AVERAGINGSTRATEGIES_H_ */
