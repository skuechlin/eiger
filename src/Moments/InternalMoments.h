/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * InternalMoments.h
 *
 *  Created on: Nov 14, 2014
 *      Author: kustepha
 */

#ifndef MOMENTS_INTERNALMOMENTS_H_
#define MOMENTS_INTERNALMOMENTS_H_


#include <mpi.h>
#include <Eigen/Dense>

#include "AveragingStrategies.h"

#include "Particles/ParticleCollection.h"

namespace Moments
{

  struct alignas(32) InternalMoments
      {

    v4df S0_i = zero<v4df>();
    double S0 = 1.e-60;
    double pad[3] = {0.};

    struct Moments {
      v4df S0_0i = zero<v4df>();
      v4df S0_1i = zero<v4df>();
      v4df S1_i  = zero<v4df>();
      v2df S0_i  = zero<v2df>();
      // derived
      v2df v_ = zero<v2df>();
      double vsq = 0.;
      //
      double S1 = 0.;

      void add_sample(const v2df& s, const v4df& wc, const double w) {
        const double e = MyIntrin::sabssq<v2df>(s);
        S0_0i += s[0]*wc;
        S0_1i += s[1]*wc;
        S1_i  += e*wc;
        S0_i  += w*s;
        S1    += w*e;
      }

      void update_derived(const double S0) {
        v_ = S0_i / S0;
        vsq = MyIntrin::sabssq<v2df>(v_);
      }

      template<class FunT>
      void for_all_S(FunT&& fun) {
        for(uint8_t i = 0; i < 4; ++i) {
            fun(S0_0i[i]); }
        for(uint8_t i = 0; i < 4; ++i) {
            fun(S0_1i[i]); }
        for(uint8_t i = 0; i < 4; ++i) {
            fun(S1_i[i]); }
        for(uint8_t i = 0; i < 2; ++i) {
            fun(S0_i[i]); }
        fun(S1);
      }

      template<class FunT>
      void for_all_S(FunT&& fun, const InternalMoments::Moments& rhs)
      {
        for(uint8_t i = 0; i < 4; ++i) {
            fun(S0_0i[i],rhs.S0_0i[i]); }
        for(uint8_t i = 0; i < 4; ++i) {
            fun(S0_1i[i],rhs.S0_1i[i]); }
        for(uint8_t i = 0; i < 4; ++i) {
            fun(S1_i[i],rhs.S1_i[i]); }
        for(uint8_t i = 0; i < 2; ++i) {
            fun(S0_i[i],rhs.S0_i[i]); }
        fun(S1,rhs.S1);
      }

      // [m s-1] mean state
      template<uint8_t i>
      double v() const {
        static_assert(i<2,"index i out of bounds");
        return v_[i];
      }

      // [J kg-1] == [m2 s-2] specific thermal energy
      double u(const double S0) const {
        //  [J kg-1]        Cercignani, 1.6.12
        return  0.5 * (S1/S0 - vsq); }

      // [kg m3 s-3] 1x contracted third order centered power sum tensor vi vk vk
      template<uint8_t i>
      double C1(const double vi, const double S0) const {
        static_assert(i<3,"index i out of bounds");
        return S1_i[i] - 2.*(S0_0i[i]*v<0>() + S0_1i[i]*v<1>()) - S1*vi + 2.*vsq*vi*S0; }

      // [m3 s-3] 1x contracted third order central moment tensor vi vk vk
      template<uint8_t i>
      double u2(const double vi, const double S0) const {
        static_assert(i<3,"index i out of bounds");
        return C1<i>(vi,S0)/S0; }

    };

    Moments rot;
    Moments vib;

    // derived
    v4df v_;

      public:

    void sample(
        const Particles::Particle* first,
        const Particles::Particle* last )
    {

      for (const Particles::Particle* p = first; p != last; ++p)
        {
          const v4df c    = p->C;
          const double w  = (p->w)*(p->m);
          const v4df wc   = w*c;

          S0   += w;
          S0_i += wc;

          rot.add_sample(p->Omega,wc,w);
          vib.add_sample(p->Xi,wc,w);
        }

      update_derived();

    } // sample

    void update_derived() {
      v_    = S0_i / S0;
      rot.update_derived(S0);
      vib.update_derived(S0);
    }


      private:

    void reset() { *this = InternalMoments(); }

      private:

    template<class FunT>
    void for_all_S(FunT&& fun)
    {
      for(uint8_t i = 0; i < 4; ++i) {
          fun(S0_i[i]);
      }
      fun(S0);
      rot.for_all_S(fun);
      vib.for_all_S(fun);
    }

    template<class FunT>
    void for_all_S(FunT&& fun, const InternalMoments& rhs)
    {
      for(uint8_t i = 0; i < 4; ++i) {
          fun(S0_i[i],rhs.S0_i[i]);
      }
      fun(S0,rhs.S0);
      rot.for_all_S(fun,rhs.rot);
      vib.for_all_S(fun,rhs.vib);
    }

    void smulS(const double s) { for_all_S( [s](auto& a)->void{a*=s;} ); }

      public:

    InternalMoments& operator+=(const InternalMoments& rhs) {
      for_all_S( [](auto& a, const auto& b)->void{a+=b;}, rhs );
      return *this; }

    InternalMoments& operator-=(const InternalMoments& rhs) {
      for_all_S( [](auto& a, const auto& b)->void{a-=b;}, rhs );
      return *this; }

    InternalMoments& operator*=(const double s) {
      smulS(s);
      return *this; }

    friend InternalMoments operator*(InternalMoments im, const double s) {
      im *= s;
      return im; }

    static void* operator new( std::size_t count_bytes ) {
      return aligned_alloc(alignof(InternalMoments),count_bytes); }

    static void operator delete( void* ptr) { free(ptr); }


      public:

    // [m s-1] mean velocity
    v4df v() const { return v_; };

    // [m s-1] mean velocity
    template<uint8_t i>
    double v() const {
      static_assert(i<3,"index i out of bounds");
      return v_[i]; }

    // [m s-1] mean rotational state
    v2df omega() const { return rot.v_; }

    // [m s-1] mean rotational state
    template<uint8_t i>
    double omega() const {
      static_assert(i<2,"index i out of bounds");
      return rot.v_[i]; }

    // [m s-1] mean vibrational state
    v2df xi() const { return vib.v_; }

    // [m s-1] mean vibrational state
    template<uint8_t i>
    double xi() const {
      static_assert(i<2,"index i out of bounds");
      return vib.v_[i]; }

    // [J kg-1] == [m2 s-2] specific thermal rotational energy
    double u_rot() const { return rot.u(S0);}

    // [J kg-1] == [m2 s-2] specific thermal vibrational energy
    double u_vib() const { return vib.u(S0);}

    // [m2 s-2] rotational temperature, in energy units
    double theta_rot() const { return u_rot(); }

    // [m2 s-2] vibrational temperature, in energy units
    double theta_vib() const { return u_vib(); }

    // [kg m3 s-3] 1x contracted third order centered power sum tensor omegai omegaj vk
    template<uint8_t i>
    double C1_rot() const {
      static_assert(i<3,"index i out of bounds");
      return rot.C1<i>(v<i>(),S0);
      /*
      return So1_ci[i] -
          2.*(So0_o0ci[i]*omega<0>() + So0_o1ci[i]*omega<1>()) -
          Soxs[1]*v<i>()  + 2.*oxsq[0]*v<i>()*Soxs[0];
       */
    }

    // [kg m3 s-3] 1x contracted third order central power sum tensor xii xij vk
    template<uint8_t i>
    double C1_vib() const {
      static_assert(i<3,"index i out of bounds");
      return vib.C1<i>(v<i>(),S0);
      /*
      return Sx1_ci[i] -
          2.*(Sx0_x0ci[i]*omega<0>() + Sx0_x1ci[i]*omega<1>()) -
          Soxs[2]*v<i>() + 2.*oxsq[2]*v<i>()*Soxs[0];
       */
    }

    // [m3 s-3] 1x contracted third order central moment tensor omegai omegaj vk
    template<uint8_t i>
    double u2_rot() const {
      static_assert(i<3,"index i out of bounds");
      return rot.u2<i>(v<i>(),S0);}

    // [m3 s-3] 1x contracted third order central moment tensor xii xij vk
    template<uint8_t i>
    double u2_vib() const {
      static_assert(i<3,"index i out of bounds");
      return vib.u2<i>(v<i>(),S0); }




      }; // struct InternalMoments

} // namespace Moments

#endif /* MOMENTS_INTERNALMOMENTS_H_ */
