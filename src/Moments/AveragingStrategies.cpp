/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * AveragingStrategies.cpp
 *
 *  Created on: Aug 12, 2014
 *      Author: kustepha
 */

#include "AveragingStrategies.h"

#include <map>
#include "Settings/Dictionary.h"

namespace Moments
{
  namespace Averaging
  {

    enum AveragingStrategyType
    {
      AR,
      EXP,
      SW,
      NONE
    };

    static std::map<std::string,AveragingStrategyType> sAvType = {
        {"arithmetic",AR},
        {"exponential",EXP},
        {"switched",SW},
        {"none",NONE}
    };

    std::unique_ptr<Moments::Averaging::AveragingStrategy>
    makeAveragingStrategy(const Settings::Dictionary& dict)
    {
      std::unique_ptr<Moments::Averaging::AveragingStrategy> p;

      MYASSERT(dict.hasMember("type"),"averaging strategy dict missing member \"type\"");

      const std::string type = dict.get<std::string>("type");
      auto it = sAvType.find( type );

      MYASSERT(it != sAvType.end(),std::string("unknown averaging strategy type \"") + type + std::string("\""));

      switch (it->second)
      {
        case AR:
          p = std::unique_ptr<Moments::Averaging::AveragingStrategy>(new class Averaging::Arithmetic(dict));
          break;
        case EXP:
          p = std::unique_ptr<Moments::Averaging::AveragingStrategy>(new class Averaging::ExpWeighted(dict));
          break;
        case SW:
          p = std::unique_ptr<Moments::Averaging::AveragingStrategy>(new class Averaging::Switched(dict));
          break;
        case NONE:
          p = std::unique_ptr<Moments::Averaging::AveragingStrategy>(new class Averaging::NoAverage());
          break;
        default:
          MYASSERT(false,"unknown averaging strategy enum, this should never happen");
          break;
      }

      return p;

    }



    ExpWeighted::ExpWeighted(const Settings::Dictionary& dict)
    : ExpWeighted(dict.get<double>("window size"))
    {}

    Arithmetic::Arithmetic(const Settings::Dictionary& dict)
    : Arithmetic(dict.get<uint64_t>("skip",0))
    {}

    Switched::Switched(const Settings::Dictionary& dict)
    : Switched(
        dict.get<uint64_t>("switch"),
        makeAveragingStrategy(dict.get<Settings::Dictionary>("first")),
        makeAveragingStrategy(dict.get<Settings::Dictionary>("second"))
    )
    {}

  } // namespace Averaging



} /* namespace VelocityMoments */
