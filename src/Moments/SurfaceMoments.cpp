/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * SurfaceMoments.cpp
 *
 *  Created on: Oct 7, 2014
 *      Author: kustepha
 */


#include "SurfaceMoments.h"

#include "AveragingStrategies.h"

#include "Particles/ParticleCollection.h"

#include "Primitives/MyVecArithm.h"
#include "Primitives/MyError.h"
#include "Primitives/MyIntrin.h"
#include "Primitives/MyMemory.h"

#include "Parallel/MyMPI.h"

using namespace Eigen;

using namespace Moments;

static void* SurfaceMoments::operator new( std::size_t count_bytes ) {
  return MyMemory::aligned_alloc<SurfaceMoments,32>( count_bytes / sizeof(SurfaceMoments) ); }

static void* SurfaceMoments::operator new[]( std::size_t count_bytes ) {
  return MyMemory::aligned_alloc<SurfaceMoments,32>( count_bytes / sizeof(SurfaceMoments) ); }

static void SurfaceMoments::operator delete( void* ptr) { MyMemory::aligned_free(ptr); }
static void SurfaceMoments::operator delete[]( void* ptr) { MyMemory::aligned_free(ptr); }


void
SurfaceMoments::sample(
    const v4df& N,
    const double ,
    const bool ,
    const Particles::Particle* const prt)
{

  //  if (!front)
  //    return;

  v4df C            = prt->C;
  const v2df Omega  = prt->Omega;
  const v2df Xi     = prt->Xi;
  const double m    = (prt->m);
  const double w    = (prt->w)*m;

  const double etr  = .5 * w * MyIntrin::shsum(C*C);
  const double erot = .5 * w * (Omega[0]*Omega[0] + Omega[1]*Omega[1]);
  const double evib = .5 * w * (Xi[0]*Xi[0] + Xi[1]*Xi[1]);
  const double vn   = MyIntrin::shsum(C*N);
  const double vn_abs_recp = 1./fabs(vn);
  const double nr   = w/m;

  C *= w;


  //  MYASSERT(C[3] == 0.0, "inv C");
  //  MYASSERT(N[3] == 0.0, "inv N");
  //
  //  if ( fabs(sqrt(N[0]*N[0] + N[1]*N[1] + N[2]*N[2] + N[3]*N[3]) - 1.0) > 5.0e-16)
  //    {
  //#pragma omp critical (COUT)
  //      {
  //        std::cout << std::scientific  << std::setw(20) << std::setprecision(16);
  //        std::cout << N[0] << "\n" << N[1] << "\n" << N[2] << "\n" << N[3] << "\n"
  //            << (sqrt(N[0]*N[0] + N[1]*N[1] + N[2]*N[2] + N[3]*N[3]) - 1.0) << std::endl;
  //        MYASSERT(false, "N not normalized");
  //      }
  //    }


#pragma omp atomic
  N_ 	+= 1.;
#pragma omp atomic
  Nr_ 	+= nr;
#pragma omp atomic
  W_ 	+= w;


#pragma omp atomic
  U0_ += C[0];          // mass weighted momentum
#pragma omp atomic
  U1_ += C[1];          // mass weighted momentum
#pragma omp atomic
  U2_ += C[2];          // mass weighted momentum
#pragma omp atomic
  U3_ += C[3];          // mass weighted momentum

#pragma omp atomic
  Etr_ += etr;
#pragma omp atomic
  Erot_ += erot;
#pragma omp atomic
  Evib_ += evib;
#pragma omp atomic
  Un_   -= w*vn;  // note -, N directed away from shape!

  if (__builtin_expect(fabs(vn) < 1.e-6,false))
    {
#pragma omp critical  (COUT)
      std::cout << std::to_string(vn) << std::endl;

      return;
    }

  // translational energy weighted by inverse magnitude of normal velocity
#pragma omp atomic
  Etr_v_ += etr*vn_abs_recp;

  // rotational energy, weighted by inverse magnitude of normal velocity
#pragma omp atomic
  Erot_v_ += erot*vn_abs_recp;

  // vibrational energy, weighted by inverse magnitude of normal velocity
#pragma omp atomic
  Evib_v_ += evib*vn_abs_recp;

  // sum of sample weights, weighted by inverse magnitude of surface normal velocity [kg s m-1]
#pragma omp atomic
  W_v_ += w*vn_abs_recp;


  // w*u_i^0/|u_i^1|
  //
  // momentum in world frame, weighted by inverse magnitude of normal velocity
  // [kg]
  //
  C *= vn_abs_recp;

#pragma omp atomic
  Uv0_ += C[0];          // mass weighted momentum
#pragma omp atomic
  Uv1_ += C[1];          // mass weighted momentum
#pragma omp atomic
  Uv2_ += C[2];          // mass weighted momentum
#pragma omp atomic
  Uv3_ += C[3];          // mass weighted momentum


  // number of represented physical particles in sample, weighted by inverse magnitude of normal velocity
#pragma omp atomic
  Nr_v_ += nr * vn_abs_recp;



  //    std::cout << "sampling: Nr_pre: " << Nr_pre << " Nr_post: " << Nr_ << std::endl;

}



void
SurfaceMoments::average(
    const SurfaceMoments& sm,
    const Moments::Averaging::Averager av)
{
  Wtot_ = av.Wtot();
  av(N_,    sm.N_);
  av(Nr_,   sm.Nr_);
  av(W_,    sm.W_); // 4

  av(U0_,   sm.U0_);
  av(U1_,   sm.U1_);
  av(U2_,   sm.U2_);
  av(U3_,   sm.U3_); // 8

  av(Etr_,  sm.Etr_);
  av(Erot_, sm.Erot_);
  av(Evib_, sm.Evib_);
  av(Un_ ,  sm.Un_); // 12

  av(Etr_v_,  sm.Etr_v_);
  av(Erot_v_, sm.Erot_v_);
  av(Evib_v_, sm.Evib_v_);
  av(W_v_,    sm.W_v_); // 16

  av(Uv0_, sm.Uv0_);
  av(Uv1_, sm.Uv1_);
  av(Uv2_, sm.Uv2_);
  av(Uv3_, sm.Uv3_); // 20

  av(Nr_v_, sm.Nr_v_);

  av(dtsamp_,sm.dtsamp_);


  //  av(Ub_,sm.Ub_);
  //
  //  if (av.alpha_ > 0.)
  //    {
  //#pragma omp critical (COUT)
  //      {
  //        std::cout << "averaging surface moments, alpha = " << av.alpha_ << "\n"
  //            << "Nr pre: " << Nr << " Nr samp: " << sm.Nr_ << " Nr post: " << Nr_ << std::endl;
  //      }
  //    }

}



namespace
{


  void addSurfaceMoments(const Moments::SurfaceMoments* const __restrict__ a, Moments::SurfaceMoments* const __restrict__ b)
  {
    b->Wtot_ += a->Wtot_;
    b->N_	 += a->N_;
    b->Nr_	 += a->Nr_;
    b->W_	 += a->W_; //4

    b->U0_   += a->U0_;
    b->U1_   += a->U1_;
    b->U2_   += a->U2_;
    b->U3_   += a->U3_; //8

    b->Etr_  += a->Etr_;
    b->Evib_ += a->Evib_;
    b->Erot_ += a->Erot_;
    b->Un_   += a->Un_; //12

    b->Etr_v_    += a->Etr_v_;
    b->Evib_v_   += a->Evib_v_;
    b->Erot_v_   += a->Erot_v_;
    b->W_v_      += a->W_v_; //16

    b->Uv0_  += a->Uv0_;
    b->Uv1_  += a->Uv1_;
    b->Uv2_  += a->Uv2_;
    b->Uv3_  += a->Uv3_; // 20

    b->Nr_v_ += a->Nr_v_;
    b->Wtot_ -= a->Wtot_;

  }



  void vecAddSurfaceMoments(void* in, void* inout, int* len, MPI_Datatype*)
  {

    const SurfaceMoments* smin  = reinterpret_cast<const SurfaceMoments* >(in);
    SurfaceMoments* sminout     = reinterpret_cast<SurfaceMoments*>(inout);
    const uint64_t n = *len;

//#pragma omp parallel for schedule(static) if(n > 1000)
        for (uint64_t i = 0; i < n; ++i)
          addSurfaceMoments(smin+i,sminout+i);

  }


  MPI_Op MakeAddSurfaceMomentsMPIOp()
  {
    MPI_Op o;
    MPI_Op_create(vecAddSurfaceMoments,true,&o);
    return o;
  }



  MPI_Datatype MakeSurfaceMomentsMPIType()
  {

    MPI_Datatype ret;

    MPI_Type_contiguous(sizeof(SurfaceMoments)/sizeof(double),MPI_DOUBLE,&ret);
    MPI_Type_commit(&ret);

    return ret;
  }


} // namespace

// MPI reduce op
MPI_Op Moments::surfaceMomentsMPIOp()
{
  static MPI_Op op = MakeAddSurfaceMomentsMPIOp();
  return op;
}


MPI_Datatype Moments::surfaceMomentsMPIType()
{
  static MPI_Datatype t = MakeSurfaceMomentsMPIType();
  return t;
}



