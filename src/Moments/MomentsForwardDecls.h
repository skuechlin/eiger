/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * MomentsForwardDecls.h
 *
 *  Created on: Mar 16, 2015
 *      Author: kustepha
 */

#ifndef MOMENTS_MOMENTSFORWARDDECLS_H_
#define MOMENTS_MOMENTSFORWARDDECLS_H_


namespace Moments
{

  namespace Averaging
  {
    struct Averager;
    struct AveragingStrategy;
  }

  namespace Filtering
  {
    class Filter;
  }

  struct VelocityMoments1stPass;
  struct VelocityMoments2ndPass;

  struct InternalMoments2ndPass;


  struct SurfaceMoments;



}


#endif /* MOMENTS_MOMENTSFORWARDDECLS_H_ */
