/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * SurfaceMoments.h
 *
 *  Created on: Oct 7, 2014
 *      Author: kustepha
 */

#ifndef SURFACEMOMENTS_H_
#define SURFACEMOMENTS_H_

#include <mpi.h>
#include <Eigen/Dense>
#include "Primitives/MyIntrin.h"

#include "MomentsForwardDecls.h"

namespace Particles
{
  class Particle;
}

namespace Moments
{


  struct SurfaceMoments {


    // ALL MOMENTS ARE MASS WEIGHTED

    double Wtot_  = 0.;	// number of time-steps in average
    double N_     = 0.; // number of samples
    double Nr_    = 0.;	// number of represented physical particles in sample
    double W_     = 1.e-30;	// sum of sample weights ("masses")

    // 4

    // w*u_i^0
    //
    // momentum in world frame
    //
    double U0_ = 0.;
    double U1_ = 0.;
    double U2_ = 0.;
    double U3_ = 0.;

    // 8

    // translational energy
    double Etr_   = 0.;

    // rotational energy
    double Erot_  = 0;

    // vibrational energy
    double Evib_  = 0.;

    // surface normal momentum
    double Un_    = 0.;

    // 12

    // translational energy weighted by inverse magnitude of normal velocity
    double Etr_v_ = 0.;

    // rotational energy, weighted by inverse magnitude of normal velocity
    double Erot_v_ = 0.;

    // vibrational energy, weighted by inverse magnitude of normal velocity
    double Evib_v_ = 0.;

    // sum of sample weights, weighted by inverse magnitude of surface normal velocity [kg s m-1]
    double W_v_ = 1.e-30;

    // 16

    // w*u_i^0/|u_i^1|
    //
    // momentum in world frame, weighted by inverse magnitude of normal velocity
    // [kg]
    //
    double Uv0_ = 0.;
    double Uv1_ = 0.;
    double Uv2_ = 0.;
    double Uv3_ = 0.;

    // 20


    // number of represented physical particles in sample, weighted by inverse magnitude of normal velocity
    double Nr_v_    = 0.;

    const double area_;

    const double FNum_ref_;

    double dtsamp_;	// [s]	simulation time over which the samples where acquired

    // 24

  public:

    void reset(const double dt) {
      new(this) SurfaceMoments(area_,FNum_ref_,dt);
    }

    void
    sample(
        const v4df& N,
        const double thit,
        const bool front,
        const Particles::Particle* const prt);

    void
    average(const SurfaceMoments& sm, const Averaging::Averager av);

    double numFlux() const { return Nr_ / (area_ * dtsamp_); }

    double pressure() const { return Un_ / (area_ * dtsamp_); }

    double heatFlux() const {
      return (Etr_ + Erot_ + Evib_) / (area_ * dtsamp_); }

    template<uint8_t i> double force() const {
      static_assert(i<3);
      switch (i) {
        case 0: return U0_ / dtsamp_; break;
        case 1: return U1_ / dtsamp_; break;
        case 2: return U2_ / dtsamp_; break;
      }
    }
    Eigen::Vector4d force() const {
      return Eigen::Vector4d{U0_,U1_,U2_,U3_} / dtsamp_; }

    template<uint8_t i> double weightedVelocity() const {
      static_assert(i<3);
      switch (i) {
        case 0: return Uv0_; break;
        case 1: return Uv1_; break;
        case 2: return Uv2_; break;
      }
    }
    Eigen::Vector4d weightedVelocity() const { return Eigen::Vector4d{Uv0_,Uv1_,Uv2_,Uv3_}; }
    //    double esTr() const { return Etr_v_ / W_v_ - .5*velocity().squaredNorm(); }
    //    double esRot() const { return Erot_v_ / W_v_; }
    //    double esVib() const { return Evib_v_ / W_v_; }

    double density() const { return W_v_ / (area_ * dtsamp_); }

    double numberDensity() const { return Nr_v_ / (area_ * dtsamp_); }

  public:

    static void* operator new( std::size_t count_bytes );
    static void* operator new[]( std::size_t count_bytes );

    static void operator delete( void* ptr);
    static void operator delete[]( void* ptr);

    static void *operator new(std::size_t size, void *ptr) {
      return ::operator new(size,ptr); };
    static void *operator new[](std::size_t size, void* ptr) {
      return ::operator new[](size,ptr); };
    void operator delete(void * memory, void *ptr) noexcept(true) {
      return ::operator delete(memory,ptr); };
    void operator delete[](void * memory, void *ptr) noexcept(true) {
      return ::operator delete[](memory,ptr); };

    SurfaceMoments(const double area, const double FNum_ref, const double dt = 1.)
    : area_(area)
    , FNum_ref_(FNum_ref)
    , dtsamp_(dt)
    {}


  };


  // MPI reduce op
  MPI_Op surfaceMomentsMPIOp();

  // MPI type
  MPI_Datatype surfaceMomentsMPIType();

}  // namespace Moments


#endif /* SURFACEMOMENTS_H_ */
