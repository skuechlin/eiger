/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

///*
// * InternalMoments.cpp
// *
// *  Created on: Nov 17, 2014
// *      Author: kustepha
// */
//
//
//#include <mpi.h>
//
//#include "InternalMoments.h"
//
//#include "AveragingStrategies.h"
//
//using namespace Moments;
//using namespace Averaging;
//
//
//
//InternalMomentData
//InternalMoments::convective2central(const InternalMomentData& F, const v4df& v, const double m)
//{
//
//  const double mrecp = m > 0. ? 1./m : 0.;
//
//  const v4df ox   = F.ox0_i_ * mrecp; // mean state
//
//  v4df oxsq = ox*ox;
//
//  oxsq += _mm256_permute_pd(oxsq,0b0101);
//
//
//  InternalMomentData rho;
//
//  rho.ox0_i_    = ox;
//
//  rho.ox2_      = -(oxsq*m) + F.ox2_;
//
//  rho.ox0_o0ui_ = -(m*ox[0]*v) + F.ox0_o0ui_;
//  rho.ox0_o1ui_ = -(m*ox[1]*v) + F.ox0_o1ui_;
//
//  rho.ox0_x0ui_ = -(m*ox[2]*v) + F.ox0_x0ui_;
//  rho.ox0_x1ui_ = -(m*ox[3]*v) + F.ox0_x1ui_;
//
//  rho.o2_ui_    = 2.0*oxsq[0]*m*v + F.o2_ui_ -
//      2.0*(ox[0]*F.ox0_o0ui_ + ox[1]*F.ox0_o1ui_) - v*F.ox2_[0];
//  rho.x2_ui_    = 2.0*oxsq[2]*m*v + F.x2_ui_ -
//      2.0*(ox[2]*F.ox0_x0ui_ + ox[3]*F.ox0_x1ui_) - v*F.ox2_[2];
//
//  // divide by mass
//
//  // ox0_i_ already divided
//
//  rho.ox2_ *= mrecp;
//  rho.ox0_o0ui_ *= mrecp;
//  rho.ox0_o1ui_ *= mrecp;
//  rho.ox0_x0ui_ *= mrecp;
//  rho.ox0_x1ui_ *= mrecp;
//  rho.o2_ui_ *= mrecp;
//  rho.x2_ui_ *= mrecp;
//
//  return rho;
//
//}
