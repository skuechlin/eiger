/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * TimeStep.cpp
 *
 *  Created on: Nov 24, 2014
 *      Author: kustepha
 */

#include <TimeStep/TimeStep.h>
#include "Settings/Dictionary.h"

#include "Primitives/MyString.h"
#include "Primitives/MyError.h"

namespace
{

  enum TsCtrlType
  {
    FIXED,
    CFL
  };

  std::map<std::string,TsCtrlType> TsCtrlNames =
      {
          {"fixed",FIXED},
          {"cfl",CFL}
      };

  enum CFLMode
  {
    STATIC,
    DYNAMIC
  };

  std::map<std::string,CFLMode> CFLModeNames =
      {
          {"static",STATIC},
          {"dynamic",DYNAMIC}
      };



  double
  getCFL(const Settings::Dictionary& dict)
  {
    MYASSERT(dict.hasMember("type"),"Time step control dict missing entry \"type\"!");

    std::string type = dict.get<std::string>("type");
    MyString::toLower(type);

    auto tsctrit = TsCtrlNames.find(type);
    MYASSERT(tsctrit != TsCtrlNames.end(),
             std::string("Time step control dict entry \"type\" set to unknown value \"") + type + std::string("\"!"));


    switch (tsctrit->second) {
      case FIXED:
        MYASSERT(dict.hasMember("dt"),"Time step control dict with \"type\" set to \"fixed\" missing entry \"dt\"!");
        return dict.get<double>("dt");
        break;
      case CFL:
        MYASSERT(dict.hasMember("cfl"),"Time step control dict with \"type\" set to \"cfl\" missing entry \"cfl\"!");
        return dict.get<double>("cfl");
        break;
    }

    return 1.;

  }

  TimeStep::TauFunT
  getTauFun(
      const Settings::Dictionary& dict,
      const TimeStep::TauFunT staticCFLTauFun,
      const TimeStep::TauFunT dynamicCFLTauFun)
  {
    MYASSERT(dict.hasMember("type"),"Time step control dict missing entry \"type\"!");

    std::string type = dict.get<std::string>("type");
    MyString::toLower(type);

    TimeStep::TauFunT ret([](const SimulationCore* sc){static_cast<void>(sc); return 1.;});


    if (TsCtrlNames[type] == FIXED)
      return ret;

    MYASSERT(dict.hasMember("cfl mode"),"Time step control dict with \"type\" set to \"cfl\" missing entry \"cfl mode\"!");

    std::string mode = dict.get<std::string>("cfl mode");
    MyString::toLower(mode);

    auto modeit = CFLModeNames.find(mode);
    MYASSERT(modeit != CFLModeNames.end(),
             std::string("Time step control dict entry \"cfl mode\" set to unknown value \"") + mode + std::string("\"!"));


    switch (modeit->second) {
      case STATIC:
        return staticCFLTauFun;
        break;
      case DYNAMIC:
        return dynamicCFLTauFun;
        break;
    }


    return ret;

  }


  TimeStep::TsFacMapT
  makeTsFacMap(const Settings::Dictionary& dict)
  {

    if (!dict.hasMember("scaling"))
      return TimeStep::TsFacMapT({{0,1.}});

    TimeStep::TsFacMapT ret;

    Settings::Dictionary tsMapDict = dict.get<Settings::Dictionary>("scaling");

    MYASSERT(tsMapDict.isArray(),"Entry \"scaling\" in time step control dictionary must be an array!");


    Settings::Dictionary tsMapEntryDict;

    for (uint i = 0; i < tsMapDict.size(); ++i)
      {
        tsMapEntryDict = tsMapDict.get<Settings::Dictionary>(i);

        MYASSERT(
            tsMapEntryDict.size() == 2,
            std::string("Entry ")
        + std::to_string(i)
        + std::string(" in \"scaling\" dictionary must be an array of length 2!")
        );

        ret.insert(
            { tsMapEntryDict.get<unsigned long int>(0),
          tsMapEntryDict.get<double>(1) }
        );


      }

    // make sure to have an entry for time step number 0!
    ret.insert({0,1.});

    return ret;

  }
}


TimeStep::TimeStep (
    const Settings::Dictionary& dict,
    const TimeStep::TauFunT staticCFLTauFun,
    const TimeStep::TauFunT dynamicCFLTauFun )
: cfl_(getCFL(dict))
, min_(dict.get<double>("dt min",1.e-15))
, max_(dict.get<double>("dt max",3600.0))
, tauFun_(getTauFun(dict,staticCFLTauFun,dynamicCFLTauFun))
, tsFacMap_(makeTsFacMap(dict))
{}

double
TimeStep::dt(const SimulationCore* c, const unsigned long int& tn)
const
{

  double dt =  std::min(std::max(cfl_ * tauFun_(c) * ((--tsFacMap_.upper_bound(tn))->second),min_),max_);


  //    std::cout << "cfl_:  " << cfl_ << std::endl;
  //    std::cout << "min_:  " << min_ << std::endl;
  //    std::cout << "max_:  " << max_ << std::endl;
  //    std::cout << "tau:   " << tauFun_(c) << std::endl;
  //    std::cout << "scale: " << ((--tsFacMap_.upper_bound(tn))->second) << std::endl;
  //
  //    std::cout << "dt:    " << dt << std::endl;

  return dt;


}


