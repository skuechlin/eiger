/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * TimeStep.h
 *
 *  Created on: Nov 24, 2014
 *      Author: kustepha
 */

#ifndef CONTROL_TIMESTEP_H_
#define CONTROL_TIMESTEP_H_

#include <vector>
#include <map>
#include <functional>

class SimulationCore;

namespace Settings
{
  class Dictionary;
}



class TimeStep
{

public:

  typedef typename std::function<double(const SimulationCore*)> TauFunT;
  typedef std::map<unsigned long int,double> TsFacMapT;

private:

  const double cfl_; // [-]

  const double min_; // [s]

  const double max_; // [s]

  const TauFunT tauFun_; // [s] flow time-scale

  const std::map<unsigned long int,double> tsFacMap_;

public:

  double dt(const SimulationCore* c, const unsigned long int& tn) const;

public:
  TimeStep (
      const Settings::Dictionary& dict,
      const TauFunT staticCFLTauFun,
      const TauFunT dynamicCFLTauFun);

};


#endif /* CONTROL_TIMESTEP_H_ */
