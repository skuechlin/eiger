/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * SimulationBox.h
 *
 *  Created on: Aug 4, 2014
 *      Author: kustepha
 */

#ifndef SIMULATIONBOX_H_
#define SIMULATIONBOX_H_

#include <vector>
#include <map>

#include <Eigen/Dense>

#include "Settings/Dictionary.h"
#include "Geometry/Parallelogram.h"

using namespace Shapes;

class SimulationBox
{

  Eigen::Vector4d O_ = Eigen::Vector4d::Zero();
  Eigen::Vector4d l_ = Eigen::Vector4d::Zero();

  Eigen::Matrix<double,4,8> verts_;


public:

  enum Face
  {
	LEFT,
	RIGHT,
	FRONT,
	BACK,
	BOTTOM,
	TOP
  };

  static const std::map<std::string, Face> FaceKeys;

  double volume() const {
    return l_.coeffRef(0) * l_.coeffRef(1) * l_.coeffRef(2);
  }

  const Eigen::Vector4d& origin() const
  {
	return O_;
  }

  const Eigen::Vector4d& extent() const
  {
	return l_;
  }

  Eigen::Vector4d min() const
  {
    return O_;
  }

  Eigen::Vector4d max() const
  {
    return O_ + l_;
  }

  Parallelogram* face(const Face face) const;
  Parallelogram* face(const std::string& faceKey) const
  {
    return face(FaceKeys.at(faceKey));
  }

  EIGEN_MAKE_ALIGNED_OPERATOR_NEW


  SimulationBox (  const Settings::Dictionary& dict );

};

#endif /* SIMULATIONBOX_H_ */
