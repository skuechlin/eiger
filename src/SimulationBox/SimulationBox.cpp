/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * SimulationBox.cpp
 *
 *  Created on: Aug 4, 2014
 *      Author: kustepha
 */

//#include <iomanip>

#include "SimulationBox.h"


const std::map<std::string, SimulationBox::Face> SimulationBox::FaceKeys = {
    {"left",LEFT},
    {"xlow",LEFT},
    {"right",RIGHT},
    {"xhigh",RIGHT},
    {"front",FRONT},
    {"ylow",FRONT},
    {"back",BACK},
    {"yhigh",BACK},
    {"bottom",BOTTOM},
    {"zlow",BOTTOM},
    {"top",TOP},
    {"zhigh",TOP}
};



SimulationBox::SimulationBox (
    const Settings::Dictionary& dict
)
{

  dict.get(O_.data(),"origin",0,3);
  dict.get(l_.data(),"extent",0,3);


  int cc = 0;

  for (int k = 0; k < 2; ++k)
    {
      for (int j = 0; j < 2; ++j)
        {
          for (int i = 0; i < 2; ++i)
            verts_.col(cc++) = Eigen::Vector4d(i,j,k,0.0).cwiseProduct(l_) + O_;
        }
    }

}


Parallelogram*
SimulationBox::face (
    const Face face) const
{

  uint a = 0;
  uint b = 0;
  uint c = 0;
  //	LEFT,
  //	RIGHT,
  //	FRONT,
  //	BACK,
  //	TOP,
  //	BOTTOM
  switch (face)
  {
    case LEFT:
      a = 0; b = 2; c = 4;
      break;
    case RIGHT:
      a = 1; b = 5; c = 3;
      break;
    case FRONT:
      a = 0; b = 4; c = 1;
      break;
    case BACK:
      a = 2; b = 3; c = 6;
      break;
    case BOTTOM:
      a = 0; b = 1; c = 2;
      break;
    case TOP:
      a = 4; b = 6; c = 5;
      break;
  }

  return new Parallelogram(verts_.col(a),verts_.col(b),verts_.col(c));

}
//
//AAPlane*
//SimulationBox::plane(
//    const Face face) const
//{
//
//  AAPlane::Axis axis 	= AAPlane::Axis::X;
//  double 	pos 	= 0.0;
//  bool 		flip	= false;
//
//  switch (face)
//  {
//    case LEFT:
//      axis 	= AAPlane::Axis::X;
//      pos	= O_.x();
//      flip	= false;
//      break;
//    case RIGHT:
//      axis 	= AAPlane::Axis::X;
//      pos	= O_.x() + l_.x();
//      flip 	= true;
//      break;
//    case FRONT:
//      axis 	= AAPlane::Axis::Y;
//      pos	= O_.y();
//      flip 	= false;
//      break;
//    case BACK:
//      axis 	= AAPlane::Axis::Y;
//      pos	= O_.y() + l_.y();
//      flip 	= true;
//      break;
//    case TOP:
//      axis 	= AAPlane::Axis::Z;
//      pos	= O_.z() + l_.z();
//      flip 	= true;
//      break;
//    case BOTTOM:
//      axis 	= AAPlane::Axis::Z;
//      pos	= O_.z();
//      flip 	= false;
//      break;
//    case LAST:
//      break;
//  }
//
//  return new AAPlane(axis, pos, flip);
//
//}
