/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * GridForwardDecls.h
 *
 *  Created on: Nov 26, 2014
 *      Author: kustepha
 */

#ifndef GRID_GRIDFORWARDDECLS_H_
#define GRID_GRIDFORWARDDECLS_H_

#include <stdint.h>

#include "Primitives/MyIntrin.h"
#include "Primitives/Box.h"

namespace Grids
{

  class Topology;
  class Grid;
  class RegularGrid;
  class DataGrid;

  class GridManager;


  using Box::box4_t;

}


#endif /* GRID_GRIDFORWARDDECLS_H_ */
