/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * ParticleGrid.cpp
 *
 *  Created on: Nov 27, 2014
 *      Author: kustepha
 */


#include <omp.h>

#include <iomanip>

#include "Grid.h"

#include "LoadBalancing/ExactBisection.h"



namespace Grids
{

  void Grid::lb_nonGhost_on_threads() {

    static uint64_t max_count;

#pragma omp single
    max_count = 0;

#pragma omp for schedule(static) reduction(max:max_count)
    for (uint64_t i = first_; i < last_; ++i)
      max_count = std::max(max_count, count_[i] );

#pragma omp single
    {

        const uint64_t nt = omp_get_num_threads();

        thread_lb_first_.resize(nt + 1,0);
        LoadBalance::ExactBisection::exactBisection(
            thread_lb_first_.data(),
            begin_.data() + first_,
            static_cast<double>(begin_[last_]) / static_cast<double>(nt),
            max_count,
            nNonGhost(),
            nt );

        for (uint64_t i = 0; i <= nt; ++i)
          thread_lb_first_[i] += first_;


        MYASSERT(thread_lb_first_[nt] == last_,"invalid partition");


    }

  }

  void Grid::print() const
  {
    std::cout << "\n\n" << "Grid \"" << name_
        << "\": first: " << first_
        << " last: " << last_
        << " nCells: " << nCells() << std::endl;
    std::cout << "number | key | count | begin" << std::endl;

    for (uint64_t i = 0; i < nCells(); ++i)
      std::cout << std::setw(5) << i << ": " << std::setw(7) << key_[i] << " " << count_[i] << " " << begin_[i] << "\n";
    std::cout << std::setw(5) << nCells()   << ": " << std::setw(7) << key_[nCells()]   << "\n";
    std::cout << std::setw(5) << nCells()+1 << ": " << std::setw(7) << key_[nCells()+1] << "\n";

    std::cout << "\n" << std::endl;

  }


} /* namespace Grids */


