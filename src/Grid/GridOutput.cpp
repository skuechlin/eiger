/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * GridOutput.cpp
 *
 *  Created on: May 12, 2016
 *      Author: kustepha
 */


#ifdef __STRICT_ANSI__
#define STRICT_ANSI_STATE
#undef __STRICT_ANSI__
#endif

#include <parallel/algorithm> // sort

#ifdef STRICT_ANSI_STATE
#define __STRICT_ANSI__ STRICT_ANSI_STATE
#endif

#include <set> // for set of unique verts
#include <iomanip> // formatting of numbers in grid file name

#include "Parallel/MyMPI.h"
#include "Parallel/MyMPI_DSDEComm2.h"

#include "Primitives/MyError.h"
#include "Primitives/MyCount.h" // count verts on node grid
#include "Primitives/MyString.h" // cat dirs in make output
#include "Primitives/MySearch.h"
#include "Primitives/Timer.h"

#include "Settings/Dictionary.h" // used in make output

#include "IO/Output.h"
#include "IO/Tecplot/Tecplot.h"

#include "Grid.h"
#include "DataGrid.h"
#include "GridGhostExchange.h"


namespace Grids
{

  // build a list of (unique) vertices of non-ghost cells that this proc references
  void
  RegularGrid::build_unique_verts(std::vector<uint64_t>& vrts)
  const
  {
    // a set to keep only unique vertices
    std::set<uint64_t> unique_vrts;

    // insert all vertices
    const uint8_t nvrts = (uint8_t(1) << topology_->ndims());

    for (uint64_t i = first_; i < last_; ++i)
      for (uint8_t j = 0; j < nvrts; ++j)
        unique_vrts.insert( vrtKey(i, j) );

    // copy set to vector
    vrts.resize(unique_vrts.size());
    vrts.assign(unique_vrts.begin(),unique_vrts.end());
  }

  // build a list of (unique) vertices of ghost cells that this proc references
  void
  RegularGrid::build_unique_ghost_verts(std::vector<uint64_t>& vrts)
  const
  {
    // a set to keep only unique vertices
    std::set<uint64_t> unique_vrts;

    // insert all vertices
    const uint8_t nvrts = (uint8_t(1) << topology_->ndims());

    for (uint64_t i = 0; i < first_; ++i)
      for (uint8_t j = 0; j < nvrts; ++j)
        unique_vrts.insert( vrtKey(i, j) );

    for (uint64_t i = last_; i < key_.size()-2; ++i)
      for (uint8_t j = 0; j < nvrts; ++j)
        unique_vrts.insert( vrtKey(i, j) );

    // copy set to vector
    vrts.resize(unique_vrts.size());
    vrts.assign(unique_vrts.begin(),unique_vrts.end());
  }


  namespace
  {

    // make a copy of list of vertices referenced on this proc containing only unique elements
    // this will be the vertex lists this proc writes.
    void
    keys_of_vrts_referenced_on_proc_to_keys_of_vrts_to_write_unique(
        std::vector<uint64_t>& keys_of_vrts_to_write_unique,
        const std::vector<uint64_t>& keys_of_vrts_referenced_on_proc )
    {
      // copy all vertices to the vrts vector
      keys_of_vrts_to_write_unique.resize( keys_of_vrts_referenced_on_proc.size() );
      std::copy(
          keys_of_vrts_referenced_on_proc.begin(),
          keys_of_vrts_referenced_on_proc.end(),
          keys_of_vrts_to_write_unique.begin() );

      // make them unique

      // sort all verts
      __gnu_parallel::sort(
          keys_of_vrts_to_write_unique.begin(),
          keys_of_vrts_to_write_unique.end() );

      // make unique
      auto vrts_to_write_unique_end = std::unique(
          keys_of_vrts_to_write_unique.begin(),
          keys_of_vrts_to_write_unique.end()
      );

      keys_of_vrts_to_write_unique.resize(
          std::distance(
              keys_of_vrts_to_write_unique.begin(),
              vrts_to_write_unique_end )
      );

    }

    // compute offset for global numbering of vertices
    uint64_t
    global_vert_number_offset(
        const uint64_t num_verts_on_this_proc,
        const MPI_Comm& comm)
    {
      uint64_t ret;
      MPI_Exscan(
          &num_verts_on_this_proc,
          &ret,
          1,
          MPI_UINT64_T, MPI_SUM, comm);

      if (MyMPI::rank(comm) == 0)
        ret = 0;

      return ret;
    }

    // compute the global number for the vertices referenced on this proc
    void
    vert_keys_to_vert_nums(
        std::vector<uint64_t>& nums_of_vrts_referenced,
        const std::vector<uint64_t>& keys_of_vrts_referenced,
        const std::vector<uint64_t>& vrts_to_write_unique,
        const uint64_t offset)
    {

      nums_of_vrts_referenced.resize(keys_of_vrts_referenced.size());

      auto begin = vrts_to_write_unique.begin();
      auto end = vrts_to_write_unique.end();

      __gnu_parallel::transform(
          keys_of_vrts_referenced.begin(),
          keys_of_vrts_referenced.end(),
          nums_of_vrts_referenced.begin(),
          [&begin,&end,&offset](const uint64_t vrt)->uint64_t
          {

        auto it = MySearch::binary_find(
            begin,
            end,
            vrt);

        //              MYASSERT(it != end, std::string("failed to locate vertex key ") +
        //                       std::to_string(vrt) + ("in vrts_to_write_unique") );

        return offset + std::distance( begin, it );

          } );

    }


    void
    comm_global_vert_lists(
        Grids::Grid* nodeGrid,
        std::vector<uint64_t>& keys_of_vrts_to_write_unique,
        std::vector<uint64_t>& nums_of_vrts_referenced_by_proc,
        const std::vector<uint64_t>& keys_of_vrts_referenced_by_proc,
        const MPI_Comm& comm,
        int tag)
    {

      //      std::vector<int32_t> rcount(nodeGrid->nNonGhost());
      //      std::vector<int32_t> rbegin(nodeGrid->nNonGhost());

      std::vector<uint64_t> keys_of_vrts_referenced_on_proc;
      std::vector<uint64_t> nums_of_vrts_referenced_on_proc;

      // determine which proc owns the vertices referenced by this proc
      // vertices outside the simulation domain, but within key range will also
      // be counted towards proc responsible for that range
      nodeGrid->resetCount();
      nodeGrid->resetBegin();

#pragma omp parallel
      {
        MyCount::parallelCount(
            nodeGrid->count_.data(),
            nodeGrid->begin_.data(),
            nodeGrid->key_.data(),
            nodeGrid->count_.size(), // include last bin to catch verts outside of simulation domain
            keys_of_vrts_referenced_by_proc.data(),
            keys_of_vrts_referenced_by_proc.size()
        );
      }

      // keys outside key range will go in last bin of node grid,
      // -> assign to last proc
      *(nodeGrid->count_.end()-2) += nodeGrid->count_.back();

      //    // DEBUG
      //    std::cout << MyMPI::rank(comm) << ": keys_of_vrts_referenced_by_proc size " << keys_of_vrts_referenced_by_proc.size() << std::endl;

      int rank;
      MPI_Comm_rank(comm,&rank);
      int num_ranks;
      MPI_Comm_size(comm,&num_ranks);

      // determine which other procs reference which vertices on this proc
      MyMPI::DSDECommSpec2 commspec(nodeGrid->count_.data(),comm,
                                    COMPILE_TIME_CRC32_STR( FILE_POS_STR ) + tag );

      keys_of_vrts_referenced_on_proc.resize(commspec.n_to_receive());

      commspec.comm(
          keys_of_vrts_referenced_by_proc.data(),
          keys_of_vrts_referenced_on_proc.data(),
          sizeof(uint64_t),
          MPI_UINT64_T,
          COMPILE_TIME_CRC32_STR( FILE_POS_STR ) + tag);

      //      // determine which other procs reference which vertices on this proc
      //      comm_vert_references(
      //          keys_of_vrts_referenced_on_proc,
      //          rcount.data(),
      //          rbegin.data(),
      //          keys_of_vrts_referenced_by_proc,
      //          nodeGrid,
      //          comm );

      // build a list of (unique) keys of vertices to write
      // (essentialy) a unique copy of list of all vertices referenced on this
      // proc by this or other porcs
      keys_of_vrts_referenced_on_proc_to_keys_of_vrts_to_write_unique(
          keys_of_vrts_to_write_unique,
          keys_of_vrts_referenced_on_proc );

      //    // DEBUG
      //          std::cout << MyMPI::rank(comm) << ": keys_of_vrts_to_write_unique size " << keys_of_vrts_to_write_unique.size() << std::endl;


      uint64_t offset = global_vert_number_offset(
          keys_of_vrts_to_write_unique.size(),
          comm );

      // compute global number for each vertex referenced on this proc
      vert_keys_to_vert_nums(
          nums_of_vrts_referenced_on_proc,
          keys_of_vrts_referenced_on_proc,
          keys_of_vrts_to_write_unique,
          offset);

      // notify other procs of the global number of the verts they are referencing
      // on this proc

      MyMPI::DSDECommSpec2
      commspec_answer(
          commspec.indegree(),
          commspec.neighbor_sources_.data(),
          commspec.neighbor_recvcounts_.data(),
          comm,
          COMPILE_TIME_CRC32_STR( FILE_POS_STR ) + tag);

      MYASSERT(keys_of_vrts_referenced_by_proc.size() == uint64_t(commspec_answer.n_to_receive()),"data exchange mismatch");
      nums_of_vrts_referenced_by_proc.resize(commspec_answer.n_to_receive());

      commspec_answer.comm(
          nums_of_vrts_referenced_on_proc.data(),
          nums_of_vrts_referenced_by_proc.data(),
          sizeof(uint64_t),
          MPI_UINT64_T,
          COMPILE_TIME_CRC32_STR( FILE_POS_STR ) + tag);

      //      comm_vert_nums(
      //          nums_of_vrts_referenced_by_proc,
      //          nums_of_vrts_referenced_on_proc,
      //          rcount.data(),
      //          rbegin.data(),
      //          comm );
    }




  } // anon namespace

  uint64_t
  RegularGrid::writeGrid(
      Grid* const nodeGrid,
      const std::string& fileName,
      const double solTime,
      const MyMPI::MPIMgr& mpiMgr )
  const
  {


    std::vector<uint64_t> keys_of_vrts_referenced_by_proc_unique;
    std::vector<uint64_t> nums_of_vrts_referenced_by_proc_unique;

    std::vector<uint64_t> keys_of_vrts_to_write_unique;


    // build a list of (unique) vertices that this proc references
    build_unique_verts(keys_of_vrts_referenced_by_proc_unique);

    const int tag = crc32(fileName.c_str(),fileName.length()+1);


    comm_global_vert_lists(
        nodeGrid,
        keys_of_vrts_to_write_unique,
        nums_of_vrts_referenced_by_proc_unique,
        keys_of_vrts_referenced_by_proc_unique,
        mpiMgr.comm(),
        COMPILE_TIME_CRC32_STR( FILE_POS_STR ) + tag);

    const uint64_t nVerts = keys_of_vrts_to_write_unique.size();

    // write all vertices referenced on node
    // write connectivity of all elements

    double rank = static_cast<double>(mpiMgr.rank());


    // function to retrieve vertex coordinate from key
    auto dataFun = [this,&keys_of_vrts_to_write_unique,rank]
                    (int32_t iDat, const int32_t iVar) -> double {
      if (iVar < 3)
        return MyIntrin::get( topology_->key2Pos(keys_of_vrts_to_write_unique[iDat]), iVar);
      else if (iVar == 3)
        return static_cast<double>(keys_of_vrts_to_write_unique[iDat]);
      else
        {
          iDat += first_;

          switch (iVar) {
            case 4:
              return static_cast<double>(cellKey(iDat));
              break;
            case 5:
              return cellVolume(iDat);
            case 6:
              return rank;
              break;
            case 7:
              return cellColor(iDat);
            default:
              return 1.23456789;
              break;
          }
        }

    };

    // function to retrieve global vertex number from element number and corner number
    auto  connFun = [this,&keys_of_vrts_referenced_by_proc_unique,&nums_of_vrts_referenced_by_proc_unique]
                     (const int32_t iElem, const int32_t iVrt) -> int32_t {

      const uint8_t vrt_kji = Tecplot::ivrt2kji( static_cast<uint8_t>(iVrt) );

      const uint64_t key = vrtKey( first_ + static_cast<uint64_t>(iElem),  vrt_kji ); // include ghost offset

      const auto it = MySearch::binary_find(
          keys_of_vrts_referenced_by_proc_unique.begin(),
          keys_of_vrts_referenced_by_proc_unique.end(),
          key);

      //      MYASSERT(it != keys_of_vrts_referenced_by_proc_unique.end(),std::string("failed to find vertex key ") +
      //               std::to_string(key) + std::string(" for element ") +
      //               std::to_string(iElem) + std::string(" vertex ") +
      //               std::to_string(iVrt));

      const uint64_t keyidx = std::distance( keys_of_vrts_referenced_by_proc_unique.begin(),
                                             it );

      return nums_of_vrts_referenced_by_proc_unique[ keyidx ];
    };


    // determine cell key number offset
    uint64_t global_offset =  global_vert_number_offset( nNonGhost(), mpiMgr.comm() );


    // determine absolute cell numbers of ghost keys
    std::vector<uint64_t> ghost_cell_numbers( nGhost() );


    exchangeGhostData(
        nodeGrid,
        this,
        [this,global_offset](const uint64_t key)->uint64_t{
      return locateKey( key ) - first_ + global_offset; },
      [this, &ghost_cell_numbers](const uint64_t iGhost, const uint64_t global_cell_number)->void{
        ghost_cell_numbers[ iGhost ] = global_cell_number; },
        mpiMgr ,
        COMPILE_TIME_CRC32_STR( FILE_POS_STR )+tag);

    //    std::cout << mpiMgr.rank() << " reached " << __FILE__ << " ln: " << std::to_string(__LINE__) << std::endl;


    MYASSERT(ghost_cell_numbers.size() == nGhost(),
             std::string("failed to receive global cell number for all ghost cells\n")
    + std::string("received ") + std::to_string(ghost_cell_numbers.size())
    + std::string(" expected ") + std::to_string(nGhost()) );

    //    // key comparator
    //    // output neighbors in logical order (i,j,k)
    //    // undocumented, but seems to give best contours
    //    auto comp = [this](const uint64_t a, const uint64_t b)->bool{
    //      auto ca = topology()->key2Coords(key_[a]);
    //      auto cb = topology()->key2Coords(key_[b]);
    //
    //      if (ca[2] == cb[2])
    //        {
    //          if (ca[1] == cb[1])
    //            return ca[0] < cb[0];
    //          else
    //            return ca[1] < cb[1];
    //        }
    //      else
    //        return ca[2] < cb[2];
    //
    //    };

    auto comp = std::less<uint64_t>();

    const uint8_t ndims = topology()->ndims();

    auto finish_record = [this,&ghost_cell_numbers,global_offset,ndims](
        const uint64_t size,
        const uint8_t lvl,
        auto&& faceNeighborBuffer,
        auto&& buffer)
        ->uint32_t{

      // determine face coverage
      double coverage = 0.0;
      for (auto n : faceNeighborBuffer)
        coverage += ldexp(1.0, int(ndims-1)*(int(cellLevel( n )) - int(lvl)) );


      if ( coverage < 1.0 )
        buffer.push_back( Tecplot::FaceObscurationFlag::PartiallyObscured );
      else
        buffer.push_back( Tecplot::FaceObscurationFlag::EntirelyObscured );

      buffer.push_back(size);

      for (auto n : faceNeighborBuffer)
        {
          if (n < first_) // ghost
            {
              buffer.push_back( ghost_cell_numbers[n] );
            }
          else if(n < last_ ) // non ghost
            {
              buffer.push_back( global_offset + n - first_ ); // correct ghost offset
            }
          else // n >= last_ -> ghost
            {
              buffer.push_back( ghost_cell_numbers[n - nNonGhost()] ); // correct ghost offset
            }
        }

      faceNeighborBuffer.clear();

      return size;
    };

    // function to build face neighbor connection list
    auto fnConnFun = [this,&comp,&finish_record,&ghost_cell_numbers,global_offset,ndims](std::vector<int32_t>& buffer,
        const int32_t iElem) -> int32_t {

      GridNeighborStack2 stack(this,1);

      std::set<uint64_t,decltype(comp)> fnb(comp);

      // correct iElem for ghost offset
      const uint64_t iElemLocal = iElem + first_;

      stack.push_neighbors( iElemLocal,
                            GridNeighborStack2::DIRECTION::HIGH | GridNeighborStack2::DIRECTION::LOW );

      int32_t n_conns = 0;

      const uint8_t lvl = cellLevel( iElemLocal );

      // face neighbor specification according to tecplot data format guide note 4.
      // for local one to many connection

      // process neighbor stack
      uint8_t current_face(-1);

      while( stack() )
        {
          const auto neighbor = stack.pop();
          uint8_t tmp;
          if ( (tmp = Tecplot::face2iface( neighbor.face, ndims )) != current_face )
            {

              uint64_t sz;
              if ( (sz = fnb.size()) > 0) // finish old record
                n_conns += finish_record(sz,lvl,fnb,buffer);

              // start a new record
              current_face = tmp;

              buffer.push_back(global_offset + iElem);
              buffer.push_back(current_face);

            }

          // append neighbor to current face neighbor list
          fnb.insert( neighbor.index );

        }

      // finish remaining unfinished record
      uint64_t sz;
      if ( (sz = fnb.size()) > 0) // finish old record
        n_conns += finish_record(sz,lvl,fnb,buffer);


      return n_conns;
    };

    // zone type to write
    Tecplot::ZoneType zoneType = Tecplot::ZoneType::INVALID;

    switch (topology_->ndims()) {
      case 1:
        zoneType = Tecplot::ZoneType::FELINESEG;
        break;
      case 2:
        zoneType = Tecplot::ZoneType::FEQUADRILATERAL;
        break;
      case 3:
        zoneType = Tecplot::ZoneType::FEBRICK;
        break;
      default:
        MYASSERT(false,std::string("unsupported dimensionality ") + std::to_string(topology_->ndims()));
        break;
    }



    // data is distributed across procs
    Tecplot::DataSharing dataSharing = Tecplot::DataSharing::DISTRIBUTED;

    // we are writing a grid file
    Tecplot::FileType fileType = Tecplot::FileType::GridFile;

    // number of variables
    int32_t nVars = 8; // X, Y, Z, node_key, cell_key, volume, rank, color

    std::string varNames = "X Y Z node_key cell_key volume rank color";

    // variable location
    std::vector<Tecplot::VarLoc> varLoc = {
        Tecplot::VarLoc::NodeCentered,
        Tecplot::VarLoc::NodeCentered,
        Tecplot::VarLoc::NodeCentered,
        Tecplot::VarLoc::NodeCentered,
        Tecplot::VarLoc::CellCentered,
        Tecplot::VarLoc::CellCentered,
        Tecplot::VarLoc::CellCentered,
        Tecplot::VarLoc::CellCentered
    };


    //    void write1DData2Tecplot(
    //        const std::function<double(const int32_t iDat, const int32_t iVar)>& dataFun,
    //        const std::function<int32_t(const int32_t iElem, const int32_t iVrt)>& connFun,
    //        const std::function<int32_t(std::vector<int32_t>& buffer, const int32_t iElem)>& fnConnFun,
    //        const ZoneType zoneType,
    //        const VarLoc varLoc,
    //        const DataSharing dataSharing,
    //        const FileType fileType,
    //        const int32_t nVars,
    //        const int32_t nPoints,
    //        const int32_t nElements,
    //        const std::string& fileName,
    //        const std::string& dataSetName,
    //        const std::string& zoneName,
    //        const std::string& varNames,
    //        const int32_t data_id,
    //        const double solTime,
    //        const bool writeConnectivity,
    //        const bool writeFaceNeighborConnectivity,
    //        const MyMPI::MPIMgr& mpiMgr );

    //    std::cout << mpiMgr.rank() << " reached " << __FILE__ << " ln: " << std::to_string(__LINE__) << std::endl;


    Tecplot::write1DData2Tecplot(
        dataFun,
        connFun,
        fnConnFun,
        zoneType,
        varLoc,
        dataSharing,
        fileType,
        nVars,
        static_cast<int32_t>(nVerts),
        static_cast<int32_t>(nNonGhost()),
        fileName,
        name(),
        name(),
        varNames,
        -1,
        solTime,
        true,
        true,
        mpiMgr );


    //        std::cout << mpiMgr.rank() << " reached " << __FILE__ << " ln: " << std::to_string(__LINE__) << std::endl;

    // return number of unique vertices for future reference
    return nVerts;


  }

  void
  RegularGrid::writeGhostGrid(
      Grid* const nodeGrid,
      const std::string& fileName,
      const double solTime,
      const MyMPI::MPIMgr& mpiMgr )
  const
  {

    const int tag = crc32(fileName.c_str(),fileName.length()+1);

    std::vector<uint64_t> keys_of_vrts_referenced_by_proc_unique;
    std::vector<uint64_t> nums_of_vrts_referenced_by_proc_unique;

    std::vector<uint64_t> keys_of_vrts_to_write_unique;


    // build a list of (unique) vertices that this proc references
    build_unique_ghost_verts(keys_of_vrts_referenced_by_proc_unique);

    //        // DEBUG
    //        std::cout << mpiMgr.id() << ": grid size " << size() << std::endl;
    //        std::cout << mpiMgr.id() << ": unique vrts referenced " << keys_of_vrts_referenced_by_proc_unique.size() << std::endl;

    //    std::cout << mpiMgr.rank() << " reached " << __FILE__ << " ln: " << std::to_string(__LINE__) << std::endl;


    comm_global_vert_lists(
        nodeGrid,
        keys_of_vrts_to_write_unique,
        nums_of_vrts_referenced_by_proc_unique,
        keys_of_vrts_referenced_by_proc_unique,
        mpiMgr.comm(),
        COMPILE_TIME_CRC32_STR( FILE_POS_STR ) + tag);

    //    std::cout << mpiMgr.rank() << " reached " << __FILE__ << " ln: " << std::to_string(__LINE__) << std::endl;


    // write all vertices referenced on node
    // write connectivity of all elements

    // function to retrieve vertex coordinate from key
    auto dataFun = [this,&keys_of_vrts_to_write_unique]
                    (const int32_t iDat, const int32_t iVar) -> double {
      if (iVar < 3)
        return MyIntrin::get(topology_->key2Pos(keys_of_vrts_to_write_unique[iDat]), iVar);
      else
        return static_cast<double>(keys_of_vrts_to_write_unique[iDat]);
    };

    // function to retrieve global vertex number from element number and corner number
    auto  connFun = [this,&keys_of_vrts_referenced_by_proc_unique,&nums_of_vrts_referenced_by_proc_unique]
                     (const int32_t iElem, const int32_t iVrt) -> int32_t {

      const uint8_t vrt_kji = Tecplot::ivrt2kji( static_cast<uint8_t>(iVrt) );

      uint64_t idx = static_cast<uint64_t>(iElem);
      if (!(idx < first_))  // skip non-ghost
        idx  = (idx - first_) + last_;

      const uint64_t key = vrtKey( idx,  vrt_kji );

      auto it = MySearch::binary_find(
          keys_of_vrts_referenced_by_proc_unique.begin(),
          keys_of_vrts_referenced_by_proc_unique.end(),
          key);

      MYASSERT(it != keys_of_vrts_referenced_by_proc_unique.end(),std::string("failed to find vertex key ") +
               std::to_string(key) + std::string(" for element ") +
               std::to_string(iElem) + std::string(" vertex ") +
               std::to_string(iVrt));

      const uint64_t keyidx = std::distance( keys_of_vrts_referenced_by_proc_unique.begin(),
                                             it );

      return nums_of_vrts_referenced_by_proc_unique[ keyidx ];
    };

    // zone type to write
    Tecplot::ZoneType zoneType = Tecplot::ZoneType::INVALID;

    switch (topology_->ndims()) {
      case 1:
        zoneType = Tecplot::ZoneType::FELINESEG;
        break;
      case 2:
        zoneType = Tecplot::ZoneType::FEQUADRILATERAL;
        break;
      case 3:
        zoneType = Tecplot::ZoneType::FEBRICK;
        break;
      default:
        MYASSERT(false,std::string("unsupported dimensionality ") + std::to_string(topology_->ndims()));
        break;
    }



    // data is distributed across procs
    Tecplot::DataSharing dataSharing = Tecplot::DataSharing::DISTRIBUTED;

    // we are writing a grid file
    Tecplot::FileType fileType = Tecplot::FileType::GridFile;

    // number of variables
    int32_t nVars = 4; // X, Y, Z

    std::string varNames = "X Y Z node_key";

    std::string zoneName = name() + std::string("_ghost");
    std::string dataSetName = zoneName;

    // variable location is at nodes
    std::vector<Tecplot::VarLoc> varLoc = {
        Tecplot::VarLoc::NodeCentered,
        Tecplot::VarLoc::NodeCentered,
        Tecplot::VarLoc::NodeCentered,
        Tecplot::VarLoc::NodeCentered
    };


    //    void write1DData2Tecplot(
    //        const std::function<double(const int32_t iDat, const int32_t iVar)>& dataFun,
    //        const std::function<int32_t(const int32_t iElem, const int32_t iVrt)>& connFun,
    //        const std::function<int32_t(std::vector<int32_t>& buffer, const int32_t iElem)>& fnConnFun,
    //        const ZoneType zoneType,
    //        const VarLoc varLoc,
    //        const DataSharing dataSharing,
    //        const FileType fileType,
    //        const int32_t nVars,
    //        const int32_t nPoints,
    //        const int32_t nElements,
    //        const std::string& fileName,
    //        const std::string& dataSetName,
    //        const std::string& zoneName,
    //        const std::string& varNames,
    //        const int32_t data_id,
    //        const double solTime,
    //        const bool writeConnectivity,
    //        const bool writeFaceNeighborConnectivity,
    //        const MyMPI::MPIMgr& mpiMgr );

    //    std::cout << mpiMgr.rank() << " reached " << __FILE__ << " ln: " << std::to_string(__LINE__) << std::endl;



    Tecplot::write1DData2Tecplot(
        dataFun,
        connFun,
        Tecplot::NO_FACENEIGHBORS, // not needed
        zoneType,
        varLoc,
        dataSharing,
        fileType,
        nVars,
        keys_of_vrts_to_write_unique.size(),
        nGhost(),
        fileName,
        dataSetName,
        zoneName,
        varNames,
        -1,
        solTime,
        true,
        false, // not needed
        mpiMgr );

  }


  void
  RegularGrid::writeToTecplot(
      Grid* const nodeGrid,
      const std::string& dir,
      const double solTime,
      const uint64_t tn,
      const MyMPI::MPIMgr& mpiMgr ) const
  {

    // print message
    std::string fName = name();

    if (mpiMgr.isRoot())
      {
        if (tn != uint64_t(-1))
          std::cout << "writing ts #" << tn << " grid \"" << fName << "\"" << std::endl;
        else
          std::cout << "writing grid \"" << fName << "\"" << std::endl;
      }

    // prepend directory
    fName = dir + std::string("grid_") + fName;

    std::string fNameTime("");

    if (solTime >= 0.)
      {

        // append solution time
        std::stringstream fnamestr;

        fnamestr.str(std::string(""));
        fnamestr << "_at_ts_";
        fnamestr << std::setw(10);
        fnamestr << std::setfill('0');
        fnamestr << tn;
        fnamestr << "_time_";
        fnamestr << std::fixed;
        fnamestr << std::setw(20);
        fnamestr << std::setprecision(10);
        fnamestr << std::setfill('0');
        fnamestr << solTime;

        fNameTime = fnamestr.str();

      }

    // write
    nVerts_ = writeGrid(
        nodeGrid,
        fName + fNameTime,
        solTime,
        mpiMgr );

    //            std::cout << mpiMgr.rank() << " reached " << __FILE__ << " ln: " << std::to_string(__LINE__) << std::endl;


    // write
    writeGhostGrid(
        nodeGrid,
        fName + std::string("_ghost") + fNameTime,
        solTime,
        mpiMgr );

    //      std::cout << mpiMgr.rank() << " reached " << __FILE__ << " ln: " << std::to_string(__LINE__) << std::endl;

  }


  void
  DataGrid::writeGrid(
      const std::string& fileName,
      const double solTime,
      const MyMPI::MPIMgr& mpiMgr )
  const
  {
    // function to retrieve data
    auto dataFun = [this]
                    (const int32_t iDat, const int32_t iVar) -> double {
      const uint64_t iCell = prtGrd_->first_+iDat;// skip ghost cells
      return getVar(iCell,iVar);
    };

    // zone type to write
    Tecplot::ZoneType zoneType = Tecplot::ZoneType::INVALID;

    switch (grid()->topology()->ndims()) {
      case 1:
        zoneType = Tecplot::ZoneType::FELINESEG;
        break;
      case 2:
        zoneType = Tecplot::ZoneType::FEQUADRILATERAL;
        break;
      case 3:
        zoneType = Tecplot::ZoneType::FEBRICK;
        break;
      default:
        MYASSERT(false,std::string("unsupported dimensionality ") + std::to_string(grid()->topology()->ndims()));
        break;
    }

    std::string varNames = getVarName(0);
    for (uint64_t i = 1; i < nVars(); ++i)
      varNames += (" " + getVarName(i));


    //    void write1DData2Tecplot(
    //        const std::function<double(const int32_t iDat, const int32_t iVar)>& dataFun,
    //        const std::function<int32_t(const int32_t iElem, const int32_t iVrt)>& connFun,
    //        const std::function<int32_t(std::vector<int32_t>& buffer, const int32_t iElem)>& fnConnFun,
    //        const ZoneType zoneType,
    //        const VarLoc varLoc,
    //        const DataSharing dataSharing,
    //        const FileType fileType,
    //        const int32_t nVars,
    //        const int32_t nPoints,
    //        const int32_t nElements,
    //        const std::string& fileName,
    //        const std::string& dataSetName,
    //        const std::string& zoneName,
    //        const std::string& varNames,
    //        const int32_t data_id,
    //        const double solTime,
    //        const bool writeConnectivity,
    //        const bool writeFaceNeighborConnectivity,
    //        const MyMPI::MPIMgr& mpiMgr );

    uint64_t nPoints = grid()->nVerts();
    MYASSERT(nPoints != uint64_t(-1), std::string("trying to output DataGrid, without having previously ")+
             std::string("written corresponding Grid.\n") +
             std::string("This means the number of vertices is unavailable.") );
    //
    //    std::cout << "hello from rank " << mpiMgr.rank() << ", have " << nPoints << " points, "
    //        << prtGrd_->nNonGhost() << " cells\n"
    //        << " cells_.size(): " << cells_.size() << std::endl;

    //    std::cout << mpiMgr.rank() << " reached " << __FILE__ << " ln: " << std::to_string(__LINE__) << std::endl;

    Tecplot::write1DData2Tecplot(
        std::function(dataFun),
        std::function(Tecplot::NO_CONNECTIVITY),
        std::function(Tecplot::NO_FACENEIGHBORS),
        zoneType,
        std::vector<Tecplot::VarLoc>(nVars(),Tecplot::VarLoc::CellCentered),      // variable location is at cell centers
        Tecplot::DataSharing::DISTRIBUTED,  // data is distributed across procs
        Tecplot::FileType::SolutionFile,    // write a solution file
        int32_t(nVars()),
        int32_t(nPoints),                    // number of (unique) vertices
        int32_t(prtGrd_->nNonGhost()),       // number of non-ghost cells
        fileName,
        name(),
        name(),
        varNames,
        -1,         // id
        solTime,
        false,
        false,
        mpiMgr );

    //    std::cout << mpiMgr.rank() << " reached " << __FILE__ << " ln: " << std::to_string(__LINE__) << std::endl;

    //    std::cout  << "rank " << mpiMgr.rank() << " done" << std::endl;
  }


  std::unique_ptr<GenericCallback>
  DataGrid::makeOutputCallback(
      Grids::Grid* nodeGrid,
      const GenericCallback* const prtGrdOutputCallback,
      const Settings::Dictionary& dict,
      const std::string& dirPref )
  const
  {

    std::string dir = dirPref;
    dir = MyString::catDirs(dir,dict.get<std::string>("directory",""));
    dir = MyString::makeLastCharDirSep(dir);


    return std::unique_ptr<GenericCallback>(
        newCallback(
            [this,dir,nodeGrid,prtGrdOutputCallback](
                const double solTime,
                const uint64_t tn,
                const MyMPI::MPIMgr& mpiMgr,
                MyChrono::TimerCollection& timers ) {

      static uint64_t particleGridChangeCountAfterLastOutput = -1;

      if (prtGrd_->changeCount() != particleGridChangeCountAfterLastOutput)
        {
          prtGrdOutputCallback->call(solTime,tn,mpiMgr,timers); // own master region, barrier
#pragma omp single nowait
          particleGridChangeCountAfterLastOutput = prtGrd_->changeCount();
        }

      timers.start("output"); // own master region
#pragma omp master
      {

        // print message
        std::string fName = this->name();

        if (mpiMgr.isRoot())
          std::cout << "writing ts #" << tn << " solution on grid \"" << fName << "\"" << std::endl;

        // prepend directory and designation
        fName = dir + std::string("solution_") + fName;

        // append solution time
        std::stringstream fnamestr;

        fnamestr.str(std::string(""));
        fnamestr << fName << "_at_ts_";
        fnamestr << std::setw(10);
        fnamestr << std::setfill('0');
        fnamestr << tn;
        fnamestr << "_time_";
        fnamestr << std::fixed;
        fnamestr << std::setw(20);
        fnamestr << std::setprecision(10);
        fnamestr << std::setfill('0');
        fnamestr << solTime;

        fName = fnamestr.str();

        // write
        this->writeGrid(
            fName,
            solTime,
            mpiMgr );

      } // master region
      timers.stop("output");
#pragma omp barrier

    },
    dict) // newCallback
    ); // unique_ptr
  }




} // namespace Grids
