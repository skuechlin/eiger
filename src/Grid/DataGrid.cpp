/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * DataGrid.cpp
 *
 *  Created on: Oct 15, 2014
 *      Author: kustepha
 */


#include <Grid/CellData.h>
#include <omp.h>
#include <Primitives/Callback.h>
#include <iomanip>

#include "Grid/DataGrid.h"
#include "GridTopology/GridTopology.h"
#include "Moments/AveragingStrategies.h"

#include "Settings/Dictionary.h"

#include "Primitives/MyError.h"
#include "Primitives/MyString.h"
#include "Primitives/MyTransform.h"
#include "Primitives/Timer.h"
#include "IO/Tecplot/Tecplot.h"

#include "Parallel/MyMPI.h"

namespace Grids
{

  // memory factor for time averaging. tn is the time step number
  double DataGrid::alpha(const uint64_t tn) const { return averager_->operator()(tn); }

  void DataGrid::setCellPtrs()
  {
    cellptrs_.resize(cells_.size());
    CellData** cellptr = cellptrs_.data();
    for (auto c = cells_.begin(); c != cells_.end(); ++c )
      *cellptr++ = &(*c);
  }



  bool
  DataGrid::checkConsistencyWithParticleGrid(PassKey, std::string& msg, std::vector<uint64_t>& fails)
  const
  {


    bool size_pass = ( nCells() == grid()->nCells() );

    if (!size_pass)
      {
        msg += std::string("\n    number of cells on data grid does not match number of cells on particle grid");

        return size_pass;
      }

    bool vol_pass = true;

#pragma omp parallel for schedule(static) reduction(&&:vol_pass)
    for ( uint64_t i = 0; i < nCells(); ++i)
      vol_pass = vol_pass && (cellptrs_[i]->volume_ == prtGrd_->cellVolume(i));

    if (!vol_pass)
      {
        msg += std::string("\n    volumes of cells on data grid do not match volumes of cells on particle grid");
        for ( uint64_t i = 0; i < nCells(); ++i)
          if ( cellptrs_[i]->volume_ != prtGrd_->cellVolume(i) )
            fails.push_back(i);
      }

    bool key_pass = true;

#pragma omp parallel for schedule(static) reduction(&&:key_pass)
    for ( uint64_t i = 0; i < nCells(); ++i)
      key_pass = key_pass && (cellptrs_[i]->key_ == prtGrd_->cellKey(i));

    if (!key_pass)
      {
        msg += std::string("\n    keys of cells on data grid do not match keys of cells on particle grid");
        for ( uint64_t i = 0; i < nCells(); ++i)
          if ( cellptrs_[i]->key_ != prtGrd_->cellKey(i) )
            fails.push_back(i);
      }


    return vol_pass && key_pass;
  }



  void
  DataGrid::updateCellVolumesFromParticleGrid(PassKey)
  {

#pragma omp parallel for schedule(static)
    for ( uint64_t i = 0; i < nCells(); ++i)
      cellptrs_[i]->setVolume( prtGrd_->cellVolume(i) );
  }





  void
  DataGrid::transformCells(
      const PassKey& key,
      const std::vector<uint64_t>& remove,
      const std::vector<uint64_t>& merge,
      const std::vector<uint64_t>& split )
  {
    static_cast<void>(key);

    if ( remove.size() == 0 &&
        merge.size() == 0 &&
        split.size() == 0 )
      return;

    auto topo = grid()->topology();
    uint64_t block_size = topo->nCellsPerCell(0,1);
    double inv_dblock_size = 1.0 / static_cast<double>(block_size);

    MyTransform::remove_merge_split(
        cells_,
        remove,
        merge,
        split,
        block_size, // block size
        [topo](CellData& lhs, auto begin, const uint64_t n_to_merge)
        ->void{ lhs.merge(begin,n_to_merge,topo->cellBox(lhs.key(),lhs.level()+1)); }, // merger
        [inv_dblock_size,topo](CellData src /* by value */, const uint64_t, const uint64_t j)
        ->CellData{
          const uint8_t ss_lvl = src.level() - 1;
          const uint64_t key_stride = topo->nCellsPerCell(0,ss_lvl);
          const uint64_t ss_key = src.key() + j*key_stride;
          src.toSubCell(topo->cellBox(ss_key,ss_lvl),ss_key,inv_dblock_size); return src; } // splitter
    );

    // update cell pointers
    setCellPtrs();

  }

  // cell i
  const CellData& DataGrid::cell(const uint64_t i) const { return *cellptrs_[i]; }

  // cell i
  CellData& DataGrid::cell(const uint64_t i) { return *cellptrs_[i]; }


  /*static*/ uint64_t DataGrid::nVars() { return 1 + CellData::nVars(); }


  /*static*/ std::string DataGrid::getVarName(const uint64_t varId) {
    if (varId == 0) return "wf";
    return CellData::getVarName(varId-1); }

  double DataGrid::getVar(const uint64_t i, const uint64_t varId) const {
    if (varId == 0) return prtGrd_->cell(i).wf;
    return cellptrs_[i]->getVar(varId-1); }






  void
  DataGrid::insertGhostCells(
      const RegularGrid::CellMsgT* const ghost_cells_to_insert,
      const uint64_t n)
  {
    for (uint64_t i = 0; i < n; ++i)
      cells_.emplace_back(
          grid()->topology()->cellBox(ghost_cells_to_insert[i].key,ghost_cells_to_insert[i].cell.level),
          ghost_cells_to_insert[i].key,
          ghost_cells_to_insert[i].cell.volume,
          ghost_cells_to_insert[i].cell.level);
  }

  void DataGrid::insertCells(CellData* const cells_to_insert, const uint64_t n)
  {
    cells_.insert(cells_.begin(), cells_to_insert, cells_to_insert + n);
  }

  void DataGrid::finalizeCellInsertion()
  {
    // sort cells by keys
    // "The order of equal elements is preserved."
    // ...which is why we insert ghost cells at end, regular cells at begin
    cells_.sort([](const CellData& a, const CellData& b)->bool{return a.key() < b.key();});

    // make unique
    // "Only the first element in each group of equal elements is left."
    cells_.unique([](const CellData& a, const CellData& b)->bool{return a.key() == b.key();});

    // refresh vector of pointers to cells
    setCellPtrs();
  }



  DataGrid::DataGrid(
      RegularGrid* prtGrd,
      const Settings::Dictionary& dict )
  : DataGrid(
      prtGrd,
      dict.get<std::string>("name"),
      Moments::Averaging::makeAveragingStrategy( dict.get<Settings::Dictionary>("averaging") )
  )
  {}

  DataGrid::DataGrid(
      RegularGrid* prtGrd,
      const std::string& name,
      std::unique_ptr<Moments::Averaging::AveragingStrategy> averager
  )
  : prtGrd_(prtGrd)
  , name_(name)
  , averager_(std::move(averager))
  {

    const uint64_t size = prtGrd_->nCells();

    MYASSERT( size < cells_.max_size(),
              std::string("requested size of data grid \"")
    + name_
    + std::string("\" exceeds max cells vector size") );

    //    cells_.reserve( size );

    for (uint64_t i = 0; i < size; ++i)
      cells_.emplace_back(
          prtGrd_->cellBox(i),
          prtGrd_->cellKey(i),
          prtGrd_->cellVolume(i),
          prtGrd_->cellLevel(i) );

    setCellPtrs();

  }


  DataGrid::~DataGrid() = default;


} // namespace Grids
