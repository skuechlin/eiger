/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * GridManagerLoadBalancing.cpp
 *
 *  Created on: Jun 29, 2016
 *      Author: kustepha
 */



//#include <vector>
//#include <set>
//#include <numeric> // partial sum


#ifdef __STRICT_ANSI__
#define STRICT_ANSI_STATE
#undef __STRICT_ANSI__
#endif

#include <parallel/algorithm> // copy_backward, stable_sort, unique

#ifdef STRICT_ANSI_STATE
#define __STRICT_ANSI__ STRICT_ANSI_STATE
#endif

#include "Parallel/MyMPI.h"
#include "Parallel/MyMPI_NeighborComm.h"
#include "Parallel/MyMPI_DSDEComm2.h"
#include "Parallel/MyMPI_AlltoallComm.h"

#include "Primitives/MyError.h"
#include "Primitives/MyTransform.h"
#include <Primitives/Callback.h>
#include "Primitives/Timer.h"
#include "Primitives/CRC32.h"
#include "Primitives/MyMemory.h"
#include "Primitives/MyCount.h"

#include "Settings/Dictionary.h"

#include "GridManager.h"

#include <Grid/CellData.h>

#include "GridManagerLoadBalancing_Detail.h"

#include "Particles/ParticleCollection.h"



namespace Grids
{

  using namespace Grids::LoadBalancing;

  double
  GridManager::cellLoad(
      const uint64_t cell_idx,
      const LoadMetric metric)
  const
  {

    constexpr uint64_t particle_memory_load = sizeof(Particles::Particle);

    auto lg = loadGrid_;

    switch (metric) {
      case LoadMetric::CPU:
        return 1. +  lg->cell(cell_idx).Np();
        break;
      case LoadMetric::MEMORY:
        {
          const uint64_t cell_memory_load =
              sizeof(CellData)*dataGrids_.size() // cell on n data grids
              + RegularGrid::cellMemoryLoad(); // cell on particle grid
          return .25 * static_cast<double>(cell_memory_load)
              + static_cast<double>(particle_memory_load)*lg->cell(cell_idx).Np();
        }
        break;
      default: // this should never happen
        MYASSERT(false,"invalid load metric enum");
        return 0.123456789;
        break;
    }
  }


  double GridManager::loadBalance(const MyMPI::MPIMgr& mpiMgr)
  {

    double wtot  = 0.;
    double wtotg = 0.;
    double wmaxg = 0.;
    double blnce = 0.;

    auto pg = particleGrid();

#pragma omp parallel for schedule(static) reduction(+:wtot)
    for (uint64_t i = pg->first_; i < pg->last_; ++i)
      wtot += cellLoad(i,LoadMetric::MEMORY) ;

//    MPI_Request req[2];

    MPI_Reduce(&wtot,&wmaxg,1,MPI_DOUBLE,MPI_MAX,mpiMgr.root(),mpiMgr.comm());
    MPI_Reduce(&wtot,&wtotg,1,MPI_DOUBLE,MPI_SUM,mpiMgr.root(),mpiMgr.comm());

//    MPI_Waitall(2,req,MPI_STATUSES_IGNORE);

    if (mpiMgr.isRoot())
      blnce = balance(wmaxg, wtotg, static_cast<double>(mpiMgr.nRanks()));


    MPI_Bcast(&blnce,1,MPI_DOUBLE,mpiMgr.root(),mpiMgr.comm());


    return blnce;

  }

  void
  GridManager::rebalanceLoad(const MyMPI::MPIMgr& mpiMgr, const int tag)
  {

    const int rank = mpiMgr.rank();

    // sanity check: check consistency of keys / volumes between grid and data grids
    assertParticleGridDataGridConsistency(
        std::string("lb ln ") + std::to_string(__LINE__) + std::string(", rank ") + std::to_string(rank)
    );

    RegularGrid*    pg = particleGrid();
    Grid*           ng = nodeGrid();

    balanceNodeGrid(
        this,
        mpiMgr,
        tag);

    // register node grid change
    ng->countChange();

    // count the particle grid keys on the new node grid
#pragma omp parallel
    {
      MyCount::parallelCount(
          ng->count_.data(),
          ng->begin_.data(),
          ng->key_.data(),
          ng->nCells(),
          pg->key_.data() + pg->first_,
          pg->nNonGhost() );
    }


    // construct MPI type
    MPI_Datatype cellmsg_mpi_t;
    MPI_Type_contiguous(sizeof(CellMsgT),MPI_BYTE,&cellmsg_mpi_t);
    MPI_Type_commit(&cellmsg_mpi_t);


    // assemble data to send

    CellMsgT*     key_send_buffer; // 1

    std::vector<uint64_t>     key_count;
    std::vector<uint64_t>     key_dest_ranks;

    CellMsgT*     ghost_keys_send_buffer; // 2

    std::vector<uint64_t>     ghost_keys_count;
    std::vector<uint64_t>     ghost_keys_dest_ranks;

    fillLoadBalanceSendBuffers(
        key_send_buffer,
        key_count,
        key_dest_ranks,
        ghost_keys_send_buffer,
        ghost_keys_count,
        ghost_keys_dest_ranks,
        ng,
        pg,
        mpiMgr.rank(),mpiMgr.nRanks());

    // communication

    MyMPI::DSDECommSpec2
    keycommspec(
        key_dest_ranks.size(),
        key_dest_ranks.data(),
        key_count.data(),
        mpiMgr.comm(),
        tag + COMPILE_TIME_CRC32_STR( FILE_POS_STR ));

    // reduce total key receive count
    uint64_t n_keys_to_receive = keycommspec.n_to_receive();

    // communicate combined key, cell structs
    CellMsgT * const key_recv_buffer = MyMemory::aligned_alloc<CellMsgT>(n_keys_to_receive); //3
    //(CellMsgT*)aligned_alloc(alignof(CellMsgT),n_keys_to_receive*sizeof(CellMsgT));
    MYASSERT(key_recv_buffer != nullptr,"aligned_alloc for recv_keys failed");

    keycommspec.comm(
        key_send_buffer,
        key_recv_buffer,
        sizeof(CellMsgT),
        cellmsg_mpi_t,
        tag + COMPILE_TIME_CRC32_STR( FILE_POS_STR ));
    MyMemory::aligned_free(key_send_buffer); //-1

    MyMPI::DSDECommSpec2
    ghostcommspec(
        ghost_keys_dest_ranks.size(),
        ghost_keys_dest_ranks.data(),
        ghost_keys_count.data(),
        mpiMgr.comm(),
        tag + COMPILE_TIME_CRC32_STR( FILE_POS_STR ));

    // reduce total ghost receive count
    uint64_t n_ghost_to_receive = ghostcommspec.n_to_receive();

    // communicate ghost cell keys, storing new keys in separate buffer
    CellMsgT * const ghost_keys_recv_buffer = MyMemory::aligned_alloc<CellMsgT>(n_ghost_to_receive); //4
    //(CellMsgT*)aligned_alloc(alignof(CellMsgT),n_ghost_to_receive*sizeof(CellMsgT));
    MYASSERT(ghost_keys_recv_buffer != nullptr,"aligned_alloc for recv_ghost_keys failed");

    ghostcommspec.comm(
        ghost_keys_send_buffer,
        ghost_keys_recv_buffer,
        sizeof(CellMsgT),
        cellmsg_mpi_t,
        tag + COMPILE_TIME_CRC32_STR( FILE_POS_STR ));
    MyMemory::aligned_free(ghost_keys_send_buffer); //-2

    // no longer need cell message mpi type
    MPI_Type_free(&cellmsg_mpi_t);


    // insert ghost cells in data grids
#pragma omp taskgroup
    {
      for( auto dgit = dataGrids_.begin(); dgit != dataGrids_.end(); ++dgit)
        {
#pragma omp task default(shared) firstprivate(dgit)
          dgit->insertGhostCells(ghost_keys_recv_buffer,n_ghost_to_receive);
        }
    }

    // communicate regular cells

    // regular cell mpi type
    MPI_Datatype cell_mpi_t;
    MPI_Type_contiguous(sizeof(CellData)/sizeof(double),MPI_DOUBLE,&cell_mpi_t);
    MPI_Type_commit(&cell_mpi_t);

    CellData* cells_sendbuffer = nullptr;
    if (keycommspec.n_to_send() > 0)
      {
        cells_sendbuffer = MyMemory::aligned_alloc<CellData>(keycommspec.n_to_send()); //5
        //(Cell*)aligned_alloc(alignof(Cell),uint64_t(keycommspec.n_to_send())*sizeof(Cell));
        MYASSERT(cells_sendbuffer != nullptr,"aligned_alloc failed to allocate "
                 + std::to_string(uint64_t(keycommspec.n_to_send())*sizeof(CellData)) + " Bytes for cells_sendbuffer");
      }

    CellData* cells_recvbuffer = nullptr;
    if (keycommspec.n_to_receive() > 0)
      {
        cells_recvbuffer = MyMemory::aligned_alloc<CellData>(keycommspec.n_to_receive()); //6
        //(Cell*)aligned_alloc(alignof(Cell),uint64_t(keycommspec.n_to_receive())*sizeof(Cell));
        MYASSERT(cells_recvbuffer != nullptr,"aligned_alloc failed to allocate "
                 + std::to_string(uint64_t(keycommspec.n_to_receive())*sizeof(CellData)) + " Bytes for cells_recvbuffer");
      }

    int idg = 0;
    constexpr int cellcommtag = COMPILE_TIME_CRC32_STR( FILE_POS_STR );
    for( auto dgit = dataGrids_.begin(); dgit != dataGrids_.end(); ++dgit)
      {

        // fill send buffer
        uint64_t current_cell = dgit->grid()->first_;
        CellData* sb = cells_sendbuffer;

        for (int i = 0; i < keycommspec.n_ranks(); ++i)
          {
            if (i != keycommspec.rank())
              for (uint64_t j = 0; j < ng->count_[i]; ++j)
                *(sb++) = dgit->cell(current_cell++);
            else
              current_cell += ng->count_[i];
          }

        // ATTENTION: if we used _mm_malloc() to allocate send/recv buffers,
        // disable mpi_leave_pinned via "--mca mpi_leave_pinned 0" argument to mpirun
        // since MPI internal intercept of memory allocation via ptmalloc2 won't work,
        // hence leading to unpredictable behavior when trying to keep memory registered
        // for repeated call of same comm pattern

        // communicate
        keycommspec.comm(cells_sendbuffer, cells_recvbuffer, sizeof(CellData), cell_mpi_t , tag + cellcommtag + idg);

        // insert received cells
        dgit->insertCells(cells_recvbuffer,keycommspec.n_to_receive());

#pragma omp task firstprivate(dgit)
        dgit->finalizeCellInsertion();

        ++idg;
      }

    MyMemory::aligned_free(cells_sendbuffer); //-3
    MyMemory::aligned_free(cells_recvbuffer); //-4

    // no longer need cell mpi type
    MPI_Type_free(&cell_mpi_t);

    merge_cells(
        pg,
        key_recv_buffer,
        n_keys_to_receive,
        ghost_keys_recv_buffer,
        n_ghost_to_receive,
        ng->key_[mpiMgr.rank()],
        (mpiMgr.rank() + 1 == mpiMgr.nRanks()) ? -1 : ng->key_[mpiMgr.rank()+1]
    );

    // no need for receive buffers anymore
    MyMemory::aligned_free(ghost_keys_recv_buffer); //-5
    MyMemory::aligned_free(key_recv_buffer); //-6

    // wait for data grid processing to complete
#pragma omp taskwait

    assertParticleGridDataGridConsistency(
        std::string("lb ln ") + std::to_string(__LINE__) + std::string(", rank ") + std::to_string(mpiMgr.rank())
    );

    // remove any cells that are no longer direct neighbors of cells in range first_,...,last_
    // this will also remove all sent cells that do not remain as ghost cells
    removeObsoleteGhostCells();

    // update cell volume statistics
    pg->updateCellVolumeStatistics();

    // sanity check: check consistency of keys / volumes between grid and data grids
    assertParticleGridDataGridConsistency(
        std::string("lb ln ") + std::to_string(__LINE__) + std::string(", rank ") + std::to_string(mpiMgr.rank())
    );
  }

  std::unique_ptr<GenericCallback>
  GridManager::makeLoadBalancingCallback(
      const double threshold,
      const Settings::Dictionary& dict)
  {
    return std::unique_ptr<GenericCallback>( newCallback(
        [this,threshold](
            const double,
            const uint64_t tn,
            const MyMPI::MPIMgr& mpiMgr,
            MyChrono::TimerCollection& timers )
            ->void{

      static double balance;

      MYASSERT(threshold > 0.,"illegal threshold " + std::to_string(threshold));
      MYASSERT(threshold <= 1.,"illegal threshold " + std::to_string(threshold));

      timers.start("load_balance"); // own master region

#pragma omp master
      {
        printParticlesAndCellsPerRank(
            simulationGrid(),loadGrid(),mpiMgr.root(),mpiMgr.comm());

        balance = loadBalance(mpiMgr);

        if (mpiMgr.isRoot())
          std::cout << "\ncurrent load balance: " << balance << " (threshold: " << threshold << ")" << std::endl;

        if (balance < threshold)
          {
            rebalanceLoad(mpiMgr,tn);

            double new_balance = loadBalance(mpiMgr);
            if (mpiMgr.isRoot())
              { std::cout << "\nnew load balance: " << new_balance << "\n"; std::cout << std::endl; }

          }
      } // master

      timers.stop("load_balance"); // own master region
#pragma omp barrier


    },  dict ) // Callback()
    ); // uniquePtr
  }

} // namespace Grids
