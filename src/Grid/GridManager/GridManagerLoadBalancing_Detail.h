/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * GridManagerLoadBalancing_Detail.h
 *
 *  Created on: Sep 15, 2016
 *      Author: kustepha
 */

#ifndef SRC_GRID_GRIDMANAGER_GRIDMANAGERLOADBALANCING_DETAIL_H_
#define SRC_GRID_GRIDMANAGER_GRIDMANAGERLOADBALANCING_DETAIL_H_

#include "mpi.h"

#include "Grid/GridForwardDecls.h"

namespace MyMPI
{
  class MPIMgr;
}

namespace Grids
{
  namespace LoadBalancing
  {

    using CellMsgT = RegularGrid::CellMsgT;

    void merge_cells(
        RegularGrid* const g,
        const CellMsgT* const keys_to_merge,
        const uint64_t n_to_merge,
        const CellMsgT* const ghost_keys_to_merge,
        const uint64_t n_ghost_to_merge,
        const uint64_t new_first_key_,
        const uint64_t new_first_key_of_next_rank);

    inline
    double
    balance(
        const double max_load,
        const double total_load,
        const double num_partitions)
    {
      const double average_load = total_load / num_partitions;
      return average_load / max_load;
    }

    void
    printParticlesAndCellsPerRank(
        const DataGrid *const sg, const DataGrid *const lg, int root, MPI_Comm comm);

    void
    balanceNodeGrid(
        GridManager *const gm,
        const MyMPI::MPIMgr& mpiMgr, const int tag);

    void
    fillLoadBalanceSendBuffers(
        CellMsgT*&      key_send_buffer,
        std::vector<uint64_t>&      key_count,
        std::vector<uint64_t>&      key_dest_ranks,
        CellMsgT*&      ghost_keys_send_buffer,
        std::vector<uint64_t>&      ghost_keys_count,
        std::vector<uint64_t>&      ghost_keys_dest_ranks,
        Grid *const ng,             // the node grid
        RegularGrid *const pg,      // the particle grid
        const uint64_t rank, const uint64_t nranks);

  }
}


#endif /* SRC_GRID_GRIDMANAGER_GRIDMANAGERLOADBALANCING_DETAIL_H_ */
