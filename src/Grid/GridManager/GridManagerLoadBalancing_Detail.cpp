/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * GridManagerLoadBalancing_Detail.cpp
 *
 *  Created on: Sep 15, 2016
 *      Author: kustepha
 */


#ifdef __STRICT_ANSI__
#define STRICT_ANSI_STATE
#undef __STRICT_ANSI__
#endif

#include <parallel/algorithm> // copy_backward, stable_sort, unique

#ifdef STRICT_ANSI_STATE
#define __STRICT_ANSI__ STRICT_ANSI_STATE
#endif

#include <vector>
#include <set>
#include <numeric> // partial sum

#include "Parallel/MyMPI.h"

#include "Primitives/MySearch.h"
#include "Primitives/MyCount.h"
#include "Primitives/CRC32.h"
#include "Primitives/MyMemory.h"
#include "Primitives/MyTransform.h"

#include "GridManager.h"
#include <Grid/CellData.h>

#include "GridManagerLoadBalancing_Detail.h"

#include "LoadBalancing/ExactBisection_Cooperative.h"

namespace Grids
{
  namespace LoadBalancing
  {

    namespace
    {


      void
      splitters2keys(
          uint64_t keys_at_s_new[],
          const uint64_t keys[],
          const uint64_t s_new[],
          const uint64_t s_old[],
          int root,
          MPI_Comm comm,
          int tag)
      {


        int num_ranks;
        MPI_Comm_size(comm,&num_ranks);

        int rank;
        MPI_Comm_rank(comm,&rank);

        // offset in global task index
        const uint64_t mys = s_old[rank];

        auto get_rank_by_global_index   = [&s_old,num_ranks](const uint64_t gind)->uint64_t{
          return MySearch::binary_search_lt(s_old,s_old+num_ranks+1,gind); };

        auto handle_key_request         = [&keys,mys](const uint64_t gind)->uint64_t{
          return keys[ gind - mys ]; };

        auto root_task                  = [&]()->void{
          for (uint64_t i = 0; i < uint64_t(num_ranks); ++i)
            keys_at_s_new[i] = MyMPI::Cooperative::requests_cooperative<uint64_t>(
                s_new[i],
                COMPILE_TIME_CRC32_STR( FILE_POS_STR ) + tag,
                rank,
                get_rank_by_global_index,
                handle_key_request,comm);
        };

        auto request_handler = [&handle_key_request,comm](const MPI_Status& request)->void{
          MyMPI::Cooperative::responds<uint64_t>(request,handle_key_request,comm);
        };

        MyMPI::Cooperative::cooperate(root_task,request_handler,root,comm,tag);

        // broadcast the keys at new splitters to all ranks
        MPI_Bcast(keys_at_s_new,num_ranks,MPI_UINT64_T,root,comm);


      } // splitters2keys


    } // anon namespace


    void
    printParticlesAndCellsPerRank(
        const DataGrid *const sg, const DataGrid *const lg,  int root, MPI_Comm comm)
    {
      int num_ranks;
      MPI_Comm_size(comm,&num_ranks);

      int rank;
      MPI_Comm_rank(comm,&rank);

      std::vector<double> partsPerRank(2*num_ranks);
      std::vector<uint64_t> cellsPerRank(num_ranks);


      const RegularGrid *const pg = sg->grid();

      double np[2] = {0.,0.};
      for (uint64_t i = pg->first_; i < pg->last_; ++i)
        {
          np[0] += sg->cell(i).Np();
          np[1] += lg->cell(i).Np();
        }


      MPI_Gather(np,2,MPI_DOUBLE,partsPerRank.data(),2,MPI_DOUBLE,root,comm);

      const uint64_t nc = pg->nCells();
      MPI_Gather(&nc,1,MPI_UINT64_T,cellsPerRank.data(),1,MPI_UINT64_T,root,comm);

      if (rank == root)
        {
          double pSumS = 0.;
          double pSumL = 0.;
          for (int i = 0; i < num_ranks; ++i){
              pSumS += partsPerRank[2*i];
              pSumL += partsPerRank[2*i+1];
          }

          uint64_t cSum = 0;
          for (int i = 0; i < num_ranks; ++i)
            cSum += cellsPerRank[i];

          const double dCSum = fmax(1.0,static_cast<double>( cSum ));

          std::stringstream ss;
          ss << "\n";
          ss << " _rank_|_#prts_(sim)_|____%___|_#prts_(load)_|____%___|___#cells___|___%___\n";
          ss << "       |             |        |              |        |            |       \n";
          for (int i = 0; i < num_ranks; ++i)
            {
              ss <<  std::setw(6) << i;
              ss << " | " << std::setw(11) << std::lround(partsPerRank[2*i]);
              ss << " | " << std::fixed << std::setprecision(2) << std::setw(6)
              <<  100. * partsPerRank[2*i] / pSumS;
              ss << " | " << std::setw(12) << std::lround(partsPerRank[2*i+1]);
              ss << " | " << std::fixed << std::setprecision(2) << std::setw(6)
              <<  100. * partsPerRank[2*i+1] / pSumL;
              ss << " | " << std::setw(10) << cellsPerRank[i];
              ss << " | " << std::fixed << std::setprecision(2) << std::setw(6)
              <<  100. * static_cast<double>(cellsPerRank[i]) / dCSum;
              ss << "\n";
            }
          ss << " ______|_____________|________|______________|________|____________|_______\n";
          ss << " total | " << std::setw(11) << std::lround(pSumS) << " | 100.00";
          ss << " | "       << std::setw(12) << std::lround(pSumL) << " | 100.00";
          ss << " | "       << std::setw(10) << cSum               << " | 100.00";

          std::cout << ss.str() << std::endl;
        }

    }

    void
    balanceNodeGrid(
        GridManager *const gm,
        const MyMPI::MPIMgr& mpiMgr,
        const int tag)
    {
      if (mpiMgr.isRoot())
        std::cout << "\n  welcome to load balancing!" << std::endl;

      RegularGrid *const pg = gm->particleGrid();

      //      const int rank = mpiMgr.rank();
      const int root = mpiMgr.root();


      // number of tasks (cells)
      const uint64_t N = pg->nNonGhost();

      // compute load
      std::vector<uint64_t> W(N+1);
      uint64_t j = 0;
      for (uint64_t i = pg->first_; i < pg->last_; ++i, ++j)
        W[j] = std::lround( gm->cellLoad(i,GridManager::LoadMetric::MEMORY) );

      // convert load to prefix sum, find maximum local load
      W[N] = 0;
      uint64_t Wtot = 0;
      uint64_t wmaxl = 0;
      for (uint64_t i = 0; i < N+1; ++i)
        {
          uint64_t tmp = W[i];
          wmaxl   = std::max(wmaxl,tmp);
          W[i]    = Wtot;
          Wtot    += tmp;
        }

      uint64_t old_bottleneck; // the maximum sub-sum of weights, only relevant on root
      uint64_t Wtotg; // sum of all weights, only relevant on root


      MPI_Reduce(&Wtot,&old_bottleneck,1,MPI_UINT64_T,MPI_MAX,root,mpiMgr.comm());
      MPI_Reduce(&Wtot,&Wtotg,1,MPI_UINT64_T,MPI_SUM,root,mpiMgr.comm());

      // compute new and old splitters
      std::vector<uint64_t> s_old(mpiMgr.nRanks()+1,0);
      std::vector<uint64_t> s_new(mpiMgr.nRanks()+1,0);

      uint64_t bottleneck = LoadBalance::ExactBisection::Cooperative::exactBisection(
          s_new.data(),
          s_old.data(),
          W.data(),
          wmaxl,
          N,
          mpiMgr.nRanks(),
          mpiMgr.root(),
          mpiMgr.comm(),
          tag + COMPILE_TIME_CRC32_STR( FILE_POS_STR ) );


      if (mpiMgr.isRoot())
        {
          std::cout << "  old bottleneck: " << old_bottleneck << "\n  new bottleneck: " << bottleneck << std::endl;
          std::cout << "    -> old balance: "
              << balance(
                  static_cast<double>(old_bottleneck),
                  static_cast<double>(Wtotg),
                  static_cast<double>(mpiMgr.nRanks())
              )
              << "\n    -> new balance: "
              << balance(
                  static_cast<double>(bottleneck),
                  static_cast<double>(Wtotg),
                  static_cast<double>(mpiMgr.nRanks())
              ) << std::endl;
        }

      // transform the node grid by computing the keys at the new splitters
      splitters2keys(
          gm->nodeGrid()->key_.data(),
          pg->key_.data() + pg->first_,
          s_new.data(),
          s_old.data(),
          mpiMgr.root(),
          mpiMgr.comm(),
          tag + COMPILE_TIME_CRC32_STR( FILE_POS_STR ));
    }


    void
    fillLoadBalanceSendBuffers(
        CellMsgT*&                  key_send_buffer,
        std::vector<uint64_t>&      key_count,
        std::vector<uint64_t>&      key_dest_ranks,
        CellMsgT*&                  ghost_keys_send_buffer,
        std::vector<uint64_t>&      ghost_keys_count,
        std::vector<uint64_t>&      ghost_keys_dest_ranks,
        Grid *const ng,             // the node grid
        RegularGrid *const pg,      // the particle grid
        const uint64_t rank, const uint64_t nranks)
    {




      // count number of keys to send
      uint64_t n_to_send = 0;
      uint64_t num_messages = 0;
      for (uint64_t i = 0; i < nranks; ++i)
        {
          num_messages += !!(ng->count_[i]);
          n_to_send += ng->count_[i];
        }

      n_to_send -= ng->count_[ rank ];
      num_messages -= !!(ng->count_[ rank ]);

      // compile keys, volumes and levels, as well as set of ghost cell indices to communicate
      key_send_buffer = MyMemory::aligned_alloc<CellMsgT>(n_to_send); // free with aligned_free later!
      //(CellMsgT*)aligned_alloc(alignof(CellMsgT),n_to_send*sizeof(CellMsgT));
      CellMsgT* ksb = key_send_buffer;

      key_count.resize(num_messages);
      key_dest_ranks.resize(num_messages);

      // communicate cell keys, volumes and levels as structs

      typedef std::set< uint64_t > set_of_inds_t;
      std::list< set_of_inds_t > list_of_ghost_cells_to_send;
      std::list<uint64_t> list_of_ghost_cells_dest_ranks;


      uint64_t ikeylvlmsg = 0;
      uint64_t nghost_to_send = 0;
      for (uint64_t i = 0; i < ng->nCells(); ++i)
        {
          if (i == rank || ng->count_[i] == 0)
            continue;

          list_of_ghost_cells_to_send.push_back( {} );
          list_of_ghost_cells_dest_ranks.push_back(i);

          set_of_inds_t* ghost_set  = &(list_of_ghost_cells_to_send.back());
          uint64_t begin    = ng->begin_[i];
          uint64_t end      = begin + ng->count_[i];
          uint64_t abs_begin    = pg->first_ + begin;
          uint64_t abs_end      = pg->first_ + end;

          // assemble list of key/level pairs
          key_dest_ranks[ikeylvlmsg]    = i;
          key_count[ikeylvlmsg]         = end - begin;
          ++ikeylvlmsg;

          for (uint64_t j = abs_begin; j < abs_end; ++j)
            new(ksb++) CellMsgT{ pg->cellKey(j), pg->cell(j) };


          // assemble list of neighbors. this routine works with absolute cell indices, NOT starting from pg->first_
          pg->traverseNeighbors(
              GridNeighborStack2::DIRECTION::HIGH | GridNeighborStack2::DIRECTION::LOW,
              [ghost_set,abs_begin,abs_end](const uint64_t, GridNeighborStack2& neighbors)
              ->void{
            while( neighbors() )
              {
                const uint64_t in = neighbors.pop().index;
                if (in < abs_begin || in >= abs_end) // cell not in range of cells to be sent
                  {
#pragma omp critical (LOAD_BALANCING_GHOST_INSERT)
                    ghost_set->insert( in );
                  }
              }
          },
          abs_begin,abs_end);

          nghost_to_send += ghost_set->size();

        }

      // delete empty ghost messages
      auto gctsi = list_of_ghost_cells_to_send.begin();
      auto gcdri = list_of_ghost_cells_dest_ranks.begin();

      while (gctsi != list_of_ghost_cells_to_send.end())
        {
          if (gctsi->size() == 0 )
            {
              list_of_ghost_cells_to_send.erase(gctsi);
              list_of_ghost_cells_dest_ranks.erase(gcdri);
            }
          ++gctsi;
          ++gcdri;
        }


      // copy ghost keys to message buffer
      ghost_keys_send_buffer = MyMemory::aligned_alloc<CellMsgT>(nghost_to_send); // free with aligned_free later!
      //(CellMsgT*)aligned_alloc(alignof(CellMsgT),nghost_to_send*sizeof(CellMsgT));
      CellMsgT* gksb = ghost_keys_send_buffer;

      ghost_keys_count.resize(list_of_ghost_cells_to_send.size());
      ghost_keys_dest_ranks.resize(list_of_ghost_cells_to_send.size());

      // find message sizes
      uint64_t ind = 0;
      for (auto&& l : list_of_ghost_cells_to_send)
        ghost_keys_count[ind++] = l.size();

      // find message destinations
      ind = 0;
      for (auto r : list_of_ghost_cells_dest_ranks)
        ghost_keys_dest_ranks[ind++] = r;

      // fill send buffer
      for (auto&& l : list_of_ghost_cells_to_send)
        {
          for (auto i : l)
            new(gksb++) CellMsgT{
              pg->cellKey(i), pg->cell(i) };
        }


    }

    void merge_cells(
        RegularGrid* const pg,
        const CellMsgT* const keys_to_merge,
        const uint64_t n_to_merge,
        const CellMsgT* const ghost_keys_to_merge,
        const uint64_t n_ghost_to_merge,
        const uint64_t new_first_key_,
        const uint64_t new_first_key_of_next_rank)
    {

      /* ----------------------------------------------------------------------
       * merge data
       * ---------------------------------------------------------------------- */

      // as in cell communication on data grids, insert new ghost cells after existing cells,
      // and new regular cells before existing cells. This ensures that the ensuing stable sort
      // and unique operations will prefer the new inserted regular cells over existing ghost cells
      // with identical keys

      uint64_t old_size = pg->nCells(); // ignore guard
      uint64_t new_size = old_size + n_ghost_to_merge + n_to_merge;

      // resize key, volume, level storage
      pg->key_.resize(    new_size );
      pg->cells_.resize( new_size );

      // shift existing data back to accommodate n_keys_to_receive at beginning of vector
#pragma omp taskgroup
      {

#pragma omp task default(shared)
        std::copy_backward(
            pg->key_.begin(),
            pg->key_.begin()+old_size,
            pg->key_.begin()+old_size+n_to_merge);

#pragma omp task default(shared)
        std::copy_backward(
            pg->cells_.begin(),
            pg->cells_.begin()+old_size,
            pg->cells_.begin()+old_size+n_to_merge);

      }

      uint64_t ind = 0;

      // copy in new regular cell data
      for (uint64_t i = 0; i < n_to_merge; ++i, ++ind)
        {
          pg->key_[ind]     = keys_to_merge[i].key;
          pg->cells_[ind]   = keys_to_merge[i].cell;
        }

      // skip existing data
      ind += old_size;

      // copy in new ghost cell data
      for (uint64_t i = 0; i < n_ghost_to_merge; ++i, ++ind)
        {
          pg->key_[ind]       = ghost_keys_to_merge[i].key;
          pg->cells_[ind]    = ghost_keys_to_merge[i].cell;
        }

      // a temporary permutation array
      std::vector<uint64_t> perm(new_size);
      for (uint64_t i = 0; i < new_size; ++i)
        perm[i] = i;

      // sort permutation array by key
      __gnu_parallel::stable_sort(
          perm.begin(),perm.end(),
          [&pg](const uint64_t a, const uint64_t b)
          ->bool{return pg->key_[a] < pg->key_[b];} );

      // reduce permutation vector to unique keys
      auto perm_end = std::unique(
          perm.begin(),perm.end(),
          [&pg](const uint64_t a, const uint64_t b)
          ->bool{return pg->key_[a] == pg->key_[b];} );

      new_size = std::distance(perm.begin(),perm_end);

      perm.resize(new_size);

#pragma omp taskgroup
      {

#pragma omp task default(shared)
        MyTransform::permute_vector_par(pg->key_,   perm,new_size+2);

#pragma omp task default(shared)
        MyTransform::permute_vector_par(pg->cells_,perm,new_size);
      }

      // clear permutation array
      perm = {};

      // re-set guard data
      pg->key_.back() = uint64_t(-1);
      pg->key_[ pg->key_.size() - 2 ] = pg->key_[ pg->key_.size() - 3 ]
                                                  + pg->topology()->nCellsPerCell(0,pg->cellLevel(pg->key_.size()-3));

      // adjust particle grid count, begin vector lengths
      pg->count_.resize(    pg->key_.size() - 1);
      pg->begin_.resize(    pg->key_.size() - 1);

      // determine new regular cell range

      pg->first_    = pg->locateKey(new_first_key_);

      if ( new_first_key_of_next_rank == uint64_t(-1) )
        pg->last_ = pg->key_.size() - 2;
      else
        pg->last_ = std::distance( pg->key_.begin(),
                                   std::lower_bound(
                                       pg->key_.begin(),
                                       pg->key_.end(),
                                       new_first_key_of_next_rank )
      ); // first cell with key larger or equal to first key of next partition

    }




  } // namespace Detail
} // namespace Grid
