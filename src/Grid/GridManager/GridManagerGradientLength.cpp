/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * GridManagerGradientLength.cpp
 *
 *  Created on: Oct 4, 2016
 *      Author: kustepha
 */

#include <Grid/CellData.h>
#include <Eigen/Dense>

#include "Parallel/MyMPI.h"

#include "Grid/GridManager/GridManager.h"
#include "Grid/GridGhostExchange.h"

using namespace Grids;


void GridManager::computeMaxNormalizedGradientLengthFromKernelEstimates(
    DataGrid *const dg,
    const double rho_ref_inv,    // if 0: use cell average
    const double theta_ref_inv,  // if 0: use cell average
    const double min_num_samples
) {

  const auto  pg = particleGrid();
  const uint8_t nd = topology()->ndims();

#pragma omp parallel for schedule(static)
  for (uint64_t ic = pg->first_; ic < pg->last_; ++ic) {

      auto& c = dg->cell(ic);

      double sm{0.};

      if (c.Nptot() >= min_num_samples) {

          const double irho   = (rho_ref_inv   > 0.) ? rho_ref_inv   : 1./fmax(c.rho(),1.e-60);
          const double itheta = (theta_ref_inv > 0.) ? theta_ref_inv : 1./fmax(c.theta(),1500.);

          v4df v,m;
          v = irho*c.drho();
          m = v*v;

          v = itheta*c.dtheta();
          m = MyIntrin::max(m,v*v);

          /*
          v = itheta*c.de();
          m = MyIntrin::max(m,v*v);
          */

          v = c.dv<0>();
          m = MyIntrin::max(m,itheta*v*v);
          v = c.dv<1>();
          m = MyIntrin::max(m,itheta*v*v);

          if (nd > 2) {
              v = c.dv<2>();
              m = MyIntrin::max(m,itheta*v*v);
          }

          /*
          m *= c.box().width();
          m *= c.box().width();
          */

          for(uint8_t j = 0; j < nd; ++j)
            sm = fmax(sm,MyIntrin::get(m,j));

      }

      c.gradient_length_ = (sm > 0.) ? 1./sqrt(sm) : 0.;

  }

}



// computes length of gradient in variable
void GridManager::computeGradientLengthFromFiniteDifferences(
    DataGrid *const dg ,
    const std::string& varName,
    const double reference,
    const bool relative,
    const MyMPI::MPIMgr& mpiMgr)
{

  auto  pg      = particleGrid();

  // get variable for refinement
  uint64_t iVar = CellData::getVarInd(varName);

  MYASSERT(iVar != uint64_t(-1),
           std::string("computeGradientLength failed: variable name \"") +
           varName + std::string("\" not found"));

  // copy variable values of non-ghost cells
  uint64_t first    = pg->first_;
  uint64_t last     = pg->last_;
  std::vector<double> vals(pg->nCells());
#pragma omp parallel for schedule(static)
  for (uint64_t i = first; i < last; ++i)
    vals[i] = dg->getVar(i,iVar);

  // exchange values on ghost grid
  Grids::exchangeGhostData(
      nodeGrid(),
      pg,
      [pg,&vals](const uint64_t key)->double{return vals[ pg->locateKey(key) ];},
      [pg,&vals](const uint64_t iGhost, const double val)->void{
        vals[ pg->ghostIdx2CellIdx(iGhost) ] = val;}, // map to ghost index range
        mpiMgr ,
        COMPILE_TIME_CRC32_STR( FILE_POS_STR ));

  particleGrid()->traverseNeighbors(
      GridNeighborStack2::DIRECTION::HIGH | GridNeighborStack2::DIRECTION::LOW,
      [this,dg,&vals,reference,relative](
          const uint64_t iCell,
          GridNeighborStack2& neighbors)
          -> void {
    // gradient length saved on data grid
    dg->cell(iCell).gradient_length_ = gradientLength_FD(
        iCell,neighbors,vals,reference,relative);
  }, pg->first_, pg->last_ ); // traverse neighbors

}



double
GridManager::gradientLength_FD(
    const uint64_t i,
    GridNeighborStack2& neighbors,
    const std::vector<double>& vals,
    const double reference_val,
    const bool relative)
{

  double gradient_length = 0.0;

  if (!neighbors())
    return gradient_length; // no neighbors found

  const uint8_t ndims = topology_->ndims();

  const auto *const pg = particleGrid();

  auto cntr = pg->cellCenter(i);
  double center_val = vals[i];

  const uint64_t nneighbors = neighbors.size();

  // gradient reconstruction following
  // Correa, C.D. et al. (2011) - A Comparison of Gradient Estimation Methods for
  //                              Volume Rendering on Unstructured Meshes

  if (nneighbors == 1)
    {
      const auto neighbor        = neighbors.pop();
      const double dv = vals[neighbor.index] - center_val;
      if (dv > 0.)
        gradient_length =  ((relative ? center_val : reference_val) / dv) *
        MyIntrin::snorm(pg->cellCenter(neighbor.index) - cntr);
    }
  else
    {

      Eigen::MatrixXd A(nneighbors, nneighbors > ndims ? ndims + 1 : ndims);
      Eigen::VectorXd b(nneighbors);
      Eigen::VectorXd df(nneighbors);

      if (nneighbors <= ndims)
        {
          // 2,2
          // 2,3
          // 3,3

          // under of fully determined regression (Eq. 13)

          for (uint64_t i = 0; i < nneighbors; ++i)
            {
              const auto neighbor        = neighbors.pop();
              A.row(i)        = to_eigen(pg->cellCenter(neighbor.index) - cntr).head(ndims).transpose();
              b.coeffRef(i)   = vals[neighbor.index] - center_val;
            }

          Eigen::ColPivHouseholderQR< Eigen::MatrixXd > QA( A );

          if (QA.isInvertible())
            {
              df = QA.solve(b);
              const double g = df.norm();
              if (g > 0.)
                gradient_length = (relative ? center_val : reference_val) / g;
            }
          else
            {
              MYASSERT(ndims == 2 && nneighbors == 2,"gradient regression is singular");

              // the cell and its two neighbors are co-linear, treat as 1D
              // 1D+1 regression,
              // df.coeff(1) contains filtered value in cell iCell

              for (uint64_t i = 0; i < 2; ++i)
                A.coeffRef(i,0) = A.row(i).norm();

              A.col(1) = Eigen::Vector2d::Ones();
              b.array() += center_val;

              df = A.colPivHouseholderQr().solve(b);
              const double g = df.coeff(0);
              if (g > 0.)
                gradient_length = (relative ? df.coeff(1) : reference_val) / g;
            }

        }
      else
        {
          // nneighbors > ndims

          // fully or over constrained d+1 regression (3.2.2)
          // df.coeff(ndims) contains filtered estimate of value in cell iCell
          for (uint64_t i = 0; i < nneighbors; ++i)
            {
              const auto neighbor = neighbors.pop();
              A.row(i) << to_eigen(pg->cellCenter(neighbor.index) - cntr).head(ndims).transpose(),1;
              b.coeffRef(i)   = vals[neighbor.index];
            }

          df = A.colPivHouseholderQr().solve(b);
          const double g = df.head(ndims).norm();
          if (g > 0.)
            gradient_length = (relative ? df.coeff(ndims) : reference_val) / g;

        }

    } // if (neighbors == 1){}else{}


  return gradient_length;

}
