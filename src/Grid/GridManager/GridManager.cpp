/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * GridManager.cpp
 *
 *  Created on: Apr 20, 2016
 *      Author: kustepha
 */




#include <Grid/CellData.h>
#include "GridManager.h"

#include "Primitives/MyError.h"

#include "IO/Output.h"


void
Grids::GridManager::updateCellVolumesOnDataGridsFromParticleGrid()
{
  for (auto&& dg : dataGrids_)
    dg.updateCellVolumesFromParticleGrid({});
}


uint64_t
Grids::GridManager::transform(const Grids::GridTransform& tr)
{
  uint64_t count = tr.size();

  if (count > 0)
    {
      // apply to particle grid

      auto* pg = particleGrid();


#pragma omp taskgroup
      {
#pragma omp task default(shared)
        pg->transform( tr.remove_, {}, tr.split_ );

        // apply to data grids
        for (auto dgit = dataGrids_.begin(); dgit != dataGrids_.end(); ++dgit)
          {
#pragma omp task default(shared) firstprivate(dgit)
            dgit->transformCells({}, tr.remove_, {}, tr.split_ );
          }

      }

    }

  // return number of changed cells
  return count;

}

uint64_t
Grids::GridManager::removeObsoleteGhostCells()
{
  // clean up: find and remove obsolete ghost cells, i.e. cells that
  // are not direct neighbors of any cell in range [first_,last_)
  auto pg = particleGrid();
  auto first = pg->first_;
  auto last = pg->last_;

  constexpr uint8_t color = 1;

  pg->removeCellColors(color);

  // color low + high ghost cells

  for (uint hi = 0; hi <= 1; ++hi) {

      pg->addColorByNeighbors(
          [first,last,color](const uint64_t, GridNeighborStack2& neighbors)->uint8_t{
        while( neighbors() ) {
            const auto n = neighbors.pop();
            if (n.index >= first && n.index < last)
              return 0;
        }
        return color; // no neighbor with index in regular cell range
      },
      1, // max level delta
      hi ? last         : 0,    // low/hi ghost range start
          hi ? pg->nCells() : first // low/hi ghost range end
      );

  }

  GridTransform tr( pg );
  tr.append( GridTransform::REMOVE, 0, color );

  return transform( tr );

}


void
Grids::GridManager::assertParticleGridDataGridConsistency(const std::string& pre_msg)
const
{
  for ( auto&& d : dataGrids_ )
    {

      std::vector<uint64_t> fails = {};

      std::string msg(pre_msg);
      bool pass = d.checkConsistencyWithParticleGrid({},msg,fails);

      if (!pass)
        {
          std::cout << msg << "\n";
          std::cout << "on grid " << d.name() << ":\n"
              << " particle grid:\n"
              << "   first: " << particleGrid_->first_
              << "   last: " << particleGrid_->last_
              << "   nCells: " << particleGrid_->nCells() << "\n"
              << " " << d.name() << ":\n"
              << "   nCells: " << d.nCells()  << "\n"
              << " number of cell check fails: " << fails.size() << std::endl;

          for (uint64_t i = 0; i < std::min(fails.size(),uint64_t(10)); ++i)
            std::cout << fails[i] << " "
            << particleGrid_->cellKey(fails[i]) << ": "
            << particleGrid_->cellVolume(fails[i]) << " <> "
            << d.cell(fails[i]).key() << ": "
            << d.cell(fails[i]).volume() << std::endl;
        }

      MYASSERT(pass,"assertParticleGridDataGridConsistency failed, see std::cout");
    }
}


Grids::GridManager::CallbackT
Grids::GridManager::popCallback()
{
  if (!(callbacks_.empty()))
    {
      Grids::GridManager::CallbackT ret = std::move( callbacks_.back() );
      callbacks_.pop_back();
      return ret;
    }
  else
    return 0;
}


Grids::GridManager::~GridManager() {
  // we are responsible for the grids
  // taken care of by smart ptr
}
