/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * GridManagerConstructor.cpp
 *
 *  Created on: Sep 29, 2016
 *      Author: kustepha
 */


#include <Grid/CellData.h>
#include "Grid/GridManager/GridManager.h"
#include "Grid/GridTopology/TopologyFactory.h"

#include "Settings/Dictionary.h"

#include "Primitives/Partitioning.h"

#include "IO/Output.h"

#include "Parallel/MyMPI.h"


Grids::GridManager::GridManager(
    const Settings::Dictionary& dict,
    const SimulationBox& simBox,
    const MyMPI::MPIMgr& mpiMgr,
    const std::string& dirPref )
: topology_( std::move( makeTopology(
    dict.get<Settings::Dictionary>("topology"), simBox ) ) )
{


  // first, add node grid
  addNodeGrid(
      std::unique_ptr<Grids::Grid>(
          new class Grid(
              topology_.get(),
              "node_grid",
              mpiMgr.nRanks(),
              topology_->maxLevel() )
      )
  );

  Grid* nGrd = nodeGrid();

  // proc id
  uint64_t proc_id = mpiMgr.rank();

  // the key range on this proc
  uint64_t start_key = nodeGrid_->key_[proc_id];
  uint64_t n_cells_at_max_level = MyPartitioning::partitionLength(
      proc_id,
      topology_->nCellsTot(topology_->maxLevel()),
      nodeGrid_->last_ - nodeGrid_->first_ );


  // DEBUG
  //    std::cout << "proc_id: " << proc_id << std::endl;
  //    std::cout << "start_key: " << start_key << std::endl;
  //    std::cout << "n_cells_tot: " << topology_->nCellsTot(topology_->maxLevel()) << std::endl;
  //    std::cout << "n_cells_at_max_level: " << n_cells_at_max_level << std::endl;

  // there is another grid to add
  bool next = true;

  // number of grid to add
  unsigned int i = 0;

  std::string simGrdName("");


  // description of all data grids
  MYASSERT(dict.hasMember("data grid"),"grid dictionary missing entry\"data grid\"");
  Settings::Dictionary array_of_data_grd_dicts = dict.get<Settings::Dictionary>("data grid");

  // particle grid description
  MYASSERT(dict.hasMember("particle grid"),"grid dictionary missing entry\"particle grid\"");
  Settings::Dictionary prtGrdDict = dict.get<Settings::Dictionary>("particle grid");
  MYASSERT(!(prtGrdDict.isArray()),"particle grid dictionary may not be an array");

  Grids::RegularGrid* prtGrd;
  std::string prtGrdName;
  uint8_t level;

  // data grid description
  Grids::DataGrid* dataGrd;
  Settings::Dictionary dataGrdDict;
  Settings::Dictionary outDict;


  // add particle grid:

  // get the level specification
  level = prtGrdDict.get<uint8_t>("level",topology_->maxLevel());


  MYASSERT(level <= topology_->maxLevel(),std::string("desired level \"") +
           std::to_string(level) +
           std::string("\" of cells on ParticleGrid \"") +
           prtGrdName +
           std::string("\" cannot be satisfied by topo with max. level \"") +
           std::to_string(topology_->maxLevel()) +
           std::string("\"!"));


  // add it
  addParticleGrid(
      std::unique_ptr<Grids::RegularGrid>(
          new class Grids::RegularGrid(
              topology_.get(),
              start_key,
              n_cells_at_max_level * topology_->nCellsPerCell(level,topology_->maxLevel()),
              topology_->maxLevel(), // default level
              prtGrdDict
          )
      )
  );

  prtGrd = particleGrid();


  if (prtGrdDict.hasMember("output"))
    outDict = prtGrdDict.get<Settings::Dictionary>("output");
  else
    outDict = Settings::Dictionary();


  // add particle grid output

  callbacks_.emplace_back(
      newOutput(
          [prtGrd,nGrd](
              const std::string& dir,
              const double solTime,
              const uint64_t tn,
              const MyMPI::MPIMgr& mpiMgr)->void{
    prtGrd->writeToTecplot(nGrd,dir,solTime,tn,mpiMgr);},
    MyString::makeLastCharDirSep(
        MyString::catDirs(dirPref,dict.get<std::string>("directory","")) ),
        -1,-1,1 ) // newOutput
  );

  // save a pointer to particle grid output callback for use after load balancing
  const GenericCallback* const particleGridOutputCallback = callbacks_.back().get();

  // add all defined data grids

  while(next)
    {

      if (array_of_data_grd_dicts.isArray())
        {
          dataGrdDict = array_of_data_grd_dicts.get<Settings::Dictionary>(i++);
          next = (i < array_of_data_grd_dicts.size());
        }
      else
        {
          dataGrdDict = array_of_data_grd_dicts;
          next = false;
        }

      // now add the data grid
      dataGrids_.emplace_back(prtGrd,dataGrdDict);
      dataGrd = &(dataGrids_.back());

      // if it is the simulation grid, also set the appropriate reference
      if (dataGrdDict.get<bool>("is simulation grid",false))
        {
          // assert no grid has been added as simulation grid before
          Grids::DataGrid* sg = simulationGrid_;
          if (sg)
            MYASSERT( false, std::string("grid \"") + dataGrd->name() +
                      std::string("\" designated as simulation grid,\n") +
                      std::string("however, grid \"") + sg->name() +
                      std::string("\" already added as simulation grid!"));

          // set the reference
          setSimGrid( dataGrd );
        }


      // add any defined outputs
      if (dataGrdDict.hasMember("output"))
        {

          outDict = dataGrdDict.get<Settings::Dictionary>("output");

          // add solution output
          callbacks_.push_back( std::move( dataGrd->makeOutputCallback(
              nodeGrid_.get(), particleGridOutputCallback,outDict, dirPref )) );

        }

    }

  // make sure a sim grid was specified
  MYASSERT( simulationGrid_, std::string("no grid designated as simulation grid!\n") +
            std::string("need member\n") +
            std::string("\"is simulation grid\" : true\n in exactly one data grid dictionary") );


  // if load balancing specified, set pointer to designated data grid
  if (dict.hasMember("load balancing"))
    {

      if (mpiMgr.nRanks() < 2)
        {
          std::cout << "ignoring load balancing directives in settings for run with single rank" << std::endl;
        }
      else
        {

          Settings::Dictionary lbDict = dict.get<Settings::Dictionary>("load balancing");

          std::string lbGridName = lbDict.get<std::string>("grid");
          for (auto&& g : dataGrids_)
            if (g.name() == lbGridName)
              {
                loadGrid_ = &g;
                break;
              }

          MYASSERT(loadGrid_,std::string("grid designated for load balancing \"")
          + lbGridName + std::string("\" not found in list of data grids"));

          double threshold  = lbDict.get<double>("threshold",1.);

          // add callback
          callbacks_.push_back( std::move( makeLoadBalancingCallback(threshold, lbDict) ) );
        }

    }


}
