/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * GridCollection.h
 *
 *  Created on: Apr 28, 2014
 *      Author: Stephan Kuechlin
 */

#ifndef GRIDCOLLECTION_H_
#define GRIDCOLLECTION_H_


#include <list>
#include <memory>

#include "Grid/RegularGrid.h"
#include "Grid/DataGrid.h"
#include "Grid/GridTopology/GridTopology.h"
#include "Grid/GridHelpers/GridTransform.h"

#include "Primitives/MyError.h"

namespace Settings
{
  class Dictionary;
}

namespace MyMPI
{
  class MPIMgr;
}

namespace MyChrono
{
  class TimerCollection;
}

class GenericCallback;

class SimulationBox;

namespace Grids
{

  class GridManager
  {
    private:
      // manages the grids


      typedef std::unique_ptr<GenericCallback> CallbackT;
      typedef std::list<CallbackT> BufferOfCallbacksT;


    public:

      const std::unique_ptr<Grids::Topology> topology_;

      std::unique_ptr<Grids::Grid>          nodeGrid_;
      std::unique_ptr<Grids::RegularGrid>   particleGrid_;
      std::list<Grids::DataGrid>            dataGrids_;

      Grids::DataGrid* simulationGrid_  = nullptr;  // pointer to simulation grid in dataGrids_
      Grids::DataGrid* loadGrid_        = nullptr;  // pointer to load grid in dataGrids_


    public:
      enum class LoadMetric : uint8_t
      {
        CPU,
        MEMORY
      };

    private:

      // Buffer of callbacks (e.g. outputs) filled during construction of individual grids
      // will be moved to core during simulation setup
      BufferOfCallbacksT callbacks_;

    public:
      // ACCESSORS

      Grids::Topology* topology() { return topology_.get(); }

      // pointer to SimulationGrid
      Grids::DataGrid* simulationGrid() { return simulationGrid_; }

      // pointer to data grid used for load balancing
      Grids::DataGrid* loadGrid() { return loadGrid_; }

      // pointer to ParticleGrid
      Grids::RegularGrid* particleGrid() { return particleGrid_.get(); }

      Grids::Grid* nodeGrid() { return nodeGrid_.get(); }

      Grids::DataGrid*
      dataGrid(const std::string& name){
        Grids::DataGrid* dg = nullptr;
        for (auto&& g : dataGrids_)
          if (g.name().compare(name) == 0) { dg = &g; break; }
        return dg;
      }


    public:

      void
      verify()
      const
      {
        MYASSERT(topology_ != nullptr, "topology may not be empty");
        MYASSERT(particleGrid_ != nullptr, "particle grid may not be empty!");
        MYASSERT(!(dataGrids_.empty()), "data grid collection may not be empty!");
        MYASSERT(simulationGrid_, "GridManager missing a simulation grid!");
      }

      CallbackT popCallback();

    public:

      double loadBalance(const MyMPI::MPIMgr& mpiMgr);

      void rebalanceLoad(const MyMPI::MPIMgr& mpiMgr, const int tag);

      double cellLoad(const uint64_t cell_idx, const LoadMetric metric) const;

    private:
      std::unique_ptr<GenericCallback>
      makeLoadBalancingCallback(
          const double threshold,
          const Settings::Dictionary& dict);

    public:

      uint64_t transform(const GridTransform& tr);

      uint64_t removeObsoleteGhostCells();

      void updateCellVolumesOnDataGridsFromParticleGrid();

    public:

      void assertParticleGridDataGridConsistency(const std::string& msg = "") const;

    public:

      void computeMaxNormalizedGradientLengthFromKernelEstimates(
          DataGrid *const dg,
          const double rho_ref_inv = 0.,    // if 0: use cell average
          const double theta_ref_inv = 0.,  // if 0: use cell average
          const double min_num_samples = 100000.
          );

      // compute gradient length in variable "variableName"
      // using finite difference
      void computeGradientLengthFromFiniteDifferences(
          DataGrid *const dg,
          const std::string& variableName,
          const double reference,
          const bool relative,
          const MyMPI::MPIMgr& mpiMgr);

    private:


      double gradientLength_FD(
          const uint64_t i,
          GridNeighborStack2& neighbors,
          const std::vector<double>& values,
          const double reference,
          const bool relative);


    private:

      // CTOR helpers

      void addParticleGrid(std::unique_ptr<Grids::RegularGrid> pg){
        particleGrid_ = std::move(pg); }

      // adds the node grid
      void addNodeGrid(std::unique_ptr<Grids::Grid> ng) {
        nodeGrid_ = std::move(ng); }

      // sets the simulation grid
      void setSimGrid(Grids::DataGrid* sg){
        simulationGrid_ = sg; }

    public:


      GridManager(
          const Settings::Dictionary& grdDict,
          const SimulationBox& simBox,
          const MyMPI::MPIMgr& mpiMgr,
          const std::string& dirPref );


      ~GridManager();

  };

} // namespace Grids


#endif /* GRIDCOLLECTION_H_ */
