/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * GridManagerColoring.cpp
 *
 *  Created on: Sep 29, 2016
 *      Author: kustepha
 */



#include "RegularGrid.h"


#include "Primitives/MyString.h"

using namespace Grids;

// adds color to all cells with one or more missing neighbors
void RegularGrid::addColorToBoundary(
    const uint8_t max_lvl_delta,
    const uint8_t color) {


  const uint64_t max_neighbors_per_face = topology()->nFacesPerFace(max_lvl_delta,0);

  addColorByFun(
      [this,max_lvl_delta,color,max_neighbors_per_face](const uint64_t iCell)->uint8_t{

    uint64_t* fn = (uint64_t*)alloca(max_neighbors_per_face*sizeof(uint64_t));

    const uint64_t key = cellKey( iCell );
    const uint8_t  lvl = cellLevel( iCell );
    const uint8_t  neighbor_lvl = lvl>max_lvl_delta ? lvl - max_lvl_delta : 0;

    const uint64_t n_background_faces_per_face = topology()->nFacesPerFace(lvl,0);

    for (uint8_t i = 0; i < 2*topology()->ndims(); ++i) // for all faces of cell
      {
        topology()->faceNeighborKeys(fn,key,lvl,neighbor_lvl,i);
        uint64_t n_background_faces_on_face = 0;
        for (uint64_t j = 0; j < max_neighbors_per_face; ++j)
          {
            const uint64_t ni = locateKey(fn[j]); // get neighbor index
            if (__builtin_expect(ni != uint64_t(-1),true))
              n_background_faces_on_face += topology()->nFacesPerFace(cellLevel(ni),0);
          }
        if (__builtin_expect(n_background_faces_on_face < n_background_faces_per_face, false ) )
          return color;
      }

    return 0;
  });


}

// colors all cells with absolute level difference to neighbor greater max_lvl_delta
void RegularGrid::addColorByLevelGradient(
    const uint8_t max_lvl_delta,
    const uint8_t color)
{
  addColorByNeighbors(
      [this,max_lvl_delta,color](const uint64_t iCell, GridNeighborStack2& neighbors)
      ->uint8_t{

    const uint8_t lvl = cellLevel( iCell );
    const uint8_t c = cellColor( iCell );
    uint8_t face_refined[6] = {0};

    while( neighbors() )
      {
        const auto n = neighbors.pop();

        if (__builtin_expect( lvl > (n.level + max_lvl_delta), false ))
          return c | color;
        if (__builtin_expect( lvl > n.level , false ))
          face_refined[n.face] = 1;
      }

    uint8_t n_faces_refined = 0;
    for (uint8_t i = 0; i < 6; ++i)
      n_faces_refined += face_refined[i];

    if (n_faces_refined > topology()->ndims()) // smooth level boundary
      return c | color;

    return c;
  }
  ,max_lvl_delta+1);
}
