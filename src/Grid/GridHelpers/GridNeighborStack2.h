/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * GridNeighborStack2.h
 *
 *  Created on: Jan 24, 2018
 *      Author: kustepha
 */

#ifndef SRC_GRID_GRIDHELPERS_GRIDNEIGHBORSTACK2_H_
#define SRC_GRID_GRIDHELPERS_GRIDNEIGHBORSTACK2_H_

#include <vector>
#include <stack>

namespace Grids
{

  class RegularGrid;

  class GridNeighborStack2
  {
  public:

    enum DIRECTION : uint8_t { HIGH = 1, LOW = 2 };

    struct Neighbor {
      const uint64_t index;
      const uint64_t key;
      const uint8_t level;
      const uint8_t min_level;
      const uint8_t max_level;
      const uint8_t color;
      const uint8_t face;
      const uint8_t pad[3] = {0};
    };

  private:

    //                 neighbor_index, face,
//    typedef std::pair< uint64_t,       uint8_t > value_type;
    typedef Neighbor value_type;
    typedef std::stack<value_type> StackT;
    typedef std::vector<uint64_t> BufferT;


    StackT                      neighbors_;
    const RegularGrid *const    g_;
    const uint8_t               ndims_;
    const uint8_t               level_delta_; // search for neighbors with level this much smaller than cell level

    const uint64_t max_n_per_f_;

    BufferT fn_; // buffer

  private:

    // push face neighbors onto stack
    void push_neighbors( const uint64_t key, const uint8_t lvl, const uint8_t neighbor_lvl, const uint8_t face );

  public:

    uint64_t size() const { return neighbors_.size(); }

    // push neighbors onto stack
    void push_neighbors( const uint64_t iCell, const uint8_t dir );


    value_type pop() {  value_type ret = neighbors_.top();  neighbors_.pop(); return ret; }

    bool empty() const { return neighbors_.empty(); }

    bool operator()() const { return !empty(); }

    void clear() { while (!empty()) neighbors_.pop(); }


  public:
    GridNeighborStack2 (const RegularGrid* const g, const uint8_t level_delta);

  };

} /* namespace Grids */

#endif /* SRC_GRID_GRIDHELPERS_GRIDNEIGHBORSTACK2_H_ */
