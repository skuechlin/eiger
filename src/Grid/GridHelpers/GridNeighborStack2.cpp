/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * GridNeighborStack2.cpp
 *
 *  Created on: Jan 24, 2018
 *      Author: kustepha
 */
#include <cstdint>
#include <set>

#include "Grid/GridHelpers/GridNeighborStack2.h"

#include "Grid/RegularGrid.h"

#include "Primitives/MyError.h"

namespace Grids
{

  // push face neighbors onto stack
  void GridNeighborStack2::push_neighbors(
      const uint64_t key, const uint8_t lvl, const uint8_t neighbor_lvl, const uint8_t face )
  {
    constexpr uint64_t inv = -1;
    std::set<uint64_t> index_set;

    fn_.assign(max_n_per_f_,inv);

    g_->topology()->faceNeighborKeys(fn_.data(),key,lvl,neighbor_lvl,face);

    // convert neighbor keys to indices and push to stack if not there yet
    for (uint64_t i = 0; i < max_n_per_f_; ++i)
      {
        const uint64_t n_ind = g_->locateKey(fn_[i]);
        if (__builtin_expect(n_ind != inv,true))
          {
            auto res = index_set.insert(n_ind);
            if (res.second) // index was not yet in set
              {
                const uint64_t n_key = g_->cellKey(n_ind);
                const auto& n_cell = g_->cell(n_ind);

                neighbors_.push( value_type{
                  n_ind,n_key,
                  n_cell.level, n_cell.min_level, n_cell.max_level, n_cell.color,
                  face} );
              }
          }
      }
  }

  // push neighbors onto stack
  void GridNeighborStack2::push_neighbors( const uint64_t iCell, const uint8_t dir )
  {

    const uint64_t key = g_->cellKey(   iCell );
    const uint8_t  lvl = g_->cellLevel( iCell );
    const uint8_t  neighbor_lvl = lvl>level_delta_ ? lvl - level_delta_ : 0;

    if (dir & LOW)
      push_neighbors( key,lvl,neighbor_lvl,0);
    if (dir & HIGH)
      push_neighbors( key,lvl,neighbor_lvl,1);

    if (ndims_ == 1)
      return;

    if (dir & LOW)
      push_neighbors( key,lvl,neighbor_lvl,2);
    if (dir & HIGH)
      push_neighbors( key,lvl,neighbor_lvl,3);

    if (ndims_ == 2)
      return;

    // ndims_ == 3

    if (dir & LOW)
      push_neighbors( key,lvl,neighbor_lvl,4);
    if (dir & HIGH)
      push_neighbors( key,lvl,neighbor_lvl,5);

  }

  GridNeighborStack2::GridNeighborStack2 (const RegularGrid* const g, const uint8_t level_delta)
  : g_( g )
  , ndims_( g_->topology()->ndims() )
  , level_delta_( level_delta )
  , max_n_per_f_( g_->topology()->nFacesPerFace(level_delta,0) )
  {  }

} /* namespace Grids */
