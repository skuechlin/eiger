/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * GridTransform.h
 *
 *  Created on: Sep 29, 2016
 *      Author: kustepha
 */

#ifndef SRC_GRID_GRIDHELPERS_GRIDTRANSFORM_H_
#define SRC_GRID_GRIDHELPERS_GRIDTRANSFORM_H_

namespace Grids
{
  class RegularGrid;

  class GridTransform
  {
      friend class GridManager;

    public:

      enum ACTION : uint8_t {
        REFINE  = 0,
            COARSEN = 1,
            REMOVE  = 2
      };

    private:

      const RegularGrid *const g_;

      std::vector<uint64_t> remove_;
      std::vector<uint64_t> merge_;
      std::vector<uint64_t> split_;


    public:

      uint64_t size() const { return remove_.size() + merge_.size() + split_.size(); }

      void clear();

      uint64_t append( ACTION action , const uint8_t min_level = 0, const uint8_t colors = -1);

      GridTransform( const RegularGrid *const g )
      : g_(g)
      {}

  };

}


#endif /* SRC_GRID_GRIDHELPERS_GRIDTRANSFORM_H_ */
