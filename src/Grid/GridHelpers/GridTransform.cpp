/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * GridTransform.cpp
 *
 *  Created on: Jun 8, 2016
 *      Author: kustepha
 */


#include "Grid/Grid.h"
#include "Grid/DataGrid.h"

#include "Primitives/MyTransform.h"

#include "Grid/GridHelpers/GridTransform.h"


namespace Grids
{


  void GridTransform::clear()
  {
    remove_ = {};
    merge_ = {};
    split_ = {};
  }

  uint64_t GridTransform::append( ACTION action, const uint8_t min_level, const uint8_t colors )
  {

    uint64_t count = 0;
    const uint64_t ncells = g_->nCells();

    switch (action) {

      case REFINE:

        for (uint64_t i = 0; i < ncells; ++i)
          if (
              ( g_->cellColor(i) & colors ) &&
              ( g_->cellLevel(i) > std::max(min_level,g_->cell(i).min_level) )
              )
            {
              split_.push_back(i);
              ++count;
            }
        break;

      case COARSEN:

        MYASSERT(false,"COARSEN action for Transform::append(...) not implemented yet");
        break;

      case REMOVE:

        for (uint64_t i = 0; i < ncells; ++i)
          if ( g_->cellColor(i) & colors )
            {
              remove_.push_back(i);
              ++count;
            }
        break;

      default:
        MYASSERT(false,"invalid action for Transform::append(...)");
        break;
    }

    return count;

  }


} // namespace Grids
