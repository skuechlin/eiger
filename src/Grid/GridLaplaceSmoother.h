/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * GridLaplaceSmoother.h
 *
 *  Created on: Feb 8, 2019
 *      Author: kustepha
 */

#ifndef SRC_GRID_GRIDLAPLACESMOOTHER_H_
#define SRC_GRID_GRIDLAPLACESMOOTHER_H_


#include <Grid/RegularGrid.h>
#include <Grid/GridGhostExchange.h>

namespace Grids {

  template<
  typename GetVFT,
  typename SetVFT,
  typename GetTmpVFT,
  typename SetTmpVFT>
  void sum_neighbors(
      Grid* const nodeGrid,
      const RegularGrid* const g,
      GetVFT&& getV,
      SetVFT&& setV,
      GetTmpVFT&& getTmpV,
      SetTmpVFT&& setTmpV,
      const MyMPI::MPIMgr* const mpiMgr,
      int tag = 0
  ) {

    using T = typename std::result_of<GetVFT(uint64_t)>::type;
    using T1 = typename std::result_of<GetTmpVFT(uint64_t)>::type;
    static_assert(std::is_same<T,T1>::value);
    static_assert(std::is_same<T,double>::value);

    exchangeGhostData(
        nodeGrid, g,
        [&getV,g](const uint64_t key)
        -> double{  return getV(g->locateKey(key)); },
        [&setV,g](const uint64_t iGhost, const double v)
        -> void{ setV(g->ghostIdx2CellIdx(iGhost),v); },
        *mpiMgr,
        COMPILE_TIME_CRC32_STR( FILE_POS_STR ) + tag);

    // first pass: compute smoothed value and save in tmp
    g->traverseNeighbors(
        GridNeighborStack2::DIRECTION::HIGH | GridNeighborStack2::DIRECTION::LOW ,
        [&getV,&setTmpV](const uint64_t iCell, GridNeighborStack2& neighbors)
        -> void {

      double v = getV(iCell);

      while( neighbors() )
        {
          const auto n = neighbors.pop();
          v += getV(n.index);
        }

      setTmpV(iCell,v);

    },
    g->first_,g->last_);

    // second pass: copy from tmp
#pragma omp parallel for schedule(static)
    for (uint64_t ic = g->first_; ic < g->last_; ++ic)
      setV(ic,getTmpV(ic));

  }

  template<
  typename GetVFT,
  typename SetVFT,
  typename GetTmpVFT,
  typename SetTmpVFT,
  typename GetWFT,
  typename SetWFT>
  void smooth(
      Grid* const nodeGrid,
      const RegularGrid* const g,
      GetVFT&& getV,
      SetVFT&& setV,
      GetTmpVFT&& getTmpV,
      SetTmpVFT&& setTmpV,
      GetWFT&& getW,
      SetWFT&& setW,
      const MyMPI::MPIMgr* const mpiMgr,
      int tag = 0
  ) {

    // compute weighted local value
#pragma omp parallel for schedule(static)
    for (uint64_t ic = g->first_; ic < g->last_; ++ic)
      setV(ic,getV(ic)*getW(ic));

    // compute (weighted) neighbor sum of values
    sum_neighbors( nodeGrid, g,
                   getV,
                   setV,
                   getTmpV, setTmpV,
                   mpiMgr, COMPILE_TIME_CRC32_STR( FILE_POS_STR ) + tag);

    // compute sum of neighbor weights
    sum_neighbors( nodeGrid, g,
                   getW, setW,
                   getTmpV, setTmpV,
                   mpiMgr, COMPILE_TIME_CRC32_STR( FILE_POS_STR ) + tag);

    // compute value
#pragma omp parallel for schedule(static)
    for (uint64_t ic = g->first_; ic < g->last_; ++ic)
      setV(ic,getV(ic)/getW(ic));


  }



}


#endif /* SRC_GRID_GRIDLAPLACESMOOTHER_H_ */
