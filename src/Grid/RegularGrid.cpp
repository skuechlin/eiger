/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * RegularGrid.cpp
 *
 *  Created on: May 23, 2018
 *      Author: kustepha
 */



/*
 * ParticleGrid.cpp
 *
 *  Created on: Nov 27, 2014
 *      Author: kustepha
 */

#include <Grid/CellData.h>
#include <iomanip>
#include <omp.h>

#include "RegularGrid.h"


#include "Parallel/MyMPI.h"

#include "LoadBalancing/ExactBisection.h"

#include "Primitives/MyCount.h"
#include "Primitives/MyError.h"
#include "Primitives/MyArithm.h"
#include "Primitives/MyString.h"
#include "Primitives/MyTransform.h"

#include "Settings/Dictionary.h"

#include "Constants/Constants.h"


namespace Grids
{


  void
  RegularGrid::transform(
      const std::vector<uint64_t>& remove,
      const std::vector<uint64_t>& merge,
      const std::vector<uint64_t>& split)
  {

    if ( remove.size() == 0 &&
        merge.size() == 0 &&
        split.size() == 0 )
      return;


    uint64_t block_size = topology()->nCellsPerCell(0,1);

    //    // DEBUG
    //            std::cout << "first " << first_ << std::endl;
    //            std::cout << "last " << last_ << std::endl;
    //            std::cout << "max_key " << key_[last_-1] << std::endl;
    //                std::cout << "key_ size " << key_.size() << std::endl;
    //                std::cout << "remove size " << remove.size() << std::endl;
    //                std::cout << "key_ size - remove size: " << key_.size() - remove.size() << std::endl;
    //                std::cout << "merge size " << merge.size() << std::endl;
    //                std::cout << "split size " << split.size() << std::endl;
    //            std::cout << "remove: " << std::endl;
    //
    //            for (auto r : remove)
    //              std::cout << r << std::endl;
    //
    //        std::cout << "\nvolume:" << std::endl;
    //        uint64_t i = 0;
    //        for (auto v : volume_)
    //          std::cout << i++ << " "<< v << std::endl;

    // save key bounds of regular (non-ghost) cells
    uint64_t key_of_first  = key_[first_];
    uint64_t key_of_last   = key_[last_];

    MyTransform::remove_merge_split(
        key_,
        remove,
        merge,
        split,
        block_size,
        [](uint64_t&, const uint64_t)->void{ }, // merge
        [this](const uint64_t key, const uint64_t i, const uint64_t j)->uint64_t{ // split
          const uint64_t key_size = topology()->nCellsPerCell(0,cellLevel(i)-1);
          return key + j*key_size;
        });

    //    std::cout << "new key_ size: " << key_.size() << std::endl;

    const double dblock_size = static_cast<double>(block_size);

    MyTransform::remove_merge_split(
        cells_,
        remove,
        merge,
        split,
        block_size,
        [](Cell& a, const Cell& b)->void{ // merger
      a.volume += b.volume; // add volumes
      a.wf = 1.; // reset
      a.level = b.level + 1; // we only merge cells on same level
      a.min_level = std::max(a.min_level, b.min_level);
      a.max_level = std::min(a.max_level, b.max_level);
      a.color = 0; },
      [dblock_size](const Cell& parent, const uint64_t, const uint64_t)->Cell{ // splitter
        const double v = parent.volume / dblock_size;
        const uint8_t l = parent.level - 1;
        return {
          v,
          1., // reset weight factor
          l,
          parent.min_level,
          parent.max_level,
          0 }; }
    );

    // re-establish range of ghost cells
    first_ = std::distance(key_.begin(), std::lower_bound(key_.begin(),key_.end(),key_of_first) );
    last_  = std::distance(key_.begin(), std::lower_bound(key_.begin(),key_.end(),key_of_last)   );

    //    std::cout << "new level_ size: " << level_.size() << std::endl;

    // restore guard cell at end
    key_[key_.size()-2] = key_[key_.size()-3] + topology()->nCellsPerCell(0,cellLevel(key_.size()-3));


    // resize remaining arrays
    count_.resize(key_.size()-1);
    begin_.resize(key_.size()-1);

    // reinitialize other data
    resetCount();
    resetBegin();

  }


  void RegularGrid::print() const
  {
    std::cout << "\n\n" << "RegularGrid \"" << name_
        << "\": first: " << first_
        << " last: " << last_
        << " nCells: " << nCells() << std::endl;
    std::cout << "number | key | count | begin | level | volume | tag" << std::endl;

    for (uint64_t i = 0; i < nCells(); ++i)
      std::cout << std::setw(5) << i << ": "
      << std::setw(7) << cellKey(i) << " "
      << count_[i] << " "
      << begin_[i] << " "
      << std::to_string(cellLevel(i)) << " "
      << cellVolume(i) << " "
      << std::to_string(cellColor(i)) << "\n";
    for (uint64_t i = nCells(); i < nCells()+2; ++i)
      std::cout << std::setw(5) << i << ": "
      << std::setw(7) << cellKey(i) << "\n";

    std::cout << "\n" << std::endl;

  }



  void
  RegularGrid::update_min_cell_width()
  {

    double mcw = std::numeric_limits<double>::infinity();

    // check all cells

#pragma omp parallel for schedule(static) reduction(min:mcw)
    for (uint64_t i = first_; i < last_; ++i)
      {
        mcw = std::min(mcw, cellMinWidth(i));
      }

    min_cell_width_ = mcw;

  }

  void
  RegularGrid::initializeCellVolumes()
  {

#pragma omp for schedule(static)
    for (uint64_t i = first_; i < last_; ++i)
      setCellVolume(i, topologicalCellVolume(i) );

  }

  void
  RegularGrid::update_av_cell_volume()
  {

    std::vector<double> shared_av_vol;

#pragma omp parallel
    {

      uint64_t n_threads = omp_get_num_threads();

      // allocate shared array
#pragma omp single
      shared_av_vol.resize(n_threads,0);


      // private access to shared array
      double& av_vol = shared_av_vol[omp_get_thread_num()];

      // private sample counter
      double sample_num = 0.;

      // compute private averages
#pragma omp for schedule(static)
      for (uint64_t i = first_; i < last_; ++i)
        {
          if (cellVolume(i) > Constants::minCellVolume)
            {
              av_vol -= cellVolume(i);
              av_vol *= 1.0 - 1.0/(++sample_num);
              av_vol += cellVolume(i);
            }
        }

    } // end parallel

    // reduce
    av_cell_volume_ = 0;
    double sample_num = 0.;
    for (uint64_t i = 0; i < shared_av_vol.size(); ++i)
      {
        av_cell_volume_ -= shared_av_vol[i];
        av_cell_volume_ *= 1.0 - 1.0/(++sample_num);
        av_cell_volume_ += shared_av_vol[i];
      }

  } // update_av_cell_volume




  // generate a grid with n cells
  // with first key equal to start
  RegularGrid::RegularGrid(
      const Topology* tp,
      const uint64_t start,
      const uint64_t n,
      const uint8_t default_level,
      const Settings::Dictionary dict)
  : Grid(tp,dict.get<std::string>("name"))
  {

    uint8_t level = dict.get<uint8_t>("level",default_level);

    topology_->generateRegularKeys(
        key_,
        start,
        n,
        level );

    topology_->generateGhostCellKeys(first_,last_,key_,level);

    // key_ now includes keys of last_ - first_ regular cells,
    // key_.size()-2 - (last_-first_) keys of ghost cells,
    // the key of the guard cell at key_[key_.size()-2]
    // the guard value at key_.back()

    //count, begin, are one element shorter than key_, last element is the guard cell
    count_.assign(key_.size()-1,0);

    begin_.assign(key_.size()-1,0);

    // cells vector is two elements shorter than key_, they do not include the guard cell

    cells_.assign(key_.size()-2,{0.,1.,level}); // also assign level to ghost cells

    initializeCellVolumes();

    updateCellVolumeStatistics();

    ref_cell_volume_ = topologicalCellVolume(first_);

  }




  RegularGrid::~RegularGrid() = default;


} /* namespace Grids */



