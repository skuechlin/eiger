/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * RegularGrid.h
 *
 *  Created on: May 23, 2018
 *      Author: kustepha
 */

#ifndef SRC_GRID_REGULARGRID_H_
#define SRC_GRID_REGULARGRID_H_

#include "Grid.h"

#include "GridHelpers/GridNeighborStack2.h"

namespace Grids {


  class RegularGrid: public Grid
  {


    // a RegularGrid
    // - is a grid
    // - stores the coarsening level of each cell
    // - stores the volume of each grid cell
    // - stores a tag for each cell

  public:

    enum COLOR : uint8_t { PERMANENT = uint8_t(1)<<7 };

    struct Cell {
      double volume; // the volume of each cell
      double wf;     // the weight factor of each cell
      uint8_t level; // the coarsening level of each cell
      uint8_t min_level = 0;
      uint8_t max_level = uint8_t(-1);
      uint8_t color     = 0; // cell color (e.g. intersected by geometry)?
      uint8_t flag      = 0;
      uint8_t intersected = 0;
      uint8_t pad[2]   = {0};
      double tmp = 0.;
    };

    struct CellMsgT {
      const uint64_t key;
      const Cell cell;
    };

  public:

    std::vector<Cell> cells_;

  public:
    static constexpr uint64_t cellMemoryLoad()
    {
      return sizeof(Cell)
          + sizeof(uint64_t)  // key
          + sizeof(uint64_t)  // begin
          + sizeof(uint64_t); // count
    }


  private:
    // statistical info

    // number of (unique) vertices
    mutable uint64_t nVerts_ = -1; // assigned to in const writeGrid

    // minimum cell width on grid
    double min_cell_width_ = 0.;

    // average cell volume on grid
    double av_cell_volume_ = 0.;

    // reference cell volume used to compute reference quantities,
    // such as number of computational particles per molecule
    double ref_cell_volume_ = 0.;


  public:

    void print() const override;

  public:

    void
    initializeCellVolumes();

  public:

    const Cell& cell(const uint64_t i) const { return cells_[i]; }
    Cell& cell(const uint64_t i) { return cells_[i]; }

    uint8_t cellLevel(const uint64_t i) const { return cells_[i].level; }

    double cellWeightFactor(const uint64_t i) const { return cells_[i].wf; }
    void setCellWeightFactor(const uint64_t i, const double wf)  { cells_[i].wf = wf; }

    double cellVolume(const uint64_t i) const { return cells_[i].volume; }
    void setCellVolume(const uint64_t i, const double vol) {
      cells_[i].volume = vol; }

    uint8_t cellColor(const uint64_t i) const {return cells_[i].color; }
    void setCellColor(const uint64_t i, const uint8_t c) {
      cells_[i].color = c; }
    void addCellColor(const uint64_t i, const uint8_t c) {
      cells_[i].color |= c; }
    void removeCellColor(const uint64_t i, const uint8_t c) {
      cells_[i].color &= ~c | COLOR::PERMANENT; }


  public:

    v4df cellWidth(const uint64_t i) const { return cellBox(i).width(); }

    v4df cellHalfWidth(const uint64_t i) const { return 0.5*cellWidth(i); }

    // return minimum extent of cell i
    // only consider non-singleton dimensions
    double cellMinWidth(const uint64_t i) const {
      const v4df w = cellWidth(i);
      double m = w[0];
      for (uint8_t j = 1; j < topology_->ndims(); ++j)
        m = fmin(m,double(w[j]));
      return m; }

    // geometric volume of cell i as defined by topology
    double topologicalCellVolume(const uint64_t i) const {
      return topology_->cellVol(cellKey(i),cellLevel(i)); }

  private:

    void update_min_cell_width();

    void update_av_cell_volume();

  public:

    // call Functor fun(const uint64_t iCell, GridNeighborStack& neighbors)
    // neighbors containing neighbors in direction dir (HIGH, LOW, or HIGH | LOW ), including ghost
    // will only traverse ALL neighbors of cell iCell if the largest absolute level difference is level_delta
    // traverses all cells in index range [begin,end) (defaults to ALL cells, INCLUDING ghost)
    template< typename Functor >
    void traverseNeighbors(
        const uint8_t dir,
        Functor&& fun,
        uint64_t begin = 0,
        uint64_t end = uint64_t(-1),
        uint8_t lvl_delta = 1) const {

      if (end > nCells())
        end = nCells();

#pragma omp parallel
      {

          GridNeighborStack2 stack( this, lvl_delta );

#pragma omp for schedule(static)
          for ( uint64_t ic = begin; ic < end; ++ic)
            {

              stack.push_neighbors( ic, dir );

              // process neighbor stack.
              fun(ic, stack);

              // remove any remaining elements
              stack.clear();

            } // for all cells

      } // omp parallel

    }

  public:

    uint64_t propagateCellLevelLimits(const uint8_t max_lvl_delta) {

      // write to new_cells to avoid race condition
      auto new_cells = cells_;

      traverseNeighbors(
          GridNeighborStack2::DIRECTION::HIGH | GridNeighborStack2::DIRECTION::LOW ,
          [this,&new_cells,max_lvl_delta](const uint64_t iCell, GridNeighborStack2& neighbors)
          -> void {
        if (cellColor(iCell) & COLOR::PERMANENT)
          {
            new_cells[iCell].min_level = cellLevel(iCell);
            new_cells[iCell].max_level = cellLevel(iCell);
          }
        else
          {
            while(neighbors())
              {
                const auto n = neighbors.pop();
                const uint8_t nminl = n.min_level > max_lvl_delta ? n.min_level - max_lvl_delta : 0;
                const uint8_t nmaxl = n.max_level > (uint8_t(-1) - max_lvl_delta) ? uint8_t(-1) :  n.max_level + max_lvl_delta;

                new_cells[iCell].min_level = std::max( new_cells[iCell].min_level, nminl );
                new_cells[iCell].max_level = std::min( new_cells[iCell].max_level, nmaxl );

              }
          }
      },
      first_,last_,max_lvl_delta);

      uint64_t n_changed = 0;
#pragma omp parallel for reduction(+:n_changed)
      for (uint64_t i = first_; i < last_; ++i)
        if (new_cells[i].min_level != cells_[i].min_level ||
            new_cells[i].max_level != cells_[i].max_level )
          ++n_changed;

      cells_.swap(new_cells);

      return n_changed;
    }


    // coloring functions

    // remove given colors on all cells, regular and ghost, except permanent color
    void removeCellColors(const uint8_t cs)
    {
#pragma omp parallel for schedule(static)
      for ( uint64_t ic = 0; ic < nCells(); ++ic)
        removeCellColor(ic,cs);
    }

    // remove given colors on regular cells, except permanent color
    void removeRegularCellColors(const uint8_t cs)
    {
#pragma omp parallel for schedule(static)
      for ( uint64_t ic = first_; ic < last_; ++ic)
        removeCellColor(ic,cs);
    }

    // remove given colors on ghost cells, except permanent color
    void removeGhostCellColors(const uint8_t cs)
    {
#pragma omp parallel for schedule(static)
      for ( uint64_t ic = 0; ic < first_; ++ic)
        removeCellColor(ic,cs);
#pragma omp parallel for schedule(static)
      for ( uint64_t ic = last_; ic < nCells(); ++ic)
        removeCellColor(ic,cs);
    }

    // add colors by function
    // result: color_[i] |= cFun(i)
    template<typename ColorFunctor>
    void addColorByFun(ColorFunctor&& cFun )
    {
#pragma omp parallel for schedule(static)
      for ( uint64_t ic = first_; ic < last_; ++ic)
        addCellColor(ic,cFun(ic));
    }

    // color cells by conditions on neighbors
    template<typename ColorFunctor>
    void addColorByNeighbors(ColorFunctor&& cFun,  const uint8_t max_lvl_delta = 1,
                          uint64_t begin = -1, uint64_t end = -1)
    {
      if (begin == uint64_t(-1))
        begin = first_;
      if (end == uint64_t(-1))
        end = last_;

      traverseNeighbors(
          GridNeighborStack2::DIRECTION::HIGH | GridNeighborStack2::DIRECTION::LOW ,
          [this,cFun](const uint64_t iCell, GridNeighborStack2& neighbors)
          -> void { addCellColor( iCell, cFun(iCell,neighbors) ); },
          begin,end,max_lvl_delta);
    }

    // result: color_[i] |= isBlank(i) ? c : 0
    void addColorToBlank(const uint8_t c)  {
      addColorByFun([this,c](const uint64_t i)->uint8_t{return isBlank(i) ? c : 0;});
    }

    // adds color to all cells with absolute level difference to neighbor greater max_lvl_delta
    void addColorByLevelGradient(
        const uint8_t max_lvl_delta,
        const uint8_t color);

    // adds color to all cells with one or more missing neighbors
    void addColorToBoundary(const uint8_t max_lvl_delta, const uint8_t color);

  public:

    void transform(
        const std::vector<uint64_t>& remove,
        const std::vector<uint64_t>& merge,
        const std::vector<uint64_t>& split);


  public:

    void
    updateCellVolumeStatistics()
    {
      update_av_cell_volume();
      update_min_cell_width();
    }


  private:

    // build a list of (unique) vertices of non-ghost cells that this proc references
    void build_unique_verts(std::vector<uint64_t>& vrts) const;

    // build a list of (unique) vertices of ghost cells that this proc references
    void build_unique_ghost_verts(std::vector<uint64_t>& vrts) const;


  public:

    // number of vertices
    uint64_t nVerts() const { return nVerts_; }

    // average volume of non-empty cells
    double averageCellVolume() const { return av_cell_volume_; }

    // minimum cell width on grid
    double minCellWidth() const { return min_cell_width_; }

    // reference cell volume used to compute reference quantities,
    // such as number of computational particles per molecule
    double referenceCellVolume() const { return ref_cell_volume_; }

  public:

    uint64_t
    writeGrid(
        Grid* const nodeGrid,
        const std::string& fileName,
        const double solTime,
        const MyMPI::MPIMgr& mpiMgr )
    const;

    void
    writeGhostGrid(
        Grid* const nodeGrid,
        const std::string& fileName,
        const double solTime,
        const MyMPI::MPIMgr& mpiMgr )
    const;

    void
    writeToTecplot(
        Grid* const nodeGrid,
        const std::string& dir,
        const double solTime,
        const uint64_t tn,
        const MyMPI::MPIMgr& mpiMgr )
    const;


  public:

    // returns true if volume of cell i is below static threshold minCellvolume
    bool isBlank(const uint64_t i) const {
      return cellVolume(i) < Constants::minCellVolume; }

    // returns index of cell containing key, or uint64_t(-1) if not in any cell on grid
    // takes into account size of cells
    uint64_t locateKey(const uint64_t key) const override
    {

      if (__builtin_expect( key >= *(key_.end()-2) || key < *(key_.begin()), false))
        return -1;

      uint64_t i = std::distance( key_.begin(),
                                  std::lower_bound(key_.begin(),key_.end(),key) );

      if (cellKey(i) == key)
        return i;

      // else:
      // check if key is within a cell
      // note: i == 0 not possible here

      --i;
      uint64_t ub = cellKey(i) + topology_->nCellsPerCell(0,cellLevel(i));

      if ( __builtin_expect(key < ub,true) )
        return i;
      else
        return -1;

    }



    // bounding box of entire grid
    box4_t boundingBox() const { return topology_->boundingBox(); }

    // bounding box of cell i
    box4_t cellBox(const uint64_t i) const { return topology_->cellBox(cellKey(i),cellLevel(i)); }

    // a random position in cell i
    v4df sample(const uint64_t i, MyRandom::Rng& rndGen) const {
      return topology_->sample(cellKey(i),cellLevel(i), rndGen); }

    // position of cell i
    v4df cellCenter(const uint64_t i) const {
      return topology_->cellCenter(cellKey(i),cellLevel(i)); }

    // return key of vrt_kji'th (from most significant bit: k,j,i) vertex of cell i
    uint64_t
    vrtKey(const uint64_t i, const uint8_t vrt_kji) const {
      return topology_->vrtKey(cellKey(i), cellLevel(i), vrt_kji); }

    // returns the index of the first (lowest coordinates) neighbor of the cell i
    // in direction "dir_skji" (from most significant bit: sign,k,j,i)
    uint64_t
    neighborIndex(const uint64_t i, const uint8_t dir_skji) const {
      return locateKey( topology_->neighborKey(cellKey(i), cellLevel(i), 0, dir_skji) ); }

    // position of center of sub cell si within cell i,
    // based on division into ns sub-cells per dimension
    v4df subCellCenter(
        const uint64_t i,
        const uint64_t si,
        const uint64_t ns_per_dim) const {
      return topology_->subCellCenter(cellKey(i),cellLevel(i),si,ns_per_dim); }


  public:

    // generate a grid with n cells
    // with first key equal to start
    RegularGrid(
        const Topology* tp,
        const uint64_t start,
        const uint64_t n,
        const uint8_t default_level,
        const Settings::Dictionary dict);

    virtual ~RegularGrid();

  };

} // namespace Grids


#endif /* SRC_GRID_REGULARGRID_H_ */
