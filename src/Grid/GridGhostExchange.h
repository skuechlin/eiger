/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * GridGhostExchange.h
 *
 *  Created on: Jun 7, 2016
 *      Author: kustepha
 */

#ifndef SRC_GRID_GRIDGHOSTEXCHANGE_H_
#define SRC_GRID_GRIDGHOSTEXCHANGE_H_

#include <type_traits> // result_of

#include "RegularGrid.h"

#include "Primitives/MyCount.h"
#include "Primitives/MyError.h"
#include "Parallel/MyMPI.h"
#include "Parallel/MyMPI_DSDEComm2.h"
#include "Parallel/MyMPI_NeighborComm.h"


namespace Grids
{


  template<typename SDataFunctor, typename RDataFunctor>
  void exchangeGhostData(
      Grid* const nodeGrid,
      const RegularGrid* const grid,
      SDataFunctor&& sDataFun,
      RDataFunctor&& rDataFun,
      const MyMPI::MPIMgr& mpiMgr,
      const int tag)
  {

    const uint64_t first = grid->first_;
    const uint64_t last = grid->last_;

    // the type of data to exchange
    using T = typename std::result_of<SDataFunctor(uint64_t)>::type;

    if ( mpiMgr.nRanks() < 2 )
      return;

    // ----------------------------------------------------//
    // assemble ghost keys

    std::vector<uint64_t> ghost_keys;
    ghost_keys.reserve( grid->nGhost() );

    for (uint64_t i = 0; i < first; ++i)
      ghost_keys.emplace_back(grid->key_[i]);

    uint64_t ghost_end = grid->nCells();
    for (uint64_t i = last; i < ghost_end; ++i)
      ghost_keys.emplace_back(grid->key_[i]);


    // ------------------------------------------------------------------------- //
    // determine sources of ghost cells by counting on node grid

    nodeGrid->resetCount();
    nodeGrid->resetBegin();

#pragma omp parallel
    {
      MyCount::parallelCount(
          nodeGrid->count_.data(),
          nodeGrid->begin_.data(),
          nodeGrid->key_.data(),
          nodeGrid->count_.size(),
          ghost_keys.data(),
          ghost_keys.size() );
    }

    MYASSERT(nodeGrid->count_.back() == 0, "found ghost cell(s) outside domain");

    // ------------------------------------------------------------------------- //
    // send ghost cell keys to sources, receive data requests

    // note: the node grid count and begin vectors have size one greater than number of nodes
//    std::vector<int32_t> scount(nodeGrid()->count_.begin(),nodeGrid()->count_.end());
//    std::vector<int32_t> sbegin(nodeGrid()->begin_.begin(),nodeGrid()->begin_.end());
//
//    std::vector<int32_t> rcount(nodeGrid()->last_ - nodeGrid()->first_);
//    std::vector<int32_t> rbegin(nodeGrid()->last_ - nodeGrid()->first_);

    //MyMPI::NeighborCommSpec
    MyMPI::DSDECommSpec2
    commspec(
        nodeGrid->count_.data(),
        mpiMgr.comm(),
        COMPILE_TIME_CRC32_STR( FILE_POS_STR ) + tag);

    std::vector<uint64_t> requests( commspec.n_to_receive() );

    commspec.comm(ghost_keys.data(),requests.data(),sizeof(uint64_t),MPI_UINT64_T,
                  COMPILE_TIME_CRC32_STR( FILE_POS_STR ) + tag);

//    MyMPI::alltoallv(
//        ghost.data(),
//        scount.data(),
//        sbegin.data(),
//        rcount.data(),
//        rbegin.data(),
//        [&requests](const uint64_t sz)->void*
//        {
//      requests.resize( sz );
//      return requests.data();
//        },
//        MPI_UINT64_T,
//        mpiMgr.comm() );


    // ------------------------------------------------------------------------- //
    // assemble data to satisfy requests

    std::vector<T> outgoing( requests.size() );

#pragma omp parallel for schedule(static)
    for (uint64_t i = 0; i < requests.size(); ++i)
      outgoing[i] = sDataFun( requests[i] );

    // ------------------------------------------------------------------------- //
    // send answer to data requests, receive data for ghosts

    std::vector<T> incoming(ghost_keys.size());

//    std::stringstream ss;
//    ss << "before:\n";
//    commspec.print(ss);

    std::vector<int> destinations = commspec.neighbor_sources_;
    std::vector<int> sendcounts = commspec.neighbor_recvcounts_;
    std::vector<int> senddispls = commspec.neighbor_recvdispls_;

    MyMPI::DSDECommSpec2
    commspec_answer(
        commspec.indegree(), // don't use destinations.size() since it is always >0 even for commspec.indegree() == 0
        destinations.data(),
        sendcounts.data(),
        mpiMgr.comm(),
        COMPILE_TIME_CRC32_STR( FILE_POS_STR ) + tag);


    for (uint64_t i = 0; i < senddispls.size(); ++i)
      MYASSERT(
          senddispls[i] == commspec_answer.neighbor_senddispls_[i],
          std::string("send mismatch:\n") +
          "  commspec_answer.neighbor_senddispls_[" + std::to_string(i) + "] == " +
          std::to_string(commspec_answer.neighbor_senddispls_[i]) + "\n" +
          "  expected " + std::to_string(senddispls[i])
    );



//    commspec.invert(); // receive count, begin are now send count, begin

//    ss << "\nafter:\n";
//    commspec.print(ss);
//
//    std::cout << ss.str() << std::endl;

    commspec_answer.comm(
        outgoing.data(),
        incoming.data(),
        sizeof(T),
        MyMPI::mpi_type<T>::type,
        COMPILE_TIME_CRC32_STR( FILE_POS_STR )+tag);


//    MPI_Alltoallv(
//        outgoing.data(),            // send
//        rcount.data(),              // send
//        rbegin.data(),              // send
//        MyMPI::mpi_type<T>::type,   // send
//        incoming.data(),            // receive
//        scount.data(),              // receive
//        sbegin.data(),              // receive
//        MyMPI::mpi_type<T>::type,   // receive
//        mpiMgr.comm() );

    // ------------------------------------------------------------------------- //
    // process the received data with the receive function

#pragma omp parallel for schedule(static)
    for (uint64_t i = 0; i < incoming.size(); ++i)
      rDataFun( i, incoming[i] );

  }

}


#endif /* SRC_GRID_GRIDGHOSTEXCHANGE_H_ */
