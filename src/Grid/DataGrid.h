/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * DataGrid.h
 *
 *  Created on: Apr 26, 2014
 *      Author: Stephan Kuechlin
 */

#ifndef DATAGRID_H_
#define DATAGRID_H_

#include <mpi.h>

#include <vector>
#include <list>
#include <utility>

#include "Grid/RegularGrid.h"
#include "Moments/MomentsForwardDecls.h"


class GenericCallback;

struct CellData;

namespace Settings
{
  class Dictionary;
}

namespace Grids {
  class DataGrid;
}

namespace MyMPI {
  struct CommSpec;
}


namespace Grids {


  class DataGrid
  {
    // a DataGrid
    // -- references the associated regular Grid
    // -- has a vector of Cells to hold data


  private:
    RegularGrid* const prtGrd_; // const pointer to (non-const) particle grid

  public:

    typedef std::list<CellData,Eigen::aligned_allocator<CellData>> DataVecT;


  private:

    const std::string name_; // the name of the grid

    DataVecT cells_; // the cells -- the objects that hold the data
    std::vector<CellData*> cellptrs_;

    std::unique_ptr<Moments::Averaging::AveragingStrategy> averager_; // provides averaging factor for data


  private:

    void setCellPtrs();

  private:

    class PassKey
    {
      friend class GridManager;
      PassKey(){}; // private, except for GridManager!
    };

  public:

    bool checkConsistencyWithParticleGrid(
        PassKey,
        std::string& msg, std::vector<uint64_t>& fails)
    const;

    void updateCellVolumesFromParticleGrid(PassKey);

    void transformCells(
        const PassKey& key,
        const std::vector<uint64_t>& remove,
        const std::vector<uint64_t>& merge,
        const std::vector<uint64_t>& split );


  public:

    // the name of the grid
    const std::string& name() const { return name_; }

    // the size of this gird
    uint64_t nCells() const
    {
      return cells_.size();
    }

    // memory factor for time averaging. tn is the time step number
    double alpha(const uint64_t tn) const;

  public:

    // cell i
    const CellData& cell(const uint64_t i) const;

    // cell i
    CellData& cell(const uint64_t i);

    static uint64_t nVars();

    static std::string getVarName(const uint64_t varId);

    double getVar(const uint64_t i, const uint64_t varId) const;

  public:

    void
    writeGrid(
        const std::string& fileName,
        const double solTime,
        const MyMPI::MPIMgr& mpiMgr )
    const;

    std::unique_ptr<GenericCallback>
    makeOutputCallback(
        Grids::Grid* nodeGrid,
        const GenericCallback* const prtGrdOutputCallback,
        const Settings::Dictionary& dict,
        const std::string& dirPref )
        const;


  public:

    void insertGhostCells(
        const RegularGrid::CellMsgT* const ghost_cells_to_insert,
        const uint64_t n);

    void insertCells(CellData* const cells_to_insert, const uint64_t n_cells_to_insert);

    void finalizeCellInsertion();


  public:

    // the referenced grid
    Grids::RegularGrid* grid() const { return prtGrd_; }



  public:

    DataGrid( RegularGrid* particleGrid,
              const Settings::Dictionary& dict );

    DataGrid( RegularGrid* particleGrid,
              const std::string& name,
              std::unique_ptr<Moments::Averaging::AveragingStrategy> averager );

    ~DataGrid();
  };


} // namespace Grids

#endif /* DATAGRID_H_ */
