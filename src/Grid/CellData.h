/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * CellData.h
 *
 *  Created on: Apr 16, 2014
 *      Author: Stephan Kuechlin
 */

#ifndef CELLDATA_H_
#define CELLDATA_H_

#include <string>
#include <cstdint>
#include <iostream>

#include "Primitives/MyError.h"
#include "Primitives/Box.h"

#include "Gas/GasModel.h"
#include "Gas/MacroState.h"
#include "Gas/InternalState.h"

#include "Moments/VelocityMoments.h"
#include "Moments/InternalMoments.h"
#include "Moments/AveragingStrategies.h"

#include "Particles/ParticleCollection.h"

#include "VelocityUpdate/VelocityUpdateStrategy.h"

struct alignas(32) CellData {

public:

  Moments::VelocityMoments vlm_;
  Moments::InternalMoments im_;

  Gas::MacroState     mst_;    // macroscopic variables
  Gas::InternalState  ist_;    // internal state

  Box::box4_t box_; // [m] cell bounding box

  VelocityUpdate::CollisionOperatorStatistics cos;

  uint64_t key_; // cell key

  double volume_;      // [m3]  fluid volume of cell

  uint8_t lvl_; // cell level
  uint8_t pad0_[7] = {0};

  double coll_carry_ = 0.; // collision counter carry-over from previous time step

  double Np_    = 0.; // number of computational particles
  double Nr_    = 0.; // number of represented (physical) particles

  double Wtot_  = 0.; // averaging window size

  double gradient_length_ = 0.;

  double tmp0_ = 0.;
  double tmp1_ = 0.;

public:

  template<typename ItT>
  void merge(ItT cell_it, const uint64_t n_to_merge, const Box::box4_t& new_box)
  {

    cos.fp_dsmc_fraction *= volume_;


    for (uint64_t i = 0; i < n_to_merge; ++i)
      {
        CellData& rhs = *cell_it++;

        vlm_ += rhs.vlm_;
        im_  += rhs.im_;


        volume_       += rhs.volume_;
        coll_carry_   += rhs.coll_carry_;

        cos.num_collisions += rhs.cos.num_collisions;
        cos.fp_dsmc_fraction += rhs.cos.fp_dsmc_fraction*rhs.volume_;

        Np_       += rhs.Np_;
        Nr_       += rhs.Nr_;

      }

    //      const v4df old_width = box_.width() + v4df{0.,0.,0.,1.};
    box_ = new_box;
    lvl_ += 1;


    //      vlm_.rescale_gradients(box_.width()/old_width);
    vlm_.update_derived();
    im_.update_derived();

    cos.fp_dsmc_fraction /= volume_;
    gradient_length_ = 0.; // reset


  }

  void toSubCell(const Box::box4_t& ss_box, const uint64_t ss_key, const double ss_vol_frac)
  {


    vlm_ *= ss_vol_frac;

    /*
    const v4df width_rat = box_.width() / ( ss_box.width() != zero<v4df>() ? ss_box.width() : one<v4df>() );
    vlm_.rescale_gradients( width_rat );
    */

    im_  *= ss_vol_frac;

    vlm_.update_derived();
    im_.update_derived();

    box_ = ss_box;
    key_ = ss_key;

    volume_     *= ss_vol_frac;
    lvl_        -= 1;
    coll_carry_ *= ss_vol_frac;
    cos.num_collisions     *= ss_vol_frac;

    Np_       *= ss_vol_frac;
    Nr_       *= ss_vol_frac;

    // reset
    gradient_length_ = 0.;
  }


public:

  // set the cell volume
  void setVolume(const double v) { volume_ = v; }


  const Box::box4_t& box() const { return box_; }

  uint64_t key() const { return key_; }

  double volume() const { return volume_; }

  uint8_t level() const { return lvl_; }



public:

  // FORWARDERS

  // [#]  total number of samples
  double Nptot() const { return Np_*Wtot_; }

  // [#] number of samples per time step
  double Np() const { return Np_; }

  // [m2 s-2] trace of second order central velocity moments
  double u2() const { return vlm_.u<2>(); };

  /*
  // [m2 s-2 / m ] gradient of trace of second order central velocity moments
  template<uint8_t m> double du2dx() const { return vlm_.dudx<2,m>(); }
  */

  // [m4 s-4] trace of fourth order central velocity moments
  double u4() const { return vlm_.u<4>(); };

  // [m s-1] mean velocity
  template<uint8_t i> double u0() const { return vlm_.u<0,i>(); }

  // [m3 s-3] 1x contracted third order central velocity moment tensor
  template<uint8_t i> double u2() const { return vlm_.u<2,i>(); }

  // [m5 s-5] 2x contracted fifth order central velocity moment tensor
  template<uint8_t i> double u4() const { return vlm_.u<4,i>(); }

  // [m6 s-6] 3x contracted sixth order central velocity moment tensor
  double u6() const { return vlm_.u<6>(); }

  // [m3 s-3] 1x contracted third order central velocity moment tensor
  v4df u2i() const { return v4df{vlm_.u<2,0>(),vlm_.u<2,1>(),vlm_.u<2,2>(),0.}; }

  // [m3 s-3] 1x contracted third order central moment tensor omegai omegaj vk
  template<uint8_t i> double u2_rot() const { return im_.u2_rot<i>(); }

  // [m3 s-3] 1x contracted third order central moment tensor xii xij vk
  template<uint8_t i> double u2_vib() const { return im_.u2_vib<i>(); }

  // [m2 s-2] second order central velocity moment tensor
  template<uint8_t i, uint8_t j> double u0() const { return vlm_.u<0,i,j>(); }

  // [m4 s-4] 1x contracted fourth order central velocity moment tensor
  template<uint8_t i, uint8_t j> double u2() const { return vlm_.u<2,i,j>(); }

  // [m6 s-6] 1x contracted sixth order central velocity moment tensor
  template<uint8_t i, uint8_t j> double u4() const { return vlm_.u<4,i,j>(); }

  // [m3 s-3] third order central velocity moment tensor
  template<uint8_t i, uint8_t j, uint8_t k>  double u0() const  {
    return vlm_.u<0,i,j,k>(); }

  // [m5 s-5] 1x contracted fifth order central velocity moment tensor
  template<uint8_t i, uint8_t j, uint8_t k>  double u2() const  {
    return vlm_.u<2,i,j,k>(); }

  // [m4 s-4] forth order central velocity moment tensor
  template<uint8_t i, uint8_t j, uint8_t k, uint8_t l>  double u0() const  {
    return vlm_.u<0,i,j,k,l>(); }

  // [m6 s-6] 1x contracted sixth order central velocity moment tensor
  template<uint8_t i, uint8_t j, uint8_t k, uint8_t l>  double u2() const  {
    return vlm_.u<2,i,j,k,l>(); }

  // [m5 s-5] fith order central velocity moment tensor
  template<uint8_t i, uint8_t j, uint8_t k, uint8_t l, uint8_t m>  double u0() const  {
    return vlm_.u<0,i,j,k,l,m>(); }

public:

  // density [ kg / m**3 ]
  double rho() const { return vlm_.C<0>()/volume(); }

  // weighted centered moments [ kg (m/s)**(2s+len(A)) / m**3 ]
  template<uint8_t s, uint8_t... A>
  double rho() const {
    static_assert(sizeof...(A) < 6, "invalid number of subscripts");
    return vlm_.C<s,A...>()/volume();
  }

  // [kg m-4] gradient of density
  template<uint8_t m>
  double drhodx() const {
    static_assert(m<3,"index m out of bounds");
    return vlm_.dSdx<0,m>()/volume(); }

  // [kg m-4] gradient vector of density
  v4df drho() const { return v4df{ drhodx<0>(), drhodx<1>(), drhodx<2>(), 0.}; }

  /*
  // weighted gradient of centered moments [ kg (m/s)**(2s) / m**3 /m ]
  template<uint8_t s, uint8_t m>
  double drhodx() const {
    return vlm_.dCdx<s,m>()/volume();
  }

  // weighted gradient of centered moments [ kg (m/s)**(2s+len(A)) / m**3 /m ]
  template<uint8_t s, uint8_t... A, uint8_t m>
  double drhodx() const {
    static_assert(sizeof...(A) < 3, "invalid number of subscripts");
    return vlm_.dCdx<s,A...,m>()/volume();
  }

  */


public:


  // [m s-1] average velocity
  template<uint8_t i>
  double v() const { return vlm_.v<i>(); }

  // [s-1] gradient of average velocity
  template<uint8_t i, uint8_t m>
  double dvdx() const { return vlm_.dvdx<i,m>(); }

  // [s-1] gradient vector of average velocity
  template<uint8_t i>
  v4df dv() const { return v4df{ dvdx<i,0>(), dvdx<i,1>(), dvdx<i,2>(), 0.}; }

  // [ms-1] average velocity vector
  v4df v() const { return vlm_.v(); }

  // [ms-1] magnitude of average velocity
  double vabs() const { return vlm_.vabs(); }

  // [-] Mach number
  double Ma() const { return mst_.Ma; }

  // [m-3] number density
  double n() const { return mst_.n; }

  // [K] temperature
  double T() const { return mst_.T; }

  // [K] rotational temperature
  double Trot() const { return ist_.T_rot; }

  // [K] vibrational temperature
  double Tvib() const { return ist_.T_vib; }

  // [m2 s-2] = [J kg-1] translational temperature in energy units (= R*T)
  double theta() const { return (1./3.)*u2(); }

  // [kg m2 s-2 m-1] = [J m-1] gradient of translational temperature in energy units
  template<uint8_t m> double dthetadx() const { /* return (1./3.)*du2dx<m>(); */
    return (1./3.)*( vlm_.dSdx<1,m>()/vlm_.S<0>() -
        2.*(v<0>()*dvdx<0,m>() + v<1>()*dvdx<1,m>() + v<2>()*dvdx<2,m>()) -
        (vlm_.S<1>()/vlm_.S<0>())*(vlm_.dSdx<0,m>()/vlm_.S<0>()) );
  }

  // [J kg-1] == [m2 s-2] rotational temperature in energy units
  double theta_rot() const { return im_.theta_rot(); }

  // [J kg-1] == [m2 s-2] vibrational temperature in energy units
  double theta_vib() const { return im_.theta_vib(); }


  // [J kg-1 m-1] == [m s-2] gradient vector of temperature in energy units
  v4df dtheta() const { return v4df{ dthetadx<0>(), dthetadx<1>(), dthetadx<2>(), 0.}; }

  // [N m-2] pressure
  double p() const { return (1./3.)*rho<1>(); }

  // [N m-3] gradient of pressure
  template<uint8_t m>
  double dpdx() const { return (1./3.)*drhodx<1,m>(); }

  // [N m-2] == [kg m-1 s-2] pressure tensor
  template<uint8_t i, uint8_t j>
  double p() const { return rho<0,i,j>(); }

  // [N m-2] == [kg m-1 s-2] stress tensor (trace-free part of pressure tensor)
  template<uint8_t i, uint8_t j>
  double sigma() const {     // [N m-2] == [kg m-1 s-2] stress tensor
    if (i == j) return p<i,j>() - p();
    return p<i,j>() ;
  }

  // [kg m s-4]
  template<uint8_t i, uint8_t j>
  double R() const {
    if (i == j) return rho<1,i,j>() - (1./3.)*rho<2>() - 7.0*theta()*sigma<i,j>();
    return rho<1,i,j>() - 7.0*theta()*sigma<i,j>(); }

  // [kg m s-4] 4th order non-equilibrium moment w^2 = rho^2 - rho^2|E
  double Delta() const { return ( rho<2>() - 15.0*p()*theta() ); }

  template<uint8_t i, uint8_t j, uint8_t k>
  double m() const {
    double ret = rho<0,i,j,k>();
    if ( i == j ) ret -= (1./5.)*rho<1,k>();
    if ( i == k ) ret -= (1./5.)*rho<1,j>();
    if ( j == k ) ret -= (1./5.)*rho<1,i>();
    return ret;
  }

  // [kg] mass
  double mass() const { return vlm_.C<0>(); }

  // [kg s-3] = [W m-2] heat flux
  template<uint8_t i> double q() const { return .5*rho<1,i>(); }

  // [kg s-3] = [W m-2] heat flux vector
  v4df q() const { return v4df{q<0>(),q<1>(),q<2>(),0.}; }

  // [W m-3] gradient of heat flux
  template<uint8_t i, uint8_t m> double dqdx() const { return .5*drhodx<1,i,m>(); }

  // [J m-2 s-1] == [kg s-3] rotational heat flux vector
  template<uint8_t i> double q_rot() const { return .5*im_.C1_rot<i>()/volume(); }

  // [J m-2 s-1] == [kg s-3] vibrational heat flux vector
  template<uint8_t i> double q_vib() const { return .5*im_.C1_vib<i>()/volume(); }

  // [J kg-1] == [m2 s-2] specific thermal translational energy
  double u() const {/*Cercignani, 1.6.12*/ return  0.5 * u2(); }

  // [J kg-1] == [m2 s-2] specific thermal rotational energy
  double u_rot() const { return im_.u_rot(); }

  // [J kg-1] == [m2 s-2] specific thermal rotational energy
  double u_vib() const { return im_.u_vib(); }

  // [J kg-1] total (thermal +  mean flow) specific translational energy
  double e() const { return vlm_.e(); }

  // [J kg-1] gradient of total (thermal +  mean flow) specific translational energy
  template<uint8_t m> double dedx() const { return .5*( vlm_.dSdx<1,m>()/vlm_.S<0>() -
      (vlm_.S<1>()/vlm_.S<0>())*(vlm_.dSdx<0,m>()/vlm_.S<0>()) ) ; }

  // [J kg-1] gradient vector of total (thermal +  mean flow) specific translational energy
  v4df de() const { return v4df{dedx<0>(),dedx<1>(),dedx<2>(),0.}; }

  // [m] equilibrium mean free path
  double mfp() const { return mst_.mfp; }

  // [s-1] equilibrium collision frequency
  double nu() const { return mst_.nu; }

  // [Pa s] == [kg m-1 s-1] viscosity
  double mu() const { return mst_.mu; }


public:

  void update(
      const Moments::VelocityMoments& convective_moments,
      const Moments::InternalMoments& convective_internal_moments,
      const double N,
      const Moments::Averaging::Averager  av,
      const Gas::GasModels::GasModel* const  gas )
  {
    av(vlm_, convective_moments);
    vlm_.update_derived();

#ifndef NDEBUG
    vlm_.assert_finite();
#endif

    // update macro state
    mst_.update( rho(), theta(), vabs(), gas );

    if (gas->diatomic())
      {
        // internal moments
        av(im_, convective_internal_moments);
        im_.update_derived();

        // update internal state
        ist_.update( im_.u_rot(), im_.u_vib(), T(), gas);
      }

    av(Np_,N);

    Nr_   = mass()/(gas->m());
    Wtot_ = av.Wtot() > 0. ? av.Wtot() : Wtot_;


  }

  void update(const VelocityUpdate::CollisionOperatorStatistics& _cos,
              const Moments::Averaging::Averager  av) {
    av(cos.fp_dsmc_fraction,_cos.fp_dsmc_fraction);
    av(cos.num_collisions,_cos.num_collisions);
  }


public:

  static uint64_t nVars();

  double getVar(const uint64_t i) const;

  static std::string
  getVarName(const uint64_t i);

  static uint64_t
  getVarInd(const std::string& varName);


public:
  EIGEN_MAKE_ALIGNED_OPERATOR_NEW

  CellData()
  : box_(
      v4df{-1.,-1.,-1.,-1.},
      v4df{-1.,-1.,-1.,-1.})
  , key_(-1)
  , volume_(-1)
  , lvl_(-1)
  {}

  CellData(
      const Box::box4_t& box,
      const uint64_t key,
      const double volume,
      const uint8_t level)
  : box_(box)
  , key_(key)
  , volume_(volume)
  , lvl_(level)
  {}

  //  // move ctor
  //  Cell(const Cell&& other); // = delete;
  //
  // copy ctor
  CellData(const CellData& other) = default;


}; // CellData



#endif /* CELLDATA_H_ */
