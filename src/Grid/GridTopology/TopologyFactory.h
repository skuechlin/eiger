/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * TopologyFactory.h
 *
 *  Created on: Apr 20, 2016
 *      Author: kustepha
 */

#ifndef SRC_GRID_TOPOLOGYFACTORY_H_
#define SRC_GRID_TOPOLOGYFACTORY_H_

#include <memory>

namespace Settings
{
  class Dictionary;
}

class SimulationBox;

namespace Grids
{

  class Topology;


  std::unique_ptr<Grids::Topology>
  makeTopology(
      const Settings::Dictionary& dict,
      const SimulationBox& simBox );

}



#endif /* SRC_GRID_TOPOLOGYFACTORY_H_ */
