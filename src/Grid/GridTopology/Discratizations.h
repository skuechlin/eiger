/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * Discratizations.h
 *
 *  Created on: Apr 6, 2016
 *      Author: kustepha
 */

#ifndef SRC_GRID_DISCRATIZATIONS_H_
#define SRC_GRID_DISCRATIZATIONS_H_

#include "Grid/GridForwardDecls.h"

#include <cstdint>
#include <map>

#include "Primitives/MyArithm.h"
#include "Primitives/MyError.h"

namespace Settings
{
  class Dictionary;
}

class SimulationBox;

namespace Grids
{

  namespace Discretizations
  {

    enum Discretization
    {
      UNIFORM
    };

    static std::map<std::string,Discretization> DiscretizationNames =
    {
        {"uniform",UNIFORM}
    };

    template<uint8_t ndims>
    struct alignas(32) Uniform
    {


      private:
        const v4df origin_;

        const v4du n_; //xyz number of background cells in each dimension

        const v4df h_; //xyz background cell widths

        const v4df d_; //xyz reciprocal background cell widths

        const uint8_t max_level_;

      public:

        uint8_t maxLevel() const { return max_level_; }

        uint64_t
        nCellsTot()
        const
        {
          uint64_t ret = 1;
          for (uint8_t i = 0; i < ndims; ++i)
            ret *= n_[i];
          return ret;
        }

        v4du dims() const { return n_; }

        box4_t boundingBox() const { return {origin_, origin_ + MyIntrin::cvt_du2df(n_)*h_ }; }

        v4df coords2Pos(const v4du& coords ) const {
          return origin_ + MyIntrin::cvt_du2df(coords) * h_; }

        v4du pos2Coords(v4df pos ) const {
          pos -= origin_;
          pos *= d_;
          return MyIntrin::cvt_df2du_truncate( pos );  }

        // position of center of sub cell si within cell with corner coordinates "lower" and "upper,
        // based on division into ns sub-cells per dimension
        v4df
        subCellCenter(
            const v4du& lower,
            const v4du& upper,
            const uint64_t si,
            const uint64_t ns)
        const
        {


          const v4du sizes = {ns,ns,ns,ns};
          v4du sccoords{0,0,0,0};


          MyArithm::ordered2coords<ndims>( si, &(sizes[0]), &(sccoords[0]) );

          const v4df subcell_spacing = MyIntrin::cvt_du2df(upper-lower)*h_ / MyIntrin::cvt_du2df(sizes);

          return coords2Pos(lower) + (MyIntrin::cvt_du2df(sccoords) + v4df{.5,.5,.5,.5}) * subcell_spacing;


        }

      public:

        Uniform(
            const Settings::Dictionary& dict,
            const SimulationBox& simBox );


    }; // struct Uniform


  } // namespace Discretizations


} // namespace Grids



#endif /* SRC_GRID_DISCRATIZATIONS_H_ */
