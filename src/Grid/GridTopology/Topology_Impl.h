/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * Topology_Impl.h
 *
 *  Created on: Apr 5, 2016
 *      Author: kustepha
 */

#ifndef SRC_GRID_TOPOLOGY_IMPL_H_
#define SRC_GRID_TOPOLOGY_IMPL_H_

#include <omp.h>
#include <cstdint>
#include <set>

#include "GridTopology.h"

#include "Primitives/MyArithm.h"
#include "Primitives/MyIntrin.h"
#include "Primitives/Partitioning.h"
#include "Primitives/MyError.h"

#include "Particles/ParticleCollection.h"


namespace Settings
{
  class Dictionary;
}

class SimulationBox;

namespace Grids
{

  template<
  uint8_t nd,
  class Csys,
  class Discr,
  class Ordering >
  class Topology_Impl : public Topology
  {


    static_assert(nd>0);
    static_assert(nd<4);


  private:

    const Csys      coordinate_system_;
    const Discr     discretization_;
    const Ordering  ordering_;

  private:


    v4du lower(const uint64_t key, const uint8_t level)
    const
    {
      // index space stride per dimension
      const uint64_t stride = (uint64_t(1)<<level);

      // lower corner, round key coords to next lower multiple of stride
      return stride * ( ordering_.key2Coords(key) / stride);
    }

    void
    to_lower_upper(const uint64_t key, const uint8_t level, v4du& l, v4du& u)
    const
    {
      // lower corner
      l = lower(key,level);

      // upper corner
      u = l + active4du<nd>()*(uint64_t(1)<<level);
    }

    // number of background cells within cell
    uint64_t
    n_cells_in_cell(const uint64_t key, const uint8_t level) const
    {
      v4du l,u,n;
      to_lower_upper(key,level,l,u);
      n = discretization_.dims();
      v4du i = (n < u ? n : u) - (n < l ? n : l);
      return MyIntrin::sprod<nd>(i);
    }


    void
    to_lower_upper(const uint64_t key, const uint8_t level, v4df& lower, v4df& upper)
    const
    {

      // lower and upper corner
      v4du clower;
      v4du cupper;
      to_lower_upper(key, level, clower, cupper);

      // compute csys representation of lower corner
      lower = discretization_.coords2Pos( clower );

      // compute csys representation of upper corner
      upper = discretization_.coords2Pos( cupper );

    }



  public:

    constexpr uint8_t ndims() const override { return nd; }

    constexpr bool axisymmetric() const override { return coordinate_system_.axisymmetric(); }

    // the maximum aggregation level guaranteed to be supported on given discretization
    constexpr uint8_t maxLevel() const override { return discretization_.maxLevel(); }

    // the largest possible key value
    constexpr uint64_t maxKey() const override { return ordering_.maxKey( discretization_.dims() ); }

    // the number of cells at level "low_level" per cell at level "high_level"
    // returns 0 if low_level > high_level
    constexpr uint64_t nCellsPerCell(const uint8_t low_level, const uint8_t high_level)
    const override {
      return MyArithm::ipow((uint64_t(1)<<high_level),nd) / MyArithm::ipow((uint64_t(1)<<low_level),nd);
    }

    // the number of neighboring faces of cells at level "low_level" per face of cell at level "high_level"
    // returns 1 if low_level > high_level
    constexpr uint64_t nFacesPerFace(const uint8_t high_level, const uint8_t low_level)
    const
    override
    {
      if (nd == 1)
        return 1;

      if (low_level > high_level)
        return 1;

      return uint64_t(1)<<( (nd-1)*(high_level-low_level) );
    }

    // the number of neighboring edges of cells at level "low_level" per edge of cell at level "high_level"
    // returns 1 if low_level > high_level
    constexpr uint64_t nEdgesPerEdge(const uint8_t high_level, const uint8_t low_level)
    const
    override
    {
      if (nd == 1)
        return 1;

      if (low_level > high_level)
        return 1;

      return uint64_t(1)<<(high_level-low_level);
    }

    // the total number of cells on given level
    uint64_t nCellsTot(const uint8_t level) const override {
      return discretization_.nCellsTot() / nCellsPerCell(0,level); }


  public:

    // return coordinates encoded by key
    v4du key2Coords(const uint64_t key) const override { return ordering_.key2Coords(key); }


  private:

    uint64_t coords2Key(const v4du& coords) const { return ordering_.coords2Key(coords); }

    // return coords of vertex vrt_kji (from most significant bit: k,j,i) in cell with key "key" and level "level"
    constexpr v4du vrtCoords(const uint64_t key, const uint8_t level, const uint8_t vrt_kji)
    const
    {
      // lower corner
      v4du c = lower(key,level);

      const uint64_t stride = (uint64_t(1)<<level);

      if (vrt_kji & 1)
        c[0] += stride;
      if (vrt_kji & 2)
        c[1] += stride;
      if (vrt_kji & 4)
        c[2] += stride;

      return c;
    }

    // return key of vertex vrt_kji (from most significant bit: k,j,i) in cell with key "key" and level "level"
    uint64_t vrtKey(const uint64_t key, const uint8_t level, const uint8_t vrt_kji) const override {
      return coords2Key(vrtCoords(key,level,vrt_kji)); }

  public:

    inline constexpr uint8_t faceVrt(const uint8_t face) const
    { //
      // xlow: *0 xhigh: *1
      // ylow: *2 yhigh: *3
      // zlow: *4 zhigh: *5
      //
      //       y               y *3                *3   y
      //      2 ____ 3         2 ___ 3          7 ______ 3
      //      /|   /|       *0  |   |  *1    *5  |      |  *4
      //     / |__/_|  x        |___|  x      x  |______|
      //    / 0/ /  /1         0     1          5        1
      //  6/__/_/7 /              *2                *2
      //   | /  | /
      //   |/___|/
      //  4      5
      // z                       0, 1, 2, 3, 4, 5
      constexpr uint8_t lut[] = {0, 1, 0, 2, 0, 4};
      return lut[face];
    }

    void
    faceNeighborKeys(
        uint64_t* const neighbor_keys,
        const uint64_t key,
        const uint8_t level,
        const uint8_t neighbor_level,
        const uint8_t face)
    const
    override
    {
      // initialize to non-existing
      const uint64_t nfaceneighbors = nFacesPerFace(level, neighbor_level);

      for (uint64_t i = 0; i < nfaceneighbors; ++i)
        neighbor_keys[i] = uint64_t(-1);

      // start computation

      // get lowest face vertex
      v4du c = vrtCoords(key,level,faceVrt(face));

      // neighbor cell width in coordinate units
      const uint64_t stride = (uint64_t(1)<<neighbor_level);

      // for low faces (0,2,4), shift base corner in low direction if possible
      if (face == 0)
        {
          if (c[0] == 0)
            return;
          else
            c[0] -= stride;
        }
      if (face == 2)
        {
          if (c[1] == 0)
            return;
          else
            c[1] -= stride;
        }
      if (face == 4)
        {
          if (c[2] == 0)
            return;
          else
            c[2] -= stride;
        }

      if (nd == 1)
        {
          neighbor_keys[0] = coords2Key(c);
        }
      else
        {

          const uint64_t nedgeneighbors = nEdgesPerEdge(level,neighbor_level);

          const uint8_t dir0 = face < 2 ? 1 : 0;

          if (nd == 2)
            {
              for (uint8_t i = 0; i < nedgeneighbors; ++i)
                {
                  neighbor_keys[i] = coords2Key(c);
                  c[dir0] += stride;
                }
            }
          else //(nd == 3)
            {
              const uint64_t dir1 = face < 4 ? 2 : 1;
              const uint64_t cdir1 = c[dir1];

              for (uint8_t i = 0; i < nedgeneighbors; ++i)
                {
                  for (uint8_t j = 0; j < nedgeneighbors; ++j)
                    {
                      neighbor_keys[j + i*nedgeneighbors] = coords2Key(c);
                      c[dir1] += stride;
                    }
                  c[dir1] = cdir1;
                  c[dir0] += stride;
                }
            }
        }
    }

  private:

    // bounding box of entire grid
    box4_t boundingBox() const override {
      return discretization_.boundingBox(); }


    // bounding box of cell with key "key" and level "level"
    // box not accounting for symmetry
    box4_t cellBox(const uint64_t key, const uint8_t level)
    const override {
      v4df lower, upper;
      to_lower_upper(key, level, lower, upper);
      return {lower, upper}; }

    // a random position in cell with key "key" and level "level"
    v4df
    sample(const uint64_t key, const uint8_t level, MyRandom::Rng& rndGen)
    const
    override
    {
      const box4_t box = cellBox(key,level);
      return coordinate_system_.sample(box.lower,box.upper,rndGen);
    }

    // volume of cell with key "key" and level "level"
    double
    cellVol(const uint64_t key, const uint8_t level)
    const
    override
    {
      const box4_t box = cellBox(key,level);
      return coordinate_system_.cellVol(box.lower,box.upper);
    }

    // position of cell center with key "key" and level "level"
    v4df cellCenter(const uint64_t key, const uint8_t level) const override  {
      return cellBox(key,level).center(); // return center in xyz
    }

    // position of center of sub cell si within cell with key "key" and level "level",
    // based on division into ns sub-cells per dimension
    v4df
    subCellCenter(
        const uint64_t key,
        const uint8_t level,
        const uint64_t si,
        const uint64_t ns)
    const
    override
    {
      v4du clower;
      v4du cupper;

      to_lower_upper(key,level,clower,cupper);

      // return  center
      return discretization_.subCellCenter(clower,cupper,si,ns);

    }

    // returns key of neighbor with level "neighbor_level"
    // of the cell with key "key" and level "level"
    // in direction "dir_skji" (from most significant bit: sign,k,j,i)
    uint64_t
    neighborKey(
        const uint64_t key,
        const uint8_t level,
        const uint8_t neighbor_level,
        const uint8_t dir_skji)
    const
    override
    {

      // lower corner of neighbor cell, start at lower corner of current cell
      v4du clower = lower(key,level);

      const uint64_t stride           = ( uint64_t(1) << level);
      const uint64_t neighbor_stride  = ( uint64_t(1) << neighbor_level);

      if ( dir_skji & 8 )
        {
          // negative direction, move by neighbor stride
          for (uint8_t d = 0; d < nd; ++d)
            if( dir_skji & (uint8_t(1)<<d) )
              clower[d] -= neighbor_stride;
        }
      else
        {
          // positive direction, move by current cell stride
          for (uint8_t d = 0; d < nd; ++d)
            if( dir_skji & (uint8_t(1)<<d) )
              clower[d] += stride;

        }

      // generate key and round down to multiple of number of fine cells at neighbor level
      const uint64_t n = nCellsPerCell(0,neighbor_level);

      return n*(ordering_.coords2Key(clower) / n);

    }

  private:

    // return position encoded by key
    v4df key2Pos(const uint64_t key) const override {
      const v4du c = ordering_.key2Coords(key);
      return discretization_.coords2Pos(c);  }

  private:

    // fills key vector keys with keys specifying n cells at level level,
    // with first key equal to start, a valid key
    void
    generateRegularKeys(
        std::vector<uint64_t>& keys,
        const uint64_t start,
        const uint64_t n,
        const uint8_t level)
    const
    override
    {

      MYASSERT(level < 64,"passed level greater 63 to generateRegularKeys!");


      // a key limiting the available key range
      uint64_t limit = maxKey();

      uint64_t keys_per_cell = nCellsPerCell(0,level);

      MYASSERT( start % keys_per_cell == 0 , "start of key range does not fall on level boundary");

      // total number of valid keys
      uint64_t tot_nkeys = discretization_.nCellsTot();

      // assert that tot_nkeys is integral multiple of keys per cell
      MYASSERT( tot_nkeys % keys_per_cell == 0, std::string("total number of finest level cells in discretization")
      + std::string(" is not an integral multiple of the desired number of keys per cell" ) );


      keys.reserve( n + 2 );

      // set current key to start
      uint64_t ck = start;
      uint64_t ik = 0;

      // key limiting the cell coordinate index range
      uint64_t corner_key = ordering_.coords2Key( discretization_.dims() );

      while (ck < limit)
        {

          if ( ordering_.inCuboid(ck,corner_key) )
            {
              keys.emplace_back( ck );
              ++ik;
            }

          ck += keys_per_cell;
          if (ik ==n)
            break;

          // skip as many additional keys as possible
          uint8_t search_level = level;
          while (true)
            {
              if (n_cells_in_cell(ck,search_level)==0) // no valid keys in cell
                {
                  ck += nCellsPerCell(0,search_level); // proceed to next cell
                  if ( (search_level < 63) && ((ck % nCellsPerCell(0,search_level+1)) == 0) ) // if in first sub-cell of next level cell
                    ++search_level; // check higher level
                }
              else
                {
                  if (search_level > level)
                    --search_level;
                  else // valid keys in cell at level level
                    break;
                }
            }

        }

      // set key at position ncells to upper limit of last cell
      keys.emplace_back( ck );

      // set guard value
      keys.emplace_back( uint64_t(-1) );


      MYASSERT(ik == n,
               std::string("generateRegularKeys seems to have run out of keys!\n") +
               std::string("after generating ") + std::to_string(ik) +
               std::string(" out of ") + std::to_string(n) +
               std::string(" keys,\n") +
               std::string("current key = ") + std::to_string(ck) +
               std::string(" out of ") + std::to_string(limit)
      );

      //      DEBUG
      //      std::cout << "generated following " << keys.size() << " keys:" << std::endl;
      //      for (uint64_t i = 0; i < n+2; ++i)
      //        std::cout << keys[i] << std::endl;


    }

    // fills key vector "keys" with keys specifying "num_partitions" cells,
    // each of which comprised of an integer amount of cells at level "sslevel"
    void
    generateRegularKeys(
        std::vector<uint64_t>& keys,
        const uint64_t npartitions,
        const uint8_t sslevel)
    const
    override
    {

      if (npartitions < 1) return;


      // a key limiting the available key range
      const uint64_t limit = maxKey();

      const uint64_t ncells_per_sslevel_cell = nCellsPerCell(0,sslevel);

      // DEBUG:
      //      std::cout << "generateRegularKeys(std::vector<uint64_t>& keys, const uint64_t n, const uint_fast8_t level)" << std::endl;
      //      std::cout << std::to_string(n) << std::endl;
      //      std::cout << std::to_string(level) << std::endl;
      //      std::cout << std::to_string((uint64_t(1)<<level)) << std::endl;
      //      std::cout << std::to_string(nd) << std::endl;
      //      std::cout << std::to_string(MyArithm::ipow((uint64_t(1)<<level),nd)) << std::endl;
      //      std::cout << keys_per_sub_cell << std::endl;
      //      std::cout << std::endl;
      //      std::cout << discretization_.dims().transpose(); std::cout << std::endl;

      // total number of valid keys
      const uint64_t tot_n_keys = discretization_.nCellsTot();


      // assert that tot_nkeys is integral multiple of keys per cell
      MYASSERT( tot_n_keys % ncells_per_sslevel_cell == 0, std::string("total number of finest level cells in discretization")
      + std::string(" is not an integral multiple of the desired number of keys per sub cell" ) );

      keys.resize(npartitions+2);

      const uint64_t nsslevel_cells = tot_n_keys / ncells_per_sslevel_cell;

      // key limiting the cell coordinate index range
      const uint64_t corner_key = ordering_.coords2Key( discretization_.dims() );

      // partition tot_n_sub_cells n-way:
      uint64_t ck = 0;

      for (uint64_t i = 0; i < npartitions; ++i)
        {
          keys[i] = ck;

          uint64_t nsslevel_cells_in_partition = MyPartitioning::partitionLength(i,nsslevel_cells,npartitions);

          while (ck < limit)
            {

              if (ordering_.inCuboid(ck,corner_key))
                {
                  --nsslevel_cells_in_partition;
                  ck += ncells_per_sslevel_cell;
                  if (nsslevel_cells_in_partition == 0)
                    break;
                }

              // skip as many additional keys as possible
              uint8_t search_level = sslevel;
              while (true)
                {
                  if (n_cells_in_cell(ck,search_level)==0) // no valid keys in cell
                    {
                      ck += nCellsPerCell(0,search_level); // proceed to next cell
                      if ( (search_level < 63) && ((ck % nCellsPerCell(0,search_level+1)) == 0) ) // if in first sub-cell of next level cell
                        ++search_level; // check higher level
                    }
                  else
                    {
                      if (search_level > sslevel)
                        --search_level;
                      else // valid keys in cell at level level
                        break;
                    }
                }
            }


        }

      // guard cell at end
      keys[npartitions] = ck;

      // guard value
      keys.back() = uint64_t(-1);

      // DEBUG
      //      std::cout << "generated following " << keys.size() << " keys:" << std::endl;
      //      for (uint64_t i = 0; i < npartitions+2; ++i)
      //        std::cout << keys[i] << std::endl;

    }

    // adds to key vector keys the keys of all neighboring cells at level level still in domain
    // returns in first and last index of first non-ghost
    // cell key and first ghost cell key after non-ghost, respectively
    void
    generateGhostCellKeys(
        uint64_t& first,
        uint64_t& last,
        std::vector<uint64_t>& keys,
        const uint8_t level)
    const
    override
    {
      // key of highest coordinates
      const uint64_t upper_corner = ordering_.coords2Key( discretization_.dims() );

      std::set<uint64_t> ghosts;
      const uint64_t size = keys.size()-2; // number of cells excluding guard
      const uint64_t minkey = keys[0];
      const uint64_t maxkey = keys[size];// maxkey = keys[size-1];

      // DEBUG
      //                std::cout << "minkey " << minkey << " maxkey " << maxkey << " upper_corner: " << upper_corner << std::endl;



      for (uint8_t s = 0; s < 2; ++s) // sign
        for (uint8_t d = 0; d < nd; ++d) // dimension
          {
            for (uint64_t i = 0; i < size; ++i)
              {
                uint64_t cand = neighborKey( // candidate key
                    keys[i],level,level,
                    (s<<3) | uint8_t(1)<<d );

                //                  std::cout << minkey << " " << maxkey << " " << keys[i] << " " << cand << " " << std::to_string(ordering_.inCuboid(cand,limit)) << std::endl;

                if ( ordering_.inCuboid(cand,upper_corner) &&
                    ( cand < minkey || cand >= maxkey ) ) // key not in existing range but in domain
                  {
                    //                std::cout << minkey << " " << maxkey << " level: " << std::to_string(level) << " " << std::to_string(s) << " " << std::to_string(d) << " " << keys[i] << " " << cand << " " << std::to_string(ordering_.inCuboid(cand,upper_corner)) << std::endl;
                    ghosts.insert( cand );
                  }

              }
          }

      // now merge the ghosts into the key vector

      // DEBUG
      //                std::cout << "n ghost: " << ghosts.size() << std::endl;
      //
      //                for (auto g: ghosts)
      //                  std::cout << g << std::endl;

      uint64_t new_size = keys.size() + ghosts.size();
      keys.resize( new_size );

      // fill from back

      auto g = ghosts.rbegin();

      auto d = keys.rbegin();
      std::advance(d,2);

      auto k = keys.rbegin();
      std::advance(k,uint64_t(ghosts.size()+2));

      MYASSERT(uint64_t( std::distance(k,keys.rend()) ) == size,"error shifting k");

      //       0   1   2   3   4   5
      //-----|---|---|---|---|---|---|----
      //       4   5                          ghosts
      //           g
      //       1   2   3  inf                 keys
      // rend      k              rbegin
      //
      //       1   2   4   5                  keys after copy
      //     first    last


      last  = keys.size()-2;

      // ghosts greater than keys
      while ( g != ghosts.rend() && *g >= maxkey )
        {
          *d++ = *g++;
          --last;
        }
      first = last;

      // keys
      while ( k != keys.rend() )
        {
          *d++ = *k++;
          --first;
        }

      // ghosts smaller than keys
      while ( g != ghosts.rend() )
        *d++ = *g++;

      MYASSERT(last - first == size,
               std::string("we seem to have lost some keys ")
      + std::to_string(first) + std::string(" ") + std::to_string(last)
      + std::string(" ") + std::to_string(size) );

      // create guard cell at end
      keys[keys.size()-1] = uint64_t(-1);
      keys[keys.size()-2] = keys[keys.size()-3] + nCellsPerCell(0,level);

    }



  public:

    uint64_t pos2Key(const v4df& pos) const override {
      return ordering_.coords2Key(
          discretization_.pos2Coords( pos ) ); }


    // update all particle keys, return maximum key (max of all valid keys)
    // set keys of particles to remove to -1
    template<bool ComputeMaxMin>
    void computeKeys_Impl(
        Particles::ParticleCollection& prts,
        uint64_t* const _max_key, uint64_t* const _min_key,
        const uint64_t first, const uint64_t last)
    const
    {
      MYASSERT(first <= last,"first > last");
      MYASSERT(last <= prts.size(),"last > prts.size()");

      // compute keys
      uint64_t max_key = 0;
      uint64_t min_key = -1;

      if (prts.nToRemove() > 0)
        {

          for (uint64_t i = first; i < last; ++i)
            {

              if ( __builtin_expect(Particles::is_marked_remove_tf(prts.getParticle(i)),false) )
                {
                  prts.getKey(i) = uint64_t(-1);
                }
              else
                {
                  const uint64_t k = ordering_.coords2Key(
                      discretization_.pos2Coords(
                          prts.getParticle(i)->R ));

                  prts.getKey(i) = k;
                  if (ComputeMaxMin)
                    {
                      max_key = k > max_key ? k : max_key;
                      min_key = k < min_key ? k : min_key;
                    }
                }
            }

        }
      else
        {

          for (uint64_t i = first; i < last; ++i)
            {
              const uint64_t k = ordering_.coords2Key(
                  discretization_.pos2Coords(
                      prts.getParticle(i)->R ));

              prts.getKey(i) = k;
              if (ComputeMaxMin)
                {
                  max_key = k > max_key ? k : max_key;
                  min_key = k < min_key ? k : min_key;
                }
            }

        }

      if (ComputeMaxMin)
        {
          *_max_key = max_key;
          *_min_key = min_key;
        }
    }

    // update all particle keys set keys of particles to remove to -1
    virtual
    void
    computeKeys(Particles::ParticleCollection& prts,
                const uint64_t first, const uint64_t last)
    const
    override
    {
      computeKeys_Impl</*compute max and min*/false>(prts,0,0,first,last);
    }

    // update all particle keys, return maximum key (max of all valid keys)
    // set keys of particles to remove to -1
    virtual
    void
    computeKeys(Particles::ParticleCollection& prts, uint64_t& _max_key, uint64_t& _min_key,
                const uint64_t first, const uint64_t last)
    const
    override
    {
      computeKeys_Impl</*compute max and min*/true>(prts,&_max_key,&_min_key,first,last);
    }

    // update all particle keys, return maximum key (max of all valid keys)
    // set keys of particles to remove to -1
    virtual
    void
    computeKeys(Particles::ParticleCollection& prts)
    const
    override
    {

      static uint64_t max_key;
      static uint64_t min_key;

      const uint64_t first = MyPartitioning::partitionStart(omp_get_thread_num(),prts.size(),omp_get_num_threads());
      const uint64_t last  = MyPartitioning::partitionEnd(omp_get_thread_num(),prts.size(),omp_get_num_threads());

      uint64_t _max_key;
      uint64_t _min_key;
      computeKeys(prts, _max_key, _min_key, first, last);

#pragma omp single
      {
        max_key = 0;
        min_key = -1;
      }

#pragma omp critical (TOPOLOGY_KEY_COMPUTE_UPDATE_MINMAX)
      {
        max_key = std::max(_max_key,max_key);
        min_key = std::min(_min_key,min_key);
      }

#pragma omp barrier

#pragma omp single
      prts.setMaxMinKey(max_key,min_key);

    }

  public:


    Topology_Impl(
        const Settings::Dictionary& csysDict,
        const Settings::Dictionary& discretizationDict,
        const Settings::Dictionary& orderingDict,
        const SimulationBox& simBox )
  : coordinate_system_( csysDict )
  , discretization_( discretizationDict, simBox )
  , ordering_( orderingDict )
  {}


    EIGEN_MAKE_ALIGNED_OPERATOR_NEW

  };

} /* namespace Grids */

#endif /* SRC_GRID_TOPOLOGY_IMPL_H_ */
