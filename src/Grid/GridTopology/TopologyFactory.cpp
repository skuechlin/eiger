/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * TopologyFactory.cpp
 *
 *  Created on: Apr 20, 2016
 *      Author: kustepha
 */



#include <string>
#include <map>

#include "TopologyFactory.h"

#include "GridTopology.h"
#include "Topology_Impl.h"

#include "Discratizations.h"
#include "Orderings.h"
#include "CoordinateSystems.h"

#include "Settings/Dictionary.h"

#include "Primitives/MyError.h"
#include "Primitives/MyString.h"




namespace Grids
{

  template< uint8_t ndims, typename csys, typename disc, typename ordering >
  std::unique_ptr<Grids::Topology>
  makeTopology(
      const Settings::Dictionary& csysDict,
      const Settings::Dictionary& discDict,
      const Settings::Dictionary& orderingDict,
      const SimulationBox& simBox ) {
    return std::unique_ptr<Grids::Topology>(
        new class Topology_Impl< ndims, csys, disc, ordering > (
            csysDict, discDict, orderingDict, simBox ) );
  }

  template< uint8_t ndims, typename csys, typename disc>
  struct topologyMakerII{ };

  template<typename csys, typename disc>
  struct topologyMakerII<1,csys,disc>{
    static constexpr char ndims = 1;
    std::unique_ptr<Grids::Topology>
    makeTopology(
        Orderings::Ordering ordering,
        const Settings::Dictionary& csysDict,
        const Settings::Dictionary& discDict,
        const Settings::Dictionary& orderingDict,
        const SimulationBox& simBox ) {
      switch (ordering) {
        case Orderings::Ordering::SEQUENTIAL:
          return Grids::makeTopology<ndims,csys,disc,Orderings::Sequential<ndims>>(
              csysDict,discDict,orderingDict,simBox);
          break;
        default:
          MYASSERT(false,"invalid ordering");
          return 0;
          break;
      }
    }
  };

  template<typename csys, typename disc>
  struct topologyMakerII<2,csys,disc>{
    static constexpr char ndims = 2;
    std::unique_ptr<Grids::Topology>
    makeTopology(
        Orderings::Ordering ordering,
        const Settings::Dictionary& csysDict,
        const Settings::Dictionary& discDict,
        const Settings::Dictionary& orderingDict,
        const SimulationBox& simBox ) {
      switch (ordering) {
        case Orderings::Ordering::MORTON:
          return Grids::makeTopology<ndims,csys,disc,Orderings::Morton<ndims>>(
              csysDict,discDict,orderingDict,simBox);
          break;
        case Orderings::Ordering::HILBERT:
          return Grids::makeTopology<ndims,csys,disc,Orderings::Hilbert<ndims,HilbertLUTs::TwoD>>(
              csysDict,discDict,orderingDict,simBox);
          break;
        default:
          MYASSERT(false,"invalid ordering");
          return 0;
          break;
      }
    }
  };

  template<typename csys, typename disc>
  struct topologyMakerII<3,csys,disc>{
    static constexpr char ndims = 3;
    std::unique_ptr<Grids::Topology>
    makeTopology(
        Orderings::Ordering ordering,
        const Settings::Dictionary& csysDict,
        const Settings::Dictionary& discDict,
        const Settings::Dictionary& orderingDict,
        const SimulationBox& simBox ) {
      switch (ordering) {
        case Orderings::Ordering::MORTON:
          return Grids::makeTopology<ndims,csys,disc,Orderings::Morton<ndims>>(
              csysDict,discDict,orderingDict,simBox);
          break;
        case Orderings::Ordering::HILBERT_ALFA:
          return Grids::makeTopology<ndims,csys,disc,Orderings::Hilbert<ndims,HilbertLUTs::Alfa>>(
              csysDict,discDict,orderingDict,simBox);
          break;
        case Orderings::Ordering::HILBERT_BUTZ:
          return Grids::makeTopology<ndims,csys,disc,Orderings::Hilbert<ndims,HilbertLUTs::Butz>>(
              csysDict,discDict,orderingDict,simBox);
          break;
        case Orderings::Ordering::HILBERT_CHI:
          return Grids::makeTopology<ndims,csys,disc,Orderings::Hilbert<ndims,HilbertLUTs::Ca00_chI>>(
              csysDict,discDict,orderingDict,simBox);
          break;
        case Orderings::Ordering::HILBERT_SASBURG:
          return Grids::makeTopology<ndims,csys,disc,Orderings::Hilbert<ndims,HilbertLUTs::Sasburg>>(
              csysDict,discDict,orderingDict,simBox);
          break;
        default:
          MYASSERT(false,"invalid ordering");
          return 0;
          break;
      }
    }
  };


  template< uint8_t ndims, typename csys>
  std::unique_ptr<Grids::Topology>
  makeTopology(
      Discretizations::Discretization disc,
      Orderings::Ordering ordering,
      const Settings::Dictionary& csysDict,
      const Settings::Dictionary& discDict,
      const Settings::Dictionary& orderingDict,
      const SimulationBox& simBox ) {
    switch (disc) {
      case Discretizations::Discretization::UNIFORM:
        return topologyMakerII<ndims,csys,Discretizations::Uniform<ndims>>().makeTopology(
            ordering,
            csysDict,discDict,orderingDict,simBox);
        break;
      default:
        MYASSERT(false,"invalid discretization");
        return 0; break;
    }
  }

  template<uint8_t ndims>
  struct topologyMaker {

    std::unique_ptr<Grids::Topology>
    makeTopology(
        CoordinateSystems::CoordinateSystem csys,
        Discretizations::Discretization disc,
        Orderings::Ordering ordering,
        const Settings::Dictionary& csysDict,
        const Settings::Dictionary& discDict,
        const Settings::Dictionary& orderingDict,
        const SimulationBox& simBox ) {
      switch (csys) {
        case CoordinateSystems::CoordinateSystem::CARTESIAN:
          return Grids::makeTopology<ndims,CoordinateSystems::Cartesian<ndims>>(
              disc,ordering,
              csysDict,discDict,orderingDict,simBox); break;
        default:
          MYASSERT(false,"invalid coordinate system");
          return 0;
          break;
      }
    }

  };

  template<>
  struct topologyMaker<2> {
    static constexpr char ndims=2;
    std::unique_ptr<Grids::Topology>
    makeTopology(
        CoordinateSystems::CoordinateSystem csys,
        Discretizations::Discretization disc,
        Orderings::Ordering ordering,
        const Settings::Dictionary& csysDict,
        const Settings::Dictionary& discDict,
        const Settings::Dictionary& orderingDict,
        const SimulationBox& simBox ) {
      switch (csys) {
        case CoordinateSystems::CoordinateSystem::CARTESIAN:
          return Grids::makeTopology<ndims,CoordinateSystems::Cartesian<ndims>>(
              disc,ordering,
              csysDict,discDict,orderingDict,simBox); break;
        case CoordinateSystems::CoordinateSystem::AXISYMMETRIC:
          return Grids::makeTopology<ndims,CoordinateSystems::AxiSymmetric<ndims>>(
              disc,ordering,
              csysDict,discDict,orderingDict,simBox); break;
        default:
          MYASSERT(false,"invalid coordinate system");
          return 0;
          break;
      }
    }

  };

  std::unique_ptr<Grids::Topology>
  makeTopology(
      const Settings::Dictionary& dict,
      const SimulationBox& simBox )
      {


    std::unique_ptr<Grids::Topology> t;

    // assert presence of dimension key
    MYASSERT( dict.hasMember("dimension"), "Topology dict missing member \"dimension\"");

    uint8_t ndims = dict.get<uint64_t>("dimension");


    // assert presence of discretization key
    MYASSERT( dict.hasMember("discretization"), "Topology dict missing member \"discretization\"" );

    // evaluate requested discretization
    Settings::Dictionary discDict = dict.get<Settings::Dictionary>("discretization");

    MYASSERT( discDict.hasMember("type"), "discretization dict missing member \"type\"");

    std::string discTypeName = discDict.get<std::string>("type");
    MyString::toLower(discTypeName);

    auto discIt = Discretizations::DiscretizationNames.find(discTypeName);

    // assert valid discretization choice
    MYASSERT( discIt != Discretizations::DiscretizationNames.end(),
              std::string("unknown discretization \"") + discTypeName + std::string("\" requested."));

    Discretizations::Discretization discType = discIt->second;


    // check presence of ordering key, user Morton ordering as default
    std::string orderingName = dict.get<std::string>("ordering","morton");
    MyString::toLower(orderingName);

    auto ordIt = Orderings::OrderingNames.find(orderingName);

    // assert valid ordering choice
    MYASSERT( ordIt != Orderings::OrderingNames.end(),
              std::string("unknown ordering \"") + orderingName + std::string("\" requested."));

    Orderings::Ordering orderingType = ordIt->second;

    Settings::Dictionary orderingDict;


    // check presence of coordinate system key.
    // use Cartesian as default
    std::string csysName = "cartesian";
    Settings::Dictionary csysDict;
    if (dict.hasMember("coordinate system"))
      {
        csysDict = dict.get<Settings::Dictionary>("coordinate system");
        MYASSERT(csysDict.hasMember("type"),"coordinate system dictionary missing entry \"type\"");
        csysName = csysDict.get<std::string>("type");
      }

    MyString::toLower(csysName);

    auto csysIt = CoordinateSystems::CoordinateSystemNames.find(csysName);

    // assert valid coordinate system choice
    MYASSERT( csysIt != CoordinateSystems::CoordinateSystemNames.end(),
              std::string("unknown coordinate system \"") + csysName + std::string("\" requested."));

    CoordinateSystems::CoordinateSystem csysType = csysIt->second;


    // dispatch based on configuration choices
    switch (ndims) {
      case 1: return topologyMaker<1>().makeTopology(
          csysType,discType,orderingType,
          csysDict,discDict,orderingDict,simBox); break;
      case 2: return topologyMaker<2>().makeTopology(
          csysType,discType,orderingType,
          csysDict,discDict,orderingDict,simBox); break;
      case 3: return topologyMaker<3>().makeTopology(
          csysType,discType,orderingType,
          csysDict,discDict,orderingDict,simBox); break;
      default:
        MYASSERT(false,std::string("invalid choice of number of dimensions. Expected 1, 2 or 3, got ") + std::to_string(ndims) );
        return 0; break;
    }

    //    switch (ndims) {
    //      case 1:
    //
    //
    //        switch (csysType) {
    //          case CoordinateSystems::CoordinateSystem::CARTESIAN:
    //
    //
    //            switch (discType) {
    //              case Discretizations::Discretization::UNIFORM:
    //
    //                t = std::unique_ptr<Grids::Topology>(
    //                    new class Topology_Impl<
    //                    1,
    //                    CoordinateSystems::Cartesian<1>,
    //                    Discretizations::Uniform<1>,
    //                    Orderings::Sequential<1> > ( csysDict, discDict, orderingDict, simBox ) );
    //
    //                break;
    //              default:
    //                MYASSERT(false,
    //                         std::string("invalid choice of discretization. Expected \"uniform\", got \"") +
    //                         discTypeName + std::string("\""));
    //                break;
    //            }
    //
    //            break;
    //              default:
    //                MYASSERT(false,
    //                         std::string("invalid choice of coordinate system for 1D simulation. Expected \"cartesian\", got \"") +
    //                         csysName + std::string("\""));
    //                break;
    //        }
    //
    //        break;
    //
    //          case 2:
    //
    //            switch (csysType) {
    //              case CoordinateSystems::CoordinateSystem::CARTESIAN:
    //
    //
    //                switch (discType) {
    //                  case Discretizations::Discretization::UNIFORM:
    //
    //                    switch (orderingType) {
    //                      case Orderings::Ordering::MORTON:
    //
    //                        t = std::unique_ptr<Grids::Topology>(
    //                            new class Topology_Impl<
    //                            2,
    //                            CoordinateSystems::Cartesian<2>,
    //                            Discretizations::Uniform<2>,
    //                            Orderings::Morton<2> > ( csysDict, discDict, orderingDict, simBox ) );
    //
    //                        break;
    //                      case Orderings::Ordering::HILBERT:
    //
    //                        t = std::unique_ptr<Grids::Topology>(
    //                            new class Topology_Impl<
    //                            2,
    //                            CoordinateSystems::Cartesian<2>,
    //                            Discretizations::Uniform<2>,
    //                            Orderings::Hilbert<2,HilbertLUTs::TwoD> > ( csysDict, discDict, orderingDict, simBox ) );
    //
    //                        break;
    //                      default:
    //                        MYASSERT(false,
    //                                 std::string("invalid choice of ordering. Expected \"morton\" or \"hilbert\", got \"") +
    //                                 orderingName + std::string("\""));
    //                        break;
    //                    }
    //
    //                    break;
    //                      default:
    //                        MYASSERT(false,
    //                                 std::string("invalid choice of discretization. Expected \"uniform\", got \"") +
    //                                 discTypeName + std::string("\""));
    //                        break;
    //                }
    //
    //                break;
    //
    //                  case CoordinateSystems::CoordinateSystem::AXISYMMETRIC:
    //
    //                    switch (discType) {
    //                      case Discretizations::Discretization::UNIFORM:
    //
    //                        switch (orderingType) {
    //                          case Orderings::Ordering::MORTON:
    //
    //                            t = std::unique_ptr<Grids::Topology>(
    //                                new class Topology_Impl<
    //                                2,
    //                                CoordinateSystems::AxiSymmetric<2>,
    //                                Discretizations::Uniform<2>,
    //                                Orderings::Morton<2> > ( csysDict, discDict, orderingDict, simBox ) );
    //
    //                            break;
    //                          case Orderings::Ordering::HILBERT:
    //
    //                            t = std::unique_ptr<Grids::Topology>(
    //                                new class Topology_Impl<
    //                                2,
    //                                CoordinateSystems::AxiSymmetric<2>,
    //                                Discretizations::Uniform<2>,
    //                                Orderings::Hilbert<2,HilbertLUTs::TwoD> > ( csysDict, discDict, orderingDict, simBox ) );
    //
    //                            break;
    //                          default:
    //                            MYASSERT(false,
    //                                     std::string("invalid choice of ordering. Expected \"morton\" or \"hilbert\", got \"") +
    //                                     orderingName + std::string("\""));
    //                            break;
    //                        }
    //
    //                        break;
    //                          default:
    //                            MYASSERT(false,
    //                                     std::string("invalid choice of discretization. Expected \"uniform\", got \"") +
    //                                     discTypeName + std::string("\""));
    //                            break;
    //                    }
    //
    //                    break;
    //                      default:
    //                        MYASSERT(false,
    //                                 std::string("invalid choice of coordinate system for 2D simulation.") +
    //                                 std::string("\nExpected \"cartesian\" or \"axisymmetric\", got \"") +
    //                                 csysName + std::string("\""));
    //                        break;
    //            }
    //
    //            break;
    //
    //              case 3:
    //
    //                switch (csysType) {
    //                  case CoordinateSystems::CoordinateSystem::CARTESIAN:
    //
    //
    //                    switch (discType) {
    //                      case Discretizations::Discretization::UNIFORM:
    //
    //                        switch (orderingType) {
    //                          case Orderings::Ordering::MORTON:
    //
    //                            t = std::unique_ptr<Grids::Topology>(
    //                                new class Topology_Impl<
    //                                3,
    //                                CoordinateSystems::Cartesian<3>,
    //                                Discretizations::Uniform<3>,
    //                                Orderings::Morton<3> > ( csysDict, discDict, orderingDict, simBox ) );
    //
    //                            break;
    //                          case Orderings::Ordering::HILBERT_ALFA:
    //
    //                            t = std::unique_ptr<Grids::Topology>(
    //                                new class Topology_Impl<
    //                                3,
    //                                CoordinateSystems::Cartesian<3>,
    //                                Discretizations::Uniform<3>,
    //                                Orderings::Hilbert<3,HilbertLUTs::Alfa> > ( csysDict, discDict, orderingDict, simBox ) );
    //
    //                            break;
    //                          case Orderings::Ordering::HILBERT_BUTZ:
    //
    //                            t = std::unique_ptr<Grids::Topology>(
    //                                new class Topology_Impl<
    //                                3,
    //                                CoordinateSystems::Cartesian<3>,
    //                                Discretizations::Uniform<3>,
    //                                Orderings::Hilbert<3,HilbertLUTs::Butz> > ( csysDict, discDict, orderingDict, simBox ) );
    //
    //                            break;
    //                          case Orderings::Ordering::HILBERT_CHI:
    //
    //                            t = std::unique_ptr<Grids::Topology>(
    //                                new class Topology_Impl<
    //                                3,
    //                                CoordinateSystems::Cartesian<3>,
    //                                Discretizations::Uniform<3>,
    //                                Orderings::Hilbert<3,HilbertLUTs::Ca00_chI> > ( csysDict, discDict, orderingDict, simBox ) );
    //
    //                            break;
    //                          case Orderings::Ordering::HILBERT_SASBURG:
    //
    //                            t = std::unique_ptr<Grids::Topology>(
    //                                new class Topology_Impl<
    //                                3,
    //                                CoordinateSystems::Cartesian<3>,
    //                                Discretizations::Uniform<3>,
    //                                Orderings::Hilbert<3,HilbertLUTs::Sasburg> > ( csysDict, discDict, orderingDict, simBox ) );
    //
    //                            break;
    //                          default:
    //                            MYASSERT(false,
    //                                     std::string("invalid choice of ordering. Expected \"morton\" or \"hilbert alfa\", \"hilbert butz\" or \"hilbert chI\", got \"") +
    //                                     orderingName + std::string("\""));
    //                            break;
    //                        }
    //
    //
    //                        break;
    //                          default:
    //                            MYASSERT(false,
    //                                     std::string("invalid choice of discretization. Expected \"uniform\", got \"") +
    //                                     discTypeName + std::string("\""));
    //                            break;
    //                    }
    //
    //                    break;
    //                      default:
    //                        MYASSERT(false,
    //                                 std::string("invalid choice of coordinate system for 3D simulation. Expected \"cartesian\", got \"") +
    //                                 csysName + std::string("\""));
    //                        break;
    //                }
    //
    //                break;
    //
    //                  default:
    //                    MYASSERT(false,std::string("invalid choice of number of dimensions. Expected 1, 2 or 3, got ") + std::to_string(ndims) );
    //                    break;
    //    }
    //
    //
    //
    //    return t;
    //
    //
    //      }


      }

} // namepace
