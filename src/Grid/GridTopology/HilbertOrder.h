/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * HilbertOrder.h
 *
 *  Created on: Jan 10, 2017
 *      Author: kustepha
 */

#ifndef SRC_GRID_GRIDTOPOLOGY_HILBERTORDER_H_
#define SRC_GRID_GRIDTOPOLOGY_HILBERTORDER_H_


#include "Primitives/MyArithm.h"

#include "Grid/GridForwardDecls.h"

#include "MortonOrder.h"

#include "SFC/HilbertCurves.h"

namespace Settings
{
  class Dictionary;
}

namespace Grids
{

  namespace Orderings
  {

    template<uint8_t nd, typename Curve>
    class Hilbert
    {
        static_assert(nd == Curve::nd,"dimension of Hilbert ordering must match dimension of curve");

        typedef HilbertCurves::Hilbert<Curve> CurveT;

        static constexpr uint8_t nbits = nd;

      public:

        static inline uint64_t coords2Key(const v4du& coords) {
          return CurveT::morton2hilbert( Morton<nd>::coords2Key(coords) ); };

        static inline v4du key2Coords(const uint64_t key) {
          return Morton<nd>::key2Coords( CurveT::hilbert2morton(key) ); };

        static inline bool inCuboid(const uint64_t key, const uint64_t limit) {
          return Morton<nd>::inCuboid(
              CurveT::hilbert2morton(key),
              CurveT::hilbert2morton(limit) ); }

        static inline uint64_t maxKey(const v4du& coord_lims) {
          uint64_t maxp2 = 0;
          for (uint8_t i = 0; i < nd; ++i)
            maxp2 = std::max(maxp2,MyArithm::nextPow2(coord_lims[i]));
          v4du coord_limsp2m1;
          for (uint8_t i = 0; i < nd; ++i)
            coord_limsp2m1[i] = maxp2-1;
          return Morton<nd>::coords2Key(coord_limsp2m1); }

        Hilbert(const Settings::Dictionary& ) {};

    };

  } /* namespace Orderings */

} /* namespace Grids */



#endif /* SRC_GRID_GRIDTOPOLOGY_HILBERTORDER_H_ */
