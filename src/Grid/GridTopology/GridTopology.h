/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * GridTopology.h
 *
 *  Created on: Nov 26, 2014
 *      Author: kustepha
 */

#ifndef GRID_GRIDTOPOLOGY_H_
#define GRID_GRIDTOPOLOGY_H_

#include <memory>
#include <vector>

#include <Eigen/Dense>

#include "Grid/GridForwardDecls.h"

namespace Particles
{
  class ParticleCollection;
}

namespace Settings
{
  class Dictionary;
}

namespace MyRandom
{
  class Rng;
}

namespace Grids
{

  class Grid;
  class RegularGrid;

  class Topology
  {
    friend class Grid;
    friend class RegularGrid;

  public:

    // number of active dimensions (1,2 or 3)
    virtual uint8_t ndims() const = 0;

    // axisymmetric topology tf?
    virtual bool axisymmetric() const = 0;

    // the maximum aggregation level guaranteed to be supported on given discretization
    virtual uint8_t maxLevel() const = 0;

    // the largest possible key value
    virtual uint64_t maxKey() const = 0;

    // the number of cells at level "low_level" per cell at level "high_level"
    // returns 0 if low_level > high_level
    virtual uint64_t nCellsPerCell(const uint8_t low_level, const uint8_t high_level) const = 0;

    virtual uint64_t nFacesPerFace(const uint8_t high_level, const uint8_t low_level) const = 0;

    // the number of neighboring edges of cells at level "low_level" per edge of cell at level "high_level"
    // returns 1 if low_level > high_level
    virtual uint64_t nEdgesPerEdge(const uint8_t high_level, const uint8_t low_level) const = 0;

    // the total number of cells on given level
    virtual uint64_t nCellsTot(const uint8_t level) const = 0;

    // bounding box of cell with key "key" and level "level"
    virtual box4_t cellBox(const uint64_t key, const uint8_t level) const = 0;

  private:


    // bounding box of entire grid
    virtual box4_t boundingBox() const = 0;

    // a random position in cell with key "key" and level "level"
    virtual v4df sample(const uint64_t key, const uint8_t level, MyRandom::Rng& rndGen) const = 0;

    // volume of cell with key "key" and level "level"
    virtual double cellVol(const uint64_t key, const uint8_t level) const = 0;

    // position of cell center with key "key" and level "level"
    virtual v4df cellCenter(const uint64_t key, const uint8_t level) const = 0;

    // position of center of sub cell si within cell with key "key" and level "level",
    // based on division into ns sub-cells per dimension
    virtual v4df subCellCenter(
        const uint64_t key,
        const uint8_t level,
        const uint64_t si,
        const uint64_t ns)
    const = 0;


    // returns key of neighbor with level "neighbor_level"
    // of the cell with key "key" and level "level"
    // in direction "dir_skji" (from most significant bit: sign,k,j,i)
    virtual
    uint64_t
    neighborKey(
        const uint64_t key,
        const uint8_t level,
        const uint8_t neighbor_level,
        const uint8_t dir_skji)
    const = 0;

  public:

    // fills neighbor_keys with nFacesPerFace(level,neighbor_level)
    // keys of neighbors of cell with key key on face face
    virtual
    void
    faceNeighborKeys(
        uint64_t* const neighbor_keys,
        const uint64_t key,
        const uint8_t level,
        const uint8_t neighbor_level,
        const uint8_t face)
    const = 0;


  private:

    // fills key vector keys with keys specifying n cells at level level
    // with first key equal to start
    virtual
    void
    generateRegularKeys(
        std::vector<uint64_t>& keys,
        const uint64_t start,
        const uint64_t n,
        const uint8_t level)
    const = 0;

    // fills key vector keys with keys specifying n cells, s.t. cells cover the entire space
    // and boundaries fall on level level
    virtual
    void
    generateRegularKeys(
        std::vector<uint64_t>& keys,
        const uint64_t n,
        const uint8_t level)
    const = 0;


    // adds to key vector keys the keys of all neighboring cells at level level still in domain
    // returns in first and last index of first non-ghost
    // cell key and first ghost cell key after non-ghost, respectively
    virtual
    void
    generateGhostCellKeys(
        uint64_t& first,
        uint64_t& last,
        std::vector<uint64_t>& keys,
        const uint8_t level)
    const = 0;


    // return position encoded by key
    virtual
    v4df
    key2Pos(const uint64_t key)
    const = 0;

  public:

    // return coordinates encoded by key
    virtual
    v4du
    key2Coords(const uint64_t key)
    const = 0;


  private:

    // return key of vertex vrt_kji (from most significant bit: k,j,i) in cell with key "key" and level "level"
    virtual
    uint64_t
    vrtKey(const uint64_t key, const uint8_t level, const uint8_t vrt_kji)
    const = 0;


  public:

    // return key belonging to position
    virtual
    uint64_t
    pos2Key(const v4df& pos)
    const = 0;

    // update all particle keys,
    // set keys of particles to remove to -1
    virtual void
    computeKeys(Particles::ParticleCollection& prts,
                const uint64_t first, const uint64_t last) const = 0;

    // update all particle keys, return maximum and minimum key (all valid keys)
    // set keys of particles to remove to -1
    virtual void
    computeKeys(Particles::ParticleCollection& prts, uint64_t& max_key, uint64_t& min_key,
                const uint64_t first, const uint64_t last) const = 0;

    // update all particle keys, update maximum and minimum key (all valid keys) in particle collection
    // set keys of particles to remove to -1
    virtual void computeKeys(Particles::ParticleCollection& prts) const = 0;

  public:

    virtual ~Topology (){}
  };

} /* namespace Grids */

#endif /* GRID_GRIDTOPOLOGY_H_ */
