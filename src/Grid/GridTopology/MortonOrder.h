/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * MortonOrder.h
 *
 *  Created on: Jan 10, 2017
 *      Author: kustepha
 */

#ifndef SRC_GRID_GRIDTOPOLOGY_MORTONORDER_H_
#define SRC_GRID_GRIDTOPOLOGY_MORTONORDER_H_


#include "SFC/MortonCodes.h"

#include "Primitives/MyArithm.h"

#include "Grid/GridForwardDecls.h"

namespace Settings
{
  class Dictionary;
}

namespace Grids
{

  namespace Orderings
  {

    template<uint8_t ndims>
    class Morton
    {};

    template<>
    class Morton<2>
    {
      public:

        static uint64_t
        coords2Key(const v4du& coords) {
          return MortonCodes::encode(coords[0],coords[1]); };

        static v4du
        key2Coords(const uint64_t key) {
          return v4du{MortonCodes::decode2i(key),MortonCodes::decode2j(key),0,0}; }

        static bool
        inCuboid(const uint64_t key, const uint64_t limit) {
          static constexpr uint64_t i_mask = 0xAAAAAAAAAAAAAAAA;
          static constexpr uint64_t j_mask = (i_mask >> 1);
          return ( (key & i_mask) < (limit & i_mask) )
              && ( (key & j_mask) < (limit & j_mask) );
        }

        static uint64_t
        maxKey(const v4du& coord_lims) { return coords2Key( coord_lims ); }

        Morton(const Settings::Dictionary& ) {};

    };

    template<>
    class Morton<3>
    {
      public:

        static uint64_t
        coords2Key(const v4du& coords) {
          return MortonCodes::encode(coords[0],coords[1],coords[2]); };

        static v4du
        key2Coords(const uint64_t key) {
          return v4du{MortonCodes::decode3i(key),MortonCodes::decode3j(key),MortonCodes::decode3k(key),0}; }

        static bool
        inCuboid(const uint64_t key, const uint64_t limit) {
          static constexpr uint64_t i_mask = 0x4924924924924924;
          static constexpr uint64_t j_mask = (i_mask >> 1);
          static constexpr uint64_t k_mask = (j_mask >> 1);

          return ( (key & i_mask) < (limit & i_mask) )
              && ( (key & j_mask) < (limit & j_mask) )
              && ( (key & k_mask) < (limit & k_mask) ); }

        static uint64_t
        maxKey(const v4du& coord_lims) { return coords2Key( coord_lims ); }

        Morton(const Settings::Dictionary& ) {};

    };

  } /* namespace Orderings */

} /* namespace Grids */


#endif /* SRC_GRID_GRIDTOPOLOGY_MORTONORDER_H_ */
