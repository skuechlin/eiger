/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * CoordinateSystems.cpp
 *
 *  Created on: Apr 7, 2016
 *      Author: kustepha
 */


#include <omp.h>

#include "CoordinateSystems.h"

#include "Primitives/MyRandom.h"
#include "Primitives/MyIntrin.h"

#include "Particles/ParticleCollection.h"


namespace Grids
{

  namespace CoordinateSystems
  {


    template<uint8_t nd> v4df
    Cartesian<nd>::sample(const v4df& lower, const v4df& upper, MyRandom::Rng& rndGen)
    const {
      v4df R{0.,0.,0.,0.};

      for (uint8_t i = 0; i < nd; ++i)
        R[i] = rndGen.uniform();

      return lower + R*( upper - lower );
    }


    template class Cartesian<uint8_t(1)>;
    template class Cartesian<uint8_t(2)>;
    template class Cartesian<uint8_t(3)>;

    // a random position in the cell
    v4df
    AxiSymmetric<uint8_t(2)>::sample(const v4df& lower, const v4df& upper, MyRandom::Rng& rndGen)
    const
    {

      v4df diff( upper - lower );

      double x = lower[0] + rndGen.uniform()*diff[0];
      double r = lower[1] + sqrt(rndGen.uniform())*diff[1];
      return v4df{x,r,0.,0.};

//      double theta = 2.*Constants::pi*rndGen.uniform();
//
//      return v4df{x, r*cos(theta), r*sin(theta), 0.0 };

    }





  }



}
