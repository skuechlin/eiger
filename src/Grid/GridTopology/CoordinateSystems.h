/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * CoordinateSystems.h
 *
 *  Created on: Apr 7, 2016
 *      Author: kustepha
 */

#ifndef SRC_GRID_COORDINATESYSTEMS_H_
#define SRC_GRID_COORDINATESYSTEMS_H_

#include <map>

#include "Grid/GridForwardDecls.h"

#include "Constants/Constants.h"



namespace MyRandom
{
  class Rng;
}

namespace Particles
{
  class Particle;
}

namespace Settings
{
  class Dictionary;
}

namespace Grids
{

  namespace CoordinateSystems
  {

    enum CoordinateSystem
    {
      CARTESIAN,
      AXISYMMETRIC
    };

    static std::map<std::string,CoordinateSystem> CoordinateSystemNames =
        {
            {"cartesian",CARTESIAN},
            {"axisymmetric",AXISYMMETRIC}
        };


    template<uint8_t ndims>
    class Cartesian {
    public:

      static constexpr bool axisymmetric() { return false; }

      v4df sample(const v4df& lower, const v4df& upper, MyRandom::Rng& rndGen) const;

      double cellVol(const v4df& lower, const v4df& upper) const {
        v4df sz( upper - lower );
        return MyIntrin::sprod<ndims>(sz);  }

      Cartesian(const Settings::Dictionary& dict) { static_cast<void>(dict); }
    };


    template<uint8_t ndims>  class AxiSymmetric;

    template<>
    class AxiSymmetric<2>
    {
      public:

      static constexpr bool axisymmetric() { return true; }

      // sample in Cartesian coordinates
      v4df sample(const v4df& lower, const v4df& upper, MyRandom::Rng& rndGen) const;

      double cellVol(const v4df& lower, const v4df& upper) const {
        const v4df sz( upper - lower );
        return 2.0*Constants::pi*sz[0]*sz[1]; }

      AxiSymmetric(const Settings::Dictionary& dict) {
        static_cast<void>(dict); }

    };

  }

}


#endif /* SRC_GRID_COORDINATESYSTEMS_H_ */
