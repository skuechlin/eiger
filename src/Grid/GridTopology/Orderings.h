/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * Orderings.h
 *
 *  Created on: Mar 8, 2016
 *      Author: kustepha
 */

#ifndef SRC_GRID_ORDERINGS_H_
#define SRC_GRID_ORDERINGS_H_

#include "MortonOrder.h"
#include "HilbertOrder.h"

namespace Grids
{

  namespace Orderings
  {

    enum Ordering
    {
      SEQUENTIAL,
      MORTON,
      HILBERT,
      HILBERT_ALFA,
      HILBERT_BUTZ,
      HILBERT_CHI,
      HILBERT_SASBURG
    };

    static std::map<std::string,Ordering> OrderingNames =
    {
        {"sequential",SEQUENTIAL},
        {"morton",MORTON},
        {"hilbert",HILBERT},
        {"hilbert alfa",HILBERT_ALFA},
        {"hilbert butz",HILBERT_BUTZ},
        {"hilbert chi",HILBERT_CHI},
        {"hilbert sasburg",HILBERT_SASBURG}
    };


    template<uint8_t ndims>
    class Sequential
    {};

    template<>
    class Sequential<1>
    {
      public:

        static uint64_t
        coords2Key(const v4du& coords) { return coords[0]; };

        static v4du
        key2Coords(const uint64_t key) { return v4du{key,0,0,0}; }

        static bool
        inCuboid(const uint64_t key, const uint64_t limit) { return key < limit; }

        static uint64_t
        maxKey(const v4du& coord_lims) { return coords2Key( coord_lims ); }

        Sequential(const Settings::Dictionary& ) {};

    };



  } /* namespace Orderings */

} /* namespace Grids */

#endif /* SRC_GRID_ORDERINGS_H_ */
