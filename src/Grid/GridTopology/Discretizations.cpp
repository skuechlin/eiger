/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * Discretizations.cpp
 *
 *  Created on: Apr 21, 2016
 *      Author: kustepha
 */


#include <iostream>

#include "Discratizations.h"

#include "Settings/Dictionary.h"

#include "SimulationBox/SimulationBox.h"

#include "Primitives/MyError.h"


namespace
{

  uint64_t
  getNi(const Settings::Dictionary& dict)
  {
    return dict.get<uint64_t>("ni");
  }

  uint64_t
  getNj(const Settings::Dictionary& dict)
  {
    return dict.get<uint64_t>("nj");
  }

  uint64_t
  getNk(const Settings::Dictionary& dict)
  {
    return dict.get<uint64_t>("nk");
  }


  template<uint8_t ndims>
  v4du
  getN(const Settings::Dictionary& dict, const uint8_t level)
  {
    uint64_t fac = ( uint64_t(1) << level );

    switch (ndims) {
      case 1:
        return v4du{getNi(dict)*fac,1,1,0};
        break;
      case 2:
        return v4du{getNi(dict)*fac,getNj(dict)*fac,1,0};
        break;
      case 3:
        return v4du{getNi(dict)*fac,getNj(dict)*fac,getNk(dict)*fac,0};
        break;
      default:
        static_assert(ndims > 0 && ndims < 4,"getN only for 0 < ndims < 4 ");
        break;
    }
  }


} // namespace



namespace Grids
{

  namespace Discretizations
  {


    template<uint8_t ndims>
    Uniform<ndims>::Uniform(
        const Settings::Dictionary& dict,
        const SimulationBox& simBox  )
        : origin_( *(v4df*)(&simBox.origin()) )
          , n_( getN<ndims>(dict,dict.get<uint8_t>("level",0)) )
          , h_(
              v4df{
      simBox.extent().coeffRef(0) / static_cast<double>(n_[0] > 0 ? n_[0] : 1),
          simBox.extent().coeffRef(1) / static_cast<double>(n_[1] > 0 ? n_[1] : 1),
          simBox.extent().coeffRef(2) / static_cast<double>(n_[2] > 0 ? n_[2] : 1),
          0. } )
          , d_( h_ > 0. ? 1./h_ : 0. )
          , max_level_( dict.get<uint8_t>("level",0) )
          {


      std::string msg = "Uniform discretization construction failed: ";

      MYASSERT((uint64_t)n_[0] > 0, msg + "number of cells in i direction must be > 0" );
      MYASSERT((uint64_t)n_[1] > 0, msg + "number of cells in j direction must be > 0" );
      MYASSERT((uint64_t)n_[2] > 0, msg + "number of cells in k direction must be > 0" );

      MYASSERT((double)h_[0] > 0., msg + "cell width in i direction must be > 0.0" );
      if (ndims > 1)
        MYASSERT((double)h_[1] > 0., msg + "cell width in j direction must be > 0.0" );
      if (ndims > 2)
        MYASSERT((double)h_[2] > 0., msg + "cell width in k direction must be > 0.0" );

      // DEBUG
      //      std::cout << "Uniform<ndims>::Uniform( const Settings::Dictionary& dict, const SimulationBox& simBox  )" << std::endl;
      //      std::cout << std::to_string(max_level_) << std::endl;
      //      std::cout << dict.get<uint64_t>("level",0) << std::endl;
      //      std::cout << std::to_string( dict.get<uint_fast8_t>("level") ) << std::endl;
      //      std::cout << "o: " << origin_.transpose(); std::cout << std::endl;
      //      std::cout << "n: " << n_.transpose(); std::cout << std::endl;
      //      std::cout << "h: " << h_.transpose(); std::cout << std::endl;
      //      std::cout << "d: " << d_.transpose(); std::cout << std::endl;



          }



    // instantiations
    template
    Uniform<uint8_t(1)>::Uniform(const Settings::Dictionary& dict,
                                 const SimulationBox& simBox);

    template
    Uniform<uint8_t(2)>::Uniform(const Settings::Dictionary& dict,
                                 const SimulationBox& simBox);

    template
    Uniform<uint8_t(3)>::Uniform(const Settings::Dictionary& dict,
                                 const SimulationBox& simBox);


  } // namespace Discretizations



} // namespace Grids

