/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * Cell.cpp
 *
 *  Created on: Oct 14, 2014
 *      Author: kustepha
 */



#include <Grid/CellData.h>
#include <string>



uint64_t
CellData::nVars()
{
  return 42;
}


double
CellData::getVar(const uint64_t i)
const
{
  switch (i) {
    case  0 : return v<0>();    break;
    case  1 : return v<1>();    break;
    case  2 : return v<2>();    break;
    case  3 : return Ma();      break;
    case  4 : return p();	    break;
    case  5 : return T();	    break;
    case  6 : return n(); 	    break;
    case  7 : return rho();	    break;
    case  8 : return p<0,0>();        break;
    case  9 : return p<0,1>();        break;
    case 10 : return p<0,2>();        break;
    case 11 : return p<1,1>();        break;
    case 12 : return p<1,2>();        break;
    case 13 : return p<2,2>();        break;
    case 14 : return q<0>();          break;
    case 15 : return q<1>();          break;
    case 16 : return q<2>();          break;
    case 17 : return mfp();           break;
    case 18 : return cos.fp_dsmc_fraction; 	break;
    case 19 : return cos.num_collisions;    break;
    case 20 : return Np();       break;
    case 21 : return Nptot();    break;
    case 22 : return ist_.T_rot;        break;
    case 23 : return ist_.T_vib;        break;
    case 24 : return drhodx<0>();        break;
    case 25 : return drhodx<1>();        break;
    case 26 : return drhodx<2>();        break;
    case 27 : return dthetadx<0>();      break;
    case 28 : return dthetadx<1>();      break;
    case 29 : return dthetadx<2>();      break;
    case 30 : return dvdx<0,0>();        break;
    case 31 : return dvdx<0,1>();        break;
    case 32 : return dvdx<0,2>();        break;
    case 33 : return dvdx<1,0>();        break;
    case 34 : return dvdx<1,1>();        break;
    case 35 : return dvdx<1,2>();        break;
    case 36 : return dvdx<2,0>();        break;
    case 37 : return dvdx<2,1>();        break;
    case 38 : return dvdx<2,2>();        break;
    case 39 : return dedx<0>();   break;
    case 40 : return dedx<1>();   break;
    case 41 : return dedx<2>();   break;
    default: return 0.12345689;
  }
}


std::string
CellData::getVarName(const uint64_t i)
{
  static constexpr char varnames[][32] = {
      "U",     "V",     "W",       "Ma",      "p",          // 0- 4
      "T",     "n",     "rho",     "pxx",     "pxy",        // 5- 9
      "pxz",   "pyy",   "pyz",     "pzz",     "qx",         //10-14
      "qy",    "qz",    "mfp",     "nfp",     "ncoll",      //15-19
      "Np",    "Nptot", "Trot",    "Tvib",    "drhodx",     //20-24
      "drhody","drhodz","dthetadx","dthetady","dthetadz",   //25-29
      "dudx",  "dudy",  "dudz",    "dvdx",    "dvdy",       //30-34
      "dvdz",  "dwdx",  "dwdy",    "dwdz",    "dedx",       //35-39
      "dedy",  "dedz"                                       //40-44
  };

  return varnames[i];
}

uint64_t
CellData::getVarInd(const std::string& varName)
{
  for (uint64_t iVar = 0; iVar<CellData::nVars();++iVar) {
      if (CellData::getVarName(iVar).compare(varName) == 0)
        return iVar;
  }
  return uint64_t(-1);
}
