/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * Grid.h
 *
 *  Created on: Jun 19, 2014
 *      Author: Stephan Kuechlin
 */

#ifndef GRID_H_
#define GRID_H_

#include <mpi.h> // MPI_Datatype declaration
#include <omp.h>
#include <iostream>
#include <vector>
#include <memory> // unique_ptr to Output object
#include <numeric>

#include "GridForwardDecls.h"

#include "GridTopology/GridTopology.h"

#include "Constants/Constants.h" // minCellVol

#include "Primitives/MyArithm.h" // binary find
#include "Primitives/MyError.h"


namespace MyMPI { class MPIMgr; }
namespace Settings { class Dictionary; }

namespace Grids
{

  class DataGrid;

  class Grid
  {

    // a Grid
    // - stores a reference to the relevant topology
    // - stores the key of each cell and additionally a guard value at last position
    // - stores the number of particles in each cell
    // - stores the particle array index of the first particle in each cell

  protected:

    const Topology* const topology_;

    const std::string name_;

    uint64_t change_count_ = 0; // to be incremented on change of grid

  public:
    // index of first non-ghost cell
    uint64_t first_;

    // index of first ghost cell after non-ghost
    uint64_t last_;


  public:

    // vector of cell keys
    //
    // example: 3 cells, keys_per_cell 4
    //
    // cell#     0    1    2
    //        |----|----|----|
    // idx    0    1    2    3     4
    // key_   0    4    8   12   (-1)
    std::vector<uint64_t> key_;

    // particle information, including guard cell

    // count_[i] holds the number of particles in cell i
    // includes guard cell value
    std::vector<uint64_t> count_;

    // begin_[i] holds the index of the first particle in cell i
    // includes guard cell value
    std::vector<uint64_t> begin_;

    // load balanced first cell index for each thread for non-ghost cell iteration
    std::vector<uint64_t> thread_lb_first_;

  public:
    uint64_t thread_first() const {
      return thread_lb_first_.at(omp_get_thread_num());
    }

    uint64_t thread_last() const {
      return thread_lb_first_.at(omp_get_thread_num()+1);
    }

    void lb_nonGhost_on_threads();

  public:

    virtual void print() const;

  public:

    // reset the particle count array
    void resetCount() { count_.assign(count_.size(),0); }

    // reset the begin array
    void resetBegin() { begin_.assign(begin_.size(),0); }

  public:

    bool check() const
    {
      bool pass = true;
      const uint64_t sz = count_.size();

      uint64_t last_begin = begin_[0];
      uint64_t last_count = count_[0];

      pass = pass && last_begin == 0;

      for (uint64_t i = 1; i < sz; ++i)
        {
          const uint64_t begin = begin_[i];
          const uint64_t count = count_[i];
          pass = pass && (begin == last_begin + last_count);
          pass = pass && (begin >= last_begin);
          last_begin = begin;
          last_count = count;
        }

      return pass;
    }


  public:
    uint64_t cellKey(const uint64_t i) const {return key_[i]; }

  public:


    const std::string& name() const { return name_;  }

    const Grids::Topology* topology() const { return topology_; }

    uint64_t changeCount() const { return change_count_; }
    void countChange() { ++change_count_; }

    // returns the number of cells,
    // INCLUDING ghost cells
    // NOT counting guard cell
    uint64_t nCells() const { return key_.size()-2; }

    // returns number of non-ghost cells
    uint64_t
    nNonGhost() const { return last_-first_; }

    // returns number of ghost cells
    uint64_t nGhost() const { return nCells() - nNonGhost(); }

    // return the combined array index of ghost cell iGhost
    uint64_t
    ghostIdx2CellIdx(const uint64_t iGhost)
    const
    {
      if ( iGhost < first_)
        return iGhost;
      else
        return iGhost + nNonGhost();
    }

    // map combined array index i to ghost / regular cell range
    uint64_t
    iCell(const uint64_t i)
    const
    {
      if (i < first_) // ghost
        return i;
      else if (i < last_) // non-ghost
        return i - first_;
      else // i >= last_
        return i - nNonGhost(); // ghost
    }

  public:

    // returns index of cell containing key, or uint64_t(-1) if not in any cell on grid
    virtual uint64_t locateKey(const uint64_t key) const
    {
      if ( key >= *(key_.end()-2) )
        return -1;

      uint64_t i = std::distance( key_.begin(),
                                  std::lower_bound(key_.begin(),key_.end(),key) );

      if (key_[i] == key)
        return i;
      else
        return i-1; // note: i == 0 produces desired result

    }


  protected:

    Grid(
        const Topology* tp,
        const std::string& name)
  : topology_(tp)
  , name_(name)
  , first_(0)
  , last_(0)
  {}

  public:


    // generates a grid that will partition the entire discretization into
    // n cells, with each cells containing an integral multiple of finest possible cells
    // implied by level
    Grid(
        const Topology* tp,
        const std::string& name,
        const uint64_t n,
        const uint8_t level)
  : Grid(tp,name)
  {
      topology_->generateRegularKeys(
          key_,
          n,
          level );

      // size of count and begin is one less than that of key_, since
      // key_ has guard value at last element
      count_.assign(key_.size()-1,0);
      begin_.assign(key_.size()-1,0);

      first_ = 0;
      last_ = key_.size() - 2; // range of regular cells does not include the guard cell

      countChange();

  }

    virtual ~Grid() = default;

  };

} // namespace Grids



#endif /* GRID_H_ */
