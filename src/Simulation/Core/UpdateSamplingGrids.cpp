/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * SimulationCoreSampling.cpp
 *
 *  Created on: Mar 11, 2015
 *      Author: ?
 */






#include <Grid/CellData.h>
#include "SimulationCore.h"

#include "Primitives/MyIntrin.h"

#include "Grid/GridManager/GridManager.h"

#include "Parallel/MyMPI.h"

#include "Gas/GasModel.h"

#include "Moments/AveragingStrategies.h"

#include "VelocityUpdate/VelocityUpdateStrategy.h"

void SimulationCore::updateGridsAndVelocities(const uint64_t tn)
{

  // the particle grid
  Grids::RegularGrid *const pg = gridMgr()->particleGrid();

  // the simulation grid
  Grids::DataGrid *const sg = gridMgr()->simulationGrid();

  // cell range
//#define SAMPLING_OMP_SCHEDULE
#ifndef SAMPLING_OMP_SCHEDULE

  pg->lb_nonGhost_on_threads();
  const uint64_t first = pg->thread_first();
  const uint64_t last = pg->thread_last();

#else
  const uint64_t first = pg->first_;    // exclude ghost
  const uint64_t last = pg->last_;      // exclude ghost
#endif

  const bool diat = gasModel()->diatomic();

  // thread local rng
  MyRandom::Rng rndGen(rndGen_);


#ifdef SAMPLING_OMP_SCHEDULE
#pragma omp for schedule(static)
#endif
  for ( uint64_t i = first; i < last; ++i)
    {
      // current particle range
      const uint64_t N = pg->count_[i];
      const double Nd  = static_cast<double>(N);

      Particles::Particle* pfirst = prts_.getParticle( pg->begin_[i] );
      Particles::Particle* plast  = pfirst + N;

      // sample moments
      Moments::VelocityMoments vlm;
      switch (pg->topology()->ndims()) {
        case 1: vlm.sample<1,Moments::VelocityMoments::SampleType::NAIVE>(pfirst,plast,pg->cellBox(i));   break;
        case 2: vlm.sample<2,Moments::VelocityMoments::SampleType::NAIVE>(pfirst,plast,pg->cellBox(i));   break;
        case 3: vlm.sample<3,Moments::VelocityMoments::SampleType::NAIVE>(pfirst,plast,pg->cellBox(i));   break;
      }

      Moments::InternalMoments im;
      if( diat ) im.sample(pfirst,plast);

      for (auto g = gridMgr()->dataGrids_.begin(); g != gridMgr()->dataGrids_.end(); ++g )
        g->cell(i).update(vlm,im,Nd,Moments::Averaging::Averager(g->alpha(tn)),gasModel());


#ifdef checkconserved

      v4df cmpre   = zero<v4df>();
      v4df cmpost  = zero<v4df>();
      v4df ccmpre  = zero<v4df>();
      v4df ccmpost = zero<v4df>();

      v4df dmpre   = zero<v4df>();
      v4df dmpost  = zero<v4df>();
      v4df cdmpre  = zero<v4df>();
      v4df cdmpost = zero<v4df>();

      double evpre   = 0.;
      double evpost  = 0.;
      double cevpre  = 0.;
      double cevpost = 0.;

      double edpre   = 0.;
      double edpost  = 0.;
      double cedpre  = 0.;
      double cedpost = 0.;

      for (auto p = pfirst; p!= plast; ++p) {
          MyArithm::sum_kahan(cmpre,ccmpre,p->C);
          MyArithm::sum_kahan(dmpre,cdmpre,p->D);
          MyArithm::sum_kahan(evpre,cevpre,MyIntrin::sdot3(p->C,p->C));
          MyArithm::sum_kahan(edpre,cedpre,MyIntrin::sdot3(p->D,p->D));
      }
#endif

      // compute local time step
      const double dt = dt_ref() * weight_factor(pg->cellLevel(i));

      // set particle time steps
      for (auto p = pfirst; p!= plast; ++p) {
          p->dt = dt;
      }

      // update particle velocities
      const auto cs = updateVelocities(
          pfirst,
          plast,
          sg->cell(i),
          dt,
          rndGen );

      // update collision statistics
      for (auto g = gridMgr()->dataGrids_.begin(); g != gridMgr()->dataGrids_.end(); ++g )
        g->cell(i).update(cs,Moments::Averaging::Averager(g->alpha(tn)));


#ifdef checkconserved
          for (auto p = pfirst; p!= plast; ++p) {
              MyArithm::sum_kahan(cmpost,ccmpost,p->C);
              MyArithm::sum_kahan(dmpost,cdmpost,p->D);
              MyArithm::sum_kahan(evpost,cevpost,MyIntrin::sdot3(p->C,p->C));
              MyArithm::sum_kahan(edpost,cedpost,MyIntrin::sdot3(p->D,p->D));
          }
#endif



#ifdef checkconserved

      MYWARN(fabs(MyIntrin::sdot(cmpost,cmpost) -  MyIntrin::sdot(cmpre,cmpre)) < 1.e-7*Nd*Nd,
             "momentum conservation: " + std::to_string(MyIntrin::sdot(cmpost,cmpost)/MyIntrin::sdot(cmpre,cmpre)) );

      MYWARN(fabs(MyIntrin::sdot(dmpost,dmpost) - MyIntrin::sdot(dmpre,dmpre)) < 1.e-7*Nd*Nd,
             "momentum conservation: " + std::to_string(MyIntrin::sdot(dmpost,dmpost)/MyIntrin::sdot(dmpre,dmpre)) );

      MYWARN(fabs(evpost - evpre) < 1.e-9*Nd*Nd,
             "energy conservation: " + std::to_string(evpost/evpre));

      MYWARN(fabs(edpost - edpre) < 1.e-9*Nd*Nd,
             "energy conservation: " + std::to_string(edpost/edpre));

#undef checkconserved
#endif



    } // for all cells

#ifndef SAMPLING_OMP_SCHEDULE
#pragma omp barrier
#endif

#ifdef SAMPLING_OMP_SCHEDULE
#undef SAMPLING_OMP_SCHEDULE
#endif

}

