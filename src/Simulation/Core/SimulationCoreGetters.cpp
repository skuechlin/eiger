/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * SimulationCoreGetters.cpp
 *
 *  Created on: Oct 15, 2014
 *      Author: kustepha
 */




#include <TimeStep/TimeStep.h>
#include "SimulationCore.h"

#include "Grid/GridManager/GridManager.h"

#include "Parallel/MyMPI.h"




uint64_t SimulationCore::nParts() const { return prts_.size(); }

// local number of cells (including ghost)
uint64_t SimulationCore::nCells() const {
  return gridMgr()->particleGrid()->nCells(); }

// const ptr to the gas model
const GasModel* SimulationCore::gasModel() const {
  return gasModel_.get(); }

// ptr to mpi mgr
MyMPI::MPIMgr* SimulationCore::mpiMgr() const {
  return mpiMgr_.get(); }

// ptr to grid mgr
Grids::GridManager* SimulationCore::gridMgr() const {
  return grdMgr_.get(); }

// ptr to boundary mgr
Boundary::BoundaryManager* SimulationCore::boundaryMgr() const {
  return bndMgr_.get(); }

// ptr to inflow mgr
Inflow* SimulationCore::inflowMgr() const { return inflMgr_.get(); }

// ptr to the particle grid
const Grids::RegularGrid* SimulationCore::particleGrid() const {
  return gridMgr()->particleGrid(); }

// ptr to the particle grid
Grids::RegularGrid* SimulationCore::particleGrid() {
  return gridMgr()->particleGrid(); }

// const reference to the simulation grid
const Grids::DataGrid* SimulationCore::simulationGrid() const {
  return gridMgr()->simulationGrid(); }

// const ptr to particle collection
const Particles::ParticleCollection* SimulationCore::prts() const {
  return &prts_; }

// const reference to velocity update strategy
const VelocityUpdate::Strategy* SimulationCore::velocityUpdate() const {
  return velUpdate_.get(); }

// reference cell volume on simulation grid
double SimulationCore::refCellVol() const {
  return simulationGrid()->grid()->referenceCellVolume(); }


// time in CFL criterion based on highest particle veloctiy
double SimulationCore::dynamicCFLTime() const {
  return simulationGrid()->grid()->minCellWidth() /
      prts_.maxSpeed(*mpiMgr());
}


double SimulationCore::computeStaticCFLTime() const {
  return simulationGrid()->grid()->minCellWidth() /
      inflMgr_->maxMostProbableSpeed();
}


// time in CFL criterion based on highest most probable inflow speed
// and simulation grid spacing
double
SimulationCore::staticCFLTime()
const
{
  static double tcfl = computeStaticCFLTime();
  return tcfl;
}
