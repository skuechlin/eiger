/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * SimulationCore.h
 *
 *  Created on: Apr 28, 2014
 *      Author: Stephan Kuechlin
 */

#ifndef SIMULATIONCORE_H_
#define SIMULATIONCORE_H_


#include <memory>
#include <list>

#include "Settings/Settings.h"

#include "Gas/GasForwardDecls.h"
#include "Gas/EqCondition.h"

#include "Particles/ParticleCollection.h"

#include "Grid/GridForwardDecls.h"

#include "Inflow/Inflow.h"

#include "Primitives/MyRandom.h"


namespace MyMPI
{
  class MPIMgr;
}

class CellData;

namespace VelocityUpdate
{
  class Strategy;
  class CollisionOperatorStatistics;
}

namespace Boundary
{
  class BoundaryManager;
}

namespace MyChrono
{
  class TimerCollection;
}

class TimeStep;

class GenericCallback;

class FieldData;

class InitialConditions;


class SimulationCore
{
  // holds simulation data and defines all methods needed for simulation

private:

  double FNum_ref_; // [-] reference number of represented molecules per particle

  double dt_ref_; // [s] reference time step

  double dt_coarsening_factor_; // [-] how much should the time step change per cell refinement level


private:

  struct PNC {

    const uint64_t N_lb;
    const uint64_t N_ub;
    const double max_upsample_rate;
    const double min_downsample_rate;
    bool operator()(){ return N_lb != 0 || N_ub != uint64_t(-1); }
    PNC(
        const uint64_t _N_lb,
        const uint64_t _N_ub,
        const double _max_upsample_rate,
        const double _min_downsample_rate)
    : N_lb(_N_lb)
    , N_ub(_N_ub)
    , max_upsample_rate(_max_upsample_rate)
    , min_downsample_rate(_min_downsample_rate)
    {
      MYASSERT(_N_ub > _N_lb,"bounds");
      MYASSERT(max_upsample_rate > 1.0,"upsample_rate");
      MYASSERT(min_downsample_rate > 0. && min_downsample_rate < 1.0,"downsample rate");

#pragma omp critical(COUT)
      std::cout << "\n" <<
          "using particle number control:\n" <<
          "  lb: " << N_lb << "\n" <<
          "  ub: " << N_ub << "\n" <<
          "  max upsample rate:   " << max_upsample_rate << "\n" <<
          "  min downsample rate: " << min_downsample_rate << "\n" << std::endl;
    }

    static std::unique_ptr<PNC> make_PNC(const Settings::Dictionary& d) {
      if (!d.isEmpty())
        return std::make_unique<SimulationCore::PNC>(
          d.get<uint64_t>("lower bound",0),
          d.get<uint64_t>("upper bound",uint64_t(-1)),
          d.get<double>("max upsample rate", 1.05),
          d.get<double>("min downsample rate", 0.95) );
      else
        return 0;
    }
  };

public:

  // reference time step size
  double dt_ref() const { return dt_ref_; }

  double weight_factor(const uint8_t level) const {
    return  __builtin_powi(dt_coarsening_factor_,level);
  }


private:

  //    // time step control
  //    std::unique_ptr<TimeStep> tsCtrl_;

  // particle number control
  std::unique_ptr<PNC> pnc_;

  // MPI management, if applicable
  std::unique_ptr<MyMPI::MPIMgr> mpiMgr_;

  // the gas model
  std::unique_ptr<Gas::GasModels::GasModel> gasModel_;

  // the particles
  Particles::ParticleCollection prts_;

  // the grids
  std::unique_ptr<Grids::GridManager> grdMgr_;

  // the simulation boundaries
  std::unique_ptr<Boundary::BoundaryManager> bndMgr_;

  // the sources
  std::unique_ptr<Inflow> inflMgr_;

  // initialization (optional)
  std::unique_ptr<InitialConditions> initConds_;

  // restart
  std::string restartPrtsFile_ = "";

  // the velocity update strategy
  std::unique_ptr<VelocityUpdate::Strategy> velUpdate_;


  //  call backs
  std::list<std::unique_ptr<GenericCallback>> callbacks_;


  // a random number generator
  MyRandom::Rng rndGen_;





public:

  // setters
  void setRndSeedByMPIRank();

  void addCallback(std::unique_ptr<GenericCallback> newCallback);
  void addCallback(std::list<std::unique_ptr<GenericCallback>> listOfNewCallbacks);



public:
  // GETTERS

  // local number of particles
  uint64_t nParts() const;

  // local number of cells (including ghost)
  uint64_t nCells() const;

  // const ptr to the gas model
  const GasModel* gasModel() const;

  // ptr to mpi mgr
  MyMPI::MPIMgr* mpiMgr() const;

  // ptr to grid mgr
  Grids::GridManager* gridMgr() const;

  // ptr to boundary mgr
  Boundary::BoundaryManager* boundaryMgr() const;

  // ptr to inflow mgr
  Inflow* inflowMgr() const;

  // ptr to particle collection
  const Particles::ParticleCollection* prts() const;

  // ptr to the particle grid
  const Grids::RegularGrid* particleGrid() const;

  // ptr to the particle grid
  Grids::RegularGrid* particleGrid();

  // ptr to the simulation grid
  const Grids::DataGrid* simulationGrid() const;

  // ptr to velocity update strategy
  const VelocityUpdate::Strategy* velocityUpdate() const;

  // reference cell volume on simulation grid
  double refCellVol() const;


  // time in CFL criterion based on highest particle velocity
  double
  dynamicCFLTime()
  const;

  // time in CFL criterion based on highest most probable inflow speed
  // and simulation grid spacing
  double
  staticCFLTime()
  const;

private:
  double
  computeStaticCFLTime()
  const;


public:
  // VERIFICATION

  // verify that the core is complete
  void verify() const;

  // verify that all particles have valid data w.r.t all grids
  void verifyPrts(
      const std::string& msg,
      const bool checkNext = false,
      const bool checkLocal = false,
      const bool checkSort = false,
      const bool checkKey = false,
      const bool checkKeyInLocalCellRange = false);

  // verify that all cell have valid data
  void verifyCells(const std::string& msg);

public:
  // FLAGS

  // have initialization data
  bool haveInitConds() const
  {
    return initConds_ != nullptr;
  }

  // have restart data
  bool haveRestartFile() const
  {
    return !(restartPrtsFile_.empty());
  }


public:
  // SIMULATION METHODS

  // collect callbacks, e.g. boundary and grid outputs
  void collectCallbacks();

  // obtain particle ensemble from restart file
  double restart();

  // initialize domain from initial condition
  void initializeDomain(MyChrono::TimerCollection& timers);


  // apply inflow boundary conditions
  void inflow();

  // apply boundary conditions to all particles
  void applyBoundaries(const uint64_t tn);


  // communicate boundary samples over mpi
  void boundarySamplesComm(const uint64_t tn);

  // communicate boundary samples over mpi
  void initBoundarySamplesComm();

  // finalize communicate boundary samples over mpi
  void finalizeBoundarySamplesComm(const uint64_t tn);

  // sort particles
  void sortPrts(MyChrono::TimerCollection& timers, const bool compute_keys);

  // sort particles
  void sortPrtsForComm(MyChrono::TimerCollection& timers);

  // sort particles
  void sortPrtsAfterComm(MyChrono::TimerCollection& timers);

  // count particles on node grid
  void countPrtsOnNodeGrid();

  // count particles on particle grid
  void countPrtsOnPrtGrid();

  // communicate particles
  void commPrts(const uint64_t tag);

  void particleNumberControl(const uint64_t tn);

  void updateGridsAndVelocities(const uint64_t tn);

  void reweightOnTimezonesUpdate();

private:

  void markIntersectedCells(const std::vector<std::string>& names_of_bnds_to_enable = {});

  VelocityUpdate::CollisionOperatorStatistics updateVelocities(
      Particles::Particle* pfirst,
      Particles::Particle* plast,
      CellData& c,
      const double dt,
      MyRandom::Rng& rndGen);


public:

  // process all registered callbacks
  void callback(const double t, const uint64_t tn, MyChrono::TimerCollection& timers);

public:

  // transform particle data to simulation coordinate system
  void rotateParticles();


public:
  // PARALLEL

  // print some mpi info
  void printMPIInfo() const;

  // write to cout if root
  void printIfRoot(const std::string& msg) const;

  // print global particle preallocation info
  void printParticlePrealloc() const;

public:

  // Constructor
  SimulationCore(const Settings::Settings& settings);

  // Destructor
  ~SimulationCore();
};

#endif /* SIMULATIONCORE_H_ */
