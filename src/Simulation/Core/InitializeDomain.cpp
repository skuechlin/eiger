/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * SimulationCoreSimInit.cpp
 *
 *  Created on: Jan 8, 2015
 *      Author: kustepha
 */


#include "SimulationCore.h"

#include "InitialConditions/InitialConditions.h"

#include "Grid/GridManager/GridManager.h"

#include "Boundary/BoundaryManager/BoundaryManager.h"



// initialize the domain with equilibrium distribution
// governed by field data
void SimulationCore::initializeDomain(MyChrono::TimerCollection& timers)
{
  if (!initConds_) // no initial conditions given
    return;


  Grids::DataGrid* g = grdMgr_->simulationGrid_;

  printIfRoot(
      "initializing particles on grid \"" + g->name() + "\"");

  auto bnds = bndMgr_.get();

  initConds_->sample(prts_,
                     g->grid(),
                     dt_ref(),
                     [this](const uint8_t level)->bool{ return weight_factor(level); },
                     [bnds](const v4df& x)->bool{ return bnds->isOutside(x); },
                     rndGen_);

  // delete initial conditions
  initConds_ = nullptr;

  sortPrts(timers,true);

  //printOnRoot(std::string("    initialized ") + std::to_string(prts_.size()) + std::string(" particles"));

  printIfRoot(" ");

}




