/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * SimulationCoreVerifiers.cpp
 *
 *  Created on: Jun 5, 2014
 *      Author: Stephan Kuechlin
 */

#include <Grid/CellData.h>
#include <cmath> // isnan
#include <iomanip>
#include <sstream>
#include <numeric> // std::accumulate



#include "SimulationCore.h"

#include "Grid/GridManager/GridManager.h"
#include "Boundary/BoundaryManager/BoundaryManager.h"

#include "Parallel/MyMPI.h"

#include "Primitives/MyError.h"
#include "Primitives/Box.h"

using namespace Particles;


void SimulationCore::verify()
const
{


  MYASSERT(gasModel_.get() != 0,
           "core incomplete: missing gas model!");
  MYASSERT(velUpdate_.get() != 0,
           "core incomplete: missing velocity update strategy!");
  MYASSERT(inflMgr_.get() != 0,
           "core incomplete: missing inflow manager!");
  MYASSERT(bndMgr_.get() != 0,
           "core incomplete: missing boundary manager!");


  // verify the DataGridCollection
  grdMgr_->verify();

  // verify the boundary manager
  bndMgr_->verify();


}


void SimulationCore::verifyCells(const std::string& msg)
{

  uint64_t gridNum = 0;
  std::stringstream errstrm;

  int rank;
  MPI_Comm_rank(MPI_COMM_WORLD,&rank);

  for (
      auto g = grdMgr_->dataGrids_.begin();
      g != grdMgr_->dataGrids_.end();
      ++g )
    {

      const uint64_t first = g->grid()->first_;
      const uint64_t last = g->grid()->last_;


      for (uint64_t i = first; i < last; ++i)
        {

          // a reference to the current cell
          const CellData& c = g->cell(i);

          // particle range
          uint64_t begin( g->grid()->begin_[i] );
          uint64_t N( g->grid()->count_[i] );

          if (begin != uint64_t(-1) 		&&
              N != uint64_t(-1) 			&&
              begin + N <= prts_.size() )
            {
              continue;
            }


          // invalid cell
          errstrm<< std::endl << std::endl;;
          errstrm<< msg << std::endl;

          errstrm << "on rank " << rank << ":" << std::endl;
          errstrm << "grid number:         " << gridNum 	<< std::endl;
          errstrm << "cell number:         " << i 			<< std::endl;
          errstrm << "begin:               " << begin       << std::endl;
          errstrm << "count:               " << N			<< std::endl;
          errstrm << "total num particles: " << prts_.size() << std::endl;
          errstrm << "grid size:           " << g->nCells() << std::endl;
          errstrm << "first:               " << first << std::endl;
          errstrm << "last:                " << last << std::endl;

          errstrm << std::endl;
          for (uint j = 0; j < CellData::nVars(); ++j) {

              errstrm << CellData::getVarName(j) << " ";
          }
          errstrm << std::endl;

          for (uint j = 0; j < CellData::nVars(); ++j) {
              errstrm << c.getVar(j) << " ";
          }
          errstrm << std::endl;
          errstrm << "begin and count: " << std::endl;
          for (uint64_t k = 0; k < g->grid()->begin_.size(); ++k)
            errstrm << k << " " << g->grid()->begin_[k] << " " << g->grid()->count_[k] << std::endl;
          errstrm << std::endl;

#pragma omp critical (COUT)
          {

            std::cout << errstrm.str();

            MYASSERT(false,"invalid cell");
          }


        } // all cells
      ++gridNum;
    } // all grids


}



namespace
{
  bool test(const bool criterion, std::string& old_msg, const std::string& fail_msg)
  {
    if (!criterion)
      old_msg += std::string(" ") + fail_msg;
    return criterion;
  }

  std::string
  double2str(const double d)
  {
    std::stringstream s;
    s << std::setprecision(23);
    s << d;
    return s.str();
  }


  bool verifyPrtsOnGrd(
      const Grids::RegularGrid& grd,
      const Eigen::AlignedBox4d& bb,
      ParticleCollection& prts,
      std::string& msg,
      const uint64_t begin,
      const uint64_t N,
      const bool checkNext,
      const bool checkSort,
      const bool checkKey,
      const bool checkKeyInLocalCellRange)
  {

    constexpr uint64_t maxerrs = 100;

    Eigen::Vector4d R,nextR,C;
    uint64_t idx, storedKey, nextKey, key;
    uint64_t prevPrtKey(0);

    if (begin > 0 && checkSort)
      prevPrtKey = grd.topology()->pos2Key(prts.getParticle(begin-1)->R);


    bool all_pass = true;

    bool pass;


    uint64_t errcnt = 0;

    std::stringstream errstrm;

    // local cell range
    const uint64_t first = grd.first_;
    const uint64_t last = grd.last_;

    for (uint64_t i = begin; i < begin+N; ++i) {

        if (errcnt >= maxerrs)
          break;

        Particle* p = prts.getParticle(i);

        if (is_marked_remove_tf(p))
          continue;

        R = to_eigen(p->R);
        C = to_eigen(p->C);

        nextR = R + p->dt*C;


        key	= grd.topology()->pos2Key(p->R);

        storedKey = prts.getKey(i);

        std::string test_msg;

        pass = true
            && test( std::isfinite(R.sum()),							test_msg,"std::isfinite(R.sum())")
        && test( (R.coeffRef(3) == 0.0),								test_msg,"R.w() == 0.0")
        && test( std::isfinite(C.sum()),								test_msg,"std::isfinite(C.sum())")
        && test( (C.coeffRef(3) == 0.0),								test_msg,std::string("C.w() == 0.0, C.w() is ") + double2str(C.coeffRef(3)))
        && test( std::isfinite(p->dt),  test_msg,"std::isfinite(p->dt)")
        && test( std::isfinite(p->m), 	test_msg,"std::isfinite(p->m)")
        && test( std::isfinite(p->w), 	test_msg,"std::isfinite(p->w)")
        && test( !checkKey || (prts.getKey(i) != uint64_t(-1)), 		test_msg,"std::isfinite(prts.key(p))")
        && test( bb.contains(R),										test_msg,"world bb.contains(R)");

        if (checkNext)
          {
            nextKey = grd.topology()->pos2Key(*(v4df*)(&nextR));
            pass = pass && test(bb.contains(nextR), test_msg,"world bb.contains(nextR)");
          }


        if (checkSort)
          pass = pass && test(prevPrtKey <= key, test_msg,"keyOfLast <= key");

        if (checkKeyInLocalCellRange)
          pass = pass && test( grd.key_[first] <= key && key < grd.key_[last], test_msg,"particle key in local cell range");

        if (pass) {
            prevPrtKey = key;
            continue;
        }

        ++errcnt;

        idx = grd.locateKey(key);


        // invalid particle
        errstrm<< std::endl;
        errstrm<< msg << std::endl;

        errstrm<< "isfinite:                 " << std::to_string( is_finite_tf(p) ) << std::endl;
        errstrm<< "isnan(R[0]):              " << std::to_string( std::isnan(double(R[0]))) << std::endl;
        errstrm<< "std::isfinite(R.sum()):   " << std::to_string(std::isfinite(R.sum())) << std::endl;

        errstrm<< "stored key: 	             "	<< storedKey				<< std::endl;
        errstrm<< "key of last:              "	<< prevPrtKey				<< std::endl;
        errstrm<< "key: 			         " 	<< key						<< std::endl;
        errstrm<< "next key: 		         " 	<< nextKey					<< std::endl;
        errstrm<< "idx:                      "  << idx                      << std::endl;
        errstrm<< "key of first non-ghost:   " 	<< grd.key_[grd.first_]		<< std::endl;
        errstrm<< "key past non-ghost:       "  << grd.key_[grd.last_]      << std::endl;
        errstrm<< "grd size:                 "  << grd.nCells()             << std::endl;

        //errstrm<< std::fixed << std::setprecision(17);
        errstrm<< "R: 	   		"	<< R.transpose(); 	    errstrm << std::endl;
        errstrm<< "next R: 		"	<< nextR.transpose(); 	errstrm << std::endl;
        errstrm<< "C: 			" 	<< C.transpose(); 	    errstrm << std::endl;

        errstrm<< "dt: 			" 	<< std::to_string(p->dt)<< std::endl;


        Eigen::AlignedBox4d bounds = Box::to_eigen(grd.boundingBox());
        errstrm<< "grd bounds min: 	" 	<< bounds.min().transpose(); errstrm << std::endl;
        errstrm<< "grd bounds max: 	" 	<< bounds.max().transpose(); errstrm  << std::endl;

        errstrm<< "sim bounds min: 	" 	<< bb.min().transpose(); errstrm  << std::endl;
        errstrm<< "sim bounds max: 	" 	<< bb.max().transpose(); errstrm  << std::endl;

        errstrm<< prts.prt2str(i) << std::endl;


        //          const Grids::CartesianTopo* cgt = dynamic_cast<const Grids::CartesianTopo*>(grd.getTopo());
        //          if (cgt)
        //            {
        //              std::vector<uint_fast64_t> idx3 = cgt->pos2KJI(prts.getVar<Particles::R>(p));
        //              std::cout<< "idx3: 		" 	<< idx3[0] << " " << idx3[1] << " " << idx3[2]<< std::endl;
        //            }

        errstrm << "grid name: 	       " << grd.name() 	<< std::endl;
        if ( idx < grd.count_.size() )
          {
            errstrm << "N:			       " << grd.count_[idx]		<< std::endl;
            errstrm << "begin:		       " << grd.begin_[idx] 		<< std::endl;
          }
        errstrm << "num parts:         " << prts.size() << std::endl;
        errstrm << "num parts on grid: " << std::accumulate(grd.count_.begin(),grd.count_.end(),0) << std::endl;

        errstrm << "particle " << i << " failed test(s): " << test_msg << std::endl;

        all_pass = false;

    } // all particles

    msg = errstrm.str();
    return all_pass;

  }


} // namespace

void SimulationCore::verifyPrts(
    const std::string& msg,
    const bool checkNext,
    const bool checkLocal,
    const bool checkSort,
    const bool checkKey,
    const bool checkKeyInLocalCellRange )
{

  static constexpr uint64_t _tile_size = 10000;

  static bool pass;
  static std::string shared_msg;
#pragma omp single
  {
    pass = true;
    shared_msg = msg;
  }

  std::string local_msg = "";

  const bool finite_pass = all_finite_tf(&prts_);

#pragma omp single
  {
    if (!finite_pass)
      {
        local_msg = "\nparticle data not finite\n";
        pass = false;
      }
  }


  if (pass)
    {

      const Grids::RegularGrid *const g = grdMgr_->particleGrid();

      const Eigen::AlignedBox4d& bb = checkLocal ? to_eigen(g->boundingBox()) : bndMgr_->worldBB();

#pragma omp for schedule(static) reduction(&&:pass)
      for (uint64_t i = 0; i < prts_.size(); i += _tile_size  )
        pass = pass && verifyPrtsOnGrd(
            *g,bb,
            prts_,
            local_msg,
            i,std::min(_tile_size,prts_.size()-i),
            checkNext,checkSort,checkKey,checkKeyInLocalCellRange);

    }

#pragma omp critical (CORE_PRT_VERIFY_MSG_AGGREGATOR)
  {
    shared_msg += local_msg;
  }
#pragma omp barrier

#pragma omp master
  {
    if (!pass)
      {
        std::cout << shared_msg << std::endl;
        MYASSERT(pass,"see cout for error");
      }
  }
#pragma omp barrier

}


