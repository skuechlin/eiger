/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * ParticleNumberControl.cpp
 *
 *  Created on: Feb 22, 2019
 *      Author: kustepha
 */


#include <cmath>
#include <random>

#include <Eigen/Dense>

#include "SimulationCore.h"

#include "Grid/GridManager/GridManager.h"
#include "Grid/RegularGrid.h"
#include "Grid/DataGrid.h"
#include "Grid/CellData.h"

#include "Particles/Particle.h"

#include "Primitives/MyCount.h"
#include "Primitives/MyRandom.h"
#include "Primitives/NormalDistribution.h"
#include "Primitives/MyString.h"
#include "Primitives/MyError.h"
#include "Primitives/Box.h"
#include "Primitives/MyArithm.h"

#include "Parallel/MyMPI.h"

namespace {

  /*
  inline void sample_moments(
      double* pm0,
      v4df* pm1,
      Eigen::Matrix4d* pm2,
      const Particles::Particle* const pfirst,
      const Particles::Particle* const plast ) {

    double          m0  = 0.;
    double          m02 = 0.;
    v4df            m1 = zero<v4df>();
    Eigen::Matrix4d m2 = Eigen::Matrix4d::Zero();


    for (const Particles::Particle* p = pfirst; p != plast; ++p ) {
        const double w = (p->w)*(p->m);
        m0   += w;
        m02  += w*w;
        m1   += w*(p->C);
        m2.selfadjointView<Eigen::Lower>().rankUpdate(to_eigen(p->C),w);
    }

    // mean:
    m1 /= m0;
    m2 /= m0;

    // center:

    // m4:
    //    S<0,i,j,k,l>() -
    //        S<0,i,j,k>()*v<l>() -
    //        S<0,i,j,l>()*v<k>() -
    //        S<0,i,k,l>()*v<j>() -
    //        S<0,j,k,l>()*v<i>() +
    //        S<0,i,j>()*v<k>()*v<l>() +
    //        S<0,i,k>()*v<j>()*v<l>() +
    //        S<0,i,l>()*v<j>()*v<k>() +
    //        S<0,j,k>()*v<i>()*v<l>() +
    //        S<0,j,l>()*v<i>()*v<k>() +
    //        S<0,k,l>()*v<i>()*v<j>() -
    //        3.0*S<0>()*v<i>()*v<j>()*v<k>()*v<l>();
    //    m4 += -4.*m3*m1 + 6.*from_eigen(m2.diagonal().eval())*m1*m1 - 3.*m1*m1*m1*m1;

    // m3:
    //    S<0,i,j,k>() -
    //        S<0,i,j>()*v<k>() -
    //        S<0,i,k>()*v<j>() -
    //        S<0,j,k>()*v<i>()
    //        + 2.0*S<0>()*v<i>()*v<j>()*v<k>();
    //    m3 += -3.*from_eigen(m2.diagonal().eval())*m1 + 2.*m1*m1*m1;

    // m2
    m2.selfadjointView<Eigen::Lower>().rankUpdate(to_eigen(m1),-1.);

    // un-bias
    m2 /= ( 1. - ( (m0*m0 > m02) ? (m02/(m0*m0)) : 0. ) );

   *pm0 = m0;
   *pm1 = m1;
   *pm2 = m2;
    //    *pm3 = m3;
    //    *pm4 = m4;

  }

   */

  template<typename T = int> // threadprivate won't work without make this a template, why??
  inline void resample(
      Particles::Particle* const ptofirst,
      Particles::Particle* const ptolast,
      const Particles::Particle* const pfromfirst,
      const Particles::Particle* const pfromlast,
      MyRandom::Rng& rng ) {

    const uint64_t M = std::distance(pfromfirst,pfromlast);
    const uint64_t N = std::distance(ptofirst,ptolast);
    const double Md = static_cast<double>(M);
    const double Nd = static_cast<double>(N);

    if (M==0 || N==0) return;

    // M->N

    // compute empirical weight cdf (prefix sum)
    static std::vector<double> w;
#pragma omp threadprivate(w)

    {
      w.resize(M+1);
      w[0] = 0.;
      uint64_t i = 1;
      for (const Particles::Particle* pfrom = pfromfirst; pfrom != pfromlast; ++pfrom, ++i)
        w[i] = w[i-1] + pfrom->w;
    }

    // new uniform weight, conserve mass
    const double wt = w[M] / Nd;

    // pre-compute and correct random numbers
    static MyMemory::aligned_array<v4df,4096> rands;
#pragma omp threadprivate(rands)

    {
      rands.resize(N);

      // standard normal distribution for smoothed velocities
      MyRandom::NormalDistribution sn;

      for (uint64_t i = 0; i < N; ++i)
        rands[i] = v4df{sn(rng),sn(rng),sn(rng),0.};

      if (N>2) {
          //  correct sample mean and variance
          v4df mu1 = zero<v4df>();
          v4df mu2 = zero<v4df>();
          double count = 0.;
          for (uint64_t i = 0; i < N; ++i) {
              count += 1.;
              v4df delta = rands[i] - mu1;
              mu1 += delta/count;
              v4df delta2 = rands[i] - mu1;
              mu2 += delta*delta2;
          }
          mu2 /= count;

          const v4df f = 1. / (MyIntrin::sqrt4(mu2) + inactive4df<3>());
          for (uint64_t i = 0; i < N; ++i)
            rands[i] = f*(rands[i] - mu1);
      }
    }


    // sample original ensemble as reference for moment correction
    //    double          m0_from;
    //    v4df            m1_from;
    //    Eigen::Matrix4d m2_from;
    //    sample_moments( &m0_from, &m1_from, &m2_from, pfromfirst, pfromlast );


    // sample N particles according to w

    // smoothing parameter
    const double h = 0.01 * pow(Md,-1./5.) /* * sqrt(fmax(10.,m2_from.trace())) */;

    // kernel transform
    //    Eigen::Matrix4d A_from = Eigen::Matrix4d::Zero();
    //    if (M<5) { A_from.topLeftCorner<3,3>() = Eigen::Matrix3d::Identity();                  }
    //    else     { A_from.topLeftCorner<3,3>() = m2_from.topLeftCorner<3,3>().llt().matrixL(); }


    const double utilde = rng.uniform();

    uint64_t k = 0; // kernel index ( in [0,...,M) )
    uint64_t i = 0; // counter for random variates, offset for stratified cdf sampling  ( in [0,...,N) )
    for (Particles::Particle* pto = ptofirst; pto != ptolast; ++pto, ++i) {

        // find kernel index by sampling empirical weight cdf

        // stratified uniform random number
        const double u = (utilde /*rng.uniform()*/ + i)*wt;

        // linear search in weight cdf
        while(w[k+1]<u) k++;

        // pointer to the kernel particle
        const Particles::Particle* const pfrom = std::next( pfromfirst, k );

        // copy kernel particle
        Particles::copy(pto,pfrom);

        // smooth
        //        const Eigen::Vector4d eps = A_from*to_eigen(rands[i]);
        //        pto->C = m1_from + (1./sqrt(1.+h*h))*(pfrom->C - m1_from + h*from_eigen( eps ) );
        pto->C = pfrom->C + h*rands[i];

        // set weight
        pto->w = wt;

    } // for all particles to be re-sampled


    // correct first, second moment
    /*
    if ( M>2 && N>2)
      {

        // sample re-sampled ensemble
        double          m0_to;
        v4df            m1_to;
        Eigen::Matrix4d m2_to;
        sample_moments( &m0_to, &m1_to, &m2_to, ptofirst, ptolast );

        // second moment correction
        Eigen::Matrix4d A_to = Eigen::Matrix4d::Zero();
        if (N<5) { A_to.topLeftCorner<3,3>() = Eigen::Matrix3d::Identity();                }
        else     { A_to.topLeftCorner<3,3>() = m2_to.topLeftCorner<3,3>().llt().matrixL(); }

        // http://eigen.tuxfamily.org/dox/group__QuickRefPage.html
        Eigen::Matrix4d fm2;
        fm2.setZero();
        fm2.topLeftCorner<3,3>()= A_to.topLeftCorner<3,3>().triangularView<Eigen::Lower>().solve<Eigen::OnTheRight>(
            A_from.topLeftCorner<3,3>());

        // correct re-sampled ensemble
        for (Particles::Particle* pto = ptofirst; pto != ptolast; ++pto) {
            const Eigen::Vector4d V = fm2 * to_eigen(pto->C  - m1_to );
            pto->C = m1_from + from_eigen(V);
        }

      }
     */



  } // resample

} // namespace




void SimulationCore::particleNumberControl(const uint64_t /* tn */ ) {


  if ( !pnc_ || !(pnc_->operator ()()) ) return;

  // thread-local rng
  MyRandom::Rng rng(rndGen_);

  // shorthands
  Grids::RegularGrid* pg = particleGrid();

  // establish particle count per cell after re-sampling
  // count in shared temporary vector, use flag to signify cells w/o change
  static std::vector<uint64_t> new_count;
  static uint64_t total;
  static uint8_t change;

#pragma omp single
  {
    new_count.resize(pg->count_.size());
    total = 0;
    change = false;
  }

#pragma omp for schedule(static) reduction(+:total) reduction(|:change)
  for (uint64_t ic = pg->first_; ic < pg->last_; ++ic)
    {
      const uint64_t M = pg->count_[ic];
      const double Md = static_cast<double>(M);
      const Particles::Particle* const pfirst = prts_.getParticle( pg->begin_[ic] );
      const Particles::Particle* const plast  = std::next(pfirst,M);

      // touched: number of particles outside bounds, or not all particles the same weights, or
      uint8_t touched = uint8_t(M < pnc_->N_lb) | uint8_t(M > pnc_->N_ub);

      if (!touched) { // particle number is within limits, but do all particles have same weight?
          const double wfirst = pfirst->w;
          for (const Particles::Particle* p = pfirst; p != plast; ++p)
            touched |= uint8_t(p->w != wfirst);
      }

      uint64_t N = M;
      if (touched) { // set resampling target number
          if (M < pnc_->N_lb)
            N = std::min( pnc_->N_lb, static_cast<uint64_t>(pnc_->max_upsample_rate*Md + rng.uniform()) );
          else if (M > pnc_->N_ub )
            N = std::max( pnc_->N_ub, static_cast<uint64_t>(pnc_->min_downsample_rate*Md + rng.uniform()) );
      }

      pg->cell(ic).flag = touched;
      new_count[ic] = N;

      total += N;
      change |= touched;

    } // for all cells


  if (!change)
    return; // nothing to do

  // --------------------------------------------------------------------------//
  // second pass: copy all particles, take care of deletions, re-sampling...

  // convert new count to begin, include extra cell at end to preserve count information
  MyCount::parallel_exclusive_scan(new_count.data()+pg->first_,pg->nNonGhost()+1);


#pragma omp single
  {
    typedef Particles::ParticleCollection::ResizeMode Mode;
    const auto& flag = Particles::ParticleCollection::resizeflag;

    // allocate particle storage
    prts_.resize(total, flag(Mode::OLD) );
  }


#pragma omp for schedule(static)
  for (uint64_t ic = pg->first_; ic < pg->last_; ++ic)
    {
      const Particles::Particle* const pfromfirst = prts_.getParticle( pg->begin_[ic] );
      const Particles::Particle* const pfromlast  = std::next(pfromfirst, pg->count_[ic]);

      Particles::Particle* const ptofirst = prts_.getOldParticle( new_count[ic] );
      Particles::Particle* const ptolast  = prts_.getOldParticle( new_count[ic+1] );

      if (!(pg->cell(ic).flag)) {
          // only copy
          Particles::copy(ptofirst,pfromfirst,pfromlast);
      } else {
          // resample
          resample( ptofirst, ptolast, pfromfirst, pfromlast, rng );
      }

      // update particle grid count, begin for current cell
      pg->begin_[ic] = new_count[ic];
      pg->count_[ic] = new_count[ic+1] - new_count[ic];

    }

#pragma omp single
  {
    typedef Particles::ParticleCollection::ResizeMode Mode;
    const auto& flag = Particles::ParticleCollection::resizeflag;

    // allocate particle storage
    prts_.resize(total, flag(Mode::ALL) | flag(Mode::KEEP_OLD_DATA) );

    // make old data (copy destination) current
    prts_.swapOldCurrent();

  }

}
