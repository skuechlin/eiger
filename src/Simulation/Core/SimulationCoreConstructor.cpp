/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * SimulationCoreConstructor.cpp
 *
 *  Created on: Jun 8, 2018
 *      Author: kustepha
 */

#include <iostream>
#include <iomanip>

#include "SimulationCore.h"

#include "Primitives/MyError.h"
#include "Primitives/MyString.h"

#include "Settings/Settings.h"

#include "TimeStep/TimeStep.h"

#include "Boundary/BoundaryManager/BoundaryManager.h"

#include "Parallel/MyMPI.h"

#include "Gas/GasModelFactory.h"
#include "Gas/EqCondition.h"

#include "Grid/GridManager/GridManager.h"

#include "GridAdaption/GridAdaption.h"

#include "VelocityUpdate/VelocityUpdateStrategy.h"

#include "Simulation/Simulation.h"
#include "Simulation/Core/SimulationCore.h"

#include "SimulationBox/SimulationBox.h"


#include "FieldData/FieldData.h"

#include "IO/Output.h"
#include "IO/Tecplot/Tecplot.h"

#include "Particles/ParticleCollection.h"

#include "Inflow/Inflow.h"

#include "InitialConditions/InitialConditions.h"

namespace
{

  double
  computeFN(
      const Gas::GasModels::GasModel* gas,
      const SimulationBox& simBox,
      const Settings::Dictionary& cndsdict)
  {

    Settings::Dictionary refdict = cndsdict.getDictByValue("name",std::string("reference"));

    const double n = gas->getn(refdict);

    const double Np = refdict.get<double>("number of particles");

    const double vol = simBox.volume();

    const double FN = n * vol / Np;

    //    std::cout << "Np    = " << Np  << std::endl;
    //    std::cout << "vol   = " << vol << std::endl;
    //    std::cout << "n ref = " << n   << std::endl;
    //    std::cout << "FNum  = " << FN  << std::endl;

    return FN;

  }




  void addCheckpoint (
      SimulationCore* core,
      const std::string& dirPref,
      const Settings::Dictionary& dict)
  {

    const Particles::ParticleCollection* prts = core->prts();

    auto writeFun = [prts](
        const std::string& dir,
        const double solTime,
        const unsigned long int tn,
        const MyMPI::MPIMgr& mpiMgr)
        {

      // print message
      if (mpiMgr.isRoot())
        std::cout << "writing ts #" << tn << " particles" << std::endl;

      // generate filename from solution time
      std::stringstream fnamestr;
      fnamestr.str(std::string(""));
      fnamestr << "prts_at_time_";
      fnamestr << std::fixed;
      fnamestr << std::setw(20);
      fnamestr << std::setprecision(10);
      fnamestr << std::setfill('0');
      fnamestr << solTime;
      fnamestr << ".prts";

      // prepend directory
      std::string fname = MyString::catDirs(dir,fnamestr.str());

      // write
      prts->write(solTime,fname,mpiMgr);

        }; // lambda

    core->addCallback( std::unique_ptr<GenericCallback>(
        newOutput(
            writeFun,
            dict,
            dirPref ) ) );
  }



} // anonymous namespace


SimulationCore::SimulationCore(const Settings::Settings& s)
{
  using namespace Settings;

  const Dictionary& dict = *(s.dict());

  // PARTICLE NUMBER CONTROL
  pnc_ = PNC::make_PNC(dict.get<Dictionary>("particle number control",Dictionary()));

  // PARTICLE PRE-ALLOCATION (per core value given)
  prts_.reserve( s.opts()->npc_ * omp_get_num_procs() );

#ifndef NDEBUG
  std::cout << "particle pre-allocation complete" << std::endl;
#endif

  // GAS MODEL
  gasModel_ = Gas::GasModels::makeGasModel(
      dict.get<Dictionary>("gas model") );

#ifndef NDEBUG
  std::cout << "gas model setup complete" << std::endl;
#endif

  // SIMULATION BOX
  SimulationBox simBox( dict.get<Dictionary>("simulation box") );

#ifndef NDEBUG
  std::cout << "simulation box setup complete" << std::endl;
#endif

  // MPI
  mpiMgr_ = std::unique_ptr<MyMPI::MPIMgr>( new class MyMPI::MPIMgr() );

#ifndef NDEBUG
  std::cout << "mpi mgr setup complete" << std::endl;
#endif

  // TIME STEP CONTROL
  const Dictionary dtdict = dict.get<Dictionary>("time step control");
  dt_ref_ = dtdict.get<double>("dt");
  dt_coarsening_factor_ = dtdict.get<double>("dt coarsening factor",1.0);
  MYASSERT(dt_coarsening_factor_ >= 1.0,"dt coarsening factor < 1.");

  FNum_ref_ = computeFN(gasModel(),simBox,dict.get<Dictionary>("condition"));

  if (mpiMgr_->isRoot())
    std::cout   <<
    "    dt ref: " << dt_ref_               << "\n"     <<
    "    Fn ref: " << FNum_ref_             << "\n"     <<
    "    dt fac: " << dt_coarsening_factor_ << "\n"     << std::endl;

  //  tsCtrl_ = std::unique_ptr<TimeStep>(
  //      new TimeStep(
  //          dict.get<Dictionary>("time step control"),
  //          TimeStep::TauFunT(&SimulationCore::staticCFLTime),
  //          TimeStep::TauFunT(&SimulationCore::dynamicCFLTime) )
  //  );

#ifndef NDEBUG
  std::cout << "time step control setup complete" << std::endl;
#endif

  // ALGORITHM
  velUpdate_ = VelocityUpdate::makeStrategy(
      dict.get<Dictionary>("algorithm") );

#ifndef NDEBUG
  std::cout << "velocity update strategy setup complete" << std::endl;
#endif

  // GRIDS
  grdMgr_ = std::unique_ptr<Grids::GridManager>( new class Grids::GridManager(
      dict.get<Dictionary>("grid"),
      simBox,
      *(mpiMgr()),
      s.opts()->oDir_ ) );

#ifndef NDEBUG
  std::cout << "grid mgr setup complete" << std::endl;
#endif


  // BOUNDARIES
  bndMgr_ = std::unique_ptr<Boundary::BoundaryManager>(
      new class Boundary::BoundaryManager(
          gasModel(),
          dt_coarsening_factor_,
          FNum_ref_,
          simBox,
          dict.get<Dictionary>("condition"),
          dict.get<Dictionary>("state",Dictionary()),
          dict.get<Dictionary>("boundary"),
          s.opts()->iDir_ ,
          s.opts()->oDir_ )
  );

#ifndef NDEBUG
  std::cout << "boundary mgr setup complete" << std::endl;
#endif

  // GRID ADAPTION
  if ( dict.get<Dictionary>("grid").hasMember("grid adaption"))
    addCallback(
        GridAdaption(
            gridMgr(),
            boundaryMgr(),
            mpiMgr(),
            gasModel(),
            dict.get<Dictionary>("grid").get<Dictionary>("grid adaption"),
            dict.get<Dictionary>("condition")
        ).makeCallbacks() );

#ifndef NDEBUG
  std::cout << "grid adaption setup complete" << std::endl;
#endif

  // INFLOWS
  inflMgr_ = std::unique_ptr<Inflow>(
      new class Inflow(
          dt_coarsening_factor_ != 1.0,
          FNum_ref_,
          boundaryMgr(),
          gasModel(),
          s.opts()->iDir_ ,
          mpiMgr()->comm()) );

#ifndef NDEBUG
  std::cout << "inflow mgr setup complete" << std::endl;
#endif

  // INITIAL CONDITION
  if( dict.hasMember("initialization") )
    initConds_ = std::unique_ptr<InitialConditions>(
        new class InitialConditions(
            FNum_ref_,
            gasModel(),
            dict.get<Dictionary>("condition").getDictByValue(
                "name",
                dict.get<Dictionary>("initialization").get<std::string>("condition")
            ),
            s.opts()->iDir_ ,
            mpiMgr()->comm() ) );
  else
    initConds_ = nullptr;


#ifndef NDEBUG
  std::cout << "initial conditions setup complete" << std::endl;
#endif

  // RESTART
  if (dict.hasMember("restart"))
    {
      restartPrtsFile_ = MyString::catDirs(
          s.opts()->iDir_ ,
          dict.get<Dictionary>("restart").get<std::string>("file") );

      std::cout << "restart setup complete" << std::endl;
    }

  // CHECKPOINT
  if (dict.hasMember("checkpoint"))
    {
      addCheckpoint(
          this,
          s.opts()->oDir_ ,
          dict.get<Dictionary>("checkpoint") );

      std::cout << "checkpoint setup complete" << std::endl;
    }

}
