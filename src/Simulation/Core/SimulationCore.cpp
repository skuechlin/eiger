/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * SimulationCore.cpp
 *
 *  Created on: Apr 28, 2014
 *      Author: Stephan Kuechlin
 */


#include <TimeStep/TimeStep.h>
#include "SimulationCore.h"

#include "Grid/GridManager/GridManager.h"

#include "Boundary/BoundaryManager/BoundaryManager.h"

#include "Gas/GasModel.h"

#include "VelocityUpdate/VelocityUpdateStrategy.h"

#include "IO/Output.h"

#include "Parallel/MyMPI.h"

#include "Primitives/MyCount.h"
#include "Primitives/Partitioning.h"
#include "Primitives/Timer.h"

#include "Particles/Particle.h"

#include "InitialConditions/InitialConditions.h"

#include "LoadBalancing/ExactBisection.h"


// -------------------------------------------------------------------------------------------------------//
// MISC

void SimulationCore::setRndSeedByMPIRank() {
  rndGen_.long_jump(mpiMgr_->rank()); }


void SimulationCore::addCallback(std::unique_ptr<GenericCallback> newCallback) {
  callbacks_.push_back(std::move(newCallback));
}

void SimulationCore::addCallback(std::list<std::unique_ptr<GenericCallback>> listOfNewCallbacks)
{
  for (auto&& c : listOfNewCallbacks)
    addCallback(std::move(c));
}

// -------------------------------------------------------------------------------------------------------//
// SIMULATION ROUTINES

void SimulationCore::markIntersectedCells(const std::vector<std::string>& names_of_bnds_to_enable) {

  std::vector<const Boundary::Boundary*> enabled;

#pragma omp master
  {

    bndMgr_->getEnabledBoundaries(&enabled);

    // tag cells intersecting boundaries
    bndMgr_->enableBoundariesByName(names_of_bnds_to_enable);

  }
#pragma omp barrier


  auto g = particleGrid();

#pragma omp for schedule(static)
  for (uint64_t ic = g->first_; ic < g->last_; ++ic)
    g->cell(ic).intersected = bndMgr_->test_AABB( Box::to_eigen( g->cellBox(ic) ) );


#pragma omp master
  {
      // restore boundary enabled state at entry
      bndMgr_->enableBoundaries(enabled);
  }
#pragma omp barrier
}



// call velocity update
VelocityUpdate::CollisionOperatorStatistics SimulationCore::updateVelocities(
    Particles::Particle* pfirst,
    Particles::Particle* plast,
    CellData& c,
    const double dt,
    MyRandom::Rng& rndGen) {
  return velocityUpdate()->updateVelocities(pfirst,plast,c,dt,gasModel(),rndGen);
}


// sort the particles
void SimulationCore::sortPrts(MyChrono::TimerCollection& timers, const bool compute_keys) {

  if (compute_keys)
    {
      timers.start("keys");
      gridMgr()->topology()->computeKeys(prts_);
      timers.stop("keys");
    }

  timers.start("sort");
  uint8_t algo = prts_.sort(timers);
  timers.stop("sort");


#ifndef NDEBUG
  bool sort_success = prts_.keysAreSorted();
  if (!sort_success)
    {

#pragma omp single
      {
        //        prts_.print();
        prts_.printSort(true,0,std::min(uint64_t(100),prts_.size()));
        std::cout.flush();
      }
    }
  MYASSERT(sort_success,std::string("keys not sorted after sort with algo ") + std::to_string(algo));
#endif


  static_cast<void>(algo); // surpress warning about unused algo in NDBUG

} // sortPrts()

// sort particles by mpi grid
void
SimulationCore::sortPrtsForComm(MyChrono::TimerCollection& timers)
{
  if (mpiMgr()->nRanks() > 1)
    sortPrts(timers,true);

}

void
SimulationCore::sortPrtsAfterComm(MyChrono::TimerCollection& timers)
{

  if (mpiMgr()->nRanks() > 1)
    {
      timers.start("sort");
      timers.start("sort: permute");
      prts_.permute();
      timers.stop("sort: permute");
      timers.stop("sort");
    }
  else
    sortPrts(timers,true);


}


void assertCorrectCount(
    const Grids::Grid* g,
    const Particles::ParticleCollection& prts,
    const int rank)
{
  constexpr int max_d = 25;
  if (!(g->begin_[g->first_] == 0))
    {
      std::cout << "error on rank " << rank
          << ": after counting particles on grid " << g->name() << ", particles found in low ghost cells" << std::endl;

      std::cout << "first: " << g->first_  << " last: " << g->last_ << " nCells: " << g->nCells( )<< std::endl;

      std::cout << "prts.size(): " << prts.size() << std::endl;

      const uint64_t beg = g->begin_[g->first_];

      if (beg > max_d)
        {
          prts.print(0,max_d/2);
          std::cout << "..." << std::endl;
          prts.print(beg-max_d/2,beg);

          std::cout << std::endl;

          prts.printSort(false,0,max_d/2);
          prts.printSort(false,beg-max_d/2,beg);

        }
      else
        {
          prts.print(0,beg);

          std::cout << std::endl;

          prts.printSort(false,0,beg);
        }

      std::cout << "grid from first-1 to first+1:" << std::endl;
      for (uint64_t i = g->first_ > 0 ? g->first_-1 : 0; i < g->first_+1; ++i)
        std::cout << i << ": " << g->key_[i] << " " << g->count_[i] << " " << g->begin_[i] << std::endl;

      std::cout << std::endl << "will fail assertion and exit..." << std::endl;
    }

  if (!(g->begin_[g->last_] == prts.size()))
    {

      std::cout << "error on rank " << rank
          << ": after counting particles on grid " << g->name() << ", particles found in high ghost cells" << std::endl;

      std::cout << "first: " << g->first_  << " last: " << g->last_ << " nCells: " << g->nCells( )<< std::endl;

      std::cout << "prts.size(): " << prts.size() << std::endl;
      std::cout << "begin[last]: " << g->begin_[g->last_] << std::endl;

      const uint64_t beg = g->begin_[g->last_];
      const uint64_t end = prts.size();

      if (end-beg > max_d)
        {
          prts.print(beg,beg+max_d/2);
          std::cout << "..." << std::endl;
          prts.print(end-max_d/2,end);

          std::cout << std::endl;

          prts.printSort(false,beg,beg+max_d/2);
          prts.printSort(false,end-max_d/2,end);

        }
      else
        {
          prts.print(beg,end);

          std::cout << std::endl;

          prts.printSort(false,beg,end);
        }

      std::cout << "grid from last-5 to last+5 (last = " << g->last_ << "):" << std::endl;
      std::cout << "     key |      count |      begin" << std::endl;
      for (uint64_t i = g->last_ - std::min(uint64_t(5),g->last_); i < g->last_+std::min(uint64_t(5),g->count_.size() - g->last_); ++i)
        std::cout << i << ": " << g->key_[i] << " " << g->count_[i] << " " << g->begin_[i] << std::endl;

      std::cout << std::endl << "will fail assertion and exit..." << std::endl;
    }

  MYASSERT(g->begin_[g->first_] == 0, "after counting particles on grid " + g->name() + ",  particles found in low ghost cells");
  MYASSERT(g->begin_[g->last_] == prts.size(), "after counting particles on grid " + g->name() + ", particles found in high ghost cells");
}

// count particles on node grid
void
SimulationCore::countPrtsOnNodeGrid()
{
  if (mpiMgr()->nRanks() <= 1) return;

  Grids::Grid *const g = gridMgr()->nodeGrid();

  MYASSERT(g->count_.size()>0,"Node grid count size 0");
  MYASSERT(g->count_.data()!=nullptr,"Node grid count vector invalid");
  MYASSERT(g->begin_.size()>0,"Node grid begin size 0");
  MYASSERT(g->begin_.data()!=nullptr,"Node grid begin vector invalid");
  MYASSERT(g->key_.size()>0,"Node grid key size 0");
  MYASSERT(g->key_.data()!=nullptr,"Node grid key vector invalid");

  MyCount::parallelCount(
      g->count_.data(),
      g->begin_.data(),
      g->key_.data(),
      g->count_.size(),
      prts_.keys(),
      prts_.size()
  );

#pragma omp single
  assertCorrectCount(g, prts_, mpiMgr_->rank());

}

// count particles on particle grid
void
SimulationCore::countPrtsOnPrtGrid()
{

  Grids::RegularGrid *const g = gridMgr()->particleGrid();

  MyCount::parallelCount(
      g->count_.data(),
      g->begin_.data(),
      g->key_.data(),
      g->count_.size(),
      prts_.keys(),
      prts_.size()
  );

#pragma omp single
  assertCorrectCount(g, prts_, mpiMgr_->rank());

}



// communicate particles
void
SimulationCore::commPrts(const uint64_t tag)
{
#pragma omp single
  {
    Grids::Grid* const nodeGrid = gridMgr()->nodeGrid();
    // perform communication
    prts_.communicate(nodeGrid,*(mpiMgr()),tag);
  } // implicit boundary, task scheduling point
}



void SimulationCore::applyBoundaries(const uint64_t tn)
{
  boundaryMgr()->applyBoundaries(
      tn,dt_ref(),prts_,gasModel(),rndGen_);
}


// communicate boundary samples over mpi
void SimulationCore::boundarySamplesComm(const uint64_t tn)
{
#pragma omp master
  boundaryMgr()->sampleComm(tn,gasModel(),*mpiMgr());
#pragma omp barrier
}

// communicate boundary state over mpi
void SimulationCore::initBoundarySamplesComm()
{
#pragma omp single nowait
  boundaryMgr()->initSampleComm(*mpiMgr());
}

// finalize communicate boundary samples over mpi
void SimulationCore::finalizeBoundarySamplesComm(const uint64_t tn)
{

#pragma omp single
  boundaryMgr()->finalizeSampleComm(tn,gasModel());

  // implicit barrier, task scheduling point

}

void SimulationCore::inflow()
{


  // all threads do sampling

  // ...using their own rng
  MyRandom::Rng rndGen(rndGen_);

  inflowMgr()->sample(
      prts_,
      dt_ref(),
      [this](const uint8_t level)->double{ return weight_factor(level); },
      mpiMgr()->rank(),
      rndGen);


}


void SimulationCore::reweightOnTimezonesUpdate() {

  countPrtsOnPrtGrid();

  auto pg = gridMgr()->particleGrid();

  pg->lb_nonGhost_on_threads();
  const uint64_t first = pg->thread_first();
  const uint64_t last = pg->thread_last();

  for (uint64_t ic = first; ic < last; ++ic)
    {
      const double wf = weight_factor(pg->cellLevel(ic));
      const double dt_new = dt_ref()*wf;
      auto p = prts_.getParticle(pg->begin_[ic]);
      const auto pend = p + pg->count_[ic];
      for (; p != pend; ++p)
        {
          const double dt_old = p->dt;

          p->dt = dt_new;
          p->w *= dt_new/dt_old;
        }
    }

#pragma omp barrier
}




// process all registered callbacks
void
SimulationCore::callback(const double t, const uint64_t tn, MyChrono::TimerCollection& timers)
{
  for (auto&& c : callbacks_)
    c->callIfTime(t,tn,*mpiMgr(),timers);

  // optional dependent triggers

  static uint64_t node_grd_change_count = -1;
  static uint64_t particle_grid_change_count = -1;

  bool nodeGridChanged = false;
  bool particleGridChanged = false;

#pragma omp single copyprivate(nodeGridChanged,particleGridChanged)
  {
    if (gridMgr()->nodeGrid()->changeCount() != node_grd_change_count)
      {
        node_grd_change_count = gridMgr()->nodeGrid()->changeCount();
        nodeGridChanged = true;
      }
    if (gridMgr()->particleGrid()->changeCount() != particle_grid_change_count)
      {
        particle_grid_change_count = gridMgr()->particleGrid()->changeCount();
        particleGridChanged = true;
      }
  }
  // implicit barrier

  if (nodeGridChanged || particleGridChanged) {
      timers.start("keys");
      gridMgr()->topology()->computeKeys(prts_);
      timers.stop("keys");
  }


  if (nodeGridChanged)
    {
      //#pragma omp single
      //      std::cout << "DEBUG: skipping element rank update" << std::endl;

      inflowMgr()->updateElementRanks(gridMgr()->nodeGrid());
    }

  if (particleGridChanged)
    {
      if (nodeGridChanged)
        {
          timers.start("count");
          countPrtsOnNodeGrid();
          timers.stop("count");

          // communicate particles
          timers.start("prt_comm");
          commPrts(COMPILE_TIME_CRC32_STR( FILE_POS_STR ) + tn);
          timers.stop("prt_comm");

          sortPrtsAfterComm(timers);
        }
      reweightOnTimezonesUpdate();
    }

}


// TRANSFORM

// transform particle data to simulation coordinate system
void
SimulationCore::rotateParticles()
{
  if (gridMgr()->topology()->axisymmetric())
    {
#pragma omp for schedule(static)
      for( uint64_t i = 0; i < prts_.size(); ++i)
        Particles::rotate2xy(prts_.getParticle(i));
    }
}


// PARALLEL

// print some mpi info
void
SimulationCore::printMPIInfo()
const
{
  mpiMgr()->printInfo();
}

// write to cout if root
void
SimulationCore::printIfRoot(const std::string& msg)
const
{
  if (mpiMgr()->isRoot())
    std::cout << msg << std::endl;
}

// print global particle preallocation info
void SimulationCore::printParticlePrealloc() const
{
  const MyMPI::MPIMgr* mpiMgr = this->mpiMgr();
  const int root = mpiMgr->root();
  const int nranks = mpiMgr->nRanks();
  const int rank = mpiMgr->rank();

  std::vector<uint64_t> prts_capacity_per_rank(nranks);
  const uint64_t cap = prts_.capacity();
  MPI_Gather(&cap,1,MPI_UINT64_T,prts_capacity_per_rank.data(),1,MPI_UINT64_T,root,mpiMgr->comm());

  if (rank == root)
    {
      std::stringstream ss;
      ss << "\npre-allocation:\n";
      ss << " _rank_|_#particles_\n";
      ss << "       |            \n";
      for (int i = 0; i < nranks; ++i)
        {
          ss <<  std::setw(6) << i;
          ss << " | " << std::setw(10) << prts_capacity_per_rank[i];
          ss << "\n";
        }
      ss << std::endl;
      std::cout << ss.str() << std::endl;
    }
}


// collect boundary callbacks, e.g. outputs from boundary manager
void SimulationCore::collectCallbacks()
{
  std::unique_ptr<GenericCallback> o;

  while ( ( o = std::move(boundaryMgr()->popCallback()) ) != 0 )
    addCallback( std::move(o) );

  while ( ( o = std::move(gridMgr()->popCallback()) ) != 0 )
    addCallback( std::move(o) );

}


// load the particle distribution from file
double SimulationCore::restart()
{

  printIfRoot("loading particles from file \"" + restartPrtsFile_ + "\"");

  double solTime(0.0);

  // read particles
  solTime = prts_.read(restartPrtsFile_,*mpiMgr());

  return solTime;

}

// Destructor
SimulationCore::~SimulationCore() = default;



