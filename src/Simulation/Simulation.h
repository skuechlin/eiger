/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * Simulation.h
 *
 *  Created on: Apr 23, 2014
 *      Author: Stephan Kuechlin
 */

#ifndef SIMULATION_H_
#define SIMULATION_H_

#include <cstdint>
#include <memory>

#include "Simulation/Core/SimulationCore.h"

namespace Settings
{
  class Dictionary;
}

class Simulation {
  // Simulation defines the simulation data and algorithm
  uint64_t n_ts_; 	// number of time steps

  uint64_t tn_info_; 	// print time-step information every so-many time steps [#]

  SimulationCore core_;

public:

  SimulationCore* core() { return &core_; }

  void run();

  void printInfo(
      const uint64_t tn,
      const double dt,
      const double t_sim,
      const double t_wall
  ) const;


  explicit Simulation(const Settings::Settings& s);

  ~Simulation();


};

#endif /* SIMULATION_H_ */
