/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * Simulation.cpp
 *
 *  Created on: Apr 23, 2014
 *      Author: Stephan Kuechlin
 */

#define PROGRESS(msg) \
    std::cout   << std::setfill('0') << std::setw(4) << rank \
    << ": "     << std::setw(6)      << tn                   \
    << ": ln. " << std::setw(3)      << __LINE__  << std::setfill(' ')\
    << ": "     << msg << std::endl;

#include <iomanip>
#include <cmath>
#include <cfenv>

#include "Simulation.h"

#include "Core/SimulationCore.h"

#include "Settings/Dictionary.h"

#include "Parallel/MyMPI.h"

#include "Primitives/MyError.h"
#include "Primitives/Timer.h"

void Simulation::run()
{



  MyMPI::barrier();


  // verify core integrity
  core_.verify();

  // make random number generator unique to node
  core_.setRndSeedByMPIRank();

  // collect callbacks, e.g. boundary and grid outputs
  core_.collectCallbacks();

  // setup complete!
  core_.printIfRoot("setup complete!\n");

  // print some mpi info
  core_.printMPIInfo();

  // shortcut to rank
  const int rank = core_.mpiMgr()->rank();

  // timers
  MyChrono::TimerCollection timers;


  // simulation time
  double t = 0.0;

  // time step
  double dt;

  // reserve memory for particles
  core_.printParticlePrealloc();

  // (optional) initializations
  if ( core_.haveRestartFile() )
    {
      // restart from saved particle ensemble
      t = core_.restart();
    }
  else
    {
      if ( core_.haveInitConds())
        {
          // initialize domain
          core_.initializeDomain(timers);
        }
    }


#ifndef NDEBUG

  // check init
  std::cout << std::setfill('0') << std::setw(4) << core_.mpiMgr()->rank() << std::setfill(' ')  << " " << "init" << std::endl;

  core_.verifyPrts("Invalid after initialization");
#endif


  // write any pre-sim callbacks (eg. grids) (also serves as test for successful write)
  core_.callback(-1.,-1,timers);

  core_.printIfRoot("");

  //  // perform any initial callbacks
  //  core_.callback(t,0,timers);


  // timers
  timers = MyChrono::TimerCollection();

  //  timers.insert("simulation"); 	// overall simulation duration
  //  timers.insert("comm"); 	// time mpi comm
  //  timers.insert("prt_comm"); 	// time particle mpi comm
  //  timers.insert("bnd_comm"); 	// time boundary mpi comm
  //  timers.insert("inflow");	// time inflow
  //  timers.insert("bounds");	// boundaries
  //  timers.insert("bnd_fin");	// boundary finalization
  //  timers.insert("sort");	// sorting
  //  timers.insert("count");   // counting particles on grids
  //  timers.insert("grid_update");	// updating the grids
  //  timers.insert("vel_update");	// velocity updates
  //  timers.insert("csys_transform"); // particle coordinate transforms
  //  timers.insert("callbacks");	// write ops

  // the main simulation loop
  MyMPI::barrier();

  timers.start("simulation");

#ifndef VERBPROGRESS
//#define VERBPROGRESS
#endif

#ifndef VERIFY
//#define VERIFY
#endif


#pragma omp parallel firstprivate(t) private(dt)
  {
    feenableexcept(FE_DIVBYZERO | FE_INVALID | FE_OVERFLOW /* FE_ALL_EXCEPT */);

    _MM_SET_FLUSH_ZERO_MODE(_MM_FLUSH_ZERO_ON);
    _MM_SET_DENORMALS_ZERO_MODE(_MM_DENORMALS_ZERO_ON);

    for ( uint64_t tn = 0; tn < n_ts_; ++tn)
      {

#ifdef VERBPROGRESS
#pragma omp master
        PROGRESS("start tn")
#endif

                // compute time step
                dt = core_.dt_ref();

//        MYASSERT(dt > 1.e-16,"time step smaller than 1.e-16!");

        timers.start("info");
#ifdef NDEBUG
        if ( ( (tn % tn_info_) == 0 ) || ( tn == (n_ts_-1) ) )
#endif
          printInfo(tn, dt, t, timers.elapsed<std::chrono::minutes>("simulation"));

#ifdef VERBPROGRESS
#pragma omp master
        PROGRESS("info")
#endif

        //        if ( tn % (10*tn_info_) == 0)
        //          printParticlesAndCellsPerRank();
        timers.stop("info");

        // apply inflow boundary conditions
        timers.start("inflow");
        core_.inflow();
        timers.stop("inflow");

#if defined(VERIFY) || 0
        // check inflow
        core_.verifyPrts("Invalid after inflow");
#endif

#ifdef VERBPROGRESS
#pragma omp master
        PROGRESS("inflow")
#endif

        // move particles and apply boundary conditions
        timers.start("bounds");
        core_.applyBoundaries(tn);
        timers.stop("bounds");

#ifdef VERBPROGRESS
#pragma omp master
        PROGRESS("apply bounds")
#endif

#if defined(VERIFY) || 0
        // check boundaries
        core_.verifyPrts("Invalid after bnds",true);
#endif

#ifdef VERBPROGRESS
#pragma omp master
        PROGRESS("fin bounds")
#endif

        //in axisymmetric simulation, transform particle coordinates and velocities
        //by rotating to xy plane
        timers.start("csys_transform");
        core_.rotateParticles();
        timers.stop("csys_transform");

#ifdef VERBPROGRESS
#pragma omp master
        PROGRESS("prts2Csys")
#endif


        // prepare particle communication
        core_.sortPrtsForComm(timers);

#ifdef VERBPROGRESS
#pragma omp master
        PROGRESS("sort for comm")
#endif

        timers.start("count");
        core_.countPrtsOnNodeGrid();
        timers.stop("count");

#ifdef VERBPROGRESS
#pragma omp master
        PROGRESS("count on node grid")
#endif

        // communicate particles
        timers.start("prt_comm");
        core_.commPrts(tn);
        timers.stop("prt_comm");

#if defined(VERIFY) || 0
        // check comm
        core_.verifyPrts("Invalid after comm",false,true);
#endif

#ifdef VERBPROGRESS
#pragma omp master
        PROGRESS("comm prts")
#endif

        // init sync boundary state
        timers.start("bnd_comm");
        core_.initBoundarySamplesComm();
        timers.stop("bnd_comm");

#ifdef VERBPROGRESS
#pragma omp master
        PROGRESS("comm bounds");
#endif

        // sort particles to merge received
        core_.sortPrtsAfterComm(timers);

#ifdef VERBPROGRESS
#pragma omp master
        PROGRESS("sort prts")
#endif

        timers.start("count");
        core_.countPrtsOnPrtGrid();
        timers.stop("count");

#ifdef VERBPROGRESS
#pragma omp master
        PROGRESS("count on prt grid")
#endif


#if defined(VERIFY) || 0
        // check sorting
        core_.verifyPrts("Particles Invalid after sorting",true,true,true,true,true);
        core_.verifyCells("Cells Invalid after sorting");
#endif

        // perform particle number control
        timers.start("pnc");
        core_.particleNumberControl(tn);
        timers.stop("pnc");

#if defined(VERIFY) || 0
        core_.verifyPrts("Invalid after pnc");
#endif

        // sample the particle data on all grids
        timers.start("grid_update");
        core_.updateGridsAndVelocities(tn);
        timers.stop("grid_update");

#ifdef VERIFY
        // check vel update
        core_.verifyPrts("Invalid after vel update");
#endif

#ifdef VERBPROGRESS
#pragma omp master
        PROGRESS("grid and vel update")
#endif

        // finalize sync boundary state
        timers.start("bnd_comm");
        core_.finalizeBoundarySamplesComm(tn);
        timers.stop("bnd_comm");

        // increment time
        t += dt;

        // process callbacks
        timers.start("callbacks");
        core_.callback(t,tn,timers);
        timers.stop("callbacks");

#ifdef VERBPROGRESS
#pragma omp master
        PROGRESS("callbacks")
#endif

#pragma omp barrier

      } // main loop

  } // omp parallel

  timers.stop("simulation");

  if (core_.mpiMgr()->isRoot())
    {
      std::cout << "\nrun finished!\n\n" << "\ntiming (in minutes):\n" << std::endl;
      timers.print_elapsed<std::chrono::minutes>();
      std::cout << std::endl;
    }

  static_cast<void>(rank);

} // void run()


void
Simulation::printInfo(
    const uint64_t tn,
    const double dt,
    const double t_sim,
    const double t_wall
) const
{

  static uint64_t tn_last(0);
  static double t_wall_last(0);

#pragma omp master
  {
    const uint64_t np_tot = core_.prts()->global_size();

    if (core_.mpiMgr()->isRoot())
      {
        std::stringstream ss;

        uint64_t dtn        = tn - tn_last;
        double dt_wall      = t_wall - t_wall_last;

        ss << "ts #: " 			        << std::setw(7) 	<< tn;
        ss << ", np tot: " 	            << std::setw(10)  	<< np_tot;
        ss << ", t sim:  "		        << std::scientific 	<< std::setprecision(6) << std::setw(13) << t_sim 	<< " s";
        ss << ", t wall: " 		        << std::fixed 		<< std::setprecision(6) << std::setw(13) << t_wall  << " min";
        ss << ", dt: " 		            << std::scientific 	<< std::setprecision(6) << std::setw(13) << dt 	    << " s";
        ss << ", dt_wall/(np*nts): "    << std::scientific  << std::setprecision(6) << std::setw(13)
        << ( (tn == 0 || dtn == 0 || np_tot == 0) ? 0. : 60.*dt_wall/static_cast<double>(dtn*np_tot) )          << " s";

        std::cout << ss.str() << std::endl;
      }

    t_wall_last = t_wall;
    tn_last = tn;

  } // no barrier

}



Simulation::Simulation(
    const Settings::Settings& s
)
: n_ts_(s.dict()->get<Settings::Dictionary>("simulation").get<uint64_t>("n timesteps"))
, tn_info_(s.dict()->get<Settings::Dictionary>("simulation").get<uint64_t>("info interval",1))
, core_(s)
{}

Simulation::~Simulation() = default;
