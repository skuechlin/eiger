/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * MyMPI_DSDEComm2.cpp
 *
 *  Created on: Dec 14, 2017
 *      Author: kustepha
 */

#include "MyMPI.h"
#include "MyMPI_DSDEComm2.h"

#include "Primitives/Timeout.h"

namespace MyMPI
{

  void DSDECommSpec2::Icomm(
      MPI_Request* const sendreqs,
      MPI_Request* const recvreqs,
      const void* const sendbuffer,
      void* const recvbuffer,
      const size_t buffer_stride_bytes,
      const MPI_Datatype mpi_type,
      int tag)
  const
  {

    // check input
    if (outdegree() > 0){
        MYASSERT(sendreqs   != nullptr,"passed null as send requests");
        MYASSERT(sendbuffer != nullptr,"passed null as send buffer");
    }
    if (indegree() > 0) {
        MYASSERT(recvreqs   != nullptr,"passed null as receive requests");
        MYASSERT(recvbuffer != nullptr,"passed null as receive buffer");
    }

    {

      MPI_Count true_lb, true_extent;
      MPI_Type_get_true_extent_x(mpi_type,&true_lb, &true_extent);

      MYASSERT(true_lb==0,"mpi_type not supported");
      MYASSERT(size_t(true_extent)==buffer_stride_bytes,"mpi_type extent, buffer stride mismatch");

    }

    // enforce valid tag
    tag = valid_tag(tag);

    // post receives
    for (int i = 0; i < indegree(); ++i)
      {
        const int       src   = neighbor_sources_[i];
        const int       count = neighbor_recvcounts_[i];
        const int       displ = neighbor_recvdispls_[i];
        char* const     recvb = (char*)recvbuffer + size_t(displ)*buffer_stride_bytes;

        MYASSERT(src >= 0,         "invalid source"             );
        MYASSERT(src < n_ranks(),  "invalid source"             );
        MYASSERT(count > 0,        "<= 0 receive count"         );
        MYASSERT(displ >=0,        "<0 receive displacement"    );
        MYASSERT(recvb != nullptr, "recv buffer is null"        );

        int err = MPI_Irecv( recvb, count, mpi_type, src, tag, comm_, recvreqs + i );
        MYASSERT(err == MPI_SUCCESS,std::to_string(i) + "th Irecv failed with error: " + errorstring(err));
      }

    // post sends
    for (int i = 0; i < outdegree(); ++i)
      {
        const int           dest  = neighbor_destinations_[i];
        const int           count = neighbor_sendcounts_[i];
        const int           displ = neighbor_senddispls_[i];
        const char* const   sendb = (const char*)sendbuffer + size_t(displ)*buffer_stride_bytes;

        MYASSERT(dest >= 0,        "invalid destination"  );
        MYASSERT(dest < n_ranks(), "invalid destination"  );
        MYASSERT(count > 0,        "<= 0 send count"      );
        MYASSERT(displ >=0,        "<0 send displacement" );
        MYASSERT(sendb != nullptr, "send buffer is null"  );

        int err = MPI_Issend( sendb, count, mpi_type, dest, tag, comm_, sendreqs + i );
        MYASSERT(err == MPI_SUCCESS,std::to_string(i) + "th Isend failed with error: " + errorstring(err));
      }


  }

  void DSDECommSpec2::comm(
      const void* const sendbuffer,
      void* const recvbuffer,
      const size_t buffer_stride_bytes,
      const MPI_Datatype mpi_type,
      const int tag)
  const
  {
    const uint64_t n = indegree() + outdegree();


    if (n > 0)
      {
        std::vector<MPI_Request> reqs(n);
        MPI_Request* sendreqs = reqs.data();
        MPI_Request* recvreqs = reqs.data() + outdegree();
        Icomm(sendreqs, recvreqs, sendbuffer, recvbuffer, buffer_stride_bytes, mpi_type, tag);

        int err;
        err = MPI_Waitall(n,reqs.data(),MPI_STATUSES_IGNORE);

        MYASSERT(err == MPI_SUCCESS,"waiting for comm failed with error: " + errorstring(err));


      } // if n > 0

  }

}
