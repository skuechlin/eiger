/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * MyMPI_DSDEComm2.h
 *
 *  Created on: Dec 6, 2017
 *      Author: kustepha
 */

#ifndef SRC_PARALLEL_MYMPI_DSDECOMM2_H_
#define SRC_PARALLEL_MYMPI_DSDECOMM2_H_

#include <mpi.h>

#include <vector>
#include <deque>
#include <queue>
#include <iomanip>
#include <numeric>
#include <iostream>

#include "Parallel/MyMPI_CommSpec.h"
#include "Parallel/MyMPI_DSDE.h"

#include "Primitives/MyError.h"
#include "Primitives/CRC32.h"

#include "MyMPI.h"


namespace MyMPI
{

  class DSDECommSpec2 : public CommSpec
  {

  public:

    void Icomm(
        MPI_Request* sendreqs,
        MPI_Request* recvreqs,
        const void* sendbuffer,
        void* recvbuffer,
        const size_t buffer_stride_bytes,
        const MPI_Datatype mpi_type,
        const int tag)
    const;

    virtual void comm(
        const void* const sendbuffer,
        void* const recvbuffer,
        const size_t buffer_stride_bytes,
        const MPI_Datatype mpi_type,
        const int tag)
    const override;

  private:

    void initialize(const int tag)
    {
      // reduce receive count

      Scalar_Contiguous_Sender<int> sender(
          outdegree_,
          neighbor_destinations_.data(),
          neighbor_sendcounts_.data());

//      Scalar_Sorted_Incremental_Receiver<int> receiver;
      Scalar_Preallocated_Receiver<int> receiver(nranks_);

      MDSDE(sender, receiver, tag, MPI_INT32_T, comm_ );
      //DSDE(sender, receiver, tag, MPI_INT32_T, comm_ );

      indegree_ = receiver.indegree();

      if (indegree_ > 0)
        {
          neighbor_sources_.resize(indegree_);
          neighbor_recvcounts_.resize(indegree_);

          // extract receive sources and counts
          total_recv_count_ = 0;
          int isrc = 0;
          for (int i = 0; i < nranks_; ++i)
            {
              if (receiver.sources[i])
                {
                  neighbor_sources_[isrc]       = i;
                  neighbor_recvcounts_[isrc]    = receiver.buffer[i];
                  total_recv_count_             += receiver.buffer[i];
                  ++isrc;
                }
            }
//          int isrc = 0;
//          while(!receiver.srcval.empty())
//            {
//              const auto& src = receiver.srcval.top();
//              neighbor_sources_[isrc]       = src.first;
//              neighbor_recvcounts_[isrc]    = src.second;
//              total_recv_count_             += src.second;
//              ++isrc;
//              receiver.srcval.pop();
//            }

          // calculate receive displacements
          neighbor_recvdispls_.resize(indegree_);
          neighbor_recvdispls_[0] = 0;
          for (int i = 1; i < indegree_; ++i)
            neighbor_recvdispls_[i] =
                neighbor_recvdispls_[i-1] +
                neighbor_recvcounts_[i-1];
        }


      //      std::stringstream ss;
      //      print(ss);
      //      std::cout << ss.str() << "\n" << "tag       :             " << valid_tag(tag,comm_) << "\n" << std::endl;
    }



  public:
    template<typename CountT>
    DSDECommSpec2(
        const CountT* const sendcounts,
        const MPI_Comm comm,
        const int tag)
        : CommSpec(sendcounts, comm)
          { initialize(tag); }

    template<typename CountT>
    DSDECommSpec2(
        const CountT* const sendcounts,
        const CountT* const senddispls,
        const MPI_Comm comm,
        const int tag)
        : CommSpec(sendcounts, senddispls, comm)
          { initialize(tag); }

    template<typename RankNumT,typename CountT>
    DSDECommSpec2(
        const int num_destinations,
        const RankNumT* const destinations,
        const CountT* const sendcounts,
        const MPI_Comm comm,
        const int tag)
        : CommSpec(num_destinations,destinations,sendcounts,comm)
          { initialize(tag); }

    virtual ~DSDECommSpec2() = default;
  };

} /* namespace MyMPI */

#endif /* SRC_PARALLEL_MYMPI_DSDECOMM2_H_ */
