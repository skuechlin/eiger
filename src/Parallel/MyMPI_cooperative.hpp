/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * MyMPI_cooperative.hpp
 *
 *  Created on: Jun 30, 2016
 *      Author: kustepha
 */

#ifndef SRC_PARALLEL_MYMPI_COOPERATIVE_HPP_
#define SRC_PARALLEL_MYMPI_COOPERATIVE_HPP_

#include <mpi.h>

#include "Primitives/CRC32.h"
#include "Primitives/MyError.h"


namespace MyMPI
{

  namespace Cooperative
  {


    template<typename ReqT, typename RespT>
    RespT
    requests(const ReqT value, const uint64_t dest_rank, int tag, const MPI_Comm comm = MPI_COMM_WORLD)
    {
      //  MPI_Sendrecv(const void *sendbuf, int sendcount, MPI_Datatype sendtype,
      //      int dest, int sendtag, void *recvbuf, int recvcount,
      //      MPI_Datatype recvtype, int source, int recvtag,
      //      MPI_Comm comm, MPI_Status *status)

      RespT ret;

      tag = valid_tag(tag);

      MPI_Send(&value,1,MyMPI::mpi_type<ReqT>::type,dest_rank,tag,comm);

      MPI_Recv(&ret,1,MyMPI::mpi_type<RespT>::type,dest_rank,tag,comm,MPI_STATUS_IGNORE);

      //      MPI_Sendrecv(&value, 1, MyMPI::mpi_type<ReqT>::type,
      //                   dest_rank, tag, &ret, 1,
      //                   MyMPI::mpi_type<RespT>::type, dest_rank, tag,
      //                   comm, MPI_STATUS_IGNORE);

      return ret;
    }

    template<typename ReqT, typename ReqHandlerT, typename RespT = typename std::result_of<ReqHandlerT(ReqT)>::type>
    void
    responds(const MPI_Status& request,
             ReqHandlerT&& handle,
             const MPI_Comm comm = MPI_COMM_WORLD )
    {
      ReqT request_value;
      MPI_Recv(&request_value,1,MyMPI::mpi_type<ReqT>::type,request.MPI_SOURCE,request.MPI_TAG,comm,MPI_STATUS_IGNORE);

      RespT response_value = handle(request_value);
      MPI_Send(&response_value,1, MyMPI::mpi_type<RespT>::type, request.MPI_SOURCE, request.MPI_TAG, comm);
    }


    template<typename ReqT, typename RankFunT, typename ReqHandlerT, typename RespT = typename std::result_of<ReqHandlerT(ReqT)>::type>
    RespT
    requests_cooperative(const ReqT value, int tag, const int rank, RankFunT&& get_dest_rank, ReqHandlerT&& handle, const MPI_Comm comm = MPI_COMM_WORLD)
    {

      const int dest_rank = get_dest_rank(value);

      tag = valid_tag(tag);

      if (rank == dest_rank)
        return handle(value); // handle on this rank
      else
        return requests<ReqT,RespT>(value, dest_rank, tag, comm); // request from dest_rank

    }




    template<typename RootTaskT, typename ReqHandlerT>
    void cooperate(
        RootTaskT&& root_task,
        ReqHandlerT&& handle,
        const int root,
        MPI_Comm comm,
        const int tag)
    {

      int rank;
      MPI_Comm_rank(comm,&rank);

      int n_ranks;
      MPI_Comm_size(comm,&n_ranks);

      const int done_tag = valid_tag(COMPILE_TIME_CRC32_STR( FILE_POS_STR ) + tag);

      // root will now perform root_task,
      // other ranks will spin and serve request on prompt

      //      MPI_Request barrier_request;

      if (rank == root)
        {
          root_task();

          MPI_Request* done_reqs = (MPI_Request*)malloc((n_ranks-1)*sizeof(MPI_Request));

          // notify done
          int ireq = 0;
          for (int r = 0; r < n_ranks; ++r)
            if (r != root) // don't notify ourselves...
              MPI_Issend(0,0,MPI_DOUBLE,r,done_tag,comm,&done_reqs[ireq++]);

          double timeout = 10.; //s
          Waitall(
              n_ranks-1,done_reqs,MPI_STATUSES_IGNORE,
              timeout,"cooperation completion on root");

          free(done_reqs);


          //          MPI_Ibarrier(comm, &barrier_request);
        }
      else
        {
          //          MPI_Ibarrier(comm, &barrier_request);

          //          int barrier_done = 0;
          int done = 0;

          while (!done) // spin until root is done
            {
              // check for message in inbox (non-blocking)

              int pending = 0;
              MPI_Status request;
              MPI_Iprobe(root,MPI_ANY_TAG,comm,&pending,&request);

              if (pending)
                {
                  if (request.MPI_TAG == done_tag)
                    {
                      MPI_Recv(0,0,MPI_DOUBLE,root,done_tag,comm,MPI_STATUS_IGNORE);
                      done = true;
                    }
                  else
                    handle(request);
                }

              // check if all other ranks (in particular root) have entered the barrier
              // if so, barrier_done will be true and while loop will terminate

              //          int MPI_Test(MPI_Request *request, int *flag, MPI_Status *status)
              //          flag:   True if operation completed (logical).
              //              MPI_Test(&barrier_request, &barrier_done, MPI_STATUS_IGNORE);
            }
        }

    } // cooperate

  } // namespace cooperative

} // namespace MyMPI


#endif /* SRC_PARALLEL_MYMPI_COOPERATIVE_HPP_ */
