/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * MyThreads.hpp
 *
 *  Created on: May 21, 2014
 *      Author: Stephan Kuechlin
 */

#ifndef MYTHREADS_HPP_
#define MYTHREADS_HPP_

#include<omp.h>

namespace MyThreads {

	inline
	void
	setNumThreads()
	{
		int thread_lim = 1;

		char* mt = getenv("OMP_NUM_THREADS");
		if (mt) // get thread constraint from environment
			thread_lim = std::max( strtol(mt,NULL,0), (long int)(1) );
		else // get from system
		{
			thread_lim = omp_get_num_procs();
//			thread_lim = std::max(
//					std::thread::hardware_concurrency(),(unsigned int)(1) );
		}

		omp_set_num_threads(thread_lim);
	}


} /* namespace MyThreads */




#endif /* MYTHREADS_HPP_ */
