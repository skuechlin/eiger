/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * MyMPI_DSDE.h
 *
 *  Created on: Jan 29, 2018
 *      Author: kustepha
 */

#ifndef SRC_PARALLEL_MYMPI_DSDE_H_
#define SRC_PARALLEL_MYMPI_DSDE_H_

#include <mpi.h>

#include "Primitives/MyError.h"

namespace MyMPI
{

  template<class ReceiverT, class SenderT>
  void DSDE(
      SenderT&& sender,
      ReceiverT&& receiver,
      const int tag,
      const MPI_Datatype type,
      const MPI_Comm comm)
  {

    // extract buffer stride from mpi type
    MPI_Aint lb;
    MPI_Aint extent;
    MPI_Type_get_extent(type,&lb,&extent);

    const int vtag = valid_tag(tag);

    const int outdegree = sender.outdegree();

    //      std::vector<MPI_Request> reqs(outdegree, MPI_REQUEST_NULL); // we send outdegree messages
    MPI_Request* reqs = (MPI_Request*)alloca(outdegree*sizeof(MPI_Request));

    if (outdegree)
      MYASSERT( reqs != nullptr, "failed to allocate requests");

    // send the messages, non-blocking, synchronous
    // synchronous sends complete only after they have been matched by receive
    for (int i = 0; i < outdegree; ++i)
      sender.Issend(i,type,extent,vtag,comm,&reqs[i]);

    MPI_Request barrier_request = MPI_REQUEST_NULL;
    int done = 0; // true iff all ranks completed communication
    int barrier_active = 0; // true iff all sends matched by receive

    while (!done)
      {
        // check for message in inbox
        int have_message = 0;
        MPI_Status status;
        MPI_Iprobe(MPI_ANY_SOURCE,vtag,comm,&have_message,&status);

        if (have_message){
            MYASSERT(status.MPI_SOURCE != MPI_PROC_NULL, "received message has MPI_PROC_NULL source");
            receiver.receive(&status,type,comm);
        }

        if (!barrier_active) // not all sends done, not in barrier yet
          {
            // check if all sends have completed
            int all_sent = 0;

            if (outdegree > 0)
              MPI_Testall(outdegree,reqs,&all_sent,MPI_STATUSES_IGNORE);
            else
              all_sent = 1;

            if (all_sent)
              { // all sends have been matched by receive, enter the non-blocking barrier
                MPI_Ibarrier(comm, &barrier_request);
                barrier_active = 1;
              }
          }
        else
          { // check if all other procs have entered the barrier
            // if so, barrier_done will be true and while loop will terminate
            int barrier_done = 0;
            MPI_Test(&barrier_request, &barrier_done, MPI_STATUS_IGNORE);
            if (barrier_done)
              done = true;
          }

      } // while barrier not done

  }



  template<class ReceiverT, class SenderT>
  void MDSDE(
      SenderT&& sender,
      ReceiverT&& receiver,
      const int tag,
      const MPI_Datatype type,
      const MPI_Comm comm)
  {
    // extract buffer stride from mpi type
    MPI_Aint lb;
    MPI_Aint extent;
    MPI_Type_get_extent(type,&lb,&extent);

    const int vtag = valid_tag(tag);

    const int outdegree = sender.outdegree();

    //      std::vector<MPI_Request> reqs(outdegree, MPI_REQUEST_NULL); // we send outdegree messages
    MPI_Request* reqs = (MPI_Request*)alloca(outdegree*sizeof(MPI_Request));

    if (outdegree)
      MYASSERT( reqs != nullptr, "failed to allocate requests");

    // send the messages, non-blocking, synchronous
    // synchronous sends complete only after they have been matched by receive
    for (int i = 0; i < outdegree; ++i)
      sender.Issend(i,type,extent,vtag,comm,&reqs[i]);

    MPI_Request barrier_request = MPI_REQUEST_NULL;
    int done = 0; // true iff all ranks completed communication
    int barrier_active = 0; // true iff all sends matched by receive

    while (!done)
      {
        // check for message in inbox
        int have_message = 0;
        MPI_Status status;
        MPI_Message message;
        MPI_Improbe(MPI_ANY_SOURCE,vtag,comm,&have_message,&message,&status);

        if (have_message){
            MYASSERT(status.MPI_SOURCE != MPI_PROC_NULL, "received message has MPI_PROC_NULL source");
            receiver.receive(&message,&status,type);
        }

        if (!barrier_active) // not all sends done, not in barrier yet
          {
            // check if all sends have completed
            int all_sent = 0;

            if (outdegree > 0)
              MPI_Testall(outdegree,reqs,&all_sent,MPI_STATUSES_IGNORE);
            else
              all_sent = 1;

            if (all_sent)
              { // all sends have been matched by receive, enter the non-blocking barrier
                MPI_Ibarrier(comm, &barrier_request);
                barrier_active = 1;
              }
          }
        else
          { // check if all other procs have entered the barrier
            // if so, barrier_done will be true and while loop will terminate
            int barrier_done = 0;
            MPI_Test(&barrier_request, &barrier_done, MPI_STATUS_IGNORE);
            if (barrier_done)
              done = true;
          }

      } // while barrier not done


  }


  template<typename T>
  struct Scalar_Sorted_Incremental_Receiver {
    using TT = typename std::pair<int,T>;
    struct comp { bool operator()(const TT& a, const TT& b){return a.first > b.first;} };
    std::priority_queue<TT,std::deque<TT>,comp> srcval;
    void receive(MPI_Message* message, const MPI_Status* const stat, const MPI_Datatype type)
    {
      T value;
      const int source = stat->MPI_SOURCE;
      MPI_Mrecv(&value,1,type,message,MPI_STATUS_IGNORE);
      srcval.emplace(source,value);
    }
    void receive(const MPI_Status* const stat, const MPI_Datatype type, const MPI_Comm comm)
    {
      T value;
      const int source = stat->MPI_SOURCE;
      const int tag = stat->MPI_TAG;
      MPI_Recv(&value,1,type,source,tag,comm,MPI_STATUS_IGNORE);
      srcval.emplace(source,value);
    }
    int indegree() const { return srcval.size(); }

    Scalar_Sorted_Incremental_Receiver() = default;
  };

  template<typename T>
  struct Scalar_Preallocated_Receiver {
    std::vector<int> sources;
    std::vector<T> buffer;
    void receive(MPI_Message* message, const MPI_Status* const stat, const MPI_Datatype type)
    {
      const int source = stat->MPI_SOURCE;
      MPI_Mrecv(&buffer[source],1,type,message,MPI_STATUS_IGNORE);
      sources[source] = 1;
    }
    void receive(const MPI_Status* const stat, const MPI_Datatype type, const MPI_Comm comm)
    {
      const int source = stat->MPI_SOURCE;
      const int tag = stat->MPI_TAG;
      MPI_Recv(&buffer[source],1,type,source,tag,comm,MPI_STATUS_IGNORE);
      sources[source] = 1;
    }

    int indegree() const {
      int indeg = 0;
      for (uint64_t i = 0; i < sources.size(); ++i)
        indeg += !!sources[i];
      return indeg;
    }

    Scalar_Preallocated_Receiver(const size_t sz): sources(sz,0), buffer(sz) {};
  };

  template<typename T>
  struct Scalar_Contiguous_Sender {
    const int        odegree;
    const int *const destinations;
    const T *const   sendbuffer;
    int outdegree() const { return odegree; }
    void Isend(const int dest_ind, MPI_Datatype type, const int, const int tag, MPI_Comm comm, MPI_Request* req) {
      MPI_Isend( &sendbuffer[dest_ind], 1, type, destinations[dest_ind], tag, comm, req ); }
    void Issend(const int dest_ind, MPI_Datatype type, const int, const int tag, MPI_Comm comm, MPI_Request* req) {
      MPI_Issend( &sendbuffer[dest_ind], 1, type, destinations[dest_ind], tag, comm, req ); }

    Scalar_Contiguous_Sender(const int _outdegree, const int* const _destinations, const T *const _sendbuffer)
    : odegree(_outdegree), destinations(_destinations), sendbuffer(_sendbuffer) {};
  };

}




#endif /* SRC_PARALLEL_MYMPI_DSDE_H_ */
