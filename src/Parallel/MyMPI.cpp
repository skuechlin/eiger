/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * MyMPI.cpp
 *
 *  Created on: Jul 2, 2014
 *      Author: ?
 */



#include <omp.h>
#include <cstdint>
#include <memory>
#include <iostream>

#include "Primitives/Timeout.h"

#include "MyMPI_DSDEComm2.h"

#include "MyMPI.h"


MyMPI::MPIMgr::MPIMgr()
: comm_(MPI_COMM_WORLD)
, nRanks_(MyMPI::numRanks(comm_))
, rank_(MyMPI::rank(comm_)) // id of this node
{
  MPI_Comm_set_errhandler(MPI_COMM_WORLD, MPI_ERRORS_RETURN);
}


MyMPI::MPIMgr::~MPIMgr() = default;


// print world size and max num threads on each proc.
// must be called on all MPI procs!
void
MyMPI::MPIMgr::printInfo()
const
{
  int32_t n_threads;
  std::vector<int32_t> places;

#pragma omp parallel
  {
#pragma omp single
    {
      n_threads = omp_get_num_threads();
      places.resize(n_threads);
    }
    int tnum = omp_get_thread_num();
    places[tnum] = omp_get_place_num();
  }

  int32_t rt = root();

  DSDECommSpec2 commspec(1,&rt,&n_threads,comm(),COMPILE_TIME_CRC32_STR( FILE_POS_STR ));

  std::vector<int32_t> vec_of_places(commspec.n_to_receive(),0);
  commspec.comm(places.data(),vec_of_places.data(),sizeof(int32_t),MPI_INT32_T,COMPILE_TIME_CRC32_STR( FILE_POS_STR ));

  if (isRoot())
    {
      std::cout << "running " << nRanks() << " MPI ranks" << std::endl;
      uint64_t place = 0;
      for (int i = 0; i < commspec.indegree(); ++ i)
        {
          int recvcount =  commspec.receivecount(i);
          std::cout << "  rank " << i << " will use " << recvcount << " threads at places:";
          for (int j = 0; j < recvcount; ++j)
            std::cout << " " << vec_of_places.at(place++);
          std::cout<< std::endl;
        }
      std::cout << std::endl;
    }

}


uint64_t
MyMPI::rank(const MPI_Comm& comm)
{
  int rank;
  MPI_Comm_rank(comm,&rank);
  return rank;
}

void*
MyMPI::getAttribute(const MPI_Comm& comm, const int attr)
{
  void* v;
  int flag;
  MPI_Comm_get_attr(comm,attr,&v,&flag);
  MYASSERT(flag,"no attr associated with " + std::to_string(attr));
  return v;
}


bool
MyMPI::isRoot(const MPI_Comm& comm, const int& root)
{
  if (comm == MPI_COMM_NULL)
    return true;

  int rank;
  MPI_Comm_rank(comm,&rank);
  return rank == root;
}

uint64_t
MyMPI::numRanks(const MPI_Comm& comm)
{
  int sz;
  MPI_Comm_size(comm,&sz);
  return sz;
}


int MyMPI::Waitall(const int n, MPI_Request* reqs, MPI_Status* stats,
                   const double timeout, const std::string& timeout_msg)
{
  if (timeout <= 0.)
    {
      return MPI_Waitall(n,reqs,stats);
    }
  else
    {
      using dsec = std::chrono::duration<double>;

      return Timeout::wait_for(
          [n,reqs,stats]()->int{return MPI_Waitall(n,reqs,stats);},
          dsec(timeout),
          [&timeout_msg](const dsec& t){
            MYASSERT(false,"timeout after waiting " + std::to_string(t.count()) + "s for " + timeout_msg);
          });
    }
}

