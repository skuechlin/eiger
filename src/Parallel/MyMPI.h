/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * MyMPI.h
 *
 *  Created on: Jul 2, 2014
 *      Author: Stephan Küchlin
 */

#ifndef MYMPI_H_
#define MYMPI_H_

#include <mpi.h>
#include <iostream>

namespace MyMPI
{

  template<typename T> struct mpi_type {  };

  template<> struct mpi_type<bool>     { static constexpr MPI_Datatype type = MPI_CXX_BOOL; };
  template<> struct mpi_type<uint8_t>  { static constexpr MPI_Datatype type = MPI_UINT8_T;  };
  template<> struct mpi_type<int32_t>  { static constexpr MPI_Datatype type = MPI_INT32_T;  };
  template<> struct mpi_type<int64_t>  { static constexpr MPI_Datatype type = MPI_INT64_T;  };
  template<> struct mpi_type<uint32_t> { static constexpr MPI_Datatype type = MPI_UINT32_T; };
  template<> struct mpi_type<uint64_t> { static constexpr MPI_Datatype type = MPI_UINT64_T; };
  template<> struct mpi_type<double>   { static constexpr MPI_Datatype type = MPI_DOUBLE;   };


  class MPIMgr {

    static constexpr uint64_t root_ = 0;

    MPI_Comm comm_;

    uint64_t nRanks_;

    uint64_t rank_;


  public:

    // return id of root
        uint64_t root() const { return root_; }

    // return the number of ranks
        uint64_t nRanks() const { return nRanks_; }

        // return the rank of this mpi process
        uint64_t rank() const { return rank_; }

        // return true if this rank is root
        bool isRoot() const { return rank_ == root_; }

        // return communicator
        MPI_Comm comm() const { return comm_; }


        // print world size and max num threads on each rank.
        // collective!
        void printInfo() const;


        MPIMgr();

        ~MPIMgr();
  };


  inline
  void
  barrier() { MPI_Barrier(MPI_COMM_WORLD); }

  bool
  isRoot(const MPI_Comm& comm, const int& root);

  uint64_t
  numRanks(const MPI_Comm& comm);

  uint64_t
  rank(const MPI_Comm& comm);


  void* getAttribute(const MPI_Comm& comm, const int attr);

  inline int max_mpi_tag() {
    static int max_tag = *(int*)getAttribute(MPI_COMM_WORLD,MPI_TAG_UB);
    return max_tag;
  }

  inline int valid_tag(const int tag) {
    //return static_cast<uint64_t>(tag) % static_cast<uint64_t>(max_mpi_tag(comm));
    return static_cast<uint64_t>(tag) % uint64_t(32767);
  }

  inline
  std::string errorstring(int error) {
    std::string errstring;
    int len;
    char str[MPI_MAX_ERROR_STRING];
    MPI_Error_string(error,str,&len);
    errstring.assign(str,str+len);
    return errstring;
  }


  int Waitall(
      const int n, MPI_Request* reqs, MPI_Status* stats = MPI_STATUSES_IGNORE,
      const double timeout = 0., const std::string& timeout_msg = "MyMPI::Waitall()" );

  inline int Wait(MPI_Request* req, MPI_Status* stat = MPI_STATUS_IGNORE,
                  const double timeout = 0., const std::string& timeout_msg = "MyMPI::Wait()" )
  { return Waitall(1,req,stat,timeout,timeout_msg); }

} // namespace MyMPI

inline std::ostream& operator<<(std::ostream& os, const MPI_Status& stat) {
  int cnt;
  MPI_Get_count(&stat,MPI_DOUBLE,&cnt);
  int cancelled;
  MPI_Test_cancelled(&stat,&cancelled);
  return os << "source: " << stat.MPI_SOURCE << ", tag: " << stat.MPI_TAG <<
      ", err: " << stat.MPI_ERROR << " errstr: " << MyMPI::errorstring(stat.MPI_ERROR) <<
      ", count (MPI_DOUBLE): " << cnt << ", cancelled?: " << cancelled;
}


// include implementation
#include "MyMPI_cooperative.hpp"


#endif /* MYMPI_H_ */
