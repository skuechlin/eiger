/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * MyMPI_CommSpec.h
 *
 *  Created on: Nov 6, 2017
 *      Author: kustepha
 */

#ifndef SRC_PARALLEL_MYMPI_COMMSPEC_H_
#define SRC_PARALLEL_MYMPI_COMMSPEC_H_

#include <mpi.h>
#include <vector>
#include <algorithm>
#include <sstream>

#include "MyMPI.h"

namespace MyMPI {

  struct SendCommSpec{

    enum Full_t {FULL};
    enum Sparse_t {SPARSE};

    int outdegree_= 0; // number of destination ranks of this rank
    int total_send_count_ = 0;

    // assign 0 as default element to avoid nullptrs
    std::vector<int> neighbor_destinations_    = {0};
    std::vector<int> neighbor_sendcounts_      = {0};
    std::vector<int> neighbor_senddispls_      = {0};

  private:

    void set_total_sendcount()
    {
      total_send_count_  = 0;
      for (int i = 0; i < outdegree_; ++i)
        total_send_count_ += neighbor_sendcounts_[i];
    }
    void set_senddispls_contiguous()
    {
      // compute send displacements
      if (outdegree_>0)
        {
          neighbor_senddispls_.resize(outdegree_);
          neighbor_senddispls_[0] = 0;
          for (int i = 1; i < outdegree_; ++i)
            neighbor_senddispls_[i] =
                neighbor_senddispls_[i-1] +
                neighbor_sendcounts_[i-1];
        }
    }

  public:

    template<typename CountT>
    SendCommSpec(
        const int nranks,
        const CountT* const sendcounts)
    {
      // extract non zero send specs
      outdegree_ = 0;
      for (int i = 0; i < nranks; ++i)
        outdegree_ += !!(sendcounts[i]);

      if (outdegree_ > 0)
        {
          neighbor_destinations_.resize(outdegree_);
          neighbor_sendcounts_.resize(outdegree_);

          int msgnum = 0;
          for (int i = 0; i < nranks; ++i)
            if (sendcounts[i]>0)
              {
                neighbor_destinations_[msgnum] = i;
                neighbor_sendcounts_[msgnum] = sendcounts[i];
                ++msgnum;
              }
        }

      set_senddispls_contiguous();
      set_total_sendcount();
    }

    template<typename CountT>
    SendCommSpec(
        const int           nranks,
        const CountT* const sendcounts,
        const CountT* const senddispls,
        Sparse_t)
    {


      // extract non zero send specs
      outdegree_ = 0;
      for (int i = 0; i < nranks; ++i)
        outdegree_ += !!(sendcounts[i]);

      if (outdegree_ > 0)
        {
          neighbor_destinations_.resize(outdegree_);
          neighbor_sendcounts_.resize(outdegree_);
          neighbor_senddispls_.resize(outdegree_+1);

          int msgnum = 0;
          for (int i = 0; i < nranks; ++i)
            if (sendcounts[i]>0)
              {
                neighbor_destinations_[msgnum] = i;
                neighbor_sendcounts_[msgnum] = sendcounts[i];
                neighbor_senddispls_[msgnum] = senddispls[i];
                ++msgnum;
              }
        }

      set_total_sendcount();
    }

    template<typename RankNumT,typename CountT>
    SendCommSpec(
        const int               num_destinations,
        const RankNumT* const   destinations,
        const CountT*   const   sendcounts,
        Full_t)
        : outdegree_(num_destinations) {

      if (outdegree_ > 0)
        {
          neighbor_destinations_.assign(destinations,destinations+outdegree_);
          neighbor_sendcounts_.assign(sendcounts,sendcounts+outdegree_);
        }

      set_senddispls_contiguous();
      set_total_sendcount();
    }

    template<typename RankNumT,typename CountT>
    SendCommSpec(
        const int               num_destinations,
        const RankNumT* const   destinations,
        const CountT*   const   sendcounts,
        const CountT*   const   senddispls)
        : outdegree_(num_destinations) {
      if (outdegree_ > 0)
        {
          neighbor_destinations_.assign(destinations,destinations+outdegree_);
          neighbor_sendcounts_.assign(sendcounts,sendcounts+outdegree_);
          neighbor_senddispls_.assign(senddispls,senddispls+outdegree_);
        }

      set_total_sendcount();
    }

    virtual ~SendCommSpec() = default;

  };


  struct RecevieCommSpec{

    int indegree_ = 0; // number of ranks with this rank as destination
    int total_recv_count_ = 0;

    // assign 0 as default element to avoid nullptrs
    std::vector<int> neighbor_sources_         = {0};
    std::vector<int> neighbor_recvcounts_      = {0};
    std::vector<int> neighbor_recvdispls_      = {0};

    virtual ~RecevieCommSpec() = default;

  };

  struct CommSpec: public SendCommSpec, public RecevieCommSpec {

    MPI_Comm comm_ = MPI_COMM_NULL;

    int rank_;
    int nranks_;

  public:

    int rank() const {return rank_; }
    int n_ranks() const { return nranks_; }
    int n_to_send() const { return total_send_count_; }
    int n_to_receive() const { return total_recv_count_; }

    int outdegree() const {return outdegree_; }
    int indegree() const { return indegree_; }

    int destination(const int i) const { return neighbor_destinations_[i]; }
    int sendcount(const int i) const { return neighbor_sendcounts_[i]; }

    int source(const int i) const { return neighbor_sources_[i]; }
    int receivecount(const int i) const { return neighbor_recvcounts_[i]; }

    virtual void comm(
        const void* const sendbuffer,
        void* const recvbuffer,
        const size_t buffer_stride_bytes,
        const MPI_Datatype mpi_type,
        const int tag) const = 0;

  public:

    virtual void print(std::stringstream& ss) const {
      ss
      << "rank:                   " << rank_              << "\n"
      << "nranks:                 " << nranks_            << "\n"
      << "total_send_count_:      " << total_send_count_  << "\n"
      << "total_recv_count_:      " << total_recv_count_  << "\n"
      << "indegree_:              " << indegree_          << "\n"
      << "outdegree_:             " << outdegree_         << "\n"
      << "neighbor_destinations_: ";
      std::for_each(neighbor_destinations_.begin(),neighbor_destinations_.end(),
                    [&ss](auto&& elem)->void{ss << elem << " ";}); ss << "\n";
      ss
      << "neighbor_sendcounts_:   ";
      std::for_each(neighbor_sendcounts_.begin(),neighbor_sendcounts_.end(),
                    [&ss](auto&& elem)->void{ss << elem << " ";}); ss << "\n";
      ss
      << "neighbor_senddispls_:   ";
      std::for_each(neighbor_senddispls_.begin(),neighbor_senddispls_.end(),
                    [&ss](auto&& elem)->void{ss << elem << " ";}); ss << "\n";
      ss
      << "neighbor_sources_:      ";
      std::for_each(neighbor_sources_.begin(),neighbor_sources_.end(),
                    [&ss](auto&& elem)->void{ss << elem << " ";}); ss << "\n";
      ss
      << "neighbor_recvcounts_:   ";
      std::for_each(neighbor_recvcounts_.begin(),neighbor_recvcounts_.end(),
                    [&ss](auto&& elem)->void{ss << elem << " ";}); ss << "\n";
      ss
      << "neighbor_recvdispls_:   ";
      std::for_each(neighbor_recvdispls_.begin(),neighbor_recvdispls_.end(),
                    [&ss](auto&& elem)->void{ss << elem << " ";}); ss << "\n";
    }

  private:

    void init_comm(const MPI_Comm comm )
    {
      comm_ = comm;
      MPI_Comm_rank(comm_,&rank_);
      MPI_Comm_size(comm_,&nranks_);
    }

  public:

    template<typename CountT>
    CommSpec(
        const CountT* const sendcounts,
        const MPI_Comm comm)
    : SendCommSpec(MyMPI::numRanks(comm),sendcounts)  {
      init_comm(comm);
    }

    template<typename CountT>
    CommSpec(
        const CountT* const sendcounts,
        const CountT* const senddispls,
        const MPI_Comm comm)
    : SendCommSpec(MyMPI::numRanks(comm),sendcounts,senddispls,SPARSE)  {
      init_comm(comm);
    }

    template<typename RankNumT,typename CountT>
    CommSpec(
        const int               num_destinations,
        const RankNumT* const   destinations,
        const CountT*   const   sendcounts,
        const MPI_Comm comm)
        : SendCommSpec(num_destinations,destinations,sendcounts,FULL) {
      init_comm(comm);
    }

    template<typename RankNumT,typename CountT>
    CommSpec(
        const int num_destinations,
        const RankNumT* const destinations,
        const CountT* const sendcounts,
        const CountT* const senddispls,
        const MPI_Comm comm)
        : SendCommSpec(num_destinations,destinations,sendcounts,senddispls) {
      init_comm(comm);
    }

    virtual ~CommSpec() = default;

  };


}



#endif /* SRC_PARALLEL_MYMPI_COMMSPEC_H_ */
