/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * MyMPI_AlltoallComm.h
 *
 *  Created on: Nov 17, 2017
 *      Author: kustepha
 */

#ifndef SRC_PARALLEL_MYMPI_ALLTOALLCOMM_H_
#define SRC_PARALLEL_MYMPI_ALLTOALLCOMM_H_

#include <mpi.h>
#include <vector>
#include "MyMPI_CommSpec.h"
#include "Primitives/MyError.h"

namespace MyMPI
{

  struct AlltoallCommSpec: public CommSpec {

  private:

    std::vector<int> sendcounts_ = {0};
    std::vector<int> senddispls_ = {0};
    std::vector<int> recvcounts_ = {0};
    std::vector<int> recvdispls_ = {0};

  public:

    virtual void comm(
        const void* sendbuffer,
        void* recvbuffer,
        const size_t /*buffer_stride_bytes*/,
        MPI_Datatype mpi_type,
        int /*tag*/)
    const override {
      MPI_Alltoallv(sendbuffer,sendcounts_.data(),senddispls_.data(),mpi_type,
                    recvbuffer,recvcounts_.data(),recvdispls_.data(),mpi_type,comm_);
    }

  private:


    void initialize() // neighbor sendcounts and neighbor destinations known at this point
    {
      senddispls_[0] = 0;
      for (int i = 0; i < n_ranks(); ++i)
        senddispls_[i+1] = senddispls_[i] + sendcounts_[i];

      MPI_Alltoall(sendcounts_.data(),1,MPI_INT32_T,recvcounts_.data(),1,MPI_INT32_T,comm_);

      indegree_ = 0;
      recvdispls_[0] = 0;
      for (int i = 0; i < n_ranks(); ++i)
        {
          indegree_ += !!(recvcounts_[i]);
          recvdispls_[i+1] = recvdispls_[i] + recvcounts_[i];
        }
      total_recv_count_ = recvdispls_[n_ranks()];

      if (indegree_ > 0)
        {
          neighbor_sources_.resize(indegree_);
          neighbor_recvcounts_.resize(indegree_);
          neighbor_recvdispls_.resize(indegree_+1);

          neighbor_recvdispls_[0] = 0;
          int n = 0;
          for (int i = 0; i < n_ranks(); ++i)
            {
              if (recvcounts_[i])
                {
                  neighbor_sources_[n]      = i;
                  neighbor_recvcounts_[n]   = recvcounts_[i];
                  neighbor_recvdispls_[n+1] = neighbor_recvdispls_[n] + recvcounts_[i];
                  ++n;
                }
            }

        }

    }

  public:

    template<typename CountT>
    AlltoallCommSpec(const CountT* const sendcounts, MPI_Comm comm)
    : CommSpec(sendcounts, comm)
      , sendcounts_(sendcounts,sendcounts+n_ranks())
      , senddispls_(n_ranks()+1,0)
      , recvcounts_(n_ranks(),0)
      , recvdispls_(n_ranks()+1,0)
      { initialize(); }

    template<typename RankNumT,typename CountT>
    AlltoallCommSpec(
        const int num_destinations,
        const RankNumT* const destinations,
        const CountT* const sendcounts,
        MPI_Comm comm)
        : CommSpec(num_destinations,destinations,sendcounts,comm)
          , sendcounts_(n_ranks(),0)
          , senddispls_(n_ranks()+1,0)
          , recvcounts_(n_ranks(),0)
          , recvdispls_(n_ranks()+1,0)
          {
      for (int i = 0; i < num_destinations; ++i)
        sendcounts_[destinations[i]] = sendcounts[i];
      initialize(); }


    virtual ~AlltoallCommSpec() = default;

  };



} // namespace MyMPI

#endif /* SRC_PARALLEL_MYMPI_ALLTOALLCOMM_H_ */
