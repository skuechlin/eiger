/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * MyMPI_NeighborComm.h
 *
 *  Created on: Oct 26, 2017
 *      Author: kustepha
 */

#ifndef SRC_PARALLEL_MYMPI_NEIGHBORCOMM_H_
#define SRC_PARALLEL_MYMPI_NEIGHBORCOMM_H_

#include <mpi.h>
#include <vector>
#include <iostream>

#include "MyMPI.h"
#include "MyMPI_CommSpec.h"
#include "Primitives/MyError.h"

namespace MyMPI
{



  struct NeighborCommSpec: public CommSpec {

    MPI_Comm ncomm_ = MPI_COMM_NULL;

  public:

    virtual void comm(
        const void* const sendbuffer,
        void* const recvbuffer,
        const size_t /*buffer_stride_bytes*/,
        const MPI_Datatype mpi_type,
        const int /*tag*/)
    const
    override
    {

      // MPI_Neighbor_alltoallv
      // - All processes send different amounts of data to,
      //   and receive different amounts of data from, all neighbors
      //
      //      int MPI_Neighbor_alltoallv(const void *sendbuf, const int sendcounts[],
      //          const int sdispls[], MPI_Datatype sendtype,
      //          void *recvbuf, const int recvcounts[],
      //          const int rdispls[], MPI_Datatype recvtype, MPI_Comm comm)

      MPI_Request request;
      int err;

      MYASSERT(ncomm_ != MPI_COMM_NULL,"tried to perfrom neighbor comm on COMM_NULL");
      MYASSERT(sendbuffer != nullptr, "passed nullptr sendbuffer");
      MYASSERT(recvbuffer != nullptr, "passed nullptr receivebuffer");
      MYASSERT(neighbor_sendcounts_.data() != nullptr, "passed nullptr sendcounts");
      MYASSERT(neighbor_senddispls_.data() != nullptr, "passed nullptr senddispls");
      MYASSERT(neighbor_recvcounts_.data() != nullptr, "passed nullptr recvcounts");
      MYASSERT(neighbor_recvdispls_.data() != nullptr, "passed nullptr recvdispls");

      err = MPI_Ineighbor_alltoallv(
          sendbuffer,
          neighbor_sendcounts_.data(),
          neighbor_senddispls_.data(),
          mpi_type,
          recvbuffer,
          neighbor_recvcounts_.data(),
          neighbor_recvdispls_.data(),
          mpi_type,
          ncomm_,
          &request
      );

      if (err != MPI_SUCCESS)
        {
          std::stringstream ss;
          ss << "on rank " << rank_ << ": indegree " << indegree_ << " outdegree " << outdegree_ << std::endl;
          print(ss);
          std::cout << ss.str() << std::endl;
          MYASSERT(err == MPI_SUCCESS, " error calling MPI_Ineighbor_alltoallv");
        }

      err = MPI_Wait(&request,MPI_STATUS_IGNORE);

      if (err != MPI_SUCCESS)
        {
          std::stringstream ss;
          ss << "on rank " << rank_ << ": indegree " << indegree_ << " outdegree " << outdegree_ << std::endl;
          print(ss);
          std::cout << ss.str() << std::endl;
          MYASSERT(err == MPI_SUCCESS, " error calling MPI_Wait");
        }

    }


  private:

    void create_dist_graph()
    {
      if (ncomm_ != MPI_COMM_NULL && ncomm_ != MPI_COMM_WORLD)
        MPI_Comm_free(&ncomm_);

      // create distributed graph communicator
      MPI_Dist_graph_create(
          comm_,    // old communicator
          (outdegree_ > 0) ? 1 : 0,        // one source node (this rank)
              &rank_,   // array of source nodes (only one element: this rank)
              &outdegree_, // array of number of destinations for for each source node (one elem.)
              neighbor_destinations_.data(), // array of destinations for each source
              neighbor_sendcounts_.data(), // weights for each destination (sendcounts)
              MPI_INFO_NULL,
              0, // do not reorder
              &ncomm_ // the new communicator
      );

      MYASSERT( ncomm_ != MPI_COMM_NULL, "dist graph create failed to create communicator");
    }

    void initialize()
    {

      create_dist_graph();

      // obtain source information
      int outdegree;
      int weighted;
      MPI_Dist_graph_neighbors_count(ncomm_,&indegree_,&outdegree,&weighted);

      // sanity check
      MYASSERT(outdegree == outdegree_, "outdegree mismatch in neighbor comm spec ctor");

      if (indegree_ > 0)
        {
          // allocate storage to obtain receive counts
          neighbor_sources_.resize(indegree_);
          neighbor_recvcounts_.resize(indegree_);
        }

      // temporaries:
      std::vector<int> dests(std::max(outdegree_,1),0);
      std::vector<int> destweights(std::max(outdegree_,1),0);

      MPI_Dist_graph_neighbors(
          ncomm_,
          std::max(indegree_,1),
          neighbor_sources_.data(),
          neighbor_recvcounts_.data(),
          std::max(outdegree_,1),
          dests.data(), // should match neighbor_destinations_ after call
          destweights.data() // should match neighbor_sendcounts_ after call
      );

      // sanity check
      for (int i = 0; i < outdegree_; ++i)
        MYASSERT(dests[i] == neighbor_destinations_[i],"destination mismatch in neighbor graph");

      // obtain total receive count and
      // calculate receive displacements
      // make sure received data arrives sorted by rank

      std::vector<int> perm(indegree_+1);
      std::iota(perm.begin(),perm.end(),0);

      std::sort(perm.begin(),perm.end()-1,[this](const int a, const int b)->bool{
        return neighbor_sources_[a] < neighbor_sources_[b]; });

      neighbor_recvdispls_.resize(indegree_+1);
      neighbor_recvdispls_[perm[0]] = 0;
      for (int i = 0; i < indegree_; ++i)
        neighbor_recvdispls_[perm[i+1]] = neighbor_recvdispls_[perm[i]] + neighbor_recvcounts_[perm[i]];

      total_recv_count_ = neighbor_recvdispls_[indegree_];

    }

  public:

    template<typename CountT>
    NeighborCommSpec(const CountT* const sendcounts, MPI_Comm comm)
    : CommSpec(sendcounts,comm)
      { initialize(); }

    template<typename RankNumT,typename CountT>
    NeighborCommSpec(
        const int num_destinations,
        const RankNumT* const destinations,
        const CountT* const sendcounts,
        MPI_Comm comm)
        : CommSpec(num_destinations,destinations,sendcounts,comm)
          { initialize(); }

    virtual ~NeighborCommSpec() {
      if (ncomm_ != MPI_COMM_NULL && ncomm_ != MPI_COMM_WORLD)
        MPI_Comm_free(&ncomm_); }

  };



} // namespace MyMPI


#endif /* SRC_PARALLEL_MYMPI_NEIGHBORCOMM_H_ */
