/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * MortonCodes.h
 *
 *  Created on: Aug 27, 2014
 *      Author: kustepha
 */

#ifndef MORTONCODES_H_
#define MORTONCODES_H_

#include <cstdint>

namespace MortonCodes
{

  namespace
  {

    // "Insert" a 0 bit after each of the 16 low bits of x
    inline
    uint32_t
    part1By1(uint32_t x)
    {
#ifdef __BMI2__
      x = _pdep_u32(x,0x55555555);
#else
      x &= 0x0000ffff;                  // x = ---- ---- ---- ---- fedc ba98 7654 3210
      x = (x ^ (x <<  8)) & 0x00ff00ff; // x = ---- ---- fedc ba98 ---- ---- 7654 3210
      x = (x ^ (x <<  4)) & 0x0f0f0f0f; // x = ---- fedc ---- ba98 ---- 7654 ---- 3210
      x = (x ^ (x <<  2)) & 0x33333333; // x = --fe --dc --ba --98 --76 --54 --32 --10
      x = (x ^ (x <<  1)) & 0x55555555; // x = -f-e -d-c -b-a -9-8 -7-6 -5-4 -3-2 -1-0
#endif
      return x;
    }

    // Inverse of part1By1 - "delete" all odd-indexed bits
    inline
    uint32_t
    compact1By1(uint32_t x)
    {
#ifdef __BMI2__
      x = _pext_u32(x,0x55555555);
#else
      x &= 0x55555555;                  // x = -f-e -d-c -b-a -9-8 -7-6 -5-4 -3-2 -1-0
      x = (x ^ (x >>  1)) & 0x33333333; // x = --fe --dc --ba --98 --76 --54 --32 --10
      x = (x ^ (x >>  2)) & 0x0f0f0f0f; // x = ---- fedc ---- ba98 ---- 7654 ---- 3210
      x = (x ^ (x >>  4)) & 0x00ff00ff; // x = ---- ---- fedc ba98 ---- ---- 7654 3210
      x = (x ^ (x >>  8)) & 0x0000ffff; // x = ---- ---- ---- ---- fedc ba98 7654 3210
#endif
      return x;
    }

    // "Insert" a 0 bit after each of the 32 low bits of x
    inline
    uint64_t
    part1By1(uint64_t x)
    {
#ifdef __BMI2__
      x = _pdep_u64(x,0x5555555555555555);
#else
                                                    //		       7	     6	       5         4         3         2         1	     0
      x &= 0x000000000ffffffff;              		// x = ---- ---- ---- ---- ---- ---- ---- ---- vuts rqpo nmlk jihg fedc ba98 7654 3210
      x = (x ^ (x << 16))  & 0x0000ffff0000ffff; 	// x = ---- ---- ---- ---- vuts rqpo nmlk jihg ---- ---- ---- ---- fedc ba98 7654 3210
      x = (x ^ (x <<  8))  & 0x00ff00ff00ff00ff; 	// x = ---- ---- vuts rqpo ---- ---- nmlk jihg ---- ---- fedc ba98 ---- ---- 7654 3210
      x = (x ^ (x <<  4))  & 0x0f0f0f0f0f0f0f0f; 	// x = ---- vuts ---- rqpo ---- nmlk ---- jihg ---- fedc ---- ba98 ---- 7654 ---- 3210
      x = (x ^ (x <<  2))  & 0x3333333333333333; 	// x = --vu --ts --rq --po --nm --lk --ji --hg --fe --dc --ba --98 --76 --54 --32 --10
      x = (x ^ (x <<  1))  & 0x5555555555555555; 	// x = -v-u -t-s -r-q -p-o -n-m -l-k -j-i -h-g -f-e -d-c -b-a -9-8 -7-6 -5-4 -3-2 -1-0
#endif
      return x;
    }

    // Inverse of part1By1 - "delete" all odd-indexed bits
    inline
    uint64_t
    compact1By1(uint64_t x)
    {
#ifdef __BMI2__
      x = _pext_u64(x,0x5555555555555555);
#else
                                                    //		       7	     6	       5         4         3         2         1	     0
      x &= 0x5555555555555555;              		// x = -v-u -t-s -r-q -p-o -n-m -l-k -j-i -h-g -f-e -d-c -b-a -9-8 -7-6 -5-4 -3-2 -1-0
      x = (x ^ (x >>  1))  & 0x3333333333333333; 	// x = --vu --ts --rq --po --nm --lk --ji --hg --fe --dc --ba --98 --76 --54 --32 --10
      x = (x ^ (x >>  2))  & 0x0f0f0f0f0f0f0f0f; 	// x = ---- vuts ---- rqpo ---- nmlk ---- jihg ---- fedc ---- ba98 ---- 7654 ---- 3210
      x = (x ^ (x >>  4))  & 0x00ff00ff00ff00ff; 	// x = ---- ---- vuts rqpo ---- ---- nmlk jihg ---- ---- fedc ba98 ---- ---- 7654 3210
      x = (x ^ (x >>  8))  & 0x0000ffff0000ffff; 	// x = ---- ---- ---- ---- vuts rqpo nmlk jihg ---- ---- ---- ---- fedc ba98 7654 3210
      x = (x ^ (x >> 16))  & 0x00000000ffffffff; 	// x = ---- ---- ---- ---- ---- ---- ---- ---- vuts rqpo nmlk jihg fedc ba98 7654 3210
#endif
      return x;
    }


    // "Insert" two 0 bits after each of the 10 low bits of x
    inline
    uint32_t
    part1By2(uint32_t x)
    {
#ifdef __BMI2__
      x = _pdep_u32(x,0x09249249);
#else
                                        //			   3		 2		   1		 0
      x &= 0x000003ff;                  // x = ---- ---- ---- ---- ---- --98 7654 3210
      x = (x ^ (x << 16)) & 0x030000ff; // x = ---- --98 ---- ---- ---- ---- 7654 3210
      x = (x ^ (x <<  8)) & 0x0300f00f; // x = ---- --98 ---- ---- 7654 ---- ---- 3210
      x = (x ^ (x <<  4)) & 0x030c30c3; // x = ---- --98 ---- 76-- --54 ---- 32-- --10
      x = (x ^ (x <<  2)) & 0x09249249; // x = ---- 9--8 --7- -6-- 5--4 --3- -2-- 1--0
#endif
      return x;
    }

    // Inverse of part1By2 - "delete" all bits not at positions divisible by 3
    inline
    uint32_t
    compact1By2(uint32_t x)
    {
#ifdef __BMI2__
      x = _pext_u32(x,0x09249249);
#else
                                        //			   3		 2		   1		 0
      x &= 0x09249249;					// x = ---- 9--8 --7- -6-- 5--4 --3- -2-- 1--0
      x = (x ^ (x >>  2)) & 0x030c30c3; // x = ---- --98 ---- 76-- --54 ---- 32-- --10
      x = (x ^ (x >>  4)) & 0x0300f00f; // x = ---- --98 ---- ---- 7654 ---- ---- 3210
      x = (x ^ (x >>  8)) & 0x030000ff; // x = ---- --98 ---- ---- ---- ---- 7654 3210
      x = (x ^ (x >> 16)) & 0x000003ff; // x = ---- ---- ---- ---- ---- --98 7654 3210
#endif
      return x;
    }

    // "Insert" two 0 bit after each of the 21 low bits of x
    inline
    uint64_t
    part1By2(uint64_t x)
    {
#ifdef __BMI2__
      x = _pdep_u64(x,0x1249249249249249);
#else
      //                                            		       7	     6	       5         4         3         2         1	     0
      x &= 0x00000000001fffff;              		// x = ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---k jihg fedc ba98 7654 3210
      x = (x ^ (x << 32))  & 0x001f00000000ffff; 	// x = ---- ---- ---k jihg ---- ---- ---- ---- ---- ---- ---- ---- fedc ba98 7654 3210
      x = (x ^ (x << 16))  & 0x001f0000ff0000ff; 	// x = ---- ---- ---k jihg ---- ---- ---- ---- fedc ba98 ---- ---- ---- ---- 7654 3210
      x = (x ^ (x <<  8))  & 0x100f00f00f00f00f; 	// x = ---k ---- ---- jihg ---- ---- fedc ---- ---- ba98 ---- ---- 7654 ---- ---- 3210
      x = (x ^ (x <<  4))  & 0x10c30c30c30c30c3; 	// x = ---k ---- ji-- --hg ---- fe-- --cd ---- ba-- --98 ---- 76-- --54 ---- 32-- --10
      x = (x ^ (x <<  2))  & 0x1249249249249249; 	// x = ---k --j- -i-- h--g --f- -e-- d--c --b- -a-- 9--8 --7- -6-- 5--4 --3- -2-- 1--0
#endif
      return x;
    }

    // Inverse of part1By2 - "delete" all bits not at positions divisible by 3
    inline
    uint64_t
    compact1By2(uint64_t x)
    {
#ifdef __BMI2__
      x = _pext_u64(x,0x1249249249249249);
#else
      //		                                                    7	     6	       5         4         3         2         1	     0
      x &= 0x1249249249249249;	             		// x = ---k --j- -i-- h--g --f- -e-- d--c --b- -a-- 9--8 --7- -6-- 5--4 --3- -2-- 1--0
      x = (x ^ (x >>  2))  & 0x10c30c30c30c30c3; 	// x = ---k ---- ji-- --hg ---- fe-- --cd ---- ba-- --98 ---- 76-- --54 ---- 32-- --10
      x = (x ^ (x >>  4))  & 0x100f00f00f00f00f; 	// x = ---k ---- ---- jihg ---- ---- fedc ---- ---- ba98 ---- ---- 7654 ---- ---- 3210
      x = (x ^ (x >>  8))  & 0x001f0000ff0000ff; 	// x = ---- ---- ---k jihg ---- ---- ---- ---- fedc ba98 ---- ---- ---- ---- 7654 3210
      x = (x ^ (x >> 16))  & 0x001f00000000ffff; 	// x = ---- ---- ---k jihg ---- ---- ---- ---- ---- ---- ---- ---- fedc ba98 7654 3210
      x = (x ^ (x >> 32))  & 0x00000000001fffff; 	// x = ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---k jihg fedc ba98 7654 3210
#endif
      return x;
    }
  }

  // 2D

  template<typename IntT>
  inline IntT decode2j(IntT m) { return compact1By1(m); }

  template<typename IntT>
  inline IntT decode2i(IntT m) { return compact1By1(m >> 1); }

  template<typename IntT>
  inline IntT encode(const IntT i, const IntT j) { return (part1By1(i) << 1) ^ part1By1(j); }

  template<typename IntT>
  inline void decode(IntT& i, IntT& j, const IntT m) { i = decode2i(m); j = decode2j(m); }

  // 3D

  template<typename IntT>
  inline IntT decode3k(IntT m) { return compact1By2(m);      }

  template<typename IntT>
  inline IntT decode3j(IntT m) { return compact1By2(m >> 1); }

  template<typename IntT>
  inline IntT decode3i(IntT m) { return compact1By2(m >> 2); }

  template<typename IntT>
  inline IntT encode(const IntT i, const IntT j, const IntT k)
  {
    return (part1By2(i) << 2) ^ (part1By2(j) << 1) ^ part1By2(k);
  }

  template<typename IntT>
  inline void decode(IntT& i, IntT& j, IntT& k, const IntT m)
  {
    i = decode3i(m);
    j = decode3j(m);
    k = decode3k(m);
  }




} //namespace

#endif /* MORTONCODES_H_ */
