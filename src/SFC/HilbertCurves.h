/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * HilbertCurves.h
 *
 *  Created on: Mar 7, 2017
 *      Author: kustepha
 */

#ifndef SRC_SFC_HILBERTCURVES_H_
#define SRC_SFC_HILBERTCURVES_H_


#include <cstdint>

#include "Primitives/MyArithm.h"

#include "HilbertLUTs/HilbertLUTs.h"

namespace HilbertCurves
{


  template<typename CurveLUT>
  struct Hilbert {

      static constexpr uint8_t shift_inc = CurveLUT::nd;
      static constexpr uint8_t shift_lim = -shift_inc;
      static constexpr uint8_t cycle     = CurveLUT::start_period * shift_inc;
      static constexpr uint8_t offset    = 64%shift_inc;
      static constexpr uint8_t mask      = (1<<shift_inc)-1;

    private:

      static inline constexpr uint64_t maxshift(const uint64_t x)
      {
        return 64 - ( cycle*( (MyArithm::nlz(x) - offset) / cycle ) + offset + shift_inc);
      }

    public:


      static inline uint64_t morton2hilbert(const uint64_t m) {

        uint64_t h     = 0;
        uint8_t  state = CurveLUT::start_state;

        for (uint8_t shift = maxshift(m); shift != shift_lim; shift -= shift_inc)
        {
          const uint8_t orthant = (m >> shift) & mask;                          // get orthant
          h    ^= uint64_t(CurveLUT::state2keyByOrthant(state,orthant)) << shift;  // save key
          state = CurveLUT::state2stateByOrthant(state,orthant);                   // state transition
        }

        return h;
      }

      static inline uint64_t hilbert2morton(const uint64_t h)
      {
        uint64_t m     = 0;
        uint8_t  state = CurveLUT::start_state;

        for (uint8_t shift = maxshift(h); shift != shift_lim; shift -= shift_inc)
        {
          const uint8_t key      = (h >> shift) & mask;                 // get key
          const uint8_t orthant = CurveLUT::state2orthantByKey(state,key); // get orthant
          m    ^= uint64_t(orthant) << shift;                           // save orthant
          state = CurveLUT::state2stateByOrthant(state,orthant);           // state transition
        }

        return m;
      }

  }; // struct Hilbert


} // namespace HilbertCurves





#endif /* SRC_SFC_HILBERTCURVES_H_ */
