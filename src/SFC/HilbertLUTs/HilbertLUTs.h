/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

#ifndef __HilbertLUTs_h__
#define __HilbertLUTs_h__
#include "./Hilbert_Alfa_LUT.h"
#include "./Hilbert_Beta_LUT.h"
#include "./Hilbert_Butz_LUT.h"
#include "./Hilbert_Ca00_c43_LUT.h"
#include "./Hilbert_Ca00_c44_LUT.h"
#include "./Hilbert_Ca00_c47_LUT.h"
#include "./Hilbert_Ca00_c49_LUT.h"
#include "./Hilbert_Ca00_c4b_LUT.h"
#include "./Hilbert_Ca00_c4d_LUT.h"
#include "./Hilbert_Ca00_c4E_LUT.h"
#include "./Hilbert_Ca00_c4J_LUT.h"
#include "./Hilbert_Ca00_c4P_LUT.h"
#include "./Hilbert_Ca00_c4X_LUT.h"
#include "./Hilbert_Ca00_ch7_LUT.h"
#include "./Hilbert_Ca00_chI_LUT.h"
#include "./Hilbert_Ca00_cTb_LUT.h"
#include "./Hilbert_Ca00_cTC_LUT.h"
#include "./Hilbert_Ca00_cTI_LUT.h"
#include "./Hilbert_Harmonious_LUT.h"
#include "./Hilbert_Sasburg_LUT.h"
#include "./Hilbert_TwoD_LUT.h"
#endif
