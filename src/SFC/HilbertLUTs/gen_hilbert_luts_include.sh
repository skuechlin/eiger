#!/bin/sh

HEADER=HilbertLUTs.h
DIR="$(dirname "${BASH_SOURCE[0]}")"

rm ${DIR}/$HEADER

echo "#ifndef __HilbertLUTs_h__" > $HEADER
echo "#define __HilbertLUTs_h__" >> $HEADER


#DIR="$(cd -P "$(dirname "${BASH_SOURCE[0]}")" && pwd)"


for file in ${DIR}/*.h
do
  if [ "$file" == "${DIR}/$HEADER" ]
  then
    :
  else
    echo "#include \"$file\"" >> $HEADER
  fi
done
echo "#endif" >> $HEADER
