/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * ExactBisection_Cooperative.h
 *
 *  Created on: Jun 28, 2016
 *      Author: kustepha
 */

#ifndef EXACTBISCETION_COOPERATIVE_H_
#define EXACTBISCETION_COOPERATIVE_H_

#include <LoadBalancing/ExactBisection.h>
#include <mpi.h>
#include <vector>

#include "Parallel/MyMPI.h"

#include "Primitives/MySearch.h"
#include "Primitives/CRC32.h"

namespace LoadBalance
{

  namespace ExactBisection
  {

    namespace Cooperative
    {
      // returns new bottleneck value on root,
      // new and old splitters on all ranks
      inline
      double
      exactBisection(
          uint64_t s_new[],     // return:  new separator positions. s_new[i] will hold the index of the first task assigned to partition i
          uint64_t s_old[],     // return:  old separator positions
          uint64_t W[],         // in:      exclusive prefix sum of task weights on this rank: W[0] = 0, W[1] = w[0], W[2] = w[0] + w[1]
          const uint64_t wmaxl, // in:      the maximum task weight on this rank
          const uint64_t N,     // in:      the number of tasks on this rank, also the length -1 of array W
          const uint64_t P,     // in:      the number of partitions, also the length -1 of arrays s_new and s_old
          const int root,
          const MPI_Comm comm,
          const int tag
      )
      {

        // get MPI info

        // rank of this process
        int rank;
        MPI_Comm_rank(comm,&rank);

        // is this rank the root process
        bool amroot = (rank == root);


        // convert local prefix sum W[] to part of global distributed prefix sum

        uint64_t offset;
        MPI_Exscan(&W[N],&offset,1,MPI_UINT64_T,MPI_SUM,comm);
        if (!amroot)
          {
            for (uint64_t i = 0; i < N+1; ++i)
              W[i] += offset;
          }

        // the first element of W[] of each rank, only relevant on root
        std::vector<uint64_t> WW(amroot ? P : 0, 0);
        MPI_Gather(&W[0],1,MPI_UINT64_T,WW.data(),1,MPI_UINT64_T,root,comm);


        /*---------------------------------------------------------------------------------*/
        // old splitters, compute on all ranks

        // first get number of tasks on each rank
        MPI_Allgather(&N,1,MPI_UINT64_T,s_old,1,MPI_UINT64_T,comm);

        // convert to prefix sum
        uint64_t sum = 0;
        for (uint64_t i = 0; i < P; ++i)
          {
            uint64_t tmp = s_old[i];
            s_old[i] = sum;
            sum += tmp;
          }
        s_old[P] = sum;

        // total number of tasks
        uint64_t Ntot = s_old[P];

        // task number offset of this rank
        uint64_t mys = s_old[rank];

        /*---------------------------------------------------------------------------------*/
        // root will now perform distributed exact bisection algorithm,
        // other ranks will spin and serve request on prompt

        int W_TAG       = MyMPI::valid_tag(tag + COMPILE_TIME_CRC32_STR( FILE_POS_STR ));
        int SEARCH_TAG  = MyMPI::valid_tag(tag + COMPILE_TIME_CRC32_STR( FILE_POS_STR ));

        MYASSERT(W_TAG != SEARCH_TAG,"duplicate tag!");

        // helpers

        auto handle_W_request           = [&W,mys](const uint64_t gind)->uint64_t{
          return W[ gind - mys ]; };

        auto handle_search_request      = [&W,N,mys](const double B)->uint64_t{
          return mys + MySearch::binary_search_lt(W,W+N+1,B); };


        // cooperative helpers, only relevant on root

        auto get_rank_by_global_index   = [&s_old,P](const uint64_t gind)->uint64_t{
          uint64_t rank = MySearch::binary_search_lt(s_old,s_old+P,gind);
          MYASSERT(rank < P,"invalid rank");
          return  rank;};

        auto get_W                      = [rank,W_TAG,&get_rank_by_global_index,&handle_W_request,&comm]
                                           (const uint64_t gind)->uint64_t{
          return MyMPI::Cooperative::requests_cooperative<uint64_t>(
              gind,W_TAG,rank,get_rank_by_global_index,handle_W_request,comm); };

        auto get_rank_by_value          = [&WW,P](const double B)->uint64_t{
          uint64_t rank = MySearch::binary_search_lt(WW.begin(),WW.end(),B);
          MYASSERT(rank < P,"invalid rank");
          return  rank;};

        auto binary_search_lt           = [rank,SEARCH_TAG,&get_rank_by_value,&handle_search_request,&comm]
                                           (const double B)->uint64_t{
          return MyMPI::Cooperative::requests_cooperative<double>(
              B,SEARCH_TAG,rank,get_rank_by_value,handle_search_request,comm); };


        // exact bisection kernels, only relevant on root

        // gather some info

        // the maximum task weight across all ranks, only relevant on root
        uint64_t wmax = 0;
        MPI_Reduce(&wmaxl,&wmax,1,MPI_UINT64_T,MPI_MAX,root,comm);

        // the total task weight of all tasks (currently known to the last rank
        // only relevant on root
        uint64_t wtot = 0;
        const int ttag = MyMPI::valid_tag(tag + COMPILE_TIME_CRC32_STR( FILE_POS_STR ));
        if (uint64_t(rank) == P-1)
          {
            if (rank == root)
              wtot = W[N];
            else
              MPI_Send(&W[N],1,MPI_UINT64_T,root,ttag,comm);
          }
        else if (rank == root)
          {
            MPI_Recv(&wtot,1,MPI_UINT64_T,P-1,ttag,comm,MPI_STATUS_IGNORE);
          }
        //        MPI_Bcast(&wtot,1,MPI_UINT64_T,P-1,comm);

        // the average partition load, only relevant on root
        double Lavg = static_cast<double>( wtot ) / static_cast<double>( P );

        // aux vectors, only relevant on root
        std::vector<uint64_t> Ws;
        if (rank == root)
          {
            Ws.resize(P+1);      // global distributed array W evaluated at splitter positions
            Ws[0] = 0;
            Ws[P] = wtot;                       // the total weight of all tasks across all ranks
          }


        auto lr_probe       = [&binary_search_lt,&get_W,wtot](
            uint64_t s[],       // return:  separator positions. s[i] will hold the index of the first task assigned to partition i
            uint64_t Ws[],      // in/out:  exclusive prefix sum of task weights: W[0] = 0, W[1] = w[0], W[2] = w[0] + w[1]
            //          evaluated at splitter positions s
            const uint64_t N,   // in:      the number of tasks, also the length of array W -1
            const uint64_t P,   // in:      the number of partitions, also the length of array s -1
            const double B )    // in:      the bottle neck value, to be checked for feasibility
            ->bool{

          static_cast<void>(N);

          uint64_t sum = 0;
          for (uint64_t p = 1; p < P; ++p)
            {
              s[p] = binary_search_lt(sum+B);

              Ws[ p ] = get_W(s[p]);
              sum = Ws[ p ];
            }
          return (sum + B) >= wtot;
        };


        auto bottleneck = []( const uint64_t s[], const uint64_t Ws[], const uint64_t P )
                    ->uint64_t{

          static_cast<void>(s);

          uint64_t bn = 0;
          for (uint64_t p = 1; p <= P; ++p) // note <=
            bn = std::max( Ws[p] - Ws[p-1], bn );

          return bn;
        };


        auto min_bid = [&get_W]( const uint64_t s[], const uint64_t Ws[], const uint64_t P )
                    ->uint64_t {

          uint64_t mb = Ws[P] - Ws[P-1]; // load of last processor, does not bid
          for (uint64_t p = 1; p < P; ++p) // note <
            {
              uint64_t Wsp1 = get_W(s[p]+1);
              mb = std::min( Wsp1 - Ws[p-1], mb );
            }

          return mb;
        };

        // the bottleneck value to be computed on root
        double b;

        auto root_task = [&b,&s_new,&Ws,Lavg,wmax,Ntot,P,&lr_probe,&bottleneck,&min_bid,&get_W]()->void{

          LoadBalance::ExactBisection::exactBisection(
              s_new,        // return:  separator positions.
              //          s[i] will hold the index of the first task assigned to partition i
              Ws.data(),    // in:      exclusive prefix sum of task weights:
              //          W[0] = 0, W[1] = w[0], W[2] = w[0] + w[1]
              //          evaluated at splitter positions
              Lavg,         // in:      the average partition load
              wmax,         // in:      maximum task weight
              Ntot,         // in:      the number of tasks, also the length of array W -1
              P,            // in:      the number of partitions, also the length of array s -1
              lr_probe,
              bottleneck,
              min_bid
          );

          // method relaxPartition might have changed splitters, so update Ws and return final bottleneck
          for (uint64_t p = 0; p < P; ++p)
            Ws[p] = get_W(s_new[p]);

          b = bottleneck(s_new,Ws.data(),P);

        };

        auto request_handler = [SEARCH_TAG,W_TAG,&handle_search_request,&handle_W_request,comm](const MPI_Status& request)->void{

          // identify request
          if (request.MPI_TAG == SEARCH_TAG)
            {
              // process request
              MyMPI::Cooperative::responds<double>(request,handle_search_request,comm);
            }
          else if( request.MPI_TAG == W_TAG)
            {
              // process request
              MyMPI::Cooperative::responds<uint64_t>(request,handle_W_request,comm);
            }
          else
            {
              MYASSERT(false,
                       "found msg with tag " + std::to_string(request.MPI_TAG) + ", expected "
                       + std::to_string(SEARCH_TAG) + " or " + std::to_string(W_TAG) );
            }

        };


        /*---------------------------------------------------------------------------------*/
        // perform cooperative task
        MyMPI::Cooperative::cooperate(root_task,request_handler,root,comm,tag);


        /*---------------------------------------------------------------------------------*/

        // broadcast the new splitters to all ranks
        MPI_Bcast(s_new,P+1,MPI_UINT64_T,root,comm);

        //        // broadcast the bottleneck value to all tasks
        //        MPI_Bcast(&b,1,MPI_DOUBLE,root,comm);



        return b;

      }

    } // namespace Cooperative

  } // namespace ExactBisection

}

#endif /* EXACTBISCETION_COOPERATIVE_H_ */
