/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * ExactBisection.h
 *
 *  Created on: Jun 27, 2016
 *      Author: kustepha
 */

#ifndef EXACTBISECTION_H_
#define EXACTBISECTION_H_

#include <iostream>

#include "Primitives/MySearch.h"

namespace LoadBalance
{

  namespace ExactBisection
  {


    inline
    bool
    lr_probe(
        uint64_t s[],       // return:  separator positions. s[i] will hold the index of the first task assigned to partition i
        const uint64_t W[], // in:      exclusive prefix sum of task weights: W[0] = 0, W[1] = w[0], W[2] = w[0] + w[1]
        const uint64_t N,   // in:      the number of tasks, also the length of array W -1
        const uint64_t P,   // in:      the number of partitions, also the length of array s -1
        const double B )    // in:      the bottle neck value, to be checked for feasibility
    {
      uint64_t sum = 0;
      for (uint64_t p = 1; p < P; ++p)
        {
          s[p] = MySearch::binary_search_lt(W,W+N+1,sum+B);
          sum = W[ s[p] ];
        }
      return (sum + B) >= W[N]; // weight of last partition smaller than probe value?
    }

    inline
    uint64_t
    bottleneck(
        const uint64_t s[],
        const uint64_t W[],
        const uint64_t P)
    {
      uint64_t bn = 0;
      for (uint64_t p = 1; p <= P; ++p) // note <=
        bn = std::max( W[s[p]] - W[s[p-1]], bn );
      return bn;
    }

    inline
    uint64_t
    min_bid(
        const uint64_t s[],
        const uint64_t W[],
        const uint64_t P)
    {
      uint64_t mb = W[s[P]] - W[s[P-1]]; // load of last processor, does not bid
      for (uint64_t p = 1; p < P; ++p) // note <
        mb = std::min( W[s[p]+1] - W[s[p-1]], mb );
      return mb;
    }

    inline
    void
    relaxPartition(uint64_t s[], const uint64_t N, const uint64_t P)
    {
      MYASSERT(s[P] == N, "invalid partitioning, last splitter must equal number of tasks");
      MYASSERT(s[0] == 0, "invalid partitioning, 0th splitter must equal 0");

      for (uint64_t p = std::min(P-1,N-1); p != 0; --p)
        if (s[p] >= s[p+1])
          s[p] = std::max(s[p+1],1ul) - 1;

    }


    template<
    typename LRProbeT       = bool(*)(uint64_t*,const uint64_t*,const uint64_t,const uint64_t,const double),
    typename BottleneckT    = uint64_t(*)(const uint64_t*,const uint64_t*,const uint64_t),
    typename MinBidT        = uint64_t(*)(const uint64_t*,const uint64_t*,const uint64_t) >
    double
    exactBisection(
        uint64_t s[],       // return:  separator positions. s[i] will hold the index of the first task assigned to partition i
        uint64_t W[],       // in:      exclusive prefix sum of task weights: W[0] = 0, W[1] = w[0], W[2] = w[0] + w[1]
        //      note: not const to allow cooperative version to reuse this parameter as in/out
        const double   Lavg,// in:      the average partition load
        const uint64_t wmax,// in:      maximum task weight
        const uint64_t N,   // in:      the number of tasks, also the length of array W -1
        const uint64_t P,   // in:      the number of partitions, also the length of array s -1
        LRProbeT&&      lr_probe    = lr_probe,
        BottleneckT&&   bottleneck  = bottleneck,
        MinBidT&&       min_bid     = min_bid
    )
    {

      MYASSERT(W[0] == 0,"invalid task weight prefix sum: 0th entry not 0!");

      double LB = Lavg;
      double UB = LB + static_cast<double>(wmax);

      s[0] = 0;
      s[P] = N;

      if (N<=P)
        {
          for (uint64_t i = 0; i < N; ++i)
            s[i] = i;
          for (uint64_t i = N; i < P; ++i)
            s[i] = N;
        }
      else if(wmax == 0)
        {
          const uint64_t avg_num_tasks = N/P;
          for (uint64_t i = 0; i < P; ++i)
            s[i] = i*avg_num_tasks;
        }
      else
        {

          bool valid = false;

          while ( UB > LB || !valid )
            {

              double midB = .5*UB + .5*LB;

              valid = lr_probe(s,W,N,P,midB) ;

              //          std::cout << "probe " << midB << ": " << (valid? std::string("succeeded") : std::string("failed"))
              //              << "\ns:       ";
              //          for (uint64_t i = 0; i <= P; ++i)
              //            std::cout << std::setw(2) << s[i] << " ";
              //          std::cout << "\nW[s[i]]: ";
              //          for (uint64_t i = 0; i <= P; ++i)
              //            std::cout << std::setw(2) << W[s[i]] << " ";
              //          std::cout << "\nL[i]:     ";
              //          for (uint64_t i = 1; i <= P; ++i)
              //            std::cout << std::setw(2) << W[s[i]]-W[s[i-1]] << " ";
              //          std::cout << std::endl << std::endl;;

              if ( valid )
                {
                  UB = bottleneck(s,W,P);
                  //              std::cout << "bottleneck: " << UB << std::endl;
                }
              else
                {
                  LB = min_bid(s,W,P);
                  //              std::cout << "min bid: " << LB << std::endl;
                }
            }


          relaxPartition(s,N,P);

        }

      return bottleneck(s,W,P);

    }



  } // namespace ExactBisection



}

#endif /* EXACTBISECTION_H_ */
