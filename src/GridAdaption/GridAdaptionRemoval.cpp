/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * GridAdaptionRemoval.cpp
 *
 *  Created on: Oct 6, 2016
 *      Author: kustepha
 */




#include "GridAdaption.h"

#include "Parallel/MyMPI.h"

#include "Grid/GridManager/GridManager.h"
#include "Grid/GridGhostExchange.h"

void GridAdaption::removeEmptyCells()
const
{
  // perform requested action on particle grid
  auto g = gm_->particleGrid();
  Grids::GridTransform tr( g );

  if (mpim_->isRoot())
    std::cout << "removing empty cells" << std::endl;

  constexpr uint8_t blank_color = 1;

  g->removeRegularCellColors(blank_color);

  g->addColorToBlank(blank_color);

  // communicate color on ghost grid
  exchangeGhostCellColors(COMPILE_TIME_CRC32_STR( FILE_POS_STR ));

  tr.append( Grids::GridTransform::REMOVE, 0, blank_color );

  gm_->transform( tr );

  if (g->nNonGhost() == 0)
    std::cout << "empty rank " << MyMPI::rank(MPI_COMM_WORLD) << std::endl;

}
