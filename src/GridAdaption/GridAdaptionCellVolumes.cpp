/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * EstimateCellVolumes.cpp
 *
 *  Created on: May 17, 2016
 *      Author: kustepha
 */

#include <omp.h>

#include "Grid/GridManager/GridManager.h"
#include "Grid/GridGhostExchange.h"

#include "Boundary/Boundary.h"
#include "Boundary/BoundaryManager/BoundaryManager.h"

#include "Particles/ParticleCollection.h"

#include "Primitives/Partitioning.h"
#include "Primitives/MyString.h"

#include "Parallel/MyMPI.h"

#include "GridAdaption.h"

namespace
{


  // monte-carlo sampling of volumes of colored non-ghost cells
  // side-effects:
  // changes count_ and begin_ vectors
  void sampleColoredCellVolumes(
      Grids::RegularGrid *const g,
      Boundary::BoundaryManager *const bndMgr,
      const uint8_t color)
  {

    std::vector<const Boundary::Boundary*> enabled;
    bndMgr->getEnabledBoundaries(&enabled);

    // enable physical boundaries, disable pure probes, time-zone boundary!
    bndMgr->enablePhysicalBoundaries();

    // individual cell discretization
    constexpr uint64_t ns_per_dim = 10; // number of sub-cells per dimension
    const uint64_t ns = MyArithm::ipow(ns_per_dim,g->topology()->ndims());
    const double nsd = static_cast<double>(ns);

#pragma omp parallel
    {

      // count colored cells and compute prefix for load balancing

#pragma omp for schedule(static)
      for (uint64_t i = g->first_; i < g->last_; ++i)
        g->count_[i] = !!(g->cellColor(i) & color);

#pragma omp single
      {
          g->begin_[g->first_] = 0;
          std::partial_sum(g->count_.begin()+g->first_,
                           g->count_.begin()+g->last_,
                           g->begin_.begin()+g->first_+1);
      }

      // load balance colored cells
      g->lb_nonGhost_on_threads();
      const uint64_t first = g->thread_first();
      const uint64_t last = g->thread_last();

      //      const uint64_t first = g->first_ + MyPartitioning::partitionStart(omp_get_thread_num(),g->last_-g->first_,omp_get_num_threads());
      //      const uint64_t last  = g->first_ + MyPartitioning::partitionEnd(omp_get_thread_num(),g->last_-g->first_,omp_get_num_threads());

      // sample volume of intersected cells
      for ( uint64_t ic = first; ic < last; ++ic)
        {

          if ( g->cellColor(ic) & color )
            {
              // divide into sub-cells and categorize into outside or inside based on center
              // -> classify positions at centers of sub-cells
              uint64_t n_inside = 0;
              for (uint64_t p = 0; p < ns; ++p)
                {
                  const auto pos = g->subCellCenter(ic,p,ns_per_dim);
                  n_inside += !( bndMgr->isOutside( pos ) );
                }

              // since we know that this cell intersects geometry, there might be uncaught
              // volume missed in sampling (all sub-cell centers outside)
              // so ensure minimum cell volume of one sub-cell
              if (n_inside == 0)
                n_inside = 1;

              // set volume
              g->setCellVolume(ic,
                               g->topologicalCellVolume(ic) *
                               static_cast<double>(n_inside)/nsd );
            }
          else // cell not intersected
            g->setCellVolume(ic, g->topologicalCellVolume(ic) ); // set to geometric cell volume


        } // for all (non-ghost) cells

    } // parallel

    // restore boundary enabled state at entry
    bndMgr->enableBoundaries(enabled);


  } // sampleTaggedCellVolumes()


  void blankCellsOutside(
      Grids::RegularGrid *const g,
      Boundary::BoundaryManager *const bndMgr,
      const uint8_t color /* color of intersected cells (defined not outside) */
  )
  {

    std::vector<const Boundary::Boundary*> enabled;
    bndMgr->getEnabledBoundaries(&enabled);

    // enable physical boundaries, disable pure probes, time-zone boundary!
    bndMgr->enablePhysicalBoundaries();

    static const double empty_vol = std::nextafter(Constants::minCellVolume,0.);

    // set volume of not-intersected cells with center outside to zero
#pragma omp parallel for schedule(runtime)
    for ( uint64_t ic = g->first_; ic < g->last_; ++ic)
      {

        if ( !(g->cellColor(ic) & color) && bndMgr->isOutside( g->cellCenter(ic) ) )
          {
            // cell is outside
            g->setCellVolume(ic,empty_vol);

            //                      Eigen::AlignedBox4d bb = (*g)->getTopo()->cellBox(i);
            //
            //                      std::cout << "cell " << i << " outside: " <<  prts.getVar<Particles::R>(0).transpose(); std::cout << std::endl;
            //                      std::cout << "min: " << bb.min().transpose(); std::cout << " max: " << bb.max().transpose(); std::cout << std::endl;
          }

      }

    // restore boundary enabled state at entry
    bndMgr->enableBoundaries(enabled);

  } //blankCellsOutside


} // namespace

// estimate cell volumes of intersected cells
void GridAdaption::correctCellVolumes()
const
{
  auto& bnds = boundaries_affecting_cell_volumes_;

  if (bnds.size() > 0)
    {
      if (mpim_->isRoot())
        {
          std::cout << "correcting cell volumes w.r.t. "
              << std::string(bnds.size() == 1 ? "boundary" : "boundaries");
          for (auto b : bnds)
            std::cout << " \"" << b->name() <<"\"";
          std::cout << std::endl;
        }
      else if (mpim_->isRoot())
        std::cout << "correcting cell volumes w.r.t. all boundaries" << std::endl;

      constexpr uint8_t intersect_color = 1;

      auto* g = gm_->particleGrid();

      g->removeRegularCellColors(intersect_color);

      // color cells intersected by selected boundaries (non-ghost)
      addColorToIntersectedCells( bnds, intersect_color  );

      sampleColoredCellVolumes(g,bm_,intersect_color);

      // zero volume of cells outside
      if (mpim_->isRoot())
        std::cout << "zeroing volumes of cells outside of fluid volume" << std::endl;

      // tag cells (non-ghost) intersected by any boundaries
      addColorToIntersectedCells( {}, intersect_color );

      blankCellsOutside(g,bm_,intersect_color);

      // make data consistent

      // exchange volume of ghost cells
      Grids::exchangeGhostData(
          gm_->nodeGrid(),
          g,
          [g](const uint64_t key)->double{
        const uint64_t idx = g->locateKey(key);
        //    MYASSERT(idx != uint64_t(-1),"could not locate key " + std::to_string(key));
        return g->cellVolume( idx );},
        [g](const uint64_t iGhost, const double v)->void{
          //      MYASSERT(iGhost < g->nGhost(), "ghost idx out of range");
          const uint64_t idx = g->ghostIdx2CellIdx(iGhost);
          //      MYASSERT(idx < g->nCells(),"cell idx out of range");
          g->setCellVolume( idx, v );}, // map to ghost index range
          *mpim_,
          COMPILE_TIME_CRC32_STR( FILE_POS_STR ) );


      // ensure consistency between data grids and particle grids
      gm_->updateCellVolumesOnDataGridsFromParticleGrid();


    } // if bnds.size() > 0

} // correct cell volumes
