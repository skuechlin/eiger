/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * GridAdaptionCoordinator.cpp
 *
 *  Created on: Oct 6, 2016
 *      Author: kustepha
 */

#include <Primitives/Callback.h>
#include "GridAdaption.h"

#include "Grid/GridManager/GridManager.h"

#include "Primitives/Timer.h"
#include "Parallel/MyMPI.h"


std::list<std::unique_ptr<GenericCallback>>
GridAdaption::makeCallbacks()
{


  std::list<std::unique_ptr<GenericCallback>> ret;

  auto gridAdaption = *this;
  // copy, since current instance not guaranteed to
  // exist at call time of callback

  if (flags_ != FLAG::NONE)
    {
      // "static" (once before simulation) callback

      ret.emplace_back( newCallback([gridAdaption](
          const double t,
          const uint64_t tn,
          const MyMPI::MPIMgr& mpiMgr,
          MyChrono::TimerCollection& timers)
          ->void{ gridAdaption.adaptGrids(t,tn,mpiMgr,timers); },-1,-1,1) );

      // dynamic callback
      if (flags_ & DYNAMIC_REFINEMENT)
        ret.emplace_back(
            newCallback(
                [gridAdaption](
                    const double t,
                    const uint64_t tn,
                    const MyMPI::MPIMgr& mpiMgr,
                    MyChrono::TimerCollection& timers)
                    ->void{ gridAdaption.adaptGrids(t,tn,mpiMgr,timers); },
                    dynamic_adaption_dict_) );
    }

  return ret;


}

void
GridAdaption::adaptGrids(
    const double,
    const uint64_t tn,
    const MyMPI::MPIMgr&,
    MyChrono::TimerCollection& timers)
const
{

  static bool change;

  timers.start("grid_adaption"); // note: own master region

#pragma omp master
  {
    change = false;

    if (flags_ != NONE)
      {
        if (tn != uint64_t(-1)) // dynamic
          {
            // non static actions
            if ( flags_ & REFINE_TO_GRADIENT_LENGTH)
              change |= refineByGradient();

            if ( flags_ & REFINE_TO_MFP)
              change |= refineByMFP();

            if ( flags_ & REFINE_BY_NUM_PRTS)
              change |= refineByNP();
          }
        else // static
          {
            if ( flags_ & REFINE_INTERSECTED_CELLS )
              refineIntersectedCells();

            if ( flags_ & REFINE_INSIDE )
              refineCellsInside();

          }


        if (tn == uint64_t(-1) || change) // only perform these computations after dynamic adaption if adaption caused change
          {
//            if ( flags_ & REFINEMENT )
//              refineToSmoothe();

            if ( flags_ & CORRECT_CELL_VOLUMES )
              correctCellVolumes();

            if ( flags_ & REMOVE_EMPTY_CELLS )
              removeEmptyCells();

            // remove any cells that are no longer direct neighbors of cells in range first_,...,last_
            gm_->removeObsoleteGhostCells();

            gm_->assertParticleGridDataGridConsistency();

            // update cell volume statistics
            gm_->particleGrid()->updateCellVolumeStatistics();

            // count grid change
            gm_->particleGrid()->countChange();

            // if local time stepping enabled, update time zone boundary
            if ( flags_ & MAINTAIN_TZ_BOUNDARY )
              updateTimeZoneBoundary();

            change = true;

          }

        if (mpim_->isRoot())
          std::cout << std::endl;

      } // if flags

  } // master region
  timers.stop("grid_adaption"); // has own master region
#pragma omp barrier


//  if (tn != uint64_t(-1) && change) // only write particle grid if change occurred during dynamic adaption
//    gm_->writeParticleGrid(t,tn,mpiMgr,timers);

}


