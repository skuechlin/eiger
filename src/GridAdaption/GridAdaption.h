/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * GridAdaptionCoordinator.h
 *
 *  Created on: Oct 6, 2016
 *      Author: kustepha
 */

#ifndef SRC_GRIDADAPTION_GRIDADAPTION_H_
#define SRC_GRIDADAPTION_GRIDADAPTION_H_

#include <stdint.h>
#include <vector>
#include <string>
#include <functional>
#include <list>
#include <memory>

#include "Grid/GridHelpers/GridTransform.h"

#include "Settings/Dictionary.h"

namespace Boundary  { class BoundaryManager; class Boundary; }
namespace Grids     { class GridManager; }
namespace MyMPI     { class MPIMgr; }
namespace MyChrono  { class TimerCollection; }
namespace Gas { namespace GasModels { class GasModel; } }

class GenericCallback;

class GridAdaption
{

public:

    typedef uint64_t flag_t;

  enum FLAG : flag_t {
    NONE = 0,
        CORRECT_CELL_VOLUMES        = uint64_t(1),
        REMOVE_EMPTY_CELLS          = uint64_t(1)<<1,
        REFINE_INTERSECTED_CELLS    = uint64_t(1)<<2,
        REFINE_TO_GRADIENT_LENGTH   = uint64_t(1)<<3,
        REFINE_TO_MFP               = uint64_t(1)<<4,
        REFINE_INSIDE               = uint64_t(1)<<5,
        REFINE_BY_NUM_PRTS          = uint64_t(1)<<6,
        MAINTAIN_TZ_BOUNDARY        = uint64_t(1)<<7,
        DYNAMIC_REFINEMENT          = REFINE_TO_GRADIENT_LENGTH | REFINE_TO_MFP | REFINE_BY_NUM_PRTS,
        REFINEMENT                  = REFINE_INTERSECTED_CELLS | REFINE_INSIDE | DYNAMIC_REFINEMENT,
        STATIC_REFINEMENT           = REFINE_INSIDE | REFINE_INTERSECTED_CELLS
  };

private:


  const FLAG flags_ = FLAG::NONE;

  // static refinement

  // boundaries that should impact the cell volume computation
  std::vector<const Boundary::Boundary*> boundaries_affecting_cell_volumes_;

  // groups of boundaries at which to refine intersected cells
  std::vector<std::vector<const Boundary::Boundary*>> boundaries_for_intersection_refinement_;
  std::vector<uint8_t> intersected_boundary_refinement_level_;
  std::vector<const Boundary::Boundary*> boundaries_with_permanent_level_;

  // groups of boundaries that define volume in which to refine
  std::vector<std::vector<const Boundary::Boundary*>> boundaries_for_inside_refinement_;

  std::vector<uint8_t> inside_boundary_refinement_level_;


  // name of grid for dynamic grid adaption
  std::string name_of_dynamic_adaption_data_grid_;

  // name of variable for gradient length refinement
  std::string name_of_variable_for_gradient_length_refinement_;

  // threshold for gradient based refinement
  // refine cells with cellWidth > gradLen*threshold
  double gradient_length_refinement_threshold_ = 0.;

  uint8_t gradient_length_refinement_level_ = 0;

  // set 0. to use cell value
  double gradient_theta_ref_ = 0.;
  double gradient_rho_ref_   = 0.;

  double min_num_gradient_samples_ = 0.;

  uint8_t num_gradient_smoothing_passes_ = 0;

  // threshold for cell width to mfp  ratio for refinement
  double mfp_refinement_threshold_ = 0.;

  uint8_t mfp_refinement_level_ = 0;

  double np_refinement_threshold_ = 0.;

  uint8_t np_refinement_level_ = 0;


  Settings::Dictionary dynamic_adaption_dict_;

private:

  Grids::GridManager *const gm_;
  Boundary::BoundaryManager *const bm_;
  MyMPI::MPIMgr *const mpim_;

public:

  std::list<std::unique_ptr<GenericCallback>> makeCallbacks();


private:

  // perform grid refinement / coarsening / cell removal / volume correction
  void adaptGrids(
      const double t,
      const uint64_t tn,
      const MyMPI::MPIMgr& mpiMgr,
      MyChrono::TimerCollection& timers)
  const;

  // helpers

  bool transformLoop( const std::function<void()>& cFun,
                      const Grids::GridTransform::ACTION action,
                      const uint8_t min_level,
                      const uint8_t colors)
  const;

  void addColorToIntersectedCells(
      const std::vector<const Boundary::Boundary*>& boundaries,
      const uint8_t color, const double tol = 0.0) const;

  void addColorToCellsInside(
      const std::vector<const Boundary::Boundary*>& boundaries,
      const uint8_t color) const;

  void exchangeGhostCellColors(const int tag) const;


private:


  void removeEmptyCells() const;

  void refineIntersectedCells() const;

  void refineCellsInside() const;

  // refine cells with width greater than gradient length (in variable iVar) * threshold
  bool refineByGradient() const;

  // refine cells with width greater than mfp * threshold
  bool refineByMFP() const;

  bool refineToSmoothe(const uint8_t n_passes = 1) const;

  // refine cells with more particles than threshold
  bool refineByNP() const;

  void correctCellVolumes() const;

  void updateTimeZoneBoundary() const;


private:
  void genTimeZoneBoundary_FE() const;
//  void genTimeZoneBoundary_CF() const;


public:
  GridAdaption (
      Grids::GridManager *const gm,
      Boundary::BoundaryManager *const bm,
      MyMPI::MPIMgr *const mpim,
      const Gas::GasModels::GasModel* const gasModel,
      const Settings::Dictionary& dict,
      const Settings::Dictionary& condDict);

  ~GridAdaption () = default;
};

#endif /* SRC_GRIDADAPTION_GRIDADAPTION_H_ */
