/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * GridAdaptionRefinement.cpp
 *
 *  Created on: Oct 6, 2016
 *      Author: kustepha
 */


#include <Grid/CellData.h>
#include "Geometry/Shape.h"

#include "Primitives/MyError.h"
#include "Primitives/MyString.h"

#include "Parallel/MyMPI.h"

#include "Grid/GridLaplaceSmoother.h"
#include "Grid/GridManager/GridManager.h"
#include "Grid/GridGhostExchange.h"
#include "GridAdaption.h"

#include "Boundary/Boundary.h"


void GridAdaption::refineIntersectedCells()
const
{

  //  MYASSERT(names_of_boundaries_for_intersection_refinement_.size() ==
  //      intersected_boundary_refinement_level_.size(),
  //      "every boundary list needs minimum refinement level");

  for (uint64_t i = 0; i < boundaries_for_intersection_refinement_.size(); ++i)
    {
      const auto& bnds = boundaries_for_intersection_refinement_[i];
      const uint8_t level = intersected_boundary_refinement_level_[i];
      if (mpim_->isRoot())
        {
          if (bnds.size() > 0)
            {
              std::cout << "refining cells intersecting "
                  << std::string(bnds.size() == 1 ? "boundary" : "boundaries");
              for (auto b : bnds)
                std::cout << " \"" << b->name() <<"\"";
              std::cout << " to level " << std::to_string(level) << std::endl;
            }
          else
            std::cout << "refining cells intersecting any boundary" << std::endl;
        }

      transformLoop( std::function<void()>{[this,bnds]()->void{ addColorToIntersectedCells(bnds,1); }},
                     Grids::GridTransform::ACTION::REFINE,
                     level,1 );

    } // for all in boundaries_for_intersection_refinement

  const auto& bnds = boundaries_with_permanent_level_;

  if (bnds.size() > 0)
    {
      if (mpim_->isRoot())
        {
          std::cout << "marking cells intersecting "
              << std::string(bnds.size() == 1 ? "boundary" : "boundaries");
          for (auto b : bnds)
            std::cout << " \"" << b->name() <<"\"";
          std::cout << " as permanent" << std::endl;
        }

      else if (mpim_->isRoot())
        std::cout << "marking cells intersecting any boundary as permanent" << std::endl;

      addColorToIntersectedCells(bnds, Grids::RegularGrid::COLOR::PERMANENT, Shapes::ABSEPS);
      // use tolerance of Shapes::ABSEPS to extend each cell's bounding box during
      // intersection check to also catch boundaries aligned to low cell borders

      // communicate color on ghost grid
      exchangeGhostCellColors(COMPILE_TIME_CRC32_STR( FILE_POS_STR ));

      // propagate level limits
      auto g = gm_->particleGrid();
      uint64_t pass = 0;
      while(true)
        {
          const uint64_t n_changed = g->propagateCellLevelLimits(1);

          Grids::exchangeGhostData(
              gm_->nodeGrid(),
              g,
              [g](const uint64_t key)->uint32_t{
            const auto& c = g->cell( g->locateKey(key) );
            return (uint32_t(c.min_level)<<8) | uint32_t(c.max_level);
          },
          [g](const uint64_t iGhost, const uint32_t mm)->void{
            auto& c = g->cell( g->ghostIdx2CellIdx(iGhost) ); // map from ghost index range
            c.min_level = mm >> 8;
            c.max_level = mm & uint32_t((uint32_t(1)<<8) - 1);
          },
          *mpim_,
          COMPILE_TIME_CRC32_STR( FILE_POS_STR ) + pass);

          uint64_t n_changed_global = 0;

          MPI_Allreduce(&n_changed,&n_changed_global,1,MPI_UINT64_T,MPI_SUM,mpim_->comm());

          if (n_changed_global == 0)
            break; // break when no cells have been changed anywhere on the global grid


          ++pass;

        }



    } // mark permanent


} // refine intersected cells

void GridAdaption::refineCellsInside()
const
{

  MYASSERT(boundaries_for_inside_refinement_.size() ==
      inside_boundary_refinement_level_.size(),
      "every boundary list needs minimum refinement level");

  for (uint64_t i = 0; i < boundaries_for_inside_refinement_.size(); ++i)
    {
      const auto& bnds = boundaries_for_inside_refinement_[i];
      const uint8_t level = inside_boundary_refinement_level_[i];
      if (bnds.size() > 0)
        {

          if (mpim_->isRoot())
            {
              std::cout << "refining cells inside "
                  << std::string(bnds.size() == 1 ? "boundary" : "boundaries");
              for (auto b : bnds)
                std::cout << " \"" << b->name() <<"\"";
              std::cout << " to level " << std::to_string(level)<< std::endl;
            }

          transformLoop( std::function<void()>{[this,bnds]()->void{ addColorToCellsInside(bnds,1); }},
                         Grids::GridTransform::ACTION::REFINE,
                         level,1 );

        }
      else if (mpim_->isRoot())
        std::cout << "skipping empty boundary enclosure for refinement" << std::endl;

    }
}

bool GridAdaption::refineByGradient()
const
{

  auto mpim = mpim_;

  auto gm = gm_;
  auto pg = gm->particleGrid();
  auto dg = gm->dataGrid( name_of_dynamic_adaption_data_grid_ );
  const double threshold = gradient_length_refinement_threshold_;

  MYASSERT(threshold > 0., "gradient length refinement threshold <= 0.");

  MYASSERT(dg, std::string("refineByGradient failed: data grid \"") +
           name_of_dynamic_adaption_data_grid_ + std::string("\" not found"));

  const double _irho   = gradient_rho_ref_   > 0. ? 1./gradient_rho_ref_   : 0.;
  const double _itheta = gradient_theta_ref_ > 0. ? 1./gradient_theta_ref_ : 0.;

  if (mpim->isRoot()) {
      std::cout << "\nrefining cells by max. norm. gradient to level " <<
          std::to_string(gradient_length_refinement_level_) << "\n";
      std::cout << " - threshold: " << threshold << "\n";
      std::cout << " - ref. grid: " << name_of_dynamic_adaption_data_grid_ << "\n";
      std::cout << " - min. num. samples: " << min_num_gradient_samples_ << "\n\n";
      std::cout << "  computing normalized gradient length estimate\n";
  }

  gm->computeMaxNormalizedGradientLengthFromKernelEstimates(dg,_irho,_itheta, min_num_gradient_samples_);

  if (num_gradient_smoothing_passes_>0) {
      // compute weighting variable
#pragma omp parallel for schedule(static)
      for (uint64_t ic = pg->first_; ic < pg->last_; ++ic) {
          dg->cell(ic).tmp1_ = dg->cell(ic).mass();
      }

      if (mpim_->isRoot()) {
          std::cout << "  smooth estimate:\n";
      }
  }

  for (uint8_t i = 0; i < num_gradient_smoothing_passes_; ++i) {
      if (mpim_->isRoot()) {
          std::cout << "    pass " << std::to_string(i+1) << "/" << std::to_string(num_gradient_smoothing_passes_) << "\n";
      }
      smooth(
          gm->nodeGrid(),pg,
          [dg](const uint64_t ic)->double{return dg->cell(ic).gradient_length_;},
          [dg](const uint64_t ic, const double v)->void{ dg->cell(ic).gradient_length_ = v;},
          [dg](const uint64_t ic)->double{return dg->cell(ic).tmp0_;},
          [dg](const uint64_t ic, const double v)->void{ dg->cell(ic).tmp0_ = v;},
          [dg](const uint64_t ic)->double{return dg->cell(ic).tmp1_;},
          [dg](const uint64_t ic, const double v)->void{ dg->cell(ic).tmp1_ = v;},
          mpim,i);
  }

  if (mpim_->isRoot()) {
      std::cout << "  proceed with refinement\n";
  }

  constexpr uint8_t refine_color = 1;

  bool change = transformLoop( std::function<void()>{
    [pg,dg,threshold,refine_color]()->void{ pg->addColorByFun(
        [pg,dg,threshold,refine_color](const uint64_t ic)->uint8_t{

      const double gl = dg->cell(ic).gradient_length_;
      const double cellw = pg->cellMinWidth(ic);

      const bool refine = (gl > 0.0) && ( cellw > ( gl * threshold ) );

      return refine ? refine_color : 0;

    } ); } },
    Grids::GridTransform::ACTION::REFINE,
    gradient_length_refinement_level_, refine_color );

  return change;

}

bool GridAdaption::refineByMFP()
const
{

  auto gm = gm_;
  auto pg = gm->particleGrid();
  auto dg = gm->dataGrid( name_of_dynamic_adaption_data_grid_ );
  double threshold = mfp_refinement_threshold_;

  if (mpim_->isRoot()) {
      std::cout << "\nrefining cells by equilibrium mean free path to level " << std::to_string(mfp_refinement_level_) << "\n";
      std::cout << " - threshold: " << threshold << "\n";
      std::cout << " - ref. grid: " << name_of_dynamic_adaption_data_grid_ << "\n";
      std::cout << std::endl;
  }



  MYASSERT(threshold > 0., "mean free path refinement threshold <= 0.");

  MYASSERT(dg, std::string("refineByMFP failed: data grid \"") +
           name_of_dynamic_adaption_data_grid_ + std::string("\" not found"));

  uint64_t mfp_var_ind = CellData::getVarInd("mfp");

  MYASSERT(mfp_var_ind != uint64_t(-1),
           "refineByMFP failed: mean free path variable \"mfp\" not found");

  auto mpim = mpim_;

  constexpr uint8_t refine_color = 1;

  bool change = transformLoop( std::function<void()>{
    [dg,pg,mfp_var_ind,threshold,mpim,refine_color]()->void{ pg->addColorByFun(
        [pg,dg,mfp_var_ind,threshold,mpim,refine_color](const uint64_t ic)->uint8_t{

      const double mfp = dg->cell(ic).getVar(mfp_var_ind);
      const double cellw = pg->cellMinWidth(ic);

      const bool refine = (mfp > 0.0) && ( cellw > ( mfp * threshold ) );

      //                // DEBUG
      //                    if (pg->key_[ic] == 0 && tag)
      //                      std::cout << mpim->rank() << ": cell with key 0 tagged by refineByMFP(), mfp: " << mfp << " cellMinWidth: " << cellw << " threshold: " << threshold << std::endl;

      return refine ? refine_color : 0;

    } ); }
  },
  Grids::GridTransform::ACTION::REFINE,
  mfp_refinement_level_, refine_color );

  return change;

}

// refine cells with more particles than threshold
bool GridAdaption::refineByNP() const
{

  if (mpim_->isRoot())
    std::cout << "\nrefining cells by number of particles to level " << std::to_string(np_refinement_level_) << "\n";

  auto gm = gm_;
  auto g = gm->particleGrid();
  auto dg = gm->dataGrid( name_of_dynamic_adaption_data_grid_ );
  double threshold = np_refinement_threshold_;

  MYASSERT(threshold > 0., "number of particles refinement threshold <= 0.");

  MYASSERT(dg, std::string("refineByNP failed: data grid \"") +
           name_of_dynamic_adaption_data_grid_ + std::string("\" not found"));

  uint64_t np_var_ind = CellData::getVarInd("Np");

  MYASSERT(np_var_ind != uint64_t(-1),
           "refineByNP failed: number of particles variable \"Np\" not found");

  constexpr uint8_t refine_color = 1;

  bool change = transformLoop( std::function<void()>{
    [g,dg,np_var_ind,threshold,refine_color]()->void{ g->addColorByFun(
        [dg,np_var_ind,threshold,refine_color](const uint64_t ic)->uint8_t{
      const double Np = dg->cell(ic).getVar(np_var_ind);
      return Np > threshold ? refine_color : 0;
    } ); }
  },
  Grids::GridTransform::ACTION::REFINE,
  np_refinement_level_,refine_color );

  return change;

}


bool GridAdaption::refineToSmoothe(const uint8_t n_passes) const
{
  if (n_passes == 0)
    return false;

  const bool root = mpim_->isRoot();

  if (root)
    std::cout << "\nrefining cells to smooth grid\n";

  auto gm = gm_;

  auto g = gm->particleGrid();
  Grids::GridTransform tr(g);

  const uint8_t max_lvl = g->topology()->maxLevel();

  constexpr uint8_t color = 1;

  uint64_t n_changed = 0;

  for (uint8_t pass = 0; pass < n_passes; ++pass )
    {

      if (root)
        std::cout << "    pass " << std::to_string(pass+1) << "/" << std::to_string(n_passes) << "\n";

      for (uint8_t l = 0; l < max_lvl; ++l)
        {

          if (mpim_->isRoot())
            std::cout << "    sub-pass " << std::to_string(l+1) << "/" << std::to_string(max_lvl)  << "\n";

          // remove color
          g->removeRegularCellColors(color);

          // no restriction on pass number, since level differences might exist
          // from previous refinements

          // color cells with neighbors 1 or more levels finer
          g->addColorByLevelGradient(0,color);

          // communicate color on ghost grid
          // communicate color on ghost grid
          exchangeGhostCellColors(COMPILE_TIME_CRC32_STR( FILE_POS_STR ) + pass*max_lvl + l);


          // append colored to transform spec, limit transform to min_levle, colors requirements
          // cells with color PERMANENT will not be refined/coarsened, but may be removed
          tr.clear();
          tr.append( Grids::GridTransform::ACTION::REFINE, l, color );

          // transform the grids
          n_changed += gm->transform( tr );
        }

    }

  uint64_t n_changed_global = 0;

  MPI_Allreduce(&n_changed,&n_changed_global,1,MPI_UINT64_T,MPI_SUM,mpim_->comm());

  if (root)
    std::cout << std::endl;

  return n_changed_global > 0;
}
