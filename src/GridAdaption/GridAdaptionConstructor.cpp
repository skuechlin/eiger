/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * GridAdaptionConstructor.cpp
 *
 *  Created on: Oct 6, 2016
 *      Author: kustepha
 */


#include <vector>
#include <algorithm> // std::find
#include <map>
#include <set>

#include "Settings/Dictionary.h"

#include "GridAdaption.h"

#include "Geometry/Shape.h"

#include "Gas/GasModel.h"

#include "Parallel/MyMPI.h"

#include "Boundary/Boundary.h"
#include "Boundary/BoundaryManager/BoundaryManager.h"

constexpr char cell_vol_corr_dict_key[] = "cell volume correction";
constexpr char cell_vol_corr_tf_key[]   = "correct cell volumes";
constexpr char rem_empty_cells_key[]    = "remove empty cells";

constexpr char intersect_refine_dict_key[]  = "intersect refinement";
constexpr char intersect_refine_tf_key[]    = "refine intersected cells";

constexpr char enclosed_refine_dict_key[]   = "refine cells inside";

constexpr char dynamic_refine_dict_key[]    = "dynamic";
constexpr char dynamic_refine_crit_key[]    = "criterion";

namespace
{

  void printFlags(GridAdaption::FLAG _f) {
    GridAdaption::flag_t f = static_cast<GridAdaption::flag_t>(_f);

    if (!f) std::cout << " - no actions configured\n";

    if (f & GridAdaption::MAINTAIN_TZ_BOUNDARY)
      std::cout << " - maintain time zone boundary\n";
    if (f & GridAdaption::DYNAMIC_REFINEMENT)
      std::cout << " - dynamic refinement:\n";
    if (f & GridAdaption::REFINE_TO_GRADIENT_LENGTH)
      std::cout << " -- refine to gradient length\n";
    if (f & GridAdaption::REFINE_TO_MFP)
      std::cout << " -- refine to mfp\n";
    if (f & GridAdaption::REFINE_BY_NUM_PRTS)
      std::cout << " -- refine by num prts\n";

    if (f & GridAdaption::STATIC_REFINEMENT)
      std::cout << " - static refinement:\n";
    if (f & GridAdaption::REFINE_INSIDE)
      std::cout << " -- refine cells inside boundaries\n";
    if (f & GridAdaption::REFINE_INTERSECTED_CELLS)
      std::cout << " -- refine intersected cells\n";

    if (f & GridAdaption::CORRECT_CELL_VOLUMES)
      std::cout << " - correct cell volumes\n";
    if (f & GridAdaption::REMOVE_EMPTY_CELLS)
      std::cout << " - remove empty cells\n";

    std::cout << std::endl;

  }

  GridAdaption::FLAG
  parseFlags(const Settings::Dictionary& fdict, const Boundary::BoundaryManager *const bm)
  {
    GridAdaption::flag_t f = GridAdaption::FLAG::NONE;

    Settings::Dictionary tmp;

    if ( fdict.hasMember(cell_vol_corr_dict_key) )
      {
        tmp = fdict.get<Settings::Dictionary>(cell_vol_corr_dict_key);

        if ( tmp.get<bool>(cell_vol_corr_tf_key) )
          f |= GridAdaption::FLAG::CORRECT_CELL_VOLUMES;

        if ( tmp.get<bool>(rem_empty_cells_key))
          f |= GridAdaption::FLAG::REMOVE_EMPTY_CELLS;
      }

    for (uint64_t i = 0; i < bm->nBoundaries(); ++i)
      if (bm->getBoundary(i)->level() != uint8_t(-1) )
        {
          f |= GridAdaption::FLAG::REFINE_INTERSECTED_CELLS;
          break;
        }

    if ( fdict.hasMember(intersect_refine_dict_key) )
      {
        tmp = fdict.get<Settings::Dictionary>(intersect_refine_dict_key);

        if (tmp.get<bool>(intersect_refine_tf_key))
          f |= GridAdaption::FLAG::REFINE_INTERSECTED_CELLS;

      }

    if ( fdict.hasMember(enclosed_refine_dict_key) )
      {
        tmp = fdict.get<Settings::Dictionary>(enclosed_refine_dict_key);

        MYASSERT( tmp.isArray(), "type of dict must be array" );

        if ( !(tmp.isEmpty()) )
          f |= GridAdaption::FLAG::REFINE_INSIDE;

      }


    if ( fdict.hasMember(dynamic_refine_dict_key) )
      {
        Settings::Dictionary dfdict = fdict.get<Settings::Dictionary>(dynamic_refine_dict_key);

        std::string criterion = dfdict.get<std::string>(dynamic_refine_crit_key);

        if ( criterion.compare("gradient") == 0 )
          f |= GridAdaption::FLAG::REFINE_TO_GRADIENT_LENGTH;
        else if ( criterion.compare("mfp") == 0 )
          f |= GridAdaption::FLAG::REFINE_TO_MFP;
        else if ( criterion.compare("Np") == 0 )
          f |= GridAdaption::FLAG::REFINE_BY_NUM_PRTS;
        else
          {
            MYASSERT(false,std::string("unrecognized dynamic refinement criterion \"")
            + criterion + std::string("\"") );
          }
      }

    if (bm->timeZoneBoundary() != nullptr)
      f |= GridAdaption::FLAG::MAINTAIN_TZ_BOUNDARY;

    return static_cast<GridAdaption::FLAG>(f);

  }

  const std::vector<std::string>&
  assertAllIn(
      const std::vector<std::string>& a,
      const std::vector<std::string>& ref)
      {
    const auto&& first = ref.begin();
    const auto&& last = ref.end();
    for (auto&& s : a)
      MYASSERT( std::find(first,last,s) != last,"\"" + s + "\" is not a known boundary name");
    return a;
      }

} // anon namespace



GridAdaption::GridAdaption (
    Grids::GridManager *const gm,
    Boundary::BoundaryManager *const bm,
    MyMPI::MPIMgr *const mpim,
    const Gas::GasModels::GasModel* const gasModel,
    const Settings::Dictionary& dict,
    const Settings::Dictionary& condDict)
: flags_( parseFlags(dict,bm) )
, gm_(gm)
, bm_(bm)
, mpim_(mpim)
{

  if(mpim->isRoot()) {
#pragma omp critical(COUT)
      {
        std::cout << "constructing grid adaption object:\n";
        printFlags(flags_);
      }
  }

  // gather boundary names for cross-check
  std::vector<std::string> bnd_names = bm_->boundaryNames();


  // parse boundary name restrictions on cell volume correction and
  // intersection refinement
  Settings::Dictionary d;
  if (flags_ & (FLAG::CORRECT_CELL_VOLUMES | FLAG::REMOVE_EMPTY_CELLS) )
    {
      d = dict.get<Settings::Dictionary>(cell_vol_corr_dict_key);
      if ( d.hasMember("boundaries") )
        {
          d = d.get<Settings::Dictionary>("boundaries");
          auto bnds = assertAllIn( d.getAsVector<std::string>(), bnd_names);
          std::transform(
              bnds.begin(),bnds.end(),
              std::back_inserter(boundaries_affecting_cell_volumes_),
              [bm](const std::string& name)->const Boundary::Boundary*{ return bm->getBoundaryByName(name);}
          );
        }
    }

  if (flags_ & FLAG::REFINE_INTERSECTED_CELLS)
    {

      std::multimap<uint8_t,const Boundary::Boundary*> blmap;
      std::set<uint8_t> lvls;

      bool add_all = false;
      uint8_t default_level = 0;
      bool default_permanent = false;
      if( dict.hasMember(intersect_refine_dict_key) )
        {
          Settings::Dictionary d = dict.get<Settings::Dictionary>(intersect_refine_dict_key);
          add_all = d.get<bool>(intersect_refine_tf_key);
          default_level = d.get<uint8_t>("level",0);
          default_permanent = d.get<bool>("permanent",false);
        }

      for (uint64_t i = 0; i < bm->nBoundaries(); ++i)
        {
          const uint8_t bl = bm->getBoundary(i)->level();
          if ( add_all || bl  != uint8_t(-1) )
            {
              const uint8_t lvl =  bl != uint8_t(-1) ? bl : default_level;
              blmap.insert( decltype(blmap)::value_type(
                  lvl,
                  bm->getBoundary(i)) );
              lvls.insert(lvl);
            }
        }

      // add vectors of boundaries w equal levels
      for (auto li = lvls.begin(); li != lvls.end(); ++li )
        {
          const auto rng = blmap.equal_range(*li);
          decltype(boundaries_for_intersection_refinement_)::value_type bnds;
          for (auto ri = rng.first; ri != rng.second; ++ri)
            bnds.push_back(ri->second);
          boundaries_for_intersection_refinement_.push_back(bnds);
          intersected_boundary_refinement_level_.push_back(*li);
        }

      // get list of boundaries w. permanent levels
      for (uint64_t i = 0; i < bm->nBoundaries(); ++i)
        {
          const uint8_t blp = bm->getBoundary(i)->level_permanent_tfd();
          if (blp == 1 || (blp == 2 && default_permanent) )
            boundaries_with_permanent_level_.push_back( bm->getBoundary(i) );
        }

    }

  if (flags_ & FLAG::REFINE_INSIDE)
    {
      d = dict.get<Settings::Dictionary>(enclosed_refine_dict_key);

      for (uint64_t i = 0; i < d.size(); ++i)
        {

          Settings::Dictionary rd;
          d.get<Settings::Dictionary>(&rd,i,1);

          auto bndnames = assertAllIn( rd.get<Settings::Dictionary>("boundaries").getAsVector<std::string>(), bnd_names);
          using T = decltype(boundaries_for_inside_refinement_)::value_type;
          T bnds;

          std::transform(
              bndnames.begin(),bndnames.end(),
              std::back_inserter(bnds),
              [bm](const std::string& name)->const Boundary::Boundary*{
            return bm->getBoundaryByName(name);}
          );

          boundaries_for_inside_refinement_.push_back(std::move(bnds));

          //          names_of_boundaries_for_inside_refinement_.push_back(
          //              assertAllIn(
          //                  rd.get<Settings::Dictionary>("boundaries").getAsVector<std::string>(),
          //                  bnd_names)
          //          );

          inside_boundary_refinement_level_.push_back( rd.get<uint8_t>("level",0) );

        }

    }

  if (flags_ & DYNAMIC_REFINEMENT)
    {
      d = dict.get<Settings::Dictionary>(dynamic_refine_dict_key);

      name_of_dynamic_adaption_data_grid_ = d.get<std::string>("grid");

      if ( flags_ & FLAG::REFINE_TO_GRADIENT_LENGTH )
        {
          // name of variable for gradient length refinement
          //          name_of_variable_for_gradient_length_refinement_ = d.get<std::string>("variable");

          // threshold for gradient based refinement
          // refine cells with cellWidth > gradLen*threshold
          gradient_length_refinement_threshold_ = d.get<double>("threshold");

          gradient_length_refinement_level_ = d.get<uint8_t>("level",0);

          min_num_gradient_samples_ = d.get<double>("min. num. samples",0.);

          num_gradient_smoothing_passes_ = d.get<uint8_t>("smoothing passes",0);



          if (d.hasMember("reference condition"))
            {

              auto refd = condDict.getDictByValue<std::string>(
                  "name",
                  d.get<std::string>("reference condition")
              );

              gradient_theta_ref_   = gasModel->getT(refd) * gasModel->R();
              gradient_rho_ref_     = gasModel->getn(refd) * gasModel->m();

            }

          if (mpim->isRoot())
            {
              std::cout <<
                  "gradient refinement:\n"
                  "- reference values:\n" <<
                  "-- theta: " << gradient_theta_ref_ << " m2 s-2\n" <<
                  "-- rho:   " << gradient_rho_ref_   << " kg m-3\n" <<
                  "- min. number of samples: " << min_num_gradient_samples_ << "\n" <<
                  "- number of smoothing passes: " << std::to_string(num_gradient_smoothing_passes_) << "\n" <<
                  "- minimum level: " << std::to_string(gradient_length_refinement_level_) << "\n" <<
                  std::endl;
            }

        }

      if ( flags_ & FLAG::REFINE_TO_MFP)
        {
          // threshold for cell width to mfp  ratio for refinement
          mfp_refinement_threshold_ = d.get<double>("threshold");

          mfp_refinement_level_ = d.get<uint8_t>("level",0);
        }

      if ( flags_ & FLAG::REFINE_BY_NUM_PRTS)
        {
          // threshold for number of particles
          np_refinement_threshold_ = d.get<double>("threshold");

          np_refinement_level_ = d.get<uint8_t>("level",0);
        }

      // save the dictionary to extract trigger times when constructing callbacks
      dynamic_adaption_dict_ = d;

    }

  if ( flags_ & FLAG::MAINTAIN_TZ_BOUNDARY)
    {

      MYASSERT(
          bm->timeZoneBoundary()->shape()->type() == Shapes::FE_COLLECTION
          || bm->timeZoneBoundary()->shape()->type() == Shapes::CELLFACE_COLLECTION,
          "time zone boundary \""
          + bm->timeZoneBoundary()->name() + "\" is not of type \"fe_collection\"" +
          " or \"cellface_collection\"");
    }

}
