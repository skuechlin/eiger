/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * GridAdaptionTimeZone.cpp
 *
 *  Created on: Jun 11, 2018
 *      Author: kustepha
 */

#include <memory>

#include "GridAdaption.h"

#include "Geometry/FECollection.h"
#include "Geometry/FEShapeData.h"

#include "Boundary/Boundary.h"
#include "Boundary/BoundaryManager/BoundaryManager.h"

#include "Grid/GridManager/GridManager.h"
#include "Grid/GridHelpers/GridNeighborStack2.h"

#include "Parallel/MyMPI.h"


void GridAdaption::genTimeZoneBoundary_FE() const
{
  const Grids::RegularGrid* pg = gm_->particleGrid();

  static std::vector<Shapes::FEShapeData*> shared_tz;

#pragma omp parallel
  {
#pragma omp single
    {
      shared_tz.resize(omp_get_num_threads(),nullptr);
    }

    const int tn = omp_get_thread_num();

    shared_tz[tn] = new class Shapes::FEShapeData(bm_->worldBB());

    Shapes::FEShapeData* tl_tz = shared_tz[tn];

    tl_tz->reserveQuads( pg->nNonGhost() / omp_get_num_threads() );

    Grids::GridNeighborStack2 stack( pg, 1 );

#pragma omp for schedule(static)
    for ( uint64_t ic = pg->first_; ic < pg->last_; ++ic)
      {

        stack.push_neighbors( ic, Grids::GridNeighborStack2::DIRECTION::HIGH | Grids::GridNeighborStack2::DIRECTION::LOW );

        uint8_t face_added[6] = {0};
        const Box::box4_t cbox = pg->cellBox(ic);
        Box::face_t<3,Box::box4_t> face;

        // process neighbor stack.
        const uint8_t lvl = pg->cellLevel( ic );
        while( stack() )
          {
            const auto n = stack.pop();
            if (__builtin_expect( lvl > n.level, false ))
              {
                if ( !face_added[n.face] )
                  {
                    face_added[n.face] = 1;

                    Box::face(&face,cbox,n.face);

                    tl_tz->addQuad(to_eigen(face.a),to_eigen(face.b),to_eigen(face.b + face.c - face.a),to_eigen(face.c));
                  }
              }
          }

      } // for all cells

  } // parallel

  // consolidate thread results
  for (uint64_t i = 1; i < shared_tz.size(); ++i)
    {
      shared_tz[0]->merge(shared_tz[i]);
      delete shared_tz[i];
    }

  // allgather across mpi universe
  shared_tz[0]->allgather(*mpim_);

  // finalize
  shared_tz[0]->finalize();

  // update boundary
  auto b = bm_->timeZoneBoundary();
  auto s = static_cast<Shapes::FECollection*>(b->shape());
  s->update(
      std::move(
          std::unique_ptr<Shapes::FEShapeData>(shared_tz[0])  ) );
  s->setGeomID(s->geomID()); // propagate geomID
  bm_->commitBoundaryChange(b);
}

/*
void GridAdaption::genTimeZoneBoundary_CF() const {

  const Grids::RegularGrid* pg = gm_->particleGrid();
  auto b = bm_->timeZoneBoundary();

  static std::vector<Shapes::CellFaceCollection*> shared_tz;

#pragma omp parallel
  {
#pragma omp single
    {
      shared_tz.resize(omp_get_num_threads(),nullptr);
      shared_tz[0] = static_cast<Shapes::CellFaceCollection*>(b->shape());
    }

    const int tn = omp_get_thread_num();

    if (tn > 0)
      shared_tz[tn] = new class Shapes::CellFaceCollection();
    else
      shared_tz[tn]->clear();

    Shapes::CellFaceCollection* tl_tz = shared_tz[tn];

    const size_t sz_guess = pg->nNonGhost() / omp_get_num_threads();
    tl_tz->reserve(sz_guess);

    Grids::GridNeighborStack2 stack( pg, 1 );

#pragma omp for schedule(static)
    for ( uint64_t ic = pg->first_; ic < pg->last_; ++ic)
      {

        stack.push_neighbors( ic, Grids::GridNeighborStack2::DIRECTION::HIGH | Grids::GridNeighborStack2::DIRECTION::LOW );

        uint8_t face_added[6] = {0};

        // process neighbor stack.
        const uint64_t key = pg->cellKey( ic );
        const uint8_t lvl = pg->cellLevel( ic );
        while( stack() )
          {
            const auto n = stack.pop();
            if (__builtin_expect( lvl > n.level, false ))
              {
                if ( !face_added[n.face] )
                  {
                    face_added[n.face] = 1;

                    tl_tz->addFace(key,lvl,n.face);
                  }
              }
          }

      } // for all cells

  } // parallel

  // consolidate thread results
  for (uint64_t i = 1; i < shared_tz.size(); ++i)
    {
      MYASSERT(shared_tz[i] != nullptr, std::string("shared_tz[") + std::to_string(i) + "] == nullptr");
      shared_tz[0]->merge(shared_tz[i]);
      delete shared_tz[i];
    }

  // allgather across mpi universe
  shared_tz[0]->allgather(*mpim_);

  // finalize
  shared_tz[0]->finalize(pg->topology());

  // update boundary
  bm_->commitBoundaryChange(b);
}
*/


void GridAdaption::updateTimeZoneBoundary() const {

  if (mpim_->isRoot())
    std::cout << "updating time zone boundary" << std::endl;

  const auto b = bm_->timeZoneBoundary();
  if (b->shape()->type() == Shapes::FE_COLLECTION)
    genTimeZoneBoundary_FE();
  else if (b->shape()->type() == Shapes::CELLFACE_COLLECTION)
    MYASSERT(false,std::string("time zone boundary shape \"cellface_collection\" disabled"));
//    genTimeZoneBoundary_CF();
  else
    MYASSERT(false,std::string("time zone boundary shape must be of type ") +
             "\"fe_collection\" or \"cellface_collection\"");
}
