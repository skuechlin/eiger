/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * GridAdaptionHelpers.cpp
 *
 *  Created on: Oct 6, 2016
 *      Author: kustepha
 */


#include <GridAdaption/GridAdaption.h>

#include "Parallel/MyMPI.h"

#include "Boundary/BoundaryManager/BoundaryManager.h"
#include "Grid/GridManager/GridManager.h"
#include "Grid/GridGhostExchange.h"

#include "Primitives/Box.h"

void GridAdaption::exchangeGhostCellColors(const int tag) const {

  auto g = gm_->particleGrid();

  Grids::exchangeGhostData(
      gm_->nodeGrid(),
      g,
      [g](const uint64_t key)->uint8_t{return g->cellColor( g->locateKey(key) );},
      [g](const uint64_t iGhost, const uint8_t color)->void{
        g->setCellColor( g->ghostIdx2CellIdx(iGhost), color );}, // map from ghost index range
        *mpim_,
        COMPILE_TIME_CRC32_STR( FILE_POS_STR ) + tag);

}

void GridAdaption::addColorToIntersectedCells(
    const std::vector<const Boundary::Boundary*>& boundaries,
    const uint8_t c, const double tol)
const
{

  std::vector<const Boundary::Boundary*> enabled;
  bm_->getEnabledBoundaries(&enabled);

  // tag cells intersecting boundaries
  // enable only selected geometries for intersection calculation
  // empty vector enables all
  bm_->enableBoundaries( boundaries );

  auto g = gm_->particleGrid();
  auto bm = bm_;

  // tag non-ghost
  g->addColorByFun(
      [bm,g,c,tol](const uint64_t ic)->uint8_t{
    return bm->test_AABB(
        Box::to_eigen( Box::extend( g->cellBox(ic), tol*active4df<3>() ) )
    )
        ? c : 0; }
  );

  // restore boundary enabled state at entry
  bm_->enableBoundaries(enabled);
}

void GridAdaption::addColorToCellsInside(
    const std::vector<const Boundary::Boundary*>& boundaries,
    const uint8_t c)
const
{

  std::vector<const Boundary::Boundary*> enabled;
  bm_->getEnabledBoundaries(&enabled);

  // tag cells with center inside boundaries
  // enable only selected geometries for intersection calculation
  bm_->enableBoundaries( boundaries );

  auto g = gm_->particleGrid();

  g->addColorByFun([this,g,c](const uint64_t ic)->uint8_t{
    return !(bm_->isOutside( g->cellCenter(ic) )) ? c : 0; });

  //  std::cout << "... tagged " << std::to_string( std::accumulate(g->tag_.begin(),g->tag_.end(),0) ) << std::endl;

  // restore boundary enabled state at entry
  bm_->enableBoundaries(enabled);
}

bool GridAdaption::transformLoop(
    const std::function<void()>& cFun,
    const Grids::GridTransform::ACTION action,
    const uint8_t min_level,
    uint8_t colors)
const
{
  auto g = gm_->particleGrid();
  Grids::GridTransform tr(g);


  uint64_t pass = 0;
  while ( true )
    {

      if (mpim_->isRoot())
        std::cout << "    pass " << std::to_string(pass+1) << std::endl;

      // remove colors
      g->removeRegularCellColors(colors); // ghost cell colors will be overwritten later

      // color for refinement
      cFun();

      if (  action == Grids::GridTransform::REFINE  )
        {
          // no restriction on pass number, since level differences might exist
          // from previous refinements

          // color cells with neighbors 2 or more levels finer
          // won't destroy other colors
          g->addColorByLevelGradient(1,colors);

        }

      // communicate color on ghost grid
      exchangeGhostCellColors(COMPILE_TIME_CRC32_STR( FILE_POS_STR ) + pass);

      // append colored to transform spec, limit transform to min_levle, colors requirements
      tr.clear();
      tr.append( action, min_level, colors );

      // transform the grids
      uint64_t n_changed = gm_->transform( tr );

      uint64_t n_changed_global = 0;

      MPI_Allreduce(&n_changed,&n_changed_global,1,MPI_UINT64_T,MPI_SUM,mpim_->comm());

      if (n_changed_global == 0)
        break; // break when no cells have been changed anywhere on the global grid

      ++pass;

    } // while ( true )


  if (mpim_->isRoot())
    {
      if (pass == 0)         std::cout << "    -> no change\n";
      std::cout << std::endl;
    }

  return (pass > 0);

} // transformLoop

