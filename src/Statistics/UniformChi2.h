/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * UniformChi2.h
 *
 *  Created on: Feb 20, 2017
 *      Author: kustepha
 */

#ifndef SRC_STATISTICS_UNIFORMCHI2_H_
#define SRC_STATISTICS_UNIFORMCHI2_H_

#include <Eigen/Dense>

#include "Particles/ParticleCollection.h"


namespace UniformChi2
{

  struct SubCellCounts {


      Eigen::Matrix<uint64_t,8,1,Eigen::ColMajor> count_ = 0;

      void count(
          const Particles::ParticleCollection& prts,
          const uint64_t first,
          const uint64_t N,
          const uint8_t level,
          const uint8_t ndims)
      {
        if (level == 0) return;

        const uint64_t mask = (uint64_t(1)<<ndims) - uint64_t(1);
        const uint8_t shift = (level-1)*ndims;

        for (uint64_t i = first; i < first+N; ++i)
          ++count_[ (prts.getKey(i)>>shift) & mask ];
      }

      uint64_t total() const
      {
        uint64_t total = 0;
        for (uint64_t i = 0; i < 8; ++i) total += count_[i];
        return total;
      }

      double chi2(const uint8_t ndims) const
      {
        const uint8_t k = (uint8_t(1)<<ndims)
        const double e = static_cast<double>(total()) / static_cast<double>(k);
        double V = 0;
        for (uint8_t i = 0; i < k; ++i)
          V += (count_[i] - e)*(count_[i] - e);
        return V *= (1./e);
      }

      static bool rejectUniform1pm(const double chi2, const uint8_t ndims) const
      {
        //                           alpha = 0.001 dof:    2-1      4-1      8-1
        constexpr double chi2quantiles1pm[3] = {10.8276, 16.2662, 24.3219};
        return chi2(ndims) > chi2quantiles1pm[ndims];
      }

      bool rejectUniform1pm(const uint8_t ndims) const
      {
        return rejectUniform1pm(chi2(ndims),ndims);
      }

      EIGEN_MAKE_ALIGNED_OPERATOR_NEW

  };


}


#endif /* SRC_STATISTICS_UNIFORMCHI2_H_ */
