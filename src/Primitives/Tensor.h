/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * Tensor.h
 *
 *  Created on: Jan 7, 2019
 *      Author: kustepha
 */

#ifndef SRC_PRIMITIVES_TENSOR_H_
#define SRC_PRIMITIVES_TENSOR_H_

template<int i, int j, typename T = double>
inline constexpr T delta() { return (i==j) ? 1 : 0; }

template<int i, int j, typename T>
inline constexpr T delta(const T& v) { return (i==j) ? v : T(0); }

template<typename T, int... kl>
inline double trace(const T& a) {
  return a.template operator()<0,0,kl...>() +
      a.template operator()<1,1,kl...>() +
      a.template operator()<2,2,kl...>();
}

template<typename T1, typename T2, int... jkl>
inline double contract(const T1& a, const T2&b) {
  return a.template operator()<0,jkl...>()*b.template operator()<0,jkl...>() +
      a.template operator()<1,jkl...>()*b.template operator()<1,jkl...>() +
      a.template operator()<2,jkl...>()*b.template operator()<2,jkl...>();
}

template<typename T1, typename T2, int... kl>
inline double contract2(const T1& a, const T2&b) {
  return contract<T1,T2,0,kl...>(a,b) +
      contract<T1,T2,1,kl...>(a,b) +
      contract<T1,T2,2,kl...>(a,b);
}

template<typename T1, typename T2>
inline double contract3(const T1& a, const T2&b) {
   return contract2<T1,T2,0>(a,b) +
       contract2<T1,T2,1>(a,b) +
       contract2<T1,T2,2>(a,b);
}


#endif /* SRC_PRIMITIVES_TENSOR_H_ */
