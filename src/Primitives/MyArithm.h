/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * MyArithm.h
 *
 *  Created on: Sep 23, 2014
 *      Author: kustepha
 */

#ifndef MYARITHM_H_
#define MYARITHM_H_

#include <type_traits>
#include <iostream>

namespace MyArithm
{


  // compensated summation (Kahan sum)
  // https://en.wikipedia.org/wiki/Kahan_summation_algorithm
  template<typename TS, typename TC>
  __attribute__((__always_inline__))
  inline void sum_kahan(TS& sum, TC& compensator, const TS& sample) {
    volatile const TS tmp0 = sample - compensator;
    volatile const TS tmp1 = tmp0 + sum;
    compensator = (tmp1 - sum) - tmp0;
    sum = tmp1;
  }


  //  // Round up "numToRound" to nearest multiple of "multiple"
  //  template<class T1, class T2,
  //  typename std::enable_if<std::is_integral<T1>::value && std::is_integral<T2>::value>::type* = nullptr >
  //  inline T1
  //  roundUp(const T1 numToRound, const T2 multiple)
  //  {
  //    if(multiple == 0)
  //      {
  //	return numToRound;
  //      }
  //
  //    T1 remainder = numToRound % multiple;
  //    if (remainder == 0)
  //      {
  //	return numToRound;
  //      }
  //
  //    return numToRound + multiple - remainder;
  //  }


  // Round up "numToRound" to nearest multiple of "multiple"
  template<class T1, class T2>
  inline constexpr T1
  roundUp(const T1 numToRound, const T2 multiple)
  {
    static_assert(std::is_integral<T1>::value && std::is_integral<T2>::value,"may only be used with integral types");

    if(multiple == 0) return numToRound;

    T1 remainder = numToRound % multiple;
    if (remainder == 0) return numToRound;

    return numToRound + multiple - remainder;
  }



  //  // Round up integer division
  //  template<class T1, class T2,
  //           typename std::enable_if<std::is_integral<T1>::value && std::is_integral<T2>::value>::type* = nullptr >
  //  inline T1
  //  ceilIntDiv(const T1 num, const T2 div)
  //  {
  //    return (num % div) ? num / div + 1 : num / div;
  //  }

  // Round up integer division
  template<class T1, class T2>
  inline T1
  ceilIntDiv(const T1 num, const T2 div)
  {
    static_assert(std::is_integral<T1>::value && std::is_integral<T2>::value,"may only be used with integral types");

    return (num % div) ? num / div + 1 : num / div;
  }

  // Round up to next higher power of 2 (return x if it's already a power
  // of 2).
  inline uint64_t
  nextPow2 (uint64_t x)
  {
    if (x == 0)
      return 1;
    --x;
    x |= x >> 1;
    x |= x >> 2;
    x |= x >> 4;
    x |= x >> 8;
    x |= x >> 16;
    x |= x >> 32;
    return x+1;
  }

  inline
  bool
  isPow2(const uint64_t x)
  {
    return x && !(x & (x-1));
  }

  inline
  bool
  isPow4(const uint64_t x)
  {
    //    3 2 1 0
    //    8 4 2 1
    // A: 1 0 1 0
    return (x & 0xAAAAAAAAAAAAAAAA) && !(x & (x-1));
  }

  inline
  bool
  isPow8(const uint64_t x)
  {
    //      3 2 1 0 | 3 2 1 0 | 3 2 1 0
    //      8 4 2 1 | 8 4 2 1 | 8 4 2 1
    // 924: 1 0 0 1   0 0 1 0   0 1 0 0
    return (x & 0x0924924924924924) && !(x & (x-1));
  }

  // returns number of leading zero bits
  inline
  int
  nlz (uint64_t x)
  {
    if (x == 0) return 64;
    return __builtin_clzl(x);
  }

  // returns index of most significant bit, (0...63)
  // of x, or undefined, if x == o
  inline
  int
  msb (uint64_t x)
  {
    return 63 - __builtin_clzl(x);


//    register uint_fast8_t r = 0; // result of log2(x) will go here
//
//    if (x & 0xFFFFFFFF00000000)
//      {
//        x >>= 32;
//        r |= 32;
//      }
//    if (x & 0xFFFF0000)
//      {
//        x >>= 16;
//        r |= 16;
//      }
//    if (x & 0xFF00)
//      {
//        x >>= 8;
//        r |= 8;
//      }
//    if (x & 0xF0)
//      {
//        x >>= 4;
//        r |= 4;
//      }
//    if (x & 0xC)
//      {
//        x >>= 2;
//        r |= 2;
//      }
//    if (x & 0x2)
//      {
//        x >>= 1;
//        r |= 1;
//      }
//
//    return r;

    //    const unsigned long int b[] = {0x2, 0xC, 0xF0, 0xFF00, 0xFFFF0000, 0xFFFFFFFF00000000};
    //    const unsigned long int S[] = {1, 2, 4, 8, 16, 32};
    //    int i;
    //
    //    register unsigned long int r = 0; // result of log2(x) will go here
    //    for (i = 5; i >= 0; i--) // unroll for speed...
    //      {
    //	if (x & b[i])
    //	  {
    //	    x >>= S[i];
    //	    r |= S[i];
    //	  }
    //      }
    //
    //    return r;

  }

  inline
  uint8_t
  msbOfPow2(const uint64_t x_pow2)
  {

    uint8_t r = ( (x_pow2 & 0xAAAAAAAAAAAAAAAA) != 0 ); // bit is set in odd position -> add 1

    r |= ( (x_pow2 & 0xFFFFFFFF00000000) != 0 ) << 5; // bit is set in upper 32 bits -> add 32
    r |= ( (x_pow2 & 0xFFFF0000FFFF0000) != 0 ) << 4; // in above partition: bit is set in upper 16 bits -> add 16
    r |= ( (x_pow2 & 0xFF00FF00FF00FF00) != 0 ) << 3; // in above partition: bit is set in upper 8 bits -> add 8
    r |= ( (x_pow2 & 0xF0F0F0F0F0F0F0F0) != 0 ) << 2; // in above partition: bit is set in upper 4 bits -> add 4
    r |= ( (x_pow2 & 0xCCCCCCCCCCCCCCCC) != 0 ) << 1; // in above partition: bit is set in upper 2 bits -> add 2


    return r;


  }

  template<typename IntTb, typename IntTe>
  IntTb ipow(IntTb base, IntTe exp)
  {
    IntTb result = 1;
      while (exp)
      {
          if (exp & 1)
              result *= base;
          exp >>= 1;
          base *= base;
      }

      return result;
  }


  template<unsigned ndims, typename IntT1, typename IntT2, typename IntT3>
  void
  ordered2coords(
      IntT1 idx,                   // in: ordered index
      const IntT2 sizes[ndims],    // in: sizes in each dimension, from slowest to fastest (array of ndims IntTs)
      IntT3 coords[ndims])         // out: coordinates corresponding to idx, from slowest to fastest (array of ndims IntTs)
  {

    uint64_t strides[ndims];

    strides[ndims-1] = 1;
    for (unsigned i = ndims-1; i > 0; --i)
      {
        strides[i-1] = strides[i]*sizes[i];
      }

    for (unsigned i = 0; i < ndims; ++i)
      {
        coords[i] = idx / strides[i];
        idx -= coords[i]*strides[i];
      }

  }

  template<typename T>
  void print_bits(T x)
  {
    for (uint8_t i = (8*sizeof(T)-1); i != uint8_t(-1); --i)
      std::cout << ((x>>i) & T(1));
    std::cout << std::endl;
  }





}  // namespace MyArithm




#endif /* MYARITHM_H_ */
