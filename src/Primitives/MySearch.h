/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * MySearch.h
 *
 *  Created on: Jun 30, 2016
 *      Author: kustepha
 */

#ifndef SRC_PRIMITIVES_MYSEARCH_H_
#define SRC_PRIMITIVES_MYSEARCH_H_

#include <algorithm> // lower_bound

#include "Primitives/MyError.h"

namespace MySearch
{


  // find element in sorted iterator range
  template<class Iter, class T>
  Iter binary_find(Iter begin, Iter end, T val)
  {
      // Finds the lower bound in at most log(last - first) + 1 comparisons
      //      Returns an iterator pointing to the first element in the range [first, last)
      //      that is not less than (i.e. greater or equal to) value.
      Iter i = std::lower_bound(begin, end, val);

      if (i != end && !(val < *i))
        return i; // found
      else
        return end; // not found
  }

  // find element in sorted iterator range
  template<class Iter, class T, typename CompFunctor>
  Iter binary_find(Iter begin, Iter end, T val, CompFunctor&& comp)
  {
      // Finds the lower bound in at most log(last - first) + 1 comparisons
      //      Returns an iterator pointing to the first element in the range [first, last)
      //      that is not less than (i.e. greater or equal to) value.
      Iter i = std::lower_bound(begin, end, val, comp);

      if ( i != end && !comp(val,*i) )
        return i; // found
      else
        return end; // not found
  }

  // searches value in sorted iterator range [begin,end) with index range [0,N),
  // where N = std::distance(begin,end),
  // to compute the index j, 0 <= j <= N
  // such that *(begin+j) is smallest value greater than or equal to value
  // returns j = N if no element is greater than or equal to value
  template<class Iter, typename T>
  uint64_t
  binary_search(
      const Iter begin, // sorted range
      const Iter end,
      const T value)
  {
      Iter it = std::lower_bound(begin, end, value);
      return std::distance( begin, it );
  }

  // searches value in sorted iterator range [begin,end) with index range [0,N),
  // where N = std::distance(begin,end),
  // to compute the index j, 0 <= j <= N
  // such that *(begin+j) is smallest value greater than or equal to value
  // returns j = N if no element is greater than or equal to value
  template<class Iter, typename T, typename Compare>
  uint64_t
  binary_search(
      const Iter begin, // sorted range
      const Iter end,
      const T value,
      Compare comp)
  {
      Iter it = std::lower_bound(begin, end, value, comp);
      return std::distance( begin, it );
  }

  // searches value in sorted iterator range [begin,end) with index range [0,N),
  // where N = std::distance(begin,end),
  // to compute the index j, 0 <= j < N
  // such that *(begin+j) is largest value less than or equal to value
  template<class Iter, typename T>
  uint64_t
  binary_search_lt(
      const Iter begin, // exclusive prefix sum
      const Iter end,
      const T value)
  {

      Iter it = std::lower_bound(begin, end, value);
      uint64_t j = std::distance( begin, it );

      if ( it != end && (*it == value || j == 0) )
        return j;
      else
        return j-1;
  }


}


#endif /* SRC_PRIMITIVES_MYSEARCH_H_ */
