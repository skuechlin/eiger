/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * MySort.h
 *
 *  Created on: May 26, 2014
 *      Author: Stephan Kuechlin
 */

#ifndef MYSORT_H_
#define MYSORT_H_

#include <omp.h>
#include <cstring> // memset
#include <utility> // std::swap
#include <algorithm> // std::sort

#include "MyArithm.h"
#include "MyMemory.h"

#include "LoadBalancing/ExactBisection.h"

namespace MySort
{
  namespace {
    template<typename T>
    struct trivial_get_key{};

    template<>
    struct trivial_get_key<uint64_t>{ uint64_t operator()(const uint64_t& k)const{return k;} };
  }

  template <typename T1, typename KeyFunT = trivial_get_key<T1>>
      void RadixSortParallel(
          T1** __restrict__ _data1,
          T1** __restrict__ _data2,
          const uint64_t n,
          const uint64_t max_key = uint64_t(-1),
          KeyFunT getKey = trivial_get_key<T1>()
      )
  {

    T1* __restrict__ data1 = *_data1;
    T1* __restrict__ data2 = *_data2;

    constexpr int nbits = 11;
    constexpr int nbins = 1<<nbits;
    constexpr int mask  = nbins-1;

    constexpr int nbits2 = 8;
    constexpr int nbins2 = 1<<nbits2;
    constexpr int mask2  = nbins2-1;

    constexpr int align = 4096;

    static_assert((nbins*sizeof(uint64_t))%align == 0);
    static_assert(nbits2 < nbits);
    static_assert((64-nbits)/nbits2 <= ((1<<(nbits-nbits2))-1));

    // index (0...63) of msb of max key
    int shift = MyArithm::msb(max_key);
    shift = (shift < nbits) ? 0 : shift - (nbits-1);


    const int nt = omp_get_num_threads();
    const int tn = omp_get_thread_num();

    // pointers to shared array
    static uint64_t* h_all;
    static uint64_t* firstbin = nullptr;

#pragma omp single
    {
      h_all = (uint64_t*)aligned_alloc(align,((nt+1)*nbins + 1)*sizeof(uint64_t));
      std::memset(h_all,0,(nt+1)*nbins*sizeof(uint64_t));
      h_all[(nt+1)*nbins] = n;
    }


    // pointer to thread's range in shared memory
    uint64_t* h_this = (uint64_t*)__builtin_assume_aligned(h_all + tn*nbins,align);
    uint64_t* h_tot  = (uint64_t*)__builtin_assume_aligned(h_all + nt*nbins,align);

    // 1x msb sort

    // thread local histogram (partition values)
#pragma omp for schedule(static)
    for (uint64_t i = 0; i < n; ++i)
      ++h_this[ (getKey(data1[i]) >> shift ) & mask ];

    // parallel prefix sum (partition bins)
#pragma omp for schedule(static)
    for (int j = 0; j < nbins; ++j)
      {
        uint64_t sum = 0;
        for (int t = 0; t < nt+1; ++t)
          {
            uint64_t tmp = h_all[t*nbins + j];
            h_all[t*nbins + j] = sum;
            sum += tmp;
          }
      }

    // serial prefix sum on nt+1'th histogram
    static uint64_t h_max = 0;
#pragma omp single
    {
      uint64_t sum = 0;
      for (int j = 0; j < nbins; ++j)
        {
          uint64_t tmp = h_tot[j];
          h_max = std::max(h_max,tmp);
          h_tot[j] = sum;
          sum += tmp;
        }
    }

    // update local histogram (starting values) from global
    for (int j = 0; j < nbins; ++j)
      h_this[j] += h_tot[j];

    // copy values in parallel
#pragma omp for schedule(static)
    for (uint64_t i = 0; i < n; ++i)
      {
        uint64_t dst = h_this[ (getKey(data1[i]) >> shift ) & mask ]++;
        data2[ dst ] = data1[ i ];
      }


    const int max_shift = shift;

    const uint nshifts = ((max_shift+nbits2-1)/nbits2) & ((1<<(nbits-nbits2))-1);

    if (max_shift > 0)
      {
        // lsd radix sort of remaining bits within each bin

        // load balancing:

#if 1
#pragma omp for schedule(dynamic)
        for (int bin = 0; bin < nbins; ++bin)
#else
#pragma omp single
          {
            firstbin = (uint64_t*)malloc((nt+1)*sizeof(uint64_t));
            const double lavg = static_cast<double>(h_tot[nbins]) / static_cast<double>(nt);
            LoadBalance::ExactBisection::exactBisection(firstbin,h_tot,lavg,h_max,nbins,nt);
          }

        for (uint64_t bin = firstbin[tn]; bin < firstbin[tn+1]; ++bin)
#endif
          {
            if (h_tot[bin] == h_tot[bin+1])
              continue;

            // data for this bin are currently in ...2 arrays
            data1 = *_data2;
            data2 = *_data1;

            // use nshifts histograms with total of nshifts*nbins2 <= nbins bins
            // (nshifts <= 7)
            std::memset(h_this,0,nbins*sizeof(uint64_t));

            // count and prefix sum are independent of position of key

            // count
            for(uint64_t i = h_tot[bin]; i < h_tot[bin+1]; ++i)
              {
                uint64_t key = getKey(data1[i]);
                for (uint k = 0; k < nshifts; ++k, key>>=nbits2)
                  ++h_this[ (key & mask2) + k*nbins2 ];
              }

            // prefix sum on each of the nshift histograms
            for (uint k = 0; k < nshifts; ++k)
              {
                uint64_t sum = h_tot[bin];
                for (int j = 0; j < nbins2; ++j)
                  {
                    uint64_t tmp = h_this[j+k*nbins2];
                    h_this[j+k*nbins2] = sum;
                    sum += tmp;
                  }
              }

            // copy
            for (uint k = 0; k < nshifts; ++k)
              {
                for(uint64_t i = h_tot[bin]; i < h_tot[bin+1]; ++i)
                  {
                    uint64_t dst =  h_this[ (( getKey(data1[i]) >> (k*nbits2) ) & mask2) + k*nbins2 ]++;
                    data2[ dst ]    = data1[ i ];
                  }
                std::swap(data1,data2);
              }


          }// for all bins

      } // if max_shift > 0


    if (!(nshifts&1))
      { // even number of copies -> data are in 2:
        std::swap(*_data1,*_data2);
      }

#pragma omp barrier


    // epilogoue
#pragma omp single nowait
    {
      free(h_all);
      free(firstbin);
    }

  }

  template <typename T1, typename T2, typename KeyFunT = trivial_get_key<T1>>
  void RadixSortParallel(
      T1** __restrict__ _keys1,
      T1** __restrict__ _keys2,
      T2** __restrict__ _payload1,
      T2** __restrict__ _payload2,
      const uint64_t n,
      const uint64_t max_key = uint64_t(-1),
      KeyFunT getKey = trivial_get_key<T1>()
  )
  {

    T1* __restrict__ keys1 = *_keys1;
    T1* __restrict__ keys2 = *_keys2;

    T2* __restrict__ payload1 = *_payload1;
    T2* __restrict__ payload2 = *_payload2;

    constexpr int nbits = 11;
    constexpr int nbins = 1<<nbits;
    constexpr int mask  = nbins-1;

    constexpr int nbits2 = 8;
    constexpr int nbins2 = 1<<nbits2;
    constexpr int mask2  = nbins2-1;

    constexpr int align = 4096;

    static_assert((nbins*sizeof(uint64_t))%align == 0);
    static_assert(nbits2 < nbits);
    static_assert((64-nbits)/nbits2 <= ((1<<(nbits-nbits2))-1));

    // index (0...63) of msb of max key
    int shift = MyArithm::msb(max_key);
    shift = (shift < nbits) ? 0 : shift - (nbits-1);


    const int nt = omp_get_num_threads();
    const int tn = omp_get_thread_num();

    // pointers to shared array
//    static uint64_t* h_all;
//    static uint64_t* firstbin = nullptr;
    static MyMemory::aligned_array<uint64_t,align> h_all;
    static MyMemory::aligned_array<uint64_t> firstbin;

#pragma omp single
    {
//      h_all = (uint64_t*)aligned_alloc(align,((nt+1)*nbins + 1)*sizeof(uint64_t));
      h_all.resize((nt+1)*nbins);
      std::memset(&(h_all[0]),0,(nt+1)*nbins*sizeof(uint64_t));
      h_all[(nt+1)*nbins] = n;
    }

    // pointer to thread's range in shared memory
    uint64_t* h_this = (uint64_t*)__builtin_assume_aligned(&(h_all[0]) + tn*nbins,align);
    uint64_t* h_tot  = (uint64_t*)__builtin_assume_aligned(&(h_all[0]) + nt*nbins,align);

    // 1x msb sort

    // thread local histogram (partition values)
#pragma omp for schedule(static)
    for (uint64_t i = 0; i < n; ++i)
      ++h_this[ (getKey(keys1[i]) >> shift ) & mask ];

    // parallel prefix sum (partition bins)
#pragma omp for schedule(static)
    for (int j = 0; j < nbins; ++j)
      {
        uint64_t sum = 0;
        for (int t = 0; t < nt+1; ++t)
          {
            uint64_t tmp = h_all[t*nbins + j];
            h_all[t*nbins + j] = sum;
            sum += tmp;
          }
      }

    // serial prefix sum on nt+1'th histogram
    static uint64_t h_max = 0;
#pragma omp single
    {
      uint64_t sum = 0;
      for (int j = 0; j < nbins; ++j)
        {
          uint64_t tmp = h_tot[j];
          h_max = std::max(h_max,tmp);
          h_tot[j] = sum;
          sum += tmp;
        }
    }

    // update local histogram (starting values) from global
    for (int j = 0; j < nbins; ++j)
      h_this[j] += h_tot[j];

    // copy values in parallel
#pragma omp for schedule(static)
    for (uint64_t i = 0; i < n; ++i)
      {
        uint64_t dst = h_this[ ( getKey(keys1[i]) >> shift ) & mask ]++;
        keys2[ dst ]    = keys1[ i ];
        payload2[ dst ] = payload1[ i ];
      }


    const int max_shift = shift;

    const uint nshifts = ((max_shift+nbits2-1)/nbits2) & ((1<<(nbits-nbits2))-1);

    if (max_shift > 0)
      {
        // lsd radix sort of remaining bits within each bin

        // load balancing:

#if 0
#pragma omp for schedule(static)
        for (int bin = 0; bin < nbins; ++bin)
#else
#pragma omp single
          {
//            firstbin = (uint64_t*)malloc((nt+1)*sizeof(uint64_t));
            firstbin.resize((nt+1));
            const double lavg = static_cast<double>(h_tot[nbins]) / static_cast<double>(nt);
            LoadBalance::ExactBisection::exactBisection(firstbin.data(),h_tot,lavg,h_max,nbins,nt);
          }

        for (uint64_t bin = firstbin[tn]; bin < firstbin[tn+1]; ++bin)
#endif
          {
            if (h_tot[bin] == h_tot[bin+1])
              continue;

            // data for this bin are currently in ...2 arrays
            keys1 = *_keys2;
            keys2 = *_keys1;
            payload1 = *_payload2;
            payload2 = *_payload1;

            // use nshifts histograms with total of nshifts*nbins2 <= nbins bins
            // (nshifts <= 7)
            std::memset(h_this,0,nbins*sizeof(uint64_t));

            // count and prefix sum are independent of position of key

            // count
            for(uint64_t i = h_tot[bin]; i < h_tot[bin+1]; ++i)
              {
                uint64_t key = getKey(keys1[i]);
                for (uint k = 0; k < nshifts; ++k, key>>=nbits2)
                  ++h_this[ (key & mask2) + k*nbins2 ];
              }

            // prefix sum on each of the nshift histograms
            for (uint k = 0; k < nshifts; ++k)
              {
                uint64_t sum = h_tot[bin];
                for (int j = 0; j < nbins2; ++j)
                  {
                    uint64_t tmp = h_this[j+k*nbins2];
                    h_this[j+k*nbins2] = sum;
                    sum += tmp;
                  }
              }

            // copy
            for (uint k = 0; k < nshifts; ++k)
              {
                for(uint64_t i = h_tot[bin]; i < h_tot[bin+1]; ++i)
                  {
                    uint64_t dst =  h_this[ (( getKey(keys1[i]) >> (k*nbits2) ) & mask2) + k*nbins2 ]++;
                    keys2[ dst ]    = keys1[ i ];
                    payload2[ dst ] = payload1[ i ];
                  }
                std::swap(keys1,keys2);
                std::swap(payload1,payload2);
              }


          }// for all bins

      } // if max_shift > 0


    if (!(nshifts&1))
      { // even number of copies -> data are in 2:
        std::swap(*_keys1,*_keys2);
        std::swap(*_payload1,*_payload2);
      }

#pragma omp barrier


//    // epilogoue
//#pragma omp single nowait
//    {
////      free(h_all);
////      free(firstbin);
//    }

  }

  //  template <typename T1, typename T2, typename KeyFunT = trivial_get_key<T1>>
  //      void RadixSortParallel2(
  //          T1** __restrict__ _keys1,
  //          T1** __restrict__ _keys2,
  //          T2** __restrict__ _payload1,
  //          T2** __restrict__ _payload2,
  //          const uint64_t n,
  //          const uint64_t max_key = uint64_t(-1),
  //          KeyFunT getKey = trivial_get_key<T1>()
  //      )
  //  {
  //
  //    T1* __restrict__ keys1 = *_keys1;
  //    T1* __restrict__ keys2 = *_keys2;
  //    T2* __restrict__ payload1 = *_payload1;
  //    T2* __restrict__ payload2 = *_payload2;
  //
  //    constexpr int nbits = 11;
  //    constexpr int nbins = 1<<nbits;
  //    constexpr int mask  = nbins-1;
  //
  //    constexpr int nbits2 = 8;
  //    constexpr int nbins2 = 1<<nbits2;
  //    constexpr int mask2  = nbins2-1;
  //
  //    constexpr int align = 4096;
  //
  //    static_assert((nbins*sizeof(uint64_t))%align == 0);
  //    static_assert(nbits2 < nbits);
  //    static_assert((64-nbits)/nbits2 <= ((1<<(nbits-nbits2))-1));
  //
  //    // index (0...63) of msb of max key
  //    int shift = MyArithm::msb(max_key);
  //    shift = (shift < nbits) ? 0 : shift - (nbits-1);
  //
  //
  //    const int nt = omp_get_num_threads();
  //    const int tn = omp_get_thread_num();
  //
  //    // pointers to shared array
  //    static uint64_t* h_all;
  //    static uint64_t* firstbin = nullptr;
  //
  //#pragma omp single
  //    {
  //      h_all = (uint64_t*)aligned_alloc(align,((nt+1)*nbins + 1)*sizeof(uint64_t));
  //      std::memset(h_all,0,(nt+1)*nbins*sizeof(uint64_t));
  //      h_all[(nt+1)*nbins] = n;
  //    }
  //
  //
  //    // pointer to thread's range in shared memory
  //    uint64_t* h_this = (uint64_t*)__builtin_assume_aligned(h_all + tn*nbins,align);
  //    uint64_t* h_tot  = (uint64_t*)__builtin_assume_aligned(h_all + nt*nbins,align);
  //
  //    // 1x msb sort
  //
  //    // thread local histogram (partition values)
  //#pragma omp for schedule(static)
  //    for (uint64_t i = 0; i < n; ++i)
  //      ++h_this[ (getKey(keys1+i) >> shift ) & mask ];
  //
  //    // parallel prefix sum (partition bins)
  //#pragma omp for schedule(static)
  //    for (int j = 0; j < nbins; ++j)
  //      {
  //        uint64_t sum = 0;
  //        for (int t = 0; t < nt+1; ++t)
  //          {
  //            uint64_t tmp = h_all[t*nbins + j];
  //            h_all[t*nbins + j] = sum;
  //            sum += tmp;
  //          }
  //      }
  //
  //    // serial prefix sum on nt+1'th histogram
  //    static uint64_t h_max = 0;
  //#pragma omp single
  //    {
  //      uint64_t sum = 0;
  //      for (int j = 0; j < nbins; ++j)
  //        {
  //          uint64_t tmp = h_tot[j];
  //          h_max = std::max(h_max,tmp);
  //          h_tot[j] = sum;
  //          sum += tmp;
  //        }
  //    }
  //
  //    // update local histogram (starting values) from global
  //    for (int j = 0; j < nbins; ++j)
  //      h_this[j] += h_tot[j];
  //
  //    // copy values in parallel
  //#pragma omp for schedule(static)
  //    for (uint64_t i = 0; i < n; ++i)
  //      {
  //        uint64_t dst = h_this[ (getKey(keys1+i) >> shift ) & mask ]++;
  //        keys2[ dst ] = keys1[ i ];
  //        payload2[ dst ] = payload1[ i ];
  //      }
  //
  //
  //    const int max_shift = shift;
  //
  //    const uint nshifts = ((max_shift+nbits2-1)/nbits2) & ((1<<(nbits-nbits2))-1);
  //
  //    if (max_shift > 0)
  //      {
  //        // lsd radix sort of remaining bits within each bin
  //
  //        // load balancing:
  //
  //#if 0
  //#pragma omp for schedule(dynamic)
  //        for (int bin = 0; bin < nbins; ++bin)
  //#else
  //#pragma omp single
  //          {
  //            firstbin = (uint64_t*)malloc((nt+1)*sizeof(uint64_t));
  //            const double lavg = static_cast<double>(h_tot[nbins]) / static_cast<double>(nt);
  //            LoadBalance::ExactBisection::exactBisection(firstbin,h_tot,lavg,h_max,nbins,nt);
  //          }
  //
  //        for (uint64_t bin = firstbin[tn]; bin < firstbin[tn+1]; ++bin)
  //#endif
  //          {
  //            if (h_tot[bin] == h_tot[bin+1])
  //              continue;
  //
  //            //            if ( (h_tot[bin+1] - h_tot[bin]) < 16)
  //            //              {
  //            //                const uint64_t begin = h_tot[bin];
  //            //                const uint64_t n = h_tot[bin+1] - begin;
  //            //
  //            //                StdSortCopy(
  //            //                    *_keys2     + begin,
  //            //                    *_keys1     + begin,
  //            //                    *_payload2  + begin,
  //            //                    *_payload1  + begin,
  //            //                    n,
  //            //                    getKey);
  //            //
  //            //                // data are now in ...1 arrays
  //            //                if (!(nshifts&1))
  //            //                  { // even number of copies -> expect data to be in 2:
  //            //                    std::memcpy(*_keys2+begin,*_keys1+begin,n*sizeof(T1));
  //            //                    std::memcpy(*_payload2+begin,*_payload1+begin,n*sizeof(T2));
  //            //                  }
  //            //
  //            //                continue; // done with this bin
  //            //              }
  //
  //            // data for this bin are currently in ...2 arrays
  //            keys1 = *_keys2;
  //            keys2 = *_keys1;
  //            payload1 = *_payload2;
  //            payload2 = *_payload1;
  //
  //            // use nshifts histograms with total of nshifts*nbins2 <= nbins bins
  //            // (nshifts <= 7)
  //            std::memset(h_this,0,nbins*sizeof(uint64_t));
  //
  //            // count and prefix sum are independent of position of key
  //
  //            // count
  //            for(uint64_t i = h_tot[bin]; i < h_tot[bin+1]; ++i)
  //              {
  //                uint64_t key = getKey(keys1+i);
  //                for (uint k = 0; k < nshifts; ++k, key>>=nbits2)
  //                  ++h_this[ (key & mask2) + k*nbins2 ];
  //              }
  //
  //            // prefix sum on each of the nshift histograms
  //            for (uint k = 0; k < nshifts; ++k)
  //              {
  //                uint64_t sum = h_tot[bin];
  //                for (int j = 0; j < nbins2; ++j)
  //                  {
  //                    uint64_t tmp = h_this[j+k*nbins2];
  //                    h_this[j+k*nbins2] = sum;
  //                    sum += tmp;
  //                  }
  //              }
  //
  //            // copy
  //            for (uint k = 0; k < nshifts; ++k)
  //              {
  //                for(uint64_t i = h_tot[bin]; i < h_tot[bin+1]; ++i)
  //                  {
  //                    uint64_t dst =  h_this[ (( getKey(keys1+i) >> (k*nbits2) ) & mask2) + k*nbins2 ]++;
  //                    keys2[ dst ]    = keys1[ i ];
  //                    payload2[ dst ] = payload1[ i ];
  //                  }
  //                std::swap(keys1,keys2);
  //                std::swap(payload1,payload2);
  //              }
  //
  //            //            for(shift = 0; shift < max_shift; shift += nbits2)
  //            //              {
  //            //
  //            //                // reset histogram
  //            //                std::memset(h_this,0,nbins2*sizeof(uint64_t));
  //            //
  //            //                // count
  //            //                for(uint64_t i = h_tot[bin]; i < h_tot[bin+1]; ++i)
  //            //                  ++h_this[ (getKey(keys1+i) >> shift ) & mask2 ];
  //            //
  //            //                // prefix sum
  //            //                uint64_t sum = h_tot[bin];
  //            //                for (int j = 0; j < nbins2; ++j)
  //            //                  {
  //            //                    uint64_t tmp = h_this[j];
  //            //                    h_this[j] = sum;
  //            //                    sum += tmp;
  //            //                  }
  //            //
  //            //                // copy
  //            //                for(uint64_t i = h_tot[bin]; i < h_tot[bin+1]; ++i)
  //            //                  {
  //            //                    uint64_t dst =  h_this[ (getKey(keys1+i) >> shift ) & mask2 ]++;
  //            //                    keys2[ dst ]    = keys1[ i ];
  //            //                    payload2[ dst ] = payload1[ i ];
  //            //                  }
  //            //
  //            //                std::swap(keys1,keys2);
  //            //                std::swap(payload1,payload2);
  //            //
  //            //              } // for all bits
  //          }// for all bins
  //
  //      } // if max_shift > 0
  //
  //
  //    if (!(nshifts&1))
  //      { // even number of copies -> data are in 2:
  //        std::swap(*_keys1,*_keys2);
  //        std::swap(*_payload1,*_payload2);
  //      }
  //
  //#pragma omp barrier
  //
  //
  //    // epilogoue
  //#pragma omp single nowait
  //    {
  //      free(h_all);
  //      free(firstbin);
  //    }
  //
  //  }

} // namespace MySort

#endif /* MYSORT_H_ */
