/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * Timer.h
 *
 *  Created on: Aug 25, 2014
 *      Author: kustepha
 */

#ifndef TIMER_H_
#define TIMER_H_

#include <omp.h>
#include <string>
#include <chrono>
#include <map>
#include <unordered_map>
#include <algorithm>
#include <iostream>
#include <iomanip>

#include "MyError.h"


namespace MyChrono
{

  template<typename fromDuration, typename toDuration>
  double
  durationRatio()
  {
    auto p1 = typename fromDuration::period();
    auto p2 = typename toDuration::period();
    if (p1.num == p2.num && p1.den == p2.den) return 1.0;
    return static_cast<double>( p1.num * p2.den  ) / static_cast<double>( p1.den * p2.num );
  }

  struct omp_clock
  {
    typedef std::chrono::duration<double> duration;
    typedef duration::rep rep;
    typedef duration::period period;
    typedef std::chrono::time_point<omp_clock, duration> time_point;

    static constexpr bool is_steady = true;

    static double now() noexcept { return  omp_get_wtime(); }
  };

  struct c_clock
  {
    typedef std::chrono::duration<double> duration;
    typedef duration::rep rep;
    typedef duration::period period;
    typedef std::chrono::time_point<c_clock, duration> time_point;

    static constexpr bool is_steady = true;

    static double now() noexcept { return  clock()/static_cast<double>(CLOCKS_PER_SEC); }
  };

  class Timer
  {

    typedef omp_clock the_clock;

    bool running_ = false;

    double start_ = 0.;
    double elapsed_ = 0.;

    double ave_ = 0.;
    double nsamps_ = 0.;

  private:

    double now() const { return the_clock::now(); }

    bool running() const { return running_;  }

    double start_time() const { return start_; }

    bool set_running(const bool state)
    {
      bool ret = running_;
      running_ = state;
      return ret;
    }

  public:
    Timer() = default;

  public:

    template<class Duration = std::chrono::seconds>
    double elapsed() const {
      double elapsed = elapsed_;

      if (running())
        elapsed += now() - start_time();

      return elapsed * durationRatio<the_clock::duration,Duration>();
    }

    void start() { // set start time to current time
      set_running(true);
      start_ = now();
    }

    void stop() {// add the time since last start to elapsed time
      if ( set_running(false) )
        elapsed_ += the_clock::now() - start_time();
    }

    void reset() { elapsed_ = 0.; }// set elapsed time to 0

    // sampling

    void sample() {
      double samp = elapsed<the_clock::duration>();

      ave_ -= samp;
      ave_ *= ( 1.0 - 1.0/(++nsamps_) );
      ave_ += samp;
    }

    template<class Duration = std::chrono::seconds>
    double average() const { return ave_ * durationRatio<the_clock::duration,Duration>(); }

    void resetSampling() {
      ave_ = 0.;
      nsamps_ = 0;
    }

    template<class Duration = std::chrono::seconds>
    void print_elapsed() {
      std::cout << std::fixed << std::setprecision(6)  << std::setw(12) << elapsed<Duration>();
    }


  };


  class TimerCollection
  {
    typedef std::unordered_map<std::string, Timer> map_t;

    map_t timers_;

  public:

    void start(const std::string& name) {
#pragma omp master
      timers_[name].start();
    }

    void stop(const std::string& name) {
#pragma omp master
      timers_[name].stop();
    }

    template<class Duration = std::chrono::seconds>
    double elapsed(const std::string& name) { // only master returns !0
      double ret = 0.;
#pragma omp master
      ret = timers_[name].elapsed<Duration>();
      return ret;
    }

    template<class Duration = std::chrono::seconds>
    void print_elapsed(const std::string& name) {
#pragma omp master
      timers_[name].print_elapsed<Duration>();
    }


    void reset() {
      std::for_each(
          timers_.begin(),
          timers_.end(),
          [](map_t::value_type& m){m.second.reset();}
      );
    }

    void sample() {
      std::for_each(
          timers_.begin(),
          timers_.end(),
          [](map_t::value_type& m){m.second.sample();}
      );
    }

    template<class Duration = std::chrono::seconds>
    void print_elapsed() {

      typedef std::multimap<double,std::string,std::greater<double>>  heap_t;
      heap_t heap;

      std::for_each(
          timers_.begin(),
          timers_.end(),
          [&heap](const map_t::value_type& m){heap.emplace(m.second.elapsed<Duration>(),m.first);}
      );

      // heap should now contain the names of the timers sorted descending by elapsed time

      double maxt = heap.begin()->first;

      unsigned long long maxNameLen = std::max_element(
          heap.begin(),
          heap.end(),
          [](const heap_t::value_type& p1, const heap_t::value_type& p2){
        return p1.second.length() < p2.second.length();
      }
      )->second.length();


      // print
      std::for_each(
          heap.begin(),
          heap.end(),
          [&maxt,&maxNameLen](const heap_t::value_type& p){

        std::cout << std::setw(maxNameLen) << p.second;
        std::cout << std::fixed << std::setprecision(6)  << std::setw(12) << p.first;
        std::cout << std::fixed << std::setprecision(6)  << std::setw(12) << p.first / maxt;
        std::cout << std::endl;

      }
      );



    }


  };








} /* namespace MyChrono */

#endif /* TIMER_H_ */
