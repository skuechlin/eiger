/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * MyPartitioning.h
 *
 *  Created on: Mar 22, 2016
 *      Author: kustepha
 */

#ifndef SRC_PRIMITIVES_PARTITIONING_H_
#define SRC_PRIMITIVES_PARTITIONING_H_

#include <cstdint>
#include <vector>

namespace MyPartitioning
{


  inline
  uint64_t
  partitionLength (
      const uint64_t partNum,
      const uint64_t totalLength,
      const uint64_t numParts ) {
    return totalLength/numParts + (partNum < (totalLength % numParts) ? 1 : 0);
  }

  inline
  uint64_t
  partitionStart (
      const uint64_t partNum,
      const uint64_t totalLength,
      const uint64_t numParts ) {
    uint64_t rest = (totalLength % numParts);
    return partNum * partitionLength(partNum,totalLength,numParts) + (partNum <  rest ? 0 : rest);
  }


  inline
  uint64_t
  partitionEnd (
      const uint64_t partNum,
      const uint64_t totalLength,
      const uint64_t numParts ) {
    return partitionStart(partNum,totalLength,numParts) + partitionLength(partNum,totalLength,numParts);
  }



} // namespace MyPartitioning

#endif /* SRC_PRIMITIVES_PARTITIONING_H_ */
