/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * MyCount.h
 *
 *  Created on: Apr 14, 2016
 *      Author: kustepha
 */

#ifndef SRC_PRIMITIVES_MYCOUNT_H_
#define SRC_PRIMITIVES_MYCOUNT_H_

#include <limits>
#include <omp.h>

#include "Primitives/Partitioning.h"
#include "Primitives/MyError.h"
#include "Primitives/MySearch.h"

#include "MyArithm.h"
#include "MyError.h"

namespace MyCount
{

  namespace {
    template<typename T>
    struct trivial_get_key{};

    template<>
    struct trivial_get_key<uint64_t>{
      uint64_t operator()(const uint64_t& k)const{return k;}
    };

    template<typename GetDataKey>
    struct Comp {
      const GetDataKey key;
      template<typename DataT>
      bool operator()(const DataT& a, const DataT& b)const{return key(a) < key(b);}
      template<typename DataT, typename KeyT, typename = std::enable_if_t<!std::is_same_v<KeyT,DataT>>>
          bool operator()(const DataT& a, const KeyT& b)const{return key(a) < b;}
      Comp(GetDataKey k):key(k){}
    };
  }

  // count sorted things
  template<
  typename CountT, typename SizeT1, typename SizeT2,
  typename DataT, typename BinT,
  typename GetDataKey = trivial_get_key<DataT>,
  typename GetBinKey  = trivial_get_key<BinT>
  >
  void
  parallelCount(
      CountT* __restrict__ count,     // out
      CountT* __restrict__ begin,     // out
      const BinT*  __restrict__ bins,   // in
      const SizeT1 nbins,    // in
      const DataT* __restrict__ data,   // in
      const SizeT2 ndata,    // in
      GetDataKey getDataKey = trivial_get_key<DataT>(),
      GetBinKey getBinKey = trivial_get_key<BinT>()
  )
  {


    // count:     array of nbins    IntT, count[i] will hold number of element with key belonging to bin i
    // begin:     array of nbins    IntT, begin[i] will hold index of first element with key belonging to bin i
    // keys:      array of nkeys    IntT, key[i] key value, assumed sorted ascending order
    // binkeys:   array of nbins+1  IntT, binkey[i] key values of bins, assumed sorted ascending order
    // nbins:     the number of bins
    // nkeys:     the number of keys

    // parallel info
    const uint64_t nthreads = omp_get_num_threads();
    const uint64_t ithread  = omp_get_thread_num();

    // partition bins sequentially
    const uint64_t first = MyPartitioning::partitionStart(ithread,nbins,nthreads);
    const uint64_t last  = first + MyPartitioning::partitionLength(ithread,nbins,nthreads);

    if (last>first)
      {


        Comp<GetDataKey> comp(getDataKey);

        begin[first] = MySearch::binary_search(
            data,data+ndata,
            getBinKey(bins[first]),
            comp);

        for (uint64_t i = first+1; i < last; ++i)
          begin[i] = begin[i-1] + MySearch::binary_search(
              data+begin[i-1],data+ndata,
              getBinKey(bins[i]),
              comp);
      }

#pragma omp barrier

    if (last>first)
      {

        if (last == nbins)
          count[last-1] = ndata - begin[last-1];
        else
          count[last-1] = begin[last] - begin[last-1];

        for (uint64_t i = first; i < last-1; ++i)
          count[i] = begin[i+1] - begin[i];
      }

#pragma omp barrier

  }


  // parallel exclusive prefix sum
  template<typename IntT, typename IntT2>
  void parallel_exclusive_scan(IntT* counts, IntT2 N)
  {
    static IntT* shared_count;

    const int ithread = omp_get_thread_num();
    const int nthreads = omp_get_num_threads();

#pragma omp single
    {
      shared_count = new IntT[nthreads+1];
      shared_count[0] = 0;
    }

    IntT& sum = shared_count[ithread+1];
    sum = 0;
#pragma omp for schedule(static)
    for (IntT2 i = 0; i < N; ++i)
      {
        IntT tmp = counts[i];
        counts[i] = sum;
        sum += tmp;
      }

    IntT offset = 0;
    for (int i = 0; i < (ithread+1); ++i)
      offset += shared_count[i];

#pragma omp for schedule(static)
    for (IntT2 i = 0; i < N; ++i)
      counts[i] += offset;

#pragma omp single nowait
      delete[] shared_count;
  }



  // out-of-place parallel exclusive prefix sum
  template<typename IntT, typename IntT2>
  void parallel_exclusive_scan(IntT* psum, const IntT* counts, IntT2 N)
  {
    static IntT* shared_count;

    const int ithread = omp_get_thread_num();
    const int nthreads = omp_get_num_threads();

#pragma omp single
    {
      shared_count = new IntT[nthreads+1];
      shared_count[0] = 0;
    }

    IntT& sum = shared_count[ithread+1];
    sum = 0;
#pragma omp for schedule(static)
    for (IntT2 i = 0; i < N; ++i)
      {
        psum[i] = sum;
        sum += counts[i];
      }

    IntT offset = 0;
    for (int i = 0; i < (ithread+1); ++i)
      offset += shared_count[i];

#pragma omp for schedule(static)
    for (IntT2 i = 0; i < N; ++i)
      psum[i] += offset;

#pragma omp single nowait
      delete[] shared_count;
  }


  //  // count sorted things
  //  template<typename IntT, typename KeyFunctor>
  //  void
  //  parallelCount(
  //      IntT count[],     // out
  //      IntT begin[],     // out
  //      const IntT binkeys[],     // in
  //      const IntT nbins,         // in
  //      const IntT nkeys,         // in
  //      KeyFunctor&& getKey       // in
  //  )
  //  {
  //
  //    // count:     array of nbins    IntT, count[i] will hold number of element with key belonging to bin i
  //    // begin:     array of nbins    IntT, begin[i] will hold index of first element with key belonging to bin i
  //    // binkeys:   array of nbins+1  IntT, key[i] key values of bins, assumed sorted ascending order
  //    // nbins:     the number of bins
  //    // nkeys:     the number of keys
  //    // getKey:    callable object that returns the ith key when invoked as getKey(i)
  //
  //
  //    // parallel info
  //    const uint64_t nthreads = omp_get_num_threads();
  //    const uint64_t ithread  = omp_get_thread_num();
  //
  //    // partition bins sequentially
  //    const uint64_t first = MyPartitioning::partitionStart(ithread,nbins,nthreads);
  //    const uint64_t last  = first + MyPartitioning::partitionLength(ithread,nbins,nthreads);
  //
  //    // count, divide key range among threads
  //
  //    // zero count
  //    for(uint64_t i=first; i<last; ++i)
  //      count[i] = 0;
  //
  //    // shared total
  //    static uint64_t* total;
  //#pragma omp single
  //    total = (uint64_t*)malloc(nthreads*sizeof(uint64_t));
  //
  //    // implicit barrier
  //
  //
  //    uint64_t bin  = 0;          // index of current bin
  //    uint64_t nbin = 1;          // index of next bin
  //    IntT nbkey = binkeys[ nbin ];   // key of next bin
  //#pragma omp for schedule(static)
  //    for(uint64_t i=0; i<nkeys; ++i)
  //      {
  //        const IntT pkey = getKey(i); // key of particle
  //        //        MYASSERT(pkey < binkeys[ nbins ],std::string("invalid key: ") + std::to_string(pkey));
  //
  //        while ( pkey >= nbkey )
  //          {
  //            bin     = nbin++;
  //            //            MYASSERT(bin < nbins,std::string("invalid key: ") + std::to_string(pkey));
  //            nbkey   = binkeys[ nbin ];
  //          }
  //
  //#pragma omp atomic update
  //        ++count[ bin ];
  //      }
  //
  //    // implicit barrier
  //
  //    // parallel exclusive prefix sum on count yields begin
  //    IntT sum = 0;
  //    for(uint64_t i=first; i<last; ++i)
  //      {
  //        begin[i] = sum;
  //        sum += count[i];
  //      }
  //    total[ithread] = sum;
  //
  //#pragma omp barrier
  //
  //    IntT offset = 0;
  //    for (uint64_t j = 0; j < ithread; ++j) // threads w small index summed preceding bins
  //      offset += total[j];
  //
  //    for(uint64_t i=first; i<last; ++i)
  //      begin[i] += offset;
  //
  //#pragma omp barrier
  //
  //#pragma omp single nowait
  //    free(total);
  //
  //  }



  //  // count sorted things
  //  template<typename IntT, typename KeyFunctor>
  //  void
  //  parallelCount(
  //      IntT count[],     // out
  //      IntT begin[],     // out
  //      const IntT binkeys[],     // in
  //      const IntT nbins,         // in
  //      const IntT nkeys,         // in
  //      KeyFunctor&& getKey       // in
  //  )
  //  {
  //
  //    // count:     array of nbins    IntT, count[i] will hold number of element with key belonging to bin i
  //    // begin:     array of nbins    IntT, begin[i] will hold index of first element with key belonging to bin i
  //    // binkeys:   array of nbins+1  IntT, key[i] key values of bins, assumed sorted ascending order
  //    // nbins:     the number of bins
  //    // nkeys:     the number of keys
  //    // getKey:    callable object that returns the ith key when invoked as getKey(i)
  //
  //
  //    constexpr uint64_t tsz = sizeof(IntT); // size of type in bytes
  //    constexpr uint64_t pgsz = 4096;        // size of page in bytes
  //
  //    const uint64_t bytes_per_thread = MyArithm::roundUp( nbins*tsz, pgsz ); //round to a multiple of page size (4096 bytes)
  //
  //    const uint64_t local_extent = bytes_per_thread / tsz;
  //
  //
  //    // global pointer to shared array
  //    //    static IntT* shared_count;
  //    //    static IntT* shared_total;
  //
  //
  //    // parallel info
  //    const uint64_t nthreads = omp_get_num_threads();
  //    const uint64_t ithread = omp_get_thread_num();
  //
  //    const uint64_t local_offset = local_extent*ithread;
  //
  //    static uint64_t shared_count_size_bytes = 0;
  //    static uint64_t shared_total_size_bytes = 0;
  //
  //    typedef void (&deleter_t)(void*);
  //
  //    // (global) pointer to shared array
  //    // alloc only once and reuse
  //    // pray that this is thread safe
  //    static std::unique_ptr<IntT,deleter_t> shared_count
  //    (
  //        nullptr,
  //        _mm_free // use to free aligned memory
  //    );
  //
  //    static std::unique_ptr<IntT,deleter_t> shared_total
  //    (
  //        nullptr,
  //        _mm_free // use to free aligned memory
  //    );
  //
  //    // single thread allocs the shared memory region for count
  //#pragma omp sections
  //    {
  //
  //
  //
  //#pragma omp section
  //      {
  //        const uint64_t new_shared_count_size_bytes = bytes_per_thread*nthreads;
  //        if (new_shared_count_size_bytes > shared_count_size_bytes)
  //          {
  //            shared_count_size_bytes = new_shared_count_size_bytes;
  //
  //            shared_count.reset(
  //                (IntT*)_mm_malloc(shared_count_size_bytes, pgsz)  //align memory to page size
  //            );
  //          }
  //      }
  //
  //#pragma omp section
  //      {
  //        const uint64_t new_shared_total_size_bytes = nthreads*tsz;
  //
  //        if (new_shared_total_size_bytes > shared_total_size_bytes)
  //          {
  //            shared_total_size_bytes = new_shared_total_size_bytes;
  //
  //            shared_total.reset(
  //                (IntT*)_mm_malloc(shared_total_size_bytes, pgsz)
  //            );
  //          }
  //      }
  //
  //    }
  //
  //
  //
  //
  //    //    MYASSERT(shared_count != nullptr,std::string("_mm_malloc failed to allocate ") + std::to_string(bytes_per_thread*nthreads)
  //    //    +std::string(" bytes at ") + std::to_string(pgsz) + std::string(" byte boundary for shared count"));
  //
  //    // pointer to thread's range in shared memory
  //    IntT* private_count = shared_count.get() + local_offset;
  //
  //    // initialize to zero
  //    for(uint64_t i=0; i<nbins; ++i)
  //      private_count[i] = 0;
  //
  //    // private count, divide key range among threads
  //    uint64_t bin  = 0;          // index of current bin
  //    uint64_t nbin = 1;          // index of next bin
  //    IntT nbkey = binkeys[ nbin ];   // key of next bin
  //#pragma omp for schedule(static)
  //    for(uint64_t i=0; i<nkeys; ++i)
  //      {
  //        const IntT pkey = getKey(i); // key of particle
  //        //        MYASSERT(pkey < binkeys[ nbins ],std::string("invalid key: ") + std::to_string(pkey));
  //
  //        while ( pkey >= nbkey )
  //          {
  //            bin     = nbin++;
  //            //            MYASSERT(bin < nbins,std::string("invalid key: ") + std::to_string(pkey));
  //            nbkey   = binkeys[ nbin ];
  //          }
  //
  //
  //        ++private_count[ bin ];
  //      }
  //
  //
  //
  //    // parallel reduce to count
  //#pragma omp for schedule(static)
  //    for(uint64_t i=0; i<nbins; ++i)
  //      {
  //        count[i] = 0;
  //        for(uint64_t t=0; t<nthreads; ++t)
  //          count[i] += shared_count[local_extent*t + i];
  //      }
  //
  //
  //    // parallel exclusive prefix sum on count yields begin
  //    IntT sum = 0;
  //#pragma omp for schedule(static)
  //    for(uint64_t i=0; i<nbins; ++i)
  //      sum += count[i];
  //
  //    shared_total.get()[ithread] = sum;
  //#pragma omp barrier
  //
  //    // compute private offset
  //    sum = 0;
  //    for (uint64_t i = 0; i < ithread; ++i)
  //      sum += shared_total.get()[i];
  //
  //#pragma omp for schedule(static)
  //    for(uint64_t i=0; i<nbins; ++i)
  //      {
  //        begin[i] = sum;
  //        sum += count[i];
  //      }
  //
  //    //
  //    //#pragma omp single nowait
  //    //    {
  //    //      _mm_free(shared_count);
  //    //      _mm_free(shared_total);
  //    //    }
  //
  //
  //
  //
  //  }




}



#endif /* SRC_PRIMITIVES_MYCOUNT_H_ */
