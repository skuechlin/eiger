/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * MyVecArithm.h
 *
 *  Created on: Oct 10, 2014
 *      Author: kustepha
 */

#ifndef MYVECARITHM_H_
#define MYVECARITHM_H_

#include <Eigen/Dense>

namespace MyVecArithm
{

template <typename DerivedA, typename DerivedB>
inline
typename DerivedA::Scalar
distSq(const Eigen::MatrixBase<DerivedA>& p1, const Eigen::MatrixBase<DerivedB>& p2)
{
  return (p1-p2).squaredNorm();
}


inline
Eigen::Vector4d
aimRot(const Eigen::Vector4d& __restrict__ v, const Eigen::Vector4d& __restrict__ n)
{
  // rotate vector v to v' s.t. (v',n) == v(0)
  //
  // find vector half way between ex and n
  // then rotate 180 around this axis. do so using Rodrigues' formula
  // http://en.wikipedia.org/wiki/Rodrigues%27_rotation_formula

  const double n0 = n(0);

  if (n0 == 1.)
    return v;
  else if(n0 == -1. )
    return v.cwiseProduct( Eigen::Vector4d(-1.,-1.,1.,0.) ); // rotate 180 degrees about z
  else
    {
      Eigen::Vector4d k = n + Eigen::Vector4d::UnitX();
      const double d = k.dot(v) / k[0];
      return -v + d*k;
    }


//  // the rotation axis
//  // if n is anti-parallel to ex, choose ey as rotation axis
//  Eigen::Vector4d k = n + Eigen::Vector4d::UnitX();
//  k.noalias() += ((n.x() != -1.) ? 0. : 1.) * Eigen::Vector4d::UnitY();
//
//
//  // fac = 2 / (squared norm of rotation axis)
//  double fac = (n.x() != -1.) ? 1. / (1. + n.x()) : 2.;
//
//  // the rotation
//  Eigen::Vector4d v_rot(-v);
//
//  return v_rot - fac*k.dot(v_rot) * k;

}



} // end namespace MyVecArithm

#endif /* MYVECARITHM_H_ */
