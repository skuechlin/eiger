/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * Progress.h
 *
 *  Created on: Nov 29, 2016
 *      Author: kustepha
 */

#ifndef SRC_PRIMITIVES_PROGRESS_H_
#define SRC_PRIMITIVES_PROGRESS_H_

#include <omp.h>
#include <iostream>

inline void printProgress(const double progress)
{
  constexpr int barWidth = 70;

  static double last_progress = 0.;

#pragma omp critical(COUT)
  {
    if (progress < 0.)
      {
        last_progress = 0.;
      }
    else if (progress <= last_progress)
      {}
    else
      {
        if (progress > 1.0)
          {
            std::cout << std::endl;
          }
        else
          {

            last_progress = std::max(std::min(progress,1.0),0.0);

            std::cout << "[";
            int pos = barWidth * last_progress;
            for (int i = 0; i < barWidth; ++i) {
                if (i < pos) std::cout << "=";
                else if (i == pos) std::cout << ">";
                else std::cout << " ";
            }
            std::cout << "] " << int(last_progress * 100.0) << " %\r";
            std::cout.flush();
          }

      }

  }
}


#endif /* SRC_PRIMITIVES_PROGRESS_H_ */
