/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * MyMemory.h
 *
 *  Created on: Mar 11, 2016
 *      Author: kustepha
 */

#ifndef SRC_PRIMITIVES_MYMEMORY_H_
#define SRC_PRIMITIVES_MYMEMORY_H_

#include <memory>
#include <stdlib.h>
#include <cstring>
#include <cstddef>

namespace MyMemory
{


  template<typename T, size_t align = alignof(T)>
  __attribute__((assume_aligned(align),always_inline))
  inline constexpr
  T* aligned_alloc(std::size_t num_elements)
  {
    constexpr size_t align_bytes = std::max(align,alignof(std::max_align_t));
    const std::size_t size_bytes = sizeof(T)*num_elements;
    void* original = std::malloc(size_bytes+align_bytes);
    // note: original guaranteed to be aligned to at least alignof(max_align_t)
    if (!original) return nullptr;
    void* aligned = (void*)(
        ( (std::size_t)original & ~(std::size_t(align_bytes-1)) ) + align_bytes );
    // hide original pointer in unused bytes before aligned
    *((void**)aligned - 1) = original;  // note: sizeof(void*) == 8 <= alignof(max_align_t)
    return (T*)aligned;
  }

  inline constexpr void aligned_free(void *ptr) {
    if (ptr) std::free( *((void**)ptr - 1) ); // recover original pointer
  }

  template<typename T, size_t align = alignof(T)>
  __attribute__((assume_aligned(align),always_inline))
  inline constexpr
  T* aligned_realloc(void* ptr, std::size_t num_elements, std::size_t = 0)
  {
    constexpr size_t align_bytes = std::max(align,alignof(std::max_align_t));
    if (ptr == nullptr) return aligned_alloc<T,align_bytes>(num_elements);
    void *original = *((void**)ptr - 1);
    std::ptrdiff_t previous_offset = static_cast<char*>(ptr)-static_cast<char*>(original);
    const std::size_t size_bytes = sizeof(T)*num_elements;

    // same logic as aligned_malloc
    original = std::realloc(original,size_bytes+align_bytes);
    if (!original) return nullptr;
    void *aligned = (void*)(
        ( (std::size_t)original & ~(std::size_t(align_bytes-1)) ) + align_bytes );
    void *previous_aligned = static_cast<char*>(original)+previous_offset;
    if(aligned!=previous_aligned) // copy data if necessary
      std::memmove(aligned, previous_aligned, size_bytes);
    *((void**)aligned - 1) = original;
    return (T*)aligned;
  }

  struct aligned_deleter {
    void operator() (void* ptr){ free(ptr); }
  };

  template<size_t alignment>
  inline constexpr bool is_aligned(const void* const ptr) { return ( (size_t)ptr & (alignment-1) ) == 0; }


  template<class T, size_t align = alignof(T)>
  class aligned_allocator : public std::allocator<T>
  {
  public:
    typedef size_t          size_type;
    typedef std::ptrdiff_t  difference_type;
    typedef T*              pointer;
    typedef const T*        const_pointer;
    typedef T&              reference;
    typedef const T&        const_reference;
    typedef T               value_type;

    template<class U>
    struct rebind { typedef aligned_allocator<U> other;  };

    aligned_allocator() : std::allocator<T>() {}

    aligned_allocator(const aligned_allocator& other) : std::allocator<T>(other) {}

    template<class U>
    aligned_allocator(const aligned_allocator<U>& other) : std::allocator<T>(other) {}

    ~aligned_allocator() {}

    pointer allocate(size_type num, const void* /*hint*/ = 0) {
      return static_cast<pointer>( aligned_alloc<T,align>(num) );
    }

    void deallocate(pointer p, size_type /*num*/) { aligned_free(p); }
  };


  template<class T, typename std::enable_if<std::is_destructible<T>{},int>::type = 0 >
  void destroy(T* first, T* last) {std::destroy(first,last);}

  template<class T, typename std::enable_if<!std::is_destructible<T>{},int>::type = 0 >
  void destroy(T* /*first*/, T* /*last*/) {}

  template<typename T, size_t align = alignof(T)>
  struct aligned_array {

    T* p_ = nullptr;
    size_t cap_ = 0;
    size_t sz_ = 0;

    __attribute__((always_inline,assume_aligned(align)))
    T* data() { return p_;}

    __attribute__((always_inline,assume_aligned(align)))
    const T* data() const { return p_;}

    __attribute__((always_inline,assume_aligned(align)))
    T* begin() { return p_;}

    __attribute__((always_inline,assume_aligned(align)))
    const T* begin() const { return p_;}

    __attribute__((always_inline))
    T* end() { return p_ + sz_;}

    __attribute__((always_inline))
    const T* end() const { return p_ + sz_;}

    __attribute__((always_inline))
    T& back() { T* tmp = end(); return *(--tmp);  }

    __attribute__((always_inline))
    const T& back() const { const T* tmp = end(); return *(--tmp);  }

    void clear() { destroy(p_,p_+sz_); sz_ = 0; }

    void free() { clear(); aligned_free(p_); p_ = nullptr; cap_ = 0; }

    T& operator[] (const size_t i) { return p_[i]; }
    const T& operator[] (const size_t i) const { return p_[i]; }

    __attribute__((assume_aligned(align)))
    T* conservative_resize(const size_t Nnew) {
      if (Nnew > cap_) { p_ = aligned_realloc<T,align>(p_,Nnew,sz_); }
      if (p_ == nullptr) { cap_ = 0; sz_ = 0; }
      else { cap_ = std::max(Nnew,cap_); sz_ = Nnew; }
      return p_;
    }

    __attribute__((assume_aligned(align)))
    T* resize(const size_t Nnew) {
      if (Nnew > cap_) { free(); p_ = aligned_alloc<T,align>(Nnew); }
      if (p_ == nullptr) { cap_ = 0; sz_ = 0; }
      else { cap_ = std::max(Nnew,cap_); sz_ = Nnew; }
      return p_;
    }

    __attribute__((assume_aligned(align)))
    T* conservative_reserve(const size_t Nnew) {
      if (Nnew > cap_) { p_ = aligned_realloc<T,align>(p_,Nnew,sz_); }
      if (p_ == nullptr) { cap_ = 0; sz_ = 0; }
      else { cap_ = std::max(Nnew,cap_); }
      return p_;
    }

    __attribute__((assume_aligned(align)))
    T* reserve(const size_t Nnew) {
      if (Nnew > cap_) { free(); p_ = aligned_alloc<T,align>(Nnew); }
      if (p_ == nullptr) { cap_ = 0; sz_ = 0; }
      else { cap_ = std::max(Nnew,cap_); }
      return p_;
    }

    void swap(aligned_array<T,align>& other)
    {
      if (&other != this)
        {
          T* const p = p_;
          const size_t cap = cap_;
          const size_t sz = sz_;
          p_ = other.p_;
          cap_ = other.cap_;
          sz_ = other.sz_;
          other.p_ = p;
          other.cap_ = cap;
          other.sz_ = sz;
        }
    }

    void concatenate(const aligned_array<T,align>& other) {
      if (!other.size()) return;
      conservative_resize(size() + other.size());
      std::memcpy((void*)(data()+size()-other.size()),(void*)(other.data()),other.size()*sizeof(T));
    }

    template<typename... Args>
    void emplace_back(Args&&... args) {
      if (size() == capacity())
        conservative_reserve(2*size());
      resize(size()+1);
      new(&(back())) T(std::forward<Args>(args)...);
    }

    size_t size() const { return sz_; }
    size_t capacity() const { return cap_; }

    aligned_array() = default;
    aligned_array(const size_t size) { resize(size); }
    ~aligned_array() { free(); }


  };

}



#endif /* SRC_PRIMITIVES_MYMEMORY_H_ */
