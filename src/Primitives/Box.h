/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * Box.h
 *
 *  Created on: Jun 1, 2018
 *      Author: kustepha
 */

#ifndef SRC_PRIMITIVES_BOX_H_
#define SRC_PRIMITIVES_BOX_H_

#include "MyIntrin.h"

namespace Box {

  // xlow: *0 xhigh: *1
  // ylow: *2 yhigh: *3
  // zlow: *4 zhigh: *5
  //
  //       y               y *3                *3   y
  //      2 ____ 3         2 ___ 3          7 ______ 3
  //      /|   /|       *0  |   |  *1    *5  |      |  *4
  //     / |__/_|  x        |___|  x      x  |______|
  //    / 0/ /  /1         0     1          5        1
  //  6/__/_/7 /              *2                *2
  //   | /  | /
  //   |/___|/
  //  4      5
  // z



  template<typename T, uint8_t n>
  struct box_t {
    using VT = typename vector_type<T,n>::type;
    VT lower; VT upper;
    VT center() const {return .5*(lower+upper);}
    VT width() const {return upper-lower;}
    template<uint8_t nd = n>
    bool contains(const VT& v) const{ return MyIntrin::all<v4di,nd>(v4di{v>=lower && v<upper} );}
    box_t(const VT& l, const VT& u): lower(l), upper(u) {}
  };

  template<typename T, uint8_t n>
  box_t<T,n> merge(box_t<T,n> a /*by value*/, const box_t<T,n>& b) {
    a.lower = b.lower < a.lower ? b.lower : a.lower;
    a.upper = b.upper > a.upper ? b.upper : a.upper;
    return a;
  }

  template<typename T, uint8_t n>
  box_t<T,n> extend(box_t<T,n> b /*by value*/, const typename box_t<T,n>::VT& delta) {
    b.lower -= delta;
    b.upper += delta;
    return b;
  }

  template<uint8_t c, typename T, uint8_t n>
  constexpr
  typename box_t<T,n>::VT corner(const box_t<T,n>& b) {
    static_assert(c<(1<<n));
    return bits<n>(c) ? b.upper : b.lower;
  }

  typedef box_t<double,4> box4_t;

  template<uint8_t nd, typename BoxT>
  struct face_t {};

  template<typename BoxT> struct face_t<3,BoxT> {
    using VT = typename BoxT::VT;
    VT a = zero<VT>();
    VT b = zero<VT>();
    VT c = zero<VT>();
    face_t(){}
    explicit face_t(const VT& _a, const VT& _b, const VT& _c)
    : a(_a), b(_b), c(_c) {}
  };



  template<typename T>
   constexpr void face(face_t<3,box_t<T,4>>* const f, const box_t<T,4>& b, const uint8_t fn) {
     if (fn==0) {f->a = corner<0>(b); f->b = corner<4>(b); f->c = corner<2>(b); }
     if (fn==1) {f->a = corner<1>(b); f->b = corner<3>(b); f->c = corner<5>(b); }
     if (fn==2) {f->a = corner<0>(b); f->b = corner<1>(b); f->c = corner<4>(b); }
     if (fn==3) {f->a = corner<2>(b); f->b = corner<6>(b); f->c = corner<3>(b); }
     if (fn==4) {f->a = corner<0>(b); f->b = corner<2>(b); f->c = corner<1>(b); }
     if (fn==5) {f->a = corner<4>(b); f->b = corner<5>(b); f->c = corner<6>(b); }
   }



  template<typename T, uint8_t n>
  constexpr Eigen::AlignedBox<T,int(n)> to_eigen(const box_t<T,n>& b) {
    return {::to_eigen(b.lower),::to_eigen(b.upper)}; }

} // namespace Box

#endif /* SRC_PRIMITIVES_BOX_H_ */
