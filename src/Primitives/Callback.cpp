/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * Callback.cpp
 *
 *  Created on: Jul 1, 2016
 *      Author: kustepha
 */




#include <Primitives/Callback.h>
#include "Settings/Dictionary.h"

std::vector<uint64_t> get_ts_vec(const Settings::Dictionary& dict) {
  if (dict.hasMember("time steps"))
    {
      const Settings::Dictionary& tdict = dict.get<Settings::Dictionary>("time steps");
      return tdict.getAsVector<uint64_t>();
    }
  return {};
}

GenericCallback::GenericCallback(/*FunT&& fun,*/ const Settings::Dictionary& dict)
: GenericCallback( /*std::move(fun),*/
           dict.get<uint64_t>("first",0),
           dict.get<uint64_t>("last",uint64_t(-1)),
           dict.get<uint64_t>("interval",1),
           get_ts_vec(dict))
{}
