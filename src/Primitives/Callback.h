/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * Callback.h
 *
 *  Created on: Jul 1, 2016
 *      Author: kustepha
 */

#ifndef SRC_PRIMITIVES_CALLBACK_H_
#define SRC_PRIMITIVES_CALLBACK_H_

#include <cstdint>
#include <utility>
#include <vector>
#include <algorithm>

namespace MyMPI{ class MPIMgr; }
namespace Settings { class Dictionary; }
namespace MyChrono { class TimerCollection; }

//#include <functional>


class GenericCallback
{

  //  typedef std::function<
  //      void(const double t,
  //           const uint64_t tn,
  //           const MyMPI::MPIMgr& mpiMgr,
  //           MyChrono::TimerCollection& timers
  //      )> FunT;
  //  FunT fun_;


  const uint64_t tn_first_;       // first time step to trigger
  const uint64_t tn_last_;        // last time step to trigger
  const uint64_t tn_interval_;    // time step interval

  const std::vector<uint64_t> tns_; // explicit time step numbers

private:

  bool
  time2Call(const uint64_t tn)
  const
  {
    if (!(tns_.empty()))
      {
        if (std::find(tns_.begin(),tns_.end(),tn) != tns_.end())
          return true;
        else
          return false;
      }

    if ( (tn == uint64_t(-1)) && (tn_first_ == tn) )
      return true;

    if (
        (tn != uint64_t(-1)) &&
        tn >= tn_first_      &&
        tn <= tn_last_       &&
        (tn - tn_first_) % tn_interval_ == 0 )
      return true;

    return false;
  }

public:


  virtual void call(
      const double t,
      const uint64_t tn,
      const MyMPI::MPIMgr& mpiMgr,
      MyChrono::TimerCollection& timers)
  const = 0;

  void callIfTime(
      const double t,
      const uint64_t tn,
      const MyMPI::MPIMgr& mpiMgr,
      MyChrono::TimerCollection& timers)
  const
  {
    if (time2Call(tn))
      call(t,tn,mpiMgr,timers);
  }

  GenericCallback(
      const uint64_t tn_first,
      const uint64_t tn_last,
      const uint64_t tn_interval,
      const std::vector<uint64_t>& tns = {} )
  : tn_first_(tn_first)
  , tn_last_(tn_last)
  , tn_interval_(tn_interval > 0 ? tn_interval : 1)
  , tns_(tns)
  {}

  GenericCallback(const Settings::Dictionary& dict);
  virtual ~GenericCallback() {}



};

template<typename FunT>
class Callback : public GenericCallback {

  FunT fun;

public:

  void call(
      const double t,
      const uint64_t tn,
      const MyMPI::MPIMgr& mpiMgr,
      MyChrono::TimerCollection& timers)
  const override { fun(t,tn,mpiMgr,timers); }

  Callback(
      FunT&& _fun,
      const uint64_t tn_first,
      const uint64_t tn_last,
      const uint64_t tn_interval,
      const std::vector<uint64_t>& tns = {})
  : GenericCallback(tn_first,tn_last,tn_interval,tns)
  ,fun(_fun)
  {}

  Callback(
      FunT&& _fun,
      const Settings::Dictionary& dict)
  : GenericCallback(dict)
  ,fun(_fun)
  {}
};

template<typename FunT, typename... Args> Callback<FunT>
constructCallback(FunT&& fun,Args... args) {
  return Callback<FunT>(std::forward<FunT>(fun),
                        std::forward<Args>(args)...); }

template<typename FunT, typename... Args> Callback<FunT>*
newCallback(FunT&& fun,Args... args) {
  return new Callback<FunT>(std::forward<FunT>(fun),
                            std::forward<Args>(args)...); }


#endif /* SRC_PRIMITIVES_CALLBACK_H_ */
