/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * MyTransform.h
 *
 *  Created on: May 11, 2016
 *      Author: kustepha
 */

#ifndef SRC_PRIMITIVES_MYTRANSFORM_H_
#define SRC_PRIMITIVES_MYTRANSFORM_H_

#include <vector>
#include <list>
#include <cstdint>

#include "Primitives/MyError.h"

namespace MyTransform
{

  namespace Detail
  {

    //    template <class N>
    //    struct is_vector { static const int value = 0; };
    //
    //    template <class N, class A>
    //    struct is_vector<std::vector<N, A> > { static const int value = 1; };
    //
    //    template<typename T>
    //    auto reserve(T&& thing, const uint64_t size, int) -> decltype(T::reserve(size),void())
    //    {
    //      thing.reserve(size);
    //    }
    //
    //    template<typename T>
    //    auto reserve(T&&, const uint64_t, long) -> void
    //    {
    //      static_assert(!is_vector<T>::value,"sfinae failed");
    //    }


    template< typename T >
    struct IdenticalCopySplitter
    {
        T operator ()(const T& thing, const uint64_t, const uint64_t) const { return thing; }
    };

  }


  template<typename T>
  void permute_vector(std::vector<T>& vec,
                      const std::vector<uint64_t>& perm,
                      const uint64_t new_size)
  {
    std::vector<T> new_vec(new_size);
    const uint64_t perm_size = perm.size();
    for (uint64_t i = 0; i < perm_size; ++i)
      new_vec[i] = vec[ perm[i] ];
    vec.swap( new_vec );
  }

  template<typename T>
  void permute_vector_par(std::vector<T>& vec,
                          const std::vector<uint64_t>& perm,
                          const uint64_t new_size)
  {
    std::vector<T> new_vec(new_size);
    const uint64_t perm_size = perm.size();

#pragma omp parallel for
    for (uint64_t i = 0; i < perm_size; ++i)
      new_vec[i] = vec[ perm[i] ];

    vec.swap( new_vec );
  }

  //  template<
  //  typename Container,
  //  typename Int1T,
  //  typename Int2T,
  //  typename MergeFunctor,
  //  typename SplitFunctor = Detail::IdenticalCopySplitter<typename Container::value_type>>
  //  Container
  //  remove_merge_split(
  //      Container& elements,
  //      const std::vector<Int1T>& remove,
  //      const std::vector<Int1T>& merge,
  //      const std::vector<Int1T>& split,
  //      const Int2T block_size,
  //      MergeFunctor&& merger,
  //      SplitFunctor&& splitter = {} )
  //  {
  //    // remove:  vector containing indices of elements to be removed, sorted ascending
  //    // merge:   vector containing indices of elements that will be merged
  //    //          with the block_size-1 consecutive neighbors by applying merger, sorted ascending
  //    // split:   vector containing indices of elements that will be split
  //    //          into block_size consecutive elements, sorted ascending
  //
  //
  //
  //    // ************************** assert correct input
  //    uint64_t n_elements = elements.size();
  //
  //    MYASSERT(block_size > 0, "argument \"block_size\" must be > 0");
  //
  //    // check sorted
  //    MYASSERT(
  //        std::is_sorted(remove.begin(),remove.end()),
  //        "argument \"remove\" not sorted" );
  //
  //    MYASSERT(
  //        std::is_sorted(merge.begin(),merge.end()),
  //        "argument \"merge\" not sorted");
  //
  //    MYASSERT(
  //        std::is_sorted(split.begin(),split.end()),
  //        "argument \"split\" not sorted");
  //
  //    // check range
  //    MYASSERT( remove.size() < n_elements, "argument \"remove\" would cause <= 0 remaining elements");
  //
  //    if ( remove.size() != 0 )
  //      MYASSERT( remove.back() < n_elements,
  //                std::string("argument \"remove\" specifying out of range indices\n")
  //    + std::string("remove.back(): ") + std::to_string(remove.back()) +std::string("\n")
  //    + std::string("n_elements:    ") + std::to_string(n_elements));
  //
  //    if ( merge.size() != 0 )
  //      MYASSERT( merge.back() < n_elements-block_size+1, "argument \"merge\" specifying out of range indices");
  //
  //    if ( split.size() != 0 )
  //      MYASSERT( split.back() < n_elements, "argument \"split\" specifying out of range indices");
  //
  //    // check duplicates
  //    for (uint64_t i = 1; i < remove.size(); ++i)
  //      MYASSERT( remove[i-1] != remove[i], "argument \"remove\" contains duplicate entries");
  //
  //    for (uint64_t i = 1; i < merge.size(); ++i)
  //      MYASSERT( merge[i-1] != merge[i], "argument \"merge\" contains duplicate entries");
  //
  //    for (uint64_t i = 1; i < split.size(); ++i)
  //      MYASSERT( split[i-1] != split[i], "argument \"split\" contains duplicate entries");
  //
  //
  //    // ************************** process elements by inserting into return container
  //    Container ret;
  //
  //    try{
  //        //    ret.reserve( n_elements - remove.size() - merge.size()*(block_size-1) + split.size()*(block_size-1) );
  //        Detail::reserve(ret,n_elements - remove.size() - merge.size()*(block_size-1) + split.size()*(block_size-1),0);
  //    } catch (const std::exception& e)
  //    {
  //        std::cout << "exception while reserving memory for transform operation: " << e.what() << std::endl;
  //        throw;         // re-throw
  //    }
  //
  //    auto push_back = [&ret](auto&& elem)->void{
  //      try{
  //          ret.push_back(std::move(elem));
  //      } catch (const std::exception& e)
  //      {
  //          std::cout << "exception while pushing element to result container during transform operation: " << e.what() << std::endl;
  //          throw;         // re-throw
  //      }
  //    };
  //
  //    auto elem = elements.begin();
  //
  //    uint64_t i = 0;
  //    uint64_t j = 0;
  //    uint_fast8_t action = 0;
  //
  //    auto cremove = remove.begin();
  //    auto cmerge = merge.begin();
  //    auto csplit = split.begin();
  //
  //    while (i < n_elements)
  //      {
  //
  //        // lookup current index in remove, merge and split
  //        while (cremove != remove.end() && *cremove < i)
  //          ++cremove;
  //        while (cmerge != merge.end() && *cmerge < i)
  //          ++cmerge;
  //        while (csplit != split.end() && *csplit < i)
  //          ++csplit;
  //
  //
  //        if (cremove != remove.end() && i == *cremove)
  //          action |= 1;
  //        if (cmerge != merge.end() && i == *cmerge)
  //          action |= 2;
  //        if (csplit != split.end() && i == *csplit)
  //          action |= 4;
  //
  //        switch (action) {
  //
  //          case 0:
  //            // no action -> move element to ret
  //            push_back( std::move( *elem++ ) ); ++i;//elements[i++] );
  //            break;
  //
  //          case 1:
  //            // element to be removed -> do not copy to ret
  //            ++i; ++elem;
  //            action = 0; // done with current action
  //            break;
  //
  //          case 2:
  //            // merge element with n-1 neighbors
  //            if (j == 0)
  //              { // first element is copied
  //                push_back( std::move( *elem++ ) ); ++i;//elements[i++] );
  //              }
  //            else
  //              { // consecutive n-1 elements are merged
  //                merger(ret.back(), *elem++); ++i;// elements[i++]);
  //              }
  //
  //            ++j;
  //
  //            if (j == block_size) // finished processing current action
  //              {
  //                action = 0;
  //                j = 0;
  //              }
  //            break;
  //
  //          case 4:
  //            // split element by applying splitter n times
  //            push_back( std::move( splitter( *elem,//elements[i]
  //                                            i, j ) ) );
  //            ++j;
  //            if (j == block_size) // finished processing current action
  //              {
  //                ++i;
  //                ++elem;
  //                action = 0;
  //                j = 0;
  //              }
  //            break;
  //
  //          default:
  //            MYASSERT(action != 3, std::string("cannot both remove and merge element at index ") + std::to_string(i));
  //            MYASSERT(action != 5, std::string("cannot both remove and split element at index ") + std::to_string(i));
  //            MYASSERT(action != 6, std::string("cannot both merge and split element at index ") + std::to_string(i));
  //            MYASSERT(false, std::string("unknown action code ") + std::to_string(action));
  //            break;
  //        }
  //
  //      }
  //
  //    // ************************** return result
  //    return ret;
  //
  //  } // remove_merge_split


  template<
  typename T,
  typename Int1T,
  typename Int2T,
  typename MergeFunctor,
  typename SplitFunctor = Detail::IdenticalCopySplitter<T>,
  typename AllocT = std::allocator<T> >
  void
  remove_merge_split(
      std::vector<T,AllocT >& elements,
      const std::vector<Int1T>& remove,
      const std::vector<Int1T>& merge,
      const std::vector<Int1T>& split,
      const Int2T block_size,
      MergeFunctor&& merger,
      SplitFunctor&& splitter = {} )
  {
    // remove:  vector containing indices of elements to be removed, sorted ascending
    // merge:   vector containing indices of elements that will be merged
    //          with the block_size-1 consecutive neighbors by applying merger, sorted ascending
    // split:   vector containing indices of elements that will be split
    //          into block_size consecutive elements, sorted ascending



    // ************************** assert correct input
    uint64_t n_elements = elements.size();

    MYASSERT(block_size > 0, "argument \"block_size\" must be > 0");

    // check sorted
    MYASSERT(
        std::is_sorted(remove.begin(),remove.end()),
        "argument \"remove\" not sorted" );

    MYASSERT(
        std::is_sorted(merge.begin(),merge.end()),
        "argument \"merge\" not sorted");

    MYASSERT(
        std::is_sorted(split.begin(),split.end()),
        "argument \"split\" not sorted");

    // check range
    MYASSERT( remove.size() <= n_elements, "argument \"remove\" would cause < 0 remaining elements");

    if ( remove.size() != 0 )
      MYASSERT( remove.back() < n_elements,
                std::string("argument \"remove\" specifying out of range indices\n")
    + std::string("remove.back(): ") + std::to_string(remove.back()) +std::string("\n")
    + std::string("n_elements:    ") + std::to_string(n_elements));

    if ( merge.size() != 0 )
      MYASSERT( merge.back() < n_elements-block_size+1, "argument \"merge\" specifying out of range indices");

    if ( split.size() != 0 )
      MYASSERT( split.back() < n_elements, "argument \"split\" specifying out of range indices");

    // check duplicates
    for (uint64_t i = 1; i < remove.size(); ++i)
      MYASSERT( remove[i-1] != remove[i], "argument \"remove\" contains duplicate entries");

    for (uint64_t i = 1; i < merge.size(); ++i)
      MYASSERT( merge[i-1] != merge[i], "argument \"merge\" contains duplicate entries");

    for (uint64_t i = 1; i < split.size(); ++i)
      MYASSERT( split[i-1] != split[i], "argument \"split\" contains duplicate entries");


    // ************************** process elements in reverse order

    try{
        elements.resize(
            std::max(n_elements,
                     n_elements + split.size()*(block_size-1)
            ));
    } catch (const std::exception& e)
    {
        std::cout << "exception while reserving memory for transform operation: " << e.what() << std::endl;
        throw;         // re-throw
    }

    uint64_t n_extra_elems = elements.size() - n_elements;

    auto elem = std::next(elements.rbegin(),n_extra_elems); // iterator to end of original range

    auto insert_pos = elements.rbegin();

    uint64_t i = n_elements-1;

    auto cremove = remove.rbegin();
    auto cmerge = merge.rbegin();
    auto csplit = split.rbegin();

    while (i != uint64_t(-1))
      {

        uint8_t action = 0;

        // lookup current index in remove, merge and split
        while (cremove != remove.rend() && *cremove > i)
          ++cremove;
        while (cmerge != merge.rend() && *cmerge > i)
          ++cmerge;
        while (csplit != split.rend() && *csplit > i)
          ++csplit;


        if (cremove != remove.rend() && i == *cremove)
          action |= 1;
        if (cmerge != merge.rend() && i == *cmerge)
          action |= 2;
        if (csplit != split.rend() && i == *csplit)
          action |= 4;

        switch (action) {

          case 0:
            // no action -> copy element
            *insert_pos++ = *elem++; --i;
            break;

          case 1:
            // element to be removed -> do not copy to ret
            --i; ++elem;
            break;

          case 2:
            // merge element with n-1 neighbors, which have already been copied
            insert_pos -= (block_size-1);
            std::swap(*insert_pos,*elem);

            if (block_size > 2)
              {
                for (Int2T j = block_size-2; j > 0; --j)
                  {
                    merger(*insert_pos,*(insert_pos + j));
                  }
              }
            merger(*insert_pos,*elem);
            ++elem; --i; ++insert_pos;

            break;

          case 4:
            // split element by applying splitter n times
            for (Int2T j = block_size-1; j != Int2T(-1); --j)
              {
                *insert_pos++ = splitter( *elem, i, j );
              }
            ++elem;
            --i;
            break;

          default:
            MYASSERT(action != 3, std::string("cannot both remove and merge element at index ") + std::to_string(i));
            MYASSERT(action != 5, std::string("cannot both remove and split element at index ") + std::to_string(i));
            MYASSERT(action != 6, std::string("cannot both merge and split element at index ") + std::to_string(i));
            MYASSERT(false, std::string("unknown action code ") + std::to_string(action));
            break;
        }

      } // while

    // if elements were removed, copy result range to beginning and resize
    if (insert_pos != elements.rend())
      {
        const uint64_t nnew = std::distance(elements.rbegin(),insert_pos);
        MYASSERT(nnew == n_elements + split.size()*(block_size-1) - remove.size() - merge.size()*(block_size-1),
                 "size mismatch");
        MYASSERT(elements.size() > nnew,"should not be here");
        const uint64_t offset = elements.size() - nnew;
        for (uint64_t j = 0; j < nnew; ++j )
          elements[j] = elements[j+offset];
        elements.resize( nnew );
      }


  } // remove_merge_split

  template<
  typename T,
  typename Int1T,
  typename Int2T,
  typename MergeFunctor,
  typename SplitFunctor = Detail::IdenticalCopySplitter<T>,
  typename AllocT = std::allocator<T> >
  void
  remove_merge_split(
      std::list<T,AllocT>& elements,
      const std::vector<Int1T>& remove,
      const std::vector<Int1T>& merge,
      const std::vector<Int1T>& split,
      const Int2T block_size,
      MergeFunctor&& merger,
      SplitFunctor&& splitter = {} )
  {
    // remove:  vector containing indices of elements to be removed, sorted ascending
    // merge:   vector containing indices of elements that will be merged
    //          with the block_size-1 consecutive neighbors by applying merger, sorted ascending
    // split:   vector containing indices of elements that will be split
    //          into block_size consecutive elements, sorted ascending



    // ************************** assert correct input
    uint64_t n_elements = elements.size();

    MYASSERT(block_size > 0, "argument \"block_size\" must be > 0");

    // check sorted
    MYASSERT(
        std::is_sorted(remove.begin(),remove.end()),
        "argument \"remove\" not sorted" );

    MYASSERT(
        std::is_sorted(merge.begin(),merge.end()),
        "argument \"merge\" not sorted");

    MYASSERT(
        std::is_sorted(split.begin(),split.end()),
        "argument \"split\" not sorted");

    // check range
    MYASSERT( remove.size() <= n_elements, "argument \"remove\" would cause < 0 remaining elements");

    if ( remove.size() != 0 )
      MYASSERT( remove.back() < n_elements,
                std::string("argument \"remove\" specifying out of range indices\n")
    + std::string("remove.back(): ") + std::to_string(remove.back()) +std::string("\n")
    + std::string("n_elements:    ") + std::to_string(n_elements));

    if ( merge.size() != 0 )
      MYASSERT( merge.back() < n_elements-block_size+1, "argument \"merge\" specifying out of range indices");

    if ( split.size() != 0 )
      MYASSERT( split.back() < n_elements, "argument \"split\" specifying out of range indices");

    // check duplicates
    for (uint64_t i = 1; i < remove.size(); ++i)
      MYASSERT( remove[i-1] != remove[i], "argument \"remove\" contains duplicate entries");

    for (uint64_t i = 1; i < merge.size(); ++i)
      MYASSERT( merge[i-1] != merge[i], "argument \"merge\" contains duplicate entries");

    for (uint64_t i = 1; i < split.size(); ++i)
      MYASSERT( split[i-1] != split[i], "argument \"split\" contains duplicate entries");


    // ************************** process elements

    auto elem = elements.begin();
    decltype(elem) merge_pos;

    uint64_t i = 0; // index in unchanged list

    auto cremove = remove.begin();
    auto cmerge = merge.begin();
    auto csplit = split.begin();

    while (i < n_elements)
      {

        // lookup current index in remove, merge and split
        while (cremove != remove.end() && *cremove < i)
          ++cremove;
        while (cmerge != merge.end() && *cmerge < i)
          ++cmerge;
        while (csplit != split.end() && *csplit < i)
          ++csplit;

        uint8_t action = 0;
        if (cremove != remove.end() && i == *cremove)
          action |= 1;
        if (cmerge != merge.end() && i == *cmerge)
          action |= 2;
        if (csplit != split.end() && i == *csplit)
          action |= 4;

        switch (action) {

          case 0:
            // no action
            ++elem; ++i;
            break;

          case 1:
            // element to be removed
            elem = elements.erase(elem);
            ++i;
            action = 0; // done with current action
            break;

          case 2:
            // merge element with n-1 neighbors
            // first element is unchanged
            merge_pos = elem++; ++i;
            // consecutive n-1 elements are merged
            merger(*merge_pos, elem, block_size-1);

            for (Int2T j = 1; j < block_size; ++j)
              {
                elem = elements.erase(elem); // erase merged element, returns iterator to next element
                ++i;
              }

            break;

          case 4:
            // split element by applying splitter n times
            for (Int2T j = 0; j < block_size; ++j)
              {
                try{
                    elements.insert( elem, std::move(splitter( *elem, i, j )) ); // insert before elem
                } catch (const std::exception& e)
                {
                    std::cout << "exception while splitting element during transform operation: " << e.what() << std::endl;
                    throw;         // re-throw
                }
              }
            elem = elements.erase(elem); // erase parent item, returns iterator to next item
            ++i;
            break;

          default:
            MYASSERT(action != 3, std::string("cannot both remove and merge element at index ") + std::to_string(i));
            MYASSERT(action != 5, std::string("cannot both remove and split element at index ") + std::to_string(i));
            MYASSERT(action != 6, std::string("cannot both merge and split element at index ") + std::to_string(i));
            MYASSERT(false, std::string("unknown action code ") + std::to_string(action));
            break;
        } // switch action

      } // while i < nelements

  } // remove_merge_split

} // namespace MyTransform



#endif /* SRC_PRIMITIVES_MYTRANSFORM_H_ */
