/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * main.cpp
 *
 *  Created on: Apr 10, 2014
 *      Author: Stephan Kuechlin
 */

#define QUOTE(str) #str
#define EXPAND_AND_QUOTE(str) QUOTE(str)

#include <fenv.h>
#include <mpi.h>
#include <iostream>
#include <xmmintrin.h>
#include <pmmintrin.h>

#include "Settings/Settings.h"

#include "Parallel/MyThreads.h"

#include "Simulation/Simulation.h"



int main(int argc, char* argv[]) {


  _MM_SET_FLUSH_ZERO_MODE(_MM_FLUSH_ZERO_ON);
  _MM_SET_DENORMALS_ZERO_MODE(_MM_DENORMALS_ZERO_ON);

//  feenableexcept(FE_DIVBYZERO | FE_INVALID | FE_OVERFLOW);


  // enable nested parallelism
  omp_set_nested(1);

  // determine and set thread team size
  MyThreads::setNumThreads();


  // startup MPI
  // If the process is multi-threaded, only the thread that called
  // MPI_Init_thread will make MPI calls.
  int requested = MPI_THREAD_MULTIPLE; // MPI_THREAD_FUNNELED
  int provided;
  MPI_Init_thread(&argc, &argv, requested, &provided);



//  MYASSERT(
//      provided >= MPI_THREAD_FUNNELED,
//      "MPI init failed to provide MPI_THREAD_FUNNELED support");


  // determine if root proc
  int rank;
  int root = 0;
  MPI_Comm_rank(MPI_COMM_WORLD,&rank);
  bool isRoot = (rank == root);

  // print information
  int mpi_versionstrlen;
  char* mpi_versionstr = (char*)calloc(MPI_MAX_LIBRARY_VERSION_STRING,1);
  MPI_Get_library_version(mpi_versionstr,&mpi_versionstrlen);


  if (isRoot)
    {
      std::cout << "**** This is EIGER ****\n" <<
          "    version id: " << EXPAND_AND_QUOTE(VERSION_ID) << "\n" <<
          "    build date: " << __DATE__ << " " << __TIME__ << "\n" <<
          "    mpi distro: " << mpi_versionstr << "\n";
      if (provided < requested)
        std::cout << "    ***WARNING*** requested thread support level " << requested << ", got " << provided << "!\n";
      std::cout << std::endl;
    }

  free(mpi_versionstr);


  /** Must be called first when calling Eigen from multiple threads */
  Eigen::initParallel();


  // parse options and read settings
  Settings::Settings s(argc,argv,isRoot);

  // write settings to output directory
  s.writeToOutput();

  // set up the simulation
  Simulation sim(s);


#ifdef EIGEN_RUNTIME_NO_MALLOC
  Eigen::internal::set_is_malloc_allowed(false);
  // It's NOT OK to allocate here
  // An assertion will be triggered if an Eigen-related heap allocation takes place
  if (isRoot)
    {
      std::cout << "eigen runtime heap allocation check enabled!" << std::endl << std::endl;
    }
#endif

  // run Forrest, run!
  sim.run();


  // finalize mpi
  MPI_Finalize();


  // exit
  return 0;

}
