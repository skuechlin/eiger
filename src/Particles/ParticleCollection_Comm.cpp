/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * ParticleCollection_CommAndSort.cpp
 *
 *  Created on: Mar 28, 2019
 *      Author: kustepha
 */


#include "ParticleCollection.h"

#include "Grid/Grid.h"

#include "Parallel/MyMPI_DSDEComm2.h"
#include "Parallel/MyMPI_AlltoallComm.h"

#include "Primitives/CRC32.h"

namespace Particles
{

  // exchange particles
  void ParticleCollection::communicate_prts(
      Grids::Grid* const nodeGrid,
      const MyMPI::MPIMgr& mpiMgr,
      const uint64_t tag)
  {

    // mpi type
    //    MPI_Datatype particle_t = particleMPIType();
    MPI_Datatype particle_t;
    MPI_Type_contiguous(sizeof(Particle)/sizeof(double), MPI_DOUBLE, &particle_t);
    MPI_Type_commit(&particle_t);

    // put particles to send in data_old_
    swapOldCurrent();

    //  make sure to pass the correct send displacements

    /*
    MyMPI::AlltoallCommSpec commspec(
        nodeGrid->count_.data(),
        mpiMgr.comm() );
    */

    MyMPI::DSDECommSpec2 commspec(
        nodeGrid->count_.data(),nodeGrid->begin_.data(),mpiMgr.comm(),
        COMPILE_TIME_CRC32_STR( FILE_POS_STR ) + tag );


    const uint64_t n_prts_tot = commspec.n_to_receive();

    // new size of current data should be size of particles received
    resize(n_prts_tot, ResizeMode::CURRENT );

    // exchange particle data

    /*
    commspec.comm(
        data_old_.data(), // send buffer
        data_.data(),     // receive buffer
        sizeof(Particle),
        particle_t,
        COMPILE_TIME_CRC32_STR( FILE_POS_STR ) + tag );


     */

    const int n_out = commspec.outdegree();
    const int n_in = commspec.indegree();

    std::vector<MPI_Request> sendreqs(n_out);
    std::vector<MPI_Request> recvreqs(n_in);

    // exchange particle data
    commspec.Icomm(
        sendreqs.data(),
        recvreqs.data(),
        data_old_.data(), // send buffer
        data_.data(),     // receive buffer
        sizeof(Particle),
        particle_t,
        COMPILE_TIME_CRC32_STR( FILE_POS_STR ) + tag );

    // wait for comm in background
#pragma omp taskgroup
    {


#pragma omp taskloop nogroup
      for(int i = 0; i < n_out; ++i)
        MPI_Wait(&(sendreqs[i]),MPI_STATUS_IGNORE);

#pragma omp taskloop nogroup
      for(int i = 0; i < n_in; ++i)
        MPI_Wait(&(recvreqs[i]),MPI_STATUS_IGNORE);

    }// taskgroup




    MPI_Type_free(&particle_t);

    resize( size(), ResizeMode::OLD);



  }



} // namespace Particles

