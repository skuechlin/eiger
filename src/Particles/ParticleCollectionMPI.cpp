/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * ParticleCollectionMPI.cpp
 *
 *  Created on: Oct 27, 2014
 *      Author: kustepha
 */

#include <mpi.h>

#include "Grid/Grid.h"

#include "Parallel/MyMPI.h"

#include "Parallel/MyMPI_CommSpec.h"
#include "Parallel/MyMPI_NeighborComm.h"
#include "Parallel/MyMPI_DSDEComm2.h"

#include "Primitives/CRC32.h"

#include "Primitives/Timeout.h"

#include "ParticleCollection.h"




namespace Particles
{

  struct ParticleMPITypeHolder
  {
    MPI_Datatype particle_t;
    ParticleMPITypeHolder() {
      MPI_Type_contiguous(sizeof(Particle), MPI_BYTE, &particle_t);
      MPI_Type_commit(&particle_t);  }
    // ~ParticleMPITypeHolder() { MPI_Type_free(&particle_t); }
    // nice idea, but will be called after MPI_finalize, which is illegal...
  };

  MPI_Datatype particleMPIType()
  {
    static const ParticleMPITypeHolder type_holder;
    return type_holder.particle_t;
  }


  // return maximum velocity
  double
  ParticleCollection::maxSpeed(const MyMPI::MPIMgr& mpiMgr)
  const
  {
    double maxVel2Local = 0;
    double maxVel2Global = 0;

    if (end_ > 0)
      {
#pragma omp parallel for schedule(static) reduction(max:maxVel2Local)
        for (uint64_t i = 0; i < end_; ++i)
          {
            const v4df C = getParticle(i)->C;
            maxVel2Local = fmax(
                maxVel2Local,  double(C[0]*C[0] + C[1]*C[1] + C[2]*C[2] + C[3]*C[3]) );
          }
      }

    MPI_Request req;
    MPI_Iallreduce(&maxVel2Local,&maxVel2Global,1,MPI_DOUBLE,MPI_MAX,mpiMgr.comm(),&req);

    MyMPI::Wait(&req,MPI_STATUS_IGNORE,60.,"maxSpeed all-reduce");

    return sqrt(maxVel2Global);
  }


  // exchange particles
  void ParticleCollection::communicate(
      Grids::Grid* const nodeGrid,
      const MyMPI::MPIMgr& mpiMgr,
      const uint64_t tag) {

    if (mpiMgr.nRanks() <= 1) {
        global_size_ = size();
        return; // nothing to do
    }

    const uint64_t ls = size();
    static uint64_t gs;

#pragma omp task
    {
      gs = 0;
      MPI_Reduce(&ls,&gs,1,MPI_UINT64_T,MPI_SUM,mpiMgr.root(),mpiMgr.comm());
    }

    //    communicate_prts(nodeGrid,mpiMgr,tag);

    communicate_and_merge3(nodeGrid,mpiMgr,tag);
    //    communicate_and_merge4(nodeGrid,mpiMgr,tag);

#pragma omp taskwait

    global_size_ = gs;

  }


}
