/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * ParticleCollection_CommAndMerge2.cpp
 *
 *  Created on: Feb 15, 2018
 *      Author: kustepha
 */

#ifdef __STRICT_ANSI__
#define STRICT_ANSI_STATE
#undef __STRICT_ANSI__
#endif

#include <parallel/algorithm> // copy_backward, stable_sort, unique

#ifdef STRICT_ANSI_STATE
#define __STRICT_ANSI__ STRICT_ANSI_STATE
#endif

#include "ParticleCollection.h"

#include "Grid/Grid.h"

#include "Parallel/MyMPI_DSDEComm2.h"

#include "Primitives/CRC32.h"

namespace Particles
{
  // exchange particles
  void ParticleCollection::communicate_and_merge3(
      Grids::Grid* const nodeGrid,
      const MyMPI::MPIMgr& mpiMgr,
      const uint64_t tag)
  {
    // mpi type
    MPI_Datatype particle_t = particleMPIType();

    // put particles to send in data_old_
    swapOldCurrent();
    swapOldCurrentSortData();

    // size of message to self
    const uint64_t this_rank_count = nodeGrid->count_[mpiMgr.rank()];
    // begin of message to self
    const uint64_t this_rank_displ = nodeGrid->begin_[mpiMgr.rank()];
    // zero count of this rank
    nodeGrid->count_[mpiMgr.rank()] = 0;

    //  make sure to pass the correct send displacements
    MyMPI::DSDECommSpec2 commspec(
        nodeGrid->count_.data(),nodeGrid->begin_.data(),mpiMgr.comm(),
        COMPILE_TIME_CRC32_STR( FILE_POS_STR ) + tag );

    // reset count on nodeGrid
    nodeGrid->count_[mpiMgr.rank()] = this_rank_count;

    const uint64_t n_prts_tot = this_rank_count + commspec.n_to_receive();

    // new size of current data should be size of particles from this rank plus those received from others
    resize(n_prts_tot,
           resizeflag(ResizeMode::CURRENT) |
           resizeflag(ResizeMode::OLD_SORT) |
           resizeflag(ResizeMode::KEEP_OLD_KEYS) );

    const int n_out = commspec.outdegree();
    const int n_in = commspec.indegree();

    std::vector<MPI_Request> sendreqs(n_out);
    std::vector<MPI_Request> recvreqs(n_in);

    // let receive-buffer begin AFTER this_rank_count particles
    commspec.Icomm(
        sendreqs.data(),
        recvreqs.data(),
        data_old_.data(), // send buffer
        (Particle*)(data_.data()) + this_rank_count, // receive buffer
        sizeof(Particle),
        particle_t,
        COMPILE_TIME_CRC32_STR( FILE_POS_STR ) + tag );

#pragma omp taskgroup
    {


      // background task copy particles
#pragma omp task
      {
        char* const       dst = (char*)(data_.data());
        const char* const src = (char*)(data_old_.data()) + this_rank_displ*sizeof(Particle);
        const uint64_t    sz  = this_rank_count*sizeof(Particle);

        std::memcpy( dst,  src,  sz );
      }

      // background task copy keys
#pragma omp task
      {
        char* const dst = (char*)(key_.data());
        const char* const src = (char*)(key_old_.data()) + this_rank_displ*sizeof(uint64_t);
        const uint64_t sz = this_rank_count*sizeof(uint64_t);

        std::memcpy( dst,  src,  sz  );
      }

      // background task compute source pointers
#pragma omp task
      {
        // if (n_in > 0, will later merge source data to src_
        Particle** src = (n_in > 0) ? src_old_.data() : src_.data();
        for (uint64_t j = 0; j < n_prts_tot; ++j)
          src[j] = getParticle(j);
      }

      // wait sends
      if (n_out)
        {
#pragma omp task untied
          {
            while (true)
              {
                int sends_complete;
                MPI_Testall(n_out,sendreqs.data(),&sends_complete,MPI_STATUSES_IGNORE);
                if (sends_complete) break;
#pragma omp taskyield
              }

          }
        }

      // receive and compute keys
      if (n_in)
        {
          while (true)
            {
              std::vector<int> completed_recvs(n_in,0);
              int n_completed;
              MPI_Waitsome(n_in,recvreqs.data(),&n_completed,completed_recvs.data(),MPI_STATUSES_IGNORE);

              if (n_completed != MPI_UNDEFINED)
                {
#pragma omp task firstprivate(completed_recvs,n_completed)
                  {
                    for (int j = 0; j < n_completed; ++j)
                      {
                        const int i = completed_recvs[j];
                        const uint64_t first_recvd = this_rank_count + commspec.neighbor_recvdispls_[i];
                        const uint64_t last_recvd  = first_recvd + commspec.neighbor_recvcounts_[i];

                        nodeGrid->topology()->computeKeys(*this,first_recvd,last_recvd);
                      }
                  }
                }
              else
                break;

#pragma omp taskyield
            }
        }


    } // taskgroup (wait for child tasks and their descendant tasks)


    if (n_in)
      {

        // all relevant particle data are now in data_, keys are computed, src ptrs set

        // sort the src_old_ ptrs by the keys using parallel multiway merge to src_

        std::vector<std::pair<Particle**,Particle**>> runs;
        runs.reserve(n_in+1);

        // the particles that remained on this rank
        runs.emplace_back(src_old_.data(), src_old_.data() + this_rank_count);

        // the received
        for (int i = 0; i < n_in; ++i)
          runs.emplace_back(
              src_old_.data() + this_rank_count + commspec.neighbor_recvdispls_[i],
              src_old_.data() + this_rank_count + commspec.neighbor_recvdispls_[i] +
              commspec.neighbor_recvcounts_[i]
          );

        const auto srccomp = [this](const Particle* const a, const Particle* const b){
          return getKey(a) < getKey(b);
        };

        __gnu_parallel::multiway_merge(
            runs.begin(),runs.end(),src_.data(),size(),srccomp,
            __gnu_parallel::parallel_tag(std::max(omp_get_num_threads() - 1,1))
           /* __gnu_parallel::exact_tag(std::max(omp_get_num_threads() - 1,1)) */
        );

        // sort data now in current

      } // if indegree()


    resize(size(),ResizeMode::OLD_DATA);
  }

}
