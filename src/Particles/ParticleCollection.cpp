/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * ParticleCollection.cpp
 *
 *  Created on: Apr 25, 2014
 *      Author: Stephan Kuechlin
 */


#include "ParticleCollection.h"

#include "Primitives/MyError.h"
#include "Primitives/MyRandom.h"
#include "Primitives/MyArithm.h"
#include "Primitives/MyVecArithm.h"
#include "Primitives/Timer.h"


using namespace Eigen;

namespace Particles {


  // reserve storage for particle data
  void ParticleCollection::reserve(const uint64_t num) {
    data_.conservative_reserve(num);
    data_old_.conservative_reserve(num);
    key_.conservative_reserve(num);
    key_old_.conservative_reserve(num);
    src_.conservative_reserve(num);
    src_old_.conservative_reserve(num);
  }

  // resizes data containers according to ResizeMode
  void ParticleCollection::resize(const uint64_t num, const uint8_t mode)
  {

    constexpr uint64_t fac = 4;

    if ( mode & resizeflag(ResizeMode::CURRENT_DATA) )
      {
        if ( num > static_cast<uint64_t>(data_.size()) )
          {
            const uint64_t new_size = MyArithm::roundUp( (num + num/fac), 4096/sizeof(Particle) );

            if ( mode & resizeflag(ResizeMode::KEEP_CURRENT_DATA) )
              {
                data_.conservative_resize(new_size);
                MYASSERT(data_.data() != nullptr, "data_.conservative_resize() failed, probably out of memory");
              }
            else
              {
                data_.resize(new_size);
                MYASSERT(data_.data() != nullptr, "data_.resize() failed, probably out of memory");
              }
          } // resize data

        // CURRENT DATA implies setting size
        end_ = num;

      } // resize mode CURRENT_DATA

    if ( mode & resizeflag(ResizeMode::CURRENT_SORT) )
      {

        if ( num > static_cast<uint64_t>(src_.size()) )
          {
            const uint64_t new_size =  MyArithm::roundUp( (num + num/fac), 4096/sizeof(uint64_t));

            if ( mode & resizeflag(ResizeMode::KEEP_CURRENT_KEYS) )
              {
                key_.conservative_resize(new_size);
                MYASSERT(key_.data() != nullptr, "key_.conservative_resize() failed, probably out of memory");                            }
            else
              {
                key_.resize(new_size);
                MYASSERT(key_.data() != nullptr, "key_.resize() failed, probably out of memory");
              }
            src_.resize(new_size);
            MYASSERT(src_.data() != nullptr, "src_.resize() failed, probably out of memory");

          } // resize keys
      } // resize mode CURRENT_SORT

    if ( mode & resizeflag(ResizeMode::OLD_DATA) )
      {

        if ( num > static_cast<uint64_t>( data_old_.size() ) )
          {
            const uint64_t new_size = MyArithm::roundUp( (num + num/fac), 4096/sizeof(Particle) );

            if ( mode & resizeflag(ResizeMode::KEEP_OLD_DATA) )
              {
                data_old_.conservative_resize(new_size);
                MYASSERT(data_old_.data() != nullptr, "data_old_.conservative_resize() failed, probably out of memory");
              }
            else
              {
                data_old_.resize(new_size);
                MYASSERT(data_old_.data() != nullptr, "data_old_.resize() failed, probably out of memory");
              }
          }// resize old data
      } // resize mode OLD_DATA

    if ( mode & resizeflag(ResizeMode::OLD_SORT) )
      {
        if ( num > static_cast<uint64_t>(src_old_.size()) )
          {
            const uint64_t new_size =  MyArithm::roundUp( (num + num/fac), 4096/sizeof(uint64_t));

            if ( mode & resizeflag(ResizeMode::KEEP_OLD_KEYS) )
              {
                key_old_.conservative_resize(new_size);
                MYASSERT(key_old_.data() != nullptr, "key_old_.conservative_resize() failed, probably out of memory");                            }
            else
              {
                key_old_.resize(new_size);
                MYASSERT(key_old_.data() != nullptr, "key_old_.resize() failed, probably out of memory");
              }
            src_old_.resize(new_size);
            MYASSERT(src_old_.data() != nullptr, "src_old_.resize() failed, probably out of memory");
          } // resize old keys

      }// resize mode OLD_SORT
  }

  uint8_t ParticleCollection::sort(MyChrono::TimerCollection& timers)  {

    // the number of particles to keep after sorting
    const uint64_t n_to_keep = size()-nToRemove();

    if (n_to_keep > 0)
      {
        MYASSERT(max_key_ >= min_key_,"min_k_ (" + std::to_string(min_key_) + ") > max_k_(" + std::to_string(max_key_) + ")\n"
                 + "size(): " + std::to_string(size()) + " n_to_keep: " + std::to_string(n_to_keep) + " nToRemove(): "
                 + std::to_string(nToRemove()));

        // if there are particles to remove, they will have key uint64_t(-1)
        // treat them as having key max_k<<1
        const uint64_t max_k1 = max_key_ << ( nToRemove() > 0 ? 1 : 0);

        if (max_k1 > min_key_) // not all keys are the same -> perform sort
          RadixSortParallel(max_k1,min_key_,n_to_keep,timers);
      }

    // all particles for which isMarkedRemove() is true are now past range 0...n_to_keep

    // resize to new size
#pragma omp barrier
#pragma omp single
    {
      // resize all containers and keep current data + keys
      resize(n_to_keep, resizeflag(ResizeMode::ALL) | resizeflag(ResizeMode::KEEP_CURRENT) );
      setNToRemove(0);
      if (size() > 0)
        {
          if(__builtin_expect(getKey(size()-1) == max_key_,true)) {}
          else
            {
              printSort(false,size()-50,size(),100);

              MYASSERT(getKey(size()-1) == max_key_, "sort failed");
            }
        }
    }

    return 3;
  }

  void ParticleCollection::permute()
  {
    ParallelPermute(size());
    ParallelPermuteKeys();
#pragma omp single
    {
      swapOldCurrent();
      swapOldCurrentSortData();
    }
  }




  // COPY CTOR
  ParticleCollection::ParticleCollection(
      const ParticleCollection& other,
      uint64_t begin,
      uint64_t N)
  {
    // allocate space
    resize(N,static_cast<uint8_t>(ResizeMode::ALL));

    // copy
    const void* __restrict__ src = (const void*)other.getParticle(begin);
    void* __restrict__ dst = getParticle(0);

    std::memcpy(dst,src,N*sizeof(Particle));
  }



} // namespace ParticleCollections
