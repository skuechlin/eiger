/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * ParticleCollection.h
 *
 *  Created on: Apr 17, 2014
 *      Author: Stephan Kuechlin
 */

#ifndef PARTICLECOLLECTION_H_
#define PARTICLECOLLECTION_H_


#include <mpi.h>
#include <omp.h>

#include <iostream>
#include <iomanip> // debug output

#include <Eigen/Dense>

#include "Primitives/MyIntrin.h"
#include "Primitives/MyMemory.h"
#include "Primitives/Partitioning.h"

#include "Particle.h"

namespace Settings { class Dictionary; }
namespace MyRandom { class Rng; }
namespace MyChrono { class TimerCollection; }
namespace MyMPI    { class MPIMgr; }
namespace Grids    { class Grid; }

namespace Particles
{

  constexpr char VarNames[] = {
      "         Rx "
      "         Ry "
      "         Rz "
      "         Rw  "
      "         Cx "
      "         Cy "
      "         Cz "
      "         Cw  "
      "         Dx "
      "         Dy "
      "         Dz "
      "         Dw  "
      "     Omegax "
      "     Omegay  "
      "        Xix "
      "        Xiy  "
      "         dt "
      "          w "
      "          m "
      "        pad"};


  class ParticleCollection
  {
  private:

    typedef MyMemory::aligned_array<Particle,64> DataT;

    DataT data_;
    DataT data_old_;

    typedef MyMemory::aligned_array<uint64_t,64> KeyDataT;
    typedef MyMemory::aligned_array<Particle*,64> SrcDataT;

    KeyDataT key_;
    SrcDataT src_;

    KeyDataT key_old_;
    SrcDataT src_old_;

  private:

    uint64_t end_ = 0;  // index of next element after last assigned, == number of particles

    uint64_t global_size_ = 0;

    uint64_t n_to_remove_ = 0;

    uint64_t max_key_ =  0;
    uint64_t min_key_ = -1;

  public:
    // SETTERS

    void setNToRemove(const uint64_t n) { n_to_remove_ = n; }

    void setMaxMinKey(const uint64_t max_k, const uint64_t min_k) {
      max_key_ = max_k;
      min_key_ = min_k; }


    enum class ResizeMode : uint8_t {
      CURRENT_DATA = 1,
          CURRENT_SORT = 2,
          CURRENT = 3,
          OLD_DATA = 4,
          OLD_SORT = 8,
          OLD = 12,
          ALL = 15,
          KEEP_CURRENT_DATA = 16,
          KEEP_CURRENT_KEYS = 32,
          KEEP_CURRENT = 48,
          KEEP_OLD_DATA = 64,
          KEEP_DATA = 80,
          KEEP_OLD_KEYS = 128,
          KEEP_KEYS = 160,
          KEEP_OLD = 192,
          KEEP_ALL = 240
    };

    static constexpr uint8_t resizeflag(ResizeMode m) {
      return static_cast<uint8_t>(m); }

    // resizes data containers according to ResizeMode
    void resize(const uint64_t newSize, const uint8_t mode);

    void resize(const uint64_t newSize, const ResizeMode mode) {
      resize(newSize,resizeflag(mode)); }

    void reserve(const uint64_t num);


  private:

    // set the size, without changing data containers
    void setSize(const uint64_t size) { end_ = size; }


  public:
    // GETTERS

    const uint64_t* keys() const { return key_.data(); }

    // return number of particles
    uint64_t size() const { return end_; }

    // return current particle storage capacity
    uint64_t capacity() const { return data_.capacity(); }

    // on root, return total number of particles across all mpi ranks
    uint64_t global_size() const { return global_size_; }

    uint64_t nToRemove() const { return n_to_remove_; }



    __attribute__((__always_inline__))
    inline Particle* getParticle(const uint64_t j) {
      return data_.data() + j; }

    __attribute__((__always_inline__))
    inline const Particle* getParticle(const uint64_t j) const {
      return data_.data() + j; }

    __attribute__((__always_inline__))
    inline Particle* getOldParticle(const uint64_t j) {
      return data_old_.data() + j; }

    __attribute__((__always_inline__))
    inline const Particle* getOldParticle(const uint64_t j) const {
      return data_old_.data() + j; }


    __attribute__((__always_inline__))
    inline Particle* first() {return getParticle(0); }

    __attribute__((__always_inline__))
    inline const Particle* first() const {return getParticle(0); }

    __attribute__((__always_inline__))
    inline Particle* last() {return getParticle(size()); }

    __attribute__((__always_inline__))
    inline const Particle* last() const {return getParticle(size()); }


    __attribute__((__always_inline__))
    inline uint64_t getIndex(const Particle* const p) const {
      return std::distance(first(),p); }


    // get key of particle j
    __attribute__((__always_inline__))
    inline uint64_t getKey(const uint64_t j) const {
      return key_[j]; }

    // get key of particle j
    __attribute__((__always_inline__))
    inline uint64_t& getKey(const uint64_t j) {
      return key_[j]; }

    // get key of particle p
    __attribute__((__always_inline__))
    inline uint64_t getKey(const Particle* const p) const {
      return getKey( getIndex(p) ); }

    // get key of particle p
    __attribute__((__always_inline__))
    inline uint64_t& getKey(const Particle* const p) {
      return getKey( getIndex(p) ); }


  private:

    // overwrite particle information at dest in _old array with information at source in current
    __attribute__((__always_inline__))
  inline
  void copyPrtToOld(const Particle* const p, const uint64_t idest) {
      std::memcpy(
          __builtin_assume_aligned((void*)getOldParticle(idest),alignof(Particle)),
          __builtin_assume_aligned((void*)p,alignof(Particle)),
          sizeof(Particle)); }

    // overwrite particle information at dest in _old array with information at source in current
    __attribute__((__always_inline__))
    inline
    void copyPrtToOld(const uint64_t isource, const uint64_t idest) {
      std::memcpy(
          __builtin_assume_aligned((void*)getOldParticle(idest),alignof(Particle)),
          __builtin_assume_aligned((void*)getParticle(isource),alignof(Particle)),
          sizeof(Particle)); }

    // overwrites sort data (index and position) at dest in _old array with information at source in current
    __attribute__((__always_inline__))
    inline
    void copySortToOld(const uint64_t source, const uint64_t dest) {
      key_old_[dest] = key_[source];
      src_old_[dest] = src_[source]; }

  public:

    // swap old and current matrices. NOTE: only pointers are swapped, no data!
    void swapOldCurrent() {  data_old_.swap(data_); }

    // swap old and current index and rank vectors
    void swapOldCurrentSortData() {
      key_old_.swap(key_);
      src_old_.swap(src_); }

  private:

    // store pointer to particles in data_ array
    // to be permuted by sort routine
    void setSrcPtrs();


  public:
    // SORT

    // sort all particles by their index (stable)
    uint8_t sort(MyChrono::TimerCollection& timers);

    void permute();


    // check if idx array is sorted
    bool keysAreSorted() const
    {
      static bool sorted;
#pragma omp single
      {
        uint64_t key, prev = 0;
        sorted = true;
        for (uint64_t i = 0; i < size() && sorted; ++i)
          {
            sorted = prev <= (key = getKey(i));
            prev = key;
          }
      }
      return sorted;
    }

  private:


    void RadixSortParallelAVX2(const uint64_t max_k, const uint64_t n_to_keep);
    void RadixSortParallelSIMD(const uint64_t max_k, const uint64_t n_to_keep);
    void RadixSortParallel(
        const uint64_t max_k,
        const uint64_t min_k,
        const uint64_t n_to_keep,
        MyChrono::TimerCollection& timers);
    void RadixSortSerial(const uint64_t max_k, const uint64_t n_to_keep);

    void CountingSortParallel(const uint64_t max_k, const uint64_t n_to_keep);
    void CountingSortSerial(const uint64_t max_k, const uint64_t n_to_keep);


    void ParallelPermute(const uint64_t n_to_keep, const bool tasks = false);
    void ParallelPermuteKeys();


  public:
    // MPI COMM

    // return maximum particle speed
    double maxSpeed(const MyMPI::MPIMgr& mpiMgr) const;

    // exchange particles
    void communicate(
        Grids::Grid* const nodeGrid,
        const MyMPI::MPIMgr& mpiMgr,
        const uint64_t tag);

  private:

    // communicate particles, will require full sort
    void communicate_prts(
        Grids::Grid* const nodeGrid,
        const MyMPI::MPIMgr& mpiMgr,
        const uint64_t tag);

    // communicate particles, avoid message to self,
    // sort src ptrs by making use of sorted sub-sequences
    // requires subsequent permute and permuteKeys
    void communicate_and_merge3(
        Grids::Grid* const nodeGrid,
        const MyMPI::MPIMgr& mpiMgr,
        const uint64_t tag);

    void communicate_and_merge4(
        Grids::Grid* const nodeGrid,
        const MyMPI::MPIMgr& mpiMgr,
        const uint64_t tag);


  public:
    // IO
    double read(
        const std::string& fileName,
        const MyMPI::MPIMgr& mpiMgr);

    void write(
        const double solTime,
        const std::string& fileName,
        const MyMPI::MPIMgr& mpiMgr) const;

    __attribute__((cold))
    std::string prt2str(const uint64_t i) const
    {
      std::stringstream ss;
      const double * const ptr = (double*)getParticle(i);
      for (uint64_t i = 0; i < sizeof(Particle)/sizeof(double); ++i)
        {
          //          ss << std::scientific  << std::setprecision(6) << std::setw(15) << *ptr++ << " ";
          ss << ptr[i] << " ";
        }
      return ss.str();
    }

    __attribute__((cold))
    void print(const uint64_t first = 0, uint64_t last = -1, const uint64_t nmax = -1) const
    {
      last = std::min(
          last,
          static_cast<uint64_t>( data_.size() )
      );

      if (last - first > nmax)
        last = first + nmax;

      std::cout << "\n\n" << "particles:" << std::endl;
      std::cout << VarNames << std::endl;

      for (uint64_t i = first; i < last; ++i)
        std::cout  << prt2str(i) << "\n";

      std::cout << std::endl;

    }

    __attribute__((cold))
    void printSort(
        const bool printSrcPtrs = false,
        const uint64_t first = 0,
        uint64_t last = -1,
        const uint64_t nmax = -1) const
    {
      last = std::min(
          last,
          static_cast<uint64_t>( data_.size() )
      );

      if (last - first > nmax)
        last = first + nmax;


      std::cout << "\n\n" << "keys:" << std::endl;
      for (uint64_t j = first; j < last; ++j)
        std::cout << std::setw(15) << j << " " << std::setw(15) << getKey(j) << "\n";

      if (printSrcPtrs)
        {
          std::cout << "\nindices:\n";
          for (uint64_t j = first; j < last; ++j)
            std::cout << std::setw(15) << j << " " << std::setw(15) << src_[j] << "\n";
          std::cout <<"\n\n"<< std::endl;
        }

    }

  public:
    // CTOR
    explicit ParticleCollection(){};

    // DTOR
    ~ParticleCollection(){};

    // COPY CTOR
    ParticleCollection(const ParticleCollection& other, uint64_t begin, uint64_t N);


  };	// class ParticleCollection

  inline  // check if all values of all particles are finite
  bool all_finite_tf(const ParticleCollection* const prts)
  {

    static bool res;

#pragma omp single
    res = true;

#pragma omp for schedule(static) reduction(&&:res)
    for (uint64_t i = 0; i < prts->size(); ++i)
      res = res && is_finite_tf(prts->getParticle(i));

    return res;
  }

  // output
  inline std::string header() { return "n"; }

  inline std::ostream& operator<<(std::ostream& os, const ParticleCollection& prts) {
    return os << prts.size(); }

  MPI_Datatype particleMPIType();


} // namespace Particles







#endif /* PARTICLECOLLECTION_H_ */
