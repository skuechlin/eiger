/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * ParticleCollection_CommAndMerge2.cpp
 *
 *  Created on: Feb 15, 2018
 *      Author: kustepha
 */

#ifdef __STRICT_ANSI__
#define STRICT_ANSI_STATE
#undef __STRICT_ANSI__
#endif

#include <parallel/algorithm> // copy_backward, stable_sort, unique

#ifdef STRICT_ANSI_STATE
#define __STRICT_ANSI__ STRICT_ANSI_STATE
#endif

#include "ParticleCollection.h"

#include "Grid/Grid.h"

#include "Parallel/MyMPI_DSDEComm2.h"

#include "Primitives/CRC32.h"

namespace Particles
{
  // exchange particles
  void ParticleCollection::communicate_and_merge4(
      Grids::Grid* const nodeGrid,
      const MyMPI::MPIMgr& mpiMgr,
      const uint64_t tag)
  {
    // mpi type
    MPI_Datatype particle_t = particleMPIType();

    // put particles to send in data_old_
    swapOldCurrent();

    //  make sure to pass the correct send displacements
    MyMPI::DSDECommSpec2 commspec(
        nodeGrid->count_.data(),nodeGrid->begin_.data(),mpiMgr.comm(),
        COMPILE_TIME_CRC32_STR( FILE_POS_STR ) + tag );

    const uint64_t n_prts_new = commspec.n_to_receive();

    resize(n_prts_new,
           resizeflag(ResizeMode::CURRENT) |
           resizeflag(ResizeMode::OLD_SORT) );

    const int n_out = commspec.outdegree();
    const int n_in  = commspec.indegree();

    std::vector<MPI_Request> sendreqs(n_out);
    std::vector<MPI_Request> recvreqs(n_in);

    commspec.Icomm(
        sendreqs.data(),
        recvreqs.data(),
        data_old_.data(), // send buffer
        data_.data(), // receive buffer
        sizeof(Particle),
        particle_t,
        COMPILE_TIME_CRC32_STR( FILE_POS_STR ) + tag );

    uint64_t spin_count = 0;
    while(true)
      {

        int sends_complete = (n_out == 0);
        int recvs_complete = (n_in == 0);
        if (n_out)
          MPI_Testall(n_out,sendreqs.data(),&sends_complete,MPI_STATUSES_IGNORE);
        if (n_in)
          MPI_Testall(n_in,recvreqs.data(),&recvs_complete,MPI_STATUSES_IGNORE);
        if (sends_complete && recvs_complete)
          break;

        ++spin_count;

        if (spin_count > 100000000)
          {
            std::cout << "ts " << tag << ": rank " << mpiMgr.rank() << " tried " << spin_count << " times, " <<
                " but the following ops are not complete:" << "\n";

            for (int i = 0; i < n_out; ++i)
              {
                int flag = 0;
                MPI_Test(&sendreqs[i],&flag,MPI_STATUS_IGNORE);
                if (!flag)
                  {
                    std::cout << "    send #" << i << ": " << commspec.neighbor_sendcounts_[i] << " to " <<
                        commspec.neighbor_destinations_[i] << "\n";
                  }

              }

            for (int i = 0; i < n_in; ++i)
              {
                int flag = 0;
                MPI_Test(&recvreqs[i],&flag,MPI_STATUS_IGNORE);
                if (!flag)
                  {
                    std::cout << "    recv #" << i << ": " << commspec.neighbor_recvcounts_[i] << " from " <<
                        commspec.neighbor_sources_[i] << "\n";
                  }
              }

            std::cout << std::endl;

            if (n_out)
              MPI_Waitall(n_out,sendreqs.data(),MPI_STATUSES_IGNORE);
            if (n_in)
              MPI_Waitall(n_in,recvreqs.data(),MPI_STATUSES_IGNORE);
          }


#pragma omp taskyield
      }

    // compute keys
    nodeGrid->topology()->computeKeys(*this,0,n_prts_new);

    // set source pointers in src_old_
    for (uint64_t i = 0; i < n_prts_new; ++i)
      src_old_[i] = getParticle(i);


    if (n_in)
      {

        // all relevant particle data are now in data_, keys are computed, src ptrs set

        // sort the src_old_ ptrs by the keys using parallel multiway merge to src_

        std::vector<std::pair<Particle**,Particle**>> runs;
        runs.reserve(n_in);

        // the received
        for (int i = 0; i < n_in; ++i)
          runs.emplace_back(
              src_old_.data() + commspec.neighbor_recvdispls_[i],
              src_old_.data() + commspec.neighbor_recvdispls_[i] +
              commspec.neighbor_recvcounts_[i]
          );

        const auto srccomp = [this](const Particle* const a, const Particle* const b){
          return getKey(a) < getKey(b);
        };

        __gnu_parallel::multiway_merge(
            runs.begin(),runs.end(),src_.data(),size(),srccomp,
            __gnu_parallel::exact_tag(std::max(omp_get_num_threads() - 1,1)));

        // sort data now in current

      } // if indegree()


    resize(size(),ResizeMode::OLD_DATA);
  }

}
