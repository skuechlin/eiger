/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * Particle.h
 *
 *  Created on: Nov 18, 2016
 *      Author: kustepha
 */

#ifndef SRC_PARTICLES_PARTICLE_H_
#define SRC_PARTICLES_PARTICLE_H_

#include <iomanip>

#include "Primitives/MyIntrin.h"

namespace Particles
{


  struct alignas(64) Particle {

    v4df R; // position            //  32
    v4df C; // velocity            //  64

    v4df D; // effective velocity  //  96
    v4df E; // pad                 // 128

    v2df Omega; // rotational dof  // 144
    v2df Xi;    // vibrational dof // 160

    double dt;  // remaining tof
    double w;   // statistical weight (FNum)
    double m;   // molecular mass
    uint64_t pad;                  // 192

    friend std::ostream& operator<<(std::ostream& os, const Particle& p);

    Particle() = delete;
    Particle(const Particle&) = delete;
    Particle(const Particle&&) = delete;
    Particle& operator=(const Particle&) = delete;
    Particle& operator=(const Particle&&) = delete;
    ~Particle() = delete;
  };

  __attribute__((__always_inline__))
  inline void mark_remove(Particle* const p) { p->dt = -2.0; }

  __attribute__((__always_inline__))
  inline bool is_marked_remove_tf(const Particle* const p) { return p->dt < -0.5; }

  template<int rw = 0, int locality = 3>
  __attribute__((__always_inline__))
  inline void prefetch(const Particle* const p) {
    for (uint i = 0; i < sizeof(Particle); i+=64)
      __builtin_prefetch(((char*)p)+i,rw,locality);
  }

  // copy complete particle information
  __attribute__((__always_inline__))
  inline void copy(Particle* const __restrict__ to, const Particle* const __restrict__ from) {
    std::memcpy((void*)to,(const void*)from,sizeof(Particle));
  }

  // copy range of particle information
  __attribute__((__always_inline__))
  inline void copy(
      Particle* const __restrict__ to,
      const Particle* const __restrict__ begin_from,
      const Particle* const __restrict__ end_from) {
    std::memcpy(
        (void*)to,
        (const void*)begin_from,
        sizeof(Particle)*std::distance(begin_from,end_from));
  }

  // update position of particle according to R += _dt*_D;
  // leave particle velocity and time of flight unchanged
  __attribute__((__always_inline__))
  inline void translate(Particle* const p, const v4df& __restrict__ _D, const double _dt) {
    p->R += _dt*_D; }

  // update position of particle according to R += _dt*D();
  // updates remaining time of flight
  __attribute__((__always_inline__))
  inline void free_flight(Particle* const p, const double _dt) {
    p->R += _dt*p->D; p->dt -= _dt; }


  // update position of particle according to xnew = xold + vel*dt;
  // s.t. remaining time of flight == 0
  __attribute__((__always_inline__))
  inline void free_flight(Particle* const p) { free_flight(p,p->dt); }

  __attribute__((__always_inline__))
  inline void set_velocity(Particle* const p, const v4df& _C) {
    p->C = _C;
    p->D = _C;
  }

  __attribute__((__always_inline__))
  inline void set_velocity(Particle* const p, const v4df& _C, const v4df& _D) {
    p->C = _C;
    p->D = _D;
  }


  // rotate particle position and velocit(y/ies) to xy-plane (xr-plane)
  inline void rotate2xy(Particle* const p) {

    //         ->
    // R[ 0 ] x  x
    // R[ 1 ] y  r
    // R[ 2 ] z  0
    // R[ 3 ] *  *

    // D[ 0 ] u  u
    // D[ 1 ] v  vr
    // D[ 2 ] w  vtheta
    // D[ 3 ] *  *

    // compute angle
    //
    // cos = y / r
    // sin = z / r
    //
    const double rc = p->R[1]; // y=r*cos
    const double rs = p->R[2]; // z=r*sin

    // compute radial position
    const double r = hypot( rc, rs );

    // set radial position
    p->R[1] = r;
    p->R[2] = 0;

    // rotate velocity
    //
    // [ vr ] = [ cos sin ] [ v ]
    // [ vt ]   [-sin cos ] [ w ]

    double v = p->C[1];   // avoid alias
    double w = p->C[2];

    p->C[1] = (  rc*v + rs*w ) /r;
    p->C[2] = ( -rs*v + rc*w ) /r;

    v = p->D[1];
    w = p->D[2];

    p->D[1] = (  rc*v + rs*w ) /r;
    p->D[2] = ( -rs*v + rc*w ) /r;

  }

  inline bool is_finite_tf(const Particle* p) {
    static_assert( ( sizeof(Particle) & (sizeof(v4df)-1) ) == 0 );
    v4df s = {0.,0.,0.,0.};
    for (uint64_t i = 0; i < sizeof(Particle)/sizeof(v4df); ++i)
      s += *(v4df*)((char*)p + i*sizeof(v4df));
    return !(std::isnan(double(s[0]+s[1]+s[2]+s[3]))); }

  inline std::ostream& operator<<(std::ostream& os, const Particle& p) {
    os.setf(std::ios_base::scientific | std::ios_base::showpos, std::ios_base::floatfield | std::ios_base::showpos);
    return os <<
        p.R[0] << " " << p.R[1] << " " << p.R[2] << " " << p.R[3] << "  " <<
        p.C[0] << " " << p.C[1] << " " << p.C[2] << " " << p.C[3] << "  " <<
        p.D[0] << " " << p.D[1] << " " << p.D[2] << " " << p.D[3] << "  " <<
        p.Omega[0] << " " << p.Omega[1] << "  " << p.Xi[0] << " " << p.Xi[1] << "  " <<
        p.dt << " " << p.w << " " << p.m << " " << p.pad <<
        std::resetiosflags(std::ios_base::floatfield | std::ios_base::showpos);
  }

  template<uint8_t reg_size>
  struct alignas(64) ParticleRegMap;

#if defined(__AVX512F__)
  template<>  struct alignas(64) ParticleRegMap<64>{

    reg_t<64> r00;
    reg_t<64> r01;
    reg_t<64> r02;

    void load(Particle* p) {
      r00.load((char*)p +   0);
      r01.load((char*)p +  64);
      r02.load((char*)p + 128);
    }
    void store(Particle* p) {
      r00.store((char*)p +   0);
      r01.store((char*)p +  64);
      r02.store((char*)p + 128);
    }
  };
#endif


  template<>  struct alignas(64) ParticleRegMap<32>{

    reg_t<32> r00;
    reg_t<32> r01;
    reg_t<32> r02;
    reg_t<32> r03;
    reg_t<32> r04;
    reg_t<32> r05;

    void load(Particle* p) {
      r00.load((char*)p +   0);
      r01.load((char*)p +  32);
      r02.load((char*)p +  64);
      r03.load((char*)p +  96);
      r04.load((char*)p + 128);
      r05.load((char*)p + 160);
    }
    void store(Particle* p) {
      r00.store((char*)p +   0);
      r01.store((char*)p +  32);
      r02.store((char*)p +  64);
      r03.store((char*)p +  96);
      r04.store((char*)p + 128);
      r05.store((char*)p + 160);
    }
  };



  template<uint8_t reg_size, uint8_t num_regs>
  struct alignas(64) ParticleRegTable {
    static constexpr uint8_t num_parts = num_regs*reg_size / sizeof(Particle);

    static_assert(sizeof(ParticleRegMap<reg_size>)==sizeof(Particle));

    ParticleRegMap<reg_size> prts[num_parts];

    void gather(Particle** pp) {
      for (uint8_t i = 0; i < num_parts; ++i)
        prts[i].load(pp[i]);
    }

    void store(Particle* p) {
      for (uint8_t i = 0; i < num_parts; ++i)
        prts[i].store(p+i);
    }


  };





} // namespace Particles

#endif /* SRC_PARTICLES_PARTICLE_H_ */
