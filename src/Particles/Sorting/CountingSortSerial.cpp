/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * CountingSortSerial.cpp
 *
 *  Created on: Jun 2, 2017
 *      Author: kustepha
 */





#include <omp.h>
#include <memory>
#include <iostream>

#include "Particles/ParticleCollection.h"

#include "Primitives/MyArithm.h"
#include "Primitives/MyError.h"
#include "Primitives/Partitioning.h"


namespace Particles
{


  void ParticleCollection::CountingSortSerial(const uint64_t max_k, const uint64_t n_to_keep)
  {

#pragma omp single
    {

      const uint64_t nbins = max_k+1;

      constexpr uint64_t tsz = sizeof(uint64_t);

      //  count
      uint64_t* count = (uint64_t*)_mm_malloc( nbins*tsz, 32 );

      // initialize to zero
      for(uint64_t i=0; i<nbins; ++i)
        count[i] = 0;

      // count
      for(uint64_t i=0; i<end_; ++i)
        ++count[getKey(i)];

      // compute prefix sum (begin of bins)
      uint64_t begin = 0;
      uint64_t tmp;
      for(uint64_t i=0; i<nbins; ++i)
        {

          tmp = count[i];
          count[i] = begin;
          begin += tmp;
        }


      // fill sorted array
      for(uint64_t i=0; i<end_; ++i)
        {
          uint64_t dest = count[ getKey(i) ]++;
          if (dest < n_to_keep)
            {
              copyPrtToOld(i, dest);
              copySortToOld(i, dest);
            }
        }


      // free memory
      _mm_free(count);

      swapOldCurrent();
      swapOldCurrentSortData();

    } // single


  }

} // namespace Particles
