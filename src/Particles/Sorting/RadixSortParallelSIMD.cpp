/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * RadixSortParallelSIMD.cpp
 *
 *  Created on: Jun 9, 2017
 *      Author: kustepha
 */




/*
 * RadixSortParallel.cpp
 *
 *  Created on: Jun 2, 2017
 *      Author: kustepha
 */




#include <omp.h>
#include <memory>
#include <iostream>

#include "Particles/ParticleCollection.h"

#include "Primitives/MyArithm.h"
#include "Primitives/MyError.h"
#include "Primitives/Partitioning.h"


namespace Particles
{
#if false && defined(__AVX2__) && defined(__BMI2__)

  void ParticleCollection::RadixSortParallelSIMD(const uint64_t max_k, const uint64_t n_to_keep)
  {

    // radix sort parameters
    constexpr uint64_t nbits = 8;
    constexpr uint64_t radix = 1ul<<nbits;
    constexpr uint64_t mask  = radix-1;

    constexpr uint64_t tsz = sizeof(uint64_t);
    constexpr uint64_t pgsz = 4096; // page size (4096 bytes)
    constexpr uint8_t vsz = 32;

    constexpr __m256i shuff_0 = __m256i{
      (int64_t)((uint64_t(-1) << 16) ^ 0x0800),
          (int64_t)uint64_t(-1),
          (int64_t)((uint64_t(-1) << 16) ^ 0x0800),
          (int64_t)uint64_t(-1)};

    constexpr __m256i ones = __m256i{
      (int64_t)uint64_t(-1),
          (int64_t)uint64_t(-1),
          (int64_t)uint64_t(-1),
          (int64_t)uint64_t(-1)};


    // round local count array size to a multiple of page size (4096 bytes)
    constexpr uint64_t local_count_extent = radix > pgsz/tsz ? radix : pgsz/tsz;


    // parallel information
    const uint64_t nthreads = omp_get_num_threads();
    const uint64_t ithread  = omp_get_thread_num();
    const uint64_t begin    = MyPartitioning::partitionStart(ithread,end_,nthreads);
    const uint64_t end      = begin + MyPartitioning::partitionLength(ithread,end_,nthreads);

    const uint64_t  prolog_end      = std::min( begin + (begin%vsz ? vsz - (begin%vsz) : 0), end );
    const uint64_t  nvec            = (end - prolog_end)/ vsz;
    const uint64_t  epilog_begin    = prolog_end + vsz * nvec;

    const uint64_t local_count_offset = local_count_extent*ithread;

    // (global) pointer to shared array
    // alloc only once and reuse
    // pray that this is thread safe
    typedef void (&deleter_t)(void*);
    static std::unique_ptr<uint64_t,deleter_t> shared_count
    (
        (uint64_t*)_mm_malloc(local_count_extent*tsz*nthreads, pgsz),  //align memory to page size
        _mm_free // use to free aligned memory
    );


    // pointer to thread's range in shared memory
    uint64_t *const __restrict__ private_count = (uint64_t*)__builtin_assume_aligned(
        shared_count.get() + local_count_offset, pgsz);

    // initialize src ptr array
    setSrcPtrs();

    // highest nbits bits to consider
    const uint8_t max_digit = max_k > 0 ? nbits * (MyArithm::msb(max_k) / nbits) : 0;

    __m256i shuff = shuff_0;


    // for each digit
    for (uint8_t d = 0; d <= max_digit; d += nbits)
      {

        for(uint64_t i=0; i<radix; ++i)
          private_count[i] = 0;

        // private count, divide key range among threads
        // prolog
        for(uint64_t i=begin; i<prolog_end; ++i)
          ++private_count[ ( key_[i] >> d ) & mask ];
        // SIMD loop
        for(uint64_t i=prolog_end; i < epilog_begin; i+=vsz)
          {
            __m256i k0 = _mm256_load_si256((__m256i*)&key_[i]);
            __m256i k1 = _mm256_load_si256((__m256i*)&key_[i+4]);
            __m256i k2 = _mm256_load_si256((__m256i*)&key_[i+8]);
            __m256i k3 = _mm256_load_si256((__m256i*)&key_[i+12]);
            __m256i k4 = _mm256_load_si256((__m256i*)&key_[i+16]);
            __m256i k5 = _mm256_load_si256((__m256i*)&key_[i+20]);
            __m256i k6 = _mm256_load_si256((__m256i*)&key_[i+24]);
            __m256i k7 = _mm256_load_si256((__m256i*)&key_[i+28]);

            k0 = _mm256_shuffle_epi8(k0,shuff);
            k1 = _mm256_shuffle_epi8(k1,_mm256_alignr_epi8(shuff,ones,14));
            k2 = _mm256_shuffle_epi8(k2,_mm256_alignr_epi8(shuff,ones,12));
            k3 = _mm256_shuffle_epi8(k3,_mm256_alignr_epi8(shuff,ones,10));
            k4 = _mm256_shuffle_epi8(k4,_mm256_alignr_epi8(shuff,ones,8));
            k5 = _mm256_shuffle_epi8(k5,_mm256_alignr_epi8(shuff,ones,6));
            k6 = _mm256_shuffle_epi8(k6,_mm256_alignr_epi8(shuff,ones,4));
            k7 = _mm256_shuffle_epi8(k7,_mm256_alignr_epi8(shuff,ones,2));

            k0 = _mm256_xor_si256(k0, k1);
            k2 = _mm256_xor_si256(k2, k3);
            k4 = _mm256_xor_si256(k4, k5);
            k6 = _mm256_xor_si256(k6, k7);

            k0 = _mm256_xor_si256(k0, k2);
            k4 = _mm256_xor_si256(k4, k6);

            k0 = _mm256_xor_si256(k0, k4);

            k1 = _mm256_permute2x128_si256 (k0, k0, 1);

            k2 = _mm256_unpacklo_epi16(k0,k1);

            k3 = _mm256_unpackhi_epi16(k1,k0);

            v32qu bytes = (v32qu)_mm256_permute2x128_si256(k2,k3,0x30);

            for (int8_t k = 0; k < vsz; ++k)
              ++private_count[ bytes[k] ];
          }
        // epilog
        for(uint64_t i=epilog_begin; i<end; ++i)
          ++private_count[ ( key_[i] >> d ) & mask ];

#pragma omp barrier

        // compute prefix sum (begin of bins)
#pragma omp single
        {
            uint64_t sum = 0;
            uint64_t* const __restrict__ count = (uint64_t*)__builtin_assume_aligned(shared_count.get(),pgsz);
            for(uint64_t i=0; i<radix; ++i)
              {
                for(uint64_t j=0; j<nthreads; ++j)
                  {
                    const uint64_t tmp = count[i+j*local_count_extent];
                    count[i+j*local_count_extent] = sum;
                    sum += tmp;
                  }
              }
        }
        //implicit barrier here


        //         fill sorted array, divide key range among threads
        // prolog
        for(uint64_t i=begin; i<prolog_end; ++i)
          copySortToOld(i, private_count[ ( key_[i] >> d ) & mask ]++);
        // SIMD loop
        for(uint64_t i=prolog_end; i < epilog_begin; i+=vsz)
          {
//            _mm_prefetch(&key_[i+32],_MM_HINT_T0);
//            _mm_prefetch(&key_[i+40],_MM_HINT_T0);
//            _mm_prefetch(&key_[i+48],_MM_HINT_T0);
//            _mm_prefetch(&key_[i+56],_MM_HINT_T0);
//
//            _mm_prefetch(&src_[i+32],_MM_HINT_T0);
//            _mm_prefetch(&src_[i+40],_MM_HINT_T0);
//            _mm_prefetch(&src_[i+48],_MM_HINT_T0);
//            _mm_prefetch(&src_[i+56],_MM_HINT_T0);

            __m256i k0 = _mm256_load_si256((__m256i*)&key_[i]);
            __m256i k1 = _mm256_load_si256((__m256i*)&key_[i+4]);
            __m256i k2 = _mm256_load_si256((__m256i*)&key_[i+8]);
            __m256i k3 = _mm256_load_si256((__m256i*)&key_[i+12]);
            __m256i k4 = _mm256_load_si256((__m256i*)&key_[i+16]);
            __m256i k5 = _mm256_load_si256((__m256i*)&key_[i+20]);
            __m256i k6 = _mm256_load_si256((__m256i*)&key_[i+24]);
            __m256i k7 = _mm256_load_si256((__m256i*)&key_[i+28]);

            k0 = _mm256_shuffle_epi8(k0,shuff);
            k1 = _mm256_shuffle_epi8(k1,_mm256_alignr_epi8(shuff,ones,14));
            k2 = _mm256_shuffle_epi8(k2,_mm256_alignr_epi8(shuff,ones,12));
            k3 = _mm256_shuffle_epi8(k3,_mm256_alignr_epi8(shuff,ones,10));
            k4 = _mm256_shuffle_epi8(k4,_mm256_alignr_epi8(shuff,ones,8));
            k5 = _mm256_shuffle_epi8(k5,_mm256_alignr_epi8(shuff,ones,6));
            k6 = _mm256_shuffle_epi8(k6,_mm256_alignr_epi8(shuff,ones,4));
            k7 = _mm256_shuffle_epi8(k7,_mm256_alignr_epi8(shuff,ones,2));

            k0 = _mm256_xor_si256(k0, k1);
            k2 = _mm256_xor_si256(k2, k3);
            k4 = _mm256_xor_si256(k4, k5);
            k6 = _mm256_xor_si256(k6, k7);

            k0 = _mm256_xor_si256(k0, k2);
            k4 = _mm256_xor_si256(k4, k6);

            k0 = _mm256_xor_si256(k0, k4);

            k1 = _mm256_permute2x128_si256 (k0, k0, 1);

            k2 = _mm256_unpacklo_epi16(k0,k1);

            k3 = _mm256_unpackhi_epi16(k1,k0);

            v32qu bytes = (v32qu)_mm256_permute2x128_si256(k2,k3,0x30);
            v32qu offset = MyIntrin::byte_rank32(bytes);

            for (int8_t k = 0; k < vsz; ++k)
              copySortToOld(i+k, uint64_t(private_count[ bytes[k] ] + offset[k]) );

            for (int8_t k = 0; k < vsz; ++k)
              ++private_count[ bytes[k] ];


          }
        // epilog
        for(uint64_t i=epilog_begin; i<end; ++i)
          copySortToOld(i, private_count[ ( key_[i] >> d ) & mask ]++);


        // swap old, current
#pragma omp barrier
#pragma omp single
        swapOldCurrentSortData();

        // implicit barrier here

        shuff = _mm256_add_epi64(shuff,__m256i{(int64_t)(0x0101),0,(int64_t)(0x0101),0});

      } // for each digit
#pragma omp barrier


    // assert sorted
    //        MYASSERT(keysAreSorted(), "particle sort failed!");


    // index array is completely sorted, now do the permutation
    ParallelPermute(n_to_keep);


#pragma omp single
    swapOldCurrent();

  }

#elif defined(__AVX2__) && defined(__BMI2__) && defined(__HYBRID_VECTORIZED_SORT_OLD__)

  void ParticleCollection::RadixSortParallelSIMD(const uint64_t max_k, const uint64_t n_to_keep)
  {

    // radix sort parameters
    constexpr uint64_t nbits = 8;
    constexpr uint64_t radix = 1ul<<nbits;

    constexpr uint64_t tsz  = sizeof(uint64_t);
    constexpr uint64_t pgsz = 4096; // page size (4096 bytes)
    constexpr uint8_t  vsz  = 4;    // vector length

    typedef __m256i vtype;

    // round local count array size to a multiple of page size (4096 bytes)
    constexpr uint64_t local_count_extent = MyArithm::roundUp(vsz*radix,pgsz/tsz);


    // parallel information
    const uint64_t nthreads = omp_get_num_threads();
    const uint64_t ithread  = omp_get_thread_num();
    const uint64_t begin    = MyPartitioning::partitionStart(ithread,end_,nthreads);
    const uint64_t end      = begin + MyPartitioning::partitionLength(ithread,end_,nthreads);

    const uint64_t  prolog_end      = std::min( begin + (begin%vsz ? vsz - (begin%vsz) : 0), end );
    const uint64_t  nvec            = (end - prolog_end)/ vsz;
    const uint64_t  epilog_begin    = prolog_end + vsz * nvec;
    const vtype     vindex          = vtype{0,(int64_t)nvec,2*(int64_t)nvec,3*(int64_t)nvec};


    // (global) pointer to shared array
    // alloc only once and reuse
    // pray that this is thread safe
    typedef void (&deleter_t)(void*);
    static std::unique_ptr<uint64_t,deleter_t> shared_count
    (
        (uint64_t*)_mm_malloc(local_count_extent*tsz*nthreads, pgsz),  //align memory to page size
        _mm_free // use to free aligned memory
    );

    // pointer to each SIMD lane's range in shared memory
    uint64_t* __restrict__ private_count[vsz];
    for (uint8_t k=0; k<vsz; ++k)
      private_count[k] = shared_count.get() + local_count_extent*ithread + k*radix;

    // initialize src ptr array
    setSrcPtrs();

    // highest byte to consider
    const uint8_t max_byte = max_k > 0 ? (MyArithm::msb(max_k) / nbits) : 0;

    // for each digit
    for (uint8_t byte = 0; byte <= max_byte; ++byte)
      {

        //        Bit 7:0 of the second source operand specifies the starting bit position
        //        of bit extraction. A START value exceeding the operand size will not
        //        extract any bits from the second source operand.
        //        Bit 15:8 of the second source operand specifies the maximum number of bits
        //        (LENGTH) beginning at the START position to extract.

        const uint64_t bextr_pattern = (nbits<<8) ^ (byte*nbits);
        const __m128i sra = __m128i{int64_t(nbits)*byte,0};

        for(uint8_t k=0; k<vsz; ++k)
          for(uint64_t i=0; i<radix; ++i)
            private_count[k][i] = 0;

        // private count, divide key range among threads

        // prolog
        for(uint64_t i=begin; i<prolog_end; ++i)
          ++private_count[0][ __builtin_ia32_bextr_u64(key_[i],bextr_pattern) ];//++private_count[0][ (key_[i] >> sra) & mask ];
        // SIMD loop
        for(uint64_t i=prolog_end; i<(prolog_end+nvec); ++i)
          {
            const vtype key = _mm256_i64gather_epi64((const long long int*)&key_[i],vindex,8);
            const vtype r   = _mm256_srl_epi64(key,sra);
            for(uint8_t k=0; k<vsz; ++k) ++private_count[k][ _mm256_extract_epi8(r,k*8) ];
          }
        // epilog
        for(uint64_t i=epilog_begin; i<end; ++i)
          ++private_count[vsz-1][ __builtin_ia32_bextr_u64(key_[i],bextr_pattern) ];

#pragma omp barrier

        // compute prefix sum (begin of bins)
#pragma omp single
        {
            uint64_t sum = 0;
            uint64_t* const __restrict__ count = (uint64_t*)__builtin_assume_aligned(shared_count.get(),pgsz);
            for(uint64_t i=0; i<radix; ++i)
              {
                for(uint64_t j=0; j<nthreads; ++j)
                  {
                    for(uint64_t k=0; k<vsz; ++k)
                      {
                        const uint64_t tmp = count[i + j*local_count_extent + k*radix];
                        count[i + j*local_count_extent + k*radix] = sum;
                        sum += tmp;
                      }
                  }
              }
        }
        //implicit barrier here

        // prolog
        for (uint64_t i = begin; i < prolog_end; ++i)
          copySortToOld(i, private_count[0][ (uint64_t)__builtin_ia32_bextr_u64(key_[i],bextr_pattern) ]++);
        // SIMD loop
        for (uint64_t i = prolog_end; i < (prolog_end+ 2*(nvec/2)); i+=2)
          {
            const vtype key_1 = _mm256_i64gather_epi64((const long long int*)&key_[i],vindex,8);
            const vtype key_2 = _mm256_i64gather_epi64((const long long int*)&key_[i+1],vindex,8);
            const vtype src_1 = _mm256_i64gather_epi64((const long long int*)&src_[i],vindex,8);
            const vtype src_2 = _mm256_i64gather_epi64((const long long int*)&src_[i+1],vindex,8);
            const vtype r_1   = _mm256_srl_epi64(key_1,sra);
            const vtype r_2   = _mm256_srl_epi64(key_2,sra);

            for (uint8_t k=0; k<vsz; ++k)
              {
                uint64_t dst = private_count[k][ _mm256_extract_epi8(r_1,8*k) ]++;
                key_old_[ dst ] = key_1[k];
                src_old_[ dst ] = (double*)(src_1[k]);
              }
            for (uint8_t k=0; k<vsz; ++k)
              {
                uint64_t dst = private_count[k][ _mm256_extract_epi8(r_2,8*k) ]++;
                key_old_[ dst ] = key_2[k];
                src_old_[ dst ] = (double*)(src_2[k]);
              }
          }
        for (uint64_t i = (prolog_end+ 2*(nvec/2)); i < (prolog_end+nvec); ++i)
          {
            const vtype key = _mm256_i64gather_epi64((const long long int*)&key_[i],vindex,8);
            const vtype src = _mm256_i64gather_epi64((const long long int*)&src_[i],vindex,8);
            const vtype r   = _mm256_srl_epi64(key,sra);

            for (uint8_t k=0; k<vsz; ++k)
              {
                uint64_t dst = private_count[k][ _mm256_extract_epi8(r,8*k) ]++;
                key_old_[ dst ] = key[k];
                src_old_[ dst ] = (double*)(src[k]);
              }
          }
        // epilog
        for(uint64_t i=epilog_begin; i<end; ++i)
          copySortToOld(i, private_count[vsz-1][ (uint64_t)__builtin_ia32_bextr_u64(key_[i],bextr_pattern) ]++);



        // swap old, current
#pragma omp barrier
#pragma omp single
        swapOldCurrentSortData();

        // implicit barrier here

      } // for each digit


    // assert sorted
    //        MYASSERT(keysAreSorted(), "particle sort failed!");

#pragma omp barrier
    // index array is completely sorted, now do the permutation
    ParallelPermute(n_to_keep);


#pragma omp single
    swapOldCurrent();



  }
#endif


} // namespace particles
