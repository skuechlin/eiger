/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * RadixSortParallelAVX2.cpp
 *
 *  Created on: Jun 9, 2017
 *      Author: kustepha
 */




/*
 * RadixSortParallel.cpp
 *
 *  Created on: Jun 2, 2017
 *      Author: kustepha
 */




#include <omp.h>
#include <memory>
#include <iostream>

#include "Particles/ParticleCollection.h"

#include "Primitives/MyArithm.h"
#include "Primitives/MyError.h"
#include "Primitives/Partitioning.h"


namespace Particles
{
#if false &&  defined(__AVX2__) && defined(__BMI2__)

  void ParticleCollection::RadixSortParallelAVX2(const uint64_t max_k, const uint64_t n_to_keep)
  {

    // radix sort parameters
    constexpr uint64_t nbits = 4;
    constexpr uint64_t radix = 1ul<<nbits;

    constexpr uint64_t tsz = sizeof(uint64_t);
    constexpr uint64_t pgsz = 4096; // page size (4096 bytes)


    // round local count array size to a multiple of page size (4096 bytes)
    constexpr uint64_t local_count_extent = radix > pgsz/tsz ? radix : pgsz/tsz;


    // parallel information
    const uint64_t nthreads = omp_get_num_threads();
    const uint64_t ithread  = omp_get_thread_num();
    const uint64_t begin    = MyPartitioning::partitionStart(ithread,end_,nthreads);
    const uint64_t length   = MyPartitioning::partitionLength(ithread,end_,nthreads);

    const uint64_t local_count_offset = local_count_extent*ithread;

    // (global) pointer to shared array
    // alloc only once and reuse
    // pray that this is thread safe
    typedef void (&deleter_t)(void*);
    static std::unique_ptr<uint64_t,deleter_t> shared_count
    (
        (uint64_t*)_mm_malloc(local_count_extent*tsz*nthreads, pgsz),  //align memory to page size
        _mm_free // use to free aligned memory
    );

    // pointer to thread's range in shared memory
    uint64_t *const __restrict__ private_count = (uint64_t*)__builtin_assume_aligned(
        shared_count.get() + local_count_offset, pgsz);

    // initialize src ptr array
    setSrcPtrs();

    // highest nbits bits to consider
    const uint8_t max_digit = max_k > 0 ? nbits * (MyArithm::msb(max_k) / nbits) : 0;

    // for each digit
    for (uint8_t d = 0; d <= max_digit; d += nbits)
      {

        for(uint64_t i=0; i<radix; ++i)
          private_count[i] = 0;

        // private count, divide key range among threads
        MyIntrin::hist4bit(key_.data() + begin,length,d,private_count);


#pragma omp barrier

        // compute prefix sum (begin of bins)
#pragma omp single
        {
          uint64_t sum = 0;
          uint64_t* const __restrict__ count = (uint64_t*)__builtin_assume_aligned(shared_count.get(),pgsz);
          for(uint64_t i=0; i<radix; ++i)
            {
              for(uint64_t j=0; j<nthreads; ++j)
                {
                  const uint64_t tmp = count[i+j*local_count_extent];
                  count[i+j*local_count_extent] = sum;
                  sum += tmp;
                }
            }
        }
        //implicit barrier here

        // fill sorted array, divide key range among threads
        MyIntrin::histfun4bit(key_.data() + begin,length,d,private_count,
                              [this,begin](const uint64_t i, const uint64_t h){
          key_old_[ h ] = begin+i;
          //          copySortToOld(i+begin,h);
        },
        [this,begin](const uint64_t i, const __m256i h){
          for (uint8_t j = 0; j < 4; ++j)
            key_old_[ h[j] ] = begin+ i+j;
          //          __m256i k = _mm256_load_si256((__m256i*)(key_.data() + begin + i));
          //          __m256i s = _mm256_load_si256((__m256i*)(src_.data() + begin + i));
          //
          //          for (uint8_t j = 0; j < 4; ++j)
          //            {
          //              key_old_[ h[j] ] = k[j];
          //              src_old_[ h[j] ] = (double*)(s[j]);
          //            }
        });

        constexpr uint64_t width = 4;
        constexpr uint64_t interleave = 4;
        constexpr uint64_t stride = width * interleave;

        const uint64_t prolog = begin+std::min(
            length,
            begin%width ? width - begin%width : 0 );
        const uint64_t epilog = prolog + stride*((length-prolog)/stride);

#pragma omp barrier

        uint64_t i = begin;
        for (; i<prolog; ++i)
          {
            uint64_t idx = key_old_[i];
            key_old_[i] = key_[idx];
            src_old_[i] = src_[idx];
          }

        for (;i<epilog;i+=stride)
          {
            __m256i vidx0 = _mm256_load_si256((__m256i*)&key_old_[i+0*width]);
            __m256i vidx1 = _mm256_load_si256((__m256i*)&key_old_[i+1*width]);
            __m256i vidx2 = _mm256_load_si256((__m256i*)&key_old_[i+2*width]);
            __m256i vidx3 = _mm256_load_si256((__m256i*)&key_old_[i+3*width]);
            __m256i k0 = _mm256_i64gather_epi64((const long long int*)key_.data(),vidx0,8);
            __m256i k1 = _mm256_i64gather_epi64((const long long int*)key_.data(),vidx1,8);
            __m256i k2 = _mm256_i64gather_epi64((const long long int*)key_.data(),vidx2,8);
            __m256i k3 = _mm256_i64gather_epi64((const long long int*)key_.data(),vidx3,8);
            __m256i s0 = _mm256_i64gather_epi64((const long long int*)src_.data(),vidx0,8);
            __m256i s1 = _mm256_i64gather_epi64((const long long int*)src_.data(),vidx1,8);
            __m256i s2 = _mm256_i64gather_epi64((const long long int*)src_.data(),vidx2,8);
            __m256i s3 = _mm256_i64gather_epi64((const long long int*)src_.data(),vidx3,8);
            _mm256_stream_si256((__m256i*)&key_old_[i+0*width],k0);
            _mm256_stream_si256((__m256i*)&key_old_[i+1*width],k1);
            _mm256_stream_si256((__m256i*)&key_old_[i+2*width],k2);
            _mm256_stream_si256((__m256i*)&key_old_[i+3*width],k3);
            _mm256_stream_si256((__m256i*)&src_old_[i+0*width],s0);
            _mm256_stream_si256((__m256i*)&src_old_[i+1*width],s1);
            _mm256_stream_si256((__m256i*)&src_old_[i+2*width],s2);
            _mm256_stream_si256((__m256i*)&src_old_[i+3*width],s3);
          }

        for (; i<(begin+length); ++i)
          {
            uint64_t idx = key_old_[i];
            key_old_[i] = key_[idx];
            src_old_[i] = src_[idx];
          }



        // swap old, current
#pragma omp barrier
#pragma omp single
        swapOldCurrentSortData();

        // implicit barrier here

      } // for each digit
#pragma omp barrier


    // assert sorted
    //MYASSERT(keysAreSorted(), "particle sort failed!");


    // index array is completely sorted, now do the permutation
    ParallelPermute(n_to_keep);


#pragma omp single
    swapOldCurrent();

  }

#endif
} // namespace particles
