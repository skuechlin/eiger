/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * RadixSortParallel.cpp
 *
 *  Created on: Jun 2, 2017
 *      Author: kustepha
 */




#include <omp.h>
#include <memory>
#include <iostream>

#include "Particles/ParticleCollection.h"

#include "Primitives/MyArithm.h"
#include "Primitives/MyError.h"
#include "Primitives/Partitioning.h"
#include "Primitives/Timer.h"

#include "Primitives/MySort.h"

namespace Particles
{

  void ParticleCollection::RadixSortParallel(
      const uint64_t max_k,
      const uint64_t min_k,
      const uint64_t n_to_keep,
      MyChrono::TimerCollection& timers)
  {

    // initialize src ptr array
    timers.start("sort: set src ptrs");
    setSrcPtrs();
    timers.stop("sort: set src ptrs");


    timers.start("sort: radix sort");

    uint64_t* keys1 = key_.data();
    uint64_t* keys2 = key_old_.data();
    auto* payload1 = src_.data();
    auto* payload2 = src_old_.data();

    // decide if it is worth shifting keys
    int msb  = MyArithm::msb(max_k);
    int msbs = MyArithm::msb(max_k-min_k);
    int n_passes        = ((msb  > 10 ? msb  - 10 : 0 ) + 7)/8 + 1;
    int n_passes_shift  = ((msbs > 10 ? msbs - 10 : 0 ) + 7)/8 + 1;

    if ( (n_passes > n_passes_shift) )
      {
        MySort::RadixSortParallel(
            &keys1,&keys2,
            &payload1,&payload2,
            size(),
            max_k - min_k,
            [min_k](const uint64_t& k){ return k - min_k; }
        );
      }
    else
      {
        MySort::RadixSortParallel(
            &keys1,&keys2,
            &payload1,&payload2,
            size(),
            max_k
        );
      }


#pragma omp single
    {
      if (src_.data() != payload1)
        {
          key_.swap(key_old_);
          src_.swap(src_old_);
        }
      MYASSERT(key_.data() == keys1,"swap error in key data");
      MYASSERT(key_old_.data() == keys2,"swap error in key data");
      MYASSERT(src_.data() == payload1,"swap error in src data");
      MYASSERT(src_old_.data() == payload2,"swap error in src data");
    }

    timers.stop("sort: radix sort");

    // index array is completely sorted, now do the permutation
    timers.start("sort: permute");
    ParallelPermute(n_to_keep);
    timers.stop("sort: permute");

#pragma omp single
    swapOldCurrent();

  }

} // namespace particles
