/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * RadixSortSerial.cpp
 *
 *  Created on: Jun 2, 2017
 *      Author: kustepha
 */





#include <omp.h>
#include <memory>
#include <iostream>

#include "Particles/ParticleCollection.h"

#include "Primitives/MyArithm.h"
#include "Primitives/MyError.h"
#include "Primitives/Partitioning.h"


namespace Particles
{




  void ParticleCollection::RadixSortSerial(const uint64_t max_k, const uint64_t n_to_keep)
  {



#pragma omp single
    {

      constexpr uint64_t tsz = sizeof(uint64_t);


      // radix sort parameters
      constexpr uint64_t radix = 256;
      constexpr uint64_t nbits = 8;
      constexpr uint64_t mask = radix-1;

      // highest nbits bits to consider
      const uint64_t max_digit = max_k > 0 ? nbits * (MyArithm::msb(max_k) / nbits) : 0;



      // storage
      uint64_t* count = (uint64_t*)_mm_malloc(radix*tsz, 32);

      // set references to original data location
      setSrcPtrs();

      // for each digit
      for (uint64_t d = 0; d <= max_digit; d += nbits)
        {

          // initialize count to zero
          for(uint64_t i=0; i<radix; ++i)
            count[i] = 0;

          // count
          for(uint64_t i=0; i<end_; ++i)
            ++count[ (getKey(i) >> d) & mask ];


          // compute prefix sum (begin of bins)
          uint64_t begin = 0;
          uint64_t tmp;
          for(uint64_t i=0; i<radix; i++)
            {
              tmp = count[i];
              count[i] = begin;
              begin += tmp;
            }

          // fill sorted array
          for(uint64_t i=0; i<end_; ++i)
            copySortToOld(i, count[ (getKey(i) >> d) & mask  ]++ );

          // copy back, i.e. swap old, current
          swapOldCurrentSortData();


        } // for each digit


      _mm_free(count);

    } // single

    // now do permutation (parallel)
    // don't call this inside single!
    ParallelPermute(n_to_keep);

#pragma omp single
    swapOldCurrent();

  }


} // namespace particles
