/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * CountingSortParallel.cpp
 *
 *  Created on: Jun 2, 2017
 *      Author: kustepha
 */





#include <omp.h>
#include <memory>
#include <iostream>

#include "Particles/ParticleCollection.h"

#include "Primitives/MyArithm.h"
#include "Primitives/MyError.h"
#include "Primitives/Partitioning.h"


namespace Particles
{




  void ParticleCollection::CountingSortParallel(const uint64_t max_k, const uint64_t n_to_keep)
  {

    constexpr uint64_t tsz = sizeof(uint64_t);
    constexpr uint64_t pgsz = 4096; // page size (4096 bytes)

    const uint64_t nbins = max_k+1;

    const uint64_t local_count_extent = MyArithm::roundUp(nbins*tsz,pgsz)/tsz;  //round to a multiple of page size (4096 bytes)


    // global pointer to shared array
    static uint64_t* shared_count;


    // parallel info
    const uint64_t nthreads = omp_get_num_threads();
    const uint64_t ithread = omp_get_thread_num();

    const uint64_t local_count_offset = local_count_extent*ithread;

    // single thread allocs the shared memory region for count
#pragma omp single
    shared_count = (uint64_t*)_mm_malloc(local_count_extent*tsz*nthreads, pgsz);  //align memory to page size


    // pointer to thread's range in shared memory
    uint64_t* private_count = shared_count + local_count_offset;

    // initialize to zero
    for(uint64_t i=0; i<nbins; ++i)
      private_count[i] = 0;

    // private count, divide key range among threads
#pragma omp for schedule(static)
    for(uint64_t i=0; i<end_; ++i)
      {
        ++private_count[ getKey(i) ];
      }


    // compute prefix sum (begin of bins)
#pragma omp single
    {
      uint64_t begin = 0;
      uint64_t tmp;
      uint64_t* target;
      for(uint64_t i=0; i<nbins; i++)
        for(uint64_t t=0; t<nthreads; t++)
          {
            target = shared_count + local_count_extent*t + i;
            tmp = *target;
            *target = begin;
            begin += tmp;
          }
    }

    // fill sorted array, divide key range among threads
#pragma omp for schedule(static)
    for(uint64_t i=0; i<end_; ++i)
      {
        uint64_t dest = private_count[getKey(i)]++;
        if (dest < n_to_keep)
          {
            copyPrtToOld(i, dest);
            copySortToOld(i, dest);
          }
      }



    // free temporary memory, swap old, current (both data and sort)
#pragma omp single
    {
      _mm_free(shared_count);
      swapOldCurrent();
      swapOldCurrentSortData();
    }

  }


} // namespace particles
