/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * ParticleCollectionPermutation.cpp
 *
 *  Created on: May 24, 2017
 *      Author: kustepha
 */

#include <immintrin.h>
#include <cstdlib>
#include <cstring>
#include <omp.h>
#include "Primitives/Partitioning.h"
#include "ParticleCollection.h"




namespace Particles
{

  void ParticleCollection::ParallelPermuteKeys()
  {
#pragma omp for schedule(static)
    for (uint64_t i = 0; i < end_; ++i)
      key_old_[i] = key_[ getIndex(src_[i]) ];
  }

  // store pointer to particles in data_ array
  // to be permuted by sort routine
  void ParticleCollection::setSrcPtrs()
  {
#pragma omp for schedule(static)
    for (uint64_t i = 0; i < end_; ++i)
      src_[i] = getParticle(i);
  }

  namespace {

    __attribute__((hot,unused))
    void gather(
        Particle** __restrict__ srcptrs,
        Particle*  __restrict__ dst,
        const size_t num)
    {

      constexpr int locality = 3;
      constexpr size_t block_size = 4096;
      constexpr size_t num_per_block = block_size/sizeof(Particle);

      const size_t num_blocks = num/num_per_block;


      for(size_t i = 0; i < num_blocks; ++i) {

          // prefetch
          for (size_t j = 0; j < num_per_block; ++j)
            prefetch<0,locality>(srcptrs[j]);

          for (size_t j = 0; j < num_per_block; ++j)
            std::memcpy(
                __builtin_assume_aligned((void*)(dst+j),alignof(Particle)),
                __builtin_assume_aligned((const void*)(srcptrs[j]),alignof(Particle)),
                sizeof(Particle));

          srcptrs += num_per_block;
          dst     += num_per_block;
      }

      const size_t num_epilogue = num - num_blocks*num_per_block;

      // prefetch
      for (size_t j = 0; j < num_epilogue; ++j)
        prefetch<0,locality>(srcptrs[j]);

      for (size_t j = 0; j < num_epilogue; ++j)
        std::memcpy(
            __builtin_assume_aligned((void*)(dst+j),alignof(Particle)),
            __builtin_assume_aligned((const void*)(srcptrs[j]),alignof(Particle)),
            sizeof(Particle));
    }


    __attribute__((hot))
    void gather_reg_table(
        Particle** __restrict__ srcptrs,
        Particle*  __restrict__ dst,
        const size_t num)
    {

      constexpr int locality = 3;
      constexpr size_t block_size = 4096;

#if defined(__AVX512F__)
      constexpr uint8_t reg_size = 64;
      constexpr uint8_t num_regs = 32;
#else
      constexpr uint8_t reg_size = 32;
      constexpr uint8_t num_regs = 16;
#endif

      typedef ParticleRegTable<reg_size,num_regs> PRT;

      PRT reg_table;

      constexpr size_t num_per_table = PRT::num_parts; // number of particles per table
      constexpr size_t tables_per_block = (block_size / sizeof(Particle)) / num_per_table;
      constexpr size_t num_per_block = tables_per_block*num_per_table; // number of particles per prefetch block

      const size_t num_blocks = num / num_per_block;

      // for each block
      for (size_t block = 0; block < num_blocks; ++block)
        {
          // prefetch
          for (size_t i = 0; i < num_per_block; ++i)
            prefetch<0,locality>(srcptrs[i]);

          for (size_t k = 0; k < tables_per_block; ++k)
            {
              reg_table.gather(srcptrs + k*num_per_table);
              reg_table.store(dst + k*num_per_table);
            }

          srcptrs += num_per_block;
          dst += num_per_block;
        }

      // epilogue

      // last partial block
      const size_t num_epilogue_tables = (num - num_blocks*num_per_block)/num_per_table;

      // prefetch
      for (size_t i = 0; i < (num_epilogue_tables*num_per_table); ++i)
        prefetch<0,locality>(srcptrs[i]);

      for (size_t k = 0; k < num_epilogue_tables; ++k)
        {
          reg_table.gather(srcptrs + k*num_per_table);
          reg_table.store(dst + k*num_per_table);
        }

      srcptrs += (num_epilogue_tables*num_per_table);
      dst += (num_epilogue_tables*num_per_table);


      // last partial table
      const size_t num_epilogue = (num - num_blocks*num_per_block - num_epilogue_tables*num_per_table);

      // prefetch
      for (size_t i = 0; i < num_epilogue; ++i)
        prefetch<0,locality>(srcptrs[i]);

      for (size_t i = 0; i < num_epilogue; ++i)
        std::memcpy((void*)(dst+i),(const void*)(srcptrs[i]), sizeof(Particle));
    }


  } // namespace


  // use information in rank_ array to permute particles
  void ParticleCollection::ParallelPermute(const uint64_t n_to_keep, const bool tasks)
  {

    const uint64_t nt = omp_get_num_threads();

    // partitioning
    if (tasks)
      {
        for (uint64_t tnum = 0; tnum < nt; ++tnum)
          {

            const uint64_t begin = MyPartitioning::partitionStart(tnum,n_to_keep,nt);
            const uint64_t N  = MyPartitioning::partitionLength(tnum,n_to_keep,nt);

            // pointers to thread data range
            //            void** __restrict__ src = (void**)(src_.data() + begin);
            //            void*  __restrict__ dst = (void*)( ((char*)(data_old_.data())) + begin*sizeof(Particle) );

            if (N > 0)
              {
#pragma omp task firstprivate(begin,N)
                {
                  gather_reg_table(
                      (Particle**)(src_.data())      + begin,
                      (Particle* )(data_old_.data()) + begin,
                      N);
                  //                  gather<sizeof(Particle)>(src,dst,size);
                }
              }
          }
#pragma omp taskwait

      }
    else
      {

        const uint64_t tnum = omp_get_thread_num();
        const uint64_t begin = MyPartitioning::partitionStart(tnum,n_to_keep,nt);
        const uint64_t N  = MyPartitioning::partitionLength(tnum,n_to_keep,nt);

        if (N > 0)
          {
            //            // pointers to thread data range
            //            void** __restrict__ src = (void**)(src_.data() + begin);
            //            void*  __restrict__ dst = (void*)( ((char*)(data_old_.data())) + begin*sizeof(Particle));
            //
            //            gather<sizeof(Particle)>(src,dst,size);

            gather_reg_table(
                (Particle**)(src_.data())      + begin,
                (Particle* )(data_old_.data()) + begin,
                N);
          }
#pragma omp barrier

      }


  } // parallel permute


} // namespace Particles

