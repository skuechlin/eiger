/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * SpheresReader.cpp
 *
 *  Created on: Nov 13, 2015
 *      Author: kustepha
 */

#include <IO/SpheresReader.h>

#include <iostream>
#include <fstream>
#include <iomanip>

#include "Primitives/MyError.h"
#include "Primitives/MyArithm.h"
#include "Primitives/MyIntrin.h"

namespace SpheresReader {



	// read file and return matrix of centers and sphere radii
	// expects the following file structure (*** = arbitrary content)
	//
	// ***
	// BEGIN SPHERES N ***
	// X0 Y0 Z0 R0 ***
	// X1 Y1 Z1 R1 ***
	// ...
	// XN-1 YN-1 ZN-1 RN-1 ***
	// ***
	//
	Shapes::SphereCollection::SphereDataT
	read(
			const std::string& spheresFileName,
			const double tol,
			const Eigen::Transform<double,4,Eigen::Affine>& tr
	)
	{
		double invtol = 1./tol;

		Shapes::SphereCollection::SphereDataT spheres;


		std::string lineBuff;

		// open the file
		std::ifstream spheresFileStream(spheresFileName);



		MYASSERT(spheresFileStream.is_open(),std::string("unable to open SPHERES file \"") + spheresFileName + std::string("\""));


		// find keyword BEGIN SPHERES
		bool valid = false;
		while ( std::getline(spheresFileStream,lineBuff) )
		{
			if (lineBuff.substr(0,13).compare(std::string("BEGIN SPHERES")) == 0 )
			{
				valid = true;
				break;
			}
		}

		MYASSERT(valid,
				std::string("file \"") + spheresFileName + std::string("\" passed to SpheresReader missing keyword \"BEGIN SPHERES\""));


		// read number of spheres from same line
		uint64_t N=0;

		std::sscanf(
				lineBuff.c_str(),
				"BEGIN SPHERES %lu",
				&N );

		MYASSERT(N>0,
				std::string("file \"") + spheresFileName + std::string("\" passed to SpheresReader doesn't seem to specify N>0 spherers"));


		// allocate space
		spheres.resize(Eigen::NoChange,N);

		uint64_t n_ins = 0;
		Eigen::Vector4d Xr;


		while ( std::getline(spheresFileStream,lineBuff) )
		{
			std::sscanf(
					lineBuff.c_str(),
					"%lf %lf %lf %lf",
					&Xr.x(),&Xr.y(),&Xr.z(),&Xr.w() );

			if (tol > 0.)
			  Xr = MyIntrin::round(Xr,tol,invtol);

			spheres.col(n_ins++) = tr*Xr;

		}


		MYASSERT(n_ins==N,
				std::string("file \"")
		+ spheresFileName
		+ std::string("\" passed to SpheresReader specified ")
		+ std::to_string(N)
		+ std::string(" spheres, but found only ")
		+ std::to_string(n_ins)
		+ std::string(" in file"));


		// close the file
		spheresFileStream.close();


		return spheres;
	}


} /* namespace SpheresReader */
