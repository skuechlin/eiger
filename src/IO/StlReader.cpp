/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * StlReader.cpp
 *
 *  Created on: Apr 10, 2014
 *      Author: Stephan Kuechlin
 */

#include <iostream>
#include <fstream>
#include <list>

#include "StlReader.h"

#include "Geometry/FECollection.h"

#include "Primitives/MyError.h"
#include "Primitives/MyArithm.h"
#include "Primitives/MyIntrin.h"

namespace {

  struct Facet {

    Eigen::Vector4d A = Eigen::Vector4d::Zero();
    Eigen::Vector4d B = Eigen::Vector4d::Zero();
    Eigen::Vector4d C = Eigen::Vector4d::Zero();

    public:

    void transform(
        const double tol,
        const double invtol,
        const Eigen::Transform<double,4,Eigen::Affine>& tr,
        const bool flipNormals)
    {

      if (tol > 0.)
        {
          A = MyIntrin::round(A,tol,invtol);
          B = MyIntrin::round(B,tol,invtol);
          C = MyIntrin::round(C,tol,invtol);
        }

      if (flipNormals)
          B.swap(C);

      A = tr*A;
      B = tr*B;
      C = tr*C;

    }

  public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW

  };

  struct Solid {

    std::string name;

    std::list<Facet,Eigen::aligned_allocator<Facet>> facets;

  public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW

  };


  // METHODS
  void parse(std::list<Solid,Eigen::aligned_allocator<Solid>>& parts, const std::string& stlFileName);
  Solid parseSolid(std::stringstream& solidBuff);
  Facet parseFacet(std::stringstream& facetBuff);
  Eigen::Vector4d parseVertex(std::string vertexBuff);

}


namespace StlReader {

  // covert all facets to a list of triangles
  std::unique_ptr<Shapes::FEShapeData>
  read(
      const std::string& stlFileName,
      const double tol,
      const Eigen::Transform<double,4,Eigen::Affine>& tr,
      const bool flipNormals,
      const Eigen::AlignedBox4d& world_bb)
      {

    double invtol = 1./tol;

    std::list<Solid,Eigen::aligned_allocator<Solid>> parts;

    std::unique_ptr<Shapes::FEShapeData> shapes(new Shapes::FEShapeData(world_bb));

    // parse into parts
    parse(parts,stlFileName);

    // count
    uint64_t tot = 0;
    for(auto&& p : parts)
      tot += p.facets.size();

    shapes->reserveTris(tot);

    // convert to triangle vector
    for(auto&& p : parts)
      for(auto&& f : p.facets)
        {
          f.transform(tol,invtol,tr,flipNormals);
          shapes->addTri( f.A, f.B, f.C  );
        }

    // trigger mapping and connectivity build
    shapes->finalize();

    return shapes;

      } // read

} // namespace StlReader

namespace {

  void
  parse(
      std::list<Solid,Eigen::aligned_allocator<Solid>>& parts,
      const std::string& stlFileName)
  {

    std::string lineBuff;
    std::stringstream solidBuff;

    // open the file
    std::ifstream stlFileStream(stlFileName);


    MYASSERT(stlFileStream.is_open(),
             std::string("failed to open stl file \"") + stlFileName + std::string("\""));

    if (stlFileStream.is_open())
      {

        // check first line
        std::getline (stlFileStream,lineBuff);
        MYASSERT(
            lineBuff.substr(0,5).compare("solid") == 0,
            std::string("file \"") + stlFileName + std::string("\" passed to StlReader doesn't start with keyword \"solid\""));

        // rewind
        stlFileStream.seekg(0,stlFileStream.beg);

        while ( std::getline (stlFileStream,lineBuff) )
          {

            // write the line to the buffer of the solid
            solidBuff << lineBuff << std::endl;

            // check for end of record
            if (lineBuff.find("endsolid") != std::string::npos) {

                // reading record complete, now parse
                parts.emplace_back( parseSolid(solidBuff) );

                // reset the buffer
                solidBuff.str("");
                solidBuff.clear();
            }
          } // while getline

        // close the file
        stlFileStream.close();

      } //if is open


  }


  Solid
  parseSolid(std::stringstream& solidBuff)
  {

    Solid newSolid;

    std::string lineBuff;
    std::stringstream facetBuff;


    // first line: solid PART_151
    // ignore "solid"
    solidBuff.ignore(256,' ');

    std::getline (solidBuff,lineBuff);
    newSolid.name = lineBuff;

    //	std::cout << "parseSolid name: " << newSolid.name << std::endl;

    // count facets
    uint64_t n_facets = 0;
    while (std::getline (solidBuff,lineBuff))
      {
        // check for end of record
        if (lineBuff.find("endfacet") != std::string::npos)
          ++ n_facets;
      }

//    std::cout << "parsing " << newSolid.name << ": " << n_facets << " facets" << std::endl;

    solidBuff.clear(); // reset eof bit
    solidBuff.seekg(0,solidBuff.beg);
    std::getline (solidBuff,lineBuff); // skip first line

    while (std::getline (solidBuff,lineBuff))
      {

        // write the line to the buffer of the solid
        facetBuff << lineBuff << std::endl;

        // check for end of record
        if (lineBuff.find("endfacet") != std::string::npos)
          {
            // reading record complete, now parse
            newSolid.facets.emplace_back(  parseFacet(facetBuff) );

            // reset the buffer
            facetBuff.str("");
            facetBuff.clear();
          }

      }

//    std::cout << "... done" << std::endl;


    return newSolid;

  }


  Facet
  parseFacet(std::stringstream& facetBuff)
  {

    Facet newFacet;

    std::string lineBuff;

    std::getline(facetBuff,lineBuff);

    // first line:   facet normal -0.346464 0.937938 0.015321
//    std::sscanf(
//        lineBuff.c_str(),
//        "%*s %*s %lf %lf %lf",
//        &newFacet.N.coeffRef(0),
//        &newFacet.N.coeffRef(1),
//        &newFacet.N.coeffRef(2)
//    );
    // ignore

    // second line:    outer loop
    std::getline(facetBuff,lineBuff);

    // third, fourth, fifth line:      vertex 4351.606934 3089.444092 -525.548828

    std::getline(facetBuff,lineBuff);
    newFacet.A = parseVertex(lineBuff);

    std::getline(facetBuff,lineBuff);
    newFacet.B = parseVertex(lineBuff);

    std::getline(facetBuff,lineBuff);
    newFacet.C = parseVertex(lineBuff);


    return newFacet;

  }


  Eigen::Vector4d
  parseVertex(std::string vertexBuff)
  {

    Eigen::Vector4d res = Eigen::Vector4d::Zero();

    //      vertex 4351.606934 3089.444092 -525.548828
    std::sscanf(
        vertexBuff.c_str(),
        "%*s %lf %lf %lf",
        &res.coeffRef(0),
        &res.coeffRef(1),
        &res.coeffRef(2)
    );

    return res;
  }




}

