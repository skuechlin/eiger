/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * TecplotConsts.h
 *
 *  Created on: Dec 17, 2014
 *      Author: kustepha
 */

#ifndef IO_TECPLOTCONSTS_H_
#define IO_TECPLOTCONSTS_H_

#include <vector>
#include <cstdint>

namespace Tecplot
{

  constexpr char datarep_native[] = {"native"};

  enum struct DataFormat : int32_t
  {
    Double = 2,
        INVALID
  };

  enum struct FileType : int32_t
  {
    FullFile = 0,
        GridFile,
        SolutionFile,
        INVALID
  };

  enum struct VarLoc : int32_t
  {
    NodeCentered = 0,
        CellCentered,
        INVALID
  };

  enum struct ZoneType : int32_t
  {
    ORDERED = 0,
        FELINESEG,
        FETRIANGLE,
        FEQUADRILATERAL,
        FETETRAHEDRON,
        FEBRICK,
        FEPOLYGON,
        FEPOLYHEDRON,
        INVALID
  };

  enum FaceObscurationFlag : int32_t
  {
    PartiallyObscured = 0,
        EntirelyObscured = 1
  };

  inline
  int32_t
  n_vrts_per_elem(const ZoneType zoneType)
  {
    //          2 for LINESEGS
    //          3 for TRIANGLES
    //          4 for QUADRILATERALS
    //          4 for TETRAHEDRONS
    //          8 for BRICKS

    switch (zoneType){
      case Tecplot::ZoneType::FELINESEG:
        return 2;
        break;
      case Tecplot::ZoneType::FETRIANGLE:
        return 3;
        break;
      case Tecplot::ZoneType::FEQUADRILATERAL:
        return 4;
        break;
      case Tecplot::ZoneType::FETETRAHEDRON:
        return 4;
        break;
      case Tecplot::ZoneType::FEBRICK:
        return 8;
        break;
      default:
        return -1;
        break;
    }
  }



  enum struct IOMode : int32_t
  {
    READ = 0,
        WRITE,
        INVALID
  };

  enum struct DataSharing : int32_t
  {
    SHARED = 0,
        DISTRIBUTED,
        INVALID
  };

  enum struct CornerType : uint8_t {

    LowerLeft = 0,
        ILJL = 0,
        BottomLowerLeft = 0,
        ILJLKL = 0,

        LowerRight = 1,
        IHJL = 1,
        BottomLowerRight = 1,
        IHJLKL = 1,

        UpperRight = 2,
        IHJH = 2,
        BottomUpperRight = 2,
        IHJHKL = 2,

        UpperLeft = 3,
        ILJH = 3,
        BottomUpperLeft = 3,
        ILJHKL = 3,

        TopLowerLeft = 4,
        ILJLKH = 4,

        TopLowerRight = 5,
        IHJLKH = 5,

        TopUpperRight = 6,
        IHJHKH = 6,

        TopUpperLeft = 7,
        ILJHKH = 7
  };

  constexpr
  int32_t NO_CONNECTIVITY(const int32_t i_elem, const int32_t i_vrt)
  {
    return
    static_cast<void>(i_elem),
    static_cast<void>(i_vrt),
    -1; // void cast with comma operator for C++11 compatibility
  }

  constexpr
  int32_t NO_FACENEIGHBORS(std::vector<int32_t>& buff, const int32_t i_elem)
  {
    return
    static_cast<void>(buff),
    static_cast<void>(i_elem),
    -1; // void cast with comma operator for C++11 compatibility
  }



}

#endif /* IO_TECPLOTCONSTS_H_ */
