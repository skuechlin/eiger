/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * TecplotHeadersRead.cpp
 *
 *  Created on: Aug 10, 2016
 *      Author: kustepha
 */




#include <mpi.h>
#include <cmath> // std::isfinite

#include "TecplotHeaders.h"

#include "Primitives/MyError.h"

using namespace Tecplot;



/* ************************************************************************************************************
 *
 * ReadTecHeader
 *
 * ************************************************************************************************************ */

Headers::TecHeader
Headers::readTecHeader(
    MPI_File&       fh,
    MPI_Offset&     pos )
{
  //  char      a[8];   // magic number
  //  int32_t       b;  // number 1
  //  FileType      c;  // file type
  //  std::vector<int32_t> d;   // title
  //  int32_t       e;  // number of variables
  //  std::vector<int32_t> f;   // variable names

  int ret;
  MPI_Status status;
  int count;
  char mpiErrStr[MPI_MAX_ERROR_STRING];
  int mpiErrStrL;

  Headers::TecHeader head;



  std::string msg = "Reading Tecplot header failed: ";



  // set pointer to pos
  ret = MPI_File_seek(fh,pos,MPI_SEEK_SET);
  MPI_Error_string(ret, mpiErrStr, &mpiErrStrL);
  MYASSERT(ret == MPI_SUCCESS, msg + "mpi failed seek file position \"" + std::to_string(pos) + "\"" + std::string(mpiErrStr,mpiErrStrL));



  // read magic number
  ret = MPI_File_read(fh,
      head.a, 8, MPI_CHAR,
      &status);

  // assert
  MPI_Error_string(ret, mpiErrStr, &mpiErrStrL);
  MPI_Get_count(&status,MPI_CHAR,&count);
  MPI_File_get_position(fh, &pos);

  MYASSERT(ret == MPI_SUCCESS, msg + "mpi failed to read magic number: " + std::string(mpiErrStr,mpiErrStrL));
  MYASSERT(std::string(head.a,8).compare(std::string(Headers::MAGIC,8)) == 0, msg +
      "magic number did not compare: got \"" + std::string(head.a,8) + "\", expected \"" + std::string(Headers::MAGIC) + "\"");

  // read number 1
  ret = MPI_File_read(fh,
      &(head.b), 1, MPI_INT,
      &status);

  // assert
  MPI_Error_string(ret, mpiErrStr, &mpiErrStrL);
  MPI_Get_count(&status,MPI_INT,&count);


  MYASSERT(ret == MPI_SUCCESS, msg + "mpi failed to read \"1\" after magic number");
  MYASSERT(head.b == 1, msg +
      "int read after magic number != 1: got \"" + std::to_string(head.b) + "\"");

  // read file type
  ret = MPI_File_read(fh,
      &(head.c), 1, MPI_INT,
      &status);

  // assert
  MYASSERT(ret == MPI_SUCCESS, msg + "mpi failed to read FileType");
  MYASSERT(head.c < Tecplot::FileType::INVALID, msg +
      "invalid FileType \"" + std::to_string(static_cast<int32_t>(head.c)) + "\"!");

  // read title
  while (head.d.size() == 0 || head.d.back() != 0) // 0 terminated string
  {
    head.d.push_back(-1);
    ret = MPI_File_read(fh, &(head.d.back()), 1, MPI_INT, &status);

    // assert
    MYASSERT(ret == MPI_SUCCESS, msg + "mpi failed to read title character (int)");
    MYASSERT(head.d.back() != -1, msg + "invalid title character (int) read");
  }

  // read number of variables
  ret = MPI_File_read(fh,
      &(head.e), 1, MPI_INT,
      &status);

  // assert
  MYASSERT(ret == MPI_SUCCESS, msg + "mpi failed to read number of variable");
  MYASSERT(head.e != -1, msg + "invalid number of variables read");


  // read variable names
  int32_t vcount = 0;

  while (vcount < head.e)
  {
    head.f.push_back(-1);
    ret = MPI_File_read(fh, &(head.f.back()), 1, MPI_INT, &status);

    // assert
    MYASSERT(ret == MPI_SUCCESS, msg + "mpi failed to read variable name character (int)");
    MYASSERT(head.f.back() != -1, msg + "invalid variable name character (int) read");

    if (head.f.back() == 0) // 0 terminated string
      ++vcount;
  }



  // set position
  MPI_File_get_position(fh,&pos);


  return head;

}


/* ************************************************************************************************************
 *
 * ReadTecZoneHeader
 *
 * ************************************************************************************************************ */

Headers::TecZoneHeader
Headers::readTecZoneHeader(
    MPI_File&       fh,
    MPI_Offset&     pos,
    const int32_t&  nvars)
{
  //  float         a = ZONEMARKER;
  //  std::vector<int32_t>  b;      // zone name
  //  int32_t           c = -1;     // ParentZone
  //  int32_t           d;      // StrandID
  //  double            e;      // SolTIme
  //  int32_t           f = -1;     // unused
  //  ZoneType          g;      // ZoneType
  // // int32_t         h = 0;  // data packing = block
  //  int32_t           i = 1;      // specify var location
  //  std::vector<VarLoc>   j;      // var location: 0 = Node, 1 = Cell Centered
  //  int32_t           k = 0;      // raw local 1to1 face neighbors
  //  int32_t           l = 0;      // num face neighbors
  //  std::vector<int32_t>  m;      // if ZonType == 0: IMax, JMax, KMax else: NumPts, NumElements, 0, 0, 0
  //  int32_t           n = 0;      // no aux data
  //  float         o = EOHMARKER;

  Headers::TecZoneHeader head;

  int ret;
  MPI_Status status;
  //  int count;
  //  char mpiErrStr[MPI_MAX_ERROR_STRING];
  //  int mpiErrStrL;

  std::string msg = "Reading Tecplot zone header failed: ";


  MPI_File_seek(fh,pos,MPI_SEEK_SET);

  // read zone marker
  ret = MPI_File_read(fh,
      &(head.a), 1, MPI_FLOAT,
      &status);

  // assert
  MYASSERT(ret == MPI_SUCCESS, msg + "mpi failed to read zone marker");
  MYASSERT(head.a == Headers::ZONEMARKER, msg + "expected zone marker \"" + std::to_string(Headers::ZONEMARKER) + "\", found \"" + std::to_string(head.a) + "\"");


  // read zone name
  while (head.b.size() == 0 || head.b.back() != 0) // 0 terminated string
  {
    head.b.push_back(-1);
    ret = MPI_File_read(fh, &(head.b.back()), 1, MPI_INT, &status);

    // assert
    MYASSERT(ret == MPI_SUCCESS, msg + "mpi failed to read zone name character (int)");
    MYASSERT(head.b.back() != -1, msg + "invalid zone name character (int) read");
  }

  // read parent zone
  ret = MPI_File_read(fh,
      &(head.c), 1, MPI_INT,
      &status);

  // assert
  MYASSERT(ret == MPI_SUCCESS, msg + "mpi failed to read parent zone");
  MYASSERT(head.c == -1, msg +
      "only parent zone == -1 specification supported");

  // read strand id
  ret = MPI_File_read(fh,
      &(head.d), 1, MPI_INT,
      &status);

  // assert
  MYASSERT(ret == MPI_SUCCESS, msg + "mpi failed to read strand id");
  //  MYASSERT(head.d != -1, msg +
  //       "invalid strand id"); -1: pending assignment

  // read solution time
  ret = MPI_File_read(fh,
      &(head.e), 1, MPI_DOUBLE,
      &status);

  // assert
  MYASSERT(ret == MPI_SUCCESS, msg + "mpi failed to read solution time");
  MYASSERT(head.e >= 0, msg + "negative solution time.");


  // read unused value
  ret = MPI_File_read(fh,
      &(head.f), 1, MPI_INT,
      &status);

  // assert
  MYASSERT(ret == MPI_SUCCESS, msg + "mpi failed to read unused int after solution time");


  // read zone type
  ret = MPI_File_read(fh,
      &(head.g), 1, MPI_INT,
      &status);

  // assert
  MYASSERT(ret == MPI_SUCCESS, msg + "mpi failed to read zone type");
  MYASSERT(head.g < Tecplot::ZoneType::INVALID, msg + "invalid zone type");


  //  int32_t           i = 1;      // specify var location
  //  std::vector<VarLoc>   j;      // var location: 0 = Node, 1 = Cell Centered
  //  int32_t           k = 0;      // raw local 1to1 face neighbors
  //  int32_t           l = 0;      // num face neighbors
  //  std::vector<int32_t>  m;      // if ZonType == 0: IMax, JMax, KMax else: NumPts, NumElements, 0, 0, 0


  // read explicit variable location specified flag
  ret = MPI_File_read(fh,
      &(head.i), 1, MPI_INT,
      &status);

  // assert
  MYASSERT(ret == MPI_SUCCESS, msg + "mpi failed to read variable location specified flag");


  if ( head.i == 1)
  {

    // variable location explicitly specified
    head.j.assign(nvars,Tecplot::VarLoc::INVALID);

    // read variable locations
    for (int i = 0; i < nvars; ++i)
    {
      ret = MPI_File_read(fh, &(head.j[i]), 1, MPI_INT, &status);

      // assert
      MYASSERT(ret == MPI_SUCCESS, msg + "mpi failed to read variable location");
      MYASSERT(head.j[i] < Tecplot::VarLoc::INVALID, msg + "invalid variable location");

    }
  }
  else
  {

    // all variables located at nodes
    head.j.assign(nvars,Tecplot::VarLoc::NodeCentered);

  }


  // read raw local 1to1 face neighbors specified flag
  ret = MPI_File_read(fh,
      &(head.k), 1, MPI_INT,
      &status);

  // assert
  MYASSERT(ret == MPI_SUCCESS, msg + "mpi failed to read face neighbors specified flag");
  MYASSERT(head.k == 0, msg + "face neighbor specification not supported");



  // read number of face neighbors

  head.l.push_back(-1);
  ret = MPI_File_read(fh,
      &(head.l.back()), 1, MPI_INT,
      &status);

  // assert
  MYASSERT(ret == MPI_SUCCESS, msg + "mpi failed to read number of face neighbors");

  if ( head.l.back() > 0 )
  {
    head.l.push_back(-1);
    ret = MPI_File_read(fh,
        &(head.l.back()), 1, MPI_INT,
        &status);

    // assert
    MYASSERT(ret == MPI_SUCCESS, msg + "mpi failed to read face neighbor mode");

    if (head.g != Tecplot::ZoneType::ORDERED)
    {

      head.l.push_back(-1);
      ret = MPI_File_read(fh,
          &(head.l.back()), 1, MPI_INT,
          &status);

      // assert
      MYASSERT(ret == MPI_SUCCESS, msg + "mpi failed to read complete face neighbor specification");

    }

  }

  // read number of data points
  int32_t n = 5; // length of vector head.m
  if (head.g == Tecplot::ZoneType::ORDERED)
    n = 3;

  for (int i = 0; i < n; ++i)
  {
    head.m.push_back(-1);
    ret = MPI_File_read(fh, &(head.m.back()), 1, MPI_INT, &status);

    // assert
    MYASSERT(ret == MPI_SUCCESS, msg + "mpi failed to read number of data points");
    MYASSERT(head.m.back() != -1, msg + "invalid number of data points read");
  }

  if (head.g == Tecplot::ZoneType::ORDERED)
    MYASSERT(
        head.m[0] > 0
        && head.m[1] > 0
        && head.m[2] > 0,
        msg + "number of data points in ordered zone must be \"[IMax (>0), JMax (>0), KMax (>0)]\"");

  else
    MYASSERT(
        head.m[0] > 0
        && head.m[1] > 0
        && head.m[2] == 0
        && head.m[3] == 0
        && head.m[4] == 0,
        msg
        + std::string("number of data points in un-ordered zone must be \"[NumPts (>0), NumElements (>0), 0, 0, 0]\"")
  + std::string("\ngot ") +
  std::to_string(head.m[0]) + " " +
  std::to_string(head.m[1]) + " " +
  std::to_string(head.m[2]) + " " +
  std::to_string(head.m[3]) + " " +
  std::to_string(head.m[4]));




  //  int32_t           n = 0;      // no aux data
  //  float         o = EOHMARKER;

  // read aux data flag
  ret = MPI_File_read(fh,
      &(head.n), 1, MPI_INT,
      &status);

  // assert
  MYASSERT(ret == MPI_SUCCESS, msg + "mpi failed to read auxiliary data flag");
  MYASSERT(head.n == 0, msg + "Auxiliary data not supported");


  // read end of header marker
  ret = MPI_File_read(fh,
      &(head.o), 1, MPI_FLOAT,
      &status);

  // assert
  MYASSERT(ret == MPI_SUCCESS, msg + "mpi failed to read end of header marker");
  MYASSERT(head.o == Headers::EOHMARKER, msg + "expected end of header marker \"" + std::to_string(Headers::EOHMARKER) + "\", found \"" + std::to_string(head.a) + "\"");


  // set position
  MPI_File_get_position(fh,&pos);


  return head;

} // ReadTecZoneHeader

/* ************************************************************************************************************
 *
 * ReadTecZoneDataHeader
 *
 * ************************************************************************************************************ */

Headers::TecZoneDataHeader
Headers::readTecZoneDataHeader(
    MPI_File&       fh,
    MPI_Offset&     pos,
    const int32_t  nvars)
{
  //  float             a = ZONEMARKER;
  //  std::vector<DataFormat>   b;              // data format of each var (2=DOUBLE)
  //  int32_t           c = 0;              // no passive variables
  //  int32_t           d = 0;              // no variable sharing
  //  int32_t           e = -1;             // no zone to share with
  //  std::vector<double>   f;              // min and max for each var

  Headers::TecZoneDataHeader head;

  int ret;
  MPI_Status status;
  //  int count;
    char mpiErrStr[MPI_MAX_ERROR_STRING];
    int mpiErrStrL;

  std::string msg = "Reading Tecplot zone data header failed: ";

  MPI_File_seek(fh,pos,MPI_SEEK_SET);


  // read zone marker
  ret = MPI_File_read(fh,
      &(head.a),
      1,
      MPI_FLOAT,
      &status);

  // assert
  MPI_Error_string(ret, mpiErrStr, &mpiErrStrL);
  MYASSERT(ret == MPI_SUCCESS, msg + "mpi failed to read zone marker:\n" + std::string(mpiErrStr,mpiErrStr+mpiErrStrL));
  MYASSERT(head.a == Headers::ZONEMARKER, msg + "expected zone marker \"" + std::to_string(Headers::ZONEMARKER) + "\", found \"" + std::to_string(head.a) + "\"");



  // read data format
  for (int i = 0; i < nvars; ++i)
  {
    head.b.push_back(Tecplot::DataFormat::INVALID);
    ret = MPI_File_read(fh, &(head.b.back()), 1, MPI_INT, &status);

    // assert
    MYASSERT(ret == MPI_SUCCESS, msg + "mpi failed to data format");
    MYASSERT(head.b.back() < Tecplot::DataFormat::INVALID, msg + "invalid data format");

  }


  // read passive variables flag
  ret = MPI_File_read(fh,
      &(head.c), 1, MPI_INT,
      &status);

  // assert
  MYASSERT(ret == MPI_SUCCESS, msg + "mpi failed to read passive variables flag");
  MYASSERT(head.c == 0, msg +
      "no passive variables supported");


  // read variable sharing flag
  ret = MPI_File_read(fh,
      &(head.d), 1, MPI_INT,
      &status);

  // assert
  MYASSERT(ret == MPI_SUCCESS, msg + "mpi failed to read variable sharing flag");
  MYASSERT(head.d == 0, msg +
      "no variable sharing supported");


  // read zone sharing number
  ret = MPI_File_read(fh,
      &(head.e), 1, MPI_INT,
      &status);

  // assert
  MYASSERT(ret == MPI_SUCCESS, msg + "mpi failed to read zone sharing number");
  MYASSERT(head.e == -1, msg +
      "no zone sharing supported");


  //  std::vector<double>   f;  // min and max for each var

  // read min and max
  head.f.resize(2*nvars);
  ret = MPI_File_read(fh, &(head.f[0]), 2*nvars, MPI_DOUBLE, &status);

  // assert
  MYASSERT(ret == MPI_SUCCESS, msg + "mpi failed to read variable min/max");
  std::for_each(
      head.f.begin(),
      head.f.end(),
      [&msg](const double m)
      {
    MYASSERT(std::isfinite(m),"non-finite value for variable min/max!");
      });



  // set position
  MPI_File_get_position(fh,&pos);


  return head;

} // ReadTecZoneDataHeader

