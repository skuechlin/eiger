/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * TecplotHeaders.cpp
 *
 *  Created on: Dec 17, 2014
 *      Author: kustepha
 */

#include <mpi.h>
#include <cmath> // std::isfinite

#include "TecplotHeaders.h"

#include "Primitives/MyError.h"

using namespace Tecplot;




MPI_Datatype
Headers::TecHeader::MPIType()
{

  constexpr uint len = 6;

  MPI_Datatype type[len] = { MPI_CHAR, MPI_INT, MPI_INT, MPI_INT, MPI_INT, MPI_INT};

  int32_t blocklen[len] = {
      8,1,1,
      static_cast<int32_t>(d.size()),
      1,
      static_cast<int32_t>(f.size())
  };


  MPI_Aint disp[len];
  MPI_Aint addr[len+1];

  uint ai = 0;

  MPI_Get_address(this,&addr[ai++]);
  MPI_Get_address(&a[0],&addr[ai++]);
  MPI_Get_address(&b,&addr[ai++]);
  MPI_Get_address(&c,&addr[ai++]);
  MPI_Get_address(d.data(),&addr[ai++]);
  MPI_Get_address(&e,&addr[ai++]);
  MPI_Get_address(f.data(),&addr[ai++]);

  for (uint i = 0; i<len; ++i)
    disp[i] = addr[i+1] - addr[0];

  MPI_Datatype ret;
  MPI_Type_create_struct(len, blocklen, disp, type, &ret);
  return ret;


}




MPI_Datatype
Headers::TecZoneHeader::MPIType()
{

  constexpr uint len = 14;

  MPI_Datatype type[len] = {
      MPI_FLOAT, // a
      MPI_INT, 	// b
      MPI_INT, 	// c
      MPI_INT, 	// d
      MPI_DOUBLE,// e
      MPI_INT, 	// f
      MPI_INT, 	// g
      //MPI_INT, 	// h
      MPI_INT, 	// i
      MPI_INT, 	// j
      MPI_INT, 	// k
      MPI_INT, 	// l
      MPI_INT, 	// m
      MPI_INT, 	// n
      MPI_FLOAT	// o
  };

  int32_t blocklen[len] = {
      1,				// a
      static_cast<int32_t>(b.size()),	// b
      1,				// c
      1,				// d
      1, 				// e
      1, 				// f
      1,				// g
      //			1,	// h
      1,				// i
      static_cast<int32_t>(j.size()),	// j
      1,				// k
      static_cast<int32_t>(l.size()), 				// l
      static_cast<int32_t>(m.size()),	// m (Ordered: 3, FE: 5)
      1,				// n
      1					// o
  };

  MPI_Aint disp[len];
  MPI_Aint addr[len+1];

  uint ai = 0;

  MPI_Get_address(this,&addr[ai++]);
  MPI_Get_address(&a,&addr[ai++]);
  MPI_Get_address(b.data(),&addr[ai++]);
  MPI_Get_address(&c,&addr[ai++]);
  MPI_Get_address(&d,&addr[ai++]);
  MPI_Get_address(&e,&addr[ai++]);
  MPI_Get_address(&f,&addr[ai++]);
  MPI_Get_address(&g,&addr[ai++]);
  //		MPI_Get_address(&h,&addr[ai++]);
  MPI_Get_address(&i,&addr[ai++]);
  MPI_Get_address(j.data(),&addr[ai++]);
  MPI_Get_address(&k,&addr[ai++]);
  MPI_Get_address(l.data(),&addr[ai++]);
  MPI_Get_address(m.data(),&addr[ai++]);
  MPI_Get_address(&n,&addr[ai++]);
  MPI_Get_address(&o,&addr[ai++]);

  for (uint i = 0; i<len; ++i)
    disp[i] = addr[i+1] - addr[0];

  MPI_Datatype ret;
  MPI_Type_create_struct(len, blocklen, disp, type, &ret);
  return ret;


}


MPI_Datatype
Headers::TecZoneDataHeader::MPIType()
{

  constexpr uint len = 6;

  MPI_Datatype type[len] = {
      MPI_FLOAT, // a
      MPI_INT, 	// b
      MPI_INT, 	// c
      MPI_INT, 	// d
      MPI_INT, 	// e
      MPI_DOUBLE // f
  };

  int32_t blocklen[len] = {
      1,				// a
      static_cast<int32_t>(b.size()),	// b
      1,				// c
      1,				// d
      1, 				// e
      static_cast<int32_t>(f.size())	// f
  };

  MPI_Aint disp[len];
  MPI_Aint addr[len+1];

  uint ai = 0;

  MPI_Get_address(this,&addr[ai++]);
  MPI_Get_address(&a,&addr[ai++]);
  MPI_Get_address(b.data(),&addr[ai++]);
  MPI_Get_address(&c,&addr[ai++]);
  MPI_Get_address(&d,&addr[ai++]);
  MPI_Get_address(&e,&addr[ai++]);
  MPI_Get_address(f.data(),&addr[ai++]);

  for (uint i = 0; i<len; ++i)
    disp[i] = addr[i+1] - addr[0];

  MPI_Datatype ret;
  MPI_Type_create_struct(len,blocklen,disp,type,&ret);
  return ret;

}
