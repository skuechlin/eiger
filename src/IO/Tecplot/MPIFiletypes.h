/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * MPIFiletypes.h
 *
 *  Created on: Apr 20, 2016
 *      Author: kustepha
 */

#ifndef SRC_IO_TECPLOT_MPIFILETYPES_H_
#define SRC_IO_TECPLOT_MPIFILETYPES_H_

#include <mpi.h>
#include <iostream>
#include <cstdint>

#include "Primitives/MyError.h"

namespace Tecplot
{

  namespace MPIFiletypes
  {
    // create an mpi datatype for a 1D vector
    inline
    void
    OneD(
        const MPI_Datatype  data_type,
        const int32_t       global_size,
        const int32_t       local_size,
        const int32_t       start,
        MPI_Datatype&       file_type
    )
    {

      if (local_size == 0)
        return;

      if (local_size == global_size && start == 0)
        {

          //        int MPI_Type_contiguous(int count, MPI_Datatype oldtype,
          //            MPI_Datatype *newtype)

          MPI_Type_contiguous(
              global_size,
              data_type,
              &file_type);

        }
      else
        {

          //    int MPI_Type_create_subarray(int ndims, int array_of_sizes[], int array_of_subsizes[],
          //    int array_of_starts[], int order, MPI_Datatype oldtype, MPI_Datatype *newtype)

          //    ndims
          //        Number of array dimensions (positive integer).
          //    array_of_sizes
          //        Number of elements of type oldtype in each dimension of the full array (array of positive integers).
          //    array_of_subsizes
          //        Number of elements of type oldtype in each dimension of the subarray (array of positive integers).
          //    array_of_starts
          //        Starting coordinates of the subarray in each dimension (array of nonnegative integers).
          //    order
          //        Array storage order flag (state).
          //    oldtype
          //        Array element data type (handle).


          int err = MPI_Type_create_subarray(
              1,
              &global_size,
              &local_size,
              &start,
              MPI_ORDER_C,
              data_type,
              &file_type);

          if (err != MPI_SUCCESS)
            MYASSERT(false,"failed to create subarray");

        }

    } // OneD

  } // namespace MPIFiletypes
} // namespace Tecplot


#endif /* SRC_IO_TECPLOT_MPIFILETYPES_H_ */
