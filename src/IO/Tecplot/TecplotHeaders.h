/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * TecplotHeaders.h
 *
 *  Created on: Jul 25, 2014
 *      Author: kustepha
 */

#ifndef TECPLOTHEADERS_H_
#define TECPLOTHEADERS_H_

#include <vector>
#include <string>

#include <mpi.h>

#include "Primitives/MyString.h"

#include "TecplotConsts.h"

namespace Tecplot
{

  namespace Headers
  {

    constexpr char MAGIC[]	= {'#','!','T','D','V','1','1','2'};

    constexpr float ZONEMARKER 	= 299.0;
    constexpr float EOHMARKER 	= 357.0;

    struct BytePositions {
        MPI_Offset ZoneHeader;
        MPI_Offset ZoneDataHeader;
        MPI_Offset ZoneData;
    };


    struct TecHeader {

        char 		        a[8];	// magic number
        int32_t		        b;	    // number 1
        FileType 		    c;	    // file type
        std::vector<int32_t>    d;	// title
        int32_t 		        e;	// number of variables
        std::vector<int32_t>    f;	// variable names


        MPI_Datatype MPIType();

        TecHeader()
        : a{0,0,0,0,0,0,0,0}
        , b( -1 )
            , c( FileType::INVALID )
            , e( -1 )
            { };

        TecHeader(
            const FileType 	FileType,
            const std::string& 	dataSetName,
            const std::string&	variableNames )
        {
          std::copy(MAGIC,MAGIC+8,a);
          b = 1;
          c = FileType;
          d = MyString::str2ASCII(dataSetName,' ');
          f = MyString::str2ASCII(variableNames,' ');
          e = std::count(f.begin(),f.end(),0);
        }

    };

    struct TecZoneHeader {

        float			        a = ZONEMARKER;
        std::vector<int32_t> 	b;	        // zone name
        int32_t			        c = -1;		// ParentZone
        int32_t			        d;		    // StrandID
        double			        e;		    // SolTIme
        int32_t			        f = -1;		// unused
        ZoneType		        g;		    // ZoneType
        //	int32_t			    h = 0;	    // data packing = block
        int32_t			        i = 1;		// specify var location
        std::vector<VarLoc>	    j;		    // var location: 0 = Node, 1 = Cell Centered
        int32_t			        k = 0;		// raw local 1to1 face neighbors
        std::vector<int32_t>    l;		    // face neighbor connection info.
        //  if first entry > 0: NumMiscFaceNeighborConn, FNMode (==1), if ZoneType != 0: FNComplete (==1)
        //  else {0}
        std::vector<int32_t>	m;	        // if ZoneType == 0: IMax, JMax, KMax else: NumPts, NumElements, 0, 0, 0
        int32_t			        n = 0;		// no aux data
        float			        o = EOHMARKER;

        MPI_Datatype MPIType();

        TecZoneHeader()
        {
          d = -1;
          e = -1.;
          g = ZoneType::INVALID;
        }


        TecZoneHeader(
            const std::string& 		zoneName,
            const int32_t 		    StrandID,
            const double 			SolTime,
            const ZoneType		    ZoneType,
            const std::vector<VarLoc>& 	VarLocation,	// 0 = Node, 1 = Cell Centered
            const int32_t   NumMiscFaceNeighborConn,
            const std::vector<int32_t>&	NumPts		// if ZonType == 0: IMax, JMax, KMax else: NumPts, NumElements
        )
        : b(MyString::str2ASCII(zoneName,' '))
        , d(StrandID)
        , e(SolTime)
        , g(ZoneType)
        , j(VarLocation)
        , m(NumPts)
        {
          if (ZoneType != Tecplot::ZoneType::ORDERED)
            m.insert(m.end(),3,0);

          if (NumMiscFaceNeighborConn > 0)
            {
              if (ZoneType != Tecplot::ZoneType::ORDERED )
                {
                  l = {NumMiscFaceNeighborConn,1,1};
                }
              else
                {
                  l = {NumMiscFaceNeighborConn,1};
                }
            }
          else
            {
              l = {0};
            }




        }


    };

    struct TecZoneDataHeader
    {

        float 			a = ZONEMARKER;
        std::vector<DataFormat> 	b;				// data format of each var (2=DOUBLE)
        int32_t			c = 0;				// no passive variables
        int32_t			d = 0;				// no variable sharing
        int32_t			e = -1;				// no zone to share with
        std::vector<double>	f;				// min and max for each var


        MPI_Datatype MPIType();

        TecZoneDataHeader(){}


        TecZoneDataHeader(
            const std::vector<DataFormat>& dataFormat,
            const std::vector<double>& 	minMax
        )
        : b(dataFormat)
        , f(minMax)
        {}


    };

    BytePositions
    createFileAndWriteHeaders(
        const std::string&              fName,
        const FileType                  fileType,
        const std::string&              dataSetName,
        const std::string&              variableNames,
        const std::string&              zoneName,
        const int32_t                   StrandID,
        const double                    SolTime,
        const ZoneType                  ZoneType,
        const std::vector<VarLoc>&      VarLocation,
        const int32_t                   NumMiscFaceNeighborConn,
        const std::vector<int32_t>&     NumPts,
        const std::vector<DataFormat>&  dataFormat,
        const std::vector<double>&      minMax,
        const int32_t   id,
        const int32_t   root
        );


    int writeTecHeader(
        MPI_File&		fh,
        MPI_Offset&		pos,
        const FileType 		FileType,
        const std::string& 	dataSetName,
        const std::string&	variableNames,
        const int32_t		id,
        const int32_t		root );

    TecHeader readTecHeader(
        MPI_File&		fh,
        MPI_Offset&		pos );


    int writeTecZoneHeader(
        MPI_File& 		fh,
        MPI_Offset&		pos,
        const std::string& 	zoneName,
        const int32_t 		StrandID,
        const double 		SolTime,
        const ZoneType		ZoneType,
        const std::vector<VarLoc>& VarLocation,
        const int32_t   NumMiscFaceNeighborConn,
        const std::vector<int32_t>& NumPts,
        const int32_t		id,
        const int32_t		root );

    TecZoneHeader readTecZoneHeader(
        MPI_File& 		fh,
        MPI_Offset&		pos,
        const int32_t&		nvars );


    int writeTecZoneDataHeader(
        MPI_File& 		fh,
        MPI_Offset&		pos,
        const std::vector<DataFormat>& dataFormat,
        const std::vector<double>& minMax,
        const int32_t		id,
        const int32_t		root );

    TecZoneDataHeader readTecZoneDataHeader(
        MPI_File& 		fh,
        MPI_Offset&		pos,
        const int32_t		nvars );

  } // namespace

} // namespace Tecplot

#endif /* TECPLOTHEADERS_H_ */
