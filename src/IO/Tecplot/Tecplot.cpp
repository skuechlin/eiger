/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * Tecplot.cpp
 *
 *  Created on: Apr 20, 2016
 *      Author: kustepha
 */


#include <mpi.h>
#include <numeric> // std::iota
#include <iostream>

#include "Tecplot.h"
#include "TecplotHeaders.h"

#include "Primitives/MyError.h"
#include "Primitives/MyString.h"


namespace Tecplot
{

  std::vector<std::string>
  getVarNamesFromTecplot( const std::string& fileName )
  {

    MPI_Offset pos  = 0;                // position in file
    int32_t ret;                        // return codes

    // open the file
    MPI_File fh;
    ret = MPI_File_open(
        MPI_COMM_WORLD,
        const_cast<char*>(fileName.c_str()),
        MPI_MODE_RDONLY,
        MPI_INFO_NULL,
        &fh );


    MYASSERT(ret == MPI_SUCCESS,"failed to open data file \"" + fileName + "\"");


    // --------------------------------------------------------------------------
    // READ HEADER INFO

    // read TecHeader
    Tecplot::Headers::TecHeader  tecHeader = Tecplot::Headers::readTecHeader(fh, pos);

    // close file
    MPI_File_close(&fh);

    // get available variable names
    return MyString::split(MyString::ASCII2str(tecHeader.f,' '),' ');

  }


  namespace Detail
  {



    void
    get_vars_and_varloc_from_header(
        const bool amRoot,                                // in: for error msg/warning
        const std::string& fileName,                      // in: for error msg/warning
        const Tecplot::Headers::TecHeader& tecHeader,     // in
        const Tecplot::Headers::TecZoneHeader& tecZoneHeader, // in
        const std::vector<std::string>& varNamesReq,        // in
        const std::vector<std::string>& optVarNamesReq,     // in
        std::vector<uint64_t>& indsOfVars2Load,             // return: indices of vars in file that were requested
        std::vector<std::string>& varNamesToLoad,           // return: names of vars in file that were requested
        std::vector<Tecplot::VarLoc>& variableLocation                   // return: location (cell/node centered) of requested variables
    )
    {


      std::vector<std::string> varNamesInFile = MyString::split(MyString::ASCII2str(tecHeader.f,' '),' ');



      if ( varNamesReq.size() == 0 )
        {
          // default: load all vars
          varNamesToLoad = varNamesInFile;
          indsOfVars2Load.resize( varNamesToLoad.size() );
          std::iota(indsOfVars2Load.begin(), indsOfVars2Load.end(), 0);
        }
      else
        {
          // determine required and optional variables to load

          for (uint i = 0; i < varNamesReq.size(); ++i)
            {
              std::string varName = varNamesReq.at(i);

              auto it = std::find(varNamesInFile.begin(), varNamesInFile.end(), varName);

              MYASSERT( it != varNamesInFile.end(),
                        std::string("requested variable \"") + varName + std::string("\" not found in file \"") + fileName + std::string("\"!") );

              varNamesToLoad.emplace_back( varName );
              indsOfVars2Load.emplace_back( std::distance(varNamesInFile.begin(),it) );

            }


          // find positions of optional variables in file

          for (uint i = 0; i < optVarNamesReq.size(); ++i)
            {
              std::string varName = optVarNamesReq.at(i);

              auto it = std::find(varNamesInFile.begin(), varNamesInFile.end(), varName);

              if ( it != varNamesInFile.end() )
                {
                  // variable is in file
                  varNamesToLoad.emplace_back( varName );
                  indsOfVars2Load.emplace_back( std::distance(varNamesInFile.begin(),it) );

                }
              else
                {
                  if ( amRoot )
                    {
                      std::cout << std::endl
                          << "  warning: while reading ordered data from tecplot: optionally requested variable \""
                          << varName << "\" not found in data file " << fileName << "." << std::endl;
                    }
                }

            }

        }


      if ( tecZoneHeader.i == 0 ) // all data at nodes
        variableLocation.resize(indsOfVars2Load.size(), Tecplot::VarLoc::NodeCentered);
      else
        {
          // locations individually specified
          for (auto ind : indsOfVars2Load)
            variableLocation.emplace_back( tecZoneHeader.j.at(ind) );

        }


    } // get_vars_and_varloc_from_header



  } // namespace Detail


}
