/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * 1DData2Tecplot_Connectivity.cpp
 *
 *  Created on: Jan 5, 2017
 *      Author: kustepha
 */

#include <mpi.h>
#include <omp.h>

#include <functional>

#include "TecplotConsts.h"
#include "MPIFiletypes.h"

#include "Primitives/MyError.h"
#include "Primitives/Partitioning.h"

#include "Parallel/MyMPI.h"

namespace Tecplot
{

  template<typename CFunT>
  MPI_Offset writeConnectivity(
      const std::string& fName,
      MPI_Offset pos,
      CFunT&& connFun,
//      const std::function<int32_t(const int32_t iElem, const int32_t iVrt)>& connFun,
      const ZoneType zoneType,
      const DataSharing dataSharing,
      const int32_t nElements,
      const MyMPI::MPIMgr& mpiMgr)
  {


//    int32_t (*const* connFunPtr)(const int32_t, const int32_t) =
//        connFun.target<int32_t(*)(const int32_t, const int32_t)>();
//    if (connFunPtr && *connFunPtr  == NO_CONNECTIVITY )
//      MYASSERT(false,"writeConnectivity flag set true, but NO_CONNECTIVITY function passed");


    MPI_Comm comm = mpiMgr.comm();

    const int32_t rank      = mpiMgr.rank();
    const int32_t num_ranks = mpiMgr.nRanks();

    const uint64_t nvpe = n_vrts_per_elem(zoneType);

    int32_t global_size;
    int32_t local_size;
    int32_t start_in_global;
    uint64_t start;
    uint64_t end;

    // re-partition
    if (dataSharing == Tecplot::DataSharing::SHARED)
      {
        // data is shared, write will be partitioned to procs.
        global_size           = nElements;

        local_size            = MyPartitioning::partitionLength(rank,global_size,num_ranks);
        start_in_global       = MyPartitioning::partitionStart(rank,global_size,num_ranks);
        start                 = start_in_global;

      }
    else
      {
        local_size = nElements;

        MPI_Request req[2];

        // compute start in global
        MPI_Iexscan(&local_size,&start_in_global,1,MPI_INT32_T,MPI_SUM,comm,&req[0]);

        // compute global size
        MPI_Iallreduce(&local_size,&global_size,1,MPI_INT32_T,MPI_SUM,comm,&req[1]);

        MPI_Waitall(2,req,MPI_STATUSES_IGNORE);

        if (rank == 0)
          start_in_global = 0;

        // local start is always zero
        start = 0;
      }

    end = start + local_size;

    //      std::cout << mpiMgr.rank() << " reached " << __FILE__ << " ln: " << std::to_string(__LINE__) << std::endl;


    // create communicators for process groups with non-empty data range
    MPI_Comm writeComm;

    uint8_t empty = (local_size == 0);
    uint8_t not_empty = !empty;

    uint8_t noneempty = 1;

    MPI_Allreduce(&not_empty,&noneempty,1,MPI_UINT8_T,MPI_LAND,comm);


    if (noneempty)
      MPI_Comm_dup(comm, &writeComm);
    else
      MPI_Comm_split(comm,!empty ? 1 : MPI_UNDEFINED,0,&writeComm);

    if (writeComm != MPI_COMM_NULL)
      {

        // write buffer
        std::vector<int32_t> buffer(local_size*nvpe);

#pragma omp parallel for schedule(static)
        for (uint64_t iElem = start; iElem < end; ++iElem)
          {
            for (uint64_t iVrt = 0; iVrt < nvpe; ++iVrt)
              buffer[(iElem-start)*nvpe + iVrt] = connFun(iElem,iVrt);
          }

        MPI_Datatype file_type = MPI_DATATYPE_NULL;

        MPIFiletypes::OneD(
            MPI_INT32_T,
            global_size * nvpe,
            local_size * nvpe,
            start_in_global * nvpe,
            file_type );

        // make usable by MPI
        MPI_Type_commit(&file_type);

        // open file on communicator
        MPI_File fh;
        MPI_File_open(writeComm, const_cast<char*>(fName.c_str()), MPI_MODE_WRONLY, MPI_INFO_NULL, &fh);

        // set file view for data output

        // temporarily disable floating point exceptions,
        // since openmpi 4.0.0 triggers one in MPI_File_set_view

        int feexceptstate = fegetexcept();
        fedisableexcept(feexceptstate);

        MPI_File_set_view(fh,pos,MPI_BYTE,file_type,datarep_native,MPI_INFO_NULL);

        feenableexcept(feexceptstate);

        // write to file
        MPI_Status stat;
        int ret = MPI_File_write_all(fh, buffer.data(),local_size*nvpe, MPI_INT32_T, &stat);

        MYASSERT(ret == MPI_SUCCESS,"writing connectivity failed");

        //                    std::cout << mpiMgr.rank() << " reached " << __FILE__ << " ln: " << std::to_string(__LINE__) << std::endl;


        MPI_File_close(&fh);

        // remove file type
        MPI_Type_free(&file_type);

        MPI_Comm_free(&writeComm);

      }

    // return byte displacement
    return  pos + global_size * nvpe * sizeof(int32_t);

  }

}
