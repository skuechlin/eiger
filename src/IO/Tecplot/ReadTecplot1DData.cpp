/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * ReadTecplot1DData.cpp
 *
 *  Created on: Apr 20, 2016
 *      Author: kustepha
 */


#include <vector>
#include <string>
#include <cstdint>

#include "Tecplot.h"
#include "TecplotHeaders.h"
#include "MPIFiletypes.h"

#include "FieldData/FieldData.h"

#include "Parallel/MyMPI.h"

std::unique_ptr<FieldData>
Tecplot::readTecplot1DData(
    const std::string& fileName,
    const std::vector<std::string>& varNamesReq,
    const std::vector<std::string>& optVarNamesReq,
    const bool readConnectivity,
    const MPI_Comm& comm)
{

  if (comm == MPI_COMM_NULL)
    return NULL;

  MPI_Offset    pos = 0;                // position in file
  int32_t       ret;                    // return codes
  char mpiErrStr[MPI_MAX_ERROR_STRING];
  int mpiErrStrL;

  const bool amRoot = MyMPI::isRoot(comm,0);  // for print out of warnings

  std::string msg = std::string("Error reading 1D Data from Tecplot file \"") + fileName + std::string("\": ");

  //  uint64_t rank = MyMPI::rank(comm);
  //  std::cout << "rank: " << rank << std::endl;

  // open the file
  MPI_File fh;
  ret = MPI_File_open(
      comm,
      const_cast<char*>(fileName.c_str()),
      MPI_MODE_RDONLY,
      MPI_INFO_NULL,
      &fh );

  MYASSERT(ret == MPI_SUCCESS,"failed to open data file \"" + fileName + "\"");

  // --------------------------------------------------------------------------
  // PROCESS READ REQUEST

  //  uint64_t start = 0;

  //  uint64_t start;
  //  if (n == uint64_t(-1))
  //    {
  //      // read all data
  //      start = 0;
  //    }
  //  else
  //    {
  //      // determine start from MPI reduction
  //
  //      //      int MPI_Exscan(const void *sendbuf, void *recvbuf, int count,
  //      //          MPI_Datatype datatype, MPI_Op op, MPI_Comm comm)
  //
  //      MPI_Exscan(&n,&start,1,MPI_UINT64_T,MPI_SUM,comm);
  //      if (amRoot)
  //        start = 0;
  //    }


  // --------------------------------------------------------------------------
  // READ HEADER INFO

  // read TecHeader
  Tecplot::Headers::TecHeader tecHeader =
      Tecplot::Headers::readTecHeader(fh, pos);

  uint64_t nVarsInFile = tecHeader.e;

  // read TecZoneHeader
  Tecplot::Headers::TecZoneHeader tecZoneHeader =
      Tecplot::Headers::readTecZoneHeader(fh, pos, nVarsInFile);

  // read TecZoneDataHeader
  Tecplot::Headers::TecZoneDataHeader tecZoneDataHeader =
      Tecplot::Headers::readTecZoneDataHeader(fh, pos, nVarsInFile);

  MPI_File_close(&fh);

  double solTime = tecZoneHeader.e;

  // --------------------------------------------------------------------------
  // ASSERT DATA CONSISTENCY

  // TecZoneHeader
  //  ZoneType              g; // ZoneType
  //  std::vector<VarLoc>   j; // var location: 0 = Node, 1 = Cell Centered
  //  std::vector<int32_t>  m; // if ZonType == 0: IMax, JMax, KMax else: NumPts, NumElements, 0, 0, 0


  //  // assert all variables are cell of node centered
  //  Tecplot::VarLoc varLoc = tecZoneHeader.j[0];
  //
  //  bool allVarsHaveSameLoc = true;
  //  auto vl = tecZoneHeader.j.begin();
  //  while ( allVarsHaveSameLoc && vl != tecZoneHeader.j.end() )
  //    allVarsHaveSameLoc = *(vl++) == varLoc;
  //  MYASSERT( allVarsHaveSameLoc, msg
  //            + std::string("Tecplot Zone Header must specify all variables either cell or node centered") );


  // assert data in file not exceeded by write request

  uint64_t nDataInFile_nc;
  uint64_t nDataInFile_cc;
  uint64_t nToRead;
  uint64_t nToRead_nc;
  uint64_t nToRead_cc;
  uint64_t start_nc = 0;
  uint64_t start_cc = 0;

  if (tecZoneHeader.g == Tecplot::ZoneType::ORDERED)
  {
    MYASSERT( tecZoneHeader.m[1] == 1 && tecZoneHeader.m[2] == 1, msg
        + std::string("Tecplot Zone Header specifies an ordered zone with JMax or KMax > 0.") );

    nDataInFile_cc = tecZoneHeader.m[0] - 1;
    nDataInFile_nc = tecZoneHeader.m[0];

  }
  else
  {
    nDataInFile_cc = tecZoneHeader.m[1];
    nDataInFile_nc = tecZoneHeader.m[0];
  }

  nToRead_nc = nDataInFile_nc;
  nToRead_cc = nDataInFile_cc;

  //  if (n != uint64_t(-1))
  //    MYASSERT(nDataInFile <= start + n, msg +
  //             std::string("Request to read data from ") + std::to_string(start) +
  //             std::string(" to ") + std::to_string(start + n) +
  //             std::string(" but Tecplot Zone Header only specifies ") + std::to_string(nDataInFile) );


  //  uint64_t nDataToRead = (n == uint64_t(-1)) ? nDataInFile : n;


  // --------------------------------------------------------------------------
  // DETERMINE VARIABLES TO LOAD

  std::vector<std::string> varNamesToLoad;

  //  positions of variables to load in file
  std::vector<uint64_t> indsOfVars2Load;

  // variable location
  std::vector<Tecplot::VarLoc> variableLocation;

  // parse header info
  Detail::get_vars_and_varloc_from_header(
      amRoot,           // in: for error msg/warning
      fileName,         // in: for error msg/warning
      tecHeader,        // in
      tecZoneHeader,    // in
      varNamesReq,      // in
      optVarNamesReq,   // in
      indsOfVars2Load,  // return: indices of vars in file that were requested
      varNamesToLoad,   // return: names of vars in file that were requested
      variableLocation  // return: location (cell/node centered) of requested variables
  );

  std::vector<uint64_t> nToReadV;
  for (auto vl : variableLocation)
    nToReadV.push_back(vl == VarLoc::NodeCentered ? nToRead_nc : nToRead_cc);

  std::vector<uint64_t> nDataInFileV;
  for (auto vl : tecZoneHeader.j)
    nDataInFileV.push_back(vl == VarLoc::NodeCentered ? nDataInFile_nc : nDataInFile_cc);






  // --------------------------------------------------------------------------
  // MAKE FILETYPE


  // file type
  MPI_Datatype file_type;
  MPI_Datatype file_type_nc;
  MPI_Datatype file_type_cc;


  // create
  MPIFiletypes::OneD(MPI_DOUBLE,nDataInFile_nc,nToRead_nc,start_nc,file_type_nc);
  MPIFiletypes::OneD(MPI_DOUBLE,nDataInFile_cc,nToRead_cc,start_cc,file_type_cc);



  // create communicators for process groups with non-empty data range
  uint8_t empty_cc = (nToRead_cc == 0);
  uint8_t empty_nc = (nToRead_nc == 0);
  uint8_t notempty_cc = !empty_cc;
  uint8_t notempty_nc = !empty_nc;

  uint8_t noneempty_nc = 1;
  uint8_t noneempty_cc = 1;

  MPI_Request emptyreq[2];
  MPI_Iallreduce(&notempty_nc,&noneempty_nc,1,MPI_UINT8_T,MPI_LAND,comm,&emptyreq[0]);
  MPI_Iallreduce(&notempty_cc,&noneempty_cc,1,MPI_UINT8_T,MPI_LAND,comm,&emptyreq[1]);

//  MyMPI::allreduce(!empty_nc,noneempty_nc,comm,MPI_LAND);
//  MyMPI::allreduce(!empty_cc,noneempty_cc,comm,MPI_LAND);

  MPI_Waitall(2,emptyreq,MPI_STATUSES_IGNORE);

  MPI_Comm readComm;
  MPI_Comm readComm_nc;
  MPI_Comm readComm_cc;

  if (noneempty_nc)
    MPI_Comm_dup(comm, &readComm_nc);
  else
    MPI_Comm_split(comm,!empty_nc ? 1 : MPI_UNDEFINED,0,&readComm_nc);

  if (noneempty_cc)
    MPI_Comm_dup(comm, &readComm_cc);
  else
    MPI_Comm_split(comm,!empty_cc ? 1 : MPI_UNDEFINED,0,&readComm_cc);


  MPI_File fh_nc;
  MPI_File fh_cc;

  // only open file if this proc participating
  if ( readComm_nc != MPI_COMM_NULL )
  {
    MPI_File_open(readComm_nc, const_cast<char*>(fileName.c_str()), MPI_MODE_RDONLY, MPI_INFO_NULL, &fh_nc);
    MPI_Type_commit(&file_type_nc);
  }
  if ( readComm_cc != MPI_COMM_NULL )
  {
    MPI_File_open(readComm_cc, const_cast<char*>(fileName.c_str()), MPI_MODE_RDONLY, MPI_INFO_NULL, &fh_cc);
    MPI_Type_commit(&file_type_cc);
  }




  // --------------------------------------------------------------------------
  // READ DATA

  //  std::cout << std::endl;
  //  std::cout << "reading data, currently at pos " << pos << std::endl;

  // set file view
  //  MPI_File_set_view(
  //      fh,
  //      pos,
  //      MPI_BYTE,
  //      file_type,
  //      const_cast<char*>(std::string("native").c_str()),
  //      MPI_INFO_NULL);


  std::unique_ptr<FieldData> data(new FieldData(solTime,nToReadV,varNamesToLoad) );


  // read status
  MPI_Status stat;

  // number of values read
  int count;

  // byte offset
  int sz;
  MPI_Type_size(MPI_DOUBLE,&sz);
  MPI_Offset doffset = MPI_Offset(sz);

  MPI_File_get_byte_offset(fh,pos,&pos);

  // for each var in file
  for (uint i = 0; i < nVarsInFile; ++i)
  {
    auto it = std::find(indsOfVars2Load.begin(),indsOfVars2Load.end(),i);
    if ( it != indsOfVars2Load.end() )
    {
      // variable is nth variable to be requested
      uint n = std::distance(indsOfVars2Load.begin(),it);

      //          std::cout << "loading var " << i << " as " << n << "th var" << std::endl;
      //
      //          MPI_Offset curr_offset;
      //          MPI_File_get_position(fh, &curr_offset)  ;
      //
      //          std::cout << offset << " " << curr_offset << std::endl;



      if (variableLocation[n] == Tecplot::VarLoc::NodeCentered)
      {
        nToRead       = nToRead_nc;
        file_type     = file_type_nc;
        fh            = fh_nc;
        readComm      = readComm_nc;
      }
      else
      {
        nToRead       = nToRead_cc;
        file_type     = file_type_cc;
        fh            = fh_cc;
        readComm      = readComm_cc;
      }

      if (readComm != MPI_COMM_NULL)
      {
        //                    std::cout << mpiMgr.rank() << " reached " << __FILE__ << " ln: " << std::to_string(__LINE__) << std::endl;

        // set file view for data output
        MPI_File_set_view(
            fh,
            pos,
            MPI_BYTE,
            file_type,
            datarep_native,
            MPI_INFO_NULL);



        // read
        MPI_File_read(fh, &(data->getVarData(n)[0]) , nToRead, MPI_DOUBLE, &stat);

        // check
        MPI_Get_count(&stat,MPI_DOUBLE,&count);


        MYASSERT(
            static_cast<uint64_t>(count) == nToRead,
            std::string("data read failed: expected to read ")
        + std::to_string(nToRead) + std::string(" doubles, got ") + std::to_string(count) );


      }
    }


    pos += nDataInFileV[i] * doffset;

  }// for each var in file


  // close file
  if ( readComm_nc != MPI_COMM_NULL )
  {
    MPI_File_close(&fh_nc);
    MPI_Type_free(&file_type_nc);
    MPI_Comm_free(&readComm_nc);
  }
  if ( readComm_cc != MPI_COMM_NULL )
  {
    MPI_File_close(&fh_cc);
    MPI_Type_free(&file_type_cc);
    MPI_Comm_free(&readComm_cc);
  }


  if (readConnectivity && tecZoneHeader.g != ZoneType::ORDERED)
  {

    const uint64_t nElements = tecZoneHeader.m[1];
    const uint64_t nVertsPerElement = n_vrts_per_elem(tecZoneHeader.g);
    const uint64_t nToRead = nElements*nVertsPerElement;

    data->setConnectivity(nElements,nVertsPerElement);

    MPI_File_open(comm, const_cast<char*>(fileName.c_str()), MPI_MODE_RDONLY, MPI_INFO_NULL, &fh);

    int ret = MPI_File_read_at(fh,pos,data->getConnectivity().data(),nToRead,MPI_INT32_T,&stat);

    MPI_Error_string(ret, mpiErrStr, &mpiErrStrL);
    MYASSERT(ret == MPI_SUCCESS,std::string(mpiErrStr,mpiErrStr+mpiErrStrL));

    int count;
    MPI_Get_count(&stat,MPI_INT32_T,&count);

    MYASSERT(count == int(nToRead),"tried reading" + std::to_string(nToRead) + ", got " + std::to_string(count));

    MPI_File_close(&fh);

  }



  return data;



}
