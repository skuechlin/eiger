/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * TecplotHeadersWrite.cpp
 *
 *  Created on: Aug 10, 2016
 *      Author: kustepha
 */

#include <mpi.h>
#include <cmath> // std::isfinite

#include "TecplotHeaders.h"

#include "Primitives/MyError.h"

using namespace Tecplot;

Headers::BytePositions
Headers::createFileAndWriteHeaders(
    const std::string&              fName,
    const FileType                  fileType,
    const std::string&              dataSetName,
    const std::string&              variableNames,
    const std::string&              zoneName,
    const int32_t                   StrandID,
    const double                    SolTime,
    const ZoneType                  ZoneType,
    const std::vector<VarLoc>&      VarLocation,
    const int32_t                   NumMiscFaceNeighborConn,
    const std::vector<int32_t>&     NumPts,
    const std::vector<DataFormat>&  dataFormat,
    const std::vector<double>&      minMax,
    const int32_t   id,
    const int32_t   root
    )
{

  MPI_Offset  tot_bytes_written = 0;        // total number of bytes written to file by all procs
  Headers::BytePositions positions;

  // open the file, delete previous copy
  MPI_File fh;
  if (id == root)
    { // only root will write

       MPI_File_open(MPI_COMM_SELF, const_cast<char*>(fName.c_str()), MPI_MODE_CREATE | MPI_MODE_WRONLY, MPI_INFO_NULL, &fh);
       MPI_File_close(&fh);

       MPI_File_delete(const_cast<char*>(fName.c_str()),MPI_INFO_NULL);
       MPI_File_open(MPI_COMM_SELF, const_cast<char*>(fName.c_str()), MPI_MODE_CREATE | MPI_MODE_WRONLY, MPI_INFO_NULL, &fh);
    }

  Headers::writeTecHeader(fh,tot_bytes_written,fileType,dataSetName,variableNames,id,root );

  positions.ZoneHeader = tot_bytes_written;

  Headers::writeTecZoneHeader(fh,tot_bytes_written,zoneName,StrandID,SolTime,ZoneType,
                              VarLocation,NumMiscFaceNeighborConn,NumPts,id,root );

  positions.ZoneDataHeader = tot_bytes_written;

  Headers::writeTecZoneDataHeader(fh,tot_bytes_written,dataFormat,minMax,id,root);

  positions.ZoneData = tot_bytes_written;

  if (id == root)
    MPI_File_close(&fh);

  return positions;


}


/* ************************************************************************************************************
 *
 * WriteTecHeader
 *
 * ************************************************************************************************************ */

int
Headers::writeTecHeader(
    MPI_File&       fh,
    MPI_Offset&     pos,
    const FileType      fileType,
    const std::string&  dataSetName,
    const std::string&  variableNames,
    const int32_t   id,
    const int32_t   root
)
{

  TecHeader tecHD(fileType,dataSetName,variableNames);

  MPI_Datatype TecHeaderDataType = tecHD.MPIType();
  MPI_Type_commit(&TecHeaderDataType);

  // now write
  int ret = MPI_SUCCESS;


  if (id == root) {
      MPI_Status status;
      ret = MPI_File_write_at(fh,pos,&tecHD,1,TecHeaderDataType, &status);

      int nwrites = 0;
      MPI_Get_count(&status,TecHeaderDataType,&nwrites);
      if (nwrites != 1)
        ret = MPI_ERR_IO;
  }

  int sz;
  MPI_Type_size(TecHeaderDataType,&sz);
  pos += sz;

  MPI_Type_free(&TecHeaderDataType);


  return ret;

} // WriteTecHeader


/* ************************************************************************************************************
 *
 * WriteTecZoneHeader
 *
 * ************************************************************************************************************ */

int
Headers::writeTecZoneHeader(
    MPI_File&       fh,
    MPI_Offset&     pos,
    const std::string&  zoneName,
    const int32_t   StrandID,
    const double    SolTime,
    const ZoneType  ZoneType,
    const std::vector<VarLoc>& VarLocation,
    const int32_t   NumMiscFaceNeighborConn,
    const std::vector<int32_t>& NumPts,
    const int32_t   id,
    const int32_t   root
)
{


  switch (ZoneType)
  {
    case Tecplot::ZoneType::ORDERED:
      MYASSERT(NumPts.size() == 3,"Ordered zone needs Number of points specified as Imax, Jmax, Kmax");
      break;
    case Tecplot::ZoneType::FELINESEG:
      MYASSERT(NumPts.size() == 2,"FE zone needs Number of points specified as NumPts, NumElements");
      break;
    case Tecplot::ZoneType::FETRIANGLE:
      MYASSERT(NumPts.size() == 2,"FE zone needs Number of points specified as NumPts, NumElements");
      break;
    case Tecplot::ZoneType::FEQUADRILATERAL:
      MYASSERT(NumPts.size() == 2,"FE zone needs Number of points specified as NumPts, NumElements");
      break;
    case Tecplot::ZoneType::FEBRICK:
      MYASSERT(NumPts.size() == 2,"FE zone needs Number of points specified as NumPts, NumElements");
      break;
    default:
      MYASSERT(false,std::string("unknown ZoneType ") + std::to_string(static_cast<int32_t>(ZoneType)));
      break;
  }


  TecZoneHeader tzh(zoneName,StrandID,SolTime,ZoneType,VarLocation,NumMiscFaceNeighborConn,NumPts);
  MPI_Datatype TecZoneHeaderType = tzh.MPIType();
  MPI_Type_commit(&TecZoneHeaderType);



  // now write
  int ret = MPI_SUCCESS;

  if (id == root) {
      MPI_Status status;
      ret = MPI_File_write_at(fh,pos,&tzh,1,TecZoneHeaderType, &status);

      int nwrites = 0;
      MPI_Get_count(&status,TecZoneHeaderType,&nwrites);
      if (nwrites != 1)
        ret = MPI_ERR_IO;

  }


  int sz;
  MPI_Type_size(TecZoneHeaderType,&sz);
  pos += sz;

  MPI_Type_free(&TecZoneHeaderType);

  return ret;

} //WriteTecZoneHeader

/* ************************************************************************************************************
 *
 * WriteTecZoneDataHeader
 *
 * ************************************************************************************************************ */

int
Headers::writeTecZoneDataHeader(
    MPI_File&       fh,
    MPI_Offset&     pos,
    const std::vector<DataFormat>&  dataFormat,
    const std::vector<double>&      minMax,
    const int32_t   id,
    const int32_t   root
)
{
  MYASSERT(minMax.size() == 2*dataFormat.size(),
           "vector \"minMax\" must have twice the size of vector \"dataFormat\" in input to \"WriteTecZoneDataHeader\"");


  TecZoneDataHeader tecZDH(dataFormat,minMax);
  MPI_Datatype TecZoneDataHeaderType = tecZDH.MPIType();
  MPI_Type_commit(&TecZoneDataHeaderType);


  // now write
  int ret = MPI_SUCCESS;

  if (id == root) {
      MPI_Status status;
      ret = MPI_File_write_at(fh,pos,&tecZDH,1,TecZoneDataHeaderType, &status);

      int nwrites = 0;
      MPI_Get_count(&status,TecZoneDataHeaderType,&nwrites);
      if (nwrites != 1)
        ret = MPI_ERR_IO;
  }

  int sz;
  MPI_Type_size(TecZoneDataHeaderType,&sz);
  pos += sz;

  MPI_Type_free(&TecZoneDataHeaderType);

  return ret;


} // WriteTecZoneDataHeader

