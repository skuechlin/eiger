/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * 1DData2Tecplot_FNConnectivity.cpp
 *
 *  Created on: Jan 5, 2017
 *      Author: kustepha
 */

#include <mpi.h>
#include <omp.h>

#include <functional>

#include "TecplotConsts.h"
#include "MPIFiletypes.h"

#include "Primitives/MyError.h"
#include "Primitives/Partitioning.h"

#include "Parallel/MyMPI.h"

namespace Tecplot
{

  template<typename FNCFunT>
  int32_t writeFaceNeigborConnectivity(
      const std::string& fName,
      MPI_Offset pos,
      FNCFunT&& fnConnFun,
//      const std::function<int32_t(std::vector<int32_t>& buffer, const int32_t iElem)>& fnConnFun,
      const DataSharing dataSharing,
      const int32_t nElements,
      const MyMPI::MPIMgr& mpiMgr)
  {

    //            std::cout << rank << " reached " << std::to_string(__LINE__) << std::endl;

//    int32_t (*const* fnConnFunPtr)(std::vector<int32_t>&, const int32_t) =
//        fnConnFun.target<int32_t(*)(std::vector<int32_t>&, const int32_t)>();
//    if (fnConnFunPtr && *fnConnFunPtr  == NO_FACENEIGHBORS )
//      MYASSERT(false,"writeFaceNeighborConnectivity flag set true, but NO_FACENEIGHBORS function passed");

    MPI_Comm comm = mpiMgr.comm();

    int32_t     rank      = mpiMgr.rank();
    int32_t     num_ranks = mpiMgr.nRanks();

    int ret; // MPI error codes

    // re-partition
    int32_t local_size;
    int32_t start;

    if (dataSharing == Tecplot::DataSharing::SHARED)
      {
        // data is shared, write will be partitioned to procs.

        local_size  = MyPartitioning::partitionLength(rank,nElements,num_ranks);
        start       = MyPartitioning::partitionStart(rank,nElements,num_ranks);
      }
    else
      {
        local_size = nElements;

        // local start is always zero
        start = 0;
      }

    // we can't construct file type yet, since we don't know total size

    // write buffer
    int32_t nFaceNeighborConn = 0;
    int32_t totBuffSize = 0;
    std::vector<int32_t> buffer;
    std::vector<std::vector<int32_t>> tlbuffers;

#pragma omp parallel reduction(+:nFaceNeighborConn,totBuffSize)
    {
#pragma omp single
      tlbuffers.resize(omp_get_num_threads());

      const int32_t  tstart = start + MyPartitioning::partitionStart(
          omp_get_thread_num(),local_size,omp_get_num_threads());

      const int32_t  tend = tstart + MyPartitioning::partitionLength(
                omp_get_thread_num(),local_size,omp_get_num_threads());

      std::vector<int32_t>& tlbuffer = tlbuffers[omp_get_thread_num()];
      tlbuffer.reserve( 8*(tend - tstart)  );

#pragma omp for schedule(static)
    for (int32_t iElem = tstart; iElem < tend; ++iElem)
      nFaceNeighborConn += fnConnFun(tlbuffer,iElem);

    totBuffSize += tlbuffer.size();

    } // parallel region

    // splice buffers
    buffer.reserve(totBuffSize);
    for (uint i = 0; i < tlbuffers.size(); ++i)
      {
        buffer.insert(buffer.end(),tlbuffers[i].begin(),tlbuffers[i].end());
        tlbuffers[i] = {};
      }

    tlbuffers = {};

    // new local size
    local_size = buffer.size();

    MPI_Request req[4];

    // obtain total number of FaceNeigborConnections written
    int32_t totNFaceNeighborConn = 0;
    MPI_Iallreduce(&nFaceNeighborConn,&totNFaceNeighborConn,1,MPI_INT32_T,MPI_SUM,comm,&req[0]);

    // compute file type info
    int32_t global_size = 0;
    MPI_Iallreduce(&local_size,&global_size,1,MPI_INT32_T,MPI_SUM,comm,&req[1]);

    int32_t start_in_global = 0;
    MPI_Iexscan(&local_size,&start_in_global,1,MPI_INT32_T,MPI_SUM,comm,&req[2]);

    uint8_t empty = buffer.empty(); // do not read from local_size while it is used as buffer while waiting for completion
    uint8_t notempty = !empty;
    uint8_t noneempty = 1;
    MPI_Iallreduce(&notempty,&noneempty,1,MPI_UINT8_T,MPI_LAND,comm,&req[3]);

    MPI_Waitall(4,req,MPI_STATUSES_IGNORE);

    if (rank == 0)
      start_in_global = 0;

    // create communicators for process groups with non-empty data range
    MPI_Comm writeComm;
    if (noneempty)
      MPI_Comm_dup(comm, &writeComm);
    else
      MPI_Comm_split(comm,!empty ? 1 : MPI_UNDEFINED,0,&writeComm);

    if (writeComm != MPI_COMM_NULL)
      {

        // now create file type

        // create new filetype
        MPI_Datatype file_type;
        MPIFiletypes::OneD
        (
            MPI_INT32_T,
            global_size,
            local_size ,
            start_in_global,
            file_type
        );

//        std::cout << rank << " reached " << __FILE__ << " ln: " << std::to_string(__LINE__) << std::endl;

        // make usable by MPI
        ret = MPI_Type_commit(&file_type);
        MYASSERT(ret == MPI_SUCCESS,
                 std::string("error committing file type on rank ") + std::to_string(rank) );

//        std::cout << rank << " reached " << __FILE__ << " ln: " << std::to_string(__LINE__) << std::endl;

        // open file on communicator
        MPI_File fh;
        ret = MPI_File_open(writeComm, const_cast<char*>(fName.c_str()), MPI_MODE_WRONLY, MPI_INFO_NULL, &fh);
        MYASSERT(ret == MPI_SUCCESS,
                 std::string("error opening file on rank ") + std::to_string(rank) );

//        std::cout << rank << " reached " << __FILE__ << " ln: " << std::to_string(__LINE__) << std::endl;

        // set file view for data output
        // temporarily disable floating point exceptions,
        // since openmpi 4.0.0 triggers one in MPI_File_set_view

        int feexceptstate = fegetexcept();
        fedisableexcept(feexceptstate);
        ret = MPI_File_set_view(fh,pos,MPI_BYTE,file_type,datarep_native,MPI_INFO_NULL);
        MYASSERT(ret == MPI_SUCCESS,
                 std::string("error setting file view on rank ") + std::to_string(rank) );

        feenableexcept(feexceptstate);

//        std::cout << rank << " reached " << __FILE__ << " ln: " << std::to_string(__LINE__)
//        << " pos: " << pos
//        << " local size: " << local_size
//        << " global size: " << global_size
//        << " start_in_global: " << start_in_global
//        << " totNFaceNeighborConn: " << totNFaceNeighborConn
//        << " begin and back of buffer: " << *iBuff.begin() << " " << iBuff.back()
//        << std::endl;


        // write to file
        MPI_Status stat;
        ret = MPI_File_write_all(fh, buffer.data(),local_size, MPI_INT32_T, &stat);
   //     ret = MPI_File_write(fh, iBuff.data(),local_size, MPI_INT32_T, &stat);

        MYASSERT(ret == MPI_SUCCESS,"writing face neighbor connectivity failed");

//        std::cout << rank << " reached " << __FILE__ << " ln: " << std::to_string(__LINE__) << std::endl;


        MPI_File_close(&fh);

        // remove filetype
        MPI_Type_free(&file_type);

        MPI_Comm_free(&writeComm);

      }



    // update byte displacement
//    MPI_Offset tot_bytes_written = pos + global_size * sizeof(int32_t);

//    std::cout << rank << " reached " << __FILE__ << " ln: " << std::to_string(__LINE__)
//    << " tot_bytes_written: " << tot_bytes_written
//    << std::endl;


    return totNFaceNeighborConn;

  }


} // namespace Tecplot
