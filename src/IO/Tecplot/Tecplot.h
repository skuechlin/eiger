/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * Tecplot.h
 *
 *  Created on: Dec 17, 2014
 *      Author: kustepha
 */

#ifndef IO_TECPLOT_H_
#define IO_TECPLOT_H_

#include <mpi.h>
#include <vector>
#include <memory>
#include <functional>

#include "Primitives/MyError.h"

#include "TecplotConsts.h"


namespace MyMPI
{
  class MPIMgr;
}

class FieldData;

namespace Tecplot
{
  namespace Headers
  {
    struct TecHeader;
    struct TecZoneHeader;
  }
}

namespace Tecplot
{

  template<typename DFunT, typename CFunT, typename FNCFunT>
  void write1DData2Tecplot(
      DFunT&& dataFun,
      CFunT&& connFun,
      FNCFunT&& fnConnFun,
//      const std::function<double(const int32_t iDat, const int32_t iVar)>& dataFun,
//      const std::function<int32_t(const int32_t iElem, const int32_t iVrt)>& connFun,
//      const std::function<int32_t(std::vector<int32_t>& buffer, const int32_t iElem)>& fnConnFun,
      const ZoneType zoneType,
      const std::vector<VarLoc>&  varLoc,
      const DataSharing dataSharing,
      const FileType fileType,
      const int32_t nVars,
      const int32_t nPoints,
      const int32_t nElements,
      const std::string& fileName,
      const std::string& dataSetName,
      const std::string& zoneName,
      const std::string& varNames,
      const int32_t data_id,
      const double solTime,
      const bool writeConnectivity,
      const bool writeFaceNeighborConnectivity,
      const MyMPI::MPIMgr& mpiMgr );

  std::unique_ptr<FieldData>
  readTecplot1DData(
      const std::string& fileName,
      const std::vector<std::string>& varNamesReq = {},
      const std::vector<std::string>& optVarNamesReq = {},
      const bool readConnectivity = false,
      const MPI_Comm& comm = MPI_COMM_WORLD);


  std::vector<std::string>
  getVarNamesFromTecplot(
      const std::string& fileName);

  // converts tecplot vertex index to bitmask
  static constexpr uint8_t ivrt2kji(const uint8_t iVrt) {
    constexpr uint8_t lut[] = {0, 1, 3, 2, 4, 5, 7, 6};
    return lut[iVrt]; }

  // convert face index 2 tecplot face index
  static constexpr uint8_t face2iface(const uint8_t face, uint8_t ndims) {
    constexpr uint8_t inv(-1); // invalid

    constexpr uint8_t lut[] =
    {// 0,  1,   2,   3,   4,   5,
        0,  1, inv, inv, inv, inv, inv, inv,     // 1d
        3,  1,   0,   2, inv, inv, inv, inv,     // 2d
        0,  1,   2,   3,   4,   5, inv, inv     // 3d
    };
    return lut[(ndims-1)*8 + face];  }


  // convert direction bitmask to tecplot face index
  static constexpr uint8_t skji2iface(const uint8_t dir_skji, uint8_t ndims)
  {
    constexpr uint8_t inv(-1); // invalid

    constexpr uint8_t lut[] =
    { //  0,   1,   2,   3,   4,   5,   6,   7,   8,   9,  10,  11,  12
        //      +i   +j        +k                       -i   -j        -k
        inv,  0, inv, inv, inv, inv, inv, inv, inv,   1, inv, inv, inv,    // 1d
        inv,  1,   2, inv, inv, inv, inv, inv, inv,   3,   0, inv, inv,    // 2d
        inv,  1,   3, inv,   5, inv, inv, inv, inv,   0,   2, inv,   4     // 3d
    };

    ndims &= uint8_t(3); // ensure < 4

    uint8_t mask( uint8_t(8) | (uint8_t(7) >> (uint8_t(3)-ndims)) );
    // 3d: mask = 1111 2d: mask = 1011 1d: mask = 1001

    return lut[  (ndims-1)*13 + (dir_skji & mask)  ];
  }


  namespace Detail
  {

    void
    get_vars_and_varloc_from_header(
        const bool amRoot,                                // in: for error msg/warning
        const std::string& fileName,                      // in: for error msg/warning
        const Tecplot::Headers::TecHeader& tecHeader,     // in
        const Tecplot::Headers::TecZoneHeader& tecZoneHeader, // in
        const std::vector<std::string>& varNamesReq,        // in
        const std::vector<std::string>& optVarNamesReq,     // in
        std::vector<uint64_t>& indsOfVars2Load,             // return: indices of vars in file that were requested
        std::vector<std::string>& varNamesToLoad,           // return: names of vars in file that were requested
        std::vector<Tecplot::VarLoc>& variableLocation                   // return: location (cell/node centered) of requested variables
    );

  }


} // namespace Tecplot

#include "1DData2Tecplot.hpp"


#endif /* IO_TECPLOT_H_ */
