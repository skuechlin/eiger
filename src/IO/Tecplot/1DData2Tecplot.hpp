/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * 1DData2Tecplot.cpp
 *
 *  Created on: Oct 17, 2014
 *      Author: kustepha
 */

#include <string>

#include <fenv.h>

#include "Tecplot.h"
#include "MPIFiletypes.h"

#include "TecplotHeaders.h"

#include "Primitives/MyError.h"
#include "Primitives/MyArithm.h"
#include "Primitives/Partitioning.h"
#include "Primitives/MyString.h"
#include "Primitives/MyMemory.h"
#include "Primitives/MyIntrin.h"

#include "Parallel/MyMPI.h"



using namespace Tecplot;

namespace Tecplot
{
  template<typename CFunT>
  MPI_Offset writeConnectivity(
      const std::string& fName,
      MPI_Offset pos,
      CFunT&& connFun,
//      const std::function<int32_t(const int32_t iElem, const int32_t iVrt)>& connFun,
      const ZoneType zoneType,
      const DataSharing dataSharing,
      const int32_t nElements,
      const MyMPI::MPIMgr& mpiMgr);

  template<typename FNCFunT>
  int32_t writeFaceNeigborConnectivity(
      const std::string& fName,
      MPI_Offset pos,
      FNCFunT&& fnConnFun,
//      const std::function<int32_t(std::vector<int32_t>& buffer, const int32_t iElem)>& fnConnFun,
      const DataSharing dataSharing,
      const int32_t nElements,
      const MyMPI::MPIMgr& mpiMgr);
}


template<typename DFunT, typename CFunT, typename FNCFunT>
void Tecplot::write1DData2Tecplot(
    DFunT&& dataFun,
    CFunT&& connFun,
    FNCFunT&& fnConnFun,
    const ZoneType zoneType,
    const std::vector<VarLoc>&  varLoc,
    const DataSharing dataSharing,
    const FileType fileType,
    const int32_t nVars,
    const int32_t nPoints,
    const int32_t nElements,
    const std::string& fileName,
    const std::string& dataSetName,
    const std::string& zoneName,
    const std::string& varNames,
    const int32_t data_id,
    const double solTime,
    const bool writeConnectivity_tf,
    const bool writeFaceNeighborConnectivity_tf,
    const MyMPI::MPIMgr& mpiMgr )
{

  // temporarily disable floating point exceptions,
  // since openmpi 4.0.0 triggers one in MPI_File_set_view

  const int feexceptstate = fedisableexcept(FE_ALL_EXCEPT);

  // create new communicator of processes that will participate in writing the file
  // i.e. those are not empty
  // only split the existing comm if there are empty procs, since split destroys topology info
  // which is needed for grid output, and in case of grid output, we are guaranteed non empty topologies

  // DEBUG
  //      std::cout << "write1DData2Tecplot called with "
  //          << "fileName "
  //          << fileName
  //          << " nVars " << nVars
  //          << " nElements " << nElements << " nPoints " << nPoints << " data sharing " << std::to_string(static_cast<uint32_t>(dataSharing))
  //          << std::endl;


  MYASSERT(zoneType != Tecplot::ZoneType::FEPOLYGON && zoneType != Tecplot::ZoneType::FEPOLYHEDRON,
           "output of FEPOLYGON / FEPOLYHEDRON zones not implemented");


  // --------------------------------------------------------------------------------//
  // MPI info

  MPI_Comm comm = mpiMgr.comm();

  int32_t     rank      = mpiMgr.rank();
  int32_t     num_ranks = mpiMgr.nRanks();
  int32_t     root      = mpiMgr.root();


  MYASSERT(nVars > 0, "tried to output 1d data, but number of variables less than 1!");
  MYASSERT(MyString::split(varNames,' ').size() == static_cast<uint32_t>(nVars),"number of variable names, number of variables mismatch!");


  int32_t     ret	= MPI_SUCCESS;		// return codes

  std::string fName(fileName + std::string(".plt")); // append tecplot ext

  std::vector<int32_t> global_numPts; // FE Zones: {nPoints, nElements} Ordered: {IMax, JMax, KMax}

  std::vector<int32_t> local_numPts = {nPoints, nElements};

  if (dataSharing == Tecplot::DataSharing::SHARED)
    {
      if ( zoneType != Tecplot::ZoneType::ORDERED )
        global_numPts = local_numPts;
      else
        global_numPts = {nPoints,1,1};
    }
  else
    {
      MPI_Request numptsreq;
      if ( zoneType != Tecplot::ZoneType::ORDERED )
        {
          global_numPts = {0,0};
          MPI_Iallreduce(local_numPts.data(),global_numPts.data(),2,MPI_INT32_T,MPI_SUM,comm,&numptsreq);
        }
      else
        {
          global_numPts = {0,1,1};
          MPI_Iallreduce(&nPoints,global_numPts.data(),1,MPI_INT32_T,MPI_SUM,comm,&numptsreq);
        }
      MPI_Wait(&numptsreq,MPI_STATUS_IGNORE);
    }

  // --------------------------------------------------------------------------------//
  // header info

  int32_t StrandID = (fileType == FileType::GridFile) ? 0 : data_id + 1;
  double solution_time_to_write = solTime;



  //  std::vector<VarLoc> 		valueLocation(nVars,varLoc);
  std::vector<DataFormat> 	dataFormat(nVars,Tecplot::DataFormat::Double);
  std::vector<double>		minMax( 2*nVars, 0.0 );		// unknown

  //      CreateFileAndWriteHeaders(
  //          const std::string&              fName,
  //          const FileType                  fileType,
  //          const std::string&              dataSetName,
  //          const std::string&              variableNames,
  //          const std::string&              zoneName,
  //          const int32_t                   StrandID,
  //          const double                    SolTime,
  //          const ZoneType                  ZoneType,
  //          const std::vector<VarLoc>&      VarLocation,
  //          const int32_t                   NumMiscFaceNeighborConn,
  //          const std::vector<int32_t>&     NumPts,
  //          const std::vector<DataFormat>&  dataFormat,
  //          const std::vector<double>&      minMax,
  //          const int32_t   id,
  //          const int32_t   root
  //          );

  Headers::BytePositions pos =
      Headers::createFileAndWriteHeaders(
          fName,
          fileType,
          dataSetName,
          varNames,
          zoneName,
          StrandID,
          solution_time_to_write,
          zoneType,
          varLoc,
          (writeFaceNeighborConnectivity_tf ? 1 : 0),
          global_numPts,
          dataFormat,
          minMax,
          rank,
          root);


  // Data partitioning and file types
  int32_t global_size;
  int32_t local_size;
  int32_t start;

  // node centered variables
  int32_t global_size_nc;
  int32_t local_size_nc;
  int32_t start_in_global_nc;
  int32_t start_nc;

  // cell centered variables
  int32_t global_size_cc;
  int32_t local_size_cc;
  int32_t start_in_global_cc;
  int32_t start_cc;



  if (dataSharing == Tecplot::DataSharing::DISTRIBUTED)
    {

      local_size_cc = nElements;
      local_size_nc = nPoints;

      MPI_Request req[4];

      MPI_Iexscan(&local_size_cc,&start_in_global_cc,1,MPI_INT32_T,MPI_SUM,comm,&req[0]);
      MPI_Iexscan(&local_size_nc,&start_in_global_nc,1,MPI_INT32_T,MPI_SUM,comm,&req[1]);

      MPI_Iallreduce(&local_size_cc,&global_size_cc,1,MPI_INT32_T,MPI_SUM,comm,&req[2]);
      MPI_Iallreduce(&local_size_nc,&global_size_nc,1,MPI_INT32_T,MPI_SUM,comm,&req[3]);

//      MyMPI::allreduce(&local_size_cc,&global_size_cc,1,comm,MPI_SUM);
//      MyMPI::allreduce(&local_size_nc,&global_size_nc,1,comm,MPI_SUM);

      MPI_Waitall(4,req,MPI_STATUSES_IGNORE);

      if (rank == 0)
        {
          start_in_global_cc = 0;
          start_in_global_nc = 0;
        }

      // local start is always zero
      start_cc = 0;
      start_nc = 0;
    }
  else
    {

      local_size_cc         = MyPartitioning::partitionLength(rank,nElements,num_ranks);
      global_size_cc        = nElements;
      start_in_global_cc    = MyPartitioning::partitionStart(rank,nElements,num_ranks);
      start_cc              = start_in_global_cc;

      local_size_nc         = MyPartitioning::partitionLength(rank,nPoints,num_ranks);
      global_size_nc        = nPoints;
      start_in_global_nc    = MyPartitioning::partitionStart(rank,nPoints,num_ranks);
      start_nc              = start_in_global_nc;

    }


  // create filetypes
  MPI_Datatype  file_type;
  MPI_Datatype  file_type_nc;
  MPI_Datatype  file_type_cc;


  //      std::cout << rank << ": "
  //          << " global_size_nc: "        << global_size_nc
  //          << " local_size_nc: "         << local_size_nc
  //          << " start_in_global_nc: "    << start_in_global_nc
  //          << std::endl;

  // node centered variables
  MPIFiletypes::OneD
  (
      MPI_DOUBLE,
      global_size_nc,
      local_size_nc,
      start_in_global_nc,
      file_type_nc
  );

  //      std::cout << rank << ": "
  //          << " global_size_cc: "        << global_size_cc
  //          << " local_size_cc: "         << local_size_cc
  //          << " start_in_global_cc: "    << start_in_global_cc
  //          << std::endl;

  // cell centered variables
  MPIFiletypes::OneD
  (
      MPI_DOUBLE,
      global_size_cc,
      local_size_cc,
      start_in_global_cc,
      file_type_cc
  );

  // create communicators for process groups with non-empty data range
  uint8_t empty_cc = (local_size_cc == 0);
  uint8_t empty_nc = (local_size_nc == 0);
  uint8_t notempty_cc = !empty_cc;
  uint8_t notempty_nc = !empty_nc;

  uint8_t noneempty_nc = 1;
  uint8_t noneempty_cc = 1;

  MPI_Request ncccemptyreq[2];
  MPI_Iallreduce(&notempty_nc,&noneempty_nc,1,MPI_UINT8_T,MPI_LAND,comm,&ncccemptyreq[0]);
  MPI_Iallreduce(&notempty_cc,&noneempty_cc,1,MPI_UINT8_T,MPI_LAND,comm,&ncccemptyreq[1]);


  MPI_Waitall(2,ncccemptyreq,MPI_STATUSES_IGNORE);

  MPI_Comm writeComm;
  MPI_Comm writeComm_nc;
  MPI_Comm writeComm_cc;

  if (noneempty_nc)
    MPI_Comm_dup(comm, &writeComm_nc);
  else
    MPI_Comm_split(comm,!empty_nc ? 1 : MPI_UNDEFINED,0,&writeComm_nc);

  if (noneempty_cc)
    MPI_Comm_dup(comm, &writeComm_cc);
  else
    MPI_Comm_split(comm,!empty_cc ? 1 : MPI_UNDEFINED,0,&writeComm_cc);


  MPI_File fh;
  MPI_File fh_nc;
  MPI_File fh_cc;

  // only open file if this proc participating
  if ( writeComm_nc != MPI_COMM_NULL )
    {
      MPI_File_open(writeComm_nc, const_cast<char*>(fName.c_str()), MPI_MODE_WRONLY, MPI_INFO_NULL, &fh_nc);
      MPI_Type_commit(&file_type_nc);
    }
  if ( writeComm_cc != MPI_COMM_NULL )
    {
      MPI_File_open(writeComm_cc, const_cast<char*>(fName.c_str()), MPI_MODE_WRONLY, MPI_INFO_NULL, &fh_cc);
      MPI_Type_commit(&file_type_cc);
    }


  //    std::cout << mpiMgr.rank() << " reached " << __FILE__ << " ln: " << std::to_string(__LINE__) << std::endl;
  //  //
  //    std::cout << mpiMgr.rank() << " " << std::to_string(writeComm_nc == MPI_COMM_NULL) << " " << std::to_string(writeComm_cc == MPI_COMM_NULL) << std::endl;

  // --------------------------------------------------------------------------------//
  // Data output

  MPI_Offset  tot_bytes_written   =  pos.ZoneData;        // total number of bytes written to file by all procs


  constexpr uint8_t n_concurrent_writes = 4;

  const uint64_t write_buffer_size = std::max(local_size_cc, local_size_nc);

//  std::cout << mpiMgr.rank() << " reached " << __FILE__ << " ln: " << std::to_string(__LINE__) << "\n";
//  std::cout << "  write_buffer_size " << write_buffer_size << std::endl;


  // write buffers
  std::vector<MyMemory::aligned_array<double,32>> write_buffers(n_concurrent_writes);

  // write requests
  MPI_Request write_requests[ n_concurrent_writes ];

  for (uint8_t i = 0; i < n_concurrent_writes; ++i)
    {
      write_requests[i] = MPI_REQUEST_NULL;
      write_buffers[i].resize( write_buffer_size );
      MYASSERT(write_buffers[i].size() == write_buffer_size,"failed to allocate write buffer");
    }

  // info object for file view hint
  MPI_Info inf;
  MPI_Info_create(&inf);
  MPI_Info_set(inf,"mpiio_concurrency","true");

  // for each variable
  for (int32_t iVar = 0; iVar < nVars; ++iVar)
    {


      if (varLoc[iVar] == Tecplot::VarLoc::NodeCentered)
        {
          start         = start_nc;
          local_size    = local_size_nc;
          global_size   = global_size_nc;
          file_type     = file_type_nc;
          fh            = fh_nc;
          writeComm     = writeComm_nc;
        }
      else
        {
          start         = start_cc;
          local_size    = local_size_cc;
          global_size   = global_size_cc;
          file_type     = file_type_cc;
          fh            = fh_cc;
          writeComm     = writeComm_cc;
        }

      if (writeComm != MPI_COMM_NULL)
        {
          //                    std::cout << mpiMgr.rank() << " reached " << __FILE__ << " ln: " << std::to_string(__LINE__) << std::endl;



          // set file view for data output
          MPI_File_set_view(
              fh,
              tot_bytes_written,
              MPI_BYTE,
              file_type,
              datarep_native,
              inf // MPI_INFO_NULL
          );


          // get next free write buffer
          double* buffer = nullptr;
          MPI_Request* reqp;
          while (!buffer)
            {
              for (uint8_t i = 0; i < n_concurrent_writes; ++i)
                {
                  reqp = &write_requests[i];

                  if (*reqp != MPI_REQUEST_NULL)
                    {
                      int write_finished_Q = false;
                      MPI_Test(reqp,&write_finished_Q,MPI_STATUS_IGNORE);
                      if (write_finished_Q)
                        {
                          MPI_Request_free(reqp);
                          *reqp = MPI_REQUEST_NULL;
                        }
                    }
                  else
                    {
                      buffer = write_buffers[i].data();
                      break;
                    }
                }
            }


          // write to buffer
          const int32_t end = start+local_size;
          double minval = std::numeric_limits<double>::max();
          double maxval = std::numeric_limits<double>::lowest();

//#pragma omp parallel for schedule(static) reduction(min:minval) reduction(max:maxval)
          for (int32_t iDat = start; iDat < end; ++iDat)
            {
              const double v = dataFun(iDat,iVar);
              minval = fmin(minval,v);
              maxval = fmax(maxval,v);
              buffer[iDat-start] = v;
            }

          //                    std::cout << mpiMgr.rank() << " filled buffer for var " << iVar << std::endl;

          minMax[ 2*iVar     ] = minval; //*(std::min_element(dBuff.begin(),dBuff.begin()+local_size)); // don't use dBuss.end(), may be beyond range of current var
          minMax[ 2*iVar + 1 ] = maxval; //*(std::max_element(dBuff.begin(),dBuff.begin()+local_size));

          minMax[ 2*iVar     ] *= -1.;

          // write to file
          ret = MPI_File_iwrite_all(fh, buffer, local_size, MPI_DOUBLE, reqp);

          MYASSERT(ret == MPI_SUCCESS,
                   std::string("MPI_File_write_all failed on rank ") + std::to_string(mpiMgr.rank()) );

        }

      // update byte displacement
      tot_bytes_written += global_size * sizeof(double);

//      std::cout << mpiMgr.rank() << " reached " << __FILE__ << " ln: " << std::to_string(__LINE__) << "\n";
//      std::cout << "iVar " << iVar << std::endl;

    }


  MPI_Waitall(n_concurrent_writes,write_requests,MPI_STATUSES_IGNORE);

  MPI_Info_free(&inf);

  write_buffers = {};

  for (uint8_t i = 0; i < n_concurrent_writes; ++i)
    MPI_Request_free(&write_requests[i]);

  //  std::cout << mpiMgr.rank() << " reached " << __FILE__ << " ln: " << std::to_string(__LINE__) << std::endl;


  //      // DEBUG
  //      std::cout << disp_bytes << std::endl;
  //      std::cout << disp_bytes + nVars * global_size * 8 << std::endl;

  // close file
  if ( writeComm_nc != MPI_COMM_NULL )
    {
      MPI_File_close(&fh_nc);
      MPI_Type_free(&file_type_nc);
      MPI_Comm_free(&writeComm_nc);
    }
  if ( writeComm_cc != MPI_COMM_NULL )
    {
      MPI_File_close(&fh_cc);
      MPI_Type_free(&file_type_cc);
      MPI_Comm_free(&writeComm_cc);
    }


  // since we dont know if root is part of the individual write comms, do reduction after writing to file
  if (rank == root)
    MPI_Reduce( MPI_IN_PLACE, minMax.data(), minMax.size(), MPI_DOUBLE, MPI_MAX, root, comm);
  else
    MPI_Reduce( minMax.data(), NULL, minMax.size(), MPI_DOUBLE, MPI_MAX, root, comm );

  //    std::cout << mpiMgr.rank() << " reached " << __FILE__ << " ln: " << std::to_string(__LINE__) << std::endl;


  // save for future reference
  MPI_Offset connPos = tot_bytes_written;

  //      std::cout << mpiMgr.rank() << " reached " << __FILE__ << " ln: " << std::to_string(__LINE__) << std::endl;

  // write connectivity
  if (writeConnectivity_tf)
    {

      tot_bytes_written = writeConnectivity(
          fName,
          connPos,
          connFun,
          zoneType,
          dataSharing,
          nElements,
          mpiMgr);

    } // write connectivity

  // save position for future reference
  MPI_Offset fnConnPos = tot_bytes_written;

  // write face neighbor connections
  int32_t totNFaceNeighborConn = 0;
  if (writeFaceNeighborConnectivity_tf)
    {

      totNFaceNeighborConn = writeFaceNeigborConnectivity(
          fName,
          fnConnPos,
          fnConnFun,
          dataSharing,
          nElements,
          mpiMgr);
    }

  // root now corrects the header information
  if (rank == root)
    {
      // invert minima
      for (int32_t i = 0; i < nVars; ++i)
        minMax[2*i] *= -1.;

      MPI_File fh;
      MPI_File_open(MPI_COMM_SELF, const_cast<char*>(fName.c_str()), MPI_MODE_WRONLY, MPI_INFO_NULL, &fh);
      MPI_File_set_view(fh,0,MPI_BYTE,MPI_BYTE,datarep_native,MPI_INFO_NULL);

      MPI_Offset tmpPos = pos.ZoneDataHeader;

      // zone data section header again with correct minmax

      ret = Headers::writeTecZoneDataHeader(
          fh,
          tmpPos,
          dataFormat,
          minMax,
          rank,
          root);

      MYASSERT(ret == MPI_SUCCESS,"re-write of zone data header with min/max values failed");

      // if we wrote fn connectivity, write zone  header again with correct total number of face neighbor connections

      if (writeFaceNeighborConnectivity_tf)
        {
          tmpPos = pos.ZoneHeader;
          ret = Headers::writeTecZoneHeader(
              fh,
              tmpPos,
              zoneName,
              StrandID,
              solution_time_to_write,
              zoneType,
              varLoc,
              totNFaceNeighborConn,
              global_numPts,
              rank,
              root);

          MYASSERT(ret == MPI_SUCCESS,"re-writing zone header with total number of face neighbor connections failed");
        }

      MPI_File_close(&fh);

    } // re-write headers


  feenableexcept(feexceptstate);


//      std::cout << rank << " reached " << __FILE__ << " ln: " << std::to_string(__LINE__) << std::endl;

}


#include "1DData2Tecplot_Connectivity.hpp"
#include "1DData2Tecplot_FNConnectivity.hpp"



