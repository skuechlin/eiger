/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

///*
// * Output.cpp
// *
// *  Created on: Oct 10, 2014
// *      Author: kustepha
// */
//
//
//#include "Output.h"
//
//#include "Parallel/MyMPI.h"
//
//#include "Settings/Dictionary.h"
//
//#include "Primitives/MyString.h"
//#include "Primitives/Timer.h"
//
//
//
//Output::Output(
//    const writeFunT& writeFun,
//    const std::string& dir,
//    const uint64_t tn_first,
//    const uint64_t tn_last,
//    const uint64_t tn_interval )
//: Callback(
//    [this](
//        const double t,
//        const uint64_t tn,
//        const MyMPI::MPIMgr& mpiMgr,
//        MyChrono::TimerCollection& timers)->void{
//  timers.start("output"); // own master region
//#pragma omp master
//  {
//    writeFun_(dir_,t,tn,mpiMgr);
//  }
//  timers.stop("output");
//#pragma omp barrier
//}, tn_first, tn_last, tn_interval )
//, dir_(MyString::makeLastCharDirSep(dir))
//, writeFun_(writeFun)
//{}
//
//Output::Output(
//    writeFunT writeFun,
//    const Settings::Dictionary& dict,
//    const std::string& dirPref )
//: Output(
//    writeFun,
//    MyString::catDirs(dirPref,dict.get<std::string>("directory","")),
//    dict.get<uint64_t>("first",0),
//    dict.get<uint64_t>("last",uint64_t(-1)),
//    dict.get<uint64_t>("interval",1) )
//{}
//
//Output::~Output() = default;
