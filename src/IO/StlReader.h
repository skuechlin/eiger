/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * StlReader.h
 *
 *  Created on: Apr 10, 2014
 *      Author: Stephan Kuechlin
 */

#ifndef STLREADER_H_
#define STLREADER_H_

#include <string>
#include <vector>
#include <memory>

#include <Eigen/Geometry>

namespace Shapes
{
class FEShapeData;
}

namespace StlReader {

// read file and covert all facets to a list of triangles
std::unique_ptr<Shapes::FEShapeData>
read(
		const std::string& stlFileName,
		const double tol = -1.,
		const Eigen::Transform<double,4,Eigen::Affine>& tr = Eigen::Transform<double,4,Eigen::Affine>::Identity(),
		const bool flipNormals = false,
		const Eigen::AlignedBox4d& = {
	          Eigen::Vector4d::Constant(-std::numeric_limits<double>::max()),
	          Eigen::Vector4d::Constant(std::numeric_limits<double>::max()) });

}

#endif /* STLREADER_H_ */
