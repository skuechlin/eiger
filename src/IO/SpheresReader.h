/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * SpheresReader.h
 *
 *  Created on: Nov 13, 2015
 *      Author: kustepha
 */

#ifndef SRC_IO_SPHERESREADER_H_
#define SRC_IO_SPHERESREADER_H_

#include "Geometry/SphereCollection.h"

namespace SpheresReader {

	// read file and return matrix of centers and sphere radii
	// expects the following file structure (*** = arbitrary content)
	//
	// ***
	// BEGIN SPHERES N ***
	// X0 Y0 Z0 R0 ***
	// X1 Y1 Z1 R1 ***
	// ...
	// XN-1 YN-1 ZN-1 RN-1 ***
	// ***
	//
	Shapes::SphereCollection::SphereDataT
	read(
			const std::string& spheresFileName,
			const double tol = 1.0e-16,
			const Eigen::Transform<double,4,Eigen::Affine>& tr = Eigen::Transform<double,4,Eigen::Affine>::Identity()
	);

} /* namespace SpheresReader */

#endif /* SRC_IO_SPHERESREADER_H_ */
