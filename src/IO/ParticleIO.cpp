/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * ParticleWriter.cpp
 *
 *  Created on: May 14, 2014
 *      Author: Stephan Kuechlin
 */

#include <vector>
#include <limits>
#include <numeric> // std::accumulate

#include "Primitives/MyError.h"
#include "Primitives/MyArithm.h"
#include "Primitives/Partitioning.h"

#include "Particles/ParticleCollection.h"

#include "Parallel/MyMPI.h"



namespace Particles {

  constexpr uint64_t MAGIC = ((uint64_t)(1)<<30) * (((uint64_t)(1)<<31)-1); // the 8th perfect number

  void
  ParticleCollection::write(
      const double t,
      const std::string& fName,
      const MyMPI::MPIMgr& mpiMgr)
  const
  {

    MPI_Datatype particle_t = particleMPIType();

    MPI_Aint particle_t_extent;
    MPI_Aint lb;
    MPI_Type_get_extent(particle_t,&lb,&particle_t_extent);

    MPI_Offset pos		    = 0;		// position in file
    MPI_Offset pos_bytes 	= 0;		// explicit byte offset
    int32_t ret	;	                    // return codes
    int32_t root	= mpiMgr.root();
    int32_t id		= mpiMgr.rank();
    MPI_Status stat;
    char mpiErrStr[MPI_MAX_ERROR_STRING];
    int mpiErrStrL;
    char datarep[] = "native";



    // open the file, delete previous copy
    MPI_File fh;

    MPI_File_open(mpiMgr.comm(), const_cast<char*>(fName.c_str()), MPI_MODE_CREATE | MPI_MODE_WRONLY, MPI_INFO_NULL, &fh);
    MPI_File_close(&fh);

    MPI_File_delete(const_cast<char*>(fName.c_str()),MPI_INFO_NULL);
    ret = MPI_File_open(mpiMgr.comm(), const_cast<char*>(fName.c_str()), MPI_MODE_CREATE | MPI_MODE_WRONLY, MPI_INFO_NULL, &fh);

    MYASSERT(ret == MPI_SUCCESS, std::string("unable to open particle file ") + fName + std::string("!") );


    // -------------------------------------------------------------------------------------
    // GATHER INFO

    // note file write limit (max file view extent) is signed int max

    // global limit
    uint64_t max_global_size_per_write = std::numeric_limits<int32_t>::max() / particle_t_extent;

    // local limit
    uint64_t max_local_size_per_write = max_global_size_per_write / mpiMgr.nRanks();

    // total local size
    uint64_t total_local_size = size();

    // compute total global size
    std::vector<uint64_t> total_local_sizes(mpiMgr.nRanks(),0);
    MPI_Allgather(&total_local_size,1,MPI_UINT64_T,total_local_sizes.data(),1,MPI_UINT64_T,mpiMgr.comm());
    uint64_t total_global_size = std::accumulate(total_local_sizes.begin(),total_local_sizes.end(),uint64_t(0));


    // -------------------------------------------------------------------------------------
    // WRITE HEADER

    if (id == root)
      {
        // magic number
        ret = MPI_File_write(fh,const_cast<uint64_t*>(&MAGIC),1,MPI_UINT64_T,&stat);
        MYASSERT(ret == MPI_SUCCESS, "failed to write magic number");

        // solution time
        ret = MPI_File_write(fh,const_cast<double*>(&t),1,MPI_DOUBLE,&stat);
        MYASSERT(ret == MPI_SUCCESS, "failed to write solution time");

        // total number of particles
        ret = MPI_File_write(fh,&total_global_size,1,MPI_UINT64_T,&stat);
        MYASSERT(ret == MPI_SUCCESS, "failed to write total number of particles");

        // number of bytes per particle
        ret = MPI_File_write(fh,&particle_t_extent,1,MPI_AINT,&stat);
        MYASSERT(ret == MPI_SUCCESS, "failed to write number of bytes per particle");

        MPI_File_get_position(fh,&pos);
        MPI_File_get_byte_offset(fh,pos,&pos_bytes);
      }


    if (total_global_size < 1)
      {
        // close the file
        MPI_File_close(&fh);
        return; // nothing more to do
      }

    // broadcast new byte position. note: no MPI_Type for MPI_Offset...

    int64_t fpos_tmp(pos_bytes);
    MPI_Bcast(&fpos_tmp,1,MPI_INT64_T,mpiMgr.root(),mpiMgr.comm());
    pos_bytes = fpos_tmp;



    // -------------------------------------------------------------------------------------
    // WRITE FILE IN CHUNKS

    MPI_Datatype 		file_type;
    int32_t 			local_size;
    int32_t 			global_size;
    int32_t 			start;
    uint64_t 			total_local_size_remaining = total_local_size;
    double*			    buffer = (double*)(const_cast<Particle*>(data_.data()));
    uint64_t			doubles_per_particle = uint64_t(particle_t_extent) / sizeof(double);

    // compute number of writes. this is the maximum total_local_size, divided by max_local_size_per_write, rounded up
    uint64_t n_writes = *(std::max_element(total_local_sizes.begin(),total_local_sizes.end()));
    n_writes = MyArithm::ceilIntDiv(n_writes,max_local_size_per_write);

    // process all chunks
    for (uint64_t i = 0; i < n_writes; ++i)
      {


        // --------------------------------------------------------------------------------//
        // compute sizes for this write

        // local
        local_size = std::min(total_local_size_remaining,max_local_size_per_write);

        // global
        MPI_Request req[2];
        MPI_Iallreduce(&local_size,&global_size,1,MPI_INT32_T,MPI_SUM,mpiMgr.comm(),&req[0]);
        MPI_Iexscan(&local_size,&start,1,MPI_INT32_T,MPI_SUM,mpiMgr.comm(),&req[1]);

        MPI_Waitall(2,req,MPI_STATUSES_IGNORE);

        if (mpiMgr.isRoot())
          start = 0;

//        MyMPI::allgather(local_size,local_sizes,mpiMgr.comm());
//        global_size = std::accumulate(local_sizes.begin(),local_sizes.end(),int32_t(0));
//        // local start
//        start = std::accumulate(local_sizes.begin(),local_sizes.begin()+id,int32_t(0));


        // will this node not participate in write?
        bool empty = (local_size < 1);


        // --------------------------------------------------------------------------------//
        // Data output

        // create filetype
        // if no data on this node, need to create dummy with size one, start 0
        // note: global_size >= 1 or else we would have returned
        if (empty)
          {
            local_size = 1;
            start = 0;
          }


        ret = MPI_Type_create_subarray(1,&global_size,&local_size,&start,MPI_ORDER_C,particle_t,&file_type);

        // assert successful type creation
        MPI_Error_string(ret, mpiErrStr, &mpiErrStrL);
        MYASSERT(ret == MPI_SUCCESS,
                 "MPI_Type_create_subarray with global_size = " + std::to_string(global_size)
        + ", local_size = " + std::to_string(local_size)
        + ", start = " + std::to_string(start) + " failed: " + std::string(mpiErrStr,mpiErrStrL) );


        // make usable by MPI
        MPI_Type_commit(&file_type);


        // set file view for data output
        ret = MPI_File_set_view(fh,pos_bytes,particle_t,file_type,datarep,MPI_INFO_NULL);

        // assert no problems during file view set
        MPI_Error_string(ret, mpiErrStr, &mpiErrStrL);
        MYASSERT(ret == MPI_SUCCESS, "setting file view failed: " + std::string(mpiErrStr,mpiErrStrL) );



        // write to file
        if (empty)
          {
            local_size = 0;
          }

        ret = MPI_File_write(fh, buffer, local_size, particle_t, MPI_STATUS_IGNORE);

        // assert no problems during write
        MPI_Error_string(ret, mpiErrStr, &mpiErrStrL);
        MYASSERT(ret == MPI_SUCCESS, "writing " + std::to_string(local_size) + " particles to file failed: " + std::string(mpiErrStr,mpiErrStrL) );

        // increment buffer pointer by number of values written from this node
        buffer += uint64_t(local_size)*doubles_per_particle; //cast first to avoid overflow

        // update remaining local size
        total_local_size_remaining -= local_size;

        // update offset
        pos_bytes += uint64_t(global_size)*uint64_t(particle_t_extent);


        // wait for all procs to finish write
        MyMPI::barrier();



        // remove filetype
        MPI_Type_free(&file_type);


      } // for all writes




    // close the file
    MPI_File_close(&fh);


  } // write



  double
  ParticleCollection::read(
      const std::string& fName,
      const MyMPI::MPIMgr& mpiMgr)
  {

    MPI_Datatype particle_t = particleMPIType();

    MPI_Aint particle_t_extent;
    MPI_Aint lb;
    MPI_Type_get_extent(particle_t,&lb,&particle_t_extent);

    MPI_Offset pos		    = 0;		// position in file
    MPI_Offset pos_bytes 	= 0;		// explicit byte offset
    int32_t ret;	                    // return codes
    int32_t nNodes	= mpiMgr.nRanks();
    int32_t id		= mpiMgr.rank();

    MPI_Status stat;
    char mpiErrStr[MPI_MAX_ERROR_STRING];
    int mpiErrStrL;
    char datarep[] = "native";

    uint64_t expectMagic;

    double solTime;


    // open the file

    MPI_File fh;
    ret = MPI_File_open(
        mpiMgr.comm(),
        const_cast<char*>(fName.c_str()),
        MPI_MODE_RDONLY,
        MPI_INFO_NULL,
        &fh );

    MYASSERT(ret == MPI_SUCCESS,"failed to open particle file \"" + fName + "\"");


    // -------------------------------------------------------------------------------------
    // READ HEADER

    // read and check magic number
    ret = MPI_File_read(fh,
                            &expectMagic, 1, MPI_UINT64_T,
                            &stat);

    MYASSERT(ret == MPI_SUCCESS,"failed to read magic number from particle file \"" + fName + "\"");

    MYASSERT(expectMagic == MAGIC,"error reading particle file, magic number did not compare");


    // read solution time
    ret = MPI_File_read(fh,
                            &solTime, 1, MPI_DOUBLE,
                            &stat);
    MYASSERT(ret == MPI_SUCCESS,"failed to read solution time from particle file \"" + fName + "\"");



    // read total number of particles
    uint64_t total_global_size;
    ret = MPI_File_read(fh,
                            &total_global_size, 1, MPI_UINT64_T,
                            &stat);
    MYASSERT(ret == MPI_SUCCESS,"failed to read number of particles from particle file \"" + fName + "\"");


    // read number of bytes per particle and assert equal to local storage dimension
    MPI_Aint particle_t_extent_in_file;
    ret = MPI_File_read(fh,
                            &particle_t_extent_in_file, 1, MPI_AINT,
                            &stat);
    MYASSERT(ret == MPI_SUCCESS,"failed to read number of bytes per particle from particle file \"" + fName + "\"");
    MYASSERT(particle_t_extent == particle_t_extent_in_file,
             std::string("Mismatch of number of bytes per particle.\n")
    + std::string("This implementation: ") + std::to_string(particle_t_extent) + std::string("\n")
    + std::string("Particle file:       ") + std::to_string(particle_t_extent_in_file) );

    // return if no particles to read
    if (total_global_size < 1)
      {
        // close the file
        MyMPI::barrier();
        MPI_File_close(&fh);
        return solTime; // nothing more to do
      }


    // update current position
    MPI_File_get_position(fh,&pos);
    MPI_File_get_byte_offset(fh,pos,&pos_bytes);


    // -------------------------------------------------------------------------------------
    // COMPUTE READ DISTRIBUTION

    // note file read limit (max file view extent) is signed int max

    // global limit
    uint64_t max_global_size_per_read = std::numeric_limits<int32_t>::max() / particle_t_extent;

    // local limit
    uint64_t max_local_size_per_read = max_global_size_per_read / mpiMgr.nRanks();

    // distribute evenly among ranks
    std::vector<uint64_t> total_local_sizes(mpiMgr.nRanks());
    for (uint64_t i = 0; i < mpiMgr.nRanks(); ++i)
      total_local_sizes[i] = MyPartitioning::partitionLength(i,total_global_size,nNodes);

    uint64_t total_local_size = total_local_sizes[id];

    // allocate local storage
    resize(total_local_size, static_cast<uint8_t>(ResizeMode::ALL));


    // -------------------------------------------------------------------------------------
    // READ FILE IN CHUNKS

    MPI_Datatype 		file_type;
    int32_t 			local_size;
    std::vector<int32_t> 	local_sizes;
    int32_t 			global_size;
    int32_t 			start;
    uint64_t 			total_local_size_remaining = total_local_size;
    double*			    buffer = (double*)(data_.data());
    uint64_t			doubles_per_particle = uint64_t(particle_t_extent) / sizeof(double);

    // compute number of reads. this is the maximum total_local_size, divided by max_local_size_per_read, rounded up
    uint64_t n_reads = *(std::max_element(total_local_sizes.begin(),total_local_sizes.end()));
    n_reads = MyArithm::ceilIntDiv(n_reads,max_local_size_per_read);

    // process all chunks
    for (uint64_t i = 0; i < n_reads; ++i)
      {


        // --------------------------------------------------------------------------------//
        // compute sizes for this write

        // local
        local_size = std::min(total_local_size_remaining,max_local_size_per_read);

        MPI_Request req[2];
        MPI_Iallreduce(&local_size,&global_size,1,MPI_INT32_T,MPI_SUM,mpiMgr.comm(),&req[0]);
        MPI_Iexscan(&local_size,&start,1,MPI_INT32_T,MPI_SUM,mpiMgr.comm(),&req[1]);

        MPI_Waitall(2,req,MPI_STATUSES_IGNORE);

        if (mpiMgr.isRoot())
          start = 0;

//        // global
//        MyMPI::allgather(local_size,local_sizes,mpiMgr.comm());
//        global_size = std::accumulate(local_sizes.begin(),local_sizes.end(),int32_t(0));
//
//        // local start
//        start = std::accumulate(local_sizes.begin(),local_sizes.begin()+id,int32_t(0));


        // will this node not participate in write?
        bool empty = (local_size < 1);


        // --------------------------------------------------------------------------------//
        // Data output

        // create filetype
        // if no data on this node, need to create dummy with size one, start 0
        // note: global_size >= 1 or else we would have returned
        if (empty)
          {
            local_size = 1;
            start = 0;
          }


        ret = MPI_Type_create_subarray(1,&global_size,&local_size,&start,MPI_ORDER_C,particle_t,&file_type);

        // assert successful type creation
        MPI_Error_string(ret, mpiErrStr, &mpiErrStrL);
        MYASSERT(ret == MPI_SUCCESS,
                 "MPI_Type_create_subarray with global_size = " + std::to_string(global_size)
        + ", local_size = " + std::to_string(local_size)
        + ", start = " + std::to_string(start) + " failed: " + std::string(mpiErrStr,mpiErrStrL) );


        // make usable by MPI
        MPI_Type_commit(&file_type);


        // set file view for data output
        ret = MPI_File_set_view(fh,pos_bytes,particle_t,file_type,datarep,MPI_INFO_NULL);

        // assert no problems during file view set
        MPI_Error_string(ret, mpiErrStr, &mpiErrStrL);
        MYASSERT(ret == MPI_SUCCESS, "setting file view failed: " + std::string(mpiErrStr,mpiErrStrL) );



        // read from file
        if (empty)
          {
            local_size = 0;
          }

        ret = MPI_File_read(fh, buffer, local_size, particle_t, MPI_STATUSES_IGNORE);

        // assert no problems during write
        MPI_Error_string(ret, mpiErrStr, &mpiErrStrL);
        MYASSERT(ret == MPI_SUCCESS, "reading " + std::to_string(local_size) + " particles from file failed: " + std::string(mpiErrStr,mpiErrStrL) );

        // increment buffer pointer by number of values read from this node
        buffer += uint64_t(local_size)*doubles_per_particle; //cast first to avoid overflow

        // update remaining local size
        total_local_size_remaining -= local_size;

        // update offset
        pos_bytes += uint64_t(global_size)*uint64_t(particle_t_extent);


        // wait for all procs to finish write
        MyMPI::barrier();



        // remove filetype
        MPI_Type_free(&file_type);


      } // for all writes

    // close the file
    MPI_File_close(&fh);

    // return solution time
    return solTime;


  } // read

} // namespace ParticleCollections
