/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * NastranReader.h
 *
 *  Created on: Nov 11, 2014
 *      Author: kustepha
 */

#ifndef IO_NASTRANREADER_H_
#define IO_NASTRANREADER_H_

#include <memory>

#include <Eigen/Dense>

namespace Shapes
{
class FEShapeData;
}

namespace NastranReader
{

// read file and covert all facets to a list of triangles
std::unique_ptr<Shapes::FEShapeData>
read(
		const std::string& nastranFileName,
		const double tol = 1.,
		const Eigen::Transform<double,4,Eigen::Affine>& tr = Eigen::Transform<double,4,Eigen::Affine>::Identity(),
		const bool flipNormals = false,
		const Eigen::AlignedBox4d& = {
		              Eigen::Vector4d::Constant(-std::numeric_limits<double>::max()),
		              Eigen::Vector4d::Constant(std::numeric_limits<double>::max()) }
);

} // namespace NastranReader



#endif /* IO_NASTRANREADER_H_ */
