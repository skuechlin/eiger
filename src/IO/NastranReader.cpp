/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * NastranReader.cpp
 *
 *  Created on: Nov 11, 2014
 *      Author: kustepha
 */

#include <iostream>
#include <fstream>
#include <unordered_map>
#include <utility>
#include <cstdint>

#include "NastranReader.h"

#include "Primitives/MyArithm.h"
#include "Primitives/MyError.h"
#include "Primitives/MyIntrin.h"

#include "Geometry/FECollection.h"



namespace {


// METHODS
std::vector<uint64_t> parseCQUAD4(const std::string& CQUAD4Buff);
std::vector<uint64_t> parseCTRIA3(const std::string& CTRIA3Buff);
std::pair<uint64_t,Eigen::Vector4d> parseVertex(const std::string& vertexBuff);

}



namespace NastranReader {

// covert all facets to a list of triangles
std::unique_ptr<Shapes::FEShapeData>
read(
		const std::string& nastranFileName,
		const double tol,
		const Eigen::Transform<double,4,Eigen::Affine>& tr,
		const bool flipNormals,
		const Eigen::AlignedBox4d& world_bb)
{

	std::unique_ptr<Shapes::FEShapeData> shapes(new Shapes::FEShapeData(world_bb));


	std::unordered_map<uint64_t,Eigen::Vector4d> verts;
	std::vector<uint64_t> vrtIds;

	std::string lineBuff;

	std::string vertKey("GRID");
	std::string CTRIA3Key("CTRIA3");
	std::string CQUAD4Key("CQUAD4");

	// open the file
	std::ifstream nastranFileStream(nastranFileName);


	double invtol = 1./tol;

	MYASSERT(nastranFileStream.is_open(),std::string("unable to open NASTRAN file \"") + nastranFileName + std::string("\""));

	bool valid = false;
	while ( std::getline(nastranFileStream,lineBuff) )
	  {
	    if (lineBuff.substr(0,10).compare(std::string("BEGIN BULK")) == 0 )
	      {
		valid = true;
		break;
	      }
	  }

	MYASSERT(valid,
		 std::string("file \"") + nastranFileName + std::string("\" passed to NastranReader missing keyword \"BEGIN BULK\""));


	while ( std::getline(nastranFileStream,lineBuff) )
	{


	    if (lineBuff.find(vertKey) != std::string::npos)
	      { verts.insert(parseVertex(lineBuff)); }
	    else if (lineBuff.find(CTRIA3Key) != std::string::npos)
	      {
	        vrtIds = parseCTRIA3(lineBuff);

	        Eigen::Vector4d A = verts[vrtIds[0]];
	        Eigen::Vector4d B = verts[vrtIds[ flipNormals ? 2 : 1]];
	        Eigen::Vector4d C = verts[vrtIds[ flipNormals ? 1 : 2]];

	        if (tol > 0.)
	          {
	            A = MyIntrin::round(A,tol,invtol);
	            B = MyIntrin::round(B,tol,invtol);
	            C = MyIntrin::round(C,tol,invtol);
	          }

	        shapes->addTri( tr*A, tr*B, tr*C );

	      }
	    else if (lineBuff.find(CQUAD4Key) != std::string::npos)
	      {
	        vrtIds = parseCQUAD4(lineBuff);

            Eigen::Vector4d A = verts[vrtIds[0]];
            Eigen::Vector4d B = verts[vrtIds[ flipNormals ? 3 : 1]];
            Eigen::Vector4d C = verts[vrtIds[2]];
            Eigen::Vector4d D = verts[vrtIds[ flipNormals ? 1 : 3]];

            if (tol > 0.)
              {
                A = MyIntrin::round(A,tol,invtol);
                B = MyIntrin::round(B,tol,invtol);
                C = MyIntrin::round(C,tol,invtol);
                D = MyIntrin::round(D,tol,invtol);
              }

            shapes->addQuad( tr*A, tr*B, tr*C, tr*D );

	      }


	}

	// close the file
	nastranFileStream.close();

	// trigger shape mapping and connectivity build
	shapes->finalize();

	return shapes;

} // read

} // namespace NastranReader





namespace
{


std::vector<uint64_t>
parseCTRIA3(const std::string& CTRIA3Buff)
{

//	CTRIA3 	Triangular Plate Element Connection
//	Defines an isoparametric membrane-bending or plane strain
//	triangular plate element.
//
//	Format:
//		1		2	3	4	5	6
//		CTRIA3 	EID PID G1 	G2 	G3
//
//	Field Contents
//	EID Element identification number. (0 < Integer < 100,000,000)
//	PID Property identification number of a PSHELL, PCOMP, PCOMPG or PLPLANE entry. (Integer > 0; Default = EID)
//	Gi Grid point identification numbers of connection points. (Integers > 0, all unique)
//
//	Example: CTRIA3,1,1,6,1,5


	std::vector<uint64_t> ids(3,0);
	std::sscanf(
			CTRIA3Buff.c_str(),
			"CTRIA3,%*u,%*u,%lu,%lu,%lu",
			&ids[0],
			&ids[1],
			&ids[2]
	);

	return ids;

}

std::vector<uint64_t>
parseCQUAD4(const std::string& CQUAD4Buff)
{

//	CQUAD4 	Quadrilateral Plate Element Connection
//	Defines an isoparametric membrane-bending or plane strain quadrilateral plate element.
//
//	Format:
//		1		2	3	4	5	6	7
//		CQUAD4 	EID PID G1 	G2 	G3	G4
//
//	Field Contents
//	EID Element identification number. (0 < Integer < 100,000,000)
//	PID Property identification number of a PSHELL, PCOMP, PCOMPG or PLPLANE entry. (Integer > 0; Default = EID)
//	Gi Grid point identification numbers of connection points. (Integers > 0, all unique)
//
//	Example: CQUAD4,12,1,17,16,15,18


	std::vector<uint64_t> ids(4,0);
	std::sscanf(
			CQUAD4Buff.c_str(),
			"CQUAD4,%*u,%*u,%lu,%lu,%lu,%lu",
			&ids[0],
			&ids[1],
			&ids[2],
			&ids[3]
	);

	return ids;

}


std::pair<uint64_t,Eigen::Vector4d>
parseVertex(const std::string& vertexBuff)
{

//	GRID	Grid Point
//	Defines the location of a geometric grid point, the
//	directions of its displacement, and its permanent single-point constraints.
//
//	1		2	3	4	5	6
//	GRID 	ID 	CP 	X1 	X2 	X3
//
//	Field Contents
//	ID Grid point identification number. (0 < Integer < 100,000,000)
//	CP Identification number of coordinate system in which the location of the grid point is
//	defined. (Integer >	0 or blank)
//	X1, X2, X3 Location of the grid point in coordinate system CP. (Real; Default = 0.0)
//
//	Example: GRID,1,,-0.5,2.5E-07,0

	// skip first 3 values
	uint64_t p = 0;
	for (uint64_t i = 0; i < 3; ++i)
			p = vertexBuff.find(',',p+1);


	uint64_t id;
	std::sscanf(
			vertexBuff.c_str(),
			"GRID,%lu,",
			&id
			);

	Eigen::Vector4d v(0.,0.,0.,0.);
	std::sscanf(
			vertexBuff.c_str()+p+1,
			"%lf,%lf,%lf",
			&v.x(),
			&v.y(),
			&v.z()
	);

	return std::pair<uint64_t,Eigen::Vector4d>(id,v);

}






} // namespace
