/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * Output.h
 *
 *  Created on: Oct 10, 2014
 *      Author: kustepha
 */

#ifndef OUTPUT_H_
#define OUTPUT_H_

#include <omp.h>
#include <Primitives/Callback.h>
#include <string>

#include "Primitives/Timer.h"
#include "Primitives/MyString.h"


#include "Settings/Dictionary.h"

template<typename WFunT>
class Output: public GenericCallback{


private:

  const std::string dir_;

  const WFunT writeFun_;


public:

  void call(
      const double t,
      const uint64_t tn,
      const MyMPI::MPIMgr& mpiMgr,
      MyChrono::TimerCollection& timers)
  const override {

    timers.start("output"); // own master region
#pragma omp master
    {
      writeFun_(dir_,t,tn,mpiMgr);
    }
    timers.stop("output");
#pragma omp barrier

  }

  Output(
      WFunT&& writeFun,
      const std::string& dir,
      const uint64_t tn_first = -1,
      const uint64_t tn_last = -1,
      const uint64_t tn_interval = 1,
      const std::vector<uint64_t>& tns = {})
  : GenericCallback(tn_first,tn_last,tn_interval,tns)
  , dir_( MyString::makeLastCharDirSep(dir) )
  , writeFun_(writeFun)
  {}

  Output(
      WFunT&& writeFun,
      const Settings::Dictionary& dict,
      const std::string& dirPref = "" )
  : GenericCallback(dict)
  , dir_( MyString::makeLastCharDirSep(
      MyString::catDirs(dirPref,dict.get<std::string>("directory","")) ) )
  , writeFun_( writeFun )
  {}

};

template<typename FunT, typename... Args> Output<FunT>
constructOutput(FunT&& fun,Args... args) {
  return Output<FunT>(std::forward<FunT>(fun),
                       std::forward<Args>(args)...); }

template<typename FunT, typename... Args> Output<FunT>*
newOutput(FunT&& fun,Args... args) {
  return new Output<FunT>(std::forward<FunT>(fun),
                       std::forward<Args>(args)...); }

#endif /* OUTPUT_H_ */
