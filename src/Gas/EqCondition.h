/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * EqCondition.h
 *
 *  Created on: Dec 3, 2014
 *      Author: kustepha
 */

#ifndef GAS_EQCONDITION_H_
#define GAS_EQCONDITION_H_

#include <memory>
#include <vector>

#include <Eigen/Dense>

#include "GasForwardDecls.h"

namespace Settings
{
  class Dictionary;
}

namespace MyRandom
{
  class Rng;
}

class FieldData;

namespace Gas
{

  class EqCondition
  {

      struct RefCondition
      {
          double n_;	// number density

          double m_;	// single physical particle mass

          double V_;	// (cell) volume

          uint64_t np_; // number of particles in reference volume

          double w_; 	// particle statistical weight



          // set condition by dictionary
          RefCondition(
              const Settings::Dictionary& dict,
              const double V_ref,
              const GasModel* gasModel);

      };

      struct alignas(32) Condition
          {
          double n_;	// number density

          double T_; 	// temperature
          double Trot_;	// rotational temperature
          double Tvib_;	// vibrational temperature

          Eigen::Vector4d U_; // mean velocity


            public:


          // set condition by dictionary
          Condition(
              const Settings::Dictionary& dict,
              const GasModel* gasModel);

          // set conditions by explicit values
          Condition(
              const double n,
              const double T,
              const Eigen::Vector4d& U);

          // set conditions by explicit values
          Condition(
              const double n,
              const double T,
              const double Trot,
              const double Tvib,
              const Eigen::Vector4d& U);

          EIGEN_MAKE_ALIGNED_OPERATOR_NEW

          };

      uint64_t field_mask_;

      RefCondition ref_;

      std::vector<Condition,Eigen::aligned_allocator<Condition>> conds_;




    public:

      bool isField() const { return field_mask_; }

      Eigen::Vector4d
      meanVel(const uint64_t i = 0) const {
        return conds_[field_mask_ & i].U_; }

      // number of computational particles in volume
      double numPrts(const double V, const uint64_t i = 0) const {
        return ref_.np_ * ((conds_[field_mask_ & i].n_* V)/(ref_.n_*ref_.V_)); }

      // number of computational particles in reference
      double numPrtsRef(const double V) const { return ref_.np_* V/ref_.V_; }

      // number density
      double n(const uint64_t i = 0) const { return conds_[field_mask_ & i].n_; }

      // temperature
      double T(const uint64_t i = 0) const { return conds_[field_mask_ & i].T_; }

      // rotational temperature
      double Trot(const uint64_t i = 0) const { return conds_[field_mask_ & i].Trot_; }

      // vibrational temperature
      double Tvib(const uint64_t i) const { return conds_[field_mask_ & i].Tvib_; }

      // statistical weight
      double w() const { return ref_.w_; }

      // physical particle mass
      double m() const { return ref_.m_; }


    public:

      // vector of variables names necessary for construction
      static std::vector<std::string> requiredVars() {
        return {"U","V","W","T","n"}; }

      // vector of variables names optional for construction
      static std::vector<std::string> optionalVars() {
        return {"TRot","TVib"}; }

      // set all conditions by same dictionary
      EqCondition(
          const Settings::Dictionary& dict,
          const double V_ref,
          const GasModel* gasModel);

      // set reference conditions by separate dictionary
      EqCondition(
          const Settings::Dictionary& dict,
          const Settings::Dictionary& refDict,
          const double V_ref,
          const GasModel* gasModel);

      // set non reference conditions by FieldData, reference by dict
      EqCondition(
          const FieldData* fData,
          const Settings::Dictionary& refDict,
          const double V_ref,
          const GasModel* gasModel);



      ~EqCondition (){}

      EIGEN_MAKE_ALIGNED_OPERATOR_NEW
  };




} /* namespace Gas */

#endif /* GAS_EQCONDITION_H_ */
