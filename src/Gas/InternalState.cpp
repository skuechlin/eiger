/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * InternalState.cpp
 *
 *  Created on: Dec 17, 2014
 *      Author: kustepha
 */

#include "Gas/InternalState.h"

#include "Gas/GasModel.h"

#include "Moments/InternalMoments.h"

namespace Gas
{

  void
  InternalState::update(
      const double es_rot,
      const double es_vib,
      const double T,
      const GasModels::GasModel* const gm)
  {
    T_rot = gm->TRot(es_rot);
    T_vib = gm->TVib(es_vib);
    Z_rot = gm->zRot(T); // [#] rotational collision number
    Z_vib = gm->zVib(T); // [#] vibrational collision number
    esEq_rot = gm->esEqRot(T); // [J kg-1] equilibrium specific rotational energy
    esEq_vib = gm->esEqVib(T); // [J kg-1] equilibrium specific vibrational energy
    c_rot = gm->cRot(T); // [-] rotational specific heat divided by kB
    c_vib = gm->cVib(T); // [-] vibrational specific heat divided by kB
  }


} /* namespace Gas */
