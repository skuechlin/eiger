/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * GasModelFactory.h
 *
 *  Created on: Aug 5, 2014
 *      Author: kustepha
 */

#ifndef GASMODELFACTORY_H_
#define GASMODELFACTORY_H_

#include <memory>

#include "GasModel.h"

namespace Settings
{
  class Dictionary;
}

namespace Gas
{
  namespace GasModels
  {

    std::unique_ptr<GasModel>
    makeGasModel(const Settings::Dictionary& dict);

  } /* namespace GasModels */
} // namespace Gas

#endif /* GASMODELFACTORY_H_ */
