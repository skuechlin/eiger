/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * GasModel.cpp
 *
 *  Created on: Dec 15, 2016
 *      Author: kustepha
 */



#include "Gas/GasModel.h"

#include "Settings/Dictionary.h"

#include "Primitives/MyString.h"

namespace
{
  double getOmega(const Settings::Dictionary& dict)
  {
    std::string type = dict.get<std::string>("type");
    MyString::toUpper(type);

    if ( type.compare("HS") == 0)
      return 0.5;
    if ( type.compare("MAXWELL") == 0)
      return 1.0;
    else
      return dict.get<double>("omega");
  }


  void
  getNT(const Settings::Dictionary& dict, const Gas::GasModels::GasModel* gasModel, double& n, double& T)
  {
    int state = 0;
    double p = 0.;

    if (dict.hasMember("p"))
      {
        p = dict.get<double>("p"); //[Pa]
        state += 1;
      }

    if (dict.hasMember("n"))
      {
        n = dict.get<double>("n"); //[m-3])
        state += 2;
      }

    if (dict.hasMember("rho"))
      {
        n = dict.get<double>("rho")/gasModel->m();
        state += 4;
      }

    if (dict.hasMember("T"))
      {
        T = dict.get<double>("T"); //[K]
        state += 8;
      }


    switch (state) {
      case 3: // p and n
        T = gasModel->T(n,p);
        break;
      case 5: // p and rho
        T = gasModel->T(n,p);
        break;
      case 9: // p and T
        n = gasModel->n(p,T);
        break;
      case 10: // n and T
        break;
      case 12: // rho and T
        break;
      default:
        MYASSERT(false,"Dictionary must specify exactly two of (n OR rho),p,T");
    }
  }



}

namespace Gas
{
  namespace GasModels
  {


    Eigen::Vector4d
    GasModel::getU(const Settings::Dictionary& dict, const double T)
    const
    {

      Eigen::Vector4d U;
      U.setZero();

      if ( dict.hasMember("Ma") )
        {
          double Ma = dict.get<double>("Ma");

          if (dict.hasMember("direction"))
            {
              Settings::Dictionary ddict = dict.get<Settings::Dictionary>("direction");
              MYASSERT(ddict.size()>=3,"direction dict must be array size 3");
              ddict.get(U.data(),0,3);
              U.normalize();
            }
          else
            {
              U = Eigen::Vector4d::UnitX();
            }

          U *= Ma*sonicVel(T);
        }
      else if( dict.hasMember("U") )
        {
          if ( dict.get<Settings::Dictionary>("U").isArray() )
            {
              Settings::Dictionary udict = dict.get<Settings::Dictionary>("U");
              MYASSERT(udict.size()>=3,"U dict must be array size 3");
              udict.get(U.data(),0,3);
            }
          else
            {
              U = Eigen::Vector4d(dict.get<double>("U"),0.0,0.0,0.0);
            }

        }
      /*
        else
        {
          MYASSERT(false,"Dictionary missing velocity specification via either \"Ma\" or \"U\"");
        }
       */

      // yaw, pitch, roll of object
      // z-y'-x''

      if (dict.hasMember("yaw") || dict.hasMember("pitch") || dict.hasMember("roll"))
        {

          double yawA     = dict.get<double>("yaw",0.0);    //[deg]
          double pitchA   = dict.get<double>("pitch",0.0);  //[deg]
          double rollA    = dict.get<double>("roll",0.0);   //[deg]

          auto yaw = Eigen::AngleAxisd(
              yawA * M_PI/180., Eigen::Vector3d::UnitZ()
          ).toRotationMatrix();

          auto pitch = Eigen::AngleAxisd(
              pitchA * M_PI/180., yaw*Eigen::Vector3d::UnitY()
          ).toRotationMatrix();

          auto roll = Eigen::AngleAxisd(
              rollA * M_PI/180., pitch*yaw*Eigen::Vector3d::UnitX()
          ).toRotationMatrix();

          U.head<3>() = roll*pitch*yaw*U.head<3>();

        }

      return U;

    }




    double GasModel::getn(const Settings::Dictionary& dict)
    const
    {

      if (dict.hasMember("n"))
        return dict.get<double>("n");
      else if (dict.hasMember("rho"))
        return dict.get<double>("rho") / m_;
      else
        {
          double n,T;
          getNT(dict,this,n,T);
          return n;
        }
    }

    double GasModel::getT(const Settings::Dictionary& dict)
    const
    {
      if(dict.hasMember("T"))
        return dict.get<double>("T");
      else
        {
          double n,T;
          getNT(dict,this,n,T);
          return T;
        }
    }


    GasModel::GasModel(
        const double m,
        const double omega,
        const bool diatomic,
        const double gamma,
        const double Zrot_inf,
        const double Trot_ref,
        const double Cvib_1,
        const double Cvib_2,
        const double Tvib_ref )
    : m_(m)
    , R_(Constants::kB / m_)
    , omega_(omega)
    , diatomic_(diatomic)
    , gamma_( gamma >= 0. ? gamma : 1. + 2./( diatomic_ ? 5. : 3. ) )
    , Zrot_inf_( Zrot_inf >= 0. ? Zrot_inf : Constants::maxdbl )
    , Trot_ref_( Trot_ref >= 0. ? Trot_ref : 1.0 )
    , Cvib_1_( Cvib_1 >= 0. ? Cvib_1 : Constants::maxdbl )
    , Cvib_2_( Cvib_2 >= 0. ? Cvib_2 : 0.0 )
    , Tvib_ref_( Tvib_ref >= 0. ? Tvib_ref : 1.0 )
    {}


    GasModel::GasModel(const Settings::Dictionary& dict)
    : GasModel(
        dict.get<double>("m"),
        getOmega(dict),
        dict.get<bool>("diatomic",false),
        dict.get<double>("gamma",-1.),
        dict.get<double>("Zrot_inf",-1.),
        dict.get<double>("Trot_ref",-1.),
        dict.get<double>("Cvib_1",-1.),
        dict.get<double>("Cvib_2",-1.),
        dict.get<double>("Tvib_ref",-1.) )
    {

      if (diatomic_)
        {

          std::string msg = "Gas model dictionary for diatiomic gas missing value ";

          MYASSERT(dict.hasMember("Zrot_inf"),msg+std::string("\"Zrot_inf\""));
          MYASSERT(dict.hasMember("Trot_ref"),msg+std::string("\"Trot_ref_\""));
          MYASSERT(dict.hasMember("Cvib_1"),msg+std::string("\"Cvib_1_\""));
          MYASSERT(dict.hasMember("Cvib_2"),msg+std::string("\"Cvib_2_\""));
          MYASSERT(dict.hasMember("Tvib_ref"),msg+std::string("\"Tvib_ref_\""));
        }

    } // ctor

  } // namespace GasModels

} // namespace Gas
