/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * MacroState.cpp
 *
 *  Created on: Nov 20, 2014
 *      Author: kustepha
 */

#include "Gas/MacroState.h"

#include "Gas/GasModel.h"


namespace Gas
{

  void
  MacroState::update(
      const double rho,
      const double theta,
      const double v,
      const GasModels::GasModel* const gm )
  {
    n      = rho / (gm->m());
    T      = gm->T    ( theta   );
    mu     = gm->mu   ( T      );
    nu     = gm->nu   ( T,  n );
    mfp    = gm->mfp  ( T,  n );
    Ma     = gm->Ma   ( v,   T );
    gam    = gm->gamma( T      );

#ifndef NDEBUG
    assert_finite();
#endif
  }


} /* namespace Gas */
