/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * EqCondition.cpp
 *
 *  Created on: Dec 3, 2014
 *      Author: kustepha
 */

#include "Gas/EqCondition.h"
#include "Settings/Dictionary.h"

#include "Constants/Constants.h"

#include "Primitives/MyError.h"
#include "Primitives/MyRandom.h"

#include "FieldData/FieldData.h"

#include "GasModel.h"



namespace Gas
{



  // set conditions by dictionary
  EqCondition::RefCondition::RefCondition(
      const Settings::Dictionary& dict,
      const double V_ref,
      const GasModel* gasModel)
  : n_( gasModel->getn(dict) )
  , m_( gasModel->m() )
  , V_( V_ref )
  , np_( dict.get<uint64_t>("particles per cell") )
  , w_( m_*n_*V_ / static_cast<double>(np_) )
  {  }

  // set condition by dictionary and reference
  EqCondition::Condition::Condition(
      const Settings::Dictionary& dict,
      const GasModel* gasModel)
  : n_( gasModel->getn(dict) )
  , T_( gasModel->getT(dict) )
  , Trot_( dict.get<double>("T rot",T_))
  , Tvib_( dict.get<double>("T vib",T_))
  , U_( gasModel->getU(dict,T_))
  {  }

  // set conditions by explicit values and reference
  EqCondition::Condition::Condition(
      const double n,
      const double T,
      const Eigen::Vector4d& U)
  : n_(n)
  , T_(T)
  , Trot_(T)
  , Tvib_(T)
  , U_(U)
  { }

  // set conditions by explicit values and reference
  EqCondition::Condition::Condition(
      const double n,
      const double T,
      const double Trot,
      const double Tvib,
      const Eigen::Vector4d& U)
  : n_(n)
  , T_(T)
  , Trot_(Trot)
  , Tvib_(Tvib)
  , U_(U)
  { }



  // set conditions by dictionary
  EqCondition::EqCondition (
      const Settings::Dictionary& dict,
      const double V_ref,
      const GasModel* gasModel)
  : field_mask_(0)
  , ref_(dict,V_ref,gasModel)   // REFERENCE
  , conds_{Condition(dict,gasModel)} // ACTUAL
  {}


  // set reference conditions by separate dictionary
  EqCondition::EqCondition(
      const Settings::Dictionary& dict,
      const Settings::Dictionary& refDict,
      const double V_ref,
      const GasModel* gasModel)
  : field_mask_(0)
  , ref_(refDict,V_ref,gasModel)   // REFERENCE
  , conds_{Condition(dict,gasModel)} // ACTUAL
  {}




  // set non reference conditions by FieldData, reference by dict
  EqCondition::EqCondition(
      const FieldData* fData,
      const Settings::Dictionary& refDict,
      const double V_ref,
      const GasModel* gasModel)
  : field_mask_(uint64_t(-1))
  , ref_(refDict,V_ref,gasModel)
  {

    std::string msg = "Error constructing FieldEqCondition: ";


    MYASSERT(fData->hasVar("n"), msg + "missing variable \"n\"");
    MYASSERT(fData->hasVar("T"), msg + "missing variable \"T\"");
    MYASSERT(fData->hasVar("U"), msg + "missing variable \"U\"");
    MYASSERT(fData->hasVar("V"), msg + "missing variable \"V\"");
    MYASSERT(fData->hasVar("W"), msg + "missing variable \"W\"");

    const double* n = &(fData->getVarData("n")[0]);
    const double* T = &(fData->getVarData("T")[0]);
    const double* U = &(fData->getVarData("U")[0]);
    const double* V = &(fData->getVarData("V")[0]);
    const double* W = &(fData->getVarData("W")[0]);
    const double* TRot = T;
    const double* TVib = T;

    if (fData->hasVar("TRot"))
      TRot =  &(fData->getVarData("TRot")[0]);

    if (fData->hasVar("TVib"))
      TRot =  &(fData->getVarData("TVib")[0]);

    const uint64_t nData = fData->getNData("n");

    for (uint64_t i = 0; i < nData; ++i, ++n, ++T, ++TRot, ++TVib, ++U, ++V, ++W)
      conds_.emplace_back(*n,*T,*TRot,*TVib,Eigen::Vector4d({*U,*V,*W,0.0}));


  }



} /* namespace Gas */
