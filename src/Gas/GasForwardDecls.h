/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * GasForwardDecls.h
 *
 *  Created on: Oct 14, 2014
 *      Author: kustepha
 */

#ifndef GASFORWARDDECLS_H_
#define GASFORWARDDECLS_H_


namespace Gas
{

  class EqCondition;

  class MacroState;

  namespace GasModels
  {
    class GasModel;
  }

}

typedef Gas::GasModels::GasModel GasModel;



#endif /* GASFORWARDDECLS_H_ */
