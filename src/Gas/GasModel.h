/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * GasModel.h
 *
 *  Created on: Apr 22, 2014
 *      Author: Stephan Kuechlin
 */

#ifndef GASMODEL_H_
#define GASMODEL_H_

#include <Eigen/Dense>

#include "Constants/Constants.h"

#include "Primitives/MyIntrin.h"
#include "Primitives/MyError.h"
#include "Primitives/MyRandom.h"
#include "Primitives/Tensor.h"

namespace Settings
{
  class Dictionary;
}

namespace Gas
{

  namespace GasModels
  {

    class GasModel
    {

    protected:

      // molecular mass [kg]
      const double m_;

      // gas constant kB/m [J kg-1 K-1]
      const double R_;

      // exponent in the viscosity law, mu ~ T^omega
      const double omega_;

      // simulate as diatomic [t/f]
      const uint64_t diatomic_ = false;

      // ratio of specific heats [-]
      const double gamma_;

      // asymptotic rotational collision number
      const double Zrot_inf_;

      // rotational   temperature
      const double Trot_ref_;

      // constants for the vibrational collision number model
      const double Cvib_1_;
      const double Cvib_2_;

      // vibrational reference temperature
      const double Tvib_ref_;



    public:

      Eigen::Vector4d getU(const Settings::Dictionary& dict, const double T) const;

      double getn(const Settings::Dictionary& dict) const;

      double getT(const Settings::Dictionary& dict) const;

    public:

      // pressure from eqn of state
      static double p(const double n, const double T) { return n*Constants::kB*T; }

      // number density from eqn of state
      static double n(const double p, const double T) { return p/(Constants::kB*T); }

      // temperature from eqn of state
      static double T(const double n, const double p) { return p/(n*Constants::kB); }

    public:

      // Mach number
      double Ma(const double v, const double T) const { return v / fmax(1.0,sonicVel(T)); }

      // Mach number
      double Ma(const Eigen::Vector4d& C, const double T) const { return Ma(C.norm(),T); }

      // molecular mass
      double m() const { return m_; };

      // gas constant
      double R() const { return R_; };

      // [t/f] is gas diatomic?
      bool diatomic() const { return diatomic_; }

      // temperature [K] from energy units
      // impose lower limit of 5. Kelvin
      double T(const double theta) const { return fmax(theta/R_,5.0); }

      // ratio of specific heats [-]
      double gamma(const double T) const {
        static_cast<void>(T); // this model has no T dependence
        return gamma_;
      }

      // inverse most probable equilibrium thermal speed eq. 4.1
      double beta(const double T) const {
        return pow(2.*R_*T,-.5); }

      // equilibrium standard deviation of a translational velocity component
      double uStd(const double T) const { return sqrt(R_*T);  }

      // most probable equilibrium thermal speed, eq. 4.7 [m s-1]
      double cMp(const double T) const { return sqrt(2.*R_*T); }

      // average equilibrium thermal speed, eq. 4.8 [m s-1]
      double cAv(const double T) const { return 2.*pow(M_PI,-.5)*cMp(T); }

      // average value of relative thermal speed in equilibrium, eq. 4.46 [m s-1]
      double crAv(const double T) const {
        return Constants::sqrt2*cAv(T); }

      // equilibrium specific translational energy [J kg-1]
      double esEqTr(const double T) const { return 1.5*R_*T; }


      // speed of sound [m s-1]
      double sonicVel(const double T) const { return sqrt(gamma_*R_*T); }


      // rotational collision number
      // eq 10
      double zRot(double T) const {
        T = Trot_ref_ / fmax(T,5.0);
        return fmax(
            Zrot_inf_ / (1. + .5*sqrt(Constants::piq*T) + (.25*Constants::pisq + Constants::pi)*T),
            1.0);
        //    return 5.;
      }

      // vibrational collision number
      double zVib(double T) const {
        T = fmax(T,5.0);
        return fmax(
            (Cvib_1_*pow(T,-omega_))*exp(Cvib_2_*pow(T,-1./3.)),
            1.0); }

      // inverse rotational collision number
      // eq 10
      double zRotInv(double T) const {
        if(!diatomic()) return 0.;
        return 1./zRot(T);
      }

      // inverse vibrational collision number
      double zVibInv(double T) const {
        if(!diatomic()) return 0.;
        return 1./zVib(T);
      }

      // probability of rotational re-distribution during collision
      double pRotRedist(const double T) const { return zRotInv(T); }


      // equilibrium specific rotational energy [J kg-1] == [m2 s-2]
      double esEqRot(const double T) const { return diatomic() ? R_*T : 0.; }

      // rotational temperature from specific rotational energy [K]
      double TRot(const double esRot) const { return esRot / R_; }

      // standard deviation of equilibrium rotational component [m s-1]
      double omegaStd(const double T) const { return sqrt(esEqRot(T)); }

      // equilibrium specific vibrational energy [J kg-1]
      double esEqVib(const double T) const {
        return diatomic() ? R_*Tvib_ref_ / expm1(Tvib_ref_/fmax(T,5.0)) : 0.; }

      // vibrational temperature from specific vibrational energy [K]
      double TVib(const double esVib) const {
        return (esVib <= 0.) ? 0. : Tvib_ref_ / log1p(R_*Tvib_ref_ / esVib); }

      // standard deviation of equilibrium vibrational component [m s-1]
      double xiStd(const double T) const { return sqrt(esEqVib(T)); }


      // rotational specific heat divided by Boltzmann constant
      double cRot(const double T) const {
        static_cast<void>(T);
        return 1.; }

      // vibrational specific heat divided by Boltzmann constant
      double cVib(double T) const {
        T = .5*(Tvib_ref_ / fmax(T,5.0));
        return pow( T / sinh(T), 2. ); }

    private:

      template<uint8_t zeta>
      __attribute__((__always_inline__))
      inline double pratLB(const double ef) const {
        // Bird, Eq. 5.21
        static constexpr double zetad = static_cast<double>(zeta); // number of internal classical modes
        return pow( ef*(zetad+.5-omega_)/(1.5-omega_), (1.5-omega_) ) *
            pow( (1.-ef)*(zetad+.5-omega_)/(zetad-1.), (zetad-1.) );
      }

      // sample the Larsen-Borgnakke distribution of the fraction of post collision translational energy
      template<uint8_t zeta> // number of internal classical modes
      __attribute__((__always_inline__))
      inline double sampleLB(MyRandom::Rng& rndGen) const {
        double ef; // post-collision fracion of translational energy
        while( pratLB<zeta>(ef = rndGen.uniform()) < rndGen.uniform() ) {}
        return ef;
      }

    public:

      // redistribute translational /  internal energies during in-elastic collision
      template<uint8_t zeta>
      void redist(double* et, double* ei1, double* ei2, MyRandom::Rng& rndGen) const
      {

        // total internal energy pre collision
        const double Ei_pre = *ei1 + *ei2;
        // total translational energy pre collision
        const double Et_pre = *et;
        // total collision energy, a constant
        const double Ec = Ei_pre + Et_pre;

        // compute post-collision translational energy as fraction
        // of total collision energy, by sampling from LB distribution
        *et = Ec*sampleLB<zeta>(rndGen);

        // remaining collision energy goes to internal modes
        const double Ei_post = Ec - *et;

        if(zeta==2) {
            // "the special case of two internal dof is such that all values of
            //  the single molecule post-collision internal energy are equally
            //  probable" Bird p. 106

            *ei1 = Ei_post*rndGen.uniform();
            *ei2 = Ei_post - *ei1;

        } else {

            static constexpr double zetad = static_cast<double>(zeta);
            double ei1f;
            do {
                ei1f = rndGen.uniform();
            } while( pow(2.,zetad-2.)*pow(ei1f*(1.-ei1f),.5*zetad-1.) < rndGen.uniform() );

            *ei1 = Ei_post*ei1f;
            *ei2 = Ei_post - *ei1;

        }


      }



    public:

      // [s-1] equilibrium collision frequency
      virtual double nu(const double T, const double n) const = 0;

      // [m] equilibrium mean free path
      virtual double mfp(const double T, const double n) const = 0;

      // [Pa s] equilibrium viscosity
      virtual double mu(const double T) const = 0;

      // [m2] squared reference diameter
      virtual double dsq(const double T) const = 0;

      // total collision cross section times relative velocity magnitude
      // mr is the reduced mass
      // crsq is the squared norm of the relative velocity vector
      virtual double sigmaTCr(const double mr, const double crsq) const = 0;
      virtual void sigmaTCr4(v4df& res, const v4df& mr,const v4df& crsq) const = 0;

      // maximum total collision cross section times relative velocity magnitude
      // a constant assumed to be a supremum of sigmaCr(mr,crsq)
      virtual double sigmaCrMax() const = 0;

      // return post collision relative velocity
      virtual v4df collide(const v4df& cr, MyRandom::Rng& rndGen) const = 0;

      // collision integral Omega(l=2,r), as defined in
      // Kremer, G.M. (2010) -
      // An Introduction to the Boltzmann Equation and Transport Processes
      // Eq. (3.40)
      // valid for r > 1, normalized by Omega(l=2,r=2)
      template<uint8_t r>
      __attribute__((__always_inline__))
      inline double Omega2() const {
        static_assert(r>4);
        return tgamma( (5./2.)+double(r)-omega_ ) / tgamma( (9./2.)-omega_ );
      }

      // collision integral Omega(l=2,r=2), as defined in
      // Kremer, G.M. (2010) -
      // An Introduction to the Boltzmann Equation and Transport Processes
      // Eq. (3.40)
      virtual double Omega22(const double T) const = 0;

    public:

      enum class Approx : uint8_t {
        GRAD13,
        GRAD26,
        INVALID
      };

      // Production of 2nd order moments (stresses)
      // [m2 s-2]*[s-1]
      template<uint8_t i, uint8_t j, class C>
      double P0(const Approx, const C& c) const;

      // Production of 3rd order moments
      // [m3 s-3]*[s-1]
      template<uint8_t i,uint8_t j, uint8_t k, class C>
      double P0(const Approx, const C& c) const;

      // Production of 1x contracted 3rd order moments (2x heat fluxes)
      // [m3 s-3]*[s-1]
      template<uint8_t i,class C>
      double P2(const Approx, const C& c) const;

      // Production of 1x contracted 4th order moments
      // [m4 s-4]*[s-1]
      template<uint8_t i,uint8_t j, class C>
      double P2(const Approx, const C& c) const;

      // Production of 2x contracted 4th order moments
      // [m4 s-4]*[s-1]
      template<class C>
      double P4(const Approx, const C& c) const;


    public:

      GasModel(const Settings::Dictionary& dict);

      GasModel(
          const double m,
          const double omega,
          const bool diatomic = false,
          const double gamma      = -1.,
          const double Zrot_inf   = -1.,
          const double Trot_ref   = -1.,
          const double Cvib_1     = -1.,
          const double Cvib_2     = -1.,
          const double Tvib_ref   = -1. );


      virtual ~GasModel(){};
    };

    template<> __attribute__((__always_inline__)) inline double GasModel::Omega2<2>() const { return 1.; }
    template<> __attribute__((__always_inline__)) inline double GasModel::Omega2<3>() const { return ( 9./2.) - omega_; }
    template<> __attribute__((__always_inline__)) inline double GasModel::Omega2<4>() const { return (99./4.) + omega_*(omega_-10.); }

  } // namespace GasModels

} // namespace Gas

//#include "GasModelSampling.hpp"
#include "Productions.h"

#endif /* GASMODEL_H_ */
