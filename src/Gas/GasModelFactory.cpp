/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * GasModelFactory.cpp
 *
 *  Created on: Aug 5, 2014
 *      Author: kustepha
 */


#include <map>

#include "Gas/GasModelFactory.h"
#include "Gas/GasModel.h"

#include "Settings/Dictionary.h"
#include "Primitives/MyError.h"
#include "Primitives/MyString.h"

#include "VSS.h"

namespace Gas
{

  namespace GasModels
  {

    enum GasModelType
    {
      VSS
      //      VSS,
      //      VHS,
      //      HS,
      //      MAXWELL
    };

    static std::map<std::string,GasModelType> GasModelTypeNameMap = {
        {"VSS",VSS},
        {"VHS",VSS},
        {"HS",VSS},
        {"MAXWELL",VSS},
    };


    std::unique_ptr<GasModel>
    makeGasModel(
        const Settings::Dictionary& dict)
        {


      std::unique_ptr<GasModel> p;
      std::string type = dict.get<std::string>("type");
      MyString::toUpper(type);

      auto gtit = GasModelTypeNameMap.find(type);
      MYASSERT(gtit != GasModelTypeNameMap.end(),
               std::string("unknown gas model type \"") + dict.get<std::string>("type") + std::string("\""));


      switch (gtit->second)
      {
        case VSS:
          p = std::unique_ptr<GasModel>(new class VSS(dict));
          break;
      }

      return p;

        }


  }

}


