/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * InternalState.h
 *
 *  Created on: Dec 17, 2014
 *      Author: kustepha
 */

#ifndef GAS_INTERNALSTATE_H_
#define GAS_INTERNALSTATE_H_

#include <mpi.h>

namespace Gas
{
  namespace GasModels
  {
    class GasModel;
  }

  struct InternalState
  {

  public:

    double T_rot = 0.; // [K] rotational temperature
    double T_vib = 0.; // [K] vibrational temperature
    double Z_rot = 0.; // [#] rotational collision number
    double Z_vib = 0.; // [#] vibrational collision number

    double esEq_rot = 0.; // [J kg-1] equilibrium specific rotational energy
    double esEq_vib = 0.; // [J kg-1] equilibrium specific vibrational energy
    double c_rot = 0.; // [-] rotational specific heat divided by kB
    double c_vib = 0.; // [-] vibrational specific heat divided by kB


  public:
    void update(
        const double es_rot,
        const double es_vib,
        const double T,
		const GasModels::GasModel* gasModel);

  };

} /* namespace Gas */


#endif /* GAS_INTERNALSTATE_H_ */
