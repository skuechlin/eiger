/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * Productions.h
 *
 *  Created on: Jan 8, 2019
 *      Author: kustepha
 */

#ifndef SRC_GAS_PRODUCTIONS_H_
#define SRC_GAS_PRODUCTIONS_H_


namespace {
  template<typename T, int... j>
  struct R {
    const T& c;
    template<int... i> double operator()() const {
      return c.template R<i...,j...>();
    }
    explicit R(const T& _c):c(_c){}
  };

  template<typename T, int... j>
  struct sigma {
    const T& c;
    template<int... i> double operator()() const {
      return c.template sigma<i...,j...>();
    }
    explicit sigma(const T& _c):c(_c){}
  };

  template<typename T, int... j>
  struct q {
    const T& c;
    template<int... i> double operator()() const {
      return c.template q<i...,j...>();
    }
    explicit q(const T& _c):c(_c){}
  };

  template<typename T, int... j>
  struct m {
    const T& c;
    template<int... i> double operator()() const {
      return c.template m<i...,j...>();
    }
    explicit m(const T& _c):c(_c){}
  };

}

namespace Gas {
  namespace GasModels{

    // (specific) production terms, according to
    // Gupta, V.K. and Torrilhon, M. (2012) -
    // Automated Boltzmann collision integrals for moment equations
    //
    // Based on Grad's 13/26 moment closure
    //
    // cf. also Kremer, G.M. (2010) -
    // An Introduction to the Boltzmann Equation and Transport Processes in Gases
    // pp. 116, Eqs. (4.41) (4.42) (4.43)
    //


    // Production of stresses
    // [m2 s-2]*[s-1]
    template<uint8_t i, uint8_t j, class C>
    double GasModel::P0(const GasModel::Approx approx, const C& c) const {

      //      static constexpr double lb = 1.e-6;
      //
      //      MYASSERT(fabs(trace(::sigma<C>(c)))<lb,"sigma");
      //      MYASSERT(fabs(trace(::R<C>(c)))<lb,"R");
      //      MYASSERT(fabs(trace(::m<C,0>(c)))<lb,"m 0");
      //      MYASSERT(fabs(trace(::m<C,1>(c)))<lb,"m 1");
      //      MYASSERT(fabs(trace(::m<C,2>(c)))<lb,"m 2");

      // see Kremer, Eq. (4.42)

      // specific (divided by mass density) inverse stress tensor relaxation time scale
      // Kremer, Eq. (4.41)

      const double T = c.T();
      const double nur = c.theta()/mu(T);

      const double sij = ::sigma<C,i,j>(c)();

      const double ssij = contract(::sigma<C,i>(c),::sigma<C,j>(c)) -
          delta<i,j>( (1./3.)*contract2(::sigma<C>(c),::sigma<C>(c)) );

      const double qqij = ::q<C,i>(c)()*::q<C,j>(c)() -
          delta<i,j>( (1./3.)*contract(::q<C>(c),::q<C>(c)) );

      double ret = -sij +
          ssij*(  .5        - (1./7. )*Omega2<3>()                         )/c.p() +
          qqij*( (63./100.) - (9./25.)*Omega2<3>() + (1./25. )*Omega2<4>() )/(c.p()*c.theta());


      if (approx == Approx::GRAD26) {

          const double sdij = sij*c.Delta();
          const double rij  = ::R<C,i,j>(c)();
          const double rdij = rij*c.Delta();

          const double srij = contract(::sigma<C,i>(c),::R<C,j>(c)) +
              contract(::sigma<C,j>(c),::R<C,i>(c)) -
              delta<i,j>( (2./3.)*contract2(::sigma<C>(c),::R<C>(c)) );

          const double rrij = contract(::R<C,i>(c),::R<C,j>(c)) -
              delta<i,j>( (1./3.)*contract2(::R<C>(c),::R<C>(c)) );

          const double mmij = contract2(::m<C,i>(c),::m<C,j>(c)) -
              delta<i,j>( (1./3.)*contract3(::m<C>(c),::m<C>(c)) );

          const double mqij = contract(::m<C,i,j>(c),::q<C>(c));

          ret +=
              rij*(.25 - (1./14.)*Omega2<3>())/c.theta() +
              (
                  srij*( -( 9./56. ) + (9./98.)*Omega2<3>() - (1./98. )*Omega2<4>() ) +
                  sdij*( -(21./160.) + (3./40.)*Omega2<3>() - (1./120.)*Omega2<4>() ) +
                  mmij*(  ( 1./8.  ) - (1./14.)*Omega2<3>() + (1./126.)*Omega2<4>() ) +
                  mqij*(  ( 9./20. ) - (9./35.)*Omega2<3>() + (1./35. )*Omega2<4>() )
              )/ (c.p()*c.theta()) +
              (
                  rrij*( (99./1568.) - (297./5488.)*Omega2<3>() + (33./2744.)*Omega2<4>() - (1./1372.)*Omega2<5>() ) +
                  rdij*( (33./640. ) - ( 99./2240.)*Omega2<3>() + (11./1120.)*Omega2<4>() - (1./1680.)*Omega2<5>() )
              )/(c.p()*c.theta()*c.theta());

      }

      return nur*ret;

    }

    // Production of 3rd order moments
    // [m3 s-3]*[s-1]
    template<uint8_t i,uint8_t j, uint8_t k, class C>
    double GasModel::P0(const GasModel::Approx, const C& c) const {



      const double T = c.T();
      const double nur = c.theta()/mu(T);
      const double nu_rot = zRotInv(T)*(4./M_PI); // here: nu_rot / nur
      const double nu_vib = zVibInv(T)*(4./M_PI);

      const double trqijk =
          delta<i,j>( c.template q<k>() ) +
          delta<i,k>( c.template q<j>() ) +
          delta<j,k>( c.template q<i>() );

      const double trqrotijk =
          delta<i,j>( c.template q_rot<k>() ) +
          delta<i,k>( c.template q_rot<j>() ) +
          delta<j,k>( c.template q_rot<i>() );
      const double trqvibijk =
          delta<i,j>( c.template q_vib<k>() ) +
          delta<i,k>( c.template q_vib<j>() ) +
          delta<j,k>( c.template q_vib<i>() );

      const double trqintijk =
          -(10./9.)*(nu_rot*cRot(T)   + nu_vib*cVib(T)  )*trqijk +
          ( 5./3.)*(nu_rot*trqrotijk + nu_vib*trqvibijk);

      const double sqijk =
          ( c.template sigma<i,j>() )*( c.template q<k>() ) +
          ( c.template sigma<i,k>() )*( c.template q<j>() ) +
          ( c.template sigma<j,k>() )*( c.template q<i>() );

      const double trsqijk =
          delta<i,j>( contract(::sigma<C,k>(c),::q<C>(c)) ) +
          delta<i,k>( contract(::sigma<C,j>(c),::q<C>(c)) ) +
          delta<j,k>( contract(::sigma<C,i>(c),::q<C>(c)) );

      const double mijk = c.template m<i,j,k>();

      const double mdijk = mijk*c.Delta();
      const double msijk =
          contract(::m<C,i,j>(c),::sigma<C,k>(c)) +
          contract(::m<C,i,k>(c),::sigma<C,j>(c)) +
          contract(::m<C,j,k>(c),::sigma<C,i>(c));
      const double mrijk =
          contract(::m<C,i,j>(c),::R<C,k>(c)) +
          contract(::m<C,i,k>(c),::R<C,j>(c)) +
          contract(::m<C,j,k>(c),::R<C,i>(c));
      const double rqijk =
          ( c.template R<i,j>() )*( c.template q<k>() ) +
          ( c.template R<i,k>() )*( c.template q<j>() ) +
          ( c.template R<j,k>() )*( c.template q<i>() );

      const double trqdijk = trqijk*c.Delta();
      const double trrqijk =
          delta<i,j>( contract(::R<C,k>(c),::q<C>(c)) ) +
          delta<i,k>( contract(::R<C,j>(c),::q<C>(c)) ) +
          delta<j,k>( contract(::R<C,i>(c),::q<C>(c)) );
      const double trmsijk =
          delta<i,j>( contract2(::m<C,k>(c),::sigma<C>(c)) ) +
          delta<i,k>( contract2(::m<C,j>(c),::sigma<C>(c)) ) +
          delta<j,k>( contract2(::m<C,i>(c),::sigma<C>(c)) );
      const double trmrijk =
          delta<i,j>( contract2(::m<C,k>(c),::R<C>(c)) ) +
          delta<i,k>( contract2(::m<C,j>(c),::R<C>(c)) ) +
          delta<j,k>( contract2(::m<C,i>(c),::R<C>(c)) );

      double ret =  -(3./2.)*mijk - (4./15.)*trqijk + (1./5.)*trqintijk +
          (
              (sqijk -      6.*trsqijk)*( -( 1./20. ) + ( 1./70. )*Omega2<3>() ) +
              (msijk - (2./3.)*trmsijk)*(  ( 1./4.  ) - ( 1./14. )*Omega2<3>() )
          )/c.p() +
          (
              (mdijk                  )*( -(27./320.) + (27./560.)*Omega2<3>() - (3./560.)*Omega2<4>() ) +
              (trqdijk                )*(  ( 7./200.) - ( 1./50. )*Omega2<3>() + (1./450.)*Omega2<4>() ) +
              (mrijk -      2.*trmrijk)*( -( 3./112.) + ( 3./196.)*Omega2<3>() - (1./588.)*Omega2<4>() ) +
              (rqijk - (2./3.)*trrqijk)*(  ( 9./80. ) - ( 9./140.)*Omega2<3>() + (1./140.)*Omega2<4>() )
          ) / (c.p() * c.theta());

      return nur*ret;
    }

    // Production of 2x heat fluxes (1x contracted 3rd order moments)
    // [m3 s-3]*[s-1]
    template<uint8_t i,class C>
    double GasModel::P2(const GasModel::Approx approx, const C& c) const {

      // see Kremer, Eq. (4.43)

      // inverse stress tensor relaxation time scale
      // Kremer, Eq. (4.41)
      const double T = c.T();
      const double nur = c.theta()/mu(T);
      const double nu_rot = nur*zRotInv(T)*(4./M_PI);
      const double nu_vib = nur*zVibInv(T)*(4./M_PI);

      const double qi = ::q<C,i>(c)();
      const double qsi = contract(::q<C>(c),::sigma<C,i>(c));

      double ret = -(2./3.)*qi +
          qsi*( (7./10.) - (1./5.)*Omega2<3>() ) / c.p();

      if (approx == Approx::GRAD26) {

          const double smi = contract2(::m<C,i>(c),::sigma<C>(c));
          const double rmi = contract2(::m<C,i>(c),::R<C>(c));
          const double qri = contract(::R<C,i>(c),::q<C>(c));
          const double dqi = qi*c.Delta();

          ret +=
              smi*( -(1./6. ) + (1./21.)*Omega2<3>() )/c.p() +
              (
                  qri*( -(3./40.) + (3./70.)*Omega2<3>() - (1./210.)*Omega2<4>() ) +
                  rmi*(  (3./28.) - (3./49.)*Omega2<3>() + (1./147.)*Omega2<4>() ) +
                  dqi*(  (7./80.) - (1./20.)*Omega2<3>() + (1./180.)*Omega2<4>() )
              )/(c.p()*c.theta());
      }

      return 2.*nur*ret + // contributions of internal modes
          -(10./9.)*(nu_rot*cRot(T) + nu_vib*cVib(T))*qi +
          (5./3.)*(nu_rot*(c.template q_rot<i>()) + nu_vib*(c.template q_vib<i>()));

    }


    // Production of 1x contracted 4th order moments
    // [m4 s-4]*[s-1]
    template<uint8_t i,uint8_t j, class C>
    double GasModel::P2(const GasModel::Approx, const C& c) const {

      const double T = c.T();
      const double nur = c.theta()/mu(T);
      const double sij = ::sigma<C,i,j>(c)();
      const double rij = ::R<C,i,j>(c)();
      const double ssij = contract(::sigma<C,i>(c),::sigma<C,j>(c));
      const double qqij = ::q<C,i>(c)()*::q<C,j>(c)();

      const double sdij = sij*c.Delta();
      const double rdij = rij*c.Delta();
      const double srij = contract(::sigma<C,i>(c),::R<C,j>(c)) + contract(::sigma<C,j>(c),::R<C,i>(c));
      const double mmij = contract2(::m<C,i>(c),::m<C,j>(c));
      const double mqij = contract(::m<C,i,j>(c),::q<C>(c));
      const double rrij = contract(::R<C,i>(c),::R<C,j>(c));

      double ret =
          -sij*( ( 7./2. ) + Omega2<3>() )*c.theta()
          -rij*( ( 1./24.) + (1./14.)*Omega2<4>() ) +
          ssij*( (19./12.) - (1./7. )*Omega2<4>() )/c.rho() +
          (
              qqij*(  (1351./600. ) - (193./300. )*Omega2<3>() - (11./50.  )*Omega2<4>() + ( 1./25.  )*Omega2<5>() ) +
              srij*( -(  19./48.  ) + ( 19./168. )*Omega2<3>() + (11./196. )*Omega2<4>() - ( 1./98.  )*Omega2<5>() ) +
              sdij*( -(1687./2880.) + (241./1440.)*Omega2<3>() + (11./240. )*Omega2<4>() - ( 1./120. )*Omega2<5>() ) +
              mmij*(  (  17./48.  ) - ( 17./168. )*Omega2<3>() - (11./252. )*Omega2<4>() + ( 1./126. )*Omega2<5>() ) +
              mqij*(  ( 373./120. ) - (373./420. )*Omega2<3>() - (11./70.  )*Omega2<4>() + ( 1./35.  )*Omega2<5>() )
          ) / c.p() +
          (
              rrij*(  ( 663./3136.) - (663./5488.)*Omega2<3>() - (13./1029.)*Omega2<4>() + (13./1372.)*Omega2<5>() - (1./1372.)*Omega2<6>() ) +
              rdij*(  (  61./256. ) - ( 61./448. )*Omega2<3>() - (31./5040.)*Omega2<4>() + (13./1680.)*Omega2<5>() - (1./1680.)*Omega2<6>() )
          ) / (c.p()*c.theta());



      if(i==j) {

          const double ss = contract2(::sigma<C>(c),::sigma<C>(c));
          const double qq = contract(::q<C>(c),::q<C>(c));

          ret +=
              ss*( -(3./4.) + (1./21.)*Omega2<4>() )/c.rho() +
              qq*( -(77./600.) + (11./300.)*Omega2<3>() + (11./150.)*Omega2<4>() - (1./75. )*Omega2<5>() )/c.p();

          const double dd = c.Delta()*c.Delta();
          const double mm = contract3(::m<C>(c),::m<C>(c));
          const double sr = contract2(::sigma<C>(c),::R<C>(c));
          const double rr = contract2(::R<C>(c),::R<C>(c));

          ret +=
              -( 2./9. )*(c.Delta()) +
              (
                  mm*( -(11./48. ) + (11./168.)*Omega2<3>() + (11./756.)*Omega2<4>() - (1./378.)*Omega2<5>() ) +
                  sr*(  ( 3./8.  ) - ( 3./28. )*Omega2<3>() - (11./294.)*Omega2<4>() + (1./147.)*Omega2<5>() )
              ) / c.p() +
              (
                  dd*(  (  7./240. ) - (  1./60.  )*Omega2<3>() + ( 1./540. )*Omega2<4>() ) +
                  rr*( -(165./3136.) + (165./5488.)*Omega2<3>() + (11./2058.)*Omega2<4>() -
                      (13./4116.)*Omega2<5>() + (1./4116.)*Omega2<6>() )
              ) / (c.p()*c.theta());


      }

      return nur*ret;

    }

    // Production of 2x contracted 4th order moments
    // [m4 s-4]*[s-1]
    template<class C>
    double GasModel::P4(const GasModel::Approx, const C& c) const {


      const double T = c.T();
      const double nur = c.theta()/mu(T);
      const double ss = contract2(::sigma<C>(c),::sigma<C>(c));
      const double qq = contract(::q<C>(c),::q<C>(c));

      double ret = -(2./3.)*ss/c.rho() + qq*(  (28./15.) - (8./15.)*Omega2<3>() )/c.p();

      const double dd = c.Delta()*c.Delta();
      const double mm = contract3(::m<C>(c),::m<C>(c));
      const double sr = contract2(::sigma<C>(c),::R<C>(c));
      const double rr = contract2(::R<C>(c),::R<C>(c));

      ret +=
          -(2./3.)*c.Delta() +
          (
              sr*(  ( 1./3. ) - (2./21.)*Omega2<3>() ) +
              mm*( -( 1./3. ) + (2./21.)*Omega2<3>() )
          ) / c.p() + (
              rr*( (3./56.) - (3./98.)*Omega2<3>() + (1./294.)*Omega2<4>() ) +
              dd*( (7./80.) - (1./20.)*Omega2<3>() + (1./180.)*Omega2<4>() )
          ) / (c.p()*c.theta());

      return nur*ret;


    }


  } // namespace GasModels
}// namepace Gas


#endif /* SRC_GAS_PRODUCTIONS_H_ */
