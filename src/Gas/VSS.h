/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * VSS.h
 *
 *  Created on: Apr 22, 2014
 *      Author: Stephan Kuechlin
 */

#ifndef VSS_H_
#define VSS_H_


#include <math.h>

#include "GasModel.h"
#include "Constants/Constants.h"

namespace Settings
{
  class Dictionary;
}

namespace Gas
{
  namespace GasModels
  {


    class VSS: public GasModel
    {

    protected:

      // reference temperature
      const double T_ref_;

      // scattering parameter in VSS logic
      // 1. for VHS
      const double alpha_;
      const double alpha_recp_;

      // derived

      // squared reference diameter (diameter at T_ref_ squared)
      double dsq_ref_;

      // reference viscosity
      double mu_ref_;

      // reference total collision cross section
      double sigmaT_ref_;

      // reference collision frequency at n == 1
      double nu_ref_;

    private:

      // compute mu_ref (eq. 4.62)
      double mu_ref() const
      {
        return 5.*(alpha_+1.)*(alpha_+2.)*sqrt(m_*Constants::kB*T_ref_/Constants::pi)
            / ( 4.*alpha_*(5.-2.*omega_)*(7.-2.*omega_)*dsq_ref_ );
      }

      // compute dsq_ref (eq. 4.62)
      double dsq_ref() const
      {
        return
            5.*(alpha_+1.)*(alpha_+2.)*sqrt(m_*Constants::kB*T_ref_/Constants::pi)
            / ( 4.*alpha_*(5.-2.*omega_)*(7.-2.*omega_)*mu_ref_ );
      }

      // compute sigmaT_ref_;
      double sigmaT_ref() const {
        return Constants::pi * dsq_ref_;
      }

      // compute nu_ref_ (eq. 4.64, T == T_ref, n == 1)
      double nu_ref() const {
        return 4.*dsq_ref_*sqrt(Constants::pi*R_*T_ref_);
      }

    public:

      // [m2] diameter squared
      double dsq(const double T) const override {
        return dsq_ref_*pow( T/T_ref_, (.5- omega_) );
      }

      // [s-1] equilibrium collision frequency (eq. 4.64)
      double nu(const double T, const double n) const override {
        return n * nu_ref_ * pow( T/T_ref_, 1.0 - omega_ ); }

      // [m] equilibrium mean free path (eq. 4.65)
      double mfp(const double T, const double n) const override {
        return pow( T/T_ref_, omega_ - .5 ) / (Constants::sqrt2*sigmaT_ref_*fmax(n,1.e-60)); }

      // [Pa s] equilibrium viscosity (eq. 3.61)
      double mu(const double T) const override {
        return mu_ref_ * pow( T/T_ref_, omega_); }

      // total collision cross section
      // as a function of reduced mass and relative velocity magnitude squared
      // Bird, Eqs. 3.60, 4.63
      __attribute__((__always_inline__,__hot__))
      inline double sigmaT(const double mr, const double crsq) const {
        static const double fac = sigmaT_ref_ * pow( (2.*Constants::kB*T_ref_), omega_-.5 ) /
            tgamma(5./2. - omega_);
        return  fac * pow( mr*crsq, .5-omega_ );
      }

      // total collision cross section
      // as a function of reduced mass and relative velocity magnitude squared
      // Bird, Eqs. 3.60, 4.63
      __attribute__((__always_inline__,__hot__))
     inline  v4df sigmaT4(const v4df& mr, const v4df& crsq) const {
        static const v4df fac =  MyIntrin::set(sigmaT_ref_ * pow( (2.*Constants::kB*T_ref_), omega_-.5 ) /
            tgamma(5./2. - omega_));
        return  fac * MyIntrin::pow4( mr*crsq, .5-omega_ );
      }

      // total collision cross section times relative velocity magnitude
      // based on eq. 4.63
      // mr is the reduced mass
      // crsq is the squared norm of the relative velocity vector
      __attribute__((__always_inline__,__hot__))
      inline double sigmaTCr(const double mr, const double crsq) const override {
        return sigmaT(mr,crsq)*sqrt(crsq);
      }

      __attribute__((__always_inline__,__hot__))
      inline void sigmaTCr4(
          v4df& res,
          const v4df& mr,
          const v4df& crsq) const override {
        res = MyIntrin::sqrt4(crsq)*sigmaT4(mr,crsq); }

      // maximum total collision cross section times relative velocity magnitude
      // a constant assumed to be a supremum of sigmaCr(mr,crsq)
      double sigmaCrMax() const override {
        return 10.*sigmaTCr( .5*m(), pow(crAv(T_ref_),2.) ); }

      // update relative velocity via binary interaction
      v4df collide(const v4df& cr, MyRandom::Rng& rndGen) const override;

    public:

      // collision integral Omega(l=2,r=2), as defined in
      // Kremer, G.M. (2010) -
      // An Introduction to the Boltzmann Equation and Transport Processes
      // Eq. (3.40)
      double Omega22(const double T) const override {
        return dsq(T) * alpha_*(2.*omega_-7.)*(2.*omega_-5.) /
            ( 4.*(1. + alpha_)*(2. + alpha_) );
      }



    public:

      VSS(
          const double m,
          const double omega,
          const double alpha,
          const double mu_ref,
          const double T_ref,
          const bool diatomic = false,
          const double gamma      = -1.,
          const double Zrot_inf   = -1.,
          const double Trot_ref   = -1.,
          const double Cvib_1     = -1.,
          const double Cvib_2     = -1.,
          const double Tvib_ref   = -1. );

      VSS(const Settings::Dictionary& dict);

      virtual ~VSS(){};
    };


  } // namespace Models
} // namespace Gas

#endif /* VSS_H_ */
