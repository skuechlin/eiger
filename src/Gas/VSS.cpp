/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * VSS.cpp
 *
 *  Created on: Apr 22, 2014
 *      Author: Stephan Kuechlin
 */

#include <limits>

#include "Gas/VSS.h"

#include "Settings/Dictionary.h"

#include "Primitives/MyRandom.h"
#include "Primitives/MyError.h"
#include "Primitives/MyString.h"


#include "Parallel/MyMPI.h"

using namespace Eigen;
using namespace Constants;
using namespace Gas;
using namespace GasModels;


namespace
{

  double getAlpha(const Settings::Dictionary& dict)
  {
    std::string type = dict.get<std::string>("type");
    MyString::toUpper(type);

    if ( type.compare("VSS") == 0 )
      return dict.get<double>("alpha");
    else
      return 1.0;

  }


  int
  getDRefMuRefState(const Settings::Dictionary& dict)
  {
    int state = 0;

    if (dict.hasMember("d_ref"))
      state += 1;

    if (dict.hasMember("mu_ref"))
      state += 2;

    return state;
  }

}


VSS::VSS(
    const double m,
    const double omega,
    const double alpha,
    const double mu_ref,
    const double T_ref,
    const bool diatomic,
    const double gamma,
    const double Zrot_inf,
    const double Trot_ref,
    const double Cvib_1,
    const double Cvib_2,
    const double Tvib_ref
)
: GasModel(m,omega,diatomic,gamma,Zrot_inf,Trot_ref,Cvib_1,Cvib_2,Tvib_ref)
, T_ref_(T_ref)
, alpha_(alpha)
, alpha_recp_(1./alpha_)
{
  mu_ref_ = mu_ref;
  dsq_ref_ = dsq_ref();
  sigmaT_ref_ = sigmaT_ref();
  nu_ref_ = nu_ref();
}


VSS::VSS(const Settings::Dictionary& dict)
: GasModel(dict)
, T_ref_(dict.get<double>("T_ref"))
, alpha_(getAlpha(dict))
, alpha_recp_(1./alpha_)
{


  // set derived

  switch (getDRefMuRefState(dict))
  {
    case 1: // d_ref set
      dsq_ref_ = __builtin_powi(dict.get<double>("d_ref"),2);
      mu_ref_ = mu_ref();
      break;
    case 2: // mu_ref set
      mu_ref_ = dict.get<double>("mu_ref");
      dsq_ref_ = dsq_ref();
      break;
    default:
      MYASSERT(false,"VSS dictionary must specify \"d_ref\" OR \"mu_ref\"");
      break;
  }

  // reference total collision cross section
  sigmaT_ref_ = sigmaT_ref();

  // reference collision frequency at n == 1
  nu_ref_ = nu_ref();

  //  std::cout << "GasModel constructed, mu(300) = " << mu(300) << std::endl;

  if (MyMPI::isRoot(MPI_COMM_WORLD,0)) {

      std::cout << "\nconstructed VSS GasModel with:\n";
      std::cout << "  diatomic:   " << diatomic_      << "\n";
      std::cout << "  sigmaT_ref: " << sigmaT_ref_    << "\n";
      std::cout << "  T_ref:      " << T_ref_         << "\n";
      std::cout << "  nu_ref:     " << nu_ref_        << "\n";
      std::cout << "  mu_ref:     " << mu_ref_        << "\n";
      std::cout << "  d_ref:      " << sqrt(dsq_ref_) << "\n";
      std::cout << "  mfp_ref:    " << mfp(T_ref_,1.) << "\n";
      std::cout << "  m:          " << m_             << "\n";
      std::cout << "  alpha:      " << alpha_         << "\n";
      std::cout << "  alpha_recp: " << alpha_recp_    << "\n";
      std::cout << "  omega:      " << omega_         << "\n";
      std::cout << "  R:          " << R_             << "\n";
      std::cout << "  gamma:      " << gamma_         << "\n";
      std::cout << "  Zrot_inf:   " << Zrot_inf_      << "\n";
      std::cout << "  Trot_ref:   " << Trot_ref_      << "\n";
      std::cout << "  Cvib_1_:    " << Cvib_1_        << "\n";
      std::cout << "  Cvib_2:     " << Cvib_2_        << "\n";
      std::cout << "  Tvib_ref:   " << Tvib_ref_      << "\n";
      std::cout << std::endl;

  }

}


// update relative velocity via binary interaction
// using 2 uniform(0,1) random fractions
v4df VSS::collide(const v4df& cr_pre, MyRandom::Rng& rndGen)
const
{

  v4df cr_post;

  const v4df cr2 = cr_pre*cr_pre;

  // magnitude of the relative velocity
  const double crMag = sqrt(MyIntrin::shsum3(cr2));

  // a random azimuth angle
  const double epsilon = 2.0*pi*rndGen.uniform();
  // its cosine
  const double cosEps = cos(epsilon);
  // its sine
  const double sinEps = sin(epsilon);


  if (
      ( fabs(alpha_ - 1.0) < 1.e-4 ) // VHS model case
      || ( fabs( MyIntrin::get(cr_pre,0) - crMag) < 1.e-9 ) ) // or relative velocity lies on x axis
    {
      // use VHS
      // cosine of a random elevation angle
      const double cosChi = 2.0*rndGen.uniform() - 1.0;
      // the sine
      const double sinChi = sqrt(1.0 - cosChi*cosChi);


      // update the velocity
      cr_post = v4df{
        cosChi*crMag,
        sinChi*cosEps*crMag,
        sinChi*sinEps*crMag,
        0.0 };
    }
  else
    {
      // use VSS

      // cosine of random deflection angle for the VSS model ( eqn 11.8 )
      const double cosChi = 2.0*pow(rndGen.uniform(),alpha_recp_) - 1.0;
      // its sine
      const double sinChi = sqrt(1.0 - cosChi*cosChi);


      // magnitude of relative velocity in x==0. plane
      const double D = sqrt( double(cr2[1] + cr2[2]) );

      if (D > 1.e-9) // avoid div by near 0
        {
          // eqn 2.22

          cr_post = v4df{
            sinChi*sinEps*D,
            sinChi*( crMag*cosEps*cr_pre[2] - sinEps*cr_pre[0]*cr_pre[1]) / D,
            -sinChi*( crMag*cosEps*cr_pre[1] + sinEps*cr_pre[0]*cr_pre[2]) / D,
            0.0
          };

          cr_post += cr_pre * cosChi;
        }
      else
        {
          cr_post = v4df{
            cosChi*crMag,
            sinChi*cosEps*crMag,
            sinChi*sinEps*crMag,
            0.0
          };
        }


    }

  //  MYASSERT(MyIntrin::isfinite(cr_post),"vss returned non-finite velocity");

  return cr_post;



}

