/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * MDF.h
 *
 *  Created on: Jan 3, 2017
 *      Author: kustepha
 */

#ifndef SRC_GAS_PDF_H_
#define SRC_GAS_PDF_H_


#include <vector>
#include <string>
#include <memory>
#include <mpi.h>

#include <Eigen/Dense>

#include "Gas/GasModel.h"


class FieldData;

namespace MyRandom
{
  class Rng;
}

namespace Gas
{

  class alignas(32) PDF {

  protected:

    const Eigen::Vector4d U_; // mean velocity

  public:
    // vector of variables names necessary for construction
    static std::vector<std::string> requiredVariables() {
      return {}; }

    // vector of variables names necessary for construction
    static std::vector<std::string> optionalVariables() {
      return {"U","V","W"}; }

  public:

    Eigen::Vector4d meanVelocity() const { return U_; }

    virtual double mostProbableSpeed() const = 0;

    virtual double normalizedMeanFlux( const Eigen::Vector4d& unitNormal ) const = 0;

    virtual Eigen::Vector4d sampleHalfspaceVelocity( const Eigen::Vector4d& unitNormal, MyRandom::Rng& rndGen ) const = 0;

    virtual Eigen::Vector4d sampleVelocity( MyRandom::Rng& rndGen ) const = 0;

    virtual Eigen::Vector2d sampleRotation( MyRandom::Rng& rndGen ) const = 0;

    virtual Eigen::Vector2d sampleVibration( MyRandom::Rng& rndGen ) const = 0;

    PDF(const Eigen::Vector4d& U): U_(U) {}

    virtual ~PDF() = default;

  };

  class MDF
  {

  private:

    const double m_; // molecular mass
    const double w_; // statistical weight(Fnum)
    const double n_; // molecular number density
    const double N_; // density of computational particles

    // velocity pdf
    const std::unique_ptr<PDF> pdf_;


  public:

    // molecular mass
    double m() const { return m_; }

    // statistical weight (FNum)
    double w() const { return w_; }

    // molecular number density
    double n() const { return n_; }

    // computational particle number density
    double N() const { return N_; }

  public:

    Eigen::Vector4d meanVelocity() const;

    double mostProbableSpeed() const;

    // mean flux of computational particles
    double meanFlux( const Eigen::Vector4d& unitNormal ) const;

    Eigen::Vector4d sampleHalfspaceVelocity(
        const Eigen::Vector4d& unitNormal, MyRandom::Rng& rndGen ) const;

    Eigen::Vector4d sampleVelocity( MyRandom::Rng& rndGen ) const;

    Eigen::Vector2d sampleRotation( MyRandom::Rng& rndGen ) const;

    Eigen::Vector2d sampleVibration( MyRandom::Rng& rndGen ) const;


  public:

    MDF(
        const GasModels::GasModel *const gas,
        const double Fn,
        const double n,
        std::unique_ptr<PDF> pdf)
  : m_(gas->m()), w_(Fn), n_(n), N_( n_ / Fn ), pdf_(std::move(pdf)) {}

    MDF(
        const GasModels::GasModel *const gas,
        const double Fn,
        const Settings::Dictionary& dict);


  };

  // PDF factory

  void variablesRequiredToMakePDF(
      std::vector<std::string>& req,
      std::vector<std::string>& opt,
      const Settings::Dictionary& dict);

  std::unique_ptr<PDF> makePDF(
      const GasModels::GasModel *const gas,
      const Settings::Dictionary& dict,
      const bool zeroMeanFlow = false);

  void makePDF(
      std::vector<std::unique_ptr<PDF>>& pdf,
      const FieldData* const fieldData,
      const GasModels::GasModel *const gas,
      const Settings::Dictionary& dict,
      const bool zeroMeanFlow = false);

  void makePDF(
      std::vector<std::unique_ptr<PDF>>& pdf,
      const GasModels::GasModel *const gas,
      const Settings::Dictionary& dict,
      const std::string& inputDirectory,
      const MPI_Comm& comm,
      const bool zeroMeanFlow = false);


  // MDF factory

  void makeMDF(
      std::vector<std::shared_ptr<MDF>>& mdf,
      const FieldData* const fieldData,
      const GasModels::GasModel *const gas,
      const double Fn,
      const Settings::Dictionary& dict);

  void makeMDF(
      std::vector<std::shared_ptr<MDF>>& mdf,
      const GasModels::GasModel *const gas,
      const double Fn,
      const Settings::Dictionary& dict,
      const std::string& inputDirectory,
      const MPI_Comm& comm);

  void makeNMDF(
      std::vector<std::shared_ptr<MDF>>& mdf,
      const uint64_t N,
      const GasModels::GasModel *const gas,
      const double Fn,
      const Settings::Dictionary& dict,
      const std::string& inputDirectory,
      const MPI_Comm& comm);



} /* namespace Gas */

#endif /* SRC_GAS_PDF_H_ */
