/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * Maxwellian.h
 *
 *  Created on: Jan 4, 2017
 *      Author: kustepha
 */

#ifndef SRC_GAS_PDF_MAXWELLIAN_H_
#define SRC_GAS_PDF_MAXWELLIAN_H_

#include <Gas/PDF/MDF.h>
#include "Constants/Constants.h"
#include "Primitives/MyRandom.h"
#include "Primitives/NormalDistribution.h"
#include "Primitives/MyVecArithm.h"


namespace Settings
{
  class Dictionary;
}

namespace Gas
{

  namespace GasModels
  {
    class GasModel;
  }

  class Maxwellian: public PDF
  {

    protected:

      const double T_;    // translational temperature
      const double Trot_; // rotational temperature
      const double Tvib_; // vibrational temperature

      const double cmp_;  // most probable speed
      const double ustd_; // equilibrium standard deviation of a translational velocity component
      const double ostd_; // equilibrium standard deviation of rotational dof
      const double xstd_; // equilibrium standard deviation of vibrational dof

    public:
      // vector of variables names necessary for construction
      static std::vector<std::string> requiredVariables() {
        auto req = PDF::requiredVariables();
        req.push_back("T");
        return req;
      }

      // vector of variables names optional for construction
      static std::vector<std::string> optionalVariables() {
        auto pdfopt = PDF::optionalVariables();
        pdfopt.insert(pdfopt.end(),{"TRot","TVib"});
        return pdfopt;
      }


    protected:

      // sample normalized thermal translational velocity components from the influx equilibrium distribution
      Eigen::Vector4d
      sampleHalfspaceNormalizedThermalVelocity( const double a, MyRandom::Rng& rng )
      const
      {
        // sample velocity from one-sided (boundary) equilibrium distribution
        // scheme as in Garcia & Wagner (2006), Generation of the Maxwellian inflow distribution
        // Table 2

        // a is the normal component of stream velocity, normalized by the most probable speed

        const MyRandom::NormalDistribution sn;

        double z_star;

        if (a == 0.) z_star = -sqrt(-log(rng.uniform()));
        else
        {

          if (a < 0.)
          {
            // step 1.
            const double z    = .5*(a-sqrt(a*a+2.));
            const double beta = a - (1.-a)*(a-z);
            const double fac  = exp(-beta*beta) / ( exp(-beta*beta) + 2.*(a-z)*(a-beta)*exp(-z*z) );

            while(true)
            {
              // step 2.
              if (  fac > rng.uniform() )
              {
                z_star = -sqrt( beta*beta - log(rng.uniform()) ); // 2.a
                if ( (a-z_star) > -z_star*rng.uniform() ) break; // 2.b
              }
              else // step 3.
              {
                z_star = beta + (a-beta)*rng.uniform(); // 3.a
                if ( (a-z_star)*exp(z*z - z_star*z_star)/(a-z) > rng.uniform() ) break; // 3.b
              }
            }
          }
          else
          {
            do {// step 1.
              if (1./(2.*Constants::sqrtpi*a + 1.) > rng.uniform())
                z_star = -sqrt(-log(rng.uniform()));
              else z_star = Constants::invsqrt2*sn(rng);
            } while ( (a-z_star) <= a*rng.uniform() ); // step 2., step 3.
          }
        }

        return Eigen::Vector4d(
            -z_star,
            Constants::invsqrt2*sn(rng),
            Constants::invsqrt2*sn(rng),
            0.0 );
      }

    public:
      double T() const { return T_; }

    public:

      virtual double mostProbableSpeed() const override
      {
        return cmp_;
      }

      // equilibrium mean inward number flux of computational particles eq. 4.22
      virtual double normalizedMeanFlux( const Eigen::Vector4d& unitNormal ) const override
          {
        const double s = unitNormal.dot(U_) / cmp_;
        return ( exp(-s*s) + Constants::sqrtpi*s*(1.0 + erf(s)) ) * 0.5 * Constants::invsqrtpi * cmp_;
          }

      // sample translational velocity from the influx equilibrium distribution
      virtual
      Eigen::Vector4d
      sampleHalfspaceVelocity( const Eigen::Vector4d& unitNormal, MyRandom::Rng& rndGen )
      const
      override
      {
        const double un = unitNormal.dot(U_) / cmp_;
        const Eigen::Vector4d C = cmp_ * sampleHalfspaceNormalizedThermalVelocity( un, rndGen );

        return U_ + MyVecArithm::aimRot( C, unitNormal );
      }

      virtual
      Eigen::Vector4d sampleVelocity( MyRandom::Rng& rng )
      const
      override
      {
        const MyRandom::NormalDistribution sn;
        return U_ + Eigen::Vector4d(
            ustd_*sn(rng),
            ustd_*sn(rng),
            ustd_*sn(rng),
            0.0);
      }


      virtual
      Eigen::Vector2d
      sampleRotation( MyRandom::Rng& rng )
      const
      override
      {
        const MyRandom::NormalDistribution sn;
        return Eigen::Vector2d{ ostd_*sn(rng), ostd_*sn(rng) };
      }

      virtual
      Eigen::Vector2d
      sampleVibration( MyRandom::Rng& rng )
      const
      override
      {
        const MyRandom::NormalDistribution sn;
        return Eigen::Vector2d{ xstd_*sn(rng), xstd_*sn(rng) };
      }

    public:

      Maxwellian(
          const GasModels::GasModel *const gas,
          const Eigen::Vector4d& U,
          const double T,
          const double Trot = -1.,
          const double Tvib = -1.)
    : PDF(U)
    , T_(T)
    , Trot_( Trot > 0. ? Trot : T_ )
    , Tvib_( Tvib > 0. ? Tvib : T_ )
    , cmp_( gas->cMp(T_) )
    , ustd_( gas->uStd(T_) )
    , ostd_( gas->omegaStd(Trot_) )
    , xstd_( gas->xiStd(Tvib_) )
    {

    }

      Maxwellian(
          const GasModels::GasModel *const gas,
          const Settings::Dictionary& dict,
          const bool zeroMeanFlow);


      virtual ~Maxwellian() = default;
  };

} /* namespace Gas */

#endif /* SRC_GAS_PDF_MAXWELLIAN_H_ */
