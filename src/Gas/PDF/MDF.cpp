/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * PDF.cpp
 *
 *  Created on: Jan 3, 2017
 *      Author: kustepha
 */

#include <Gas/PDF/MDF.h>
#include <map>

#include "Maxwellian.h"
#include "Grad13.h"

#include "Settings/Dictionary.h"

#include "FieldData/FieldData.h"

#include "Primitives/MyError.h"
#include "Primitives/MyString.h"

#include "IO/Tecplot/Tecplot.h"

namespace
{



  enum PDFType
  {
    MAXWELLIAN,
    GRAD13
  };

  static std::map<std::string,PDFType> PDFTypeNameMap = {
      {"MAXWELLIAN",MAXWELLIAN},
      {"GRAD13",GRAD13}
  };


  PDFType getPDFType(const Settings::Dictionary& dict)
  {

    std::string pdftypename = dict.get<std::string>("pdf","maxwellian");
    MyString::toUpper( pdftypename );

    MYASSERT( PDFTypeNameMap.find(pdftypename) != PDFTypeNameMap.end(),
              std::string("unknown pdf type ") + pdftypename );

    return PDFTypeNameMap.find(pdftypename)->second;

  }

}


namespace Gas {

  Eigen::Vector4d MDF::meanVelocity() const {
    return pdf_->meanVelocity(); }

  double MDF::mostProbableSpeed() const {
    return pdf_->mostProbableSpeed();
  }

  double MDF::meanFlux( const Eigen::Vector4d& unitNormal ) const {
    return N()*pdf_->normalizedMeanFlux(unitNormal);
  }

  Eigen::Vector4d MDF::sampleHalfspaceVelocity(
      const Eigen::Vector4d& unitNormal,
      MyRandom::Rng& rndGen ) const {
    return pdf_->sampleHalfspaceVelocity(unitNormal,rndGen);
  }

  Eigen::Vector4d MDF::sampleVelocity( MyRandom::Rng& rndGen ) const {
    return pdf_->sampleVelocity(rndGen);
  }

  Eigen::Vector2d MDF::sampleRotation( MyRandom::Rng& rndGen ) const {
    return pdf_->sampleRotation(rndGen);
  }

  Eigen::Vector2d MDF::sampleVibration( MyRandom::Rng& rndGen ) const {
    return pdf_->sampleVibration(rndGen);
  }

  MDF::MDF(
      const GasModels::GasModel *const gas,
      const double Fn,
      const Settings::Dictionary& dict)
  : MDF(gas,Fn,gas->getn(dict),std::move(makePDF(gas,dict)))
  {}

}


namespace Gas
{

  void variablesRequiredToMakePDF(
      std::vector<std::string>& req,
      std::vector<std::string>& opt,
      const Settings::Dictionary& dict) {

    PDFType type = getPDFType(dict);

    std::vector<std::string> r;
    std::vector<std::string> o;

    switch (type) {
      case MAXWELLIAN: {
        r = Maxwellian::requiredVariables();
        o = Maxwellian::optionalVariables();
      } break;
      case GRAD13: {
        r = Grad13::requiredVariables();
        o = Grad13::optionalVariables();
      } break;
      default:
        break;
    }

    req.insert(req.end(),r.begin(),r.end());
    opt.insert(opt.end(),o.begin(),o.end());

  }

  std::unique_ptr<PDF>
  makePDF(const GasModels::GasModel *const gas, const Settings::Dictionary& dict,
          const bool zeroMeanFlow) {
    PDFType type = getPDFType(dict);

    switch (type) {
      case MAXWELLIAN:
        return std::unique_ptr<PDF>( new class Maxwellian(gas,dict,zeroMeanFlow) );
        break;
      case GRAD13:
        return std::unique_ptr<PDF>( new class Grad13(gas,dict,zeroMeanFlow) );
        break;
      default:
        break;
    }

    return nullptr;
  }

  void makePDF(
      std::vector<std::unique_ptr<PDF>>& pdf,
      const FieldData* const fData,
      const GasModels::GasModel *const gas,
      const Settings::Dictionary& dict,
      const bool zeroMeanFlow)
  {

    const uint64_t numPDF = fData->getNData("T");

    pdf.reserve(numPDF);

    PDFType type = getPDFType(dict);

    const double zero = 0.;
    uint64_t haveU = !zeroMeanFlow && fData->hasVar("U");
    uint64_t haveV = !zeroMeanFlow && fData->hasVar("V");
    uint64_t haveW = !zeroMeanFlow && fData->hasVar("W");

    auto T = fData->getVarData("T");
    auto U = &zero;
    auto V = &zero;
    auto W = &zero;
    auto TRot = T;
    auto TVib = T;

    if (haveU) U = fData->getVarData("U");
    if (haveV) V = fData->getVarData("V");
    if (haveW) W = fData->getVarData("W");

    if (fData->hasVar("TRot")) TRot =  fData->getVarData("TRot");
    if (fData->hasVar("TVib")) TVib =  fData->getVarData("TVib");

    switch (type) {
      case MAXWELLIAN:
        {

          for (uint64_t i = 0; i < numPDF; ++i)
            pdf.emplace_back(
                new class Maxwellian(
                    gas,
                    Eigen::Vector4d({U[i*haveU],V[i*haveV],W[i*haveW],0.0}),
                    T[i], TRot[i], TVib[i] )
            );
        }
        break;
      case GRAD13:
        {

          auto qx = fData->getVarData("qx");
          auto qy = fData->getVarData("qy");
          auto qz = fData->getVarData("qz");

          auto pxx = fData->getVarData("pxx");
          auto pxy = fData->getVarData("pxy");
          auto pxz = fData->getVarData("pxz");
          auto pyy = fData->getVarData("pyy");
          auto pyz = fData->getVarData("pyz");
          auto pzz = fData->getVarData("pzz");


          for (uint64_t i = 0; i < numPDF;++i)
            pdf.emplace_back(new class Grad13(
                gas,
                Eigen::Vector4d({U[i*haveU],V[i*haveV],W[i*haveW],0.0}),
                T[i], TRot[i], TVib[i],
                qx[i],qy[i],qz[i],
                pxx[i],pxy[i],pxz[i],
                pyy[i],pyz[i],pzz[i]
            )
            );

        }
        break;
      default:
        MYASSERT(false,"unknown PDF type");
        break;
    }
  }


  void makePDF(
      std::vector<std::unique_ptr<PDF>>& pdf,
      const GasModels::GasModel *const gas,
      const Settings::Dictionary& dict,
      const std::string& inputDirectory,
      const MPI_Comm& comm,
      const bool zeroMeanFlow)
  {

    std::vector<std::string> req;
    std::vector<std::string> opt;
    variablesRequiredToMakePDF(req, opt, dict);

    if (dict.hasAllMembers(req)) {

        // dictionary takes precedence, generate single pdf
        pdf.push_back(
            std::move(makePDF(gas,dict,zeroMeanFlow))
        );

    } else if (dict.hasMember("data file")) {

        auto fData = Tecplot::readTecplot1DData(
            MyString::catDirs(inputDirectory, dict.get<std::string>("data file") ),
            req,
            opt,
            false,
            comm );

        makePDF(pdf,fData.get(),gas,dict,zeroMeanFlow);


    } else {
        MYASSERT(false,"dict does not contain necessary variables of data file spec to make pdf");
    }

  }


}

// MDF factory

namespace Gas {


  void makeMDF(
      std::vector<std::shared_ptr<MDF>>& mdf,
      const FieldData* const fData,
      const GasModels::GasModel *const gas,
      const double Fn,
      const Settings::Dictionary& dict)
  {

    const uint64_t numMDF = fData->getNData("n");
    auto n = fData->getVarData("n");

    mdf.reserve(numMDF);

    std::vector<std::unique_ptr<PDF>> pdf;
    makePDF( pdf, fData, gas, dict );

    MYASSERT(pdf.size() == numMDF,"pdf vector, mdf vector size mismatch");

    for (uint64_t i = 0; i < numMDF; ++i)
      mdf.push_back( std::move( std::make_shared<MDF>( gas, Fn, n[i], std::move(pdf[i]) ) )  );

  }


  void makeMDF(
      std::vector<std::shared_ptr<MDF>>& mdf,
      const GasModels::GasModel *const gas,
      const double Fn,
      const Settings::Dictionary& dict,
      const std::string& inputDirectory,
      const MPI_Comm& comm)
  {

    std::vector<std::string> req;
    std::vector<std::string> opt;
    variablesRequiredToMakePDF(req, opt, dict);
    req.push_back("n");

    if (dict.hasAllMembers(req)) {

        // dictionary takes precedence, generate single pdf
        mdf.push_back(std::move(std::make_shared<MDF>(gas,Fn,dict)));

    } else if (dict.hasMember("data file")) {

        auto fData = Tecplot::readTecplot1DData(
            MyString::catDirs(inputDirectory, dict.get<std::string>("data file") ),
            req,
            opt,
            false,
            comm );

        makeMDF(mdf,fData.get(),gas,Fn,dict);


    } else {
        MYASSERT(false,"dict does not contain necessary variables of data file spec to make mdf");
    }

  }


  void makeNMDF(
      std::vector<std::shared_ptr<MDF>>& mdf,
      const uint64_t N,
      const GasModels::GasModel *const gas,
      const double Fn,
      const Settings::Dictionary& dict,
      const std::string& inputDirectory,
      const MPI_Comm& comm)
  {

    mdf.reserve(mdf.size() + N);

    makeMDF(mdf,gas,Fn,dict,inputDirectory,comm);

    if (!dict.hasMember("data file"))
      {
        auto p = mdf.back();
        for (uint64_t i = 1; i < N; ++i)
          mdf.push_back( p );
      }


  }


} /* namespace Gas */
