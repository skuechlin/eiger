/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * Maxwellian.cpp
 *
 *  Created on: Jan 11, 2017
 *      Author: kustepha
 */


#include "Parallel/MyMPI.h"

#include "Gas/PDF/Maxwellian.h"

#include "Settings/Dictionary.h"

namespace Gas
{



  Maxwellian::Maxwellian(
      const GasModels::GasModel *const gas,
      const Settings::Dictionary& dict,
      const bool zeroMeanFlow)
  : Maxwellian(
      gas,
      zeroMeanFlow ? Eigen::Vector4d::Zero() : gas->getU(dict,gas->getT(dict)),
      gas->getT(dict),
      dict.get<double>("T rot",-1.),
      dict.get<double>("T vib",-1.) )
  {

    /*
    if( MyMPI::isRoot(MPI_COMM_WORLD,0) ) {

        //#ifndef NDEBUG
#pragma omp critical (COUT)
        {
          std::cout << "constructed Maxwellian pdf with\n"
              << "  U:    " << U_.transpose();   std::cout << " m s-1\n"
              << "  cmp:  " << cmp_ << " m s-1\n"
              << "  ustd: " << ustd_ << " m s-1\n"
              << "  T:    " << T_ << " K\n"
              << std::endl;
        }
        //#endif
    }
    */

  }

}
