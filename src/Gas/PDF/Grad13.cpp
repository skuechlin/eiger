/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * Grad13.cpp
 *
 *  Created on: Jan 13, 2017
 *      Author: kustepha
 */

#include <Gas/PDF/Grad13.h>

#include "Settings/Dictionary.h"

namespace
{
  Eigen::Vector4d getQ(const Settings::Dictionary& dict)
  {
    Settings::Dictionary qdict = dict.get<Settings::Dictionary>("q");
    Eigen::Vector4d q = Eigen::Vector4d::Zero();
    qdict.get<double>( q.data(), 0, 3 );
    return q;
  }

  Eigen::Matrix4d getSigma(const Settings::Dictionary& dict)
      {
        Settings::Dictionary sdict = dict.get<Settings::Dictionary>("sigma");
        Eigen::Matrix4d sigma;
        sdict.get<double>( sigma.data()  , 0, 3 );
        sdict.get<double>( sigma.data()+4, 3, 3 );
        sdict.get<double>( sigma.data()+8, 6, 3 );
        return sigma;
      }

  Eigen::Vector4d getSigmaDiag(const Settings::Dictionary& dict)
  {
    Eigen::Matrix4d sigma = getSigma(dict);
    return {sigma(0,0), sigma(1,1), sigma(2,2), 0.0};
  }

  Eigen::Vector4d getSigmaOffDiag(const Settings::Dictionary& dict)
  {
    Eigen::Matrix4d sigma = getSigma(dict);
    return {sigma(0,1), sigma(1,2), sigma(0,2), 0.0};
  }


}

namespace Gas
{

  Grad13::Grad13(
      const GasModels::GasModel *const gas,
      const Settings::Dictionary& dict,
      const bool zeroMeanFlow)
  : Maxwellian(gas,dict,zeroMeanFlow)
  , q_red_( 2./(cmp_*gas->p(gas->getn(dict),T_)) * MyIntrin::load4d( getQ(dict) ) )
  , tau_red_diag_( -MyIntrin::load4d( getSigmaDiag(dict) ) / gas->p(gas->getn(dict),T_) )
  , tau_red_offdiag_( -MyIntrin::load4d( getSigmaOffDiag(dict) ) / gas->p(gas->getn(dict),T_) )
  , A_( amplitude() )
  {}


} /* namespace Gas */
