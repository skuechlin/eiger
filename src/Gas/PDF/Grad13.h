/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * Grad13.h
 *
 *  Created on: Jan 13, 2017
 *      Author: kustepha
 */

#ifndef SRC_GAS_PDF_GRAD13_H_
#define SRC_GAS_PDF_GRAD13_H_

#include <Gas/PDF/Maxwellian.h>

#include "Primitives/MyIntrin.h"
#include "Primitives/NormalDistribution.h"

namespace Gas
{

  class Grad13: public Gas::Maxwellian
  {

    const v4df q_red_;  // dimension-less heat flux

    const v4df tau_red_diag_;     // 00, 11, 22, ** // negative dimension-less trace-free pressure tensor
    const v4df tau_red_offdiag_;  // 01, 12, 02, **

    const double A_; // amplitude parameter

  private:

    double breakdown()
    const
    {
      return std::max( {
        fabs( MyIntrin::get(q_red_,0) ) ,
            fabs( MyIntrin::get(q_red_,1) ),
            fabs( MyIntrin::get(q_red_,2) ),
            fabs( MyIntrin::get(tau_red_diag_,0) ),
            fabs( MyIntrin::get(tau_red_diag_,1) ),
            fabs( MyIntrin::get(tau_red_diag_,2) ),
            fabs( MyIntrin::get(tau_red_offdiag_,0) ),
            fabs( MyIntrin::get(tau_red_offdiag_,1) ),
            fabs( MyIntrin::get(tau_red_offdiag_,2) ) } );
    }

    double amplitude() const { return 1. + 30.*breakdown(); }

    double gam(const v4df& C)
    const
    {
      return 1. +
          MyIntrin::sdot(q_red_,C)*(2./5.*MyIntrin::sdot(C,C) - 1.) +
          2.*MyIntrin::sdot(tau_red_offdiag_, v4df(C * __builtin_shuffle(C,v4du{1,2,0,3})) ) +
          MyIntrin::sdot(tau_red_diag_, C*C);
    }

  public:
    // vector of variables names necessary for construction
    static std::vector<std::string> requiredVariables() {
      std::vector<std::string> mrv = Maxwellian::requiredVariables();
      mrv.insert(mrv.end(), {
          "qx","qy","qz",
          "pxx","pxy","pxz",
          "pyy","pyz","pzz"}
      );
      return mrv;
    }

    // vector of variables names optional for construction
    static std::vector<std::string> optionalVariables() {
      return Maxwellian::optionalVariables(); }

  public:

    // mean flux of COMPUTATIONAL particles per unit time per unit area
    virtual double normalizedMeanFlux( const Eigen::Vector4d& unitNormal ) const override
    {
      const v4df n = MyIntrin::load4d(unitNormal.data());
      const double s = unitNormal.dot(U_) / cmp_;
      const double tauxx = MyIntrin::sdot(tau_red_diag_,n*n) +
          2.*MyIntrin::sdot(tau_red_offdiag_,v4df(n * __builtin_shuffle(n,v4du{1,2,0,3})));

      return cmp_ * -0.5 * Constants::invsqrtpi * (
          ( -1.0 + tauxx + 0.2*s*MyIntrin::sdot(q_red_,n) ) * exp(-s*s)
          - Constants::sqrtpi * s * ( 1. + erf(s) )
      );
    }

    // sample a thermal translational velocity from the influx equilibrium distribution
    virtual
    Eigen::Vector4d
    sampleHalfspaceVelocity( const Eigen::Vector4d& unitNormal, MyRandom::Rng& rndGen )
    const
    override
    {
      // sample velocity from one-sided (boundary) distribution
      // procedure from
      //
      // Alejandro L. Garcia and Berni J. Alder
      // Generation of the Chapman-Enskog Distribution (1998)
      //
      // Stephani, K.A., Goldstein, D.B. and Varghese, P.L.
      // A non-equilibrium surface reservoir approach
      // for hybrid DSMC/Navier–Stokes particle generation (2013)

      const double a = U_.dot(unitNormal) / cmp_;

      Eigen::Vector4d Ctry;

      while (
          A_*rndGen.uniform() > gam(
              MyIntrin::load4d( (Ctry = MyVecArithm::aimRot(
                  Maxwellian::sampleHalfspaceNormalizedThermalVelocity(a,rndGen),
                  unitNormal)).data() ) )
      );

      return U_ + cmp_ * Ctry;

    }

    virtual
    Eigen::Vector4d sampleVelocity( MyRandom::Rng& rng )
    const
    override
    {
      // sample velocity
      // procedure from
      //
      // Alejandro L. Garcia and Berni J. Alder
      // Generation of the Chapman-Enskog Distribution (1998)
      //

      const MyRandom::NormalDistribution sn;

      v4df Ctry;

      while ( A_*rng.uniform() > gam( Ctry =
          v4df{
          Constants::invsqrt2*sn(rng),
              Constants::invsqrt2*sn(rng),
              Constants::invsqrt2*sn(rng),
              0.0} )  );
      Ctry *= cmp_;
      Ctry += MyIntrin::load4d(U_.data());

      return *(Eigen::Vector4d*)&Ctry;
    }


  public:

    Grad13(
        const GasModels::GasModel *const gas,
        const Eigen::Vector4d& U,
        const double T,
        const double Trot,
        const double Tvib,
        const double q0,
        const double q1,
        const double q2,
        const double pxx,
        const double pxy,
        const double pxz,
        const double pyy,
        const double pyz,
        const double pzz)
  : Maxwellian(gas,U,T,Trot,Tvib)
  , q_red_(2.*3./(cmp_*(pxx + pyy + pzz)) * v4df{q0,q1,q2,0.0})
  , tau_red_diag_( -3.*v4df{pxx,pyy,pzz,0.0} / (pxx + pyy + pzz) + v4df{1.,1.,1.,0.} )
  , tau_red_offdiag_( -3.*v4df{pxy,pyz,pxz,0.0} / (pxx + pyy + pzz) )
  , A_( amplitude() )
  { }


    Grad13(
        const GasModels::GasModel *const gas,
        const Settings::Dictionary& dict,
        const bool zeroMeanFlow);

    virtual ~Grad13() = default;
  };

} /* namespace Gas */

#endif /* SRC_GAS_PDF_GRAD13_H_ */
