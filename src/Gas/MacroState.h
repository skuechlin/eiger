/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * MacroState.h
 *
 *  Created on: Nov 20, 2014
 *      Author: kustepha
 */

#ifndef GAS_MACROSTATE_H_
#define GAS_MACROSTATE_H_

#include "Moments/MomentsForwardDecls.h"

#include <mpi.h>
#include <iostream>

#include <Eigen/Dense>

#include "Primitives/MyError.h"

namespace Gas
{
  namespace GasModels
  {
    class GasModel;
  }

  struct MacroState
  {

  public:


    double n     = 0.; // [m-3]    number density
    double T     = 0.; // [K]      translational temperature
    double mu    = 0.; // [Pa s]   viscosity
    double nu    = 0.; // [s-1]    equilibrium collision frequency

    double mfp   = 0.; // [m]      equilibrium mean free path
    double Ma    = 0.; // [-]      Mach number
    double gam   = 0.; // [-]      ratio of specific heats
    double pad0  = 0.; // pad to multiple of 32 bytes

  public:
    void assert_finite() const {
      if( std::isfinite(T)
      && std::isfinite(mu)
      && std::isfinite(nu)
      && std::isfinite(mfp)
      && std::isfinite(Ma)
      && std::isfinite(gam)
      && std::isfinite(n) ) {}
      else
        {
#pragma omp critical (COUT)
          {
            std::cout

            << n << "\n"
            << T << "\n"
            << mu << "\n"
            << nu << "\n"
            << mfp << "\n"
            << Ma << "\n"
            << gam << "\n"
            << std::endl;
          }
          MYASSERT(false, "macro state contains non-finite"   );
        }
    }

  public:
    void update(
        const double rho,
        const double theta,
        const double v,
        const GasModels::GasModel* const gasModel );

  };

} /* namespace Gas */

#endif /* GAS_MACROSTATE_H_ */
