/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * BoundaryProbe.h
 *
 *  Created on: Oct 14, 2014
 *      Author: kustepha
 */

#ifndef BOUNDARYPROBE_H_
#define BOUNDARYPROBE_H_

#include <mpi.h>


//#include "Moments/MomentsForwardDecls.h"
#include "Moments/SurfaceMoments.h"

#include "IO/Tecplot/TecplotConsts.h"

#include "Primitives/MyError.h"
#include "Primitives/MyMemory.h"


#include <memory>

namespace Particles { class Particle; }
namespace Shapes { class Shape; }
namespace Settings { class Dictionary; }
namespace MyMPI { class MPIMgr; }
namespace Boundary { class BoundaryProbe; }

namespace Gas
{
  namespace GasModels
  {
    class GasModel;
  }
}


namespace Boundary
{

  class BoundaryProbe {

    struct MacroStateEstimate {
      double T_;
      double Trot_;
      double Tvib_;
      double Ma_;
    };


  public:
    // implicit tecplot ordered writer interface

    // number of variables
    static uint
    nVars();

    // number of elements
    uint64_t
    nElements()
    const;

    // get value of variable varId for element elId
    double
    getVar(const uint64_t elId, const uint64_t varId)
    const;

    // get name of variable varId
    static std::string
    getVarName(const uint64_t varId);

    // get variable location (node/cell centered)
    Tecplot::VarLoc
    getVarLoc(const uint64_t varId) const;

    // id
    uint64_t id() const;


  private:

    typedef Moments::SurfaceMoments MomentsT;
    typedef MyMemory::aligned_array<MomentsT> MomentsCT;

    const Shapes::Shape* shape_;   // shape this probe pertains to

    MomentsCT m_i_; 	    // moments of incident particles
    MomentsCT m_i_av_;	// time averaged moments of incident particles

    MomentsCT m_r_;       // moments of reflected particles
    MomentsCT m_r_av_;    // time averaged moments of reflected particles

    std::vector<MacroStateEstimate> mstate_;
    std::vector<MacroStateEstimate> mstate_av_;

    std::unique_ptr<Moments::Averaging::AveragingStrategy> alpha_; // provides factor for averaging


  public:


    double area(const uint64_t shapeId) const {
      return m_i_av_[shapeId].area_; }

    double dt_av(const uint64_t shapeId) const {
      return m_i_av_[shapeId].dtsamp_; }

    double avNumSamps(const uint64_t shapeId) const {
      return m_i_av_[shapeId].N_ /* + m_r_av_[shapeId].N_ */; }

    double totNumSamps(const uint64_t shapeId) const {
      return m_i_av_[shapeId].Wtot_*avNumSamps(shapeId); }


    double pressure(const uint64_t shapeId) const {
      return m_i_[shapeId].pressure() - m_r_[shapeId].pressure(); }

    double pressure_av(const uint64_t shapeId) const {
      return m_i_av_[shapeId].pressure() - m_r_av_[shapeId].pressure(); }


    double numFlux(const uint64_t shapeId) const { return m_i_[shapeId].numFlux(); }

    double numFlux_av(const uint64_t shapeId) const { return m_i_av_[shapeId].numFlux(); }


    double heatFlux(const uint64_t shapeId) const {
      return m_i_[shapeId].heatFlux() - m_r_[shapeId].heatFlux(); }

    double heatFlux_av(const uint64_t shapeId) const {
      return m_i_av_[shapeId].heatFlux() - m_r_av_[shapeId].heatFlux(); }


    Eigen::Vector4d force(const uint64_t shapeId) const {
      return m_i_[shapeId].force() - m_r_[shapeId].force(); }

    Eigen::Vector4d force_av(const uint64_t shapeId) const {
      return m_i_av_[shapeId].force() - m_r_av_[shapeId].force(); }

    template<uint8_t i> double force(const uint64_t shapeId) const {
      return m_i_[shapeId].force<i>() - m_r_[shapeId].force<i>(); }

    template<uint8_t i> double force_av(const uint64_t shapeId) const {
      return m_i_av_[shapeId].force<i>() - m_r_av_[shapeId].force<i>(); }

    Eigen::Vector4d velocity(const uint64_t shapeId) const {
      return ( m_i_[shapeId].weightedVelocity() + m_r_[shapeId].weightedVelocity() ) /
          ( m_i_[shapeId].W_v_ + m_r_[shapeId].W_v_ ); }

    Eigen::Vector4d velocity_av(const uint64_t shapeId) const {
      return ( m_i_av_[shapeId].weightedVelocity() + m_r_av_[shapeId].weightedVelocity() ) /
          ( m_i_av_[shapeId].W_v_ + m_r_av_[shapeId].W_v_ ); }

    template<uint8_t i> double velocity(const uint64_t shapeId) const {
      return ( m_i_[shapeId].weightedVelocity<i>() + m_r_[shapeId].weightedVelocity<i>() ) /
          ( m_i_[shapeId].W_v_ + m_r_[shapeId].W_v_ ); }

    template<uint8_t i> double velocity_av(const uint64_t shapeId) const {
      return ( m_i_av_[shapeId].weightedVelocity<i>() + m_r_av_[shapeId].weightedVelocity<i>() ) /
          ( m_i_av_[shapeId].W_v_ + m_r_av_[shapeId].W_v_ ); }

    double density_av(const uint64_t shapeId) const {
      return m_i_av_[shapeId].density() + m_r_av_[shapeId].density(); }

    double numberDensity_av(const uint64_t shapeId) const {
      return m_i_av_[shapeId].numberDensity() + m_r_av_[shapeId].numberDensity(); }


    double T(const uint64_t shapeId)          const { return mstate_[shapeId].T_; }
    double T_av(const uint64_t shapeId)       const { return mstate_av_[shapeId].T_; }

    double Trot(const uint64_t shapeId)       const { return mstate_[shapeId].Trot_; }
    double Trot_av(const uint64_t shapeId)    const { return mstate_av_[shapeId].Trot_; }


    double Tvib(const uint64_t shapeId)       const { return mstate_[shapeId].Tvib_; }
    double Tvib_av(const uint64_t shapeId)    const { return mstate_av_[shapeId].Tvib_; }

    double Ma(const uint64_t shapeId)         const { return mstate_[shapeId].Ma_; }
    double Ma_av(const uint64_t shapeId)      const { return mstate_av_[shapeId].Ma_; }


    void
    sampleIncident(
        const uint64_t elementId,
        const v4df& N,
        const double thit,
        const bool front,
        const Particles::Particle* prt);

    void
    sampleReflected(
        const uint64_t elementId,
        const v4df& N,
        const double thit,
        const bool front,
        const Particles::Particle* prt);


    void resetSamples(const double dt);

    /*
    void averageSamples(const uint64_t tn);

    void updateState(const Gas::GasModels::GasModel* const gasModel);

    void updateAverageState(const Gas::GasModels::GasModel* const gasModel);
    */

    void finilizeSampling(const uint64_t tn, const Gas::GasModels::GasModel* const gm);

    void initAllreduceSamples(
        MPI_Request *const incindent_req, MPI_Request *const refl_req, MPI_Comm comm);



  public:

    void
    writeToTecplot(
        const std::string& boundaryName,
        const std::string& dir,
        const double solTime,
        const uint64_t tn,
        const MyMPI::MPIMgr& mpiMgr
    ) const;

  public:

    BoundaryProbe(
        const Shapes::Shape* shape,
        const double FNum_ref,
        const Settings::Dictionary& dict);

    ~BoundaryProbe();
  };




} // namespace Boundary

#endif /* BOUNDARYPROBE_H_ */
