/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * BoundaryFactory.cpp
 *
 *  Created on: Oct 29, 2015
 *      Author: kustepha
 */

#include <Boundary/BoundaryProbe.h>
#include "Boundary.h"

#include "BoundaryFactory.h"
#include "BoundaryState.h"
#include "BoundaryStrategies/BoundaryStrategyFactory.h"

#include "Geometry/Shape.h"

#include "Geometry/ShapeFactory.h"

#include "Moments/AveragingStrategies.h"

#include "Settings/Dictionary.h"

#include "SimulationBox/SimulationBox.h"

#include "Gas/PDF/MDF.h"

namespace Boundary {

  std::unique_ptr<BoundaryState>
  makeBoundaryState(
      const Strategies::BoundaryStrategy* const strat,
      const uint64_t nElements,
      const Settings::Dictionary& stateDict,
      const std::string& inputDir,
      const Gas::GasModels::GasModel* gas)   {

    std::unique_ptr<BoundaryState> state = nullptr;

    if (strat || !(stateDict.isEmpty())) {

        bool needPDF = (strat != nullptr && strat->needsPDF());
        bool needState = (strat != nullptr && strat->needsState());

        if ( needPDF || needState || !(stateDict.isEmpty()) )
          {
            // construct state
            state = std::make_unique<BoundaryState>(
                nElements,
                stateDict,
                inputDir,
                needPDF,
                gas);
          }

    }

    return state;
  }

  std::unique_ptr<BoundaryProbe>
  makeBoundaryProbe(
      const Shapes::Shape* const shape,
      const double FNum_ref,
      const Settings::Dictionary& dict)   {
    std::unique_ptr<BoundaryProbe> probe = nullptr;

    if (dict.hasMember("sample"))
      {
        probe = std::unique_ptr<BoundaryProbe>(
            new BoundaryProbe(
                shape,
                FNum_ref,
                dict.get<Settings::Dictionary>("sample") ) );
      }
    return probe;
  }

  std::unique_ptr<Boundary::estimate_viewfactors_spec>
  makeBoundaryViewfactorsSpec(const Settings::Dictionary& dict,
                              const std::string& outputDir) {
    std::unique_ptr<Boundary::estimate_viewfactors_spec> vf = nullptr;

    if ( dict.hasMember("viewfactors") )
      {
        Settings::Dictionary vfdict = dict.get<Settings::Dictionary>("viewfactors");
        if ( vfdict.get<bool>("compute viewfactors") )
          {
            vf = std::unique_ptr<Boundary::estimate_viewfactors_spec>(
                new Boundary::estimate_viewfactors_spec(
                    vfdict.get<uint64_t>("points per shape",10000),
                    vfdict.get<uint64_t>("rays per point",1000),
                    outputDir ) );
          }
      }

    return vf;
  }



  std::unique_ptr<Boundary>
  makeBoundary(
      const Gas::GasModels::GasModel* gas,
      const double FNum_ref,
      const SimulationBox& box,
      const Settings::Dictionary& frontCondDict,
      const Settings::Dictionary& backCondDict,
      const Settings::Dictionary& frontStateDict,
      const Settings::Dictionary& backStateDict,
      const Settings::Dictionary& dict,
      const std::string& inputDir,
      const std::string& outputDir)   {

    MYASSERT(dict.hasMember("name"),"boundary dictionary missing entry \"name\"!");

    const std::string name = dict.get<std::string>("name");

    MYASSERT(dict.hasMember("geometry"),
             std::string("boundary dictionary \"")
    + name
    + std::string("\" missing entry \"geometry\"") );

    std::unique_ptr<Shape> shape = std::move(
        Shapes::makeShape(
            box,
            dict.get<Settings::Dictionary>("geometry"),
            inputDir ) );

    std::unique_ptr<Strategies::BoundaryStrategy> frontStrat = nullptr;

    if (!frontCondDict.isEmpty())
      frontStrat = Strategies::makeBoundaryStrategy(frontCondDict); // front

    std::unique_ptr<Strategies::BoundaryStrategy> backStrat = nullptr;

    if ( !backCondDict.isEmpty() )
      backStrat = Strategies::makeBoundaryStrategy(backCondDict); // back

    std::unique_ptr<BoundaryState> frontState = makeBoundaryState(
        frontStrat.get(),
        shape->nElements(),
        frontStateDict,
        inputDir,
        gas);

    std::unique_ptr<BoundaryState> backState = makeBoundaryState(
        backStrat.get(),
        shape->nElements(),
        backStateDict,
        inputDir,
        gas);

    std::unique_ptr<BoundaryProbe> probe =
        makeBoundaryProbe( shape.get(),FNum_ref,dict);

    std::unique_ptr<Boundary::estimate_viewfactors_spec> vf =
        makeBoundaryViewfactorsSpec(dict, outputDir);

    const uint8_t level = dict.get<uint8_t>("level",-1);
    const uint8_t permanent_level_tfd = dict.hasMember("permanent") ?
        dict.get<bool>("permanent") : 2;

    return std::make_unique<Boundary> (
        name,
        level,
        permanent_level_tfd,
        std::move(shape),
        std::move(frontStrat),  // front
        std::move(backStrat),   // back
        std::make_unique<Settings::Dictionary>( frontCondDict ), // front condition dict
        std::make_unique<Settings::Dictionary>( backCondDict ), // back condition dict
        std::move(probe),       // probe
        std::move(frontState),  // front state
        std::move(backState),  // back state
        std::move(vf)           // viewfactor estimation
    );

  }



} /* namespace Boundary */
