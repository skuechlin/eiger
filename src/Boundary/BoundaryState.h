/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * BoundaryState.h
 *
 *  Created on: Nov 13, 2015
 *      Author: kustepha
 */

#ifndef SRC_BOUNDARY_BOUNDARYSTATE_H_
#define SRC_BOUNDARY_BOUNDARYSTATE_H_

#include <vector>
#include "Primitives/MyIntrin.h"
#include "Primitives/MyMemory.h"
#include "Gas/PDF/MDF.h"

namespace Settings { class Dictionary; }
namespace Gas { namespace GasModels { class GasModel; } }

namespace Boundary {

  class BoundaryState {


    private:

      const uint64_t numShapeElements; // number of basic shape elements the boundary shape is comprised of

      std::vector<v4df,MyMemory::aligned_allocator<v4df>> U; // velocity of each shape element

      std::vector<std::unique_ptr<Gas::PDF>> pdfs; // pdf associated with each shape element

    public:


      v4df velocity(const uint64_t primID) const {
        return U.size() > 1 ? U[primID] : U[0];
      }

      Gas::PDF* pdf(const uint64_t primID) const {
        return pdfs.size() > 1 ? pdfs[primID].get() : pdfs[0].get();
      }

      BoundaryState(
          const uint64_t nShapeElements,
          const v4df& U,
          std::unique_ptr<Gas::PDF> pdf);

      BoundaryState(
          const uint64_t nShapeElements,
          const Settings::Dictionary& dict,
		  const std::string& inputDir,
          const bool needPDF,
          const Gas::GasModels::GasModel* gas);

  };

} /* namespace Boundary */

#endif /* SRC_BOUNDARY_BOUNDARYSTATE_H_ */
