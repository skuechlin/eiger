/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * Boundary.h
 *
 *  Created on: Apr 22, 2014
 *      Author: Stephan Kuechlin
 */

#ifndef BOUNDARY_H_
#define BOUNDARY_H_

#include <memory>
#include <string>
#include <mpi.h>

#include "Gas/GasForwardDecls.h"

namespace Shapes{ class Shape; }

namespace Particles
{
  class Particle;
  class ParticleCollection;
}

namespace MyRandom{ class Rng; }
namespace MyMPI{ class MPIMgr; }
namespace RayTracing{ struct Ray; }
namespace Settings{ class Dictionary; }


namespace Boundary
{

  namespace Strategies { class BoundaryStrategy; }

  class BoundaryProbe;

  class BoundaryState;

  class Boundary {
    // a Boundary consists of a Shape, that allows to detect and
    // compute intersections of trajectories
    // and a BoundaryStrategy, that invokes the boundary condition
    // associated with the Boundary, e.g. specular reflection

  public:
    struct estimate_viewfactors_spec {

      const uint64_t pointsPerShape_;
      const uint64_t raysPerPoint_;
      const std::string outputDirectory_;

      estimate_viewfactors_spec(
          const uint64_t pointsPerShape,
          const uint64_t raysPerPoint,
          const std::string& outputDirecory)
      : pointsPerShape_(pointsPerShape)
      , raysPerPoint_(raysPerPoint)
      , outputDirectory_(outputDirecory)
      {}

    };
  private:

    const std::string name_;
    const uint8_t level_;
    const uint8_t level_permanent_tfd_;

    std::unique_ptr<estimate_viewfactors_spec> estimate_viewfactors_;

    std::unique_ptr<Shapes::Shape>	shape_;
    std::unique_ptr<Strategies::BoundaryStrategy> 	frontBndStrat_;
    std::unique_ptr<Strategies::BoundaryStrategy> 	backBndStrat_;

    std::unique_ptr<Settings::Dictionary>             frontCndDict_;
    std::unique_ptr<Settings::Dictionary>             backCndDict_;

    // the boundary probe samples incident and outgoing particles
    // at each shape element
    std::unique_ptr<BoundaryProbe> probe_;

    // the boundary state holds velocity and optionally pdf
    // for each shape element
    std::unique_ptr<BoundaryState> frontState_;
    std::unique_ptr<BoundaryState> backState_;

    uint64_t shape_change_count_ = 0; // to be incremented on change of shape

  public:

    std::string name() const { return name_; }

    uint8_t level() const { return level_; }

    // 0: not permanent 1: permanent 2: use default
    uint8_t level_permanent_tfd() const { return level_permanent_tfd_; }

    const estimate_viewfactors_spec*
    viewfactorEstimationSpec() const { return estimate_viewfactors_.get(); };

    Shapes::Shape* shape() { return shape_.get(); }
    const Shapes::Shape* shape() const { return shape_.get(); }

    BoundaryProbe* probe() { return probe_.get(); }
    const BoundaryProbe* probe() const { return probe_.get(); }

    const BoundaryState* frontState() const { return frontState_.get(); }
    const BoundaryState* backState() const { return backState_.get(); }

    const Settings::Dictionary* frontCndDict() const { return frontCndDict_.get(); }
    const Settings::Dictionary* backCndDict() const { return backCndDict_.get(); }

    bool hasFrontStrat() const { return frontBndStrat_ != nullptr; }
    bool hasBackStrat() const { return backBndStrat_ != nullptr; }

    // return true if this boundary has sampling probe associated
    bool hasProbe() const { return probe_ != nullptr;  }

    // return true if this boundary has front state information associated
    bool hasFrontState() const { return frontState_ != nullptr; }
    // return true if this boundary has back state information associated
    bool hasBackState() const { return backState_ != nullptr; }

    uint64_t shapeChangeCount() const { return shape_change_count_; }
    void countShapeChange() { ++shape_change_count_; }


  public:

    void resetProbeSamples(const double dt); // parallel no barrier!

    void finalizeProbeSampling(const uint64_t tn, const Gas::GasModels::GasModel* const gasModel);

    /*
    void averageProbeSamples(const uint64_t tn); // parallel no barrier!

    void updateProbeState(const Gas::GasModels::GasModel* const gasModel);
    void updateProbeAverageState(const Gas::GasModels::GasModel* const gasModel);
    */

    void initAllreduceProbeSamples(
        MPI_Request *const incindent_req,
        MPI_Request *const refl_req,
        MPI_Comm comm);



  public:

    bool apply(
        Particles::Particle* const p,
        const RayTracing::Ray& hit,
        const GasModel* const gasModel,
        MyRandom::Rng& rndGen);

  public:


    Boundary(
        const std::string& name,
        const uint8_t level,
        const uint8_t level_permanent_tfd,
        std::unique_ptr<Shapes::Shape> shape,
        std::unique_ptr<Strategies::BoundaryStrategy>     frontBndStrat,
        std::unique_ptr<Strategies::BoundaryStrategy>     backBndStrat,
        std::unique_ptr<Settings::Dictionary>             frontCndDict,
        std::unique_ptr<Settings::Dictionary>             backCndDict,
        std::unique_ptr<BoundaryProbe> probe,
        std::unique_ptr<BoundaryState> frontState,
        std::unique_ptr<BoundaryState> backState,
        std::unique_ptr<estimate_viewfactors_spec> compute_viewfactors = 0
    );


    ~Boundary();


  }; // class Boundary

} // namespace Boundary


#endif /* BOUNDARY_H_ */
