/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * DiffusiveRefl.cpp
 *
 *  Created on: May 8, 2014
 *      Author: Stephan Kuechlin
 */

#include <Boundary/BoundaryStrategies/DiffuseRefl.h>
#include "Geometry/Ray.h"

#include "Boundary/Boundary.h"
#include "Boundary/BoundaryState.h"

#include "Settings/Dictionary.h"

#include "Particles/Particle.h"

#include "Gas/GasModel.h"
#include "Gas/PDF/MDF.h"

#include "Constants/Constants.h"

#include "Primitives/MyVecArithm.h"
#include "Primitives/MyError.h"
#include "Primitives/MyRandom.h"

using namespace Eigen;
using namespace Boundary;
using namespace Strategies;


uint8_t DiffuseRefl::apply(
    Particles::Particle *const p,
    const RayTracing::Ray& hit,
    const BoundaryState* bndState,
    const GasModel* const gm,
    MyRandom::Rng& rndGen) const
{

  // --- TRANSLATIONAL DOF


  // sample normal and tangential (thermal) velocities in boundary frame
  // distribution of velocities given in Bird, $12.1
  // rotated to xyz frame
  Eigen::Vector4d C = bndState->pdf(hit.primID)->sampleHalfspaceVelocity(to_eigen(hit.N),rndGen);

  // take care of case when reflecting off of back of surface
  if (hit.back_hit_tf())
    C *= -1.;

  // add boundary velocity to go to global frame
  C += to_eigen(bndState->velocity(hit.primID));

  // save
  set_velocity(p, from_eigen(C));

  if (gm->diatomic())
    {

      // --- INTERNAL

      p->Omega = from_eigen(bndState->pdf(hit.primID)->sampleRotation(rndGen));

      p->Xi    = from_eigen(bndState->pdf(hit.primID)->sampleVibration(rndGen));
    }

  return REFLECTED_FLAG;

}


