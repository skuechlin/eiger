/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * DiffuseRefl.h
 *
 *  Created on: May 8, 2014
 *      Author: Stephan Kuechlin
 */

#ifndef DIFFUSEREFL_H_
#define DIFFUSEREFL_H_

#include "BoundaryStrategy.h"


namespace Settings
{
  class Dictionary;
}

namespace Boundary
{

  namespace Strategies
  {

    class DiffuseRefl: public BoundaryStrategy {

    public:

      bool needsPDF() const override { return true; }
      bool needsState() const override { return true; }

      const char* type() const override { return "diffuse"; }

      // update velocity by computing a diffuse reflection using hit structure hit
      uint8_t apply(
          Particles::Particle *const p,
          const RayTracing::Ray& hit,
          const BoundaryState* bndState,
          const GasModel* const gasModel,
          MyRandom::Rng& rndGen) const  override;


      DiffuseRefl() = default;

      ~DiffuseRefl(){}
    };

  } // namespace BoundaryStrategies

} // namespace Boundary

#endif /* DIFFUSEREFL_H_ */
