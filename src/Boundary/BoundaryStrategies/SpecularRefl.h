/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * SpecularRefl.h
 *
 *  Created on: Apr 23, 2014
 *      Author: Stephan Kuechlin
 */

#ifndef SPECULARREFL_H_
#define SPECULARREFL_H_

#include <omp.h>

#include "BoundaryStrategy.h"

#include "Boundary/BoundaryState.h"

#include "Particles/Particle.h"

#include "Geometry/Ray.h"

namespace Boundary
{

  namespace Strategies
  {

    class SpecularRefl: public BoundaryStrategy
    {

    public:

      bool needsPDF() const override { return false; }
      bool needsState() const override { return true; }

      const char* type() const override { return "specular"; }

      // update velocity by computing a specular reflection using hit structure hit
      uint8_t apply(
          Particles::Particle *const p,
          const RayTracing::Ray& hit,
          const BoundaryState* bndState,
          const GasModel*,
          MyRandom::Rng&) const  override {

        Particles::set_velocity(
            p,
            p->C - 2.0*MyIntrin::dot(p->C - bndState->velocity(hit.primID), hit.N )*hit.N,
            p->D - 2.0*MyIntrin::dot(p->D - bndState->velocity(hit.primID), hit.N )*hit.N
            );

        return REFLECTED_FLAG; // reflection occurred
      }
    };

  }  // namespace BoundaryStrategies

} // namespace Boundary;

#endif /* SPECULARREFL_H_ */
