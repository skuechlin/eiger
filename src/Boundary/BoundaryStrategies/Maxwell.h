/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * Maxwell.h
 *
 *  Created on: Nov 11, 2014
 *      Author: kustepha
 */

#ifndef BOUNDARY_MAXWELL_H_
#define BOUNDARY_MAXWELL_H_

#include <Boundary/BoundaryStrategies/DiffuseRefl.h>
#include "BoundaryStrategy.h"
#include "SpecularRefl.h"

namespace Boundary
{

  namespace Strategies
  {

    class Maxwell: public BoundaryStrategy
    {

        const double alpha_; // accommodation coefficient
        const DiffuseRefl diff_;
        const SpecularRefl spec_;

      public:

        bool needsPDF() const override { return diff_.needsPDF() || spec_.needsPDF(); }
        bool needsState() const override { return diff_.needsState() || spec_.needsState(); }

        const char* type() const override { return "maxwell"; }

        uint8_t apply(
            Particles::Particle *const p,
            const RayTracing::Ray& hit,
            const BoundaryState* bndState,
            const GasModel* const gasModel,
            MyRandom::Rng& rndGen) const  override;


        // construct from accommodation coefficient
        Maxwell(double alpha);

        // construct from Dictionary
        Maxwell(const Settings::Dictionary& dict);

    };

  } /* namespace Boundary */

} /* namespace BoundaryStrategies */

#endif /* BOUNDARY_MAXWELL_H_ */
