/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * Vacuum.h
 *
 *  Created on: May 6, 2014
 *      Author: Stephan Kuechlin
 */

#ifndef VACUUM_H_
#define VACUUM_H_

#include "BoundaryStrategy.h"

#include "Particles/Particle.h"

namespace Boundary
{

  namespace Strategies
  {

    class Vacuum: public BoundaryStrategy {

    public:

      bool needsPDF() const override { return false; }
      bool needsState() const override { return false; }

      const char* type() const override { return "vacuum"; }

      // mark the particle for deletion
      uint8_t apply(
          Particles::Particle *const p,
          const RayTracing::Ray&,
          const BoundaryState*,
          const GasModel*,
          MyRandom::Rng&) const  override {
        Particles::mark_remove(p);
        return NO_FLAG;
      }
    };

  } // namespace BoundaryStrategies

} // namespace Boundary

#endif /* VACUUM_H_ */
