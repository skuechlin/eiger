/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * BoundaryStrategy.h
 *
 *  Created on: Apr 22, 2014
 *      Author: Stephan Kuechlin
 */

#ifndef BOUNDARYSTRATEGY_H_
#define BOUNDARYSTRATEGY_H_

#include <stdint.h>

#include "Boundary/BoundaryState.h"

#include "Gas/GasForwardDecls.h"



namespace MyRandom
{
  class Rng;
}

namespace Particles
{
  class Particle;
  class ParticleCollection;
}

namespace RayTracing
{
  struct Ray;
}

namespace Boundary
{

  class Boundary;

  namespace Strategies
  {

    class BoundaryStrategy {

    public:

      enum : uint8_t { NO_FLAG = 0, REFLECTED_FLAG = 1, FILTER_FLAG = 2 };

      virtual bool needsPDF() const = 0;

      virtual bool needsState() const = 0;

      virtual const char* type() const = 0;

      // modify position, velocity and remaining time of flight according to boundary condition
      // return combination of REFLECTEC_FLAG if reflection occurred
      // and FILTER_FLAG and/or same particle should not intersect same primitve again, resp.
      virtual uint8_t apply(
          Particles::Particle *const p,
          const RayTracing::Ray& hit,
          const BoundaryState* bndState,
          const GasModel* const gasModel,
          MyRandom::Rng& rndGen) const = 0;

      virtual ~BoundaryStrategy(){};
    };

  } // namespace BoundaryStrategies

} // namespace Boundary

#endif /* BOUNDARYSTRATEGY_H_ */
