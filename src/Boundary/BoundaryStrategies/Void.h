/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * Void.h
 *
 *  Created on: Oct 31, 2014
 *      Author: kustepha
 */

#ifndef VOID_H_
#define VOID_H_

#include "BoundaryStrategy.h"

namespace Boundary
{

  namespace Strategies
  {

    class Void: public BoundaryStrategy {

    public:

      bool needsPDF() const override { return false; }
      bool needsState() const override { return false; }

      const char* type() const override { return "void"; }

      // do not interact
      uint8_t apply(
          Particles::Particle *const,
          const RayTracing::Ray&,
          const BoundaryState*,
          const GasModel*,
          MyRandom::Rng&) const  override {
        return FILTER_FLAG; // no reflection, avoid re-hit
      }
    };

  } // namespace BoundaryStrategies

} // namespace Boundary




#endif /* VOID_H_ */
