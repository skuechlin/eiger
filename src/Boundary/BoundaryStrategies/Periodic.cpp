/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * Periodic.cpp
 *
 *  Created on: Jan 15, 2015
 *      Author: kustepha
 */

#include "Geometry/Shape.h"
#include "Geometry/Ray.h"

#include "Periodic.h"

#include "Settings/Dictionary.h"


namespace Boundary
{

	Strategies::Periodic::Periodic (const Settings::Dictionary& dict)
	: dist_( dict.get<double>("distance") )
	, dist_red_( fmax(dist_ - Shapes::ABSEPS,0.0) )
	{}

} // namespace Baoundary
