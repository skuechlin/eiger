/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * BoundaryStrategyFactory.h
 *
 *  Created on: Aug 5, 2014
 *      Author: kustepha
 */

#ifndef BOUNDARYSTRATEGYFACTORY_H_
#define BOUNDARYSTRATEGYFACTORY_H_

#include <Boundary/BoundaryStrategies/DiffuseRefl.h>
#include <Boundary/BoundaryStrategies/Maxwell.h>
#include <memory>

#include "Settings/Dictionary.h"

#include "SpecularRefl.h"
#include "Vacuum.h"
#include "Void.h"
#include "Periodic.h"

namespace Boundary
{


namespace Strategies
{

  std::unique_ptr<BoundaryStrategy>
  makeBoundaryStrategy(const Settings::Dictionary& dict);

}  // namespace BoundaryStrategies

}



#endif /* BOUNDARYSTRATEGYFACTORY_H_ */
