/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * Periodic.h
 *
 *  Created on: Jan 15, 2015
 *      Author: kustepha
 */

#ifndef BOUNDARY_PERIODIC_H_
#define BOUNDARY_PERIODIC_H_

#include "BoundaryStrategy.h"

#include "Particles/Particle.h"


namespace Settings
{
  class Dictionary;
}

namespace Boundary
{
  namespace Strategies
  {

    class Periodic : public BoundaryStrategy
    {

      const double dist_;
      const double dist_red_;

    public:

      bool needsPDF() const override { return false; }
      bool needsState() const override { return false; }

      const char* type() const override { return "periodic"; }

      // displace particle by fixed vector, do not modify time of flight remaining
      uint8_t apply(
          Particles::Particle *const p,
          const RayTracing::Ray& hit,
          const BoundaryState* ,
          const GasModel* ,
          MyRandom::Rng& ) const  override {
        // displace, take into account shape thickness
        // keep velocity, remaining dt unchanged
        translate(p, hit.N, dist_red_);
        return NO_FLAG; // no reflection, allow re-hit
      }


      Periodic (const Settings::Dictionary& dict);
    };

  } /* namespace Strategies */

}


#endif /* BOUNDARY_PERIODIC_H_ */
