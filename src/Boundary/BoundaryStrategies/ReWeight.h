/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * ReWeight.h
 *
 *  Created on: May 29, 2018
 *      Author: kustepha
 */

#ifndef SRC_BOUNDARY_REWEIGHT_H_
#define SRC_BOUNDARY_REWEIGHT_H_

#include "BoundaryStrategy.h"
#include "Particles/Particle.h"

namespace Boundary
{
  namespace Strategies
  {

    class ReWeight : public BoundaryStrategy
    {
      const double f_;

    public:

      bool needsPDF() const override { return false; }
      bool needsState() const override { return false; }

      const char* type() const override { return "re-weight"; }

      // change weight by factor f
      uint8_t apply(
          Particles::Particle *const p,
          const RayTracing::Ray& /*r*/,
          const BoundaryState* ,
          const GasModel* ,
          MyRandom::Rng& ) const override {
        // re-weight by factor f

        p->dt *= f_;
        p->w *= f_;
        return FILTER_FLAG; // no reflection, avoid re-hit
      }

      ReWeight (const double f) : f_(f) {}
    };

  } /* namespace Strategies */
} /* namespace Boundary */

#endif /* SRC_BOUNDARY_REWEIGHT_H_ */
