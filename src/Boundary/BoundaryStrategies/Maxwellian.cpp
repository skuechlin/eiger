/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * Maxwellian.cpp
 *
 *  Created on: Nov 11, 2014
 *      Author: kustepha
 */

#include <Boundary/BoundaryStrategies/Maxwell.h>
#include "Geometry/Ray.h"
#include "Settings/Dictionary.h"
#include "Primitives/MyRandom.h"
#include "Primitives/MyError.h"


namespace Boundary
{
	namespace Strategies
	{

	  uint8_t Maxwell::apply(
	              Particles::Particle *const p,
	              const RayTracing::Ray& hit,
	              const BoundaryState* bndState,
	              const GasModel* const gasModel,
	              MyRandom::Rng& rndGen) const
		{
			if (rndGen.uniform() < alpha_)
				return diff_.apply(p,hit,bndState,gasModel,rndGen);
			else
				return spec_.apply(p,hit,bndState,gasModel,rndGen);
		}

		// construct from accommodation coefficient and wall temperature [K]
		Maxwell::Maxwell(double alpha)
		: alpha_(alpha)
		, diff_()
		, spec_()
		{}

		namespace
		{
			double
			getAlpha(const Settings::Dictionary& dict)
			{
				MYASSERT(dict.hasMember("alpha"),std::string("dictionary for Maxwellian boundary strategy missing accommodation coefficient entry \"alpha\""));

				double alpha = dict.get<double>("alpha");

				MYASSERT(alpha >= 0. && alpha <= 1.,std::string("Accommodation coefficient for Maxwellian boundary strategy must be ge 0. and le 1."));

				return alpha;
			}

		}

		// construct from Dictionary
		Maxwell::Maxwell(const Settings::Dictionary& dict)
		: alpha_(getAlpha(dict))
		, diff_()
		, spec_()
		{}

	} /* namespace Boundary */

} /* namespace BoundaryStrategies */
