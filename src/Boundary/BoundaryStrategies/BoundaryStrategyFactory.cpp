/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * BoundaryStrategyFactory.cpp
 *
 *  Created on: Aug 5, 2014
 *      Author: kustepha
 */


#include <map>

#include "BoundaryStrategyFactory.h"

#include "Primitives/MyError.h"

namespace Boundary
{

  namespace Strategies
  {

    enum BoundaryStrategyType
    {
      DIFFUSE,
      SPECULAR,
      VACUUM,
      VOID,
      MAXWELL,
      PERIODIC
    };

    static std::map<std::string,BoundaryStrategyType> boundaryStrategyTypeNameMap =
    {
        {"diffuse",DIFFUSE},
        {"diffusive",DIFFUSE},
        {"specular",SPECULAR},
        {"symmetry",SPECULAR},
        {"inlet",VACUUM},
        {"stream",VACUUM},
        {"inflow",VACUUM},
        {"vacuum",VACUUM},
        {"void",VOID},
        {"maxwellian",MAXWELL},
        {"maxwell",MAXWELL},
        {"periodic",PERIODIC},
        {"displace",PERIODIC}
    };

    std::unique_ptr<BoundaryStrategy>
    makeBoundaryStrategy(
        const Settings::Dictionary& dict)
        {
      std::unique_ptr<BoundaryStrategy> pStrategy;

      MYASSERT(dict.hasMember("type"),std::string("boundary strategy dictionary missing entry \"type\""));

      std::string typeName = dict.get<std::string>("type");
      std::transform(typeName.begin(), typeName.end(), typeName.begin(), ::tolower);

      auto stratt = boundaryStrategyTypeNameMap.find(typeName);
      MYASSERT(stratt != boundaryStrategyTypeNameMap.end(),
               std::string("unknown boundary strategy type \"") + typeName + std::string("\"") );


      switch (stratt->second)
      {
        case DIFFUSE:
          pStrategy = std::unique_ptr<BoundaryStrategy>(new class DiffuseRefl());
          break;
        case SPECULAR:
          pStrategy = std::unique_ptr<BoundaryStrategy>(new class SpecularRefl());
          break;
        case VACUUM:
          pStrategy = std::unique_ptr<BoundaryStrategy>(new class Vacuum());
          break;
        case VOID:
          pStrategy = std::unique_ptr<BoundaryStrategy>(new class Void());
          break;
        case MAXWELL:
          pStrategy = std::unique_ptr<BoundaryStrategy>(new class Maxwell(dict));
          break;
        case PERIODIC:
          pStrategy = std::unique_ptr<BoundaryStrategy>(new class Periodic(dict));
          break;
      }

      return pStrategy;

        }

  }  // namespace BoundaryStrategies

} // namespace Boundary;

