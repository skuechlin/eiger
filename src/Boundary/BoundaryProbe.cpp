/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * BoundaryProbe.cpp
 *
 *  Created on: Oct 14, 2014
 *      Author: kustepha
 */

#include <iostream>
#include <iomanip>

#include <Boundary/BoundaryProbe.h>

#include "Settings/Dictionary.h"

#include "Gas/GasModel.h"

#include "Particles/Particle.h"

#include "Geometry/Shape.h"

#include "Moments/SurfaceMoments.h"
#include "Moments/AveragingStrategies.h"

#include "Parallel/MyMPI.h"

#include "Primitives/MyError.h"

#include "IO/Output.h"
#include "IO/Tecplot/Tecplot.h"

using namespace Boundary;


// number of elements
uint64_t
BoundaryProbe::nElements()
const
{
  return shape_->nElements();
}

// id
uint64_t
BoundaryProbe::id()
const
{
  return shape_->geomID();
}

void BoundaryProbe::finilizeSampling(
    const uint64_t tn
    , const Gas::GasModels::GasModel* const gm) {

  Moments::Averaging::Averager av(alpha_->operator ()(tn));
  const uint64_t n = nElements();

#pragma omp taskloop grainsize(1000) firstprivate(av) nogroup
  for (uint64_t i = 0; i < n; ++i)
    {
      // average
      m_i_av_[i].average(m_i_[i],av);
      m_r_av_[i].average(m_r_[i],av);

      // update state
      mstate_[i].T_    = gm->T( (2./3.) * (
          ( m_i_[i].Etr_v_ + m_r_[i].Etr_v_ ) / ( m_i_[i].W_v_ + m_r_[i].W_v_ )
          - .5 * velocity(i).squaredNorm() ) );
      mstate_[i].Trot_ = gm->TRot( ( m_i_[i].Erot_v_ + m_r_[i].Erot_v_ ) / ( m_i_[i].W_v_ + m_r_[i].W_v_ ) );
      mstate_[i].Tvib_ = gm->TVib( ( m_i_[i].Evib_v_ + m_r_[i].Evib_v_ ) / ( m_i_[i].W_v_ + m_r_[i].W_v_ ) );
      mstate_[i].Ma_   = gm->Ma( velocity(i), mstate_[i].T_ );

      // update average state
      mstate_av_[i].T_    = gm->T( (2./3.) * (
          ( m_i_av_[i].Etr_v_ + m_r_av_[i].Etr_v_ ) / ( m_i_av_[i].W_v_ + m_r_av_[i].W_v_ )
          - .5 * velocity_av(i).squaredNorm() ) );
      mstate_av_[i].Trot_ = gm->TRot( ( m_i_av_[i].Erot_v_ + m_r_av_[i].Erot_v_ ) / ( m_i_av_[i].W_v_ + m_r_av_[i].W_v_ ) );
      mstate_av_[i].Tvib_ = gm->TVib( ( m_i_av_[i].Evib_v_ + m_r_av_[i].Evib_v_ ) / ( m_i_av_[i].W_v_ + m_r_av_[i].W_v_ ) );
      mstate_av_[i].Ma_   = gm->Ma( velocity_av(i), mstate_av_[i].T_ );
    }
}

/*
void BoundaryProbe::averageSamples(const uint64_t tn)
{
  Moments::Averaging::Averager av(alpha_->operator ()(tn));
  const uint64_t n = nElements();

#pragma omp taskloop grainsize(1000) firstprivate(av) nogroup
  for (uint64_t i = 0; i < n; ++i)
    {
      m_i_av_[i].average(m_i_[i],av);
      m_r_av_[i].average(m_r_[i],av);
    }
}

void BoundaryProbe::updateState(const Gas::GasModels::GasModel* const gm)
{
  const uint64_t n = nElements();

#pragma omp taskloop grainsize(10000) nogroup
  for (uint64_t i = 0; i < n; ++i)
    {
      mstate_[i].T_    = gm->T( (2./3.) * (
          ( m_i_[i].Etr_v_ + m_r_[i].Etr_v_ ) / ( m_i_[i].W_v_ + m_r_[i].W_v_ )
          - .5 * velocity(i).squaredNorm() ) );
      mstate_[i].Trot_ = gm->TRot( ( m_i_[i].Erot_v_ + m_r_[i].Erot_v_ ) / ( m_i_[i].W_v_ + m_r_[i].W_v_ ) );
      mstate_[i].Tvib_ = gm->TVib( ( m_i_[i].Evib_v_ + m_r_[i].Evib_v_ ) / ( m_i_[i].W_v_ + m_r_[i].W_v_ ) );
      mstate_[i].Ma_   = gm->Ma( velocity(i), mstate_[i].T_ );
    }
}

void BoundaryProbe::updateAverageState(const Gas::GasModels::GasModel* const gm)
{
  const uint64_t n = nElements();

#pragma omp taskloop grainsize(10000) nogroup
  for (uint64_t i = 0; i < n; ++i)
    {
      mstate_av_[i].T_    = gm->T( (2./3.) * (
          ( m_i_av_[i].Etr_v_ + m_r_av_[i].Etr_v_ ) / ( m_i_av_[i].W_v_ + m_r_av_[i].W_v_ )
          - .5 * velocity_av(i).squaredNorm() ) );
      mstate_av_[i].Trot_ = gm->TRot( ( m_i_av_[i].Erot_v_ + m_r_av_[i].Erot_v_ ) / ( m_i_av_[i].W_v_ + m_r_av_[i].W_v_ ) );
      mstate_av_[i].Tvib_ = gm->TVib( ( m_i_av_[i].Evib_v_ + m_r_av_[i].Evib_v_ ) / ( m_i_av_[i].W_v_ + m_r_av_[i].W_v_ ) );
      mstate_av_[i].Ma_   = gm->Ma( velocity_av(i), mstate_av_[i].T_ );
    }
}
*/


void BoundaryProbe::resetSamples(const double dt)
{
  const uint64_t n = nElements();

#pragma omp for schedule(static) nowait
  for (uint64_t i = 0; i < n; ++i)
    {
      m_i_[i].reset(dt);
      m_r_[i].reset(dt);
    }
}



void BoundaryProbe::initAllreduceSamples(MPI_Request *const incindent_req, MPI_Request *const refl_req, MPI_Comm comm)
{
  MPI_Iallreduce
  (
      MPI_IN_PLACE,
      m_i_.data(),
      nElements(),
      Moments::surfaceMomentsMPIType(),
      Moments::surfaceMomentsMPIOp(),
      comm,
      incindent_req
  );

  MPI_Iallreduce
  (
      MPI_IN_PLACE,
      m_r_.data(),
      nElements(),
      Moments::surfaceMomentsMPIType(),
      Moments::surfaceMomentsMPIOp(),
      comm,
      refl_req
  );

}



void
BoundaryProbe::sampleIncident(
    const uint64_t elementId,
    const v4df& N,
    const double thit,
    const bool front,
    const Particles::Particle* prt)
{
  m_i_[elementId].sample(N,thit,front,prt);
}

void
BoundaryProbe::sampleReflected(
    const uint64_t elementId,
    const v4df& N,
    const double thit,
    const bool front,
    const Particles::Particle* prt)
{
  m_r_[elementId].sample(N,thit,front,prt);
}


uint BoundaryProbe::nVars() { return 19; }

double
BoundaryProbe::getVar(const uint64_t elId, const uint64_t varId)
const
{
  switch (varId) {
    case 0 : return totNumSamps(elId); 	break;
    case 1 : return numFlux_av(elId); 	break;
    case 2 : return pressure_av(elId); 	break;
    case 3 : return heatFlux_av(elId); 	break;
    case 4 : return force_av<0>(elId);	break;
    case 5 : return force_av<1>(elId);	break;
    case 6 : return force_av<2>(elId);	break;
    case 7 : return shape_->element(elId)->geomID();   break;
    case 8 : return shape_->element(elId)->primID();   break;
    case 9 : return area(elId);     break;
    case 10: return dt_av(elId);    break;
    case 11 : return T_av(elId);            break;
    case 12 : return Trot_av(elId);         break;
    case 13: return Tvib_av(elId);          break;
    case 14: return numberDensity_av(elId); break;
    case 15: return Ma_av(elId);            break;
    case 16: return velocity_av<0>(elId); break;
    case 17: return velocity_av<1>(elId); break;
    case 18: return velocity_av<2>(elId); break;
    default: return 0./0.;
  }
}

std::string
BoundaryProbe::getVarName(const uint64_t varId)
{
  switch (varId) {
    case 0 : return std::string("nSmpls_tot"); 	break;
    case 1 : return std::string("N_dot"); 		break;
    case 2 : return std::string("p"); 			break;
    case 3 : return std::string("q"); 			break;
    case 4 : return std::string("Fx");			break;
    case 5 : return std::string("Fy");			break;
    case 6 : return std::string("Fz");			break;
    case 7 : return std::string("geom_id");     break;
    case 8 : return std::string("el_id");       break;
    case 9 : return std::string("area");        break;
    case 10: return std::string("dt");          break;
    case 11: return std::string("T");           break;
    case 12: return std::string("Trot");        break;
    case 13: return std::string("Tvib");        break;
    case 14: return std::string("n");           break;
    case 15: return std::string("Ma");          break;
    case 16: return std::string("U");           break;
    case 17: return std::string("V");           break;
    case 18: return std::string("W");           break;
    default: return std::string();
  }
}

// get variable location (node/cell centered)
Tecplot::VarLoc
BoundaryProbe::getVarLoc(const uint64_t)
const
{
  if (shape_->nVerts() == shape_->nElements())
    return Tecplot::VarLoc::NodeCentered;
  else
    return Tecplot::VarLoc::CellCentered;
}


void
BoundaryProbe::writeToTecplot(
    const std::string& boundaryName,
    const std::string& dir,
    const double solTime,
    const uint64_t tn,
    const MyMPI::MPIMgr& mpiMgr
) const {

  // print message
  std::string fname = boundaryName; // note by value, reference to temporary otherwise!


  if (mpiMgr.isRoot())
    std::cout << "writing ts #" << tn << " solution on boundary \"" << fname << "\"" << std::endl;


  // prepend directory
  fname = dir + std::string("boundary_solution_") + fname;

  std::string fNameTime("");

  if (solTime >= 0.)
    {

      // append solution time
      std::stringstream fnamestr;

      fnamestr.str(std::string(""));
      fnamestr << "_at_ts_";
      fnamestr << std::setw(10);
      fnamestr << std::setfill('0');
      fnamestr << tn;
      fnamestr << "_time_";
      fnamestr << std::fixed;
      fnamestr << std::setw(20);
      fnamestr << std::setprecision(10);
      fnamestr << std::setfill('0');
      fnamestr << solTime;

      fNameTime = fnamestr.str();

    }

  fname += fNameTime;

  std::string varNames = getVarName(0);
  for (uint64_t i = 1; i < nVars(); ++i)
    varNames += (" " + getVarName(i));

  std::vector<Tecplot::VarLoc> varLoc(nVars());
  for (uint64_t i = 0; i < nVars(); ++i)
    varLoc[i] = getVarLoc(i);


  //    void write1DData2Tecplot(
  //        const std::function<double(const int32_t iDat, const int32_t iVar)>& dataFun,
  //        const std::function<int32_t(const int32_t iElem, const int32_t iVrt)>& connFun,
  //        const std::function<int32_t(std::vector<int32_t>& buffer, const int32_t iElem)>& fnConnFun,
  //        const ZoneType zoneType,
  //        const VarLoc varLoc,
  //        const DataSharing dataSharing,
  //        const FileType fileType,
  //        const int32_t nVars,
  //        const int32_t nPoints,
  //        const int32_t nElements,
  //        const std::string& fileName,
  //        const std::string& dataSetName,
  //        const std::string& zoneName,
  //        const std::string& varNames,
  //        const int32_t data_id,
  //        const double solTime,
  //        const bool writeConnectivity,
  //        const bool writeFaceNeighborConnectivity,
  //        const MyMPI::MPIMgr& mpiMgr );


  // write
  Tecplot::write1DData2Tecplot(
      [this](const int32_t iDat, const int32_t iVar)->double{ return getVar(iDat,iVar); },
      Tecplot::NO_CONNECTIVITY,
      Tecplot::NO_FACENEIGHBORS,
      shape_->tecplotZoneType(),
      varLoc,
      Tecplot::DataSharing::SHARED,
      Tecplot::FileType::SolutionFile,
      nVars(),
      shape_->nVerts(),
      nElements(),
      fname,
      boundaryName,
      boundaryName,
      varNames,
      id(),
      solTime,
      false,
      false,
      mpiMgr);

}





BoundaryProbe::BoundaryProbe(
    const Shapes::Shape* shape,
    const double FNum_ref,
    const Settings::Dictionary& dict)
: shape_( shape )
//, mIn_(shape->nElements())
//, mInAv_(shape->nElements())
//, mOut_(shape->nElements())
//, mOutAv_(shape->nElements())
{

  MYASSERT(dict.get<bool>("incident",true),"disabling incident sampling disabled");
  MYASSERT(dict.get<bool>("outgoing",true),"disabling outgoing sampling disabled");

  MYASSERT(shape->finite(),"can't create BoundaryProbe for non-finite shape");

  const uint64_t n = shape->nElements();

  mstate_.resize(n);
  mstate_av_.resize(n);

  m_i_.resize(n);
  m_i_av_.resize(n);
  m_r_.resize(n);
  m_r_av_.resize(n);

  MYASSERT( ((uint64_t)m_i_.data()      % alignof(MomentsT)) == 0, "m_i_ misaligned");
  MYASSERT( ((uint64_t)m_i_av_.data()   % alignof(MomentsT)) == 0, "m_i_av_ misaligned");
  MYASSERT( ((uint64_t)m_r_.data()      % alignof(MomentsT)) == 0, "m_r_ misaligned");
  MYASSERT( ((uint64_t)m_r_av_.data()   % alignof(MomentsT)) == 0, "m_r_av_ misaligned");

  for (uint64_t i = 0; i < n; ++i)
    {
      const double a = shape->element(i)->area();
      new(&(m_i_[i]))       MomentsT(a,FNum_ref);
      new(&(m_i_av_[i]))    MomentsT(a,FNum_ref);
      new(&(m_r_[i]))       MomentsT(a,FNum_ref);
      new(&(m_r_av_[i]))    MomentsT(a,FNum_ref);
    }




  if (dict.hasMember("averaging"))
    alpha_ = std::move(
        Moments::Averaging::makeAveragingStrategy(
            dict.get<Settings::Dictionary>("averaging") )
    );
  else
    alpha_ = std::move(
        std::unique_ptr<Moments::Averaging::AveragingStrategy>(
            new class Moments::Averaging::NoAverage() )
    );


  //  std::cout << mIn_.size() << " " << mOut_.size() << " " << mInAv_.size() <<" " << mOutAv_.size() << std::endl;


}

BoundaryProbe::~BoundaryProbe() = default;
