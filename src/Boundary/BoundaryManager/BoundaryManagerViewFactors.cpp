/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * BoundaryManagerViewFactors.cpp
 *
 *  Created on: Sep 27, 2016
 *      Author: kustepha
 */




#include "BoundaryManager.h"

#include "Boundary/Boundary.h"

#include "ViewFactor/ViewFactors.h"

#include "Primitives/MyRandom.h"
#include "Primitives/Timer.h"
#include "Parallel/MyMPI.h"

using namespace Boundary;

// estimate view factors
void BoundaryManager::estimateViewFactors(const MyMPI::MPIMgr& mpiMgr)
{

  // view factors object
  ViewFactor::ViewFactors vf;


  MyRandom::Rng rndGen;
  rndGen.long_jump(mpiMgr.rank());

  // timers
  MyChrono::TimerCollection timers;
  timers.start("viewfactors");

  for ( auto bndit = bnds_.begin(); bndit != bnds_.end(); ++bndit )
    {

      const Boundary* bnd = bndit->get();

      if ( !(bnd->viewfactorEstimationSpec()) )
        continue;

      MyMPI::barrier();

      if (mpiMgr.isRoot())
        std::cout << "\nsampling boundary \"" << bnd->name() << "\" shape view factors" << std::endl;

      // estimate view factors
      vf.estimateViewFactors(
          bnd->viewfactorEstimationSpec()->pointsPerShape_,
          bnd->viewfactorEstimationSpec()->raysPerPoint_,
          bnd->shape(),
          this,
          rndGen,
          mpiMgr);

      //      std::cout << mpiMgr_->id() << " exited computeViewFactors " <<  std::endl;

      //DEBUG:
      //vf.printViewFactors();

      //      std::cout << mpiMgr_->id() << " exited printViewFactors " <<  std::endl;


      // print message
      if (mpiMgr.isRoot())
        std::cout << "writing boundary \"" << bnd->name() << "\" shape view factors" << std::endl;


      // write view factors to disk
      vf.writeViewFactors(
          bnd->viewfactorEstimationSpec()->outputDirectory_,
          bnd->name(),
          mpiMgr);

      if (mpiMgr.isRoot())
        std::cout << std::endl;

    } // for each boundary

  timers.stop("viewfactors");

  if (mpiMgr.isRoot())
    {
      std::cout << "time (s):" << std::endl;
      timers.print_elapsed<std::chrono::seconds>("viewfactors");
      std::cout << std::endl << std::endl;
    }


} // viewfactor computation
