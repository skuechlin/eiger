/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * BoundaryManager.cpp
 *
 *  Created on: Apr 28, 2014
 *      Author: Stephan Kuechlin
 */

#include <string>
#include <iomanip>
#include <embree3/rtcore.h>

//// use internal task manager
//#ifndef TASKING_INTERNAL
//#ifndef TASKING_TBB
//#define TASKING_INTERNAL
//#endif
//#endif
//#include "../kernels/bvh/bvh.h"


#include "Settings/Dictionary.h"

#include "SimulationBox/SimulationBox.h"

#include "Gas/GasModel.h"


#include "Primitives/MyError.h"
#include "Primitives/MyRandom.h"
#include "Primitives/Timer.h"

#include "Boundary/BoundaryState.h"
#include "Boundary/Boundary.h"
#include "BoundaryManager.h"


#include "Particles/ParticleCollection.h"

#include "Geometry/Ray.h"
#include "Geometry/Shape.h"
#include "Geometry/FECollection.h"

#include "IO/Output.h"

#include "Parallel/MyMPI.h"

using namespace Boundary;

void
BoundaryManager::verify()
const
{
  MYASSERT(!bnds_.empty(),std::string("BoundaryManager incomplete: empty boundary list!"));
}

void
BoundaryManager::addBoundary(std::unique_ptr<Boundary> newBnd)
{

  MYASSERT(newBnd->shape()->rtcCompatible(),"only boundaries with ray tracing compatible shapes may be added to collection! (failed on boundary " + newBnd->name() + ")");

  // move to boundary list
  bnds_.push_back(std::move(newBnd));

  // add communication requests
  reqs_.push_back(MPI_REQUEST_NULL);
  reqs_.push_back(MPI_REQUEST_NULL);
}

bool
BoundaryManager::commit()
{

  for (auto&& b : bnds_) {
//      std::cout <<
//          b->name() << "\n" <<
//          b->shape()->name() << "\n" <<
//          (b->hasState() ? b->state()->velocity(0): zero<v4df>()) << "\n" <<
//          std::endl;

      bvh_.addShape(b->shape());
  }

  bvh_.commit();

  //  enableNonVoidBoundaries();

  return 0;
}

void BoundaryManager::commitBoundaryChange(Boundary* const b) {
  b->countShapeChange();
  bvh_.commitShapeChange(b->shape());
  bvh_.commit();
}

std::vector<std::string>
BoundaryManager::boundaryNames() const
{
  std::vector<std::string> names;
  names.reserve(bnds_.size());
  for (auto&& b : bnds_)
    names.push_back( b->name() );
  return names;
}


Boundary::Boundary*
BoundaryManager::getBoundaryByName(
    const std::string& name_of_bnd) {

  for (auto&& b : bnds_)
    {
      if ( b->name().compare(name_of_bnd) == 0 )
        return b.get();
    }
  return nullptr;
}

void BoundaryManager::getEnabledBoundaries(
    std::vector<const Boundary*>* const bnds)
const {
  bnds->clear();
  for (auto&& b : bnds_)
    if (bvh_.geom_enabled_tf( b->shape()->geomID() ))
      bnds->push_back( b.get() );
}

void BoundaryManager::enableBoundaries(
    const std::vector<const Boundary*>& bnds_to_enable)
{
  if (bnds_to_enable.size() == 0)
    {
      for (auto&& b : bnds_)
        bvh_.enable_geom( b->shape()->geomID() );
    }
  else
    {

      // enable/disable
      auto first = bnds_to_enable.begin();
      auto last = bnds_to_enable.end();

      for (auto&& b : bnds_)
        {
          if (std::find(first,last,(const Boundary*)(b.get())) != last )
            bvh_.enable_geom( b->shape()->geomID() );
          else
            bvh_.disable_geom( b->shape()->geomID() );
        }
    }

  if (bvh_.modified_tf())
    bvh_.commit();
}


void
BoundaryManager::enableBoundariesByName(
    const std::vector<std::string>& names_of_bnds_to_enable)
{

  if (names_of_bnds_to_enable.size() == 0)
    {
      for (auto&& b : bnds_)
        bvh_.enable_geom( b->shape()->geomID() );
    }
  else
    {

      // enable/disable
      auto first = names_of_bnds_to_enable.begin();
      auto last = names_of_bnds_to_enable.end();

      for (auto&& b : bnds_)
        {
          if (std::find(first,last,b->name()) != last )
            bvh_.enable_geom( b->shape()->geomID() );
          else
            bvh_.disable_geom( b->shape()->geomID() );
        }
    }

  if (bvh_.modified_tf())
    bvh_.commit();

  //  for (auto&& b : bndlst_)
  //    {
  //      std::cout << b->name() << " ";
  //      if (bvh_.enabled_tf(b->shape()->geomID()))
  //        std::cout << "enabled";
  //      else
  //        std::cout << "disabled";
  //      std::cout << std::endl;
  //    }


}


void
BoundaryManager::enablePhysicalBoundaries()
{
  for (auto&& b : bnds_)
    {
      if (b.get() == timeZoneBoundary_){
          bvh_.disable_geom( b->shape()->geomID() );
          continue;
      }

      if (b->hasFrontStrat() || b->hasBackStrat())
        bvh_.enable_geom( b->shape()->geomID() );
      else
        bvh_.disable_geom( b->shape()->geomID() );
    }

  if (bvh_.modified_tf())
    bvh_.commit();

}




bool
BoundaryManager::findBoundaryIntersection(
    const double* const __restrict__ optr,
    const double* const __restrict__ dptr,
    const double* const __restrict__ dtptr,
    uint64_t& geomID,
    uint64_t& shapeID)
const
{

  // ray for intersect determination
  RTCRayHit rayhit;
  RayTracing::IntersectContext context;

  RayTracing::initRayTrace(&context,&rayhit,*(v4df*)optr,*(v4df*)dptr,*dtptr);


  // intersect
  bvh_.intersect(&context,&rayhit);

  // save intersect results
  geomID = context.ray.geomID;
  shapeID = context.ray.primID;

  return context.ray.hit_tf();
}


const char* show_classification(double x) {
  switch(std::fpclassify(x)) {
    case FP_INFINITE:  return "Inf";
    case FP_NAN:       return "NaN";
    case FP_NORMAL:    return "normal";
    case FP_SUBNORMAL: return "subnormal";
    case FP_ZERO:      return "zero";
    default:           return "unknown";
  }
}


void
BoundaryManager::applyBoundaries(
    const uint64_t,
    const double dt_ref,
    Particles::ParticleCollection& prts,
    const GasModel* gm,
    MyRandom::Rng& rndGenParent)
{

  // reset boundary samples
  for (auto&& b : bnds_)
    b->resetProbeSamples(dt_ref);


  // per thread rng
  MyRandom::Rng rndGen( rndGenParent );

#ifndef NDEBUG
  RTCBounds scbnds;
  rtcGetSceneBounds(bvh_.scene_, &scbnds);

  const Eigen::AlignedBox4d worldbb(
      Eigen::Vector4d{ scbnds.lower_x, scbnds.lower_y, scbnds.lower_z, 0.0},
      Eigen::Vector4d{ scbnds.upper_x, scbnds.upper_y, scbnds.upper_z, 0.0} );

#endif



  static uint64_t n_to_remove;

  static uint64_t n_intersects_tot;

#pragma omp single
  {
    n_to_remove = 0;
    n_intersects_tot = 0;
  }
  // implicit barrier

#pragma omp for schedule(static) reduction(+:n_to_remove,n_intersects_tot)
  for (uint64_t i = 0; i < prts.size(); ++i)
    {

      auto* const __restrict__ p = prts.getParticle(i);
      Particles::prefetch<0,1>(prts.getParticle(i+1));

      //      MYASSERT(prts.isFinite(i) ,"particle " + std::to_string(i) + " has non-finite values before bounds:\n" + prts.prt2str(i));

      if ( __builtin_expect(Particles::is_marked_remove_tf(p),false) )
        {
          ++n_to_remove;
          continue;
        }

#ifndef NDEBUG
      if ( !worldbb.contains(*(Eigen::Vector4d*)(&p->R)) )
        {
          prts.print(i,i+1);
          MYASSERT(false,"prt out of world bb before bnds");
        }
#endif

      uint64_t nintersects = 0;

      RTCRayHit rayhit;
      RayTracing::IntersectContext context;

      while(true)
        {

          // ray for intersect determination
          // set to particle trajectory
          RayTracing::initRayTrace(&context,&rayhit,p->R,p->D,p->dt);

          // intersect
          bvh_.intersect(&context,&rayhit);

          if ( __builtin_expect(context.ray.hit_tf(), false) ) // expect no intersection
            { // there was an intersection

              // move particle to intersection point
              Particles::free_flight(p,context.ray.thit);

              bool filter = bnds_[context.ray.geomID]->apply(p,context.ray,gm,rndGen);
              ++nintersects;

              if ( Particles::is_marked_remove_tf(p) )
                {
                  // if particle crossed a remove boundary,
                  // done with this particle
                  ++n_to_remove;
                  break;
                }

              if ( filter ) // do not intersect same primitive again
                RayTracing::setFilter(&context);

            }
          else
            {
              // no intersection
              // update position assuming free flight
              Particles::free_flight(p);

              break; // end of trajectory

            } // if intersect else

        } // while(true)

      n_intersects_tot += nintersects;

    } // for all particles
  // implicit barrier

#pragma omp single
  {
    prts.setNToRemove(n_to_remove);

//    std::cout << "total number of boundary intersections: " << n_intersects_tot << std::endl;
  }

  // implicit barrier


} // apply boundaries



// return true if position pos is outside of fluid volume
bool
BoundaryManager::isOutside(const v4df& pos)
const
{

  static constexpr v4df testdirs[6] = {
      {1.,0.,0.,0.},{-1.,0.,0.,0.},
      {0.,1.,0.,0.},{0.,-1.,0.,0.},
      {0.,0.,1.,0.},{0.,0.,-1.,0.} };

  constexpr uint8_t ndirs = 1;

  // for all test directions
  for (uint64_t i = 0; i < ndirs; ++i)
    {

      RTCRayHit rayhit;
      RayTracing::IntersectContext context;

      RayTracing::initRayTrace(&context,&rayhit,pos, testdirs[i],r_max_);


      bvh_.intersect(&context,&rayhit);

      // if (no intersection) or (intersect is from back and no back intersection strategy)
      // -> outside
      if (
          (!(context.ray.hit_tf())) ||
          ( (context.ray.back_hit_tf()) && !(bnds_[context.ray.geomID]->hasBackStrat()) )
      ) return true;

    } // for all test directions

  return false;

}


// communicate boundary samples over MPI
void BoundaryManager::sampleComm(
    const uint64_t tn,
    const Gas::GasModels::GasModel* gasModel,
    const MyMPI::MPIMgr& mpiMgr)
{

  const uint64_t n = bnds_.size();

  //#pragma omp taskloop default(shared)
  for (uint64_t i = 0; i < n; ++i)
    {
      reqs_[i] = MPI_REQUEST_NULL;
      reqs_[i+n] = MPI_REQUEST_NULL;
      bnds_[i]->initAllreduceProbeSamples(&reqs_[i],&reqs_[i+n],mpiMgr.comm());
    }

  //#pragma omp taskloop default(shared)
  for (uint64_t i = 0; i < n; ++i)
    {
      MPI_Wait(&reqs_[i],MPI_STATUS_IGNORE);
      MPI_Wait(&reqs_[i+n],MPI_STATUS_IGNORE);

      bnds_[i]->finalizeProbeSampling(tn,gasModel);

      /*
      bnds_[i]->averageProbeSamples(tn);
      bnds_[i]->updateProbeState(gasModel);
      bnds_[i]->updateProbeAverageState(gasModel);
      */
    }


}

// communicate boundary samples over MPI
void BoundaryManager::initSampleComm(const MyMPI::MPIMgr& mpiMgr)
{

  const uint64_t n = bnds_.size();

  for (uint64_t i = 0; i < n; ++i)
    {
      reqs_[i] = MPI_REQUEST_NULL;
      reqs_[i+n] = MPI_REQUEST_NULL;
      bnds_[i]->initAllreduceProbeSamples(&reqs_[i],&reqs_[i+n],mpiMgr.comm());
    }

}

void BoundaryManager::finalizeSampleComm(
    const uint64_t tn,
    const Gas::GasModels::GasModel* gasModel)
{
  const uint64_t n = bnds_.size();

  MPI_Request* reqs = reqs_.data();

#pragma omp taskloop
  for (uint64_t i = 0; i < n; ++i)
    {
      MPI_Wait(&reqs[i],MPI_STATUS_IGNORE);
      MPI_Wait(&reqs[i+n],MPI_STATUS_IGNORE);

      bnds_[i]->finalizeProbeSampling(tn,gasModel);

      /*
      bnds[i]->averageProbeSamples(tn);
      bnds[i]->updateProbeState(gasModel);
      bnds[i]->updateProbeAverageState(gasModel);
      */
    }
}


BoundaryManager::CallbackT
BoundaryManager::popCallback()
{
  if (!(callbacks_.empty()))
    {
      BoundaryManager::CallbackT ret = std::move( callbacks_.back() );
      callbacks_.pop_back();
      return ret;
    }
  else
    return 0;
}

BoundaryManager::~BoundaryManager() = default;

