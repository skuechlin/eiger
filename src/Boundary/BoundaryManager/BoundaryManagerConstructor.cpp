/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * BoundaryManagerConstructor.cpp
 *
 *  Created on: Jun 25, 2018
 *      Author: kustepha
 */

#include <memory>
#include <utility>

#include "SimulationBox/SimulationBox.h"

#include "Settings/Dictionary.h"


#include "BoundaryManager.h"

#include "Boundary/Boundary.h"
#include "Boundary/BoundaryFactory.h"
#include "Boundary/BoundaryProbe.h"

#include "Boundary/BoundaryStrategies/ReWeight.h"

#include "Geometry/FECollection.h"

#include "IO/Output.h"

#include "Gas/PDF/MDF.h"

using namespace Boundary;

BoundaryManager::BoundaryManager(
    const Gas::GasModels::GasModel* gas,
    const double dt_coarsening_factor_,
    const double FNum_ref,
    const SimulationBox& box,
    const Settings::Dictionary& condsDict,
    const Settings::Dictionary& statesDict,
    const Settings::Dictionary& boundsDict,
    const std::string& inputDirectory,
    const std::string& outputDirectory)
: bvh_()
, r_max_( 1.1*(box.extent().norm()) )
{


  //  enum RTCSceneFlags
  //  {
  //    /* dynamic type flags */
  //    RTC_SCENE_STATIC     = (0 << 0),    //!< specifies static scene
  //    RTC_SCENE_DYNAMIC    = (1 << 0),    //!< specifies dynamic scene
  //
  //    /* acceleration structure flags */
  //    RTC_SCENE_COMPACT    = (1 << 8),    //!< use memory conservative data structures
  //    RTC_SCENE_COHERENT   = (1 << 9),    //!< optimize data structures for coherent rays
  //    RTC_SCENE_INCOHERENT = (1 << 10),    //!< optimize data structures for in-coherent rays (enabled by default)
  //    RTC_SCENE_HIGH_QUALITY = (1 << 11),  //!< create higher quality data structures
  //
  //    /* traversal algorithm flags */
  //    RTC_SCENE_ROBUST     = (1 << 16)     //!< use more robust traversal algorithms
  //  };


  //  "boundary": [
  //               {
  //    "name":         "boundaryname",
  //    "condition":    "conditionname"
  //    "geometry": {
  //      "type": "stl" | "box",
  //      if type == stl: "file": "filename",
  //      if type == box: "face": "top"|"bottom"|"left"|"right"|"front"|"back",
  //    }
  //
  //
  //
  //               }
  //  ]

  MYASSERT(boundsDict.isArray(),"Boundaries dictionary must be array.");

  std::string cndType;

  std::unique_ptr<Boundary> b;

  bool addViewFactorCallback = false;

  // add all defined boundaries
  for (uint i = 0; i < boundsDict.size(); ++i)
    {

      Settings::Dictionary cndDict, stateDict,
      frontCndDict, backCndDict,
      frontStateDict, backStateDict,
      bndDict, outDict;

      bndDict = boundsDict.get<Settings::Dictionary>(i);

      if (bndDict.hasMember("is time zone boundary") && bndDict.get<bool>("is time zone boundary"))
        {

          MYASSERT(bndDict.hasMember("name"),"boundary dictionary missing entry \"name\"!");

          const std::string name = bndDict.get<std::string>("name");

          b = std::make_unique<Boundary>(
              name,
              uint8_t(-1), // no level
              0, // level not permanent
              std::make_unique<Shapes::FECollection>(),
              std::make_unique<Strategies::ReWeight>( dt_coarsening_factor_ ),
              std::make_unique<Strategies::ReWeight>( 1./dt_coarsening_factor_ ),
              std::make_unique<Settings::Dictionary>(frontCndDict),
              std::make_unique<Settings::Dictionary>(backCndDict),
              nullptr, // no probe
              nullptr, // no front state
              nullptr  // no back state
          );

        }
      else
        {

          if (bndDict.hasMember("condition"))
            {
              cndDict = bndDict.get<Settings::Dictionary>("condition");

              if (cndDict.isString())
                frontCndDict  = condsDict.getDictByValue(
                    "name",
                    bndDict.get<std::string>("condition") );
              else
                {
                  if (cndDict.hasMember("front"))
                    frontCndDict  = condsDict.getDictByValue(
                        "name",
                        cndDict.get<std::string>("front") );

                  if (cndDict.hasMember("back"))
                    backCndDict  = condsDict.getDictByValue(
                        "name",
                        cndDict.get<std::string>("back") );
                }
            }

          if (bndDict.hasMember("state"))
            {
              stateDict = bndDict.get<Settings::Dictionary>("state");

              if (stateDict.isString())
                frontStateDict  = statesDict.getDictByValue(
                    "name",
                    stateDict.get<std::string>() );
              else
                {
                  if (stateDict.hasMember("front"))
                    frontStateDict  = statesDict.getDictByValue(
                        "name",
                        stateDict.get<std::string>("front") );

                  if (stateDict.hasMember("back"))
                    backStateDict  = statesDict.getDictByValue(
                        "name",
                        stateDict.get<std::string>("back") );
                }
            }

          b = std::move(
              makeBoundary(
                  gas,
                  FNum_ref,
                  box,
                  frontCndDict,
                  backCndDict,
                  frontStateDict,
                  backStateDict,
                  bndDict,
                  inputDirectory,
                  outputDirectory)
          );


        }

      if (b->viewfactorEstimationSpec())
        addViewFactorCallback = true;

      if (bndDict.hasMember("output"))
        {
          outDict = bndDict.get<Settings::Dictionary>("output");
          const std::string n = b->name();

          // solution
          if (b->hasProbe())
            {
              const auto pb = b.get();

              const std::string dir = MyString::makeLastCharDirSep(
                  MyString::catDirs(outputDirectory,outDict.get<std::string>("directory","")) );

              callbacks_.emplace_back(
                  newCallback(
                      [pb,n,dir](
                          const double solTime,
                          const uint64_t tn,
                          const MyMPI::MPIMgr& mpiMgr,
                          MyChrono::TimerCollection& timers ) {

                static uint64_t shapeChangeCountAfterLastOutput = -1;

                timers.start("output"); // own master region
#pragma omp master
                {
                  if (pb->shapeChangeCount() != shapeChangeCountAfterLastOutput)
                    {
                      pb->shape()->writeToTecplot(n,dir,solTime,tn,mpiMgr);
                      shapeChangeCountAfterLastOutput = pb->shapeChangeCount();
                    }
                  pb->probe()->writeToTecplot(n,dir,solTime,tn,mpiMgr);
                } // master region
                timers.stop("output");
#pragma omp barrier
              },
              outDict
                  ) // newCallback
              ); // emplace_back
            } // if hasProbe()
          else
            {
              // just output the shape
              const auto s = b->shape();

              callbacks_.emplace_back(
                  newOutput(
                      [s,n](
                          const std::string& dir,
                          const double solTime,
                          const uint64_t tn,
                          const MyMPI::MPIMgr& mpiMgr )->void{
                s->writeToTecplot(n,dir,solTime,tn,mpiMgr);
              },
              outDict,
              outputDirectory) // newOutput
              );


            } // else if hasProbe()
        } // if hasOutput()

      addBoundary( std::move(b) );

      if (dt_coarsening_factor_ != 1.0 &&
          bndDict.hasMember("is time zone boundary") &&
          bndDict.get<bool>("is time zone boundary") )
        {
          MYASSERT(timeZoneBoundary() == nullptr,"multiple boundaries specified as time zone boundary");
          timeZoneBoundary_ = bnds_.back().get();
        }

    } // for all boundaries

  if (dt_coarsening_factor_ != 1.0)
    MYASSERT(timeZoneBoundary() != nullptr,
             "local time step enabled, at least one boundary must be specified as time zone boundary!");



  //#ifndef NDEBUG
  //  std::cout << "finished adding " << bndlst_.size() << " boundaries, committing" << std::endl;
  //#endif

  // finished adding boundaries, so finalize boundaries
  commit();

  // at least one boundary requires view factor estimation, so add appropriate callback
  if (addViewFactorCallback)
    {
      callbacks_.emplace_back(
          newCallback(
              [this](
                  const double,
                  const uint64_t,
                  const MyMPI::MPIMgr& mpiMgr,
                  MyChrono::TimerCollection& timers)
                  ->void{
        timers.start("viewfactors");
#pragma omp master
        estimateViewFactors(mpiMgr);

        timers.stop("viewfactors");
#pragma omp barrier
      },-1,-1,1 )
      );
    }

}
