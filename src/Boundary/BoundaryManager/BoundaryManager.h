/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * BoundaryManager.h
 *
 *  Created on: Apr 28, 2014
 *      Author: Stephan Kuechlin
 */

#ifndef BOUNDARYMANAGER_H_
#define BOUNDARYMANAGER_H_


#include <memory>
#include <vector>
#include <list>
#include <map>
#include <mpi.h>

#include "Gas/GasForwardDecls.h"

#include "BVH/BVH.h"

namespace MyMPI     { class MPIMgr;             }
namespace MyRandom  { class Rng;                }
namespace Settings  { class Dictionary;         }
namespace Particles { class ParticleCollection; }

class GenericCallback;
class SimulationBox;


namespace Boundary
{

  class Boundary;

  class BoundaryManager {
      // maintains a list of all boundaries in the domain
      // provides means of enforcing these

      typedef std::unique_ptr<Boundary> BndT;

      typedef std::map<std::string,std::tuple<uint64_t,Boundary*,bool>> BndNameMapT;

      typedef std::unique_ptr<GenericCallback> CallbackT;

      // the boundaries
      std::vector<BndT> bnds_;

      // communication requests
      std::vector<MPI_Request> reqs_;

      // bounding volume hierarchy for intersection queries
      BVH::BVH bvh_;

      // Buffer of outputs and other callbacks created at constructions
      // will be moved to core during simulation setup
      std::list<CallbackT> callbacks_;

      // distance guaranteed to be greater than distance between any object in bvh
      const double r_max_;

      // special boundary with automatically generated shape and strategy
      // used to re-weight particles for local time stepping
      Boundary* timeZoneBoundary_ = nullptr;


    private:

      // add a boundary to collection
      void addBoundary(std::unique_ptr<Boundary> newBnd);

      // must be called before any call to applyBoundaries()
      bool commit();

    private:

      // estimate viewfactors for specified geometries
      void estimateViewFactors(const MyMPI::MPIMgr& mpiMgr);

    public:

      void verify() const;

      CallbackT popCallback();

    public:

      std::vector<std::string> boundaryNames() const;

      Eigen::AlignedBox4d worldBB() const { return bvh_.worldBB(); }

      uint64_t nBoundaries() const { return bnds_.size(); }
      const Boundary* getBoundary(const uint64_t i) const { return bnds_[i].get(); }
      Boundary* getBoundaryByName(const std::string& name_of_bnd);

      Boundary* timeZoneBoundary() { return timeZoneBoundary_; }
      const Boundary* timeZoneBoundary() const { return timeZoneBoundary_; }

      void commitBoundaryChange(Boundary*const b);

      void enableBoundaries(
          const std::vector<const Boundary*>& bnds_to_enable);

      void enableBoundariesByName(
          const std::vector<std::string>& names_of_bnds_to_enable);

      // enable only those boundaries with boundary strategy attached,
      // also disables time-zone boundary
      void enablePhysicalBoundaries();

      void getEnabledBoundaries(std::vector<const Boundary*>* const bnds) const;

      // find boundary intersections of trajectory of particle i
      bool findBoundaryIntersection(
          const double* const __restrict__ optr,
          const double* const __restrict__ dptr,
          const double* const __restrict__ dtptr,
          uint64_t& geomID,
          uint64_t& shapeID)  const;

    public:

      // apply boundary conditions
      void applyBoundaries(
          const uint64_t tn,
          const double dt_ref,
          Particles::ParticleCollection& prts,
          const GasModel* gasModel,
          MyRandom::Rng& rndGen);

      // return true if position pos is outside of fluid volume
      bool isOutside(const v4df& pos) const;

      // test bounding box for intersection with geometry
      bool test_AABB(const Eigen::AlignedBox4d& bb) const { return bvh_.test_AABB(bb); }

      // print the bvh to std::cout
      void printBVH() const { bvh_.print(); }


      // communicate boundary samples over MPI
      void sampleComm(
          const uint64_t tn,
          const Gas::GasModels::GasModel* gasModel,
          const MyMPI::MPIMgr& mpiMgr);

      // communicate boundary samples over MPI
      void initSampleComm(const MyMPI::MPIMgr& mpiMgr);

      void finalizeSampleComm(const uint64_t tn, const Gas::GasModels::GasModel* gasModel);


      BoundaryManager(
          const Gas::GasModels::GasModel* gas,
          const double dt_coarsening_factor,
          const double FNum_ref,
          const SimulationBox& box,
          const Settings::Dictionary& condsDict,
          const Settings::Dictionary& statesDict,
          const Settings::Dictionary& boundsDict,
          const std::string& inputDirectory = "",
          const std::string& outputDirectory = "");

      ~BoundaryManager();

  };

} // namespace Boundary

#endif /* BOUNDARYMANAGER_H_ */
