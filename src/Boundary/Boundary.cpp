/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * Boundary.cpp
 *
 *  Created on: Aug 5, 2014
 *      Author: kustepha
 */

#include <iostream>
#include <iomanip>

#include <mpi.h>

#include "Boundary.h"

#include "Primitives/MyError.h"

#include "Settings/Dictionary.h"

#include "Geometry/Ray.h"
#include "Geometry/Shape.h"

#include "Particles/Particle.h"

#include "BoundaryState.h"
#include "BoundaryProbe.h"
#include "BoundaryStrategies/BoundaryStrategy.h"

#include "Gas/PDF/MDF.h"


#include "Parallel/MyMPI.h"

namespace Boundary
{


  using namespace Strategies;
  using namespace Shapes;


  uint8_t back_intersect_warning_and_removal(
      const std::string& name,
      Particles::Particle* prt,
      const RayTracing::Ray& hit)
  {
#pragma omp critical (COUT)
    {
      std::cout << "boundary \"" << name << "\" intersected from back!" << std::endl;
      std::cout << std::setprecision(15);
      std::cout << "sid:    " << hit.primID << std::endl;
      std::cout << "N:      " << (*(Eigen::Vector4d*)(&(hit.N))).transpose(); std::cout << std::endl;
      std::cout << "hit t:  " << hit.thit << std::endl;
      std::cout << "prt dt: " << prt->dt << std::endl;
      std::cout << "R:      " << prt->R; std::cout << std::endl;
      std::cout << "r:      " << to_eigen(prt->R).head(2).norm(); std::cout << std::endl;
      std::cout << "D:      " << prt->D; std::cout << std::endl;
      std::cout << "R+t*D:  " << (prt->R + hit.thit * prt->D); std::cout << std::endl;
      std::cout << "R+dt*D: " << ( prt->R + prt->dt * prt->D) << std::endl;
      std::cout << "removing particle from simulation..." << std::endl;
      //		MYASSERT(false,std::string("boundary \"") + name_ + std::string("\" intersected from back!"));
    }

    Particles::mark_remove(prt);

    return 0; // no reflection, no need to filter
  }


  bool Boundary::apply(
      Particles::Particle* const p,
      const RayTracing::Ray& h,
      const GasModel* const gm,
      MyRandom::Rng& rndGen)
  {
    uint8_t flags;
    //    bool refl; // reflection occurred
    //    bool filter; // prevent same primitive from being intersected again by this particle

    if (probe_.get() != nullptr)
      probe_->sampleIncident(
          h.primID,
          h.N,
          h.thit,
          h.front_hit_tf(),
          p );

    if ( __builtin_expect(h.front_hit_tf(), true)  )
      {
        flags = frontBndStrat_->apply(p,h,frontState(),gm,rndGen);
      }
    else
      {
        if (__builtin_expect(backBndStrat_.get() != nullptr ,true))
          flags = backBndStrat_->apply(p,h,backState(),gm,rndGen);
        else
          flags = back_intersect_warning_and_removal(name_, p, h);
      }

    if ((probe_.get() != nullptr) && !!(flags & BoundaryStrategy::REFLECTED_FLAG))
      probe_->sampleReflected(
          h.primID,
          h.N,
          h.thit,
          h.front_hit_tf(),
          p );

    return !!(flags & BoundaryStrategy::FILTER_FLAG);

  }

  void Boundary::initAllreduceProbeSamples(
      MPI_Request *const incindent_req,
      MPI_Request *const refl_req,
      MPI_Comm comm)
  {
    if (probe_.get() == nullptr) return;
    probe_->initAllreduceSamples(incindent_req,refl_req,comm);
  }


  void Boundary::resetProbeSamples(const double dt)
  {
    if (probe_.get() == nullptr) return;
    probe_->resetSamples(dt);
  }

  void Boundary::finalizeProbeSampling(
      const uint64_t tn,
      const Gas::GasModels::GasModel* const gasModel) {
    if (probe_.get() == nullptr) return;
    probe_->finilizeSampling(tn, gasModel);
  }

  /*
  void
  Boundary::averageProbeSamples(const uint64_t tn)
  {
    if (probe_.get() == nullptr) return;
    probe_->averageSamples(tn);
  }

  void Boundary::updateProbeState(const Gas::GasModels::GasModel* const gasModel)
  {
    if (probe_.get() == nullptr) return;
    probe_->updateState(gasModel);
  }

  void Boundary::updateProbeAverageState(const Gas::GasModels::GasModel* const gasModel)
  {
    if (probe_.get() == nullptr) return;
    probe_->updateAverageState(gasModel);
  }

   */

  Boundary::Boundary(
      const std::string& name,
      const uint8_t level,
      const uint8_t level_permanent_tfd,
      std::unique_ptr<Shapes::Shape> shape,
      std::unique_ptr<Strategies::BoundaryStrategy> frontBndStrat,
      std::unique_ptr<Strategies::BoundaryStrategy> backBndStrat,
      std::unique_ptr<Settings::Dictionary>         frontCndDict,
      std::unique_ptr<Settings::Dictionary>         backCndDict,
      std::unique_ptr<BoundaryProbe> probe,
      std::unique_ptr<BoundaryState> frontState,
      std::unique_ptr<BoundaryState> backState,
      std::unique_ptr<estimate_viewfactors_spec> compute_viewfactors
  )
  : name_(name)
  , level_(level)
  , level_permanent_tfd_(level_permanent_tfd)
  , estimate_viewfactors_( std::move(compute_viewfactors) )
  , shape_(std::move(shape))
  , frontBndStrat_(std::move(frontBndStrat))
  , backBndStrat_(std::move(backBndStrat))
  , frontCndDict_(std::move(frontCndDict))
  , backCndDict_(std::move(backCndDict))
  , probe_(std::move(probe))
  , frontState_(std::move(frontState))
  , backState_(std::move(backState))
  {

    MYASSERT(shape_ != 0,
             std::string("attempted to create boundary \"")
    + name_
    + std::string("\" without shape!") );


    if (frontBndStrat_ && frontBndStrat_->needsPDF()) {
        MYASSERT( this->frontState() != nullptr && this->frontState()->pdf(0) != nullptr,
                  std::string("front boundary strategy for boundary \"")
        + name_
        + std::string("\" depends on state pdf, but none available") );
    }

    if (backBndStrat_ && backBndStrat_->needsPDF()) {
        MYASSERT( this->backState() != nullptr && this->backState()->pdf(0) != nullptr,
                  std::string("back boundary strategy for boundary \"")
        + name_
        + std::string("\" depends on state pdf, but none available") );
    }


    // print summary

    if( MyMPI::isRoot(MPI_COMM_WORLD,0) ) {

#pragma omp critical (COUT)
        {
          std::cout << "constructed boundary " << name_                                  << "\n" <<
              "  shape           " << this->shape()->name()                               << "\n" <<
              "  front strategy  " << (hasFrontStrat() ? frontBndStrat_->type() : "none") << "\n" <<
              "  back strategy   " << (hasBackStrat()  ? backBndStrat_->type()  : "none") << "\n" <<
              "  probe           " << (hasProbe() ? "yes" : "none")                       << "\n" <<
              "  front state     " << (hasFrontState() ? "yes" : "no")                    << "\n" <<
              "  back state      " << (hasBackState() ? "yes" : "no")                     << "\n" <<
              "  front state pdf " << ((hasFrontState() && (this->frontState()->pdf(0) != nullptr)) ? "yes" : "no") << "\n" <<
              "  back state pdf  " << ((hasBackState() && (this->backState()->pdf(0) != nullptr)) ? "yes" : "no") << "\n";
          std::cout <<std::endl;


        }

    }

  }


  Boundary::~Boundary() = default;

}
