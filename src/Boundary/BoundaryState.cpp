/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * BoundaryState.cpp
 *
 *  Created on: Nov 13, 2015
 *      Author: kustepha
 */

#include <iostream>

#include <Boundary/BoundaryState.h>

#include "Geometry/Shape.h"

#include "Settings/Dictionary.h"
#include "Settings/FileDict.h"

#include "IO/Tecplot/Tecplot.h"

#include "FieldData/FieldData.h"

#include "Gas/GasModel.h"
#include "Gas/PDF/MDF.h"


namespace Boundary {

  BoundaryState::BoundaryState(
      const uint64_t _nShapeElements,
      const v4df& _U,
      std::unique_ptr<Gas::PDF> _pdf)
  : numShapeElements(_nShapeElements)
  , U(1,_U)
  {
    pdfs.push_back(std::move(_pdf));
  }

  BoundaryState::BoundaryState(
      const uint64_t _nShapeElements,
      const Settings::Dictionary& dict,
      const std::string& inputDir,
      const bool needPDF,
      const Gas::GasModels::GasModel* gas)
  : numShapeElements(_nShapeElements)
  {
    std::vector<std::string>  varNamesInFile;
    std::vector<std::string>  varNamesRequiredFromFile;
    std::vector<std::string>  varNamesOptionalFromFile;

    if ( !dict.isEmpty() && dict.hasMember("file") ){
        Settings::FileDict fdict( dict.get<Settings::Dictionary>("file"), inputDir );
        varNamesInFile = Tecplot::getVarNamesFromTecplot(fdict.full_file_name);
    }

    if (!dict.isEmpty() && dict.hasMember("velocity")) {
        MYASSERT(dict.get<Settings::Dictionary>("velocity").size()==3,"velocity dict must be array size >= 3");
        double Uarray[3] = {0.};
        dict.get( Uarray, "velocity",0,3);
        U = {v4df{Uarray[0],Uarray[1],Uarray[2],0.}};
    } else if (
        std::find(varNamesInFile.begin(), varNamesInFile.end(), "U") != varNamesInFile.end() &&
        std::find(varNamesInFile.begin(), varNamesInFile.end(), "V") != varNamesInFile.end() &&
        std::find(varNamesInFile.begin(), varNamesInFile.end(), "W") != varNamesInFile.end() ) {
        varNamesRequiredFromFile.insert(varNamesRequiredFromFile.end(),{"U","V","W"});
    } else {
        // velocity == zero
        U = {zero<v4df>()};
    }

    if (needPDF) {

        MYASSERT(!dict.isEmpty(),"boundary state needs pdf, but dictionary is empty!");

        std::vector<std::string> req;
        std::vector<std::string> opt;
        Gas::variablesRequiredToMakePDF(req,opt,dict);

        if (dict.hasAllMembers(req))
          pdfs.push_back(std::move(Gas::makePDF(gas,dict,true)));
        else {
            varNamesRequiredFromFile.insert(varNamesRequiredFromFile.end(),req.begin(),req.end());
            varNamesOptionalFromFile.insert(varNamesOptionalFromFile.end(),opt.begin(),opt.end());
        }

    } else {
        pdfs.push_back(nullptr);
    }

    if(varNamesRequiredFromFile.size()>0) {

        MYASSERT(!dict.isEmpty(),"boundary state needs variable from file, but dictionary is empty!");

        // read file
        Settings::FileDict fdict( dict.get<Settings::Dictionary>("file"), inputDir );
        std::unique_ptr<FieldData> fieldData = Tecplot::readTecplot1DData(
            fdict.full_file_name,
            varNamesRequiredFromFile,
            varNamesOptionalFromFile);

        if (U.size()==0) {

            // read per-element velocities from file

            const double* _U = fieldData->getVarData("U");
            const double* _V = fieldData->getVarData("V");
            const double* _W = fieldData->getVarData("W");

            const uint64_t numData = fieldData->getNData("U");
            MYASSERT( numData == fieldData->getNData("V"), "field data U and V size mismatch");
            MYASSERT( numData == fieldData->getNData("W"), "field data U and W size mismatch");
            MYASSERT( uint64_t(U.size()) == numData,"field data, velocity storage size mismatch");

            for (uint64_t i = 0; i < numData; ++i, ++_U,++_V,++_W)
              {
                const double Uval = *_U;
                const double Vval = *_V;
                const double Wval = *_W;

                MYASSERT(std::isfinite(Uval),
                         std::string("boundary state received illegal U velocity value from field data: ")
                + std::to_string(Uval));
                MYASSERT(std::isfinite(Vval),
                         std::string("boundary state received illegal V velocity value from field data: ")
                + std::to_string(Vval));
                MYASSERT(std::isfinite(Wval),
                         std::string("boundary state received illegal W velocity value from field data: ")
                + std::to_string(Wval));

                U[i] = v4df{Uval,Vval,Wval,0.0};
              }

        }

        if (needPDF)
          Gas::makePDF(pdfs,fieldData.get(),gas,dict,true);

    }

  }


} /* namespace Boundary */
