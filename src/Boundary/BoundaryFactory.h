/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * BoundaryFactory.h
 *
 *  Created on: Oct 29, 2015
 *      Author: kustepha
 */

#ifndef SRC_BOUNDARY_BOUNDARYFACTORY_H_
#define SRC_BOUNDARY_BOUNDARYFACTORY_H_

#include <memory>

namespace Settings
{
  class Dictionary;
}

namespace Gas { namespace GasModels { class GasModel; } }

class SimulationBox;

namespace Boundary {

  class Boundary;


  std::unique_ptr<Boundary>
  makeBoundary(
      const Gas::GasModels::GasModel* gas,
      const double FNum_ref,
      const SimulationBox& box,
      const Settings::Dictionary& frontCondDict,
      const Settings::Dictionary& backCondDict,
      const Settings::Dictionary& frontStateDict,
      const Settings::Dictionary& backStateDict,
      const Settings::Dictionary& dict,
      const std::string& inputDir,
      const std::string& outputDir);


} /* namespace Boundary */

#endif /* SRC_BOUNDARY_BOUNDARYFACTORY_H_ */
