/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * InitialConditions.h
 *
 *  Created on: Jan 25, 2017
 *      Author: kustepha
 */

#ifndef SRC_INITIALCONDITIONS_INITIALCONDITIONS_H_
#define SRC_INITIALCONDITIONS_INITIALCONDITIONS_H_

#include <vector>
#include <memory>
#include <mpi.h>

#include <Eigen/Dense>

#include <nanoflann.hpp>


#include "Primitives/MyIntrin.h"

namespace Gas
{
  class MDF;

  namespace GasModels
  {
    class GasModel;
  }
}

namespace Settings
{
  class Dictionary;
}

namespace Grids
{
  class RegularGrid;
}

namespace Particles
{
  class Particle;
  class ParticleCollection;
}

namespace MyRandom
{
  class Rng;
}

class SimulationBox;


class InitialConditions
{

    static constexpr uint64_t ndims = 3;
    typedef Eigen::Matrix<double,Eigen::Dynamic,ndims,Eigen::RowMajor>  pos_store_t;
    typedef nanoflann::KDTreeEigenMatrixAdaptor< pos_store_t, ndims, nanoflann::metric_L2>  kd_tree_t;

    pos_store_t x_;
    std::unique_ptr<kd_tree_t> tree_ = nullptr;

    std::vector< std::shared_ptr<Gas::MDF> > mdf_;


    void make_mdf_vec(std::vector<Gas::MDF*>& mdf,
                      Grids::RegularGrid *const pg) const;

    uint64_t partition_sampling(const Grids::RegularGrid *const pg,
                                uint64_t* const tfirstCell,
                                uint64_t* const tendCell) const;

    void resize_particle_storage(
        Particles::ParticleCollection& prts,
        const uint64_t n_to_add) const;

  public:

    template<typename WFFunT, typename OQFunT>
    void sample(
        Particles::ParticleCollection& prts,
        Grids::RegularGrid *const particleGrid,
        const double dt_ref,
        WFFunT&& weightFacFun,
        OQFunT&& isOutsideQ,
        MyRandom::Rng& rndGen);


  public:

    InitialConditions(
        const double FN,
        const Gas::GasModels::GasModel* gasModel,
        const Settings::Dictionary& dict,
        const std::string& inputDirectory,
        const MPI_Comm& comm);

    ~InitialConditions() = default;
};

#include "InitialConditionsSampling.hpp"

#endif /* SRC_INITIALCONDITIONS_INITIALCONDITIONS_H_ */
