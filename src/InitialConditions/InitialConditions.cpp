/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * InitialConditions.cpp
 *
 *  Created on: Jan 25, 2017
 *      Author: kustepha
 */


#include <Gas/PDF/MDF.h>
#include <omp.h>

#include "InitialConditions.h"

#include "Gas/PDF/MDF.h"

#include "IO/Tecplot/Tecplot.h"

#include "Particles/ParticleCollection.h"

#include "Grid/RegularGrid.h"

#include "Primitives/MyRandom.h"
#include "Primitives/MyString.h"
#include "Primitives/Partitioning.h"

#include "FieldData/FieldData.h"

#include "Settings/Dictionary.h"

void InitialConditions::make_mdf_vec(
    std::vector<Gas::MDF*>& mdf,
    Grids::RegularGrid *const pg) const {

  const uint64_t nCells = pg->nNonGhost();

  mdf.resize( nCells );

  // use count array to count number of particles to sample per cell
  pg->resetCount();

#pragma omp parallel
  {

    // for each grid cell, find closest specified mdf

    if (mdf_.size() > 1)
      {
        MYASSERT(tree_ != nullptr,"kd tree missing");
        MYASSERT(mdf_.size() == nCells,"not enough initital conditions for grid");

#pragma omp for schedule(static)
        for (uint64_t i = pg->first_; i < pg->last_; ++i)
          {
            // find specified pdf closest to cell center

            auto center = pg->cellCenter(i);
            pos_store_t::Index j;
            pos_store_t::Scalar r2;
            tree_->query((double*)(&center[0]),1ul,&j,&r2);

            mdf[i - pg->first_] = mdf_[j].get();
          }

      }
    else if (mdf_.size() == 1)
      {
        //
        //#pragma omp single nowait
        //        {
        //          std::cout
        //              << "FNum:                   " << pdf_.back()->w() / pdf_.back()->m() << "\n"
        //              << "density:                " << pdf_.back()->n() << "\n"
        //              << "density of comp prt:    " << pdf_.back()->N() << "\n"
        //              << "temperature:            " << ((Gas::Maxwellian*)(pdf_.back().get()))->T() << "\n"
        //              << "pressure:               " << pdf_.back()->n() * ((Gas::Maxwellian*)(pdf_.back().get()))->T() * Constants::kB << "\n"
        //              << "vol of first cell:      " << pG->volume_[pG->first_] << "\n"
        //              << "mean Np in first cell:  " << pdf_.back()->N() * pG->volume_[pG->first_] << "\n"
        //              << "ncells:                 " << pG->nCells() << "\n"
        //              << "mean Np in ncells:      " << pdf_.back()->N() * pG->volume_[pG->first_] * pG->nCells() << std::endl;
        //        }

        // copy ptr to single pdf
#pragma omp for schedule(static)
        for (uint64_t i = 0; i < nCells; ++i )
          mdf[i] = mdf_.back().get();
      }
  }

}

uint64_t InitialConditions::partition_sampling(const Grids::RegularGrid *const pg,
                                               uint64_t* const tfirstCell,
                                               uint64_t* const tendCell)
const
{
  // partition sampling
  const uint64_t nCells = pg->nNonGhost();
  const uint64_t tnum       = omp_get_thread_num();
  const uint64_t nthreads   = omp_get_num_threads();
  *tfirstCell = pg->first_ + MyPartitioning::partitionStart(  tnum, nCells, nthreads );
  const uint64_t tnCells    = MyPartitioning::partitionLength( tnum, nCells, nthreads );
  *tendCell   = *tfirstCell + tnCells;

  // find first particle position for this thread
  uint64_t prt = 0;
  for (uint64_t i = pg->first_; i < *tfirstCell; ++i)
    prt += pg->count_[i];

  return prt;

}

void InitialConditions::resize_particle_storage(
    Particles::ParticleCollection& prts,
    const uint64_t n_to_add) const
{
#pragma omp single
    {
      //      std::cout << "total: " << total << std::endl;

      typedef Particles::ParticleCollection::ResizeMode mode;
      const auto& flag = Particles::ParticleCollection::resizeflag;

      prts.resize(prts.size() + n_to_add,
                  flag(mode::ALL) | flag(mode::KEEP_CURRENT_DATA) );
    }
}






InitialConditions::InitialConditions(
    const double FN,
    const Gas::GasModels::GasModel* gasModel,
    const Settings::Dictionary& dict,
    const std::string& inputDirectory,
    const MPI_Comm& comm)
{

  if (dict.get<std::string>("type") == "vacuum")
    return;

  if (dict.hasMember("data file"))
    {

      auto fData = Tecplot::readTecplot1DData(
          MyString::catDirs(inputDirectory, dict.get<std::string>("data file") ),
          {},
          {},
          true,
          comm );


      Gas::makeMDF(mdf_,fData.get(),gasModel,FN,dict);

      // locations

      if (fData->hasConnectivity())
        {
          const uint64_t nVpE = fData->nVertsPerElement();

          auto& conn = fData->getConnectivity();
          const uint64_t nE = conn.size() / nVpE;

          MYASSERT(mdf_.size() == nE,"connectivity size, element size mismatch");

          auto X = fData->getVarData("X");
          auto Y = fData->getVarData("Y");
          auto Z = fData->getVarData("Z");

          int32_t* cptr = &conn[0];

          pos_store_t element_vertices;
          element_vertices.resize(nVpE,Eigen::NoChange);

          x_.resize(nE,Eigen::NoChange);

          for (uint64_t i = 0; i < nE; ++i)
            {

              for (uint64_t j = 0; j < nVpE; ++j)
                {
                  element_vertices.row(i) = Eigen::Vector3d( X[*cptr], Y[*cptr], Z[*cptr] );
                  ++cptr;
                }

              x_.row(i) = element_vertices.colwise().mean();

            }

        } // fData has connectivity
      else
        {
          // fData does not have connectivity

          const uint64_t nE = mdf_.size();

          MYASSERT(fData->getNData("X") == nE && fData->getNData("Y") == nE && fData->getNData("Z") == nE,
                   "mismatch between pdf and location size");

          auto X = fData->getVarData("X");
          auto Y = fData->getVarData("Y");
          auto Z = fData->getVarData("Z");

          x_.resize(nE,Eigen::NoChange);

          for (uint64_t i = 0; i < nE; ++i)
            x_.row(i) = Eigen::Vector3d(X[i],Y[i],Z[i]);

        }


      // finished fetching locations, delete field data and construct tree
      fData = nullptr;

      tree_ = std::unique_ptr<kd_tree_t>( new kd_tree_t(ndims /*dim*/, x_, 10 /* max leaf */ ) );

    } // dict has data file
  else
    {
      // dict does not have data file -> constant value for entire field
      mdf_.push_back( std::move(std::make_shared<Gas::MDF>( gasModel,FN,dict )));


    }

}
