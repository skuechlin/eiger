/* © 2014-2019, ETH Zurich, Institute of Fluid Dynamics, Stephan Küchlin */ 

/*
 * InitialConditionsSampling.hpp
 *
 *  Created on: Jun 14, 2018
 *      Author: kustepha
 */

#ifndef SRC_INITIALCONDITIONS_INITIALCONDITIONSSAMPLING_HPP_
#define SRC_INITIALCONDITIONS_INITIALCONDITIONSSAMPLING_HPP_

#include <Gas/PDF/MDF.h>
#include "Grid/RegularGrid.h"
#include "Particles/ParticleCollection.h"
#include "Primitives/MyRandom.h"


template<typename WFFunT, typename OQFunT>
void InitialConditions::sample(
    Particles::ParticleCollection& prts,
    Grids::RegularGrid *const pg,
    const double dt_ref,
    WFFunT&& weightFacFun,
    OQFunT&& isOutsideQ,
    MyRandom::Rng& rndGen)
{

  if (mdf_.size() == 0)
    return;

  uint64_t total = 0; // shared total


  const uint64_t nCells = pg->nNonGhost();

  std::vector<Gas::MDF*> mdf( nCells );
  make_mdf_vec(mdf,pg);


  // first, compute realizations

  double carry = 0.;

  double Tmean = 0.;

#pragma omp parallel
  {

    MyRandom::Rng rng(rndGen); // thread local rng
//
//    MyRandom::URng urng(&rng);  // thread local urng

//    typedef std::poisson_distribution<uint64_t> DistT;
//    DistT d; // thread local distribution

#pragma omp for schedule(runtime) reduction(+:total,carry)
    for (uint64_t i = pg->first_; i < pg->last_; ++i)
      {
        const double wf = weightFacFun(pg->cellLevel(i));
        const double mean = pg->cellVolume(i)*mdf[i - pg->first_]->N()/wf + carry;
//        d.param( DistT::param_type( mean ) ); // set mean
//        const uint64_t N = d(urng); // sample
        const uint64_t N = static_cast<uint64_t>(mean); // truncates
        carry = mean - static_cast<double>(N);
        pg->count_[i] = N; // save
        total += N; // tally
      }

#pragma omp single
    {
      uint64_t i = pg->first_;
      while (carry >= 1. && i < pg->last_)
        {
          if (pg->cellVolume(i)*mdf[i - pg->first_]->N() > 0.) {
            pg->count_[i]++;
            ++total;
            carry -= 1.;
            ++i;
          }
        }
    }


    // allocate particle storage

    resize_particle_storage(prts,total);

    uint64_t tfirstCell;
    uint64_t tendCell;
    uint64_t prt = prts.size() - total + partition_sampling(pg,&tfirstCell,&tendCell);

    uint64_t n_sampled = 0;

    // sample particles from respective pdfs
    for (uint64_t i = tfirstCell; i < tendCell; ++i)
      {
        auto cellpdf = mdf[i-pg->first_];

        const double wf = weightFacFun(pg->cellLevel(i));

        double rho = 0.;
        v4df ri  = zero<v4df>();
        v4df ci  = zero<v4df>();
        v4df c0i = zero<v4df>();
        v4df c1i = zero<v4df>();
        v4df c2i = zero<v4df>();


        for (uint64_t j = 0; j < pg->count_[i]; ++j,++prt,++n_sampled)
          {
            Particles::Particle* const p = prts.getParticle(prt);

            const auto pos = pg->sample(i,rng);

            if ( __builtin_expect(isOutsideQ(pos),false) )
              {
                Particles::mark_remove(p);
              }
            else
              {

                const v4df C = from_eigen(cellpdf->sampleVelocity(rng));

                const double w = cellpdf->w()*wf;
                const v4df wC = w*C;

                rho += w;
                ri += w*pos;
                ci += wC;
                c0i += C[0]*wC;
                c1i += C[1]*wC;
                c2i += C[2]*wC;

                p->R = pos;
                Particles::set_velocity(p,C);
                p->Omega = from_eigen(cellpdf->sampleRotation(rng));
                p->Xi = from_eigen(cellpdf->sampleVibration(rng));
                p->dt =  dt_ref * wf;
                p->w = cellpdf->w()*wf;
                p->m = cellpdf->m();
              }
          }

        const double vol = pg->cellVolume(i);

        rho /= vol;
        ri /= vol;
        ci /= vol;
        c0i /= vol;
        c1i /= vol;
        c2i /= vol;

        const v4df v = ci/rho;
        const double p = (1./3.)*(c0i[0] - v[0]*v[0]*rho + c1i[1] - v[1]*v[1]*rho + c2i[2] - v[2]*v[2]*rho);
        const double theta = p/rho;
        const double T = theta /  ( Constants::kB/cellpdf->m() );

//#pragma omp critical (COUT)
//        {
//          std::cout <<
//              "cell " << i << ":\n" <<
//              "      r: " << ri / rho << "\n" <<
//              "    rho: " << rho << "\n" <<
//              "      n: " << rho / cellpdf->m() << "\n" <<
//              "      v: " << v << "\n" <<
//              "      p: " << p << "\n" <<
//              "  theta: " << theta << "\n" <<
//              "      T: " << T << "\n" <<
//              "     dt: " << dt_ref * wf << "\n" <<
//              std::endl;
//        }

#pragma omp atomic
        Tmean += T;
      }

    // cross-check
#pragma omp atomic
    total -= n_sampled;

  } // parallel region

  Tmean /= nCells;

  std::cout << "mean temperature: " << Tmean << std::endl;

  MYASSERT(total == 0, std::to_string(total) + " unsampled particles remaining after initialization");

} // sample


#endif /* SRC_INITIALCONDITIONS_INITIALCONDITIONSSAMPLING_HPP_ */
