# Eiger

A parallel particle code for stochastic simulation of rarefied gas flows.  
The defining characteristic is the ordering of particle and mesh data on a space-filling curve.  
This allows for efficient hybrid parallelization (shared + distributed memory), load-balancing, communication, cache-coherence and automatic mesh refinement.

## Getting Started

This section describes the neccessary steps to obtain a runnable version of Eiger.  

### Obtaining the source

Simply clone the repository to a local directory using `git`.  

    mkdir git && cd git
    git init
    git clone https://username@bitbucket.org/skuechlin/eiger.git
    
### Installation

See [INSTALL.md](./INSTALL.md) for instructions on obtaining and installing prerequisits and for building Eiger.


## Running simulations

Eiger is executed via `mpirun`. It is important to ensure that mpi uses the ucx transport (see [INSTALL.md](./INSTALL.md) for how to build openmpi with ucx).
The following invokes a parallel Eiger run with `num_procs` mpi ranks (processes), each using `num_cores_pre_proc` cores: 

    mpirun --n num_procs --map-by node:PE=num_cores_per_proc --mca btl ^vader,openib,tcp --mca pml ucx --bind-to core EIGER args

where the arguments `args` to the executable are:

 Key | Value           | Optional | Description 
-----|-----------------|:--------:|------------
 `-s`| `settings_file` |          |`settings_file` is the settings file that defines the run.
 `-i`| `data_dir`      |          |`data_dir` is an absolute path where any geometry files referenced in the settings file are located.
 `-o`| `out_dir`       |          |`out_dir` is an absolute path do a writable directory where all simulation output will be written.
 `-n`| `N`             | *        | may be used to preallocate memory for `N` particle *per thread*.
 `-l`|                 | *        | will print all thirdparty license information and exit.
 

Example settings files are provided in the [eiger-cases](https://skuechlin@bitbucket.org/skuechlin/eiger-cases) repository.

## Postprocessing

The ouputfiles are written in the [tecplot binary format](http://download.tecplot.com/360/current/360_data_format_guide.pdf).
A simple Matlab script (useful for debugging and 1D/2D post-processing) is provided in the [scripts/Matlab](scripts/Matlab) directory.



## Authors

Eiger was developed and implemented by Stephan Küchlin at the [Institute of Fluid Dynamics](http://www.ifd.mavt.ethz.ch), ETH Zurich.


## License

This project is licensed under the [MIT license](https://opensource.org/licenses/MIT) (see [LICENSE](./LICENSE)).
Further, any research making use of this software should appropriately cite the references given below, in keeping with normal academic practice.

## References

* S. Küchlin. "Stochastic Computation of Rarefied Gas Flows Using the Fokker-Planck-DSMC Method: Theory, Algorithms, and Parallel Implementation".  
  PhD thesis ETH Zurich (2018). doi: [10.3929/ethz-b-000330322](https://doi.org/10.3929/ethz-b-000330322).
  
* S. Küchlin and P. Jenny. "Kernel Estimation of Mixed Spatial Derivatives of Statistics of Scattered Data".  
  Manuscript submitted for publication. 2018. doi: [10.13140/RG.2.2.27883.03366](https://doi.org/10.13140/RG.2.2.27883.03366).

* S. Küchlin and P. Jenny. "Automatic mesh refinement and parallel load balancing for Fokker-Planck-DSMC algorithm".  
  In: Journal of Computational Physics 363 (June 2018), pp. 140--157. doi: [10.1016/ j.jcp.2018.02.049](https://doi.org/10.1016/j.jcp.2018.02.049).

* S. Küchlin and P. Jenny. "Parallel Fokker-Planck-DSMC algorithm for rarefied gas flow simulation in complex domains at all Knudsen numbers".  
  In: Journal of Computational Physics 328 (Jan. 2017), pp. 258--277. doi: [10.1016/j.jcp.2016.10.018](https://doi.org/10.1016/j.jcp.2016.10.018).
  
* S. Küchlin and P. Jenny. "Parallel Fokker-Planck-DSMC algorithm for rarefied gas flow simulation in complex domains at all Knudsen numbers".  
  In: Journal of Computational Physics 328 (Jan. 2017), pp. 258--277. doi: [10.1016/j.jcp.2016.10.018](https://doi.org/10.1016/j.jcp.2016.10.018).
  
* M.H. Gorji, M. Torrilhon, and P. Jenny. "Fokker-Planck Model for Computational Studies of Monatomic Rarefied Gas Flows".  
  In: Journal of Fluid Mechanics 680 (Aug. 2011), pp. 574--601. doi: [10.1017/jfm.2011.188](https://doi.org/10.1017/jfm.2011.188).
  
* P. Jenny, M. Torrilhon, and S. Heinz. "A Solution Algorithm for the Fluid Dynamic Equations Based on a Stochastic Model for Molecular Motion".  
  In: Journal of Computational Physics 229 (Feb. 2010), pp. 1077--1098. doi: [10.1016/j.jcp.2009.10.008](https://doi.org/10.1016/j.jcp.2009.10.008).


## Acknowledgments

* Prof. Patrick Jenny, ETH
* Dr. Hossein Gorji, EPFL
