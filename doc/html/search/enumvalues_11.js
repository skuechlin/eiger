var searchData=
[
  ['radius',['RADIUS',['../classShapes_1_1Disk.html#a7054e0ddcb758fbc8bd1e7bc12a63054a5c6d0ec312d680ed011756648c9e96ea',1,'Shapes::Disk']]],
  ['read',['READ',['../namespaceTecplot.html#a18cbb1b42130759b5f0d46c8a03a185ca3466fab4975481651940ed328aa990e4',1,'Tecplot']]],
  ['refine',['REFINE',['../classGrids_1_1GridTransform.html#a05992565c5ac21e1f680b4005f717ddaaef223a001a0bcf60de9f7757c7bc7c1b',1,'Grids::GridTransform']]],
  ['refine_5fby_5fnum_5fprts',['REFINE_BY_NUM_PRTS',['../classGridAdaption.html#ae1bd8bfe2722c34889ac37516114d9f2ab2ec54b02c342171dd5e63dbb71e693c',1,'GridAdaption']]],
  ['refine_5finside',['REFINE_INSIDE',['../classGridAdaption.html#ae1bd8bfe2722c34889ac37516114d9f2ad0e3bec6de8a0740910cdd2fac96bdde',1,'GridAdaption']]],
  ['refine_5fintersected_5fcells',['REFINE_INTERSECTED_CELLS',['../classGridAdaption.html#ae1bd8bfe2722c34889ac37516114d9f2a76bf063e11f21885241f48da05652c82',1,'GridAdaption']]],
  ['refine_5fto_5fgradient_5flength',['REFINE_TO_GRADIENT_LENGTH',['../classGridAdaption.html#ae1bd8bfe2722c34889ac37516114d9f2af3738a0888ce719c71c41bd5ecda7e48',1,'GridAdaption']]],
  ['refine_5fto_5fmfp',['REFINE_TO_MFP',['../classGridAdaption.html#ae1bd8bfe2722c34889ac37516114d9f2a7cc613447fee8e7138445e09a34f96f9',1,'GridAdaption']]],
  ['refinement',['REFINEMENT',['../classGridAdaption.html#ae1bd8bfe2722c34889ac37516114d9f2ae78e7733a28124905f26e8cd48b2f640',1,'GridAdaption']]],
  ['reflected_5fflag',['REFLECTED_FLAG',['../classBoundary_1_1Strategies_1_1BoundaryStrategy.html#a501f5f051de92f489b95c34b4fe8a662ab1d30236495b74d0cac479a80db30281',1,'Boundary::Strategies::BoundaryStrategy']]],
  ['remove',['REMOVE',['../classGrids_1_1GridTransform.html#a05992565c5ac21e1f680b4005f717ddaa98e3e1e8782b5f60b7cde1c0ff05b528',1,'Grids::GridTransform']]],
  ['remove_5fempty_5fcells',['REMOVE_EMPTY_CELLS',['../classGridAdaption.html#ae1bd8bfe2722c34889ac37516114d9f2aa7e97701c7ff60fdbf9be0a678e462f5',1,'GridAdaption']]],
  ['right',['RIGHT',['../classSimulationBox.html#a577af7ec7f6a24cd16d58d0029ad6914a6a61dac1972b2cc6bf2d1f0676849cc1',1,'SimulationBox']]]
];
