var searchData=
[
  ['q',['q',['../structCellData.html#a98ec24bd5b9f376f936791f97ae4192d',1,'CellData::q() const'],['../structCellData.html#a47bfe60b763e115c0561081789b62556',1,'CellData::q() const']]],
  ['q_5frot',['q_rot',['../structCellData.html#a5b57333207ea1aec1557442585983a2c',1,'CellData']]],
  ['q_5fvib',['q_vib',['../structCellData.html#a4bf98586a1c7b0f131cb1f1ec97f66b7',1,'CellData']]],
  ['quadrilateral',['Quadrilateral',['../classShapes_1_1Quadrilateral.html#ad9be4c9e6bcb96e935c131a8eabe08c5',1,'Shapes::Quadrilateral::Quadrilateral(const Eigen::Matrix&lt; double, 4, 4 &gt; &amp;vrts, const uint32_t prim_id=0)'],['../classShapes_1_1Quadrilateral.html#a0b26e965a780c4a49d3abd85b9e510d3',1,'Shapes::Quadrilateral::Quadrilateral(const Eigen::Vector4d &amp;A, const Eigen::Vector4d &amp;B, const Eigen::Vector4d &amp;C, const Eigen::Vector4d &amp;D, const uint32_t prim_id=0)']]]
];
