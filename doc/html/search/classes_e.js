var searchData=
[
  ['ray',['Ray',['../structRayTracing_1_1Ray.html',1,'RayTracing']]],
  ['rebind',['rebind',['../structMyMemory_1_1aligned__allocator_1_1rebind.html',1,'MyMemory::aligned_allocator']]],
  ['receviecommspec',['RecevieCommSpec',['../structMyMPI_1_1RecevieCommSpec.html',1,'MyMPI']]],
  ['refcondition',['RefCondition',['../structGas_1_1EqCondition_1_1RefCondition.html',1,'Gas::EqCondition']]],
  ['reg_5ft',['reg_t',['../structreg__t.html',1,'']]],
  ['reg_5ft_3c_2016_20_3e',['reg_t&lt; 16 &gt;',['../structreg__t_3_0116_01_4.html',1,'']]],
  ['reg_5ft_3c_2032_20_3e',['reg_t&lt; 32 &gt;',['../structreg__t.html',1,'']]],
  ['regulargrid',['RegularGrid',['../classGrids_1_1RegularGrid.html',1,'Grids']]],
  ['reweight',['ReWeight',['../classBoundary_1_1Strategies_1_1ReWeight.html',1,'Boundary::Strategies']]],
  ['rng',['Rng',['../classMyRandom_1_1Rng.html',1,'MyRandom']]]
];
