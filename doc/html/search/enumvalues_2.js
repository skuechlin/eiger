var searchData=
[
  ['cartesian',['CARTESIAN',['../namespaceGrids_1_1CoordinateSystems.html#af643eb7646b767bd25e5ee29dfdfc98da25907539cc352910c3fd8e4081c8f74e',1,'Grids::CoordinateSystems']]],
  ['cellcentered',['CellCentered',['../namespaceTecplot.html#a7eed19073cd10cb82cb2701e27549681a453721c84645715ad5cc9772a3ab4c02',1,'Tecplot']]],
  ['cellface_5fcollection',['CELLFACE_COLLECTION',['../namespaceShapes.html#ae4a8d727d1ac9a6a05a22340ff1125bfa1ac36134212bc3cae40d8e07cdb77ec1',1,'Shapes']]],
  ['coarsen',['COARSEN',['../classGrids_1_1GridTransform.html#a05992565c5ac21e1f680b4005f717ddaa37a49bf92cdfbd6806ea56b56a0ef6d8',1,'Grids::GridTransform']]],
  ['conditional',['CONDITIONAL',['../classVelocityUpdate_1_1FokkerPlanck.html#a442fd6ee0e25107f0c86c168bac7aab1a6685384d880fca387e09391a6e53e3a8',1,'VelocityUpdate::FokkerPlanck']]],
  ['correct_5fcell_5fvolumes',['CORRECT_CELL_VOLUMES',['../classGridAdaption.html#ae1bd8bfe2722c34889ac37516114d9f2a274183ee0022dc5d31ce4fa0c875687b',1,'GridAdaption']]],
  ['cpu',['CPU',['../classGrids_1_1GridManager.html#ac4d26ffe7a74ed6bd360f415178add2fa2b55387dd066c5bac646ac61543d152d',1,'Grids::GridManager']]],
  ['cubic',['CUBIC',['../classVelocityUpdate_1_1FokkerPlanck.html#a8f59e443ae4f5faee3ff29cc4f620d7daccd681e34e5e40fbce74618c3ccffcff',1,'VelocityUpdate::FokkerPlanck']]],
  ['cubic_5flegacy',['CUBIC_LEGACY',['../classVelocityUpdate_1_1FokkerPlanck.html#a8f59e443ae4f5faee3ff29cc4f620d7da6ba87d01d3d8229e68d432af206dfae0',1,'VelocityUpdate::FokkerPlanck']]],
  ['current',['CURRENT',['../classParticles_1_1ParticleCollection.html#a4b6805ca552776bae7815dd27ff00ed0aa2770969c827f0f2910f6179418462df',1,'Particles::ParticleCollection']]],
  ['current_5fdata',['CURRENT_DATA',['../classParticles_1_1ParticleCollection.html#a4b6805ca552776bae7815dd27ff00ed0acacefde0c3b6f913fb6664eddced7938',1,'Particles::ParticleCollection']]],
  ['current_5fsort',['CURRENT_SORT',['../classParticles_1_1ParticleCollection.html#a4b6805ca552776bae7815dd27ff00ed0a2ba4241e99eeb2e0dfcc4534b395e765',1,'Particles::ParticleCollection']]],
  ['cylinder',['CYLINDER',['../namespaceShapes.html#ae4a8d727d1ac9a6a05a22340ff1125bfa00a8ca35a10955524c9847a40b61b62d',1,'Shapes']]]
];
