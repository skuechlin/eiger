var searchData=
[
  ['sequential',['SEQUENTIAL',['../namespaceGrids_1_1Orderings.html#a651aa4bd117a037fdbb48dc470828c04abca92560e026bf2b7df70d9a34111183',1,'Grids::Orderings']]],
  ['shared',['SHARED',['../namespaceTecplot.html#aa7dfe20174a4dc2c2831f3031f9b9393a049518eb4dc1859c7cebbe15876cfd63',1,'Tecplot']]],
  ['solutionfile',['SolutionFile',['../namespaceTecplot.html#a83899308c4ec1cbe38ad3df71305d6e1aebbb2c913179198a8d5df78f5fcd22a5',1,'Tecplot']]],
  ['sparse',['SPARSE',['../structMyMPI_1_1SendCommSpec.html#ad5d871a20a0e4737e0d695b819a3d648ad6a9c73e84cbf336c46eaa461db54b6f',1,'MyMPI::SendCommSpec']]],
  ['specular',['SPECULAR',['../namespaceBoundary_1_1Strategies.html#a806292ec5c90c775395ba504becc9981ad9ff93f54cfd0c2b2e3827c3ac3e598e',1,'Boundary::Strategies']]],
  ['sphere',['SPHERE',['../namespaceShapes.html#ae4a8d727d1ac9a6a05a22340ff1125bfa16653393830b7bb4dd553e6fb7f9aeed',1,'Shapes']]],
  ['sphere_5fcollection',['SPHERE_COLLECTION',['../namespaceShapes.html#ae4a8d727d1ac9a6a05a22340ff1125bfa6af6d54e977ae50cf659ac1a7466f7df',1,'Shapes']]],
  ['standard',['STANDARD',['../classVelocityUpdate_1_1FokkerPlanck.html#a55fbf905adf2bfb22dfbc9ecdfa9f931a94e94133f4bdc1794c6b647b8ea134d0',1,'VelocityUpdate::FokkerPlanck']]],
  ['static_5frefinement',['STATIC_REFINEMENT',['../classGridAdaption.html#ae1bd8bfe2722c34889ac37516114d9f2a8cb11a09fcd4d6dd626378d388789196',1,'GridAdaption']]],
  ['stl',['STL',['../namespaceSettings.html#ab2281e2ea9385ed08060f38458eba1b2afe7515960c762c75935054f9e1ff8ae8',1,'Settings']]],
  ['sw',['SW',['../namespaceMoments_1_1Averaging.html#a8c85e204a4732629762a493629ae920baddd06e491df0f17529cd6402ed353fa7',1,'Moments::Averaging']]]
];
