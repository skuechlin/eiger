var searchData=
[
  ['j',['j',['../structTecplot_1_1Headers_1_1TecZoneHeader.html#a52f9367c1942ec6083388d29bf5da295',1,'Tecplot::Headers::TecZoneHeader']]],
  ['j_5f',['J_',['../classViewFactor_1_1ViewFactors.html#a1afa1132e5900e42c04551a1d20ae579',1,'ViewFactor::ViewFactors']]],
  ['joint',['JOINT',['../classVelocityUpdate_1_1FokkerPlanck.html#a442fd6ee0e25107f0c86c168bac7aab1a5429249d7b424ed4ff7494e8ac70426d',1,'VelocityUpdate::FokkerPlanck']]],
  ['jump',['jump',['../classMyRandom_1_1Rng.html#aec75f368e0def88e0359aa51bb2aa417',1,'MyRandom::Rng::jump()'],['../classMyRandom_1_1Rng.html#ad176c9c15ec926f62e538a534bfeefae',1,'MyRandom::Rng::jump(uint64_t num_jumps)']]]
];
