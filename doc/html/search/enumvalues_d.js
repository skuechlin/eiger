var searchData=
[
  ['n_5ffileformat_5ftypes',['N_FILEFORMAT_TYPES',['../namespaceSettings.html#ab2281e2ea9385ed08060f38458eba1b2a4a0fab282e652bd0879358c44c2a11f8',1,'Settings']]],
  ['n_5fshape_5ftypes',['N_SHAPE_TYPES',['../namespaceShapes.html#ae4a8d727d1ac9a6a05a22340ff1125bfaebf67578fe3a0ca1c2f44505fec51e70',1,'Shapes']]],
  ['naive',['NAIVE',['../structMoments_1_1VelocityMoments.html#a49602ab845cda3f9983a26a100f9733eaf527aeb16a21a4fc5960cf2ac2d3038b',1,'Moments::VelocityMoments']]],
  ['nastran',['NASTRAN',['../namespaceSettings.html#ab2281e2ea9385ed08060f38458eba1b2a5aa837d449c530a890210fee2835f353',1,'Settings']]],
  ['no_5fflag',['NO_FLAG',['../classBoundary_1_1Strategies_1_1BoundaryStrategy.html#a501f5f051de92f489b95c34b4fe8a662a78244f04561bc6efd64368d4ab800914',1,'Boundary::Strategies::BoundaryStrategy']]],
  ['nodecentered',['NodeCentered',['../namespaceTecplot.html#a7eed19073cd10cb82cb2701e27549681a3f2d66d2b67bdaf3866380f57d6c3fde',1,'Tecplot']]],
  ['none',['NONE',['../classGridAdaption.html#ae1bd8bfe2722c34889ac37516114d9f2afb36d86145d402a46e557495e2506d7f',1,'GridAdaption::NONE()'],['../namespaceMoments_1_1Averaging.html#a8c85e204a4732629762a493629ae920ba99b363a9003789615c109bd9f22c19f1',1,'Moments::Averaging::NONE()'],['../namespaceVelocityUpdate.html#a498242c3f6bd06e9f8e71103d509195eac845d3e1f54e26532780a399eb488ceb',1,'VelocityUpdate::NONE()']]],
  ['nsampletypes',['NSAMPLETYPES',['../structMoments_1_1VelocityMoments.html#a49602ab845cda3f9983a26a100f9733ea0d2db8a6ab77279546409a7576f989a5',1,'Moments::VelocityMoments']]]
];
