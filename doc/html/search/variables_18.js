var searchData=
[
  ['x',['X',['../structVelocityUpdate_1_1FokkerPlanck_1_1Model.html#af88b1e1a299e5e42271e326cc0af17ff',1,'VelocityUpdate::FokkerPlanck::Model::X()'],['../structVelocityUpdate_1_1FokkerPlanck_1_1GeneralizedModel.html#a7d302955017c7967ec38a7b71cf4b33d',1,'VelocityUpdate::FokkerPlanck::GeneralizedModel::X()']]],
  ['x_5f',['x_',['../classInitialConditions.html#ac56c7ab3de213c3996cc55dd557260ce',1,'InitialConditions::x_()'],['../classShapes_1_1Cylinder.html#aa4c9c684881cea3b8bfd3b33931391ec',1,'Shapes::Cylinder::X_()'],['../classShapes_1_1Disk.html#a0aa16f0eaa8665df42945e54dad78cee',1,'Shapes::Disk::X_()'],['../classShapes_1_1Sphere.html#a067583de56f6f1bbdc203a52bb714ce7',1,'Shapes::Sphere::X_()']]],
  ['xi',['Xi',['../structParticles_1_1Particle.html#adb926df9be791450d0e3dbf9282e8926',1,'Particles::Particle']]],
  ['xstd_5f',['xstd_',['../classGas_1_1Maxwellian.html#ab0289b49dee2011c5efb4dc8fb5645ae',1,'Gas::Maxwellian']]]
];
