function [DATA, CONNECTIVITY, FNCONN, HEADER] = readTecplot(varargin) % .plt files

DATA = [];
CONNECTIVITY = [];
FNCONN = [];


if nargin == 0    
    
    filepath = '~';
    
    [fName,fPath] = uigetfile('*.plt','Select .plt file',filepath);
    
    file = fullfile(fPath,fName);
    
else
    file = varargin{1};    
end

only_header = 0;
if (nargin > 1)
    if strcmp(varargin{2},'header')
        only_header = 1;
    end
end




fid = fopen(file, 'r'); 

% I. HEADER SECTION



% i. Magic number, Version number

MAGIC = fread(fid,8,'char');
HEADER.('MAGIC')       = MAGIC';
HEADER.('MAGIC_char')  = char(MAGIC'); 


% ii. Integer value of 1.

HEADER.('IVAL1')       = fread(fid,1,'int32');


% iii. Title and variable names.

FILETYPE = fread(fid,1,'int32');
HEADER.('FILETYPE')  = FILETYPE;

TITLE       = fread(fid,1,'int32');
while (TITLE(end) ~= 0)
    TITLE   = [TITLE,fread(fid,1,'int32')]; %#ok<AGROW>
end
TITLE_char = char(TITLE);
HEADER.('TITLE') = TITLE;
HEADER.('TITLE_char') = TITLE_char;

NVARS       = fread(fid,1,'int32');
HEADER.('NVARS') = NVARS;
VARNAMES    = [];
for i = 0:NVARS-1
    VARNAMES    = [VARNAMES,fread(fid,1,'int32')]; %#ok<AGROW>
    while (VARNAMES(end) ~= 0)
        VARNAMES   = [VARNAMES,fread(fid,1,'int32')]; %#ok<AGROW>
    end
end
clear i;
HEADER.('VARNAMES') = VARNAMES;
VARNAMES_char = char(VARNAMES);
HEADER.('VARNAMES_char') = VARNAMES_char;
VARNAMES_C = strsplit(VARNAMES_char,char(0));
HEADER.('VARNAMES_C') = VARNAMES_C(1:end-1);
% iv. Zones

ZONEMARKER      = fread(fid,1,'float');
HEADER.('ZONEMARKER') = ZONEMARKER;
ZONENAME        = fread(fid,1,'int32');
while (ZONENAME(end) ~= 0)
    ZONENAME    = [ZONENAME,fread(fid,1,'int32')]; %#ok<AGROW>
end
HEADER.('ZONENAME') = ZONENAME;
ZONENAME_char   = char(ZONENAME);
HEADER.('ZONENAME_char') = ZONENAME_char;
PARENTZONE      = fread(fid,1,'int32');
HEADER.('PARENTZONE') = PARENTZONE;
STRANDID        = fread(fid,1,'int32');
HEADER.('STRANDID') = STRANDID;
SOLUTIONTIME    = fread(fid,1,'float64');
HEADER.('SOLUTIONTIME') = SOLUTIONTIME;
UNUSED          = fread(fid,1,'int32');
HEADER.('UNUSED') = UNUSED;
ZONETYPE        = fread(fid,1,'int32');
HEADER.('ZONETYPE') = ZONETYPE;
%DATAPACKING     = fread(fid,1,'int32'); % auskommentiert lassen
VARLOCISSPEC    = fread(fid,1,'int32');
HEADER.('VARLOCISSPEC') = VARLOCISSPEC;
VARLOC          = zeros(NVARS,1);
if (VARLOCISSPEC == 1)
    VARLOC      = fread(fid,NVARS,'int32');
end
HEADER.('VARLOC') = VARLOC;
HAVERAWLOCALFN  = fread(fid,1,'int32');
HEADER.('HAVERAWLOCALFN') = HAVERAWLOCALFN;
NMISCFN         = fread(fid,1,'int32');
HEADER.('NMISCFN') = NMISCFN;
if (NMISCFN ~= 0)
    FNMODE = fread(fid,1,'int32');
    HEADER.('FNMODE') = FNMODE;
    if (ZONETYPE ~= 0) %FE
        MISCFNCOMPLETE = fread(fid,1,'int32');
        HEADER.('MISCFNCOMPLETE') = MISCFNCOMPLETE;
    end
end
if (ZONETYPE == 0) %ordered
    IJKMAX          = fread(fid,3,'int32');
    HEADER.('IJKMAX') = IJKMAX;
else
    NUMPTS      = fread(fid,1,'int32');
    HEADER.('NUMPTS') = NUMPTS;
    NUMELEMENTS = fread(fid,1,'int32');
    HEADER.('NUMELEMENTS') = NUMELEMENTS;
    ICellDim  = fread(fid,1,'int32');
    HEADER.('ICellDim') = ICellDim;
    JCellDim  = fread(fid,1,'int32');
    HEADER.('JCellDim') = JCellDim;
    KCellDim  = fread(fid,1,'int32');
    HEADER.('KCellDim') = KCellDim;

end
AUXVALUES       = fread(fid,1,'int32');
HEADER.('AUXVALUES') = AUXVALUES;

EOHMARKER       = fread(fid,1,'float');
HEADER.('EOHMARKER') = EOHMARKER;


% II. DATA SECTION


% i. For both ordered and fe zones:

ZONEMARKER      = fread(fid,1,'float');
HEADER.('ZONEMARKER') = ZONEMARKER;
VARDATAFORMAT   = fread(fid,NVARS,'int32');
HEADER.('VARDATAFORMAT') = VARDATAFORMAT;
HASPASSIVEVARS  = fread(fid,1,'int32');
HEADER.('HASPASSIVEVARS') = HASPASSIVEVARS;
HASVARSHARING   = fread(fid,1,'int32');
HEADER.('HASVARSHARING') = HASVARSHARING;
CONNSHARZONEN   = fread(fid,1,'int32');
HEADER.('CONNSHARZONEN') = CONNSHARZONEN;
MINMAX          = fread(fid,2*NVARS,'float64');
HEADER.('MINMAX') = MINMAX;


if only_header
    fclose(fid);
    return
end


DATA = struct;

if (ZONETYPE == 0)
    
        
    for ivar = 1:NVARS

        
        DATA.(VARNAMES_C{ivar}) = zeros(IJKMAX(1),IJKMAX(2),IJKMAX(3));
        
        KMAX = IJKMAX(3);
        
        if (KMAX > 1)
            KMAX = KMAX - VARLOC(ivar);
        end
        
        NDATA = KMAX*IJKMAX(2)*IJKMAX(1);
        
        DATA.(VARNAMES_C{ivar})(1:NDATA) = fread(fid,NDATA,'float64');



        
    end
    clear ivar KMAX NDATA;
    
else
    
    
    
    for ivar = 1:NVARS
        
        if (VARLOCISSPEC == 1)
            if (VARLOC(ivar) == 0)
                nvals = NUMPTS;
            else
                nvals = NUMELEMENTS;
            end
        else
            nvals = NUMPTS;
        end

        DATA.(VARNAMES_C{ivar}) = fread(fid,nvals,'float64');
        
    end
    clear ivar i
end

CONNECTIVITY = [];

% iii. specific to fe zones
% if ZoneType is NOT FEPOLYGON or FEPOLYHEDRON:
% if ?zone number to share connectivity lists with? == -1
% +-----------+
% | INT32*N
% |
% Zone Connectivity Data N=L*JMax
% +-----------+
% (see note 2 below ).

% disp( ftell(fid) );

if (ZONETYPE ~= 0 && FILETYPE ~= 2) %not a pure solution file 
    
    if (ZONETYPE == 1) %LINEAR
        L = 2;
    end
    
    if (ZONETYPE == 2) %FETRIANGLE
        L = 3;
    end
    
    if (ZONETYPE == 3) %FEQUAD
        L = 4;
    end
    
    if (ZONETYPE == 5) %FEBRICK
        L = 8;
    end
    
    
    CONNECTIVITY = fread(fid,L*NUMELEMENTS,'int32');
    
    CONNECTIVITY = CONNECTIVITY + 1;
    
    CONNECTIVITY = reshape(CONNECTIVITY,L,NUMELEMENTS);
    
%     for j = 1:NUMELEMENTS
%         for i = 1:L
%         CONNECTIVITY(i,j) = fread(fid,1,'int32') + 1;
%         end
%     end
%     clear i j
end

% disp( ftell(fid) );



if (NMISCFN > 0)
    
    if (FNMODE == 1) %local one-to-many
        
        FNCONN = zeros(NMISCFN*4,1); % maximum
        
        pos = 1;
        i = 0;
        while i < NMISCFN
            
            % cz fz oz nz
            FNCONN(pos:(pos+3)) = fread(fid,4,'int32');
            pos = pos + 3;
            nz = FNCONN(pos);
            pos = pos + 1;
            % cz1 ... czn
            FNCONN(pos:(pos+nz-1)) = fread(fid,nz,'int32');          
            pos = pos + nz;
            i = i + nz;
            
        end        
        
        FNCONN = FNCONN(1:pos-1);        
    end
    
end

fclose(fid);





end