%% hilbert lut test

clearvars; clc; close all;

curves = struct;

outdir = 'HilbertLUTs';

%% the curve definitions below were generated using the hilbex software by Herman Haverkort
% http://herman.haverkort.net/lib/exe/fetch.php?media=research:hilbex.zip

% Butz
curves(end).name = 'Butz';
curves(end).defstring = '< [ 2 3 1 } 1 [ 3 1 2 } 2 [ 3 1 2 } -1 [ -1 -2 3 } 3 { -1 -2 -3 ] 1 { -3 1 2 ] -2 { -3 1 2 ] -1 { 2 -3 1 ] >';
% % Alfa
% curves(end+1).name = 'Alfa';
% curves(end).defstring = '< [ 3 2 1 } 1 [ 3 1 2 } 2 { 3 1 -2 ] -1 [ -2 -1 3 } 3 { -2 -1 -3 ] 1 [ -3 1 -2 } -2 { -3 1 2 ] -1 { 2 -3 1 ] >';
% % Ca00chI
% curves(end+1).name = 'Ca00_chI';
% curves(end).defstring = '< [ 2 3 1 } 1 [ 3 1 2 } 2 [ 1 2 3 } -1 [ -3 -1 2 } 3 { 3 -1 2 ] 1 { 1 2 -3 ] -2 { -3 1 2 ] -1 { 2 -3 1 ] >';
% % Beta
% curves(end+1).name = 'Beta';
% curves(end).defstring = '< { -3 -1 -2 ] 1 { -3 -2 -1 ] 2 [ -3 2 -1 } -1 { 2 -3 1 ] 3 [ 2 3 1 } 1 { 3 2 -1 ] -2 [ 3 -2 -1 } -1 [ -2 -1 3 } >';
% % Ca00.c49
% curves(end+1).name = 'Ca00_c49';
% curves(end).defstring = '< [ 3 2 1 } 1 [ 1 3 2 } 2 [ 1 3 2 } -1 [ -1 -2 3 } 3 { -1 -2 -3 ] 1 { 1 -3 2 ] -2 { 1 -3 2 ] -1 { -3 2 1 ] >';
% % Ca00.c47
% curves(end+1).name = 'Ca00_c47';
% curves(end).defstring = '< [ 3 2 1 } 1 [ 1 3 2 } 2 [ 3 1 2 } -1 [ -1 -2 3 } 3 { -1 -2 -3 ] 1 { -3 1 2 ] -2 { 1 -3 2 ] -1 { -3 2 1 ] >';
% % Ca00.c44
% curves(end+1).name = 'Ca00_c44';
% curves(end).defstring = '< [ 3 2 1 } 1 [ 3 1 2 } 2 [ 1 3 2 } -1 [ -1 -2 3 } 3 { -1 -2 -3 ] 1 { 1 -3 2 ] -2 { -3 1 2 ] -1 { -3 2 1 ] >';
% % Ca00.c43
% curves(end+1).name = 'Ca00_c43';
% curves(end).defstring = '< [ 3 2 1 } 1 [ 1 3 2 } 2 [ 1 3 2 } -1 [ -2 -1 3 } 3 { -2 -1 -3 ] 1 { 1 -3 2 ] -2 { 1 -3 2 ] -1 { -3 2 1 ] >';
% % Ca00.c4b
% curves(end+1).name = 'Ca00_c4b';
% curves(end).defstring = '< [ 2 3 1 } 1 [ 3 1 2 } 2 [ 1 3 2 } -1 [ -2 -1 3 } 3 { -2 -1 -3 ] 1 { 1 -3 2 ] -2 { -3 1 2 ] -1 { 2 -3 1 ] >';
% % Ca00.c4d
% curves(end+1).name = 'Ca00_c4d';
% curves(end).defstring = '< [ 3 2 1 } 1 [ 3 1 2 } 2 [ 1 3 2 } -1 [ -2 -1 3 } 3 { -2 -1 -3 ] 1 { 1 -3 2 ] -2 { -3 1 2 ] -1 { -3 2 1 ] >';
% % Ca00.c4E
% curves(end+1).name = 'Ca00_c4E';
% curves(end).defstring = '< [ 2 3 1 } 1 [ 1 3 2 } 2 [ 1 3 2 } -1 [ -2 -1 3 } 3 { -2 -1 -3 ] 1 { 1 -3 2 ] -2 { 1 -3 2 ] -1 { 2 -3 1 ] >';
% % Ca00.c4P
% curves(end+1).name = 'Ca00_c4P';
% curves(end).defstring = '< [ 2 3 1 } 1 [ 1 3 2 } 2 [ 1 3 2 } -1 [ -1 -2 3 } 3 { -1 -2 -3 ] 1 { 1 -3 2 ] -2 { 1 -3 2 ] -1 { 2 -3 1 ] >';
% % Ca00.c4J
% curves(end+1).name = 'Ca00_c4J';
% curves(end).defstring = '< [ 3 2 1 } 1 [ 3 1 2 } 2 [ 3 1 2 } -1 [ -2 -1 3 } 3 { -2 -1 -3 ] 1 { -3 1 2 ] -2 { -3 1 2 ] -1 { -3 2 1 ] >';
% % Ca00.c4X
% curves(end+1).name = 'Ca00_c4X';
% curves(end).defstring = '< [ 3 2 1 } 1 [ 3 1 2 } 2 [ 3 1 2 } -1 [ -1 -2 3 } 3 { -1 -2 -3 ] 1 { -3 1 2 ] -2 { -3 1 2 ] -1 { -3 2 1 ] >';
% % Harmonious
% curves(end+1).name = 'Harmonious';
% curves(end).defstring = '< [ 3 2 1 } 1 [ 1 3 2 } 2 [ 3 1 2 } -1 [ -2 -1 3 } 3 { -2 -1 -3 ] 1 { -3 1 2 ] -2 { 1 -3 2 ] -1 { -3 2 1 ] >';
% % Ca00.cTI
% curves(end+1).name = 'Ca00_cTI';
% curves(end).defstring = '< [ 2 3 1 } 1 [ 3 1 2 } 2 [ 1 2 3 } -1 [ 2 -3 -1 } 3 { 2 3 -1 ] 1 { 1 2 -3 ] -2 { -3 1 2 ] -1 { 2 -3 1 ] >';
% % Ca00.cTb
% curves(end+1).name = 'Ca00_cTb';
% curves(end).defstring = '< [ 2 3 1 } 1 [ 3 1 2 } 2 [ 2 1 3 } -1 [ -3 2 -1 } 3 { 3 2 -1 ] 1 { 2 1 -3 ] -2 { -3 1 2 ] -1 { 2 -3 1 ] >';
% % Ca00.cTC
% curves(end+1).name = 'Ca00_cTC';
% curves(end).defstring = '< [ 2 3 1 } 1 [ 1 3 2 } 2 [ 1 2 3 } -1 [ -3 2 -1 } 3 { 3 2 -1 ] 1 { 1 2 -3 ] -2 { 1 -3 2 ] -1 { 2 -3 1 ] >';
% % Sasburg
% curves(end+1).name = 'Sasburg';
% curves(end).defstring = '< [ 3 2 1 } 1 [ 3 1 2 } 2 [ 2 1 3 } -1 [ 2 -3 -1 } 3 { 2 3 -1 ] 1 { 2 1 -3 ] -2 { -3 1 2 ] -1 { -3 2 1 ] >';
% % Ca00.ch7
% curves(end+1).name = 'Ca00_ch7';
% curves(end).defstring = '< [ 3 2 1 } 1 [ 1 3 2 } 2 [ 1 2 3 } -1 [ -3 -1 2 } 3 { 3 -1 2 ] 1 { 1 2 -3 ] -2 { 1 -3 2 ] -1 { -3 2 1 ] >';
% 

for curve = curves
    
    name = curve.name;
    defstring = curve.defstring;
    
    [ state2stateByOn, state2keyByOn, state2onByKey, startState, startPeriod ] = hilbertCurveDef2Transition( defstring );
    
    
    N = 4;
    x = 0:1:N-1;
    y = 0:1:N-1;
    z = 0:1:N-1;
    [X, Y, Z] = ndgrid(x,y,z);
    
    % generate morton codes
    m = arrayfun(@(x,y,z) interleave([x,y,z]), X(:),Y(:),Z(:),'UniformOutput',true);
    
    % [~,I] = sort(m);
    %
    % line(X(I),Y(I),Z(I));
    % xlabel('x');
    % ylabel('y');
    % zlabel('z');
    
    % generate hilbert codes
    h = zeros(numel(m),1,'uint64');
    
    % hh = 134231552;
    % state = startState;
    %     mmcheck = 0;
    %     for shift = 60:-3:0 %(3*log2(N)-3):-3:0
    %
    %         k = bitand( bitshift(hh,-shift), 7); % octant key
    %
    %         on = state2onByKey(state,k+1);  % octant number from key (state dependent, 1 based)
    %
    %         mmcheck = bitxor(mmcheck, bitshift(on-1,shift));
    %
    %         state = state2stateByOn(state,on); % state transition
    %     end
    %
    %     disp(mmcheck);
    %     [i,j,k] = deinterleave(mmcheck);
    %     disp([i,j,k]);
    %
    % name = 'Campbell';
    % startState = 1;
    % state2stateByOn = [...
    %      1  2  0  3  4  0  5  6; ...  0
    %      0  7  1  8  5  1  4  9; ...  1
    %     15  0  2 22 20  2 19 23; ...  2
    %     20  6  3 23 15  3 16 22; ...  3
    %     22 13  4 12 11  4  1 20; ...  4
    %     11 19  5 20 22  5  0 12; ...  5
    %      9  3  6  2 21  6 17  0; ...  6
    %     10  1  7 11 12  7 13 14; ...  7
    %     12  9  8 14 10  8 18 11; ...  8
    %      6  8  9  7 17  9 21  1; ...  9
    %      7 15 10 16 13 10 12 17; ... 10
    %      5 14 11  9  0 11 22  8; ... 11
    %      8 20 12 19 18 12 10  5; ... 12
    %     18  4 13  5  8 13  7 19; ... 13
    %     17 11 14  1  6 14 23  7; ... 14
    %      2 10 15 18 19 15 20 21; ... 15
    %     19 17 16 21  2 16  3 18; ... 16
    %     14 16 17 15 23 17  6 10; ... 17
    %     13 21 18 17  7 18  8 16; ... 18
    %     16  5 19  4  3 19  2 13; ... 19
    %      3 12 20 13 16 20 15  4; ... 20
    %     23 18 21 10 14 21  9 15; ... 21
    %      4 23 22  6  1 22 11  3; ... 22
    %     21 22 23  0  9 23 14  2; ... 23
    %     ];
    % state2stateByOn = state2stateByOn + 1;
    % state2onByKey = [...
    %     0 1 3 2 6 7 5 4; ...
    %     0 4 6 2 3 7 5 1; ...
    %     0 1 5 4 6 7 3 2; ...
    %     5 1 0 4 6 2 3 7; ...
    %     3 7 6 2 0 4 5 1; ...
    %     6 7 3 2 0 1 5 4; ...
    %     5 1 3 7 6 2 0 4; ...
    %     0 4 5 1 3 7 6 2; ...
    %     5 4 0 1 3 2 6 7; ...
    %     5 4 6 7 3 2 0 1; ...
    %     0 2 3 1 5 7 6 4; ...
    %     6 4 0 2 3 1 5 7; ...
    %     5 7 3 1 0 2 6 4; ...
    %     3 7 5 1 0 4 6 2; ...
    %     6 4 5 7 3 1 0 2; ...
    %     0 2 6 4 5 7 3 1; ...
    %     6 2 0 4 5 1 3 7; ...
    %     6 2 3 7 5 1 0 4; ...
    %     3 2 0 1 5 4 6 7; ...
    %     6 7 5 4 0 1 3 2; ...
    %     5 7 6 4 0 2 3 1; ...
    %     3 2 6 7 5 4 0 1; ...
    %     3 1 0 2 6 4 5 7; ...
    %     3 1 5 7 6 4 0 2];
    % state2onByKey = state2onByKey + 1;
    %
    % state2keyByOn = zeros(size(state2onByKey));
    % for i=1:size(state2keyByOn,1)
    %     state2keyByOn(i,state2onByKey(i,:)) = 0:7;
    % end
    %
    % startPeriod = 22;
    
    % keep track of states
    states = zeros(numel(m),21);
    for i = 1:numel(m)
        
        mm = m(i);
        
        hh = morton2hilbert(mm, startState,state2keyByOn,state2stateByOn,startPeriod);
        % check
        mmcheck = hilbert2morton(hh, startState,state2onByKey,state2stateByOn);
        assert(mmcheck == mm);
        
        h(i) = hh;
        
    end
    
    clear hh on shift state i j k mm;
    
    
    [~,I] = sort(h);
    
    figure;
    mesh([X(I),X(I)],[Y(I),Y(I)],[Z(I),Z(I)],[h(I),h(I)],'LineWidth',2);
    title(name);
    xlabel('x');
    ylabel('y');
    zlabel('z');
%     
    mat2LUTheader( outdir, ['Hilbert_',name,'_LUT'], 'HilbertLUTs', ['  struct ', name],...
        [...
        '    static constexpr uint8_t nd           = 3;',char(13),...
        '    static constexpr uint8_t start_state  = ', num2str(startState-1), ';' ,char(13),...
        '    static constexpr uint8_t start_period = ', num2str(startPeriod),  ';' ,...
        ],...
        struct(...
        'mat',{(state2stateByOn-1),(state2keyByOn),(state2onByKey-1)},...
        'name',{'state2stateByOrthant','state2keyByOrthant','state2orthantByKey'},...
        'tname', {'uint8_t','uint8_t','uint8_t'},...
        'dim1name',{'state','state','state'},...
        'dim2name',{'orthant','orthant','key'}...
        ) );
%     
%     mat2TEX(outdir, ['Hilbert_',name,'_state2state'],...
%         ['%',char(13),...
%         '      State transition table to generate the Hilbert code on curve ``',name,''''' from the Morton code. %',char(13),...
%         '      If the encoding is in state $i$ and the next three bit Morton digit is $j$, %',char(13),...
%         '      the table entry at row $i$ and column $j$ specifies the next state.'],...
%         ['Hilbert_',name,'_state2state'],...
%         (state2stateByOn-1));
%     
%     mat2TEX(outdir, ['Hilbert_',name,'_state2key'],...
%         ['%',char(13),...
%         '      Permutation table to generate the Hilbert code on curve ``',name,''''' from the Morton code. %',char(13),...
%         '      If the encoding is in state $i$ and the next three bit Morton digit is $j$, %',char(13),...
%         '      the table entry at row $i$ and column $j$ specifies the next digit of the Hilbert code.'],...
%         ['Hilbert_',name,'_state2key'],...
%         (state2keyByOn));
        
end