function [ state2state, state2key, baseStateNo ] = compactTransition( state2state, state2key, baseStateNo )
%removeTransitionDuplicates removes duplicates in state transition matrix
%and remaps

cycle = findTransitionCycles(state2state,baseStateNo);

baseStateNo = find(cycle==baseStateNo);
state2state = state2state(cycle,:);
state2state = arrayfun(@(x) find(cycle==x), state2state );

state2key = state2key(cycle,:);


[~,ia,ic] = unique([state2state, state2key],'rows');

baseStateNo = ic(baseStateNo);
state2state = state2state(ia,:);
state2state = arrayfun(@(x) ic(x), state2state );

state2key = state2key(ia,:);


end
