function [visited] = findTransitionCycles(state2state,startstate)


stack = [startstate];
visited = [];

cont = true;

while cont
    
    state = stack(1);
    stack = stack(2:end);       
    
    if (any(visited==state))
        if numel(stack)==0
            cont = false;
        end
    else
        visited = [state,visited];
        stack = [state2state(state,:),stack];
    end
    
end

visited = sort(visited(:));

end

