function str = mat2LUTheader( folder, filename, namespace, structname, prologue, s )
%MAT2HEADER generates a c header file, defining all elements of mat as an
%array of constant floats
%   mat is a 2 dimensional numeric Matrix, filename is the name of the
%   Headerfile without .h



str=[...
    '#ifndef ' filename, '_h ' char(13),...
    '#define ' filename, '_h ' char(13),...
    char(13),...
    'namespace ', namespace, char(13),...
    '{' char(13) ];

str = [str, structname, char(13), '  {', char(13), char(13)];

str = [str, prologue, char(13), char(13)];

for i = 1:numel(s)
    
    mat = s(i).mat;
    tname = s(i).tname;
    name = s(i).name;
    dim1name = s(i).dim1name;
    dim2name = s(i).dim2name;
    
    [m, n]=size(mat);
    
    switch tname(1:4)
        case 'uint'
            form = '%*u';
        case 'doub'
            form = '%*f';
    end
    
    
    str = [str,...
        '    inline static constexpr ', tname, ' ', name, '(const uint8_t ' dim1name ', const uint8_t ' dim2name ')' char(13),...
        '    {' char(13),...
        '      constexpr ', tname, ' ', name '[' num2str(m) '][' num2str(n) '] =' char(13),...
        '      {//   '];
    
    width = numel(num2str(max(mat(:))));
    
    for i2 =1:n
        str=[str sprintf('%*u',width,i2-1) '  '];
    end
    str(end-1) = '';
    str=[str, char(13)];
    
    for i1=1:m
        str=[str '          { '];
        for i2=1:n
            str=[str sprintf(form,width,mat(i1, i2)) ', '];
        end
        str(end-1) = '';
        str=[str '}, //' sprintf('%4u',i1-1), char(13)];
    end
    str(end-7) = ' ';
    str=[str '      };' char(13), char(13)];
    str=[str '      return ' name '[' dim1name '][' dim2name '];',char(13),...
        '    }',char(13),char(13)];
    
end

str=[str '  }; // end ', structname, char(13)];

str=[str '} // end namespace ', namespace, char(13),...
    '#endif'];



fid=fopen(fullfile(folder,[filename '.h']), 'wt+');
fprintf(fid, '%s', str);
fclose(fid);
end