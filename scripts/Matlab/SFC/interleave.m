function [ m ] = interleave( v )
%interleave bitwise interleaving vector elements

d = numel(v);

m = dec2bin(0,64);

bits_per_element = floor(64/d);
unused_bits = 64 - d*bits_per_element;

for i=1:d
    m( (unused_bits+i) : d : (64-d+i) ) = dec2bin(v(i),bits_per_element);    
end

m = uint64(bin2dec(m));

end

