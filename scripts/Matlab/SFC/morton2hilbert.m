function [ h ] = morton2hilbert( m, startState, state2keyByOn, state2stateByOn, period )
%MORTON2HILBERT convert morton key m to key on hilbert curve
%   hilbert curve given by
%   startState,
%   state2keyByOn
%   state2stateByOn

h = 0;
state = startState;
cycle = period * 3;
%     j = 1;

nlz = @(x) 64 - msb(x);

start_shift = 60 - cycle*floor((nlz(m) - 1)/cycle); 

for shift = start_shift:-3:0 
    %         states(i,j) = state;         j = j+1;
    on = bitand( bitshift(m,-shift), 7) + 1; % octant number (1 based)
    k = state2keyByOn(state,on); % octant key (state dependent)
    h = bitxor(h, bitshift(k,shift) ); % add octant key to key
    state = state2stateByOn(state,on); % state transition
end


end

