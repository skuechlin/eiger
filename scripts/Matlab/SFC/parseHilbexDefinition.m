function [base_on2perm,base_order,base_on2rev] = parseHilbexDefinition(defstr)
% 
% defstr = '< [ -2 3 -1 } -1 [ -3 -1 2 } 1 2 [ -1 2 3 } -1 3 [ 1 3 2 } -3 { 1 -3 2 ] 1 3 { -1 2 -3 ] -1 -2 { 3 -1 2 ] 1 { -2 -3 -1 ] >';
% % "Butz"
% defstr = '< [ 2 3 1 } 1 [ 3 1 2 } 2 [ 3 1 2 } -1 [ -1 -2 3 } 3 { -1 -2 -3 ] 1 { -3 1 2 ] -2 { -3 1 2 ] -1 { 2 -3 1 ] >';
% "Alfa"
% defstr = ['< [ 3 2 1 } 1 [ 3 1 2 } 2 { 3 1 -2 ] -1 [ -2 -1 3 } ',...
%     '3 { -2 -1 -3 ] 1 [ -3 1 -2 } -2 { -3 1 2 ] -1 { 2 -3 1 ] >'];

a = regexp(defstr,'(?<rev>([\[\{])) (?<perm>(-?\d.?){3}) [\]\}] (?<trav>(-?\d).)*','names');

rev = ([a.rev]' == '{');
perm = reshape(str2double(strsplit(strjoin({a.perm}))),[3,8])';
trav = str2double(strsplit(strtrim(strjoin({a.trav}))));

base_oct = [-1,-1,-1];
dir_checked = [0,0,0];

for t = trav
    at = abs(t);
    if dir_checked(at)==0
        dir_checked(at) = 1;
        base_oct(at) = base_oct(at)*sign(t);
    end
end

base_order = zeros(8,1);

%% octant vector [-1,-1,-1]...[1,1,1] to number (one based)
o2on = @(o) [2 1 .5] * (o(:)+1) + 1;

% octs = zeros(8,3);

oct = base_oct;
i = 1;
for tcs = {a(1:end-1).trav}
    t = str2double(strsplit(strtrim(strjoin(tcs))));
    base_order(i) = o2on(oct);
%     octs(i,:) = oct;
    oct(abs(t)) = sign(t);
    i = i+1;
end
base_order(i) = o2on(oct);
% % octs(i,:) = oct;

base_on2perm = perm;

base_on2rev = rev;


% rev_order = base_order(end:-1:1);
% 
% % reorder perm from base order to octant order
% base_on2perm(base_order,:) = perm;
% rev_on2perm(base_order,:) = perm(end:-1:1,:);
% 
% % 
% base_on2rev(base_order,:) = rev;
% rev_on2rev(base_order,:) = rev(end:-1:1);

% line(octs(:,1),octs(:,2),octs(:,3),'LineWidth',2);
% xlabel('1');
% ylabel('2');
% zlabel('3');

end