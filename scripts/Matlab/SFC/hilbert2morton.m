function [ m ] = hilbert2morton( h, startState, state2onByKey, state2stateByOn )
%HILBERT2MORTON convert hilbert key h to morton key
%   hilbert curve given by
%   startState,
%   state2onByKey
%   state2stateByOn

state = startState;
m = 0;
for shift = 60:-3:0 %(3*log2(N)-3):-3:0
    
    k = bitand( bitshift(h,-shift), 7); % octant key
    
    on = state2onByKey(state,k+1);  % octant number from key (state dependent, 1 based)
    
    m = bitxor(m, bitshift(on-1,shift));
    
    state = state2stateByOn(state,on); % state transition
end

end

