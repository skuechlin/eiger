/*
 * licenses.cpp
 *
 *  Created on: Apr 26, 2019
 *      Author: kustepha
 */

#include <iostream>

#include "licenses.h"

#include "eigen_lic.h"
#include "embree_lic.h"
#include "fastprng_lic.h"
#include "nanoflann_lic.h"
#include "optionparser_lic.h"
#include "rapidjson_lic.h"
#include "stacktrace_lic.h"
#include "xoshiro256plus_lic.h"

namespace ThirdpartyLicenses {

  template<unsigned int n>
  __attribute__((cold))
  static void _print_licenses() {
    constexpr LicenseData<(Name)n> l;
    std::cout << "********************************************************************************" <<
        std::endl;
    std::cout << l.name() << std::endl;
    std::cout << "Version " << l.version() << std::endl;
    std::cout << l.copyright() << std::endl;
    std::cout << l.url() << std::endl;
    std::cout << l.license() << std::endl;
    std::cout << "********************************************************************************" <<
        std::endl << std::endl;

    if constexpr (n>0) _print_licenses<n-1>();
  }

  __attribute__((cold))
  void print_thirdparty_licenses() {
    std::cout << "this software uses various third-party components, governed by the following licenses:";
    std::cout << std::endl << std::endl;;
    _print_licenses<_NUM_THIRDPARTY_NAMES_-1>();
  }

}
