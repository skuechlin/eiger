#ifndef optionparser_lic_h
#define optionparser_lic_h

namespace ThirdpartyLicenses {

  template<>
  struct LicenseData<OPTIONPARSER> {
    static constexpr const char* name()      { return "The Lean Mean C++ Option Parser"; }
    static constexpr const char* version()   { return "1.7"; }
    static constexpr const char* copyright() { return "Copyright (C) 2012-2017 Matthias S. Benkmann"; }
    static constexpr const char* url()       { return "http://optionparser.sourceforge.net/"; }
    static constexpr const char* license()   {
      return R"(
The "Software" in the following 2 paragraphs refers to this file containing
the code to The Lean Mean C++ Option Parser.
The "Software" does NOT refer to any other files which you
may have received alongside this file (e.g. as part of a larger project that
incorporates The Lean Mean C++ Option Parser).

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software, to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to permit
persons to whom the Software is furnished to do so, subject to the following
conditions:
The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.)"; }
  };
  
}
#endif
