#ifndef xoshiro256plus_lic_h
#define xoshiro256plus_lic_h

namespace ThirdpartyLicenses {

  template<>
  struct LicenseData<XOSHIRO> {
    static constexpr const char* name()      { return "xoshiro256+"; }
    static constexpr const char* version()   { return "1.0"; }
    static constexpr const char* copyright() { return "Written in 2018 by David Blackman and Sebastiano Vigna (vigna@acm.org)"; }
    static constexpr const char* url()       { return "http://xoshiro.di.unimi.it/"; }
    static constexpr const char* license()   {
      return R"(
To the extent possible under law, the author has dedicated all copyright
and related and neighboring rights to this software to the public domain
worldwide. This software is distributed without any warranty.

See http://creativecommons.org/publicdomain/zero/1.0/)";}
  };

}
#endif
