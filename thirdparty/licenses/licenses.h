/*
 * licenses.h
 *
 *  Created on: Apr 26, 2019
 *      Author: kustepha
 */

#ifndef THIRDPARTY_LICENSES_LICENSES_H_
#define THIRDPARTY_LICENSES_LICENSES_H_

namespace ThirdpartyLicenses {

  enum Name : unsigned int {
    XOSHIRO,
    STACKTRACE,
    RAPIDJSON,
    OPTIONPARSER,
    NANOFLANN,
    FASTPRNG,
    EIGEN,
    EMBREE,
    _NUM_THIRDPARTY_NAMES_
  };

  template <Name> struct LicenseData { };

  __attribute__((cold))
  void print_thirdparty_licenses();


}



#endif /* THIRDPARTY_LICENSES_LICENSES_H_ */
