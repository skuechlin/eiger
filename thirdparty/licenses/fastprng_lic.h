#ifndef fastprng_lic_h
#define fastprng_lic_h

namespace ThirdpartyLicenses {

  template<>
  struct LicenseData<FASTPRNG> {
    static constexpr const char* name()      { return "Fast PRNG: an Exponentially- and Normally-distributed PseudoRandom Number Generator"; }
    static constexpr const char* version()   { return "---"; }
    static constexpr const char* copyright() { return "(C) 2016 Christopher D. McFarland (cmcfarl2@stanford.edu)"; }
    static constexpr const char* url()       { return "https://bitbucket.org/cdmcfarland/fast_prng"; }
    static constexpr const char* license()   {
      return "https://www.tandfonline.com/doi/full/10.1080/00949655.2015.1060234"; }
  };
  
}
#endif
