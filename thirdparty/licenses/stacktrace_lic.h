#ifndef stacktrace_lic_h
#define stacktrace_lic_h

namespace ThirdpartyLicenses {

  template<>
  struct LicenseData<STACKTRACE> {
    static constexpr const char* name()      { return "stacktrace.h"; }
    static constexpr const char* version()   { return "---"; }
    static constexpr const char* copyright() { return "(c) 2008, Timo Bingmann from http://idlebox.net/"; }
    static constexpr const char* url()       { return "https://panthema.net/2008/0901-stacktrace-demangled/"; }
    static constexpr const char* license()   {
      return R"(
published under the WTFPL v2.0

            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
                    Version 2, December 2004

 Copyright (C) 2004 Sam Hocevar <sam@hocevar.net>

 Everyone is permitted to copy and distribute verbatim or modified
 copies of this license document, and changing it is allowed as long
 as the name is changed.

            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION

  0. You just DO WHAT THE FUCK YOU WANT TO.)";}
  };
}
#endif
